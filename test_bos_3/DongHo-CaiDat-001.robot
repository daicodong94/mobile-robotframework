*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary
Library     String

Resource    ../page/dong_ho/cai_dat.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/common.robot

*** Test Cases ***
BP-783
    [Documentation]    check mo giao dien khi cham vao bieu tuong dau ba cham
    [Tags]    BP-783    BP-784    BP-785    BP-786
    Log To Console    check mo giao dien khi cham vao bieu tuong dau ba cham tu giao dien dong ho
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    sleep    1
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    1
    AppiumLibrary.Page Should Contain Element    ${dh_hien_thi_kieu_dong_ho}
    Log To Console    check mo giao dien khi cham vao bieu tuong dau ba cham tu giao dien dong ho
    Go Back
    Giao dien dong ho
    Sleep    2
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    1
    AppiumLibrary.Page Should Contain Element    ${dh_hien_thi_kieu_dong_ho}
    Log To Console    check mo giao dien khi cham vao bieu tuong dau ba cham tu giao dien bo hen gio
    Go Back
    Giao dien bo hen gio
    Sleep    2
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    1
    AppiumLibrary.Page Should Contain Element    ${dh_hien_thi_kieu_dong_ho}
    Log To Console    check mo giao dien khi cham vao bieu tuong dau ba cham tu giao dien bam gio
    Go Back
    Giao dien dong ho bam gio
    Sleep    2
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    1
    AppiumLibrary.Page Should Contain Element    ${dh_hien_thi_kieu_dong_ho}
    Close Application
    sleep    5
BP-787
    [Documentation]    Check hien thi text dong ho
    [Tags]    BP-789    BP-788    BP-790    BP-791
    Log To Console    Check hien thi text dong ho
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    sleep    1
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    1
    AppiumLibrary.Get Element Attribute    ${dh_btn_check_kieu_dong_ho}    text
    ${get_text}=    AppiumLibrary.Get Text    ${dh_btn_check_kieu_dong_ho}
    Sleep    2
    Log To Console    Check hien thi cham vao kieu
    Kieu hien thi
    AppiumLibrary.Page Should Contain Element    ${dh_hien_thi_kieu_dong_ho_kim}
    AppiumLibrary.Page Should Contain Element    ${dh_btn_check_kieu_dong_ho}
    Log To Console    Check hien thi kieu dong ho kim tren tab dong ho khi cho kieu dong ho kim
    Hien thi dong ho kim
    Go Back
    sleep    1
    Giao dien dong ho
    AppiumLibrary.Page Should Not Contain Element    ${dh_btn_check_kieu_dong_ho}
    Log To Console    Check hien thi kieu dong ho so tren tab dong ho khi chon kieu dong ho so
    Cham vao dau ba cham
    sleep    2
    Nhan vao nut cai dat
    sleep    2
    Kieu hien thi
    sleep    1
    Hien thi dong ho so
    Go Back
    sleep    1
    Giao dien dong ho
    AppiumLibrary.Page Should Contain Element    ${dh_dong_ho_so_trong_dong_ho}
    Close Application
    sleep    5
BP-793
    [Documentation]    Check hien thi thoi gian voi so giay mac dinh tat
    [Tags]    BP-793    BP-797    BP-796
    Log To Console    Check hien thi thoi gian voi so giay mac dinh tat
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    sleep    1
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    1
    AppiumLibrary.Get Element Attribute    ${dh_on_off_hien_thi_thoi_gian}    text
    ${get_text}=    AppiumLibrary.Get Text    ${dh_on_off_hien_thi_thoi_gian}
    sleep    2
    Log To Console    Check hien thi thoi gian tren tab dong ho voi kieu dong ho so va hien thi thoi gian voi so giay bat
    AppiumLibrary.Click Element    ${dh_on_off_hien_thi_thoi_gian}
    Go Back
    Giao dien dong ho
    sleep    1
    AppiumLibrary.Page Should Contain Element    ${dh_time_seconds}
    sleep    1
    Log To Console    Check hien thi thoi gian tren tab dong ho voi kieu dong ho kim va hien thi thoi gian voi so giay bat
    Cham vao dau ba cham
    sleep    2
    Nhan vao nut cai dat
    sleep    1
    Kieu hien thi
    Hien thi dong ho kim
    Go Back
    Giao dien dong ho
    AppiumLibrary.Page Should not Contain Element    ${dh_time_seconds}
    Close Application

BP-805
    [Documentation]    Check hien thi danh sach mui gio khi cham vao mui gio chinh
    [Tags]    BP-804    BP-805    BP-806    BP-807
    ...    DH_CD_020505    BP-809    BP-811    BP-812
    Log To Console    Check hien thi danh sach mui gio khi cham vao mui gio chinh
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    sleep    1
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    1
    Click icon on va off tu dong mui gio chinh
    Sleep    2
    Go Back
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    AppiumLibrary.Get Element Attribute    ${dh_check_hien_thi_mui_gio}    text
    ${get_text}=    AppiumLibrary.Get Text    ${dh_check_hien_thi_mui_gio}
    sleep    3
    Log To Console    Check hien thi mui gio chinh bi chim khi thoat ra va vao lai cai dat
    Go Back
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    1
    AppiumLibrary.Get Element Attribute    ${dh_check_hien_thi_mui_gio}    text
    ${get_text}=    AppiumLibrary.Get Text    ${dh_check_hien_thi_mui_gio}
    sleep    2
    Log To Console    Check hien thi mui gio ro net khi bat dong ho tu dong
    Click icon on va off tu dong mui gio chinh
    AppiumLibrary.Get Element Attribute    ${dh_check_hien_thi_mui_gio}    text
    ${get_text}=    AppiumLibrary.Get Text    ${dh_check_hien_thi_mui_gio}
    sleep    2
    Log To Console    Check hien thi mui gio chinh hien thi khi thoat ra va vao lai cai dat
    AppiumLibrary.Go Back
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    1
    AppiumLibrary.Get Element Attribute    ${dh_check_hien_thi_mui_gio}    text
    ${get_text}=    AppiumLibrary.Get Text    ${dh_check_hien_thi_mui_gio}
    sleep    2
    Log To Console    Check hien thi mui gio chinh mac dinh jakarta
    AppiumLibrary.Click Element    ${dh_check_mui_gio_chinh}
    sleep    1
    Chon mui gio Jakarta
    sleep    1
    AppiumLibrary.Page Should Contain Element    ${dh_check_mui_gio_hien_thi_jakarta}
    Log To Console    Check dong popup    quay ve cai    dat khi button huy o man hinh danh sach mui gio
    AppiumLibrary.Click Element    ${dh_check_mui_gio_chinh}
    Cham vao huy
    AppiumLibrary.Page Should Not Contain Element    ${dh_btn_huy}
    Close Application
    sleep    5

BP-813
    [Documentation]    Check hien thi ngay va gio cua Cai dat he thong khi cham vao thay doi ngay gio
    [Tags]    BP-813    BP-814    BP-815    BP-816
    ...    BP-817    BP-818     BP-819    BP-820
    Log To Console    Check hien thi ngay va gio cua Cai dat he thong khi cham vao thay doi ngay gio
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    sleep    1
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    Sleep    2
    Cham vao thay doi ngay gio
    sleep    2
    AppiumLibrary.Page Should Contain Element    ${dh_cd_thoi_gian}
    Log To Console    Check hien thi thu ngay thang tren tab dong ho
    sleep    1
    Go Back
    Go Back
    Giao dien dong ho
    sleep    1
    AppiumLibrary.Page Should Contain Element    ${dh_dong_ho_so_trong_dong_ho}
    Log To Console    Check hien thi thu ngay tren tab dong ho
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    2
    Cham vao thay doi ngay gio
    sleep    2
    Click icon tat tu dong cap nhat ngay theo mang
    Sleep    2
    Click cai dat ngay
    sleep    2
    Cham vao ngay 1 trong thay doi ngay gio
    sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_ngay_thang}
    ${get_text_hoa}=    Convert To Upper Case    ${get_text}
    sleep    3
    cham vao dong y
    Sleep    2
    Go Back
    Sleep    2
    Go Back
    sleep    1
    Giao dien dong ho
    AppiumLibrary.Page Should Contain Element    ${dh_dong_ho_so_trong_dong_ho}    ${get_text_hoa}
    sleep    2
    Log To Console    Check hien thi io tren tab dong ho khi thay doi gio mac dinh
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    1
    Cham vao thay doi ngay gio
    sleep    2
    Click icon tat tu dong cap nhat ngay theo mang
    Sleep    2
    Click Element    ${dh_cd_thoi_gian}
    sleep    2
    AppiumLibrary.Click Element    ${dh_cd_check_1h}
    AppiumLibrary.Click Element    ${dh_cd_check_5min}
    AppiumLibrary.Get Element Attribute    ${dh_cd_gio}    text
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_gio}
    cham vao dong y
    Go Back
    Sleep    2
    Go Back
    Sleep    2
    Go Back
    Giao dien dong ho
    sleep    1
    Element Should Contain Text    ${dh_dong_ho_so_trong_dong_ho}    ${get_text}
    Log To Console    Check hien thi thoi gian khi tao bao thuc tran tab quan ly bao thuc
    giao dien bao thuc
    sleep    1
    Log To Console    Check hien thi thoi gian khi toa bao thuc nhanh
    cham vao mo rong trong bao thuc
    Cham xoa trong bao thuc
    cham vao mo rong trong bao thuc
    Cham xoa trong bao thuc
    Sleep    1
    Close Application

BP-820
    # co thay doi tesxt giua b3 va b86
    [Documentation]    Check hien thi thoi gian khi mui gio mac dinh bat
    [Tags]    BP-820    BP-821
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    sleep    1
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    Sleep    2
    Cham vao thay doi ngay gio
    Sleep    2
    Click icon tat tu dong cap nhat gio theo mang
    Sleep    2
    ${get_text}=    Get Text     ${dh_check_mui_gio_dong_au}
    Click Element    ${dh_cd_mui_gio}
    Sleep    1
    Log To Console    Check hien thi thoi gian tren tab dong ho khi chon mui gio khac
    Click Element    ${dh_cd_chon_mui_gio_jakarta}
    Sleep    2
    Click icon tat tu dong cap nhat gio theo mang
    Sleep    1
    Element Should Contain Text    ${dh_check_mui_gio_dong_au}    GMT+07:00 Western Indonesia Time
    Sleep    2
    Close Application

BP-829
    [Documentation]    Check hien thi thoi gian im lang voi mac dinh la 10 phut
    [Tags]    BP-829    BP-830    DH_CD_040103
    Log To Console    Check hien thi thoi gian im lang voi mac dinh la 10 phut
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    sleep    1
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    2
    AppiumLibrary.Get Element Attribute    ${dh_cd_im_lang_sau_10_phut}    text
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_im_lang_sau_10_phut}
    Element Should Contain Text    ${dh_cd_im_lang_sau_10_phut}    ${get_text}
    Log To Console    Check cham chon thoi gian im lang sau
    Cham vao im lang sau
    AppiumLibrary.Page Should Contain Element    ${dh_btn_huy}
    sleep    1
    Log To Console    Check hien thi thoi gian im lang sau khi chon
    Cham vao 5 phut
    sleep    1
    AppiumLibrary.Get Element Attribute    ${dh_cd_im_lang_sau_5_phut}    text
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_im_lang_sau_5_phut}
    Element Should Contain Text    ${dh_cd_im_lang_sau_5_phut}    ${get_text}
    Close Application
BP-835
    [Documentation]    Check hien thi thoi luong bao lai la 10 phut
    [Tags]    BP-835    BP-836    BP-837    BP-838    BP-839
    Log To Console    Check hien thi thoi luong bao lai la 10 phut
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    sleep    1
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    AppiumLibrary.Get Element Attribute    ${dh_cd_im_lang_sau_10_phut}    text
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_im_lang_sau_10_phut}
    Element Should Contain Text    ${dh_cd_im_lang_sau_10_phut}    ${get_text}
    Log To Console    Check cham cho thoi gian bao thuc
    Sleep    2
    Cham vao thoi luong bao thuc
    AppiumLibrary.Page Should Contain Element    ${dh_btn_huy}
    Log To Console    Check di chuyen len xuong
    Sleep    2
    Vuot xuong mm
    AppiumLibrary.Page Should Contain Element    ${dh_cd_get_so_phut}
    Sleep    2
    Vuot xuong mm
    AppiumLibrary.Page Should Contain Element    ${dh_cd_get_so_phut}
    Log To Console    Check cham huy
    Sleep    2
    Cham vao huy
    AppiumLibrary.Page Should Contain Element    ${dh_cd_im_lang_sau}
    Log To Console    Check hien thi thoi luong bao lai sau khi chon
    Sleep    2
    Cham vao thoi luong bao thuc
    Sleep    2
    Vuot xuong mm
    sleep    3
    cham vao dong y
    Sleep    2
    AppiumLibrary.Get Element Attribute    ${dh_cd_im_lang_sau_13_phut}    text
    sleep    1
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_im_lang_sau_13_phut}
    Element Should Contain Text    ${dh_cd_im_lang_sau_13_phut}    ${get_text}
    sleep    1
    Close Application
    sleep    5

BP-845
    [Documentation]    Check tang dan tang dan am luong mac dinh la tat
    [Tags]     BP-845     BP-846     BP-847    BP-848
    Log To Console    Check tang dan tang dan am luong mac dinh la tat
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    2
    Vuot launcher len tren
    Sleep    2
    Log To Console    Check chon thoi gian tang dan am luong
    Cham tang dan am luong
    Sleep    2
    Swipe    395    1193    395    1030
    Sleep    2
    cham vao dong y
    AppiumLibrary.Page Should Contain Element    ${dh_cd_thoi_luong_5_giay}
    Log To Console    Check di chuyen len xuong
    Sleep    1
    Cham tang dan am luong
    Sleep    1
    Swipe    395    1030    395    1193
    Swipe    395    1030    395    1193
    cham vao dong y
    Sleep    1
    AppiumLibrary.Page Should Contain Element    ${dh_cd_thoi_luong_60_giay}
    Sleep    1
    Cham tang dan am luong
    Swipe    395    1030    395    1193
    Cham vao huy
    AppiumLibrary.Page Should Contain Element    ${dh_cd_thoi_luong_60_giay}
    Log To Console    Check di chuyen len xuong
    Sleep    1
    AppiumLibrary.Page Should Contain Element    ${dh_cd_tang_dan_am_luong}
    Sleep    1
    Cham tang dan am luong
    Swipe    395    1193    395    1030
    Swipe    395    1193    395    1030
    cham vao dong y
    Sleep    1
    AppiumLibrary.Get Element Attribute    ${dh_cd_thoi_luong_5_giay}    text
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_thoi_luong_5_giay}
    Element Should Contain Text    ${dh_cd_thoi_luong_5_giay}    ${get_text}
    Close Application

BP-863
    [Documentation]    Check tuan bat dau mac dinh la thu hai
    [Tags]        BP-863    BP-864    BP-865
    Log To Console    Check tuan bat dau mac dinh la thu hai
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Click Element At Coordinates    1006      144
    sleep    1
    Nhan vao nut cai dat
    sleep    1
    Swipe By Percent    50    90    50    20
    Check hien thi thu hai
    log to console     Check chon thu
    Nhan vao tuan bat dau
    Sleep    1
    Page Should Contain Element     ${dh_cd_txt_chu_nhat}
    Page Should Contain Element     ${dh_cd_txt_thu_hai}
    Page Should Contain Element    ${dh_cd_txt_thu_bay}
    log to console     Check hien thi sau khi chon thu hai thu bay chu nhat
    Click Element    ${dh_cd_txt_thu_bay}
    Sleep    1
    Page Should Contain Element    ${dh_cd_txt_thu_bay}
    Nhan vao tuan bat dau
    Click Element    ${dh_cd_txt_chu_nhat}
    Sleep    1
    Page Should Contain Element    ${dh_cd_txt_chu_nhat}

BP-866
    [Documentation]    Check am thanh hen gio mac dinh
    [Tags]    BP-866    BP-867    BP-875    BP-868
    Log To Console    Check am thanh hen gio mac dinh
    Page Should Contain Element     ${dh_cd_txt_bo_hen_gio_het_han}
    Sleep    2
    Log To Console    Check am thanh them moi
    Click Element    ${dh_cd_txt_am_thanh_hen_gio}
    Sleep    1
    Click Element    ${dh_cd_txt_Argon_am_nhac}
    Sleep    2
    Go Back
    Log To Console    Check am thanh thiet bi mac dinh
    Element Attribute Should Match     ${dh_cd_txt_am_thanh_hen_gio}     text    *Timer Expired*
    Log To Console    Check di chuyen len xuong danh sach
    Swipe By Percent    50    20    50    90
    Page Should Not Contain Element    ${dh_cd_txt_Argon_am_nhac}
    Swipe By Percent    50    90    50    20
    Page Should Contain Element    ${dh_cd_txt_am_thanh_hen_gio}
    Log To Console    Check hien thi am thanh sau khi chon am thanh bo hen gio
    Click Element     ${dh_cd_txt_am_thanh_hen_gio}
    Sleep    1
    cham vao dong y
    Page Should Contain Element     ${dh_cd_txt_Argon_am_nhac}
    log to console    Check tang dan am luong
    Click Element    ${dh_cd_txt_tang_am_luong}
    Sleep    3
    Swipe    395    1193    395    1030
    Sleep    2
    cham vao dong y
    AppiumLibrary.Page Should Contain Element    ${dh_cd_thoi_luong_5_giay}
    Sleep    2
    Close Application

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================