*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library    AppiumLibrary

Resource    ../page/dong_ho/bao_thuc.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/common.robot

*** Test Cases ***
BP-27
    [Documentation]    Mo app dong ho tu launcher
    [Tags]    BP-27
    Mo Dong ho
    Cho phep cap quyen cho ung dung

BP-320
    [Documentation]    Check mo giao dien quan ly bao thuc
    [Tags]    BP-320
    Bam vao bao thuc nhanh
    sleep    2
    Log To Console    mo giao dien quan ly bao thuc
    AppiumLibrary.Page Should Contain Element    ${dh_bt_btn_xong}


BP-309
    [Documentation]    Check chuc nang huy bao thuc nhanh bang cach vuot tu mep trai phai man hinh vao giua
    [Tags]    BP-309
    # Vuot tu mep trai vao giua man hinh
    Go Back
    sleep    1
    Log To Console    Dong giao dien quan ly bao thuc
    AppiumLibrary.Page Should not Contain Element    ${dh_bt_btn_xong}
    sleep    2
    Close Application

BP-311
    [Documentation]    Check chuc nang huy tao bao thuc nhanh bang thao tac vuot home
    [Tags]    BP-311
    Log To Console    Dong giao dien quan ly bao thuc
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Sleep    2
    Bam vao bao thuc nhanh
    Sleep    2
    vuot tu mep duoi vao giua man hinh
    Sleep    2
    Bam dong toan bo app tren da nhiem
    Sleep    2
    AppiumLibrary.Page Should not Contain Element    ${dh_bt_btn_xong}
    Sleep    2
    Close Application
    Sleep    5

BP-316
    [Documentation]    Check chuc nang tao bao thuc nhanh
    [Tags]    BP-316
    Log To Console    Tao bao thuc nhanh
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    5
    Bam vao bao thuc nhanh
    Sleep    2
    Bam nut tao xong bao thuc
    Sleep    2
    AppiumLibrary.Page Should not Contain Element    ${dh_bt_btn_xong}
    Sleep    2
    AppiumLibrary.Page Should Contain Element    ${dh_cd_bao_thuc_ngan}
    Close Application
BP-319
    [Documentation]    Check mo tu giao dien quan ly bao thuc
    [Tags]    BP-319
    Log To Console    Mo giao dien quan ly bao thuc
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    5
    Bam vao nut bao thuc
    Sleep    2
    AppiumLibrary.Page Should Contain Element    ${dh_bt_bao_thuc_nhanh}

BP-322
    [Documentation]    Check hien thi dung thoi gian bao thuc da chon
    [Tags]    BP-322
    Log To Console    Check hien thi dung thoi gian bao thuc da chon
    Bam vao them bao thuc
    sleep    1
    Cham chon so gio dat bao thuc b3mm
    sleep    1
    AppiumLibrary.Page Should Contain Element    ${dh_bt_nut_huy}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_hien_thi_thoi_gian_bao_thuc}

BP-324
    [Documentation]    Check chuc nang huy bao thuc bang cach bam vao huy
    [Tags]    BP-324
    Log To Console    Dong giao dien quan ly bao thuc
    Bam vao huy
    AppiumLibrary.Page Should Contain Element    ${dh_bt_bao_thuc_nhanh}

BP-326
    [Documentation]    Check chuc nang huy bao thuc bang cach vuot back
    [Tags]    BP-326
    Log To Console    Dong giao dien quan ly bao thuc
    Bam vao them bao thuc
    Sleep    2
    Go Back
    Sleep    2
    AppiumLibrary.Page Should Contain Element    ${dh_bt_bao_thuc_nhanh}
    AppiumLibrary.Page Should Not Contain Element    ${dh_bt_nut_huy}
    Close Application

BP-327
    [Documentation]    Check chuc nang huy bao thuc bang cach vuot tu home ve giua man hinh
    [Tags]    BP-327
    Log To Console    Dong giao dien quan ly bao thuc
    Log To Console    Check chuc nang chuyen qua giao dien bao thuc theo dong ho so
    Mo Dong ho
    sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Bam vao them bao thuc
    sleep    1
    vuot tu mep duoi vao giua man hinh
    sleep    1
    Bam dong toan bo app tren da nhiem
    AppiumLibrary.Page Should not Contain Element    ${dh_bt_nut_dong_y}
    sleep    1
    Close Application

BP-333
    [Documentation]    Check chuc nang huy bao thuc
    [Tags]    BP-333
    Log To Console    Check chuc nang huy bao thuc
    Mo Dong ho
    sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Bam vao them bao thuc
    sleep    1
    Bam vao huy
    AppiumLibrary.Page Should Contain Element    ${dh_bt_bao_thuc_nhanh}
    sleep    1

BP-329
    [Documentation]    Check chuc nang dong y bao thuc
    [Tags]    BP-329
    Log To Console    Check chuc nang dong y bao thuc
    Bam vao them bao thuc
    Sleep    2
    Bam nut tao xong bao thuc
    AppiumLibrary.Page Should Contain Element    ${dh_bt_bao_thuc_nhanh}
    sleep    1
BP-351
    [Documentation]    Check chuc nang lap lai bao thuc
    [Tags]    BP-351
    Log To Console    Check chuc nang lap lai bao thuc
    Bam vao lap lai bao thuc
    sleep    2
    AppiumLibrary.Page Should Contain Element    ${dh_bt_txt_hien_thi_thu_hai}
    sleep    1
    Close Application

BP-356
    [Documentation]    Check chuc tat lap lai
    [Tags]    BP-356
    Log To Console    Check chuc tat lap lai
    Mo Dong ho
    sleep    2
    Cho phep cap quyen cho ung dung
    sleep    1
    Bam vao them bao thuc
    sleep    1
    Bam nut tao xong bao thuc
    Sleep    2
    Bam vao lap lai bao thuc
    sleep    2
    Bam vao cac thu trong bao thuc 2 3 4
    sleep    5
    Bam vao lap lai bao thuc
    AppiumLibrary.Page Should Contain Element    ${dh_bt_nut_lap_lai_bao_thuc}

BP-358
    [Documentation]    Check chuc nang tat lap lai
    [Tags]    BP-358
    Log To Console    Check chuc tat lap lai
    Bam vao them bao thuc
    sleep    1
    Bam nut tao xong bao thuc
    Sleep    2
    Bam vao lap lai bao thuc
    sleep    1
    Bam vao cac thu trong bao thuc 2 3 4
    sleep    5
    Bam vao cac thu trong bao thuc 5 6 7 cn
    sleep    5
    AppiumLibrary.Page Should not Contain Element    ${dh_bt_txt_hien_thi_thu_hai}
    Close Application
BP-359
    [Documentation]    Check mo danh sach am bao thuc
    [Tags]    BP-359
    Log To Console    Check mo danh sach am bao thuc
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Bam vao them bao thuc
    Sleep    1
    Bam nut tao xong bao thuc
    Sleep    2
    Bam vao chuong
    Sleep    2
    Bam vao mo danh sach nha chuong
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Bam vao mo danh sach nhac chuong tai file chon
    Sleep    1
    AppiumLibrary.Page Should Contain Element    ${dh_bt_kieu_chuong_neptunium}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_kieu_chuong_tititit}

BP-365
    [Documentation]    Check chuc nag dong am thanh bao thuc bang nut back
    [Tags]    BP-365
    Log To Console    Check chuc nag dong am thanh bao thuc
    Sleep    1
    Go Back
    sleep    2
    AppiumLibrary.Page Should Not Contain Element    ${dh_bt_kieu_chuong_neptunium}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_bao_thuc_nhanh}

BP-368
    [Documentation]    Check chuc nag dong am thanh bang thao tac vuot home
    [Tags]    BP-368
    Log To Console    Check chuc nag dong am thanh bang thao tac vuot home
    Bam vao chuong
    Sleep    2
    Bam vao mo danh sach nha chuong
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Bam vao mo danh sach nhac chuong tai file chon
    Sleep    1
    vuot tu mep duoi vao giua man hinh
    Sleep    1
    Bam dong toan bo app tren da nhiem
    AppiumLibrary.Page Should not Contain Element    ${dh_bt_bao_thuc_nhanh}
    Close Application

BP-372
    [Documentation]    Check chuc nag dong am thanh bang thoa tac vuot home
    [Tags]    BP-372
    Log To Console    Check chuc nag dong am thanh bang thoa tac vuot home
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Bam vao them bao thuc
    sleep    1
    Bam nut tao xong bao thuc
    sleep    2
    Bam vao rung
    sleep    1
    Element Attribute Should Match    ${dh_bt_cd_rung}    checked    false
BP-374
    [Documentation]    Check chuc nag tao nhan
    [Tags]    BP-374
    Log To Console    Check chuc nag tao nhan
    Bam vao nhan
    Sleep    2
    Bam vao thanh nhap text nhan
    sleep    1
    AppiumLibrary.Input Text    ${dh_bt_label_nhan_text}    thinhlq
    sleep    1
    Bam vao dong y bao thuc
    Sleep    2
    Element Attribute Should Match    ${dh_bt_label_nhan_dan}    text    thinhlq
    sleep    1

BP-376
    [Documentation]    Check chuc nang huy tao nhan
    [Tags]    BP-376
    Log To Console    Check chuc nang huy tao nhan
    Bam vao nhan
    Sleep    2
    Bam vao huy nhan
    AppiumLibrary.Page Should Not Contain Element    ${dh_bt_label_nhan_text}
    Close Application
BP-377
    [Documentation]    Check chuc nang xoa bao thuc
    [Tags]    BP-377
    Log To Console    Check chuc nang xoa bao thuc
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Bam vao them bao thuc
    Sleep    1
    Bam nut tao xong bao thuc
    Sleep    2
    Bam vao xoa
    Sleep    2
    Bam vao mo rong bao thuc xoa lan luot bao thuc
    sleep    1
    Bam vao xoa
    sleep    1
    Bam vao mo rong bao thuc xoa lan luot bao thuc
    sleep    1
    Bam vao xoa
    sleep    1
    AppiumLibrary.Page Should Contain Element    ${dh_tab_bao_thuc}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_khong_co_bao_thuc}
    sleep    1

BP-383
    [Documentation]    Check mo rong giao dien chinh sua bao thuc
    [Tags]    BP-383
    Log To Console    Check mo rong giao dien chinh sua bao thuc
    Bam vao them bao thuc
    Sleep    2
    Bam nut tao xong bao thuc
    Sleep    2
    AppiumLibrary.Page Should Contain Element    ${dh_bt_cd_rung}

BP-384
    [Documentation]    Check thu gon giao dien chinh sua bao thuc
    [Tags]    BP-384
    Log To Console    Check thu gon giao dien chinh sua bao thuc
    Sleep    2
    Bam vao thu gon bao thuc
    sleep    1
    AppiumLibrary.Page Should not Contain Element    ${dh_bt_cd_rung}
    Close Application

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================