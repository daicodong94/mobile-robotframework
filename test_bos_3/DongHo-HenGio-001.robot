*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/dong_ho/hen_gio.robot
Resource    ../page/common.robot
Resource    ../page/dong_ho/dh_common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot

*** Test Cases ***
# BP-666
    # [Documentation]    Check mo giao dien hen gio
    # [Tags]    BP-424    BP-426    BP-428
    # Log To Console    thuc hien kiem tra giao dien bao thuc
    # Cho phep cap quyen cho ung dung
    # Sleep    1
    # Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_baothuc    3
    # Sleep    1
    # Nhan nut tab menu top dong ho
    # Log To Console    thuc hien kiem tra giao dien dong ho
    # Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_dongho    3
    # Nhan nut tab menu top bam gio
    # Sleep    1
    # Log To Console    thuc hien kiem tra giao dien bam gio
    # Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_bamgio    3
    # Nhan nut tab menu top hen gio
    # Sleep    1
    # Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_hengio    3
    # Sleep    2
    # Close Application

BP-659
    [Documentation]    Check mo giao dien hen gio khi dang o man hinh bao thuc
    [Tags]    BP-659
    Log To Console    Check mo giao dien hen gio khi dang o man hinh bao thuc
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Nhan nut menu top hen gio
    Sleep    2
    Kiem tra emlement man hinh hen gio
    Close Application

BP-660
    [Documentation]    Check mo giao dien hen gio khi dang o man hinh dong ho
    [Tags]    BP-660    BP-661
    Log To Console    Check mo giao dien hen gio khi dang o man hinh dong ho
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Nhan nut dong ho
    Sleep    2
    Nhan nut menu top hen gio
    Sleep    2
    Kiem tra emlement man hinh hen gio
    Sleep    2
    Nhan nut dong ho bam gio
    Sleep    2
    Nhan nut menu top hen gio
    Sleep    2
    Kiem tra emlement man hinh hen gio
    Sleep    2
    Close Application

BP-662
    [Documentation]    Check da bat hen gio tai man hinh bao thuc
    [Tags]    BP-662    BP-663    BP-664
    Log To Console    Check da bat hen gio tai man hinh bao thuc
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Nhan nut menu top hen gio
    Sleep    2
    Thiet lap cai bo hen gio
    Sleep    2
    Nhan nut dong ho
    Sleep    2
    Nhan nut menu top hen gio
    Sleep    5
    Kiem tra veryfile man hinh hen gio da bat
    Sleep    2
    Nhan nut dong ho bam gio
    Sleep    2
    Nhan nut menu top hen gio
    Sleep    5
    Kiem tra veryfile man hinh hen gio da bat
    Sleep    2
    Xoa bo hen gio
    Sleep    2
    Close Application

BP-673
    [Documentation]    Check nhap 1s hien thi tren man hinh bo hen gio
    [Tags]    BP-664     BP-674    BP-678    BP-679    BP-680    BP-681
    ...    BP-681    BP-683    BP-684     BP-685    BP-686    BP-687
    ...    BP-690     BP-694
    Log To Console    Check nhap 1s hien thi tren man hinh bo hen gio
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Sleep    2
    Nhan nut menu top hen gio
    Sleep    2
    Thuc hien kiem tra nhap so va check nut xoa
    Sleep    3
    Kiem tra emlement man hinh hen gio
    Sleep    2
    Close Application

BP-688
    [Documentation]    Thuc hien kiem tra text dat lai hen gio
    [Tags]    BP-688
    Log To Console    Thuc hien kiem tra text dat lai hen gio
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Nhan nut menu top hen gio
    Sleep    5
    Thuc hien tao hen gio check dat lai hen gio nhu ban dau khi nhan Pause
    Sleep    2
    Close Application

BP-689
    [Documentation]    Thuc hien kiem tra text dat lai hen gio khi nhan vao dong ho hen gio dang chay
    [Tags]    BP-689    BP-691    BP-692    BP-693
    Log To Console    Thuc hien kiem tra text dat lai hen gio khi nhan vao dong ho hen gio dang chay
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Sleep    2
    Nhan nut menu top hen gio
    Sleep    5
    Thuc hien nhan dong ho hen gio dang chay
    Sleep    2
    Close Application

BP-695
    [Documentation]    Thuc hien kiem tra text dat lai hen gio khi nhan vao dong ho hen gio dang chay
    [Tags]    BP-695
    Log To Console    Thuc hien kiem tra text dat lai hen gio khi nhan vao dong ho hen gio dang chay
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Nhan nut menu top hen gio
    Sleep    6
    Thuc bam vao dat lai restet lai bo hen gio ve ban dau
    Sleep    2
    Close Application

BP-697
    [Documentation]    Thuc hien kiem tra nhan comment
    [Tags]    BP-697    BP-698     DH_BHG_060604    BP-700
    Log To Console    Thuc hien kiem tra nhan comment
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    5
    Nhan nut menu top hen gio
    Sleep    6
    Thuc hien nhap vao nhan comment va thiet lap
    Sleep    2
    Close Application

BP-702
    [Documentation]    Thuc hien kiem tra bo hen gio qua han
    [Tags]    BP-701    BP-702    BP-703
    Log To Console    Thuc hien kiem tra bo hen gio qua han
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    6
    Nhan nut menu top hen gio
    Sleep    6
    Thuc hien kiem tra notify bo hen gio qua han thong bao
    Sleep    6
    Close Application

BP-704
    [Documentation]    Thuc hien kiem tra bo hen gio qua han nhan them 1 phut
    [Tags]    BP-704
    Log To Console    Thuc hien kiem tra bo hen gio qua han nhan them 1 phut
    Mo Dong ho
    Sleep    6
    Cho phep cap quyen cho ung dung
    Sleep    6
    Nhan nut menu top hen gio
    Sleep    6
    Thuc hien kiem tra bo hen gio qua han nhan cong them thoi gian 1 phut
    Sleep    6
    Close Application

BP-706
    [Documentation]    Thuc hien kiem tra nut xoa khi bo hen gio dang chay
    [Tags]    BP-706
    Log To Console    Thuc hien kiem tra nut xoa khi bo hen gio dang chay
    Mo Dong ho
    Sleep    6
    Cho phep cap quyen cho ung dung
    Sleep    6
    Nhan nut menu top hen gio
    Sleep    6
    Thuc hien thiet lap bo hen gio kiem tra nut xoa khi bo hen gio dang chay
    Sleep    6
    Close Application

BP-707
    [Documentation]    Thuc hien kiem tra nut xoa khi bo hen gio da tam dung
    [Tags]    BP-707
    Log To Console    Thuc hien kiem tra nut xoa khi bo hen gio da tam dung
    Mo Dong ho
    Sleep    6
    Cho phep cap quyen cho ung dung
    Sleep    6
    Nhan nut menu top hen gio
    Sleep    6
    Thuc hien thiet lap bo hen gio kiem tra nut xoa khi bo hen gio da tam dung
    Sleep    6
    Close Application

BP-708
    [Documentation]    Thuc hien kiem tra nut them bo hen gio khi co mot bo hen gio dang chay
    [Tags]    BP-708    BP-709
    Log To Console    Thuc hien kiem tra nu them bo hen gio khi co mot bo hen gio dang chay
    Mo Dong ho
    Sleep    3
    Cho phep cap quyen cho ung dung
    Sleep    6
    Nhan nut menu top hen gio
    Sleep    10
    Thuc hien thiet lap bo hen gio moi
    Sleep    6
    Close Application

BP-710
    [Documentation]    Thuc hien kiem tra nut them bo hen gio khi co mot bo hen gio dang chay
    [Tags]    BP-710
    Log To Console    Thuc hien kiem tra nu them bo hen gio khi co mot bo hen gio dang chay
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    6
    Nhan nut menu top hen gio
    Sleep    8
    Thuc hien thiet lap huy bo hen gio moi
    Sleep    6
    Close Application

BP-711
    [Documentation]    Check vi tri cua bo hen gio khi vuot thay doi vi tri cho nhau
    [Tags]    BP-712    BP-713     BP-714
    Log To Console    Check vi tri cua bo hen gio khi vuot thay doi vi tri cho nhau
    Mo Dong ho
    Sleep    6
    Cho phep cap quyen cho ung dung
    Sleep    6
    Nhan nut menu top hen gio
    Sleep    10
    Thuc hien vuot chuyen tu giao dien bo hen gio 1 den giao dien 2 va 3
    Sleep    6
    Close Application

BP-715
    [Documentation]    Check vi tri cua bo hen gio khi vuot thay doi vi tri cho nhau
    [Tags]    BP-715
    Log To Console    Check vi tri cua bo hen gio khi vuot thay doi vi tri cho nhau
    Mo Dong ho
    Sleep    6
    Cho phep cap quyen cho ung dung
    Sleep    6
    Nhan nut menu top hen gio
    Sleep    6
    Thuc hien them 3 bo hen gio va xoa bo 2 giu 1 va 3
    Close Application

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================