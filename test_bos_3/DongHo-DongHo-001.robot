*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/dong_ho/dh_common.robot
Resource    ../page/dong_ho/cai_dat.robot
Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot

# Test Setup    Mo Dong ho
# Suite Teardown    Close Application

*** Test Cases ***
BP-424
    [Documentation]    Check mo giao dien dong ho
    [Tags]    BP-424    BP-426    BP-428
    Log To Console    thuc hien mo app dong ho
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Nhan nut tab menu top dong ho
    Check hien thi man hinh dong ho thoi gian
    Sleep    2
    Nhan nut tab menu top hen gio
    Sleep    2
    Nhan nut tab menu top dong ho
    Check hien thi man hinh dong ho thoi gian
    Sleep    2
    Nhan nut tab menu top bam gio
    Sleep    2
    Nhan nut tab menu top dong ho
    Sleep    2
    Check hien thi man hinh dong ho thoi gian
    Sleep    2
    Close Application
BP-436
    [Documentation]    Check chuc nang mo danh sach quoc te
    [Tags]    BP-436    BP-438
    Log To Console    thuc hien mo giao dien gio quoc te
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Nhan nut tab menu top dong ho
    Sleep    2
    Nhan nut icon gio quoc te
    Sleep    2
    Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    Sleep    2
    Nhan nut icon back quay lai
    Sleep    2
    Check hien thi man hinh dong ho thoi gian
    Sleep    2
    Close Application
BP-440
    [Documentation]    Check mo o text box tim kiem
    [Tags]    BP-440    BP-442
    Log To Console    thuc hien mo giao dien quoc te
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Nhan nut tab menu top dong ho
    Sleep    2
    Nhan nut icon gio quoc te
    Sleep    2
    Nhan nut icon tim kiem
    Sleep    2
    Check kiem tra verify o tim kiem
    Sleep    2
    Nhan nut xoa text tim kiem
    Sleep    2
    Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    Sleep    5
    Close Application
BP-443
    [Documentation]    Check chuc nang tim kiem
    [Tags]    BP-443    BP-445    BP-446    BP-448    BP-449    BP-450
    ...    DH_ĐH_080901    DH_ĐH_081001    DH_ĐH_081101
    Log To Console    thuc hien mo giao dien quoc te
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Nhan nut tab menu top dong ho
    Sleep    2
    Nhan nut icon gio quoc te
    Sleep    2
    Nhan nut icon tim kiem
    Sleep    2
    Thuc hien input text tim kiem khong co du lieu
    Sleep    2
    Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    Sleep    2
    Nhan nut xoa text tim kiem
    Sleep    2
    Thuc hien input text tim kiem
    Sleep    2
    Check hien thi ten cua cac nuoc
    Sleep    2
    Nhan nut xoa text tim kiem
    Sleep    2
    Thuc hien input text tim kiem ha noi khong dau
    Sleep    2
    Check hien thi ten cua cac nuoc
    Sleep    2
    Nhan nut xoa text tim kiem
    Sleep    2
    Thuc hien input text tim kiem chu in hoa
    Sleep    2
    Check hien thi ten cua cac nuoc
    Sleep    2
    Nhan nut xoa text tim kiem
    Sleep    2
    Thuc hien input text tim kiem chu thuong co dau
    Sleep    2
    Check hien thi ten cua cac nuoc
    Sleep    2
    Nhan nut xoa text tim kiem
    Sleep    2
    Copy text Paste noi dung da copy
    Sleep    2
    Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    Sleep    2
    Nhan nut xoa text tim kiem
    Sleep    2
    Close Application

BP-646
    [Documentation]    check chuc nang tich chon thanh pho
    [Tags]    BP-646     BP-647    BP-648     BP-650    BP-651
    Log To Console    check chuc nang tich chon thanh pho
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Sleep    2
    Nhan nut tab menu top dong ho
    Sleep    2
    Nhan nut icon gio quoc te
    Sleep    2
    Nhan o checkbox tich chon hien thi
    Sleep    5
    AppiumLibrary.Click Element At Coordinates        577    1696
    Sleep    5
    Nhan nut icon back quay lai
    Sleep    2
    Check dem phan tu hien thi danh sach gio quoc te da duoc tich chon
    Sleep    10
    Nhan nut icon gio quoc te
    Sleep    2
    AppiumLibrary.Click Element At Coordinates        227    503
    Sleep    2
    Nhan nut icon back quay lai
    Sleep    2
    Check dem phan tu hien thi danh sach gio quoc te da duoc tich chon
    Sleep    10
    Nhan nut icon gio quoc te
    Sleep    2
    Ckeck vuot launcher len tren
    Sleep    2
    Ckeck vuot launcher xuong
    Sleep    2
    Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    Sleep    2
    Nhan nut icon back quay lai
    Sleep    2
    Nhan cham va giu vao thanh pho da them tren giao dien dong ho
    Sleep    2
    Check hien thi man hinh dong ho thoi gian
    Sleep    2
    AppiumLibrary.Go Back
    Sleep    5
    Close Application

BP-455
    [Documentation]    check chuc nang tuy chon chuyen thoi gian sang ten
    [Tags]    BP-455    BP-456    BP-459    BP-461
    Log To Console    kiem tra hoat dong chuc nang tuy chon
    Mo Dong ho
    Sleep    3
    Cho phep cap quyen cho ung dung
    Sleep    2
    Nhan nut tab menu top dong ho
    Sleep    2
    Nhan nut icon gio quoc te
    Sleep    2
    Nhan nut tab menu top tuy chon
    Sleep    2
    Nhan nut chuyen danh sach gio quoc te sang dinh dang khac
    Sleep    2
    Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    Nhan nut tab menu top tuy chon
    Sleep    2
    Nhan nut chuyen danh sach gio quoc te sang dinh dang khac
    Sleep    2
    Check hien thi chu cai ten cua cac nuoc quoc te
    Sleep    2
    Go Back
    Cham vao dau ba cham
    sleep    1
    Nhan vao nut cai dat
    sleep    1
    AppiumLibrary.Page Should Contain Element    ${dh_hien_thi_kieu_dong_ho}
    Close Application


#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
