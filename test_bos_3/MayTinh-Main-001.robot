*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library    AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/may_tinh/mt_common.robot
Resource    ../page/may_tinh/mt_myanmar.robot
Resource    ../page/may_tinh/lich_su.robot

Suite Setup    Mo Launcher

*** Test Cases ***
# -------------------------------------------------------------
MT_LS_010101
    Log To Console     -----------------Man hinh lic su-----------------
    [Documentation]    Check mo lich su bang thao tac cham vao lich su
    [Tags]    MayTinh    LichSu    Myanmar    MT_LS_010101
    Log To Console     Check mo lich su bang thao tac cham vao lich su
    Mo May tinh
    Sleep    3
    Bam vao nut lich su
    Sleep    2
    Page Should Contain Element    ${mt_txt_khong_co_lich_su}

MT_LS_120101
    [Documentation]    Check dong lich su bang thao tac cham vao lich su
    [Tags]    MT_LS_120101
    Log To Console     Check dong lich su bang thao tac cham vao lich su
    Bam vao nut lich su
    Sleep    2
    Page Should Not Contain Text    ${mt_txt_khong_co_lich_su}

MT_LS_010201
    [Documentation]    Check mo lich su bang thao tac vuot
    [Tags]    MT_LS_010201
    Log To Console     Check mo lich su bang thao tac vuot
    Vuot mo giao dien lich su
    Sleep    2
    Page Should Contain Element    ${mt_txt_khong_co_lich_su}
    sleep    2

MT_LS_120201
    [Documentation]    Check dong lich su bang thao tac vuot
    [Tags]    MT_LS_120201
    Log To Console     Check dong lich su bang thao tac vuot
    Dong lich su bang thao tac vuot
    Page Should Not Contain Text    ${mt_txt_khong_co_lich_su}

MT_LS_100101
    [Documentation]    Check chuc nang xoa lich su
    [Tags]    MT_LS_100101
    Thuc hien phep tinh
    Bam vao nut lich su
    Log To Console     Check chuc nang xoa lich su
    Xoa lich su
    AppiumLibrary.Page Should Not Contain Element    ${mt_nut_xoa_lich_su}
    Page Should Contain Element    ${mt_txt_khong_co_lich_su}

MT_LS_110101
    [Documentation]    Check chon mot ket qua trong lich su
    [Tags]    MT_LS_110101    MT_LS_110201    MT_LS_110203
    Dong lich su bang thao tac vuot
    Thuc hien phep tinh
    Log To Console    Check chon mot ket qua trong lich su
    Bam vao nut lich su
    Bam ket qua trong lich su
    Bam ket qua trong lich su
    Bam vao nut lich su
    ${get_text_expected}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    444
    Log To Console    Check thuc hien phep tinh voi ket qua trong lich su
    An nut dau cong (+)
    An nut 123
    An vao bang
    ${get_text_expected}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    567
    Close Application

MT_Main_010101
    [Documentation]    Check mo May tinh tu Launcher
    [Tags]    MT_Main_010101
    Mo May tinh

MT_Main_040102
    [Documentation]    Check man hinh hien thi trong khi mo app lan dau
    [Tags]    MT_Main_040102
    Log To Console    Check man hinh hien thi trong khi mo app lan dau
    Sleep    2
    Element Should Contain Text    ${mt_hien_thi}           ${EMPTY}
    Element Should Contain Text    ${mt_hien_thi_phep_tinh}    ${EMPTY}
    Element Should Contain Text    ${mt_hien_thi_ket_qua}      ${EMPTY}
MT_Main_050102
    [Documentation]    Check noi dung text Lich su
    [Tags]    MT_Main_050102    MT_Main_100101
    Log To Console    Check noi dung text Lich su
    An nut lich su
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_nut_lich_su}
    Page Should Contain Element    ${mt_nut_lich_su}    text
MT_Main_070202
    [Documentation]    Check hien thi noi dung DEL
    [Tags]    MT_Main_070202    MT_Main_100101
    Log To Console    Check noi dung DEL
    An nut lich su
    ${get_texxt}=    AppiumLibrary.Get Text   ${mt_nut_xoa}
    Element Attribute Should Match    ${mt_nut_xoa}    text    *DEL*
MT_Main_070302
    [Documentation]    Check noi dung nut dau phay
    [Tags]    MT_Main_070302
    Log To Console    Check noi dung nut dau phay
    ${get_text}=     AppiumLibrary.Get Text     ${mt_nut_phay}

MT_Main_150101
    [Documentation]    Check chuc nang hien thi noi dung tu 1-7
    [Tags]    MT_Main_150101    MT_Main_150102
    Log To Console    Check chuc nang hien thi noi dung tu 1-7
    # Xoa man hinh
    An nut 1234567
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *1**234**567*

MT_Main_150103
    [Documentation]    Check chuc nang hien thi kich thuoc khi nhap 8 so tro len
    [Tags]    MT_Main_150103    MT_Main_180101    MT_Main_180201    MT_Main_180301    MT_Main_180401    MT_Main_180501    MT_Main_180601
    ...    MT_Main_180701    MT_Main_180801    MT_Main_180901    MT_Main_181001
    Log To Console    Check chuc nang hien thi kich thuoc khi nhap 8 so tro len
    Xoa man hinh
    Sleep    2
    An nut 12345678901234556
    Sleep    2
    ${get_text}=   AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *12**345**678**901**234**556*

MT_Main_150201
    [Documentation]    Check chuc nang hien thi du lieu loi
    [Tags]    MT_Main_150201
    Log To Console    Check chuc nang hien thi du lieu loi
    Xoa man hinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    1
    An nut ham In trong nang cao
    Sleep    1
    An nut man hinh nang cao
    An nut so 0
    Sleep    1
    An nut man hinh nang cao
    Sleep    1
    Thuc hien click ngoac phai
    Sleep    1
    An nut man hinh nang cao
    An nut dau (=)
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    −∞
    Sleep    2
    An nut Clear xoa ket qua

MT_Main_160101
    [Documentation]    Check chuc nang khi cham vao lich su
    [Tags]    MT_Main_160101
    Log To Console    Check chuc nang khi cham vao lich su
    An nut lich su
    AppiumLibrary.Element Should Be Enabled    ${mt_nut_lich_su}
    An nut lich su

MT_Main_170101
    [Documentation]    Check chuc nang khi cham vao nang cao
    [Tags]    MT_Main_170101
    Log To Console    Check chuc nang khi cham vao nang cao
    An nut man hinh nang cao
    AppiumLibrary.Element Should Be Enabled    ${mt_nut_nang_cao}
    Sleep    2
    An nut man hinh nang cao

MT_Main_190101
    [Documentation]    Check nut dau phay thap phan
    [Tags]    MT_Main_190101
    Log To Console    Check nut dau phay thap phan
    An nut phay thap phan
    AppiumLibrary.Element Should Be Enabled    ${mt_nut_phay}
    ${get_text}=    AppiumLibrary.Get Text   ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    ${EMPTY}
    Close Application
MT_Main_200101
    [Documentation]    Check nut dau chia (:) khi da nhap so
    [Tags]    MT_Main_200101     MT_Main_200201
    Log To Console    Check nut dau (:) khi khong nhap so
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut dau chia (÷)
    Page Should Contain Element    ${mt_nut_chia}
    Sleep    2
    Log To Console    Check nut dau (:) khi co nhap so
    An nut 1234567
    Sleep    2
    An nut dau chia (÷)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *1**234**567÷*

MT_Main_200301
    [Documentation]    Check nhap dau tru (-) va dau chia (:)
    [Tags]    MT_Main_200301
    Log To Console    Check nhap dau (-) va dau chia (:)
    Sleep    2
    Xoa man hinh
    Sleep    2
    An nut dau tru (-)
    Sleep    2
    An nut dau chia (÷)
    Log To Console    ket qua mong muon hien thi dau -
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}     −

MT_Main_210101
    [Documentation]    Check nhap dau nhan (x)
    [Tags]    MT_Main_210101    MT_Main_210301
    Log To Console    Check nhap dau nhan (x) chua nhap so
    Xoa man hinh
    Sleep    2
    An nut dau nhan (x)
    AppiumLibrary.Page Should Contain Element    ${mt_nut_nhan}
    Sleep    2
    An nut dau tru (-)
    An nut dau nhan (x)
    Log To Console    ket qua mong muon hien thi dau -
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}     −

MT_Main_210201
    [Documentation]    Check nut dau nhan (x) khi da nhap so
    [Tags]    MT_Main_210201
    Log To Console    Check nut dau nhan (x) khi da nhap so
    Xoa man hinh
    Sleep    2
    An nut 1234567
    Sleep    2
    An nut dau nhan (x)
    Log To Console    ket qua mong muon hien thi dau chia truoc so da nhap
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *1**234**567×*
    Close Application
MT_Main_220101
    [Documentation]    Check nhap dau nhan (+)
    [Tags]    MT_Main_220101    MT_Main_220301
    Log To Console    Check nhap dau nhan (+) khi chua nhap so
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut dau cong (+)
    Page Should Contain Element    ${mt_nut_cong}
    Sleep    2
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}     −

MT_Main_220201
    [Documentation]    Check nhap dau cong (+)
    [Tags]    MT_Main_220201
    Log To Console    Check nut dau cong (+) khi da nhap so
    Sleep    2
    Xoa man hinh
    Sleep    2
    An nut 1234567
    An nut dau cong (+)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *1**234**567+*

MT_Main_230101
    [Documentation]    Check nhap dau tru (−)
    [Tags]    MT_Main_230101    MT_Main_230301
    Log To Console    Check nhap dau tru (-) khi khong nhap so
    Sleep    2
    Xoa man hinh
    Sleep    2
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    −

MT_Main_230201
    [Documentation]    Check nhap dau tru (−)
    [Tags]    MT_Main_230201
    Log To Console    Check nut dau tru (−) khi da nhap so
    Sleep    2
    Xoa man hinh
    Sleep    2
    An nut 1234567
    Sleep    2
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *1**234**567−*

MT_Main_240101
    [Documentation]    Check nhan nut bang (=) khi chua nhap phep tinh
    [Tags]    MT_Main_240101
    Log To Console    Check nhan nut bang (=) khi chua nhap phep tinh
    Xoa man hinh
    Sleep    2
    An nut dau (=)
    Sleep    2
    AppiumLibrary.Page Should Contain Element    ${mt_nut_bang}

MT_Main_240201
    [Documentation]    Check nhan nut bang (=) khi da nhap phep tinh
    [Tags]    MT_Main_240201    MT_Main_250101    MT_Main_250102    MT_Main_250103    MT_Main_260101
    Log To Console    Check chuc nang xoa 1 so
    Sleep    2
    Xoa man hinh
    Sleep    2
    An nut 123
    Sleep    2
    Click xoa so
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    12
    Log To Console    Check chuc nang xoa 2 so
    Click xoa so
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    1
    Log To Console    Check chuc nang DEL
    Xoa man hinh
    Log To Console    Check nhan nut dau (=) khi thuc hien phep tinh
    An nut 123
    An nut dau cong (+)
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    579
    Log To Console    Check chuc nang CLR
    An nut Clear xoa ket qua
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    ${EMPTY}

MT_Main_270101
    [Documentation]    Check chuc nang doi vi tri con tro chuot xoa so va them so
    [Tags]    MT_Main_270101    MT_Main_270102
    Log To Console    Check chuc nang doi vi tri con tro chuot xoa so va them so
    AppiumLibrary.Click Element    ${nut_1}
    AppiumLibrary.Click Element    ${nut_2}
    AppiumLibrary.Click Element    ${nut_3}
    AppiumLibrary.Click Element    ${nut_4}
    AppiumLibrary.Click Element    ${nut_5}
    AppiumLibrary.Click Element    ${nut_6}
    AppiumLibrary.Click Element    ${nut_7}
    AppiumLibrary.Click Element    ${nut_8}
    AppiumLibrary.Click Element    ${nut_9}
    ${get_text1}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    # AppiumLibrary.Click Element At Coordinates    28    313
    AppiumLibrary.Click Element At Coordinates    ${space_dau_toa_do_x}    ${space_dau_toa_do_y}
    AppiumLibrary.Click Element    ${nut_9}
    ${get_text2}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match   ${mt_hien_thi_phep_tinh}    text    *9**123**456**789*
    AppiumLibrary.Click Element At Coordinates    ${space_cuoi_toa_do_x}    ${space_cuoi_toa_do_y}
    Click xoa so
    ${get_text3}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match   ${mt_hien_thi_phep_tinh}    text    912**345**678*

MT_Main_280101
    [Documentation]    kiem tra phep tinh cong (+) giua cac so nguyen
    [Tags]    MT_Main_280101
    Log To Console    kiem tra phep tinh cong (+) giua cac so nguyen
    Xoa man hinh
    An nut 123
    An nut dau cong (+)
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    579
    Element Attribute Should Match    ${mt_nut_clr}    text    CLR
    Sleep    2

MT_Main_280102
    [Documentation]    kiem tra phep tinh cong (+) giua so am (-) va cac so nguyen
    [Tags]    MT_Main_280102
    Log To Console    kiem tra phep tinh cong (+) giua so am va cac so nguyen
    An nut Clear xoa ket qua
    An nut dau tru (-)
    An nut 123
    An nut dau cong (+)
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    333
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

MT_Main_280103
    [Documentation]    kiem tra phep tinh cong (+) giua cac so thap phan
    [Tags]    MT_Main_280103
    Log To Console    kiem tra phep tinh cong (+) giua cac so thap phan
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau cong (+)
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *246**912*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    5

MT_Main_280104
    [Documentation]    kiem tra phep tinh cong (+) giua cac so thap phan va cac so nguyen
    [Tags]    MT_Main_280104
    Log To Console    kiem tra phep tinh cong (+) giua cac so thap phan va cac so nguyen
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau cong (+)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *246**456*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

MT_Main_280105
    [Documentation]    kiem tra phep tinh cong (+) giua cac so thap phan va cac so nguyen
    [Tags]    MT_Main_280105
    Log To Console    kiem tra phep tinh cong (+) giua cac so thap phan va cac so nguyen
    An nut Clear xoa ket qua
    An nut 12345678901234556
    An nut dau cong (+)
    An nut 12345678901234556
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *2**469135*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

MT_Main_280201
    [Documentation]    kiem tra phep tinh tru (-) giua cac so nguyen
    [Tags]    MT_Main_280201
    Log To Console    kiem tra phep tinh tru (-) giua cac so nguyen
    An nut Clear xoa ket qua
    An nut 456
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *333*
    Should Be Equal    ${get_text}    333
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

MT_Main_280202
    [Documentation]    kiem tra phep tinh tru (-) giua so am (-) va cac so nguyen
    [Tags]    MT_Main_280202
    Log To Console    kiem tra phep tinh tru (-) giua so am va cac so nguyen
    An nut Clear xoa ket qua
    An nut dau tru (-)
    An nut 123
    An nut dau tru (-)
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    −579
    Should Be Equal    ${get_text}    −579
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2
MT_Main_280203
    [Documentation]    kiem tra phep tinh tru (-) giua cac so thap phan
    [Tags]    MT_Main_280203
    Log To Console    kiem tra phep tinh tru (-) giua cac so thap phan
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau tru (-)
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    0
    Should Be Equal    ${get_text}    0
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2
MT_Main_280204
    [Documentation]    kiem tra phep tinh tru (-) giua cac so thap phan va cac so nguyen
    [Tags]    MT_Main_280204
    Log To Console    kiem tra phep tinh tru (-) giua cac so thap phan va cac so nguyen
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *0**456*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2
MT_Main_280301
    [Documentation]    kiem tra phep tinh nhan (x) giua cac so nguyen
    [Tags]    MT_Main_280301
    Log To Console    kiem tra phep tinh nhan (x) giua cac so nguyen
    An nut Clear xoa ket qua
    An nut 456
    An nut dau nhan (x)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *56**088*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

MT_Main_280302
    [Documentation]    kiem tra phep tinh nhan (x) giua so am (-) va cac so nguyen
    [Tags]    MT_Main_280302
    Log To Console    kiem tra phep tinh nhan (x) giua so am va cac so nguyen
    An nut Clear xoa ket qua
    An nut dau tru (-)
    An nut 123
    An nut dau nhan (x)
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *−56**088*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

MT_Main_280303
    [Documentation]    kiem tra phep tinh nhan (x) giua cac so thap phan
    [Tags]    MT_Main_280303
    Log To Console    kiem tra phep tinh nhan (x) giua cac so thap phan
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau nhan (x)
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *15**241**38*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

MT_Main_280304
    [Documentation]    kiem tra phep tinh tru (-) giua cac so thap phan va cac so nguyen
    [Tags]    MT_Main_280304
    Log To Console    kiem tra phep tinh tru (-) giua cac so thap phan va cac so nguyen
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau nhan (x)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *15**185**088*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2
MT_Main_280305

    [Documentation]    kiem tra phep tinh nhan (x) giua cac so thap phan va cac so nguyen
    [Tags]    MT_Main_280305
    Log To Console    kiem tra phep tinh nhan (x) giua cac so thap phan va cac so nguyen
    An nut Clear xoa ket qua
    An nut 12345678901234556
    An nut dau nhan (x)
    An nut 12345678901234556
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *1**524157*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

MT_Main_280401
    [Documentation]    kiem tra phep tinh chia (:) giua cac so nguyen
    [Tags]    MT_Main_280401
    Log To Console    kiem tra phep tinh chia (:) giua cac so nguyen
    An nut Clear xoa ket qua
    An nut 456
    An nut dau chia (÷)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *3**707317*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

MT_Main_280402
    [Documentation]    kiem tra phep tinh chia (:) giua so am (-) va cac so nguyen
    [Tags]    MT_Main_280402
    Log To Console    kiem tra phep tinh chia (:) giua so am va cac so nguyen
    An nut Clear xoa ket qua
    An nut dau tru (-)
    An nut 123
    An nut dau chia (÷)
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *−0**26973*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

MT_Main_280403
    [Documentation]    kiem tra phep tinh chia (:) giua cac so thap phan
    [Tags]    MT_Main_280403
    Log To Console    kiem tra phep tinh chia (:) giua cac so thap phan
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau chia (÷)
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    1
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

MT_Main_280404
    [Documentation]    kiem tra phep tinh chia (:) giua cac so thap phan va cac so nguyen
    [Tags]    MT_Main_280404
    Log To Console    kiem tra phep tinh chia (:) giua cac so thap phan va cac so nguyen
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau chia (÷)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *1**003707*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

MT_Main_290101
    [Documentation]    kiem tra phep tinh nhan (x) voi 2 loai phep tinh
    [Tags]    MT_Main_290101
    Log To Console    kiem tra phep tinh chia (x) voi 2 loai phep tinh
    An nut Clear xoa ket qua
    An nut 123
    An nut dau chia (÷)
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    123÷−

MT_Main_290102
    [Documentation]    kiem tra phep tinh chia (:) voi 2 loai phep tinh
    [Tags]    MT_Main_290102    MT_Main_290103
    Log To Console    kiem tra phep tinh chia (:) voi 2 loai phep tinh
    Xoa man hinh
    An nut 123
    An nut dau chia (÷)
    An nut dau cong (+)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    123+
    An nut dau nhan (x)
    ${get_textt}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_textt}    123×

MT_Main_290201
    [Documentation]    kiem tra phep tinh nhan (x) voi 2 loai phep tinh
    [Tags]    MT_Main_290201    MT_Main_290202
    Log To Console    kiem tra phep tinh chia (x) voi 2 loai phep tinh
    Xoa man hinh
    An nut 123
    An nut dau nhan (x)
    An nut dau cong (+)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    123+
    An nut dau chia (÷)
    ${get_textt}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_textt}    123÷

MT_Main_290203
    [Documentation]    kiem tra phep tinh nhan (x) voi 2 loai phep tinh
    [Tags]    MT_Main_290203
    Log To Console    kiem tra phep tinh chia (x) voi 2 loai phep tinh
    Xoa man hinh
    An nut 123
    An nut dau nhan (x)
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    123×−

MT_Main_290301
    [Documentation]    kiem tra phep tinh cong (+) voi 2 loai phep tinh
    [Tags]    MT_Main_290301    MT_Main_290302    MT_Main_290303
    Log To Console    kiem tra phep tinh cong (+) voi 2 loai phep tinh
    Xoa man hinh
    An nut 123
    An nut dau cong (+)
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    123+−
    An nut dau nhan (x)
    ${get_text1}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text1}    123×
    An nut dau chia (÷)
    ${get_text2}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text2}    123÷
MT_Main_290401
    [Documentation]    kiem tra phep tinh tru (-) voi 2 loai phep tinh
    [Tags]    MT_Main_290401    MT_Main_290402    MT_Main_290403
    Log To Console    kiem tra phep tinh tru (-) voi 2 loai phep tinh
    Xoa man hinh
    An nut 123
    An nut dau chia (÷)
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    123÷−
    An nut dau nhan (x)
    ${get_text1}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text1}    123×
    An nut dau cong (+)
    ${get_text2}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text2}    123+
MT_Main_300102
    [Documentation]    kiem tra nhap dau phay (,) vao giua cac so
    [Tags]    MT_Main_300102
    Log To Console    kiem tra nhap dau phay (,) vao giua cac so
    Xoa man hinh
    An nut 123
    An nut phay thap phan
    An nut 456
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *123**456*

MT_Main_310101
    [Documentation]    Kiem tra them so vao sau ket qua phep tinh da thuc hien
    [Tags]    MT_Main_310101
    Log To Console    Kiem tra them so vao sau ket qua phep tinh da thuc hien
    Xoa man hinh
    An nut 12345
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    Thuc hien nhap so
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    45

MT_Main_310201
    [Documentation]    Kiem tra them nut phep tinh cong vao sau ket qua phep tinh da thuc hien
    [Tags]    MT_Main_310101
    Log To Console    Kiem tra them nut phep tinh cong vao sau ket qua phep tinh da thuc hien
    Xoa man hinh
    An nut 12345
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    An nut dau cong (+)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    12**222+

MT_Main_310301
    [Documentation]    Kiem tra them nut phep tru vao sau ket qua phep tinh da thuc hien
    [Tags]    MT_Main_310301
    Log To Console    Kiem tra them nut phep tru vao sau ket qua phep tinh da thuc hien
    Xoa man hinh
    An nut 12345
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    12**222−

MT_Main_310401
    [Documentation]    Kiem tra them nut phep nhan vao sau ket qua phep tinh da thuc hien
    [Tags]    MT_Main_310301
    Log To Console    Kiem tra them nut phep nhan vao sau ket qua phep tinh da thuc hien
    Xoa man hinh
    An nut 12345
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    An nut dau nhan (x)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    12**222×

MT_Main_310501
    [Documentation]    Kiem tra them nut phep chia vao sau ket qua phep tinh da thuc hien
    [Tags]    MT_Main_310301
    Log To Console    Kiem tra them nut phep chia vao sau ket qua phep tinh da thuc hien
    Xoa man hinh
    An nut 12345
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    An nut dau chia (÷)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    12**222÷
MT_Main_300101
    [Documentation]    Kiem tra hien thi so 0 dang truoc dau phay
    [Tags]    MT_Main_310301
    Log To Console    Kiem tra hien thi so 0 dang truoc dau phay
    Xoa man hinh
    AppiumLibrary.Click Element    ${mt_nut_phay}
    ${get_text}=    Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *0*

MT_Main_150301
    [Documentation]    Check chuc nang luu phep tinh gan nhat
    [Tags]    MT_Main_150301    MT_Main_150302    MT_Main_150303
    Log To Console    Check chuc nang hien thi phep tinh gan nhat
    Xoa man hinh
    Thuc hien nhap phep tinh
    Sleep    2
    Thuc hien phep tinh kiem tra hien thi ket qua
    Sleep    2
    Close Application
MT_NC10101
    Log To Console    ---------------Man hinh may tinh nang cao---------------
    [Documentation]    Check mo May tinh tu Launcher
    [Tags]    MT_NC_010101    MT_NC_100901
    Log To Console    Check man hinh hien thi trong khi mo app lan dau
    Mo May tinh
    Log To Console    Check chuc nang An nut luy thua (^)
    An nut man hinh nang cao
    Sleep    2
    AppiumLibrary.Element Should Be Enabled    ${mt_nut_luy_thua}
    An nut luy thua
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Xoa man hinh
    Sleep    2

MT_NC_101001
    [Documentation]    Check chuc nang An nut dau ngoac trai
    [Tags]    MT_NC_101001
    Log To Console    Check chuc nang An nut dau ngoac trai
    An nut man hinh nang cao
    Sleep    2
    An nut dau ngoac trai
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    (
    Sleep    2
    Close Application
MT_NC_101101
    [Documentation]    Check chuc nang An nut dau ngoac phai
    [Tags]    MT_NC_101101    MT_NC_101201
    Log To Console    Check chuc nang An nut dau ngoac phai
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    An nut dau ngoac phai
    AppiumLibrary.Element Should Be Enabled    ${mt_nut_ngoac_phai}
    ${get_text1}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal   ${get_text1}    )
    Sleep    2
    An nut khai can
    AppiumLibrary.Element Should Be Enabled    ${mt_nut_can_bac_2}
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal   ${get_text}    )√
    Sleep    2
    Close Application
    Sleep    2
MT_NC_101202
    [Documentation]    Check chuc nang hoat dong cua khai can
    [Tags]    MT_NC_101202
    Log To Console    Check chuc nang hoat dong cua khai can
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut khai can
    Sleep    2
    An nut man hinh nang cao
    An nut so 4
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Should Be Equal   ${get_text}    2
    Sleep    2
    Close Application
MT_NC_101301
    [Documentation]    Check chuc nang dau %
    [Tags]    MT_NC_101301    MT_NC_101302
    Log To Console    Check chuc nang dau % - chua nhap so
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut phan tram
    ${get_text1}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *%*
    Sleep    2
    Xoa man hinh
    Sleep    2
    An nut so 2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut phan tram
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0**02*
    Sleep    2
    Close Application
MT_NC_101401
    [Documentation]    Check phep tinh m(-)
    [Tags]    MT_NC_101401
    Log To Console    Check phep tinh m(-)

    Mo May tinh
    Sleep    2
    An phep tinh nhan 2x2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut m tru
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An phep tinh nhan 3x3
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut m tru
    Sleep    2
    An nut mr
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    −13
    Sleep    2
    Close Application
MT_NC_101501
    [Documentation]    Check phep tinh m(+)
    [Tags]    MT_NC_101501
    Log To Console    Check phep tinh m(+)
    Mo May tinh
    Sleep    2
    An phep tinh nhan 2x2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut m cong
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An phep tinh nhan 3x3
    An nut man hinh nang cao
    Sleep    2
    An nut m cong
    An nut mr
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    13
    Sleep    2
    Close Application
MT_NC_101602
    [Documentation]    Check hien thi ket qua phep tinh m- dang luu trong bo nho
    [Tags]    MT_NC_101602
    Log To Console    Check hien thi ket qua phep tinh m- dang luu trong bo nho
    Mo May tinh
    Sleep    2
    An phep tinh nhan 2x2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut m tru
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut Clear xoa ket qua
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut mr
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    −4
    Sleep    2
    Close Application
 MT_NC_101701
    [Documentation]    Check xoa ket qua phep tinh m+ dang luu trong bo nho bang mc
    [Tags]    MT_NC_101701
    Log To Console    Check xoa ket qua phep tinh m+ dang luu trong bo nho bang mc
    Mo May tinh
    Sleep    2
    An phep tinh nhan 2x2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut mc
    Sleep    2
    An nut m cong
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut Clear xoa ket qua
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut mc
    Sleep    2
    An nut mr
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    ${EMPTY}
    Sleep    2
    Close Application
 MT_NC_101702
    [Documentation]    Check xoa ket qua phep tinh m- dang luu trong bo nho bang mc
    [Tags]    MT_NC_101602
    Log To Console    Check xoa ket qua phep tinh m- dang luu trong bo nho bang mc
    Mo May tinh
    Sleep    2
    An phep tinh nhan 2x2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut m tru
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut Clear xoa ket qua
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut mc
    Sleep    2
    An nut mr
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    ${EMPTY}
    Sleep    2
    Close Application

MT_NC_110101
    [Documentation]    Check chuc nang DEG voi phep tinh cong ki hieu sin
    [Tags]    MT_NC_110101
    Log To Console    Check chuc nang DEG voi phep tinh cong ki hieu sin
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh cong sin
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *0**824236970*
    Sleep    2
    Close Application

MT_NC_110103
    [Documentation]    Check chuc nang DEG voi phep tinh cong ki hieu cos
    [Tags]    MT_NC_110103
    Log To Console     Check chuc nang DEG voi phep tinh cong ki hieu cos
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh cong cos
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *−1**822260523*
    Sleep    2
    Close Application

MT_NC_110105
    [Documentation]    Check chuc nang DEG voi phep tinh cong ki hieu tan
    [Tags]    MT_NC_110105
    Log To Console    Check chuc nang DEG voi phep tinh cong ki hieu tan
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Thuc hien va kiem tra phep tinh cong tan
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *−0**90463131*
    Sleep    2
    Close Application

MT_NC_110107
    [Documentation]    Check phep tinh cong ki hieu ln
    [Tags]    MT_NC_110107
    Log To Console    Check phep tinh cong ki hieu ln
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh cong ln
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *4**39444*
    Sleep    2
    Close Application
MT_NC_110108
    [Documentation]    Check phep tinh cong ki hieu log
    [Tags]    MT_NC_110108
    Log To Console    Check phep tinh cong ki hieu log
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh cong log
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *1**90848*
    Sleep    2
    Close Application
MT_NC_110201
    [Documentation]    Check chuc nang DEG voi phep tinh tru ki hieu sin
    [Tags]    MT_NC_110201
    Log To Console    Check chuc nang DEG voi phep tinh tru ki hieu sin
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh tru sin
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    0
    Sleep    2
    Close Application

MT_NC_110203
    [Documentation]    Check chuc nang DEG voi phep tinh tru ki hieu cos
    [Tags]    MT_NC_110203
    Log To Console     Check chuc nang DEG voi phep tinh tru ki hieu cos
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh tru cos
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    0
    Sleep    2
    Close Application
MT_NC_110204
    [Documentation]    Check chuc nang RAD voi phep tinh tru ki hieu cos
    [Tags]    MT_NC_110204
    Log To Console    Check chuc nang RAD voi phep tinh tru ki hieu cos
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh tru cos
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    0
    Sleep    2
    Close Application
MT_NC_110205
    [Documentation]    Check chuc nang DEG voi phep tinh tru ki hieu tan
    [Tags]    MT_NC_110205
    Log To Console    Check chuc nang DEG voi phep tinh tru ki hieu tan
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh tru tan
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text   ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    0
    Sleep    2
    Close Application

MT_NC_110207
    [Documentation]    Thuc hien va kiem tra phep tinh tru ln
    [Tags]    MT_NC_110207
    Log To Console    Thuc hien va kiem tra phep tinh tru ln
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh tru ln
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *0*
    Sleep    2
    Close Application

MT_NC_110208
    [Documentation]    Check phep tinh tru ki hieu log
    [Tags]    MT_NC_110208
    Log To Console    Check phep tinh tru ki hieu log
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh tru log
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    0
    Sleep    2
    Close Application
MT_NC_110301
    [Documentation]    Check chuc nang DEG voi phep tinh nhan ki hieu sin
    [Tags]    MT_NC_110301
    Log To Console    Check chuc nang DEG voi phep tinh nhan ki hieu sin
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh nhan sin
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *0**16984164*
    Sleep    2
    Close Application
MT_NC_110303
    [Documentation]    Check chuc nang DEG voi phep tinh nhan ki hieu cos
    [Tags]    MT_NC_110303
    Log To Console     Check chuc nang DEG voi phep tinh nhan ki hieu cos
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh nhan cos
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *0**8301583541*
    Sleep    2
    Close Application

MT_NC_110305
    [Documentation]    Check chuc nang DEG voi phep tinh nhan ki hieu tan
    [Tags]    MT_NC_110305
    Log To Console    Check chuc nang DEG voi phep tinh nhan ki hieu tan
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh nhan tan
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *0**20458945*
    Sleep    2
    Close Application
MT_NC_110307
    [Documentation]    Check phep tinh nhan ki hieu ln
    [Tags]    MT_NC_110307
    Log To Console    Check phep tinh nhan ki hieu ln
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh nhan ln
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text   *4**82779584*
    Sleep    2
    Close Application
MT_NC_110308
    [Documentation]    Check phep tinh nhan ki hieu log
    [Tags]    MT_NC_110308
    Log To Console    Check phep tinh nhan ki hieu log
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh nhan log
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text   *0**910578766*
    Sleep    2
    Close Application
MT_NC_110401
    [Documentation]    Check chuc nang DEG voi phep tinh chia ki hieu sin
    [Tags]    MT_NC_110401
    Log To Console    Check chuc nang DEG voi phep tinh chia ki hieu sin
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh chia sin
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    1
    Sleep    2
    Close Application

MT_NC_110403
    [Documentation]    Check chuc nang DEG voi phep tinh chia ki hieu cos
    [Tags]    MT_NC_110403
    Log To Console     Check chuc nang DEG voi phep tinh chia ki hieu cos
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh chia cos
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    1
    Sleep    2
    Close Application

MT_NC_110405
    [Documentation]    Check chuc nang DEG voi phep tinh chia ki hieu tan
    [Tags]    MT_NC_110405
    Log To Console    Check chuc nang DEG voi phep tinh chia ki hieu tan
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh chia tan
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    1
    Sleep    2
    Close Application

MT_NC_110407
    [Documentation]    Check phep tinh chia ki hieu ln
    [Tags]    MT_NC_110407
    Log To Console    Check phep tinh chia ki hieu ln
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh chia ln
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    1
    Sleep    2
    Close Application
MT_NC_110408
    [Documentation]    Check phep tinh chia ki hieu log
    [Tags]    MT_NC_110408
    Log To Console    Check phep tinh chia ki hieu log
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh chia log
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    1
    Sleep    2
    Close Application
 MT_NC_101601
    [Documentation]    Check hien thi phep tinh m(+) dang luu trong bo nho
    [Tags]    MT_NC_101601
    Log To Console    Check hien thi phep tinh m(+) dang luu trong bo nho
    Sleep    2
    Mo May tinh
    Sleep    2
    An phep tinh nhan 2x2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut m cong
    Sleep    5
    An nut man hinh nang cao
    Sleep    2
    An nut Clear xoa ket qua
    Sleep    2
    An nut man hinh nang cao
    An nut mr
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    4
    Close Application
