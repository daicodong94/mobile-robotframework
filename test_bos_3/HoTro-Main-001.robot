*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/ho_tro/ht_common.robot

Suite Setup       Mo Launcher
# Suite Teardown    Close All Applications

*** Test Cases ***
BP-5151
    [Documentation]    Check giao dien app ho tro tong the
    [Tags]    BP-5151    BP-5327    BP-5328    BP-5330    BP-5331    BP-5335    BP-5360    BP-5361
    Log To Console    Mo giao dien tu launcher
    Mo Ho tro tu Launcher
    Chup anh man hinh va so sanh voi anh mau    ${ht_alias}    kc_d_ttdh_d_d_Hotro_Main_Bos3    3
    Sleep    1
    Click 7 lan vao avatar dong
    Sleep    2
    Page Should Contain Element    ${ht_icon_tuy_chon_khac}
    Click vao tuy chon cai dat
    Log To Console    Check kiem tra giao dien man hinh tong the co nut tuy chon
    Chup anh man hinh va so sanh voi anh mau    ${ht_alias}    kc_d_ttdh_d_d_nut_TuyChon_Bos3    2
    Go Back
    Click vao tab icon goi dien
    Kiem tra hien thi so dien thoai ho tro
    Go Back
    Go Back
    Close Application

BP-5152
    [Documentation]    Kiem  tra link sang giao dien web bphone.vn/bphone-stores khi touch button Bstore
    [Tags]    BP-5152    BP-5153    BP-5154
    Mo Ho tro
    Click 7 lan vao avatar dong
    Click vao tuy chon cai dat
    Click vao tab xuat trang thai may
    Vuot mo bang thong bao
    Kiem tra hien thi text dang tao loi 1
    Sleep    2
    Vuot dong bang thong bao
    Click vao tuy chon cai dat
    Click vao tab xuat trang thai may
    Vuot mo bang thong bao
    Kiem tra hien thi text dang tao loi 2
    Sleep    2
    Vuot dong bang thong bao
    Close Application

BP-5359
    [Documentation]    kiem tra khong hien thi chuc nang tuy chon khi cham vao avatar co gai
    [Tags]    BP-5359    BP-5376    BP-5379
    Mo Ho tro
    Click 6 lan vao avatar check khong hien thi icon tuy chon
    Click vao tuy chon cai dat
    Kiem tra hien thi check box bao loi
    Click vao btn dung bao cao loi
    Click vao tuy chon cai dat
    Kiem tra khong hien thi check box bao loi
    Close Application

BP-5386
    [Documentation]    kiem tra khong bi crash khi cham lien tuc vao avatar
    [Tags]    BP-5386
    Mo Ho tro
    Click lien tuc vao avatar kiem tra crash
    Close Application

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================