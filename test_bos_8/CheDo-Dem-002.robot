*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/cai_dat/che_do_dem.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/cai_dat/cd_common.robot

Suite Setup    Mo Cai dat
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu
*** Test Cases ***
#dieu kien chua bat 24h
CDD_050201
    [Documentation]    Case Fake time chung
    [Tags]    CDD_050201
    Vuot sang man hinh ben duoi
    Vuot sang man hinh ben duoi
    Bam vao He thong
    Bam vao Ngay va gio
    Fake time chung
    
CDD_060201
    [Documentation]    Check hien thi popup dong ho khi cham Thoi gian bat dau/ket thuc
    [Tags]    CDD_060201
    ${kiem_tra}    Run Keyword And Return Status    Element Attribute Should Match    ${cd_su_dung_dinh_dang_24_gio}    checked    true 
    Run Keyword If   ${kiem_tra}==False     Click Element   ${cd_su_dung_dinh_dang_24_gio}            
    Go Back
    Go Back
    Vuot sang man hinh ben tren
    Vuot sang man hinh ben tren
    An vao Hien thi
    Nhap vao tab che do dem hien thi cai dat
    Nhan nut bat check list lich bieu
    Nhan nut bat che do thoi gian tuy chinh tai lich bieu
    Bam vao thoi gian bat dau
    Page Should Contain Element     ${cdd_dong_ho}
    Bam vao dong y
    
CDD_06030101
    [Documentation]    Check chuc nang chon thoi gian
    [Tags]     CDD_06030101    
    Bam vao thoi gian bat dau
    Bam chon 2gio15phut
    Element Attribute Should Match    ${cdd_hien_thi_gio}    text    2    
    Element Attribute Should Match    ${cdd_hien_thi_phut}    text    15 
    Bam vao huy
    
CDD_06030103
    [Documentation]    Check thay doi thoi gian
    [Tags]    CDD_06030103
    Bam vao thoi gian bat dau
    Bam chon 2gio15phut
    Bam vao dong y
    Element Attribute Should Match    ${cdd_thoi_gian_bat_dau}    text    2:15  

CDD_06030102
    [Documentation]    Check dong popup khi cham vao button huy/vuot/cham ben ngoai popup
    [Tags]    CDD_06030102
    Bam vao thoi gian bat dau
    Bam vao huy
    Element Attribute Should Match    ${cdd_thoi_gian_bat_dau}    text    2:15  
    Bam vao thoi gian bat dau
    Vuot back ve giao dien truoc
    Element Attribute Should Match    ${cdd_thoi_gian_bat_dau}    text    2:15  
    Bam vao thoi gian bat dau
    Click Element At Coordinates      ${cdd_ra_khoi_popup_dong_hoa_toa_do_x}    ${cdd_ra_khoi_popup_dong_hoa_toa_do_y}
    Element Attribute Should Match    ${cdd_thoi_gian_bat_dau}    text    2:15  
    
CDD_06030104 
    [Documentation]    Check chuyen sang giao dien dong ho so khi chon 
    [Tags]     CDD_06030104
    Bam vao thoi gian bat dau
    Cham vao icon ban phim
    Page Should Contain Text    Nhập thời gian  
    
CDD_06030201
    [Documentation]    Check chuc nang chon thoi gian  
    [Tags]             CDD_06030201 
    Page Should Contain Text    Nhập thời gian    
    Input gio tren dong ho so
    Input phut tren dong ho so
    Element Attribute Should Match    ${cdd_input_gio_dong_ho_so}    text    3    
    Element Attribute Should Match    ${cdd_input_phut_dong_ho_so}    text   20
    
CDD_06030202
    [Documentation]   Check dong popup khi cham button vuot back cham ben ngoai popup 
    [Tags]    CDD_06030202
    Vuot back ve giao dien truoc
    Element Attribute Should Match    ${cdd_thoi_gian_bat_dau}    text    2:15  
    Bam vao thoi gian bat dau
    Cham vao icon ban phim
    Input gio tren dong ho so
    Input phut tren dong ho so
    Bam vao huy 
    Element Attribute Should Match    ${cdd_thoi_gian_bat_dau}    text    2:15  
    Bam vao thoi gian bat dau
    Cham vao icon ban phim
    Input gio tren dong ho so
    Input phut tren dong ho so
    Click Element At Coordinates      ${cdd_ra_khoi_popup_dong_hoa_toa_do_x}    ${cdd_ra_khoi_popup_dong_hoa_toa_do_y}
    Element Attribute Should Match    ${cdd_thoi_gian_bat_dau}    text    2:15  

CDD_06030205
    [Documentation]    Check thay doi thoi gian khong thanh cong khi nhap khong hop le
    [Tags]    CDD_06030205    
    Bam vao thoi gian bat dau
    Cham vao icon ban phim
    Input gio khong hop le vao gio
    Bam vao dong y
    Page Should Contain Element    ${cdd_nhap_thoi_gian_khong_hop_le}    
    Bam vao huy
    Bam vao thoi gian bat dau
    Cham vao icon ban phim
    Input 23 vao gio 
    Input gio khong hop le vao phut
    Bam vao dong y
    Page Should Contain Element    ${cdd_nhap_thoi_gian_khong_hop_le}    
    Bam vao huy
    
CDD_06030206
    [Documentation]    Check nhap thoi gian thanh cong khi nhap vao cac ki tu hop le
    [Tags]    CDD_06030206
    Bam vao thoi gian bat dau
    Cham vao icon ban phim
    Input 23 vao gio
    Input 59 vao phut
    Bam vao dong y
    Element Attribute Should Match    ${cdd_thoi_gian_bat_dau}    text    23:59    
 
CDD_06030207
    [Documentation]    Check chuc nang chuyen sang giao dien dong ho
    [Tags]    CDD_06030207
    Bam vao thoi gian bat dau
    Cham vao icon ban phim
    Page Should Contain Element    ${cdd_nhap_thoi_gian}    
    Bam vao icon dong ho
    Page Should Contain Element    ${cdd_dong_ho}       
    Bam vao huy
    
CDD_060303
    [Documentation]    Check thay doi thoi gian khi bat/tat su dung dinh dang 24h
    [Tags]    CDD_060303
    Go Back
    Go Back
    Vuot sang man hinh ben duoi
    Vuot sang man hinh ben duoi
    Bam vao He thong
    Bam vao Ngay va gio
    ${kiem_tra}    Run Keyword And Return Status    Element Attribute Should Match    ${cd_su_dung_dinh_dang_24_gio}    checked    false 
    Run Keyword If   ${kiem_tra}==False     Click Element   ${cd_su_dung_dinh_dang_24_gio}            
    Go Back
    Go Back 
    Vuot sang man hinh ben tren
    Vuot sang man hinh ben tren
    An vao Hien thi
    Nhap vao tab che do dem hien thi cai dat
    Element Attribute Should Match    ${cdd_thoi_gian_bat_dau}   text    11:59 CH    

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================    
