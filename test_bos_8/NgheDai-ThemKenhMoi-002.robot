*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
ThinhLQ         | 2021/07/23 | update code , tach nho case rieng biet
----------------|------------|--------------------------------------------------------------------

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/nghe_dai/yeu_thich.robot

Suite Setup       Mo Nghe dai
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***

BP-2537
    [Documentation]    Check chuc nang nghe thu kenh khi khong co mang
    [Tags]    BP-2537
    Cho phep cap quyen cho ung dung
    Mang di dong = 1, data = 0, wifi = 0, may bay = 0
    Thuc hien click button them moi
    Cho phep cap quyen cho ung dung
    Thuc hien input link kenh moi
    Thuc hien click nghe thu
    Page Should Contain Element    ${nd_tm_trang_thai_nghe_thu}
    Page Should Not Contain Element    ${nut_dung_hoa}
    Go Back
    Go Back

BP-2542
    [Documentation]    Check input link kenh moi loi
    [Tags]    BP-2542
    Mang di dong = 1, data = 1, wifi = 1, may bay = 0
    Thuc hien click button them moi
    Cho phep cap quyen cho ung dung
    Thuc hien input link kenh moi loi
    Thuc hien click nghe thu
    Click vao ten kenh
    Wait Until Page Contains Element    ${nd_tm_trang_thai_mat_ket_noi}    timeout=50
    Page Should Not Contain Element    ${nut_dung_hoa}
    Page Should Not Contain Element    ${nd_tm_trang_thai_nghe_thu}
    Sleep    1
    Go Back
    Go Back
    Thuc hien click button them moi

BP-2544
    [Documentation]     Check input cau lenh SQL
    [Tags]    BP-2544
    Thuc hien input cau lenh SQL
    Thuc hien click nghe thu
    Click vao ten kenh
    Sleep    5
    Page Should Not Contain Element    ${nut_dung_hoa}
    Page Should Not Contain Element    ${nd_tm_trang_thai_mat_ket_noi}
    Page Should Contain Element    ${nd_tm_trang_thai_nghe_thu}
    Sleep    1
    Go Back
    Go Back
    Thuc hien click button them moi

BP-2541
    [Documentation]     Check input link kenh bang so
    [Tags]    BP-2541
    Thuc hien input so
    Thuc hien click nghe thu
    Click vao ten kenh
    Sleep    5
    Page Should Not Contain Element    ${nut_dung_hoa}
    Page Should Not Contain Element    ${nd_tm_trang_thai_mat_ket_noi}
    Page Should Contain Element    ${nd_tm_trang_thai_nghe_thu}
    Sleep    1
    Go Back
    Go Back
    Thuc hien click button them moi

BP-2543
    [Documentation]     Check input link kenh ky tu dac biet
    [Tags]    BP-2543
    Thuc hien input ky tu dac biet
    Thuc hien click nghe thu
    Click vao ten kenh
    Sleep    5
    Page Should Not Contain Element    ${nut_dung_hoa}
    Page Should Not Contain Element    ${nd_tm_trang_thai_mat_ket_noi}
    Page Should Contain Element    ${nd_tm_trang_thai_nghe_thu}
    Sleep    1
    Go Back
    Go Back
    Thuc hien click button them moi

BP-2540
    [Documentation]     Check chuc nang nghe thu khi de trong
    [Tags]    BP-2540
    Thuc hien input link rong
    Thuc hien click nghe thu
    Click vao ten kenh
    Page Should Contain Element    ${nd_tm_trang_thai_nghe_thu}
    Page Should Not Contain Element    ${nut_dung_hoa}
    Page Should Not Contain Element    ${nd_tm_trang_thai_mat_ket_noi}



#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
