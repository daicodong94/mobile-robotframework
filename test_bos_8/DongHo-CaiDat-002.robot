*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary
Library     String

Resource    ../page/dong_ho/cai_dat.robot
Resource    ../page/dong_ho/dh_common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/common.robot


Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu
*** Test Cases ***

BP-820
    [Documentation]    Check hien thi thoi gian tren tab Dong ho khi Mui gio tu dong o mac dinh bat
    [Tags]    BP-820
    Log To Console    Check hien thi thoi gian tren tab Dong ho khi Mui gio tu dong o mac dinh bat
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Cham vao dau ba cham
    Nhan vao nut cai dat
    Cham vao thay doi ngay gio
    Click icon tat tu dong cap nhat gio theo mang
    ${get_text}=    Get Text     ${dh_check_mui_gio_dong_au}
    Click Element    ${dh_cd_mui_gio}
    Cham khu vuc
    Click Element    ${dh_cd_khu_vuc_ai_cap}
    Page Should Contain Text    (GMT+02:00)
    Click Element At Coordinates    77    154
    Click Element At Coordinates    937    1059
    Element Should not Contain Text    ${dh_check_mui_gio_dong_au}    GMT+02:00 Giờ chuẩn Đông Âu
    Element Should Contain Text    ${dh_check_mui_gio_dong_au}    ${get_text}
    Close Application

BP-821
    [Documentation]    Check hien thi thoi gian tran tab dong ho khi chon mui gio khac
    [Tags]    BP-821
    Log To Console    Check hien thi thoi gian tren tab dong ho khi chon mui gio khac
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Cham vao dau ba cham
    Nhan vao nut cai dat
    Cham vao thay doi ngay gio
    Click icon tat tu dong cap nhat gio theo mang
    Cham vao mui gio
    Cham khu vuc
    AppiumLibrary.Click Element    ${dh_cd_khu_vuc_ai_cap}
    AppiumLibrary.Page Should Contain Element    ${dh_cd_khu_vuc_cairo}
    Log To Console    Check hien thi thoi gian khi them bao thuc tren tab quan ly bao thuc
    Vuot back ve giao dien truoc
    Close Application

BP-829
    [Documentation]    Check hien thi thoi gian im lang voi mac dinh la 10 phut
    [Tags]    BP-829    BP-830    BP-831
    Log To Console    Check hien thi thoi gian im lang voi mac dinh la 10 phut
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Cham vao dau ba cham
    Nhan vao nut cai dat
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_im_lang_sau_10_phut}
    Element Should Contain Text    ${dh_cd_im_lang_sau_10_phut}    ${get_text}
    Log To Console    BP-830: Check hien thi popup Im lang sau khi click label Im lang sau
    Cham vao im lang sau
    AppiumLibrary.Page Should Contain Element    ${dh_btn_huy}
    Log To Console    BP-831: Check dong poup va hien thi dung n phut khi chon gia tri n phut trong popup
    Cham vao 5 phut
    Page Should Contain Element    ${dh_cd_im_lang_sau_5_phut}

BP-835
    [Documentation]    Check hien thi thoi luong bao lai la 10 phut
    [Tags]    BP-835    BP-836    BP-837    BP-838    BP-839
    Log To Console    Check hien thi thoi luong bao lai la 10 phut
    Page Should Contain Element    ${dh_cd_im_lang_sau_10_phut}
    Log To Console    BP-836: Check hien thi popup "Thoi luong bao lai" khi click label "Thoi luong bao lai"
    Cham vao thoi luong bao thuc
    AppiumLibrary.Page Should Contain Element    ${dh_btn_huy}
    AppiumLibrary.Page Should Contain Element    ${dh_cd_im_lang_sau_10_phut}
    Log To Console    BP-837: Check chuc nang vuot len/xuong list radio thoi gian
    Vuot xuong
    AppiumLibrary.Page Should Contain Element    ${dh_cd_im_lang_sau_5_phut}
    Vuot len
    AppiumLibrary.Page Should Contain Element    ${dh_cd_im_lang_sau_13_phut}
    Log To Console    BP-838: Check cham huy
    Cham vao huy
    AppiumLibrary.Page Should Contain Element    ${dh_cd_im_lang_sau}
    Log To Console    BP-839: Check hien thi thoi luong bao lai sau khi chon
    Cham vao thoi luong bao thuc
    Vuot len
    AppiumLibrary.Click Element    ${dh_cd_im_lang_sau_13_phut}
    Page Should Contain Element    ${dh_cd_im_lang_sau_13_phut}
    Close Application


BP-845
    [Documentation]    Check hien thi popup "Tang dan am luong" khi click label "Tang dan am luong"
    [Tags]     BP-844    BP-845     BP-846     BP-847    BP-848
    Log To Console    Check hien thi popup "Tang dan am luong" khi click label "Tang dan am luong"
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Cham vao dau ba cham
    Nhan vao nut cai dat
    Vuot launcher len tren
    Cham tang dan am luong
    Log To Console    BP-844: Check gia tri mac dinh cua Tang dan am luong la "5 giay"
    AppiumLibrary.Page Should Contain Element    ${dh_cd_thoi_luong_5_giay}
    Log To Console    BP-846: Check di chuyen len xuong
    Vuot len
    AppiumLibrary.Page Should Contain Element    ${dh_cd_thoi_luong_60_giay}
    Vuot xuong
    AppiumLibrary.Page Should Not Contain Element    ${dh_cd_thoi_luong_60_giay}
    Log To Console    BP-847: Check di chuyen len xuong
    Cham vao huy
    AppiumLibrary.Page Should Contain Element    ${dh_cd_tang_dan_am_luong}
    Cham tang dan am luong
    AppiumLibrary.Click Element    ${dh_cd_thoi_luong_5_giay}
    Log To Console    BP-848: Check hien thi dung thoi gian sau khi chon thoi gian tang dan am luong
    Page Should Contain Element    ${dh_cd_thoi_luong_5_giay}
    Close Application

BP-863
    [Documentation]    Check tuan bat dau mac dinh la thu hai
    [Tags]        BP-863    BP-864    BP-865
    Log To Console    Check tuan bat dau mac dinh la thu hai
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Click Element At Coordinates    1006      144
    Nhan vao nut cai dat
    Swipe By Percent    50    90    50    20
    Check hien thi thu hai
    log to console     BP-864: Check hien thi popup 3 tuy chon Thu Hai, Thu Bay, Chu Nhat khi cham "Tuan bat dau vao"
    Nhan vao tuan bat dau
    Page Should Contain Element     ${dh_cd_txt_chu_nhat}
    Page Should Contain Element     ${dh_cd_txt_thu_hai}
    Page Should Contain Element    ${dh_cd_txt_thu_bay}
    log to console     BP-865: Check hien thi sau khi chon thu hai thu bay chu nhat
    Click Element    ${dh_cd_txt_thu_bay}
    Sleep    1
    Page Should Contain Element    ${dh_cd_txt_thu_bay}
    Nhan vao tuan bat dau
    Click Element    ${dh_cd_txt_chu_nhat}
    Sleep    1
    Page Should Contain Element    ${dh_cd_txt_chu_nhat}

BP-866
    [Documentation]    Check am thanh hen gio mac dinh
    [Tags]    BP-866    BP-867    BP-875    BP-868    BP-869
    Log To Console    Check am thanh hen gio mac dinh la "Bo hen gio da het han"
    Page Should Contain Element     ${dh_cd_txt_bo_hen_gio_het_han}
    Log To Console    BP-867: Check mo giao dien "Quan ly file" khi cham Them am thanh moi
    Click Element    ${dh_cd_txt_am_thanh_hen_gio}
    Sleep    1
    Click Element    ${dh_cd_txt_them_am_thanh_moi}
    Page Should Not Contain Element    ${dh_cd_txt_them_am_thanh_moi}
    Vuot back ve giao dien truoc
    Log To Console    BP-868: Check am thanh thiet bi mac dinh "Bo hen gio da het han"
    Element Attribute Should Match     ${dh_cd_txt_bo_hen_gio_het_han}     text    *Bộ**hẹn**giờ**đã**hết**hạn*
    Log To Console    BP-869: Check di chuyen len xuong danh sach
    Swipe By Percent    50    90    50    20
    Page Should Not Contain Element    ${dh_cd_txt_Argon_am_nhac}
    Swipe By Percent    50    20    50    90
    Page Should Contain Element    ${dh_cd_txt_Argon_am_nhac}
    Log To Console    BP-875: Check hien thi am thanh sau khi chon am thanh bo hen gio
    Click Element     ${dh_cd_txt_Argon_am_nhac}
    Sleep    1
    Vuot back ve giao dien truoc
    Page Should Contain Element     ${dh_cd_txt_Argon_am_nhac}
    Close Application

BP-872
    [Documentation]    Check gia tri mac dinh Bo hen gio rung mac dinh la tat
    [Tags]     BP-872
    Log To Console    Check gia tri mac dinh Bo hen gio rung mac dinh la tat
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Cham vao dau ba cham
    Nhan vao nut cai dat
    Vuot launcher len tren
    Element Attribute Should Match    ${dh_bt_nut_bo_hen-gio_rung}    checked    false

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================*** Test Cases ***