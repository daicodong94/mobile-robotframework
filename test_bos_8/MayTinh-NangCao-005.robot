*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/may_tinh/mt_common.robot
Resource    ../page/may_tinh/nang_cao.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-1103
    [Documentation]    Check chuc nang DEG voi phep tinh chia ki hieu sin
    [Tags]    BP-1103
    Log To Console    Check chuc nang DEG voi phep tinh chia ki hieu sin
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh chia sin
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    1
    Close Application

BP-1110
    [Documentation]    Check chuc nang RAD voi phep tinh chia ki hieu sin
    [Tags]    BP-1110
    Log To Console    Check chuc nang RAD voi phep tinh chia ki hieu sin
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut RAD
    Sleep    2
    Thuc hien va kiem tra phep tinh chia sin
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    1
    Close Application

BP-1106
    [Documentation]    Check chuc nang DEG voi phep tinh chia ki hieu cos
    [Tags]    BP-1106
    Log To Console     Check chuc nang DEG voi phep tinh chia ki hieu cos
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh chia cos
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    1
    Close Application

BP-1111
    [Documentation]    Check chuc nang RAD voi phep tinh chia ki hieu cos
    [Tags]    BP-1111
    Log To Console    Check chuc nang RAD voi phep tinh chia ki hieu cos
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut RAD
    Sleep    2
    Thuc hien va kiem tra phep tinh chia cos
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    1
    Close Application

BP-1109
    [Documentation]    Check chuc nang DEG voi phep tinh chia ki hieu tan
    [Tags]    BP-1109
    Log To Console    Check chuc nang DEG voi phep tinh chia ki hieu tan
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh chia tan
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    1
    Close Application

BP-1112
    [Documentation]    Check chuc nang RAD voi phep tinh chia ki hieu tan
    [Tags]    BP-1112
    Log To Console    Check chuc nang RAD voi phep tinh chia ki hieu tan
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut RAD
    Sleep    2
    Thuc hien va kiem tra phep tinh chia tan
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    1
    Sleep    2
    Close Application

BP-1113
    [Documentation]    Check phep tinh chia ki hieu ln
    [Tags]    BP-1113
    Log To Console    Check phep tinh chia ki hieu ln
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh chia ln
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    1
    Close Application

BP-1114
    [Documentation]    Check phep tinh chia ki hieu log
    [Tags]    BP-1114
    Log To Console    Check phep tinh chia ki hieu log
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh chia log
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    1
    Sleep    2
    Close Application

BP-1018
    [Documentation]    Check hien thi phep tinh m(+) dang luu trong bo nho
    [Tags]    BP-1018
    Log To Console    Check hien thi phep tinh m(+) dang luu trong bo nho
    Sleep    2
    Mo May tinh
    Sleep    2
    An phep tinh nhan 2x2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut m cong
    Sleep    5
    An nut man hinh nang cao
    Sleep    2
    An nut Clear xoa ket qua
    Sleep    2
    An nut man hinh nang cao
    An nut mr
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    4

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================