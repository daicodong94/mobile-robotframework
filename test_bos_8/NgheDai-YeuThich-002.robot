*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
ThinhLQ         | 2021/07/23 | update code , tach nho case rieng biet
----------------|------------|--------------------------------------------------------------------

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/nghe_dai/yeu_thich.robot

Suite Setup       Mo Nghe dai
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***

BP-1765
    [Documentation]    Xin cap quyen android
    [Tags]        BP-1765    BP-1768    BP-1770    BP-1776
    ...    BP-1777    BP-1778    BP-1782
    Log To Console    BP-1765, BP-1776, BP-1780: 1. Hien thi cac thong bao xin cap quyen
    AppiumLibrary.Page Should Contain Element    ${nut_cho_phep_hoa}
    AppiumLibrary.Page Should Contain Element    ${nut_tu_choi_hoa}
    Log To Console    BP-1778, BP-1770: 1. Man hinh thong bao xin cap quyen se bi keo xuong duoi man hinh
    Tu choi cap quyen cho ung dung
    AppiumLibrary.Page Should Not Contain Element    ${nut_cho_phep_hoa}
    AppiumLibrary.Page Should Not Contain Element    ${nut_tu_choi_hoa}
    Vuot quay ve man hinh Home
    Mo Cai dat tu Launcher
    Click Element    ${cd_ung_dung}
    Sleep    2
    Click Element    ${nd_ung_dung}
    Sleep    1
    Log To Console    BP-1777, BP-1782: 1. Mac dinh cac quyen voi he thong android phai "tat"
    Page Should Contain Element    ${nd_ung_dung_chua_cap_quyen}
    Vuot quay ve man hinh Home
    Mo Nghe dai tu Launcher
    Cho phep cap quyen cho ung dung
    Vuot quay ve man hinh Home
    Mo Cai dat tu Launcher
    Click Element    ${cd_ung_dung}
    Sleep    1
    Click Element    ${nd_ung_dung}
    Sleep    1
    Log To Console    BP-1768: 1. Mac dinh cac quyen voi he thong android phai "bat"
    Page Should Contain Element    ${nd_ung_dung_da_cap_quyen}
    Vuot quay ve man hinh Home
    Mo Nghe dai tu Launcher
    Kiem tra nut play co hoat dong khi bat dau mo app
    Log To Console    BP-1830: Kiem tra chuc nang phat kenh mac dinh
    Thuc hien kiem tra kenh vov3 dang duoc phat
    Sleep    2
    Thuc hien stop kenh dang chay
    Log To Console    BP-1831: Check chuc nang chuyen kenh
    Thuc hien chon kenh vov2 duoc phat
    Sleep    2
    Thuc hien stop kenh dang chay


#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================