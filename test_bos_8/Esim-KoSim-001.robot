*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/esim/es_common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/chim_lac/cl_common.robot

Suite Setup       Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-229
    [Documentation]    Check mo eSIM  tu Launcher
    [Tags]     High    BP-229
    Log To Console   Check mo eSIM  tu Launcher
    Mo eSim tu Launcher
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-231
    [Documentation]    Check hien thi chua lap esim
    [Tags]     High    BP-231
    Mo esim
    Element Should Contain Text    ${eSim_khong_sim_text_dang_trong}    Thẻ eSIM đang trống
    Page Should Contain Element    ${eSim_khong_sim_hotline}    
    Page Should Contain Element    ${eSim_khong_sim_so_hotline}    

BP-239
    [Documentation]    Check mo website huong dan su dung esim khi cham vao link Bphone.vn/eSim
    [Tags]     High    BP-239
    Bam vao website huong dan su dung
    Bat Che do thu gon cua Chim Lac bang popup
    Page Should Contain Element        ${sSim_khong_sim_text_hdsd}
    Page Should Contain Element        ${eSim_khong_sim_bphone_b86}    
    Page Should Not Contain Element    ${eSim_khong_sim_hotline}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================