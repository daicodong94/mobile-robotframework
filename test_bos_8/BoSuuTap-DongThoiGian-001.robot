*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/bo_suu_tap/album.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/bo_suu_tap/xem_video.robot
Resource    ../page/bo_suu_tap/dong_thoi_gian.robot

Suite Setup       Mo Launcher
# Suite Teardown    Run Keywords    Quit Application    Close All Applications

*** Test Cases ***
BP-1209
    [Documentation]    Mo bo suu tap tu Launcher
    [Tags]             High    BP-1209
    Mo Bo suu tap tu Launcher
    Close All Applications

BP-1210 
    [Documentation]    Cham vao tab Dong thoi gian, Album, Video, Thung rac
    [Tags]             High    BP-1210   
    Mo Bo suu tap
    Cho phep cap quyen cho ung dung
    Log To Console    Mo giao dien Album    
    Mo giao dien Album
    Log To Console    Mo giao dien Dong thoi gian    
    Mo giao dien Dong thoi gian
    Log To Console    Mo giao dien Quay phim    
    Mo giao dien Quay phim
    Log To Console    Mo giao dien Thung rac    
    Mo giao dien Thung rac
    Sleep    2    
    Close All Applications
    
BP-1211
    [Documentation]    Vuot chuyen qua cac tab Dong thoi gian, Album, Video, Thung rac
    [Tags]             High    BP-1211
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Log To Console     Vuot sang giao dien Album    
    Vuot man tu phai qua trai
    Page Should Contain Element    ${bst_album_nut_them_moi}
    Sleep    2    
    Log To Console     Vuot sang giao Quay phim
    Vuot man tu phai qua trai
    Page Should Not Contain Element    ${bst_album_nut_them_moi} 
    Sleep    2    
    Log To Console    Vuot sang giao dien Thung rac   
    Vuot man tu phai qua trai
    Page Should Contain Element    ${bst_album_thung_rac_mess}
    Close All Applications
    
BP-1221
    [Documentation]    Check thong tin thoi gian anh khi vuot huong tu duoi len
    [Tags]             BP-1221    BP-1222
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Sleep    2    
    Log To Console    Kiem tra text Hom nay    
    Vuot man tu duoi len tren man Dong thoi gian
    Kiem tra tim kiem gia tri text hom nay trong dong thoi gian
    Vuot man tu duoi len tren man Dong thoi gian
    Vuot man tu duoi len tren man Dong thoi gian
    Log To Console    Kiem tra text Hom qua
    Kiem tra tim kiem gia tri text hom qua trong dong thoi gian
    Log To Console    Kiem tra text radom
    Vuot man tu duoi len tren man Dong thoi gian
    Vuot man tu duoi len tren man Dong thoi gian
    Kiem tra tim kiem gia tri text ngay rundom trong dong thoi gian
    Vuot man tu duoi len tren man Dong thoi gian
    Vuot man tu duoi len tren man Dong thoi gian
    Vuot man tu tren xuong tren man Dong thoi gian
    Sleep    1    
    Kiem tra tim kiem gia tri text ngay rundom trong dong thoi gian
    Sleep    2
    Log To Console    Check thong tin thoi gian anh khi vuot huong tu tren xuong     
    Log To Console    Kiem tra text Hom qua
    Vuot man tu tren xuong tren man Dong thoi gian
    Sleep    1    
    Vuot man tu tren xuong tren man Dong thoi gian
    Sleep    2
    Kiem tra tim kiem gia tri text hom qua trong dong thoi gian   
    Log To Console    Kiem tra text Hom nay
    Sleep    2        
    Vuot man tu tren xuong tren man Dong thoi gian
    Sleep    1    
    Kiem tra tim kiem gia tri text hom nay trong dong thoi gian    
    Close All Applications
    
BP-1240
    [Documentation]    Check hien thanh toolbar khi vuot tu duoi len trong thumbnail thoi gian nam
    [Tags]             BP-1240    BP-1241
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Sleep    3    
    Chon thoi gian "nam" tren man Dong thoi gian
    Chon nam 2021 tren man thoi gian "nam"
    Vuot man tu duoi len tren man Dong thoi gian
    Vuot man tu duoi len tren man Dong thoi gian
    Page Should Contain Element    ${bst_dtg_nut_selectall_toolbar}    
    Page Should Contain Element    ${bst_dtg_nut_exit_toolbar}    
    Page Should Contain Element    ${bst_dtg_title_toolbar}    
    Log To Console  Check an thanh toolbar khi vuot huong tu tren xuong
    Vuot man tu tren xuong tren man Dong thoi gian
    Vuot man tu tren xuong tren man Dong thoi gian
    Page Should Not Contain Element    ${bst_dtg_nut_selectall_toolbar}    
    Page Should Not Contain Element    ${bst_dtg_nut_exit_toolbar}    
    Page Should Not Contain Element    ${bst_dtg_title_toolbar}  
    Close All Applications

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
