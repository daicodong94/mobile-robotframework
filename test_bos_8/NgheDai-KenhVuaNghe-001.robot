*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
ThinhLQ         | 2021/07/23 | update code , tach nho case rieng biet
----------------|------------|--------------------------------------------------------------------

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/nghe_dai/yeu_thich.robot
Resource    ../page/nghe_dai/tat_ca_kenh.robot
Resource    ../page/nghe_dai/kenh_vua_nghe.robot


Suite Setup       Mo Nghe dai
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-2076
    [Documentation]    Check mo tab "Kenh vua nghe" bang thao tac cham
    [Tags]    High    BP-2076
    Cho phep cap quyen cho ung dung
    Sleep   10
    Kiem tra nut play co hoat dong khi bat dau mo app
    Thuc hien click chon tab kenh vua nghe
    Thuc hien kiem tra dang o tai man hinh kenh vua nghe

BP-2079
    [Documentation]    Thuc hien thao tac vuot mo giao dien tat ca kenh
    [Tags]    High    BP-2076
    Vuot sang man hinh ben trai
    Vuot sang man hinh ben phai
    Thuc hien kiem tra dang o tai man hinh kenh vua nghe
    Thuc hien play kenh dang dung
    thuc hien kiem tra tai man hinh kenh vua nghe dang phat nhac
    Thuc hien stop kenh dang chay

BP-2169
    [Documentation]    Check chuc nang phat kenh khong co mang
    [Tags]    BP-2169
    Mang di dong = 1, data = 0, wifi = 0, may bay = 0
    Kiem tra nut play co hoat dong khi bat dau mo app
    Page Should Not Contain Element    ${nd_yt_txt_dang_phat}
    Page Should Contain Element    ${nd_yt_txt_khong_co_ket_noi}
    Sleep    1

BP-2168
    [Documentation]    Check chuc nang phat kenh co mang
    [Tags]    High    BP-2168l
    Mang di dong = 1, data = 1, wifi = 1, may bay = 0
    Kiem tra nut play co hoat dong khi bat dau mo app
    Sleep    3
    Page Should Contain Element    ${nd_yt_txt_dang_phat}
    Sleep    1

BP-2153
    [Documentation]    Thuc hien kiem tra mo giao dien tat ca kenh
    [Tags]    BP-2153    BP-2081    BP-2083    BP-2084    BP-2085    BP-2086    BP-2087    BP-2088    BP-2091
    ...    BP-2093    BP-2094    BP-2096    BP-2097    BP-2099    BP-2100    BP-2101    BP-2102    BP-2103
    ...    BP-2135    BP-2136    BP-2137    BP-2138    BP-2139    BP-2140    BP-2141    BP-2105    BP-2106
    ...    BP-2108    BP-2113    BP-2114    BP-2116    BP-2119    BP-2148    BP-2149    BP-2150    BP-2151
    ...    BP-2152    BP-2158    BP-2159    BP-2160    BP-2161    BP-2162    BP-2163    BP-2164    BP-2165
    ...    BP-2166    BP-2167
    Chup anh man hinh va so sanh voi anh mau    ${nd_alias}    kc_d_ttdh_d_d_KenhVuaNghe    3
    Thuc hien stop kenh dang chay
    AppiumLibrary.Click Element    ${nd_kvn_btn_icon_yeu_thich}
    Chup anh man hinh va so sanh voi anh mau    ${nd_alias}    kc_d_ttdh_d_d_Stop_KenhVuaNghe    3
    Thuc hien play kenh dang dung

BP-2170
    [Documentation]    check chuc nang luu kenh
    [Tags]    BP-2170
    Vuot sang man hinh ben trai
    Thuc hien bam chon vov1 duoc phat

BP-2178
    [Documentation]    Kiem tra chuc nang phat kenh mac dinh
    [Tags]    BP-2178
    Thuc hien click chon tab kenh vua nghe

BP-2173
    [Documentation]    Kiem tra chuc nang luu gia tri tab khi vuot ve home
    [Tags]    BP-2173
    Vuot quay ve man hinh Home
    Mo Nghe dai tu Launcher

BP-2174
    [Documentation]    Kiem tra hien thi man hinh "Kenh vua nghe"
    [Tags]    BP-2174

    Thuc hien bam chon vov1 duoc phat
    AppiumLibrary.Go Back
    Thuc hien kiem tra da an app nghe dai tro ve home
    Mo Nghe dai tu Launcher
    Xac nhan mo app Nghe dai thanh cong
    Thuc hien kiem tra trang thai dang o man hinh yeu thich

BP-2180
    [Documentation]    kiem tra chuc nang chuyen kenh
    [Tags]    BP-2180
    Kiem tra nut play co hoat dong khi bat dau mo app
    Thuc hien chon kenh vov2 duoc phat
    Thuc hien click chon tab kenh vua nghe
    Thuc hien bam chon vov1 duoc phat
    Thuc hien chon kenh vov2 duoc phat
    Thuc hien kiem tra thay doi vi tri theo trang thai phan tu

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================