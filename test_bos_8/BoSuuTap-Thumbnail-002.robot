*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/bo_suu_tap/album.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/bo_suu_tap/album.robot
Resource    ../page/bo_suu_tap/thumbnail.robot
Resource    ../page/bo_suu_tap/thumbnail.robot

# Suite Setup       Mo Launcher
# Suite Teardown    Run Keywords    Quit Application    Close All Applications

*** Test Cases ***
BP-1284
    [Documentation]    Check so luong anh o album goc sau khi sao chep - album sao chep
    [Tags]             BP-1278     BP-1295    BP-1296 
    Mo Bo suu tap
    Cho phep cap quyen cho ung dung  
    Sleep    3    
    Mo giao dien Album
    ${so_luong_anh_truoc_sao_chep}=    Get Text    ${bst_thumbnail_album_thu_1}
    Mo album dau tien
    Sleep    2    
    Chon giu 1 anh trong Dong thoi gian
    Sleep    2    
    Chon anh thu 2 trong Dong thoi gian
    Chon anh thu 2 trong Dong thoi gian
    Chon anh thu 3 trong Dong thoi gian
    Sleep    2    
    Nhan chon Them vao album trong Dong thoi gian
    Sleep    2    
    Nhan chon Album moi
    Log To Console    Check dat ten album la ky tu dac biet, emoticon roi cham Tao tren popup Album moi   
    Dat ten album co ky tu dac biet, emoticon
    Nhan chon Tao
    Sleep    2    
    Page Should Contain Element    ${bst_album_moi_nut_di_chuyen}
    Page Should Contain Element    ${bst_album_moi_nut_sao_chep}
    Page Should Contain Element    ${bst_Tn_popup_di_chuyen_sao_chep}  
    Cham nut Sao chep
    Sleep    3    
    Vuot sang man hinh ben trai 2
    Sleep    2    
    ${so_luong_anh_sau_sao_chep}=    Get Text    ${bst_thumbnail_album_thu_1_sau_khi_tao_1_album}
    Element Attribute Should Match   ${bst_thumbnail_album_thu_1_sau_khi_tao_1_album}    text   ${so_luong_anh_truoc_sao_chep}     
    Log To Console    Check so luong anh o album nhan sao chep   
    Element Attribute Should Match    ${bst_thumnail_co_3_anh}     text   3    
    Close All Applications     
          
Thumbnail_040218
    [Documentation]    Check so luong anh o album goc sau khi sao chep - album moi tao
    [Tags]             BP-1273    Thumbnail_040218    Thumbnail_040219
    Mo Bo suu tap
    Cho phep cap quyen cho ung dung  
    Mo giao dien Album
    ${so_luong_anh_truoc_sao_chep}=    Get Text    ${bst_thumbnail_album_thu_1}    
    Mo album dau tien
    Sleep    2    
    Chon giu 1 anh trong Dong thoi gian
    Sleep    2    
    Chon anh thu 2 trong Dong thoi gian
    Chon anh thu 2 trong Dong thoi gian
    Nhan chon Them vao album trong Dong thoi gian
    Sleep    2    
    Nhan chon Album moi 
    Log To Console    Check dat ten album <80 ky tu roi cham vao Tao tren popup Album moi    
    Dat ten album moi <80 ky tu
    Sleep    2    
    Nhan chon Tao
    Sleep    2    
    Cham nut Sao chep   
    Vuot sang man hinh ben trai 2
    ${so_luong_anh_sau_sao_chep}=    Get Text    ${bst_thumbnail_album_thu_1_sau_khi_tao_1_album}
    Element Attribute Should Match   ${bst_thumbnail_album_thu_1_sau_khi_tao_1_album}    text   ${so_luong_anh_truoc_sao_chep}     
    Log To Console    Check so luong anh o album nhan sao chep   
    Element Attribute Should Match    ${bst_thumnail_co_2_anh}     text   2    
    Close All Applications     

BP-1273
    [Documentation]    Check hien popup Album moi khi cham album moi tren thanh toolbar
    [Tags]             BP-1273    BP-1276
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon giu 1 anh trong Dong thoi gian
    Nhan chon Them vao album trong Dong thoi gian
    Nhan chon Album moi
    Log To Console    Check dat ten album <80 ky tu roi cham vao Tao tren popup Album moi    
    Dat ten album moi <80 ky tu
    Sleep    2    
    Nhan chon Tao
    Sleep    2    
    Cham nut Sao chep
    Close All Applications

BP-1277
    [Documentation]    Check dat ten album moi trung ten voi Album da co roi cham Tao tren popup Album moi
    [Tags]             BP-1277    BP-1280 
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon giu 1 anh trong Dong thoi gian
    Nhan chon Them vao album trong Dong thoi gian
    Nhan chon Album moi
    Dat ten album trung voi ten album da co san
    Nhan chon Tao
    Log To Console    Hien popup thong bao cho phep Sao chep, Di chuyen 
    Page Should Contain Element    ${bst_album_moi_nut_di_chuyen}
    Page Should Contain Element    ${bst_album_moi_nut_sao_chep}
    Page Should Contain Element    ${bst_TN_popup_di_chuyen_sao_chep}
    Log Source    Check sao chep/di chuyen anh khi dat ten album moi trung ten album da co
    Cham nut Sao chep
    Close All Applications

BP-1279 
    [Documentation]    Check dong popup Album moi khi cham Huy
    [Tags]             BP-1279 
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung   
    Chon giu 1 anh trong Dong thoi gian
    Nhan chon Them vao album trong Dong thoi gian
    Nhan chon Album moi 
    Nhan chon HUy trong giao dien Album moi
    Page Should Contain Element    ${bst_TN_nut_huy}   
    Page Should Contain Element    ${bst_thumbnail_nut_album_moi}  
    Page Should Contain Element    ${bst_thumbnail_btn_dong_y}  
    Close All Applications
    
BP-1283
    [Documentation]    Check ten album moi tao sau khi sao chep thanh cong
    [Tags]             BP-1283
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung    
    Mo giao dien Album
    Page Should Contain Element    ${bst_thumbnail_album_tien@😂}  
    Close All Applications

#Can xac dinh chinh xac vi tri cua video
# BP-1318
    # [Documentation]    Check thong bao khi chon video Dat lam
    # [Tags]             BP-1318
    # Mo Bo Suu Tap
    # Cho phep cap quyen cho ung dung  
    # Chon 1 video tren Dong thoi gian
    # Chon Dat lam tren thanh edit tabbar
    # Log To Console    Hien thong bao Khong ung dung nao co the thuc hien tac vu nay
    # Element Text Should Be    ${bst_TN_text_video_dat_lam}    Không ứng dụng nào có thể thực hiện tác vụ này.
    # Sleep    5    
    # Nhan chon vao khoang trong
    # Sleep    2    
    # Close All Applications

BP-1319
    [Documentation]    Check mo thiet lap hinh nen khi chon icon Dat lam
    [Tags]             BP-1319
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung 
    Chon giu 1 anh trong Dong thoi gian
    Chon Dat lam tren thanh edit tabbar
    Log To Console    Hien thi giao dien thiet lap hinh nen cho phep Dat lam hinh nen    
    Sleep    2    
    Page Should Contain Element    ${bst_TN_text_dat_lam_hinh_nen} 
    Page Should Contain Element    ${bst_TN_text_dat_lam_ca_hai}
    Page Should Contain Element    ${bst_TN_text_dat_lam_man_hinh_khoa}
    Close All Applications

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================