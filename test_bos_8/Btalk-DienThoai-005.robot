*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/dien_thoai.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/btalk/nhat_ki_cuoc_goi.robot
Resource    ../page/btalk/gui_nhan_tin_nhan.robot
Resource    ../page/cai_dat/cd_common.robot

*** Test Cases ***
#luu y setup dien thoai khong co lien he trong danh sach cuoc goi nhanh bang cach thay doi ngay len 30 ngay  ko co lien he yeu thich
#tat luu vi tri tab
#==================================================================DANH BA QUAY SO NHANH====================================================================================
BP-2999
    [Documentation]    Check hien thi thong bao Chua co ai trong danh ba quay so nhanh
    [Tags]    BP-2999      BP-3000      1sim    BP-3001
    Mo Btalk
    Vuot len launcher de vao danh ba quay so nhanh
    Page Should Contain Element    ${dt_tab_dt_nut_them_lien_he_yeu_thich}
    Page Should Contain Element    ${dt_tab_dt_chua_co_ai_trong_db_quay_so_nhanh}
    Log To Console    Check mo giao dien chon nguoi lien he khi cham vao Them lien he yeu thich
    An nut them lien he yeu thich
    Page Should Contain Element    ${dt_tab_dt_chon_nguoi_lien_he}
    Log To Console    Check them lien he vao danh ba quay so nhanh
    An chon lien he dau tien chon them lien he yeu thich
    Page Should Contain Element    ${dt_tab_dt_db_quay_so_nhanh_lienhe1}
    Close Application

BP-3002
    [Documentation]    Check them danh ba quay so nhanh khi co cuoc goi di
    [Tags]    BP-3002
    Mo Btalk
    Bam sdt test
    An nut quay so
    Bam nut ket thuc cuoc goi
    Vuot len launcher de vao danh ba quay so nhanh
    Element Attribute Should Match    ${dt_db_quay_so_nhanh_ten_lien_he2}    text    Sim Test

BP-2997
    [Documentation]    Check mo giao dien Danh ba quay so nhanh trong TH chua nhap so tren giao dien Tab dien thoai
    [Tags]    BP-2997    1sim
    Mo Btalk
    Vuot xuong launcher de vao danh ba quay so nhanh
    Page Should Contain Element    ${dt_tab_dt_db_quay_so_nhanh_lienhe1}

BP-2998
    [Documentation]    Check mo giao dien Danh ba quay so nhanh trong TH da nhap so tren giao dien tab dien thoai
    [Tags]    BP-2998    1sim
    Mo Btalk
    An nut 2
    Vuot len launcher de vao danh ba quay so nhanh
    Page Should Contain Element    ${dt_tab_dt_db_quay_so_nhanh_lienhe1}

BP-3005
    [Documentation]    Check mo giao dien goi di khi cham vao 1 lien he o db quay so nhanh
    [Tags]    BP-3005        1sim
    Mo Btalk
    Vuot len launcher de vao danh ba quay so nhanh
    An vao lien he dau tien db quay so nhanh
    Page Should Contain Element    ${dt_ket_thuc_cuoc_goi}

BP-3004
    [Documentation]    Check xoa lien he A trong danh ba Quay so nhanh khi xoa toan bo cuoc goi cua lien he do trong tab gan day
    [Tags]    BP-3004    1sim
    Mo Btalk roi mo Gan day
    An nut ba cham nang cao tab gan day
    An nut xoa nhat ky
    An nut chap nhan
    Bam tab dien thoai
    Vuot len launcher de vao danh ba quay so nhanh
    Page Should Not Contain Element    ${dt_tab_dt_db_quay_so_nhanh_lienhe1}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
