*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/notification/notification.robot
Resource    ../page/config/mac_dinh.robot

# Suite Setup       Mo Launcher
# Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-2720
    [Documentation]    Check thong bao bao thuc luon hien thi ngay duoi header.
    [Tags]    BP-2720    BP-2719    BP-2727
    Tao notify cho app dong ho
    Vuot mo bang thong bao
    Sleep    2    
    ${text_dong_ho}=    Get Text    ${notify_bao_thuc}
    Should Be Equal    Báo thức sắp tới    ${text_dong_ho}
    Log To Console    BP-2719    Check dong thong bao Bao thuc sap toi khi cham nut Loai bo.
    Log To Console    BP-2727    Check cham nut Loai bo se dong thong bao bao thuc. 
    Click Element    ${notify_nut_loai_bo}
    Sleep    2    
    Page Should Not Contain Text    ${text_dong_ho}    
    Xoa toan bo thong bao
    
BP-2730
    [Documentation]     Check mo tuy chon USB khi cham thong bao ket noi USB mo rong
    [Tags]    BP-2730    BP-2732
    Vuot mo bang thong bao
    Click Element    ${notify_nut_mo_rong}
    Sleep    2    
    Click Element    ${notify_txt_bat_truyen_tep}
    Sleep    2    
    Page Should Contain Text    USB    
    Log To Console    BP-2732    Check cham giu vao thong bao mo tuy chon thong bao.
    Vuot mo bang thong bao
    Long Press    ${notify_cham_giu_thong_bao}    
    Sleep    1    
    Page Should Contain Element    ${notify_tat_thong_bao}    
    Xoa toan bo thong bao
    
BP-2740
    [Documentation]    Check cham vuot nhe tu trai sang phai/tu phai sang trai hien thi nut Banh rang va Dong Ho.
    [Tags]    BP-2740    BP-2741
    Vuot mo bang thong bao
    Swipe    935    461    217    461
    Sleep    2    
    Page Should Contain Element    ${notify_setting_thong_bao}
    Swipe    68    458    996    458    
    Sleep    1        
    Page Should Contain Element    ${notify_setting_thong_bao}
    Log To Console    BP-2741    Check cham nut Banh rang hien thi tuy chon thong bao.
    Click Element    ${notify_setting_thong_bao}     
    Sleep    1    
    Page Should Contain Element    ${notify_tat_thong_bao}
    Xoa toan bo thong bao
    
BP-2746
    [Documentation]    Check xoa thong bao khi vuot tu trai qua phai
    [Tags]    BP-2746
    Tao notify cho app dong ho
    Vuot mo bang thong bao
    Swipe    105    477    921    477
    Sleep    3    
    Page Should Not Contain Text    Báo thức sắp tới    
    Xoa toan bo thong bao
    Close Application
    
BP-2747
    [Documentation]     Check xoa thong bao khi vuot tu phai qua trai 
    [Tags]    BP-2747
    Tao notify cho app dong ho
    Vuot mo bang thong bao
    Swipe    921    477    105    477
    Sleep    3    
    Page Should Not Contain Text   Báo thức sắp tới    
    Xoa toan bo thong bao
    Close Application
    
BP-2748
    [Documentation]     Check xoa thong bao khi cham nut Xoa tat ca 
    [Tags]    BP-2748    BP-2761
    Tao notify cho app dong ho
    Vuot mo bang thong bao
    Sleep    2    
    Log To Console    BP-2761     Check dong Notification khi cham nut xoa tat ca.     
    Xoa toan bo thong bao
    Sleep    2    
    Page Should Not Contain Text    Báo thức sắp tới   
    Close Application
    
BP-2756
    [Documentation]    Check dong Notification khi vuot Back 
    [Tags]    BP-2756    BP-2757     BP-2759    
    Mo Launcher
    Vuot mo bang thong bao
    Sleep    2    
    Vuot back ve giao dien truoc
    Page Should Not Contain Text    Appium Settings      
    Log To Console    BP-2757    Check dong Notification khi vuot Home
    Log To Console    BP-2759    Check dong Notification khi vuot tu day len qua 1/2 man hinh    
    Vuot mo bang thong bao
    Sleep    2    
    Vuot quay ve man hinh Home    
    Page Should Not Contain Text    Appium Settings    
    Close Application  
    
BP-2758
    [Documentation]    Check dong Notification khi cham vao khoang trong tren bang thong bao  
    [Tags]    BP-2758    BP-2760  
    Mo Launcher
    Vuot mo bang thong bao
    Sleep    2    
    Click Element At Coordinates    561    1702
    Sleep    2    
    Page Should Not Contain Text    Appium Settings
    Log To Console    BP-2760    Check khong Vuot mo bang thong bao bang cach vuot xuong tu noc khong qua 1/2 man hinh.
    Swipe By Percent    50    10    50    25    duration=4000 
    Sleep    2    
    Page Should Not Contain Text    Appium Settings
    Close Application      
    
BP-2788
    [Documentation]     Check giao dien bang thong bao khi cai dat "Hien thi tat ca noi dung thong bao" 
    [Tags]    BP-2788    BP-2793    BP-2794
    Bat hien thi tat ca cac noi dung thong bao
    # Run Keyword If    ${bphone}==b86    Bat hien thi tat ca cac noi dung thong bao    
    # Run Keyword If    ${bphone}==b3    bat hien thi thong bao tren man hinh khoa cho b3   
    Page Should Contain Text    Appium Settings   
    Log To Console    BP-2793    Check xuat hien thong bao "Nhan lai de mo" khi cham 1 lan vao thong bao    
    Click Element    ${notify_man_hinh_khoa_mo_thong_bao}
    Page Should Contain Text    Nhấn lại để mở
    Log To Console    BP-2794    Check chuyen den giao dien mo khoa khi cham lan 2 vao thong bao
    Click Element    ${notify_man_hinh_khoa_mo_mat_khau}      
    Click Element    ${notify_man_hinh_khoa_mo_mat_khau}
    Vuot back ve giao dien truoc
    Mo khoa voi mat khau 8888   
    Click Element    ${nut_huy_hoa}
    Close Application   
    
  
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================