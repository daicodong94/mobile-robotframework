*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/bms/bms_common.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup        Mo Launcher
Suite Teardown     Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-3182
    [Documentation]    Check mo Diet Virus khi bam BMS tu man hinh Launcher
    [Tags]    High    BP-3182
    Mo BMS tu Launcher roi mo Diet virus
    Sleep    2    
    Page Should Contain Element    ${bms_dvr_nut_quet_virus}    
    Vuot quay ve man hinh Home 
    
BP-3183
    [Documentation]    Check mo Diet Virus khi bam BMS tu Notification
    [Tags]    High    BP-3183    BP-3247
    Vuot mo bang thong bao
    AppiumLibrary.Click Element    ${bms_noti_icon_bms}
    Sleep    2    
    Page Should Contain Element    ${bms_main_nut_diet_virus}    
    Click Element    ${bms_main_nut_diet_virus}
    Sleep    2    
    Page Should Contain Element    ${bms_dvr_nut_quet_virus}    
    Log To Console    BP-3247     Check mo giao dien Diet virus khi cham vao Diet virus hoac icon so xuong hoac anh dai dien   
    Click Element    ${bms_main_nut_diet_virus}
    Click Element At Coordinates    147    213
    Sleep    2    
    Page Should Contain Element    ${bms_dvr_nut_quet_virus}   
    Close All Applications
    
BP-3262
    [Documentation]    Check mo cua so Khuyen cao khi thuc hien Quet toan bo
    [Tags]    BMS-Main    High    CoVirus    BP-3262
    Mo BMS tu Launcher roi mo Diet virus
    Mang di dong = 1, data = 0, wifi = 0, may bay = 0
    Log To Console    Mo giao dien Quet virus, mac dinh da chon Quet toan bo, bam nut Quet
    AppiumLibrary.Click Element    ${bms_dvr_nut_quet_virus}
    AppiumLibrary.Wait Until Page Contains Element    ${bms_dvr_nut_quet}           timeout=2
    AppiumLibrary.Click Element    ${bms_dvr_nut_quet}
    Log To Console    Xac nhan mo cua so Khuyen cao roi bam nut Dong y. Sleep mot luc de on dinh
    AppiumLibrary.Wait Until Page Contains Element    ${bms_dvr_txt_khuyen_cao1}    timeout=2
    AppiumLibrary.Wait Until Page Contains Element    ${bms_dvr_txt_khuyen_cao2}    timeout=2
    AppiumLibrary.Click Element    ${nut_dong_y}
    Tiep tuc va chi bao cao khi quet xong

BP-3266
    [Documentation]    Check qua trinh quet hien thi thanh ProgressBar, danh sach file dang quet... khi thuc hien bam Dong y tren Khuyen cao
    [Tags]    BMS-Main    High    CoVirus    BP-3266
    AppiumLibrary.Wait Until Page Contains Element    ${bms_dvr_pb_thanh_tien_trinh_quet}    timeout=10
    AppiumLibrary.Wait Until Page Contains Element    ${bms_dvr_txt_dang_quet}               timeout=10
    # Sleep    20

BP-3269
    [Documentation]    Check hien thi cua so Thong bao: Tong so file da quet, So virus phat hien khi thuc hien quet xong
    [Tags]    BMS-Main    High    CoVirus    BP-3269    BP-3275 
    AppiumLibrary.Wait Until Page Contains Element    ${bms_dvr_txt_tong_so_file_da_quet}    timeout=30
    AppiumLibrary.Wait Until Page Contains Element    ${bms_dvr_txt_so_virus_phat_hien}      timeout=30
    Log To Console    Dong tat ca ung dung
    Dong toan bo ung dung trong da nhiem
    AppiumLibrary.Close All Applications
    
BP-3270
    [Documentation]    Check quet virus khi thuc hien Quet toan bo
    [Tags]    High    BP-3270    BP-3275
    Mo BMS tu Launcher roi mo Diet virus
    Mang di dong = 1, data = 1, wifi = 1, may bay = 0
    AppiumLibrary.Click Element    ${bms_dvr_nut_quet_virus}
    Wait Until Page Contains Element    ${bms_dvr_nut_quet}    
    AppiumLibrary.Click Element    ${bms_dvr_nut_quet}
    Sleep    2    
    Tiep tuc va chi bao cao khi quet xong
    Page Should Contain Element    ${bms_dvr_txt_tong_so_file_da_quet}    
    Page Should Contain Element    ${bms_dvr_txt_so_virus_phat_hien}    
    Close All Applications 
    
BP-3613
    [Documentation]     Check tu dong quet virus bang cong nghe dien toan dam may khi cai dat cac app tu CH Play 
    [Tags]    High    BP-3613
    Mo Quan ly file
    Mang di dong = 1, data = 1, wifi = 1, may bay = 0
    Cho phep cap quyen cho ung dung
    Click Element At Coordinates    173    643
    Sleep    2    
    Page Should Contain Element    ${bms_dvr_mau_test}    
    AppiumLibrary.Click Element    ${bms_dvr_mau_test}
    Click Element At Coordinates    407    491
    Sleep    1    
    AppiumLibrary.Click Element    ${bms_app_test_quet_dam_may}
    Sleep    2    
    AppiumLibrary.Click Element    ${bms_nut_cai_dat_hoa}
    Sleep    5    
    AppiumLibrary.Wait Until Page Contains Element    ${bms_dvr_txt_tu_dong_bao_ve}    timeout=10    
    Page Should Contain Element    ${bms_dvr_txt_tu_dong_bao_ve}    

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================