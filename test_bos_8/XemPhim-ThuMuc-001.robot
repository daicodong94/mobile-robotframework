*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/chim_lac/cl_common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/xem_phim/thu_muc.robot

Suite Setup       Mo Xem phim
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-3557
    [Documentation]    Check mo Xem phim tu launcher
    [Tags]     High    BP-3557
    Mo Xem phim tu Launcher

BP-3559
    [Documentation]    Check mo Xem phim khi bam app xem phim tu man hinh da nhiem
    [Tags]     High    BP-3559
    Cho phep cap quyen cho ung dung
    Bam mo xem phim tu man hinh da nhiem

BP-3563
    [Documentation]    Check giao dien hien thi cac thu muc, bo cuc sap xep icon co du lieu
    [Tags]    BP-3563
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_Danh_sach_thu_muc    2

BP-3566
    [Documentation]    Check giao dien hien thi khi cham vao icon kinh lup
    [Tags]    BP-3566
    Cham icon tim kiem hinh kinh lup
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kd_d_ttdh_d_d_icon_kinh_lup    2

BP-3573
    [Documentation]    Check giao dien hien thi khi cham icon dau ba cham
    [Tags]             BP-3573
    Cham icon ba cham
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_tm_icon_ba_cham    2

BP-3574
    [Documentation]    Check giao dien hien thi khi cham Sap xep theo..
    [Tags]             BP-3574
    Cham icon ba cham
    Nhan chon Sap xep theo
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_video_icon_sap_xep_theo    2

BP-3575
    [Documentation]    Check giao dien hien thi khi cham Thong tin
    [Tags]             BP-3575
    Bam vao man hinh
    Cham icon ba cham
    Nhan chon Thong tin
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_tm_igd_thong_tin    2

BP-3583
    [Documentation]    Check mac dinh hien thi text "Tim kiem phim" tren thanh tim kiem
    [Tags]             BP-3583
    Vuot sang phai tro ve giao dien truoc do
    Vuot sang phai tro ve giao dien truoc do
    Vuot sang phai tro ve giao dien truoc do
    Cham giu icon kinh lup
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_tm_text_vid_gan_day    2

BP-3587
    [Documentation]    Check chuc nang tim kiem
    [Tags]    BP-3587
    Cham icon tim kiem hinh kinh lup
    Input Text    ${xp_tm_text_tim_kiem_phim}    Sa
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kd_d_ttdh_d_d_tim_kiem    2

BP-3585
    [Documentation]    Check hien thi icon xoa [X] khi nhap du lieu
    [Tags]             BP-3585    BP-3586
    # Nhap text ten phim hshfj
    Page Should Contain Element    ${xp_tm_nut_xoa_truy_van}
    Sleep    2
    Log To Console    Check TH bam dau [X] khi da nhap du lieu tim kiem
    Cham icon xoa [X] de xoa text
    Page Should Contain Element    ${xp_tm_text_tim_kiem_phim}

BP-3592
    [Documentation]    Check hien thi menu khi cham icon ... tren giao dien Danh sach thu muc
    [Tags]             BP-3592
    Cham icon ba cham
    Page Should Contain Element    ${xp_tm_nut_sap_xep_theo}
    Page Should Contain Element    ${xp_tm_nut_cap_nhat_lai}
    Page Should Contain Element    ${xp_tm_nut_thong_tin}

BP-3639
    [Documentation]    Check hien thi giao dien khi chon chia doi man hinh
    [Tags]    BP-3639
    Mo da nhiem
    Click Element    ${dn_icon_ung_dung}
    Sleep    2
    AppiumLibrary.Click Element    ${dn_chia_doi_man_hinh}
    ${hien_thi_app_cai_dat}    Run Keyword And Return Status    AppiumLibrary.Page Should Contain Element    ${app_cai_dat}
    Run Keyword If    ${hien_thi_app_cai_dat}==True    AppiumLibrary.Click Element    ${app_cai_dat}
    ...    ELSE    Vuot sang trai launcher va click app cai dat
    Sleep    2
    Page Should Contain Element    ${cd_tieu_de}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================