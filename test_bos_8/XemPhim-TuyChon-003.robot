*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
ThinhLQ         | 2021/07/23 | update bo comment va thay doi video test, thay doi anh giao dien so sanh
----------------|------------|--------------------------------------------------------------------

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/xem_phim/tuy_chon.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup       Mo Xem phim
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5225
    [Documentation]    Check giao dien hien thi khi hien phim Back 10s
    [Tags]    BP-5225
    Cho phep cap quyen cho ung dung
    Click vao bo danh sach video1
    Click vao video 5
    Cham vao man hinh video
    Click vao nut play
    Vuot thanh thoi gian ve ban dau
    Click vao tuy chon
    Click vao phim back
    Kiem tra giao dien pop up thoi gian back

BP-5286
    [Documentation]    Check hien thi chuc nang An phim Back n giay khi phim Back ns dang hien tren man hinh trinh phat video
    [Tags]    BP-5286
    Kiem tra trang thai khong hien thi text phim back
    Cham vao man hinh video

BP-5287
    [Documentation]    Check an phim Back ns khi thuc hien cham An phim Back n giay o Tuy chon
    [Tags]    BP-5287
    Click vao tuy chon
    Click vao an phim back
    Kiem tra trang thai khong hien thi text an phim back

BP-5238
    [Documentation]    Check khong hien thi nut 1X khi toc do phat mac dinh 1.00x
    [Tags]    BP-5238
    Cham vao man hinh video
    Click vao tuy chon
    Kiem tra trang thai khong hien thi toc do phat la 1X
    Go Back
    Go Back

BP-5232
    [Documentation]    Check dieu chinh toc do phat khi cham/keo giu diem tron tren thanh ProgressBar Toc do phat
    [Tags]    BP-5232
    Click vao video cuoi danh sach1
    Cham vao man hinh video
    Click vao nut play
    Vuot thanh thoi gian ve ban dau
    Click vao tuy chon
    Cham vuot toa do thay doi toc do phat

BP-5239
    [Documentation]    Check hien thi nut 1X khi chon toc do phat khac 1.00x
    [Tags]    BP-5239
    Kiem tra trang thai hien thi toc do phat la 1X
    Click vao nut 1X

BP-5240
    [Documentation]    Check hien thi nut 1X khi chon toc do phat khac 1.00x
    [Tags]    BP-5240
    Kiem tra trang thai mac dinh toc do phat la 1_00x
    Go Back

BP-5241
    [Documentation]    Check mac dinh khong hien thi thoi gian cho
    [Tags]    BP-5241
    Cham vao man hinh video
    Click vao tuy chon
    Kiem tra trang thai mac dinh thoi gian cho
    Click vao tab thoi gian cho
    Click vao nut huy bo

BP-5243
    [Documentation]    Check huy tao thoi gian cho va dong giao dien dong ho kim khi cham khu vuc ngoai giao dien dong ho/cham HUY BO/cham DONG Y
    [Tags]    BP-5243
    Kiem tra trang thai khong hien nut huy bo
    Kiem tra trang thai khong hien nut dong y

BP-5246
    [Documentation]    Check chuc nang chuyen sang giao dien tao thoi gian cho theo dong ho kim
    [Tags]    BP-5246
    Cham vao man hinh video
    Click vao tuy chon
    Click vao tab thoi gian cho
    Click vao kieu dong ho
    Click vao kieu dong ho
    Kiem tra trang thai khong hien thi thoi gian cho

BP-5245
    [Documentation]    Check chuc nang huy tao thoi gian cho khi bam HUY BO/cham phan man hinh ben ngoai giao dien dong ho so
    [Tags]    BP-5245
    Click vao kieu dong ho
    Click vao nut huy bo
    Kiem tra trang thai khong hien thi thoi gian cho

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================