*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/chim_lac/cl_common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/xem_phim/thu_muc.robot

Suite Setup       Mo Xem phim
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
# LUU Y: BAT WIFI
BP-3627
    [Documentation]    Check hien thi "Ban chua chay video nao" khi cham icon Xem video gan day
    [Tags]             BP-3627
    Cho phep cap quyen cho ung dung
    Sleep    2
    Cham icon xem video gan day hinh mui ten quay vong
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_tm_text_khi_cham_icon_xem_gan_day    4

BP-3628
    [Documentation]    Check chuc nang xem lai video khi cham icon Xem video gan day
    [Tags]             BP-3628    BP-3636    BP-3629
    Chon danh sach video SampleVideosMP4
    Chon video dau tien trong danh sach video
    Cham Da nam duoc de tiep tuc xem video
    ${video_da_xem}    Get Text    ${xp_v_ten_video}
    Sleep    2
    Vuot back ve giao dien truoc
    Vuot back ve giao dien truoc
    Sleep    2
    Log To Console    Check xem lai video moi xem gan day nhat khi cham icon Xem video gan day
    Cham icon xem video gan day hinh mui ten quay vong
    Bam vao man hinh
    ${video_phat_gan_nhat}    Get Text    ${xp_v_ten_video}
    Sleep    2
    Should Be Equal    ${video_da_xem}    ${video_phat_gan_nhat}
    Sleep    2
    Log To Console    Check dong Danh sach thu muc khi thuc hien vuot back
    Vuot back ve giao dien truoc
    AppiumLibrary.Page Should Contain Element    ${xp_tm_xem_video_gan_day}

# TRUONG HOP CO MANG WIFI
BP-3599
    [Documentation]    Check mo giao dien Mo bang cho phep chon Chim lac khi cham vao link trang chu
    [Tags]             BP-3599
    Mo Cai dat
    Chon Ung dung va thong bao
    Chon Nang cao trong ung dung va thong bao
    Chon Ung dung mac dinh
    Chon Ung dung trinh duyet
    Chon checkbox mo bang Chim lac
    Dong toan bo ung dung va ket thuc phien kiem thu
    Mo Xem phim
    Cho phep cap quyen cho ung dung
    Cham icon ba cham
    Nhan chon Thong tin
    Nhan chon vao link Trang chu Bkav
    Bat Che do thu gon cua Chim Lac bang popup
    Element Attribute Should Match    ${xp_tm_chim_lac_url_bar}    text    bkav.com.vn*
    Page Should Contain Element    ${xp_gdrm_chim_lac_nut_dong_tab_dang_mo}
    Dong toan bo ung dung va ket thuc phien kiem thu
    Sleep    1
    Log To Console        Check mo giao dien Mo bang cho phep chon Chrome khi cham vao link trang chu
    Mo Cai dat
    Chon Ung dung va thong bao
    Chon Nang cao trong ung dung va thong bao
    Chon Ung dung mac dinh
    Chon Ung dung trinh duyet
    Chon checkbox mo bang Chrome
    Dong toan bo ung dung va ket thuc phien kiem thu
    Mo Xem phim
    Cho phep cap quyen cho ung dung
    Cham icon ba cham
    Nhan chon Thong tin
    Nhan chon vao link Trang chu Bkav
    Element Attribute Should Match    ${xp_tm_chrome_url_bar}    text    *bkav.com.vn*
    Page Should Contain Element    ${xp_tm_chrome_nut_home}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================