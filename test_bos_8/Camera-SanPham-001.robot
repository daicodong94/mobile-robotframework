*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/camera/cam_common.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5729
    [Documentation]    Check mo che do chup San pham o giao dien AI
    [Tags]     High    BP-5729
    Mo May anh roi mo che do chup San pham

BP-5730
    [Documentation]    Mo che do chup San pham tu giao dien cua cac che do chup khac trong AI MODE
    [Tags]     High    BP-5730
    Mo May anh roi mo che do chup Chan dung
    Mo menu AI MODE roi mo che do chup San pham
    Dong toan bo ung dung va ket thuc phien kiem thu
    Mo May anh roi mo che do chup sMacro
    Mo menu AI MODE roi mo che do chup San pham
    Dong toan bo ung dung va ket thuc phien kiem thu
    Mo May anh roi mo che do chup Khoanh khac
    Mo menu AI MODE roi mo che do chup San pham
    Dong toan bo ung dung va ket thuc phien kiem thu
    Mo May anh roi mo che do chup sNight
    Mo menu AI MODE roi mo che do chup San pham
    Dong toan bo ung dung va ket thuc phien kiem thu
    Mo May anh roi mo che do chup Goc rong
    Mo menu AI MODE roi mo che do chup San pham

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================