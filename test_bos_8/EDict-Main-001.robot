*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library    AppiumLibrary    
Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/edict/ttn_common.robot


Suite Setup        Mo Launcher
Suite Teardown     Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5718
    [Documentation]     Check mo thanh cong app eDict tu Launcher 
    [Tags]    High    BP-5718
    Mo eDict tu Launcher
    Dong toan bo ung dung va ket thuc phien kiem thu
    
BP-5719
    [Documentation]    Check tra tu thanh cong khi tra tu bang text
    [Tags]    High    BP-5719
    Mo eDict
    Cho phep cap quyen cho ung dung
    An textbox tim kiem
    Input Text    ${edict_tim_kiem}       a
    Sleep    2    
    Click Element At Coordinates    ${edict_toa_do_x}    ${edict_toa_do_y}
    Sleep    2   
    Chup anh man hinh va so sanh voi anh mau    ${eDict_alias}    kc_d_ttdh_d_d_ketqua_timkiem    2
    
    






 
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================  
        