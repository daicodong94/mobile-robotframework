*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/xem_phim/xp_common.robot

# Suite Setup       Mo Launcher

*** Test Cases ***

BP-275
    [Documentation]    Check bố cục sắp xếp, hình nền, các icon, text Xem phim và mã màu
    [Tags]    BP-275
    Log To Console    kiem tra giao dien man hinh chinh khong co du lieu
    Mo Xem phim
    Cho phep cap quyen cho ung dung
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_XemPhim_Main    2
    Sleep    1



