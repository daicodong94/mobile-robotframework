*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/ghi_am/ga_common.robot
Resource    ../page/config/mac_dinh.robot

# Suite Setup       Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-4349
    [Documentation]     Kiem tra hien thi mac dinh cua screen Danh sach ban ghi
    [Tags]    BP-4349
    Xoa toan bo du lieu app ghi am trong quan ly file
    Mo Ghi am
    Click Element    ${nut_cho_phep_hoa}
    Sleep    2
    Click Element At Coordinates    154    2050
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${ga_alias}    kc_d_ttdh_d_d_danh_sach_ban_ghi_md    2
    Close All Applications

BP-4363
    [Documentation]    Kiem tra quay lai screen Ghi am khi touch button back tai screen Danh sach ban ghi
    [Tags]    BP-4363    BP-4362
    Mo ghi am
    Cho phep cap quyen cho ung dung
    An nut danh sach ban ghi
    Click Element    ${ga_nut_back}
    Sleep    2
    Page Should Contain Element    ${ga_tieu_de}
    An nut danh sach ban ghi
    Sleep    5
    Log To Console    BP-4362     Kiem tra thuc hien ghi am khi bam vao button Ghi am tai Danh sach ban ghi
    Click Element    ${ga_ban_ghi_tao_ghi_am}
    Sleep    2
    Click Element    ${nut_cho_phep_hoa}
    Sleep    2
    AppiumLibrary.Click Element    ${nut_cho_phep_hoa}
    Sleep    30
    Page Should Contain Text    Đang ghi âm
    Click Element At Coordinates    154    2050
    Sleep    2
    AppiumLibrary.Click Element    ${nut_dong_y_hoa}
    Sleep    2

BP-4364
    [Documentation]    Kiem tra hien popup tuy chon cua file khi giu file
    [Tags]    BP-4364    BP-4365    BP-4366    BP-4368
    Long Press    ${ga_ban_ghi_list}
    Sleep    5
    Page Should Contain Element    ${ga_ban_ghi_doi_ten}
    Page Should Contain Element    ${ga_ban_ghi_xoa}
    Page Should Contain Element    ${ga_ban_ghi_chia_se}
    Log To Console    BP-4365    Kiem tra hien thi popup Ten moi khi touch Doi ten
    AppiumLibrary.Click Element    ${ga_ban_ghi_doi_ten}
    Sleep    2
    ${get_text}=    Get Text    ${ga_tieu_de}
    Page Should Contain Text    ${get_text}
    Log To Console    BP-4366    Kiem tra kich ban Luu file
    Log To Console    BP-4368    Kiem tra hien thi dung ten file da doi sau khi thuc hien doi ten file
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}    DaiTVb
    Sleep    2
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    Page Should Contain Text    DaiTVb

BP-4372
    [Documentation]    Kiem tra hoan tac file ban ghi vua xoa khi bam button Hoan tac
    [Tags]    BP-4372
    # ${vi_tri}=    AppiumLibrary.Get Element Attribute    ${ga_ban_ghi_list}    index
    Long Press    ${ga_doi_ten_file}
    Sleep    2
    AppiumLibrary.Click Element    ${ga_ban_ghi_xoa}
    AppiumLibrary.Click Element    ${ga_nut_hoan_tac}
    Sleep    2
    # Element Attribute Should Match    ${ga_ban_ghi_list}    index    ${vi_tri}
    Page Should Contain Element    ${ga_doi_ten_file}

BP-4369
    [Documentation]    Kiem tra xoa file ban ghi khi bam Xoa tai Tuy chon file
    [Tags]    BP-4369    BP-4373    BP-4375
    Long Press    ${ga_doi_ten_file}
    Sleep    2
    AppiumLibrary.Click Element    ${ga_ban_ghi_xoa}
    Sleep    5
    Page Should Not Contain Text    DaiTVb
    Log To Console    BP-4373    Kiem tra an button Hoan tac sau 5s
    Page Should Not Contain Element    ${ga_nut_hoan_tac}
    Log To Console    BP-4375    Kiem tra dong bo khi xoa file ban ghi o Quan ly file
    Mo thu muc ghi am trong quan ly file
    Page Should Not Contain Text    DaiTVb
    Close All Applications

BP-4376
    [Documentation]    Kiem tra hien thi mac dinh popup Chia se
    [Tags]    BP-4376    BP-4377    BP-4378
    Mo app ghi am va tao ban ghi
    Mo popup chia se file ghi am
    # Page Should Contain Element    ${ga_popup_chia_se_bms}
    # Page Should Contain Element    ${ga_popup_chia_se_btalk}
    Page Should Contain Element    ${ga_popup_chia_se_bluetooth}
    Page Should Contain Element    ${ga_popup_chia_se_gmail}
    Log To Console    BP-4377    Kiem tra thuc hien chia se thanh cong ban ghi qua Bluetooth trong popup Chia se
    ${so_phan_tu_bluetooth}=    Get Matching Xpath Count    ${ga_popup_chia_se_bluetooth}
    Run Keyword If    ${so_phan_tu_bluetooth}>1     AppiumLibrary.Click Element    ${ga_popup_chia_se_bluetooth_1}
    Run Keyword If    ${so_phan_tu_bluetooth}==1    AppiumLibrary.Click Element    ${ga_popup_chia_se_bluetooth}
    Sleep    5
    ${trang_thai_bluetooth}    Run Keyword And Return Status    AppiumLibrary.Wait Until Page Contains Element    ${nut_bat}
    Run Keyword If    ${trang_thai_bluetooth}==True    AppiumLibrary.Click Element    ${nut_bat}
    ...    ELSE    Sleep    10
    Sleep    10
    Page Should Contain Element    ${ga_title_bluetooth}
    Log To Console    BP-4378    Kiem tra thuc hien chia se thanh cong ban ghi qua Gmail trong popup Chia se
    Go back
    Sleep    2
    Mo popup chia se file ghi am
    ${so_phan_tu_gmail}=    Get Matching Xpath Count    ${ga_popup_chia_se_gmail}
    Run Keyword If    ${so_phan_tu_gmail}>1     AppiumLibrary.Click Element    ${ga_popup_chia_se_gmail_1}
    Run Keyword If    ${so_phan_tu_gmail}==1    AppiumLibrary.Click Element    ${ga_popup_chia_se_gmail}
    AppiumLibrary.Wait Until Page Contains Element    ${ga_popup_soan_thu_toi}    timeout=10
    Input Text    ${ga_popup_soan_thu_toi}    daicodong94@gmail.com
    Sleep    1
    Click Element    ${ga_nut_gui_mail}
    Sleep    2
    Page Should Contain Element    ${ga_tieu_de}
    Close All Applications

BP-4379
    [Documentation]    Kiem tra thuc hien chia se thanh cong ban ghi qua Chat trong popup Chia se
    [Tags]    BP-4379
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut danh sach ban ghi
    Mo popup chia se file ghi am
    ${so_phan_tu_chats}=    Get Matching Xpath Count    ${ga_popup_chia_se_chats}
    Run Keyword If    ${so_phan_tu_chats}>1     AppiumLibrary.Click Element    ${ga_popup_chia_se_chats_1}
    Run Keyword If    ${so_phan_tu_chats}==1    AppiumLibrary.Click Element    ${ga_popup_chia_se_chats}
    Sleep    2
    Page Should Contain Text    Messenger
    Close All Applications

BP-4381
    [Documentation]    Kiem tra thuc hien chia se thanh cong ban ghi qua Zalo trong popup Chia se
    [Tags]    BP-4381
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut danh sach ban ghi
    Mo popup chia se file ghi am
    ${so_phan_tu_zalo}=    Get Matching Xpath Count    ${ga_popup_chia_se_zalo}
    Run Keyword If    ${so_phan_tu_zalo}>1     AppiumLibrary.Click Element    ${ga_popup_chia_se_zalo_1}
    Run Keyword If    ${so_phan_tu_zalo}==1    AppiumLibrary.Click Element    ${ga_popup_chia_se_zalo}
    Sleep    2
    Page Should Contain Text    Zalo
    Close All Applications

BP-4351
    [Documentation]    Kiem tra thu tu sap xep ban ghi theo thoi gian luu
    [Tags]    BP-4351
    Tao file ghi am theo thoi gian
    Long Press    ${ga_ban_ghi_list}
    Sleep    2
    Click Element    ${ga_ban_ghi_doi_ten}
    Sleep    2
    ${text_ban_ghi_moi_nhat}=    AppiumLibrary.Get Text    ${ga_textbox_ten_ban_ghi}
    Should Be Equal    ban ghi 3    ${text_ban_ghi_moi_nhat}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================