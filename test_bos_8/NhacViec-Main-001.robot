*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/nhac_viec/nv_common.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup       Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5714
    [Documentation]    Check mo app Nhac viec tu Launcher
    [Tags]    High    BP-5714
    Mo Nhac viec tu Launcher
    Close All Applications
    
NV_Quan_ly_nhac_viec
    [Documentation]    Quan ly nhac viec
    [Tags]             High    NV_Quan_ly_nhac_viec
    Mo Nhac viec
    Cho phep cap quyen cho ung dung
    Sleep    2    
    Log To Console    Them nhac viec Dam cuoi    
    An nut them nhac viec
    Chon them nhac viec dam cuoi
    Nhap text them nhac viec dam cuoi
    Chon ngay - hom nay
    Chon nhac truoc - nhac truoc 1 ngay
    Chon nhac lai - khong lap
    Chon nut Luu de them nhac viec moi
    Sleep    2   
    Page Should Contain Text    Dam cuoi nhe    
    Sleep    2    
    Log To Console    Sua nhac viec dam cuoi    
    Chon sua nhac viec Dam cuoi - text Dam cuoi ne
    Chon ngay - ngay mai
    Chon nhac truoc - nhac truoc 2 ngay
    Chon nut Luu de them nhac viec moi
    Close All Applications
    Sleep    5    
    Log To Console    Them nhac viec Sinh nhat    
    Mo Nhac viec
    Cho phep cap quyen cho ung dung
    An nut them nhac viec
    Chon them nhac viec sinh nhat
    Nhap tex them nhac viec Sinh nhat
    Chon ngay - ngay mai
    Chon nhac truoc - nhac truoc 2 ngay
    Chon nhac lai - hang nam
    Chon nut Luu de them nhac viec moi
    Sleep    2   
    Page Should Contain Text    Sinh nhat nho di    
    Sleep    2    
    Log To Console    Sua nhac viec sinh nhat    
    Chon sua nhac viec Sinh nhat - text Sinh nhat nho di
    Chon nhac truoc - nhac truoc 1 ngay
    Chon nhac lai - khong lap
    Chon nut Luu de them nhac viec moi
    Close All Applications
    Sleep    5    
    Log To Console    Them nhac viec Cong viec    
    Mo Nhac viec
    Cho phep cap quyen cho ung dung    
    An nut them nhac viec
    Chon them nhac viec Cong viec
    Nhap tex them nhac viec Cong viec
    Chon ngay - hom nay
    Chon nhac truoc - khong nhac truoc
    Chon nhac lai - khong lap
    Chon nut Luu de them nhac viec moi
    Sleep    2   
    Page Should Contain Text    Cong viec nho lam    
    Sleep    2    
    Log To Console    Sua nhac viec Cong viec
    Chon sua nhac viec Cong viec - text Cong viec nho lam
    Chon ngay - ngay mai
    Chon nut Luu de them nhac viec moi    
    Close All Applications

BP-5715
    [Documentation]    Check tao thanh cong nhac viec cong viec
    [Tags]    High    BP-5715    BP-5716    BP-5717
    Mo Nhac viec
    Cho phep cap quyen cho ung dung
    An nut them nhac viec
    An nut them cong viec
    Input Text    ${nhap_ten_cong_viec}    Nhac cong viec
    An nut luu
    Log To Console    BP-5716     Check tao thanh cong Dam cuoi
    An nut them nhac viec
    An nut them dam cuoi
    Input Text    ${nhap_ten_ngay_cuoi}    Nhac dam cuoi
    An nut luu
    Log To Console    BP-5717     Check tao thanh cong nhac viec Sinh nhat 
    An nut them nhac viec
    An nut them sinh nhat
    Input Text    ${nhap_ten_sinh_nhat}    Nhac sinh nhat
    An nut luu    
    Page Should Contain Text    Nhac cong viec
    Page Should Contain Text    Nhac dam cuoi
    Page Should Contain Text    Nhac sinh nhat

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================