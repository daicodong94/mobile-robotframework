*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/chim_lac/cl_common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/wifi/wifi.robot
Resource    ../page/cai_dat/cd_common.robot

Suite Setup       Mo Cai dat
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5777
    [Documentation]    Check mo wifi trong ung dung Cai dat
    [Tags]             BP-5777
    Chon mang va internet
    Bat ket noi wifi
    Chon wifi trong cai dat
    Page Should Contain Element    ${wifi_text_su_dung_wifi}
    Element Attribute Should Match    ${wifi_nut_bat_wifi_bat}    checked    true

Wi-Fi_011
    [Documentation]    Check hien thi mat khau da tick vao checkbox Hien thi mat khau
    [Tags]             Wi-Fi_011
    Chon wifi Bkav-QA-50GHz
    Nhap mat khau Bkav@2021 - mat khau dung
    Hien thi mat khau
    Click HUY thay doi

Wi-Fi_013
    [Documentation]    Check trang thai nut Ket noi khi nhap mat khau nho hon 8 ky tu
    [Tags]             Wi-Fi_013
    Chon wifi Bkav-QA-50GHz
    Nhap mat khau duoi 8 ky tu
    Element Attribute Should Match    ${wifi_nut_ket_noi}    enabled    false
    Click HUY thay doi

Wi-Fi_014
    [Documentation]    Check trang thai nut Ket noi khi nhap mat khau lon hon 8 ky tu
    [Tags]             Wi-Fi_014    Wi-Fi_017
    Chon wifi Bkav-QA-50GHz
    Nhap mat khau tren 8 ky tu - mat khau sai
    Element Attribute Should Match    ${wifi_nut_ket_noi}    enabled    true
    Log To Console     Wi-Fi_014 : Check trang thai nut Ket noi khi mat khau co 8 ky tu
    Nhao mat khau co 8 ky tu - mat khau sai
    Element Attribute Should Match    ${wifi_nut_ket_noi}    enabled    true
    Log To Console     Wi-Fi_017 : Check dong giao dien nhap mat khau khi cham HUY
    Click HUY thay doi

Wi-Fi_015
    [Documentation]    Check hien thi thong bao ket noi voi diem phat song khi nhap sai mat khau
    [Tags]             Wi-Fi_015
    Chon wifi Bkav-QA-50GHz
    Nhap mat khau tren 8 ky tu - mat khau sai
    Chon nut Ket noi
    Element Attribute Should Match    ${wfi_bkav_qa_50GHz_kiem_tra_ket_noi}     text   Kiểm tra mật khẩu và thử lại

Wi-Fi_016
    [Documentation]    Check trang thai ket noi voi diem phat song khi nhap dung mat khau
    [Tags]             Wi-Fi_016
    Chon wifi Bkav-QA-50GHz
    Nhap mat khau Bkav@2021 - mat khau dung
    Chon nut Ket noi
    Element Attribute Should Match    ${wfi_bkav_qa_50GHz_kiem_tra_ket_noi}     text    Đã kết nối

Wi-Fi_023
    [Documentation]    Check mo giao dien Thong tin chi tiet ve mang
    [Tags]             Wi-Fi_023
    Click chon icon cai dat xem thong tin wifi
    Page Should Contain Element    ${wifi_thong_tin_text_cuong_do_tin_hieu}
    Page Should Contain Element    ${wifi_thong_tin_text_tan_so}
    Page Should Contain Element    ${wifi_thong_tin_text_chia_se}
    Page Should Contain Element    ${wifi_dps_text_bao_mat}
    Chup anh man hinh va so sanh voi anh mau    ${wifi_alias}    kc_d_ttdh_d_d_giao_dien_thong_tin_wifi    2
    Vuot sang man hinh ben trai dong giao dien wifi

Wi-Fi_026
    [Documentation]    Check TH mo giao dien Tuy chon Wifi khi cham Tuy chon Wifi
    [Tags]             Wi-Fi_026
    Vuot man xuong duoi tim kiem Tuy chon wifi
    Click chon Nang cao
    Page Should Contain Element    ${wifi_tuy_chon_wifi_text_cai_dat_chung_chi}
    Page Should Contain Element    ${wifi_tuy_chon_wifi_text_NCCDV_xep_hang_mang}
    Page Should Contain Element    ${wifi_tuy_chon_wifi_text_wifi_Direct}
    Page Should Contain Element    ${wifi_tuy_chon_wifi_text_dia_chi_MAC}
    Page Should Contain Element    ${wifi_tuy_chon_wifi_text_dia_chi_IP}
    Page Should Contain Text    d4:d2:e5
    Vuot sang man hinh ben trai dong giao dien wifi
    Sleep    5

Wi-Fi_021
    [Documentation]    Check dong giao dien Them mang khi cham HUY
    [Tags]             Wi-Fi_021
    Vuot man xuong duoi tim kiem Them mang
    Page Should Contain Element    ${wifi_them_mang_text_nhap_ssid}
    Page Should Contain Element    ${wif_them_mang_icon_maQR}
    Click HUY thay doi
    Page Should Not Contain Element    ${wifi_nut_HUY}
    Page Should Not Contain Element    ${wifi_nut_LUU}
    Sleep    2

Wi-Fi_038
    [Documentation]    Check mo giao dien Mang da luu khi cham Mang da luu
    [Tags]             Wi-Fi_038
    Vuot man xuong duoi tim kiem Mang da luu
    Page Should Contain Element    ${wifi_bkav_qa_50GHz}
    Page Should Contain Element    ${wifi_icon_khoa_mang_da_luu}
    Sleep    2
    Vuot sang man hinh ben trai dong giao dien wifi

Wi-Fi_043
    [Documentation]    Check giao dien Diem phat song va chia se ket noi
    [Tags]             Wi-Fi_043    Wi-Fi_044
    Vuot sang man hinh ben trai dong giao dien wifi
    Chon diem phat song va chia se ket noi
    Page Should Contain Element    ${wifi_text_diem_phat_song_wifi}
    Page Should Contain Element    ${wifi_text_chia_se_kn_qua_usb}
    Page Should Contain Element    ${wifi_text_chia_se_kn_qua_bluetooth}
    Log To Console    Wi-Fi_044 : Check mo giao dien Diem phat song Wi-Fi khi cham Diem phat song Wi-Fi
    Chon Diem phat song Wifi
    Tat chia se ket noi wifi
    Page Should Contain Element    ${wifi_dps_text_ten_diem_phat_song}
    Page Should Contain Element    ${wifi_dps_text_bao_mat}
    Page Should Contain Element    ${wifi_dps_mat_khau_diem_phat_song}
    Sleep    2

Wi-Fi_047
    [Documentation]    Check dong cua so chinh sua Ten diem phat song khi cham DONG Y
    [Tags]             Wi-Fi_047
    Chon Diem phat song Wifi
    Click Ten diem phat song
    Thay doi Ten diem phat song -> BkavAutoTest
    Click DONG Y thay doi
    Element Attribute Should Match    ${wifi_vt_ten_diem_phat_song}    text    BkavAutoTest

Wi-Fi_049
    [Documentation]    Check dong cua so chinh sua Ten diem phat song khi cham HUY
    [Tags]             Wi-Fi_049
    Click Ten diem phat song
    Click HUY thay doi
    Element Attribute Should Match    ${wifi_vt_ten_diem_phat_song}    text    BkavAutoTest

Wi-Fi_050
    [Documentation]    Check hien thi popup tuy chon dang Bao mat khi cham Bao mat
    [Tags]             Wi-Fi_050      Wi-Fi_051
    Click Ten diem phat song
    Thay doi Ten diem phat song -> Bphone_7779
    Click DONG Y thay doi
    Click chon dang Bao mat
    Chup anh man hinh va so sanh voi anh mau    ${wifi_alias}    kc_d_ttdh_d_d_popup_tuy_chon_dang_bao_mat    2
    Page Should Contain Element    ${wifi_dps_bao_mat_text_wpa2_perrsonl}
    Page Should Contain Element    ${wifi_dps_bap_mat_text_khong}
    Log To Console    Wi-Fi_051 : Check hien thi truong Mat khau diem phat song chon dang bao mat la WPA2 PSK khi cham radio button WPA2 PSK
    Click chon WPA2-Personal
    Element Attribute Should Match    ${wifi_vt_mat_khau_diem_phat_song}       text    •••••••••••

Wi-Fi_053
    [Documentation]    Check thay doi mat khau diem phat song
    [Tags]             Wi-Fi_053    Wi-Fi_054
    Click mat khau diem phat song
    Thay doi mat khau diem phat song -> AutoTest123
    Log To Console     Wi-Fi_054 : Check dong cua so nhap mat khau khi cham DONG Y
    Click DONG Y thay doi

Wi-Fi_055
    [Documentation]    Check dong cua so nhap mat khau khi cham HUY
    [Tags]             Wi-Fi_055
    Click mat khau diem phat song
    Click HUY thay doi
    Element Attribute Should Match    ${wifi_vt_mat_khau_diem_phat_song}       text    •••••••••••
    Page Should Not Contain Element    ${wifi_nut_HUY}

Wi-Fi_056
    [Documentation]    Check dong truong Mat khau diem phat song khi chon dang bao mat la Khong
    [Tags]             Wi-Fi_056
    Click chon dang Bao mat
    Click chon Khong - Bao mat
    Page Should Not Contain Element    ${wifi_dps_mat_khau_diem_phat_song}
    Chup anh man hinh va so sanh voi anh mau    ${wifi_alias}    kc_d_ttdh_d_d_bao_mat_khong    2

Wi-Fi_063
    [Documentation]    Check hien thi popup tuy chon Bang tan AP khi cham Bang tan AP
    [Tags]             Wi-Fi_063
    Click chon dang Bao mat
    Click chon WPA2-Personal
    Click chon Nang cao
    Click chon bang tan AP
    Element Attribute Should Match    ${wifi_text_bang_tan_2,4GHz}    checkable    true
    Chup anh man hinh va so sanh voi anh mau    ${wifi_alias}    kc_d_ttdh_d_d_popup_bang_tan_AP_2,4GHz    1
    Click chon bang tan 2,4GHz

Wi-Fi_064
    [Documentation]    Check thay doi Bang tan 5GHz khi cham radio button Bang tan 5GHz
    [Tags]             Wi-Fi_064
    Click chon bang tan AP
    Click chon bang tan 5HGz

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================