*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/bo_suu_tap/album.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/bo_suu_tap/xem_anh.robot
Resource    ../page/bo_suu_tap/xem_video.robot

Suite Setup       Mo Launcher
# Suite Teardown    Run Keywords    Quit Application    Close All Applications

*** Test Cases ***
BP-1394
    [Documentation]    Mo bo suu tap tu Launcher
    [Tags]             High    BP-1394
    Mo Bo suu tap tu Launcher
    Close All Applications

BP-1395 
    [Documentation]    Cham vao tab Dong thoi gian, Album, Video, Thung rac
    [Tags]             High    BP-1395   
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Log To Console    Mo giao dien Album    
    Mo giao dien Album
    Log To Console    Mo giao dien Dong thoi gian    
    Mo giao dien Dong thoi gian
    Log To Console    Mo giao dien Quay phim    
    Mo giao dien Quay phim
    Log To Console    Mo giao dien Thung rac    
    Mo giao dien Thung rac
    Sleep    2    
    Close All Applications
    
BP-1396
    [Documentation]    Vuot chuyen qua cac tab Dong thoi gian, Album, Video, Thung rac
    [Tags]             High    BP-1396
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Log To Console     Vuot sang giao dien Album    
    Vuot man tu phai qua trai
    Page Should Contain Element    ${bst_album_nut_them_moi}
    Sleep    2    
    Log To Console     Vuot sang giao Quay phim
    Vuot man tu phai qua trai
    Page Should Not Contain Element    ${bst_album_nut_them_moi} 
    Sleep    2    
    Log To Console    Vuot sang giao dien Thung rac   
    Vuot man tu phai qua trai
    Page Should Contain Element    ${bst_album_thung_rac_mess}
    Close All Applications
    
BP-1415 
    [Documentation]    Check mo popup xem thong tin anh/video khi cham vao icon tren thanh edit tabbar
    [Tags]     High    BP-1415 
    Mo Bo suu tap
    Cho phep cap quyen cho ung dung 
    Mo giao dien Album
    Chon album May anh tren giao dien album
    Log To Console    Check cac truong thong tin hien thi doi voi anh chụp tu dien thoai    
    Sleep    2    
    Chon 1 anh bat ky
    Cham vao anh  
    Nhan mo popup xem thong tin anh/video
    Page Should Contain Element    ${bst_xem_anh_thong_tin_ten}
    Page Should Contain Element    ${bst_xem_anh_thong_tin_ngay}    
    Page Should Contain Element    ${bst_xem_anh_thong_tin_kich_thuoc}    
    Page Should Contain Element    ${bst_xem_anh_thong_tin_dung_luong}    
    Page Should Contain Element    ${bst_xem_anh_thong_tin_trinh_tao}    
    Page Should Contain Element    ${bst_xem_anh_thong_tin_thiet_bi}    
    Page Should Contain Element    ${bst_xem_anh_thong_tin_chieu}
    Page Should Contain Element    ${bst_xem_anh_thong_tin_man_chap}    
    Page Should Contain Element    ${bst_xem_anh_thong_tin_iso}    
    Page Should Contain Element    ${bst_xem_anh_thong_tin_den_flash}    
    Page Should Contain Element    ${bst_xem_anh_thong_tin_khau_do}
    Page Should Contain Element    ${bst_xem_anh_thong_tin_tieu_cu}    
    Page Should Contain Element    ${bst_xem_anh_thong_tin_duong_dan}
    Nhan chon ĐONG tren popup thong tin
    Close All Applications 

BP-1404
    [Documentation]    Check hien thanh toolbar va thanh edit tabbar khi cham vao lan 1
    [Tags]             BP-1404
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon 1 anh bat ky
    Cham vao anh
    Page Should Contain Element    ${bst_xem_anh_nut_exit_toolbar}    
    Page Should Contain Element    ${bst_xem_anh_nut_more_func}    
    Page Should Contain Element    ${bst_xem_anh_nut_thong_tin}    
    Page Should Contain Element    ${bst_xem_anh_nut_chia_se}    
    Page Should Contain Element    ${bst_xem_anh_nut_chinh_sua}    
    Page Should Contain Element    ${bst_xem_anh_nut_xoa}
    Page Should Contain Element    ${bsT_xem_anh_nut_khoi_phuc}   
    Close All Applications

BP-1405
    [Documentation]    Check an thanh toolbar va thanh edit tabbar khi cham vao lan 2
    [Tags]             BP-1405
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon 1 anh bat ky
    Cham vao anh
    Cham vao anh   
    Page Should Not Contain Element    ${bst_xem_anh_nut_exit_toolbar}    
    Page Should Not Contain Element    ${bst_xem_anh_nut_more_func}    
    Page Should Not Contain Element    ${bst_xem_anh_nut_thong_tin}    
    Page Should Not Contain Element    ${bst_xem_anh_nut_chia_se}    
    Page Should Not Contain Element    ${bst_xem_anh_nut_chinh_sua}    
    Page Should Not Contain Element    ${bst_xem_anh_nut_xoa}
    Page Should Not Contain Element    ${bsT_xem_anh_nut_khoi_phuc}   
    Close All Applications

BP-1420
    [Documentation]    Check chia se kho cham vao icon chia se tren thanh edit tabbar
    [Tags]             BP-1420
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon 1 anh bat ky
    Cham vao anh
    Chon chia se anh tren thanh edit tabbar
    Page Should Contain Element    ${bst_xem_anh_app_facebook}    
    Cham vao anh
    Close All Applications

BP-1423
    [Documentation]    Check sua anh dang xem khi cham vao icon Sua anh tren thanh edit tabbar
    [Tags]             BP-1423
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Cham vao anh
    Cham vao anh
    Cham vao Sua anh tren thanh edit tabbar
    Cham vao icon Sua mau anh tren thanh edit tabbar 
    Page Should Contain Element    ${bst_xem_anh_mau_khong_co}    
    Page Should Contain Element    ${bst_xem_anh_punch}    
    Page Should Contain Element    ${bst_xem_anh_vintage}    
    Page Should Contain Element    ${bst_xem_anh_BM}
    Cham vao nut Huy

BP-1425
    [Documentation]    Check khi cham vao icon Sua anh tren thanh edit tabbar
    [Tags]             BP-1425
    Cham vao anh
    Cham vao anh    
    Cham vao Sua anh tren thanh edit tabbar
    Cham vao iccon thay doi kich thuoc anh
    Page Should Contain Element    ${bst_xem_anh_chinh_sua_nut_cat}    
    Page Should Contain Element    ${bst_xem_anh_chinh_Sua_nut_lam_phang}    
    Page Should Contain Element    ${bst_xem_anh_chinh_sua_nut_xoay}
    Sleep    2  

BP-1426
    [Documentation]    Check mo giao dien cat anh khi cham vao icon Cat
    [Tags]    BP-1426
    Cham vao nut Cat tren giao dien chinh sua kich thuoc anh
    Sleep    2    
    Page Should Contain Element    ${bst_xem_anh_chinh_sua_text_khong_co}    
    Page Should Contain Element    ${bst_xem_anh_chinh_sua_text_goc}    
    Page Should Contain Element    ${bst_xem_anh_chinh_sua_text_11}     
    Cham vao nut Huy tren thanh edit tabbar
    Sleep    2        

BP-1444
    [Documentation]    Check mo giao dien khi cham vao icon do tuong phan tong mau
    [Tags]             BP-1444
    Cham vao icon dieu chinh do phoi sang/tuong phan/ do sac net
    Sleep    5    
    Page Should Contain Element    ${bst_xem_anh_do_sang_tu_dong}    
    Page Should Contain Element    ${bst_xem_anh_so_sang_phoi_sang}    
    Page Should Contain Element    ${bst_xem_anh_do_sang_tuong_phan}    
    Page Should Contain Element    ${bst_xem_anh_do_sang_dao_dong}        
    Sleep    2    
    
BP-1451 
    [Documentation]    Check mo giao dien tinh nang chen icon len anh khi cham vao icon Chen
    [Tags]             BP-1451 
    Cham vao icon chen them anh
    Sleep    5    
    Page Should Contain Element    ${bst_xem_anh_icon_vi_tri}    
    Page Should Contain Element    ${bst_xem_anh_icon_thoi_gian}    
    Page Should Contain Element    ${bst_xem_anh_icon_thoi_tiet}    
    Page Should Contain Element    ${bst_xem_anh_chen_icon_cam_xuc}
    Sleep    2        
    
BP-1488 
    [Documentation]    Check huy icon da chen tren anh khi cham vao icon Huy
    [Tags]             BP-1488 
    Cham vao icon Vi Tri tren giao dien icon chem them anh
    Sleep    5    
    Cham vao icon 1 tren giao dien icon chen them anh
    Sleep    2    
    Page Should Contain Element    ${bst_xem_anh_chen_icon_1_da_chen}
    Cham vao nut Huy tren thanh edit tabbar
    Sleep    2    
    Page Should Not Contain Element    ${bst_xem_anh_chen_icon_1_da_chen}  
    Sleep    2    
    
BP-1485
    [Documentation]    Check luu cac icon da chen tren anh khi cham vao icon Luu
    [Tags]    BP-1485
    Cham vao icon Vi Tri tren giao dien icon chem them anh
    Sleep    5    
    Cham vao icon 1 tren giao dien icon chen them anh
    Cham vao nut Luu tren thanh edit tabbar
    Page Should Contain Element    ${bst_xem_anh_chen_icon_1_da_chen}  
    Sleep    2           

BP-1497 
    [Documentation]    Check hien thi thong bao Huy thay doi khi cham vao Huy
    [Tags]    BP-1497     BP-1498
    Cham vao icon thoi tiet tren giao dien icon chen them anh    
    Sleep    5    
    Cham vao icon 2 tren giao dien icon chen them anh
    Cham vao nut Luu tren thanh edit tabbar
    Cham vao nut Huy
    Page Should Contain Element    ${bst_xem_anh_chen_icon_mess}   
    Log To Console    Check dong thong bao khi cham vao Huy         
    Cham vao huy trong thong bao tren giao dien chen them icon vao anh     
    
BP-1501
    [Documentation]    Check thoat khoi che do chinh sua anh khi chon Huy va thoat
    [Tags]    BP-1501
    Cham vao nut Huy
    Cham vao huy va thoat trong thong bao tren giao dien chen them icon vao anh
    Page Should Not Contain Element    ${bst_xem_anh_chen_icon_2}
    Page Should Not Contain Element    ${bst_xem_anh_chinh_anh_nut_fxbutton}    
    Page Should Not Contain Element    ${bst_xem_anh_chinh_anh_nut_geometry}    
    Page Should Not Contain Element    ${bst_xem_anh_chinh_anh_nut_colors}    
    Page Should Not Contain Element    ${bst_xem_anh_chinh_anh_nut_waterMark}    
    Page Should Not Contain Element    ${bst_xem_anh_chinh_sua_nut_edit_photo}    
    Close All Applications
    
BP-1531 
    [Documentation]    Hien thi danh sach cac icon ho tro
    [Tags]             BP-1531   
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon 1 anh bat ky
    Cham vao anh
    Cham vao Sua anh tren thanh edit tabbar
    Cham vao icon chinh sua anh nang cao
    Cho phep cap quyen cho ung dung
    Page Should Contain Element    ${bst_xem_anh_brush_control}    
    Page Should Contain Element    ${bst_xem_anh_color_select}    
    Page Should Contain Element    ${bst_xem_anh_erase_control}    
    Page Should Contain Element    ${bst_xem_anh_icon_control}    
    Page Should Contain Element    ${bst_xem_anh_text_control}    
    Page Should Contain Element    ${bst_xem_anh_menu_control}    
    Close All Applications
    
BP-1545 
    [Documentation]    Check khi cham vao icon ... tren thanh toolbar
    [Tags]             BP-1545   
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon 1 anh bat ky
    Cham vao anh
    Cham vao icon ba cham tren thanh toolbar
    Page Should Contain Element    ${bst_xem_anh_ba_cham_mo_voi}    
    Page Should Contain Element    ${bst_xem_anh_ba_cham_dung_lam}    
    Page Should Contain Element    ${bst_xem_anh_ba_cham_in}    
    Page Should Contain Element    ${bst_xem_anh_ba_cham_doi_ten}    
        
BP-1546
    [Documentation]    Check khi cham vao Mo voi
    [Tags]             BP-1546
    Cham vao Mo voi tu icon ba cham tren thanh toolbar
    Page Should Contain Element    ${bst_xem_anh_mo_voi_app_chia_se_bst}    
    Page Should Contain Element    ${bst_xem_anh_mo_voi_app_chia_se_anh}
    Cham vao anh
    
BP-1548
    [Documentation]    Check khi cham vao dung lam
    [Tags]             BP-1548
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon 1 anh bat ky
    Cham vao anh
    Cham vao icon ba cham tren thanh toolbar 
    Cham vao Dung lam tu icon ba cham tren thanh toolbar
    Page Should Contain Element    ${bst_xem_anh_dat_lam_hinh_nen}    
    Page Should Contain Element    ${bst_xem_anh_dat_lam_hinh_khoa}    
    Page Should Contain Element    ${bst_xem_anh_dat_lam_ca_hai}
    Close All Applications    
    
BP-1552    
    [Documentation]    Check mo giao dien in anh khi cham vao IN
    [Tags]             BP-1552
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon 1 anh bat ky
    Cham vao anh
    Cham vao icon ba cham tren thanh toolbar
    Cham vao In tu icon ba cham tren thanh toolbar
    Page Should Contain Element    ${bst_xem_anh_in_chon_may_in}    
    Page Should Contain Element    ${bst_xem_anh_in_ban_sao}    
    Page Should Contain Element    ${bst_xem_anh_in_kho_giay}
    Close All Applications
    
BP-1553
    [Documentation]    Check khi cham vao doi ten
    [Tags]             BP-1553    BP-1555     BP-1558
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon 1 anh bat ky
    Cham vao anh
    Cham vao icon ba cham tren thanh toolbar
    Cham vao Doi ten tu icon ba cham tren thanh toolbar
    Log Source    Check doi ten voi ky tu dac biet
    Thay doi ten anh voi ky tu dac biet
    Cham vao dong y tren giao dien doi ten anh
    Log Source    Check doi ten thanh cong
    Page Should Contain Element    ${bst_xem_anh_dat_ten_ky_tu_dac_biet}
        
BP-1559
    [Documentation]    Check huy doi ten khi cham vao huy       
    [Tags]    BP-1559
    Cham vao icon ba cham tren thanh toolbar
    Cham vao Doi ten tu icon ba cham tren thanh toolbar
    Cham vao Huy tren giao dien doi ten anh
    Page Should Contain Element    ${bst_xem_anh_dat_ten_ky_tu_dac_biet}    
    Close All Applications        
    
# BP-1417
    # [Documentation]    Check cac truong thong tin voi ca nguon anh tu Anh chup man hinh, anh download
    # [Tags]             BP-1417
    # Mo Bo Suu Tap
    # Cho phep cap quyen cho ung dung 
    # Log To Console     Check cac truong thong tin voi ca nguon anh tu Anh chup man hinh
    # Mo giao dien Album
    # Chon album Chup man hinh tren giao dien album
    # Chon 1 anh bat ky   
    # Cham vao anh
    # Nhan mo popup xem thong tin anh/video
    # Page Should Contain Element    ${bst_xem_anh_thong_tin_ten}
    # Page Should Contain Element    ${bst_xem_anh_thong_tin_ngay}    
    # Page Should Contain Element    ${bst_xem_anh_thong_tin_kich_thuoc}    
    # Page Should Contain Element    ${bst_xem_anh_thong_tin_dung_luong}     
    # Page Should Contain Element    ${bst_xem_anh_thong_tin_duong_dan}
    # Sleep    3    
    # Nhan chon ĐONG tren popup thong tin
    # Close All Applications
    # Mo Bo Suu Tap
    # Cho phep cap quyen cho ung dung 
    # Log To Console    Check cac truong thong tin voi ca nguon anh tu Anh Download    
    # Mo giao dien Album
    # Chon album Download tren giao dien album
    # Chon 1 anh bat ky   
    # Cham vao anh
    # Nhan mo popup xem thong tin anh/video
    # Sleep    5    
    # Page Should Contain Element    ${bst_xem_anh_thong_tin_ten}
    # Page Should Contain Element    ${bst_xem_anh_thong_tin_ngay}    
    # Page Should Contain Element    ${bst_xem_anh_thong_tin_kich_thuoc}    
    # Page Should Contain Element    ${bst_xem_anh_thong_tin_dung_luong}     
    # Page Should Contain Element    ${bst_xem_anh_thong_tin_duong_dan}
    # Nhan chon ĐONG tren popup thong tin
    # Close All Applications
    
BP-1418
    [Documentation]    Check cac truong thong tin hient hi doi voi video
    [Tags]             BP-1418    BP-1419
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung 
    Mo giao dien Quay phim   
    Chon 1 video tren man Quay Phim
    Sleep    2    
    Cham vao anh
    Nhan mo popup xem thong tin anh/video
    Sleep    2    
    Page Should Contain Element    ${bst_xem_anh_thong_tin_ten}
    Page Should Contain Element    ${bst_xem_anh_thong_tin_ngay}    
    Page Should Contain Element    ${bst_xem_anh_thong_tin_kich_thuoc}    
    Page Should Contain Element    ${bst_xem_anh_thong_tin_dung_luong}     
    Page Should Contain Element    ${bst_xem_anh_thong_tin_duong_dan}
    Page Should Contain Element    ${bst_xem_anh_thong_tin_toc_do_khung_hinh}  
    Sleep    1    
    Log To Console    Dong popup xem thong tin anh/video           
    Nhan chon ĐONG tren popup thong tin
    Close All Applications
    
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================