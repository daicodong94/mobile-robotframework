*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/may_tinh/mt_common.robot

Suite Setup    Mo May tinh
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu
*** Test Cases ***
BP-302
    [Documentation]    kiem tra phep tinh tru (-) giua cac so nguyen
    [Tags]    BP-302
    An nut 456
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *333*
    Should Be Equal    ${get_text}    333
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2
BP-304
    [Documentation]    kiem tra phep tinh tru (-) giua so am (-) va cac so nguyen
    [Tags]    BP-304
    An nut Clear xoa ket qua
    An nut dau tru (-)
    An nut 123
    An nut dau tru (-)
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    −579
    Should Be Equal    ${get_text}    −579
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

BP-306
    [Documentation]    kiem tra phep tinh tru (-) giua cac so thap phan
    [Tags]    BP-306
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau tru (-)
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    0
    Should Be Equal    ${get_text}    0
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2
BP-308
    [Documentation]    kiem tra phep tinh tru (-) giua cac so thap phan va cac so nguyen
    [Tags]    BP-308
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0**456*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2
BP-310
    [Documentation]    kiem tra phep tinh nhan (x) giua cac so nguyen
    [Tags]    BP-310
    An nut Clear xoa ket qua
    An nut 456
    An nut dau nhan (x)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *56**088*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

BP-312
    [Documentation]    kiem tra phep tinh nhan (x) giua so am (-) va cac so nguyen
    [Tags]    BP-312
    An nut Clear xoa ket qua
    An nut dau tru (-)
    An nut 123
    An nut dau nhan (x)
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *−56**088*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

BP-314
    [Documentation]    kiem tra phep tinh nhan (x) giua cac so thap phan
    [Tags]    BP-314
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau nhan (x)
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *15**241**38*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

BP-317
    [Documentation]    kiem tra phep tinh tru (-) giua cac so thap phan va cac so nguyen
    [Tags]    BP-317
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau nhan (x)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *15**185**088*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

BP-318
    [Documentation]    kiem tra phep tinh nhan (x) giua cac so thap phan va cac so nguyen
    [Tags]    BP-318
    An nut Clear xoa ket qua
    An nut 12345678901234556
    An nut dau nhan (x)
    An nut 12345678901234556
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *1**524157*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

BP-321
    [Documentation]    kiem tra phep tinh chia (:) giua cac so nguyen
    [Tags]    BP-321
    An nut Clear xoa ket qua
    An nut 456
    An nut dau chia (÷)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *3**707317*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

BP-323
    [Documentation]    kiem tra phep tinh chia (:) giua so am (-) va cac so nguyen
    [Tags]    BP-323
    An nut Clear xoa ket qua
    An nut dau tru (-)
    An nut 123
    An nut dau chia (÷)
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *−0**26973*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

BP-325
    [Documentation]    kiem tra phep tinh chia (:) giua cac so thap phan
    [Tags]    BP-325
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau chia (÷)
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Should Be Equal    ${get_text}    1
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

BP-328
    [Documentation]    kiem tra phep tinh chia (:) giua cac so thap phan va cac so nguyen
    [Tags]    BP-328
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau chia (÷)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *1**003707*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2
    An nut Clear xoa ket qua


#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================