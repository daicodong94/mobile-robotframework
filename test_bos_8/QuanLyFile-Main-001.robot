*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library    AppiumLibrary    

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/quan_ly_file/qlf_common.robot

Suite Setup        Mo Launcher
Suite Teardown     Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5720
    [Documentation]    Check mo QLF tu man hinh Launcher 
    [Tags]    High    BP-5720
    Mo Quan ly file tu Launcher
    Close All Applications
    
BP-5721
    [Documentation]    Check mo QLF tu app khac (Vi du app Cai dat)
    [Tags]    High    BP-5721    
    Mo Cai dat
    AppiumLibrary.Click Element    ${qlf_cd_amthanh}
    Sleep    2    
    AppiumLibrary.Click Element    ${qlf_cd_amthanh_nhac_chuong}
    Sleep    2    
    AppiumLibrary.Click Element    ${qlf_cd_icon_qlf}
    Sleep    2    
    Cho phep cap quyen cho ung dung
    # Chup anh man hinh va so sanh voi anh mau    ${qlf_alias}    kc_d_ttdh_d_d_QLF_mo_tu_cai_dat   2
    Close All Applications
    
BP-5722
    [Documentation]    Check mo QLF tu man hinh da nhiem
    [Tags]    High    BP-5722
    Mo Quan ly file
    Cho phep cap quyen cho ung dung
    Mo da nhiem
    Mo quan ly file tu da nhiem
    # Chup anh man hinh va so sanh voi anh mau    ${qlf_alias}    kc_d_ttdh_d_d_QLF_main    2
    Close All Applications
    
BP-5733
    [Documentation]     Check di chuyen noi dung tu dien thoai vao the nho khi cham Di chuyen noi dung
    [Tags]    High    BP-5733    BP-5746
    Mo Quan ly file
    Cho phep cap quyen cho ung dung
    Click Element At Coordinates    168    664
    Sleep    2    
    Click Element At Coordinates    975    479
    Sleep    2    
    Click Element    ${qlf_nut_nhieu_hon}
    Sleep    2    
    Click Element    ${qlf_nut_copy}
    Sleep    2    
    Click Element    ${qlf_the_nho_SD}
    Sleep    2    
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    Click Element At Coordinates    498    2076
    Sleep    2    
    Click Element    ${nut_cho_phep_hoa}
    Sleep    2    
    Click Element    ${nut_dong_y_hoa}
    Sleep    2    
    Page Should Contain Text    Alarms    
   
    
    






#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================  
        