*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/cai_dat/cu_chi_dieu_huong.robot
Resource    ../page/launcher/launcher_common.robot

*** Test Cases ***
BP-4541
    [Documentation]    Kiem tra gia tri mac dinh cua dieu huong trong cai dat bang cu chi
    [Tags]     High    BP-4541    BP-4542    BP-4544    BP-4556
    Mo Cai dat
    Nhan vao tab dieu huong
    Kiem tra hien thi mac dinh dieu huong bang cu chi
    Log To Console    BP-4542
    Nhan vao tab dieu huong bang cu chi
    Nhan vao tab dieu huong bang thanh dieu huong
    Nhan vao nut icon tat cu chi dieu huong
    Vuot back ve giao dien truoc
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    ${bphone}_kc_d_ttdh_d_d_cd_cu_chi    4
    Vuot quay ve man hinh Home
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    ${bphone}_kc_d_ttdh_d_d_cd_cu_chi    4
    Mo da nhiem
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    ${bphone}_kc_d_ttdh_d_d_cd_cu_chi    4
    Nhan mo thanh tang giam do sang
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    ${bphone}_kc_d_ttdh_d_d_cd_cu_chi    4
    Nhan mo thanh tang giam am thanh
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    ${bphone}_kc_d_ttdh_d_d_cd_cu_chi    4
    Nhan vao nut icon bat cu chi dieu huong
    Vuot back ve giao dien truoc
    Kiem tra hien thi tai trang cai dat
    Nhan vao tab dieu huong
    Nhan mo thanh tang giam do sang
    Vuot cu chi giam do sang
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    ${bphone}_kc_d_ttdh_d_d_giam_do_sang    6
    Nhan mo thanh tang giam do sang
    Vuot cu chi tang do sang
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    ${bphone}_kc_d_ttdh_d_d_tang_do_sang    6
    Nhan mo thanh tang giam am thanh
    Vuot cu chi giam am thanh
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    ${bphone}_kc_d_ttdh_d_d_giam_am_thanh    6
    Nhan mo thanh tang giam am thanh
    Vuot cu chi tang am thanh
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    ${bphone}_kc_d_ttdh_d_d_tang_am_thanh    6
    Vuot mo bang dieu khien
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    ${bphone}_kc_d_ttdh_d_d_dashboard    13
    Vuot back ve giao dien truoc
    Nhan vao tab dieu huong bang thanh dieu huong
    Nhan vao tab dieu huong bang cu chi
    Close Application

BP-4562
    [Documentation]    vuot mo da nhiem bang cu chi
    [Tags]     High    BP-4562
    Mo Cai dat
    Nhan vao tab dieu huong
    Mo da nhiem
    Page Should Contain Element    ${nut_dong_toan_bo_app}
    Sleep    1
    Click Element    ${nut_dong_toan_bo_app}
    Close Application

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================