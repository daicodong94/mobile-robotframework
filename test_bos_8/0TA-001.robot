*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/chim_lac/cl_common.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup       Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-958
    [Documentation]   Mo tat ca ung dung sau khi len OTA thanh cong
    [Tags]     High    BP-958
    Run Keyword And Continue On Failure    Dong toan bo ung dung trong da nhiem
    Run Keyword And Continue On Failure    Mo BMS tu Launcher
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Cai dat tu Launcher
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo May tinh tu Launcher
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Thoi tiet tu Launcher
    Run Keyword And Continue On Failure    Cho phep cap quyen cho ung dung
    Log To Console     Xac nhan mo ung dung Thoi tiet thanh cong
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${tt_nut_do_c}
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${tt_nut_do_f}
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Nghe nhac tu Launcher
    Run Keyword And Continue On Failure    Cho phep cap quyen cho ung dung
    Log To Console     Xac nhan mo ung dung Nghe nhac thanh cong
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${nn_view}
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Ghi chep tu Launcher
    Run Keyword And Continue On Failure    Cho phep cap quyen cho ung dung
    Log To Console     Xac nhan mo ung dung Ghi chep thanh cong
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${gc_dang_xu_ly}
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Bo suu tap tu Launcher
    Run Keyword And Continue On Failure    Cho phep cap quyen cho ung dung
    Log To Console     Xac nhan mo ung dung Bo suu tap thanh cong
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${bst_xem_anh}
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Nhac viec tu Launcher
    Run Keyword And Continue On Failure    Cho phep cap quyen cho ung dung
    Log To Console     Xac nhan mo ung dung Nhac viec thanh cong
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${nv_nut_them_moi}
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Hinh nen tu Launcher
    Run Keyword And Continue On Failure    Bam nut huy thay doi hinh nen
    Run Keyword And Continue On Failure    Mo Kham pha tu Launcher
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Ho tro tu Launcher
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Suc khoe tu Launcher
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Danh ba tu Launcher
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Quan ly file tu Launcher
    Run Keyword And Continue On Failure    Cho phep cap quyen cho ung dung
    Log To Console     Xac nhan mo ung dung Quan ly file thanh cong
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${qlf_bo_nho}
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo eDict tu Launcher
    Run Keyword And Continue On Failure    Cho phep cap quyen cho ung dung
    Log To Console     Xac nhan mo ung dung eDict thanh cong
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${edict_tim_kiem}
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Ghi am tu Launcher
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Dong ho tu Launcher
    Run Keyword And Continue On Failure    Cho phep cap quyen cho ung dung
    Log To Console     Xac nhan mo ung dung Dong ho thanh cong
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${dh_tab_bao_thuc}
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${dh_tab_dong_ho}
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${dh_tab_bo_hen_gio}
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${dh_tab_bam_gio}
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Xem phim tu Launcher
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Nghe dai tu Launcher
    Run Keyword And Continue On Failure    Cho phep cap quyen cho ung dung
    # Run Keyword And Continue On Failure    AppiumLibrary.Click Element    ${nd_stop}
    Log To Console     Xac nhan mo ung dung Nghe dai thanh cong
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${nd_tab_yeu_thich}
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${nd_tab_tat_ca_kenh}
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${nd_tab_kenh_vua_nghe}
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo eSIM tu Launcher
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Dien thoai tu Launcher
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Tin nhan tu Launcher
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo May anh tu Launcher
    Run Keyword And Continue On Failure    Cho phep cap quyen cho ung dung
    Log To Console     Xac nhan mo ung dung May anh thanh cong
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${cam_nut_cai_dat}
    Run Keyword And Continue On Failure    Vuot quay ve man hinh Home
    Run Keyword And Continue On Failure    Mo Chim lac tu Launcher
    Run Keyword And Continue On Failure    Bat Che do thu gon cua Chim Lac bang popup
    Log To Console     Xac nhan mo ung dung Chim lac thanh cong
    Run Keyword And Continue On Failure    AppiumLibrary.Page Should Contain Element    ${cl_url_bar}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================