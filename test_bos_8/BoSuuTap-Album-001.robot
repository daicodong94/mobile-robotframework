*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/bo_suu_tap/album.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup       Mo Launcher

*** Test Cases ***
BP-1126
    [Documentation]    Mo bo suu tap tu Launcher
    [Tags]             High    BP-1126
    Mo Bo suu tap tu Launcher
    Close All Applications

BP-1127 
    [Documentation]    Cham vao tab Dong thoi gian, Album, Video, Thung rac
    [Tags]             High    BP-1127   
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Log To Console    Mo giao dien Album    
    Mo giao dien Album
    Log To Console    Mo giao dien Dong thoi gian    
    Mo giao dien Dong thoi gian
    Log To Console    Mo giao dien Quay phim    
    Mo giao dien Quay phim
    Log To Console    Mo giao dien Thung rac    
    Mo giao dien Thung rac
    Sleep    2    
    Close All Applications
    
BP-1128
    [Documentation]    Vuot chuyen qua cac tab Dong thoi gian, Album, Video, Thung rac
    [Tags]             High    BP-1128
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Log To Console     Vuot sang giao dien Album    
    Vuot man tu phai qua trai
    Page Should Contain Element    ${bst_album_nut_them_moi}
    Sleep    2    
    Log To Console     Vuot sang giao Quay phim
    Vuot man tu phai qua trai
    Page Should Not Contain Element    ${bst_album_nut_them_moi} 
    Sleep    2    
    Log To Console    Vuot sang giao dien Thung rac   
    Vuot man tu phai qua trai
    Page Should Contain Element    ${bst_album_thung_rac_mess}
    Close All Applications
    
BP-1184
    [Documentation]    Check mo giao dien tao Album khi bam vao them moi album
    [Tags]             High    BP-1184         
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Mo giao dien Album
    Log To Console    Check mo giao dien tao Album khi bam vao them moi album 
    Sleep    2    
    Bam vao icon tao them Album
    Close All Applications

BP-1147
    [Documentation]    Check chon album khi cham giu vao album
    [Tags]             BP-1147   BP-1148    BP-1150 
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Mo giao dien Album
    Nhan giu chon album chup man hinh 
    Nhan giu chon album may anh
    Sleep    1 
    Close All Applications
    
BP-1152
    [Documentation]    Check bo chon khi cham giu vao album
    [Tags]             BP-1152   
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Mo giao dien Album
    Nhan giu chon album chup man hinh 
    Nhan giu chon album may anh
    Click Element    ${bst_album_chup_man_hinh}
    Page Should Contain Element    ${bst_album_title_1_muc_duoc_chon} 
    Click Element    ${bst_album_may_anh}
    Page Should Contain Element    ${bst_album_nut_them_moi}    
    Sleep    1    
    Close All Applications
    
BP-1154
    [Documentation]    Check bo chon tat ca album tren thanh toolbar
    [Tags]             BP-1154
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Sleep    2    
    Mo giao dien Album
    Nhan giu chon album chup man hinh 
    Nhan giu chon album may anh
    Sleep    2   
    Click Element    ${bst_album_toolbar_nut_bo_chon_tat_ca}
    Sleep    1   
    Page Should Contain Element    ${bst_album_nut_them_moi}  
    Sleep    2    
    Close All Applications
    
BP-1163
    [Documentation]    Check hien thi danh sach cac app ho tro chia se
    [Tags]             BP-1163 
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Mo giao dien Album
    Nhan giu chon album chup man hinh
    Nhan chon chia se
    Log To Console    Check mo rong/thu nho app chia se khi vuot    
    Vuot mo rong man chia se tu duoi len tren
    Sleep    1    
    Vuot thu nho man chia se tu tren xuong
    Sleep    1    
    Log To Console    Check an danh sach app chia se khi cham vao khoang trong    
    Nhan chon vao khoang trong
    Sleep    1  
    Click Element    ${bst_album_chup_man_hinh}
    Close All Applications
    
# BP-1189
    # [Documentation]    Check mo giao dien tao Album khi bam vao them moi album
    # [Tags]             BP-1189     BP-1187
    # Mo Bo Suu Tap
    # Cho phep cap quyen cho ung dung
    # Mo giao dien Album
    # Sleep    2    
    # Bam vao icon tao them Album
    # Sleep    5    
    # Log To Console    Check mo rong thumbnail trong giao dien tao album khi vuot an ban phim    
    # Vuot mo rong thumbnail khi vuot an ban phim
    # Sleep    2    
    # Chon 1 anh trong album
    # Log To Console    Check dat ten album trung voi album da co    
    # Dat ten album trung voi ten album da co
    # Sleep    2    
    # Bam chon dong y them ten moi cho them album
    # Sleep    5    
    # Log To Console    Check dong poup Di chuyen hay sao chep khi vuot back    
    # Vuot man tu phai qua trai
    # Sleep    5    
    # Bam chon dong y them ten moi cho them album
    # Sleep    1    
    # Bam chon Sao chep trong popup Di chuyen hay sao chep
    # Sleep    2    
    # Close All Applications
    
BP-1190
    [Documentation]    Check dat ten album <30 ky tu
    [Tags]             BP-1190
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Mo giao dien Album
    Sleep    2    
    Bam vao icon tao them Album
    Vuot mo rong thumbnail khi vuot an ban phim
    Sleep    2  
    Chon 1 anh trong album
    Log To Console    Them moi album Test    
    Dat ten album khong co ky tu dac biet
    Sleep    2    
    Bam chon dong y them ten moi cho them album
    Sleep    5    
    Log To Console    Check dong poup Di chuyen hay sao chep khi chon vao khoang trong    
    Nhan chon vao khoang trong tren man dat ten album
    Sleep    5    
    Bam chon dong y them ten moi cho them album
    Log To Console    Kiem tra album Test vua moi tao
    Bam chon Sao chep trong popup Di chuyen hay sao chep    
    Page Should Contain Element    ${bst_album_test}     
    Close All Applications

BP-1192 
    [Documentation]    Check dong popup di chuyen hay sao chep khi vuot back
    [Tags]             BP-1192 
    Mo Bo suu tap
    Cho phep cap quyen cho ung dung 
    Mo giao dien Album
    Bam vao icon tao them Album
    Chon 1 anh trong album
    Dat ten album khong co ky tu dac biet  
    Sleep    2    
    Bam chon dong y them ten moi cho them album   
    Vuot man tu trai qua phai
    Close All Applications
    
BP-1193 
    [Documentation]    Check dong popup Di chuyen hay sao chep khi cham ra vung ngoai popup tren man hinh
    [Tags]             BP-1193    
    Mo Bo suu tap
    Cho phep cap quyen cho ung dung 
    Mo giao dien Album
    Bam vao icon tao them Album
    Chon 1 anh trong album
    Dat ten album khong co ky tu dac biet  
    Sleep    2    
    Bam chon dong y them ten moi cho them album  
    Nhan chon vao khoang trong tren man dat ten album
    Close All Applications
     
 BP-1191
    [Documentation]    Check dat ten album co ky tu dac biet va emoticon
    [Tags]             BP-1191
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Mo giao dien Album
    Bam vao icon tao them Album
    Vuot mo rong thumbnail khi vuot an ban phim
    Sleep    2  
    Chon 1 anh trong album
    Log To Console    Them moi album Test@😂    
    Dat ten album co ky tu dac biet
    Sleep    2    
    Bam chon dong y them ten moi cho them album
    Sleep    5    
    Bam chon Sao chep trong popup Di chuyen hay sao chep
    Sleep    1    
    Close All Applications
    
# Thuc hien tren B86 ban ROM 88
# BP-1200
    # [Documentation]    Check tu dong thay doi ten album khi thay doi ten o Quan ly file
    # [Tags]             BP-1200
    # Mo Quan ly file
    # Cho phep cap quyen cho ung dung
    # Sleep    2    
    # Chon thu muc anh tu quan ly file
    # Thay doi ten album tu Quan ly file
    # Log To Console    Kiem tra ten album vua thay doi    
    # Close All Applications
    # Mo Bo Suu Tap
    # Cho phep cap quyen cho ung dung
    # Mo giao dien Album
    # Page Should Contain Element    ${album_camera3}   
    # Close All Applications

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================