*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/Goi_dien_goi_di.robot
Resource    ../page/btalk/dien_thoai.robot
Resource    ../page/btalk/btalk_common.robot

Suite Setup    Mo Launcher

*** Test Cases ***
BP-3389
    [Documentation]    Check goi di tu giao dien Tab dien thoai
    [Tags]     High    BP-3389    1sim
    Log To Console     Check goi di tu giao dien Tab dien thoai
    Mo Dien thoai tu Launcher
    Bam sdt test
    ${get_text}=  AppiumLibrary.Get Text    ${dt_man_hinh_hien_thi_nhap_sdt}
    Bam nut quay so
    Page Should Contain Element    ${dt_tab_dien_thoai_loa}
    ${get_text_1}=  AppiumLibrary.Get Text    ${dt_man_hinh_hien_thi_so_da_luu_khi_goi}
    Should Be Equal    ${get_text}    ${get_text_1}
    Goi_dien_goi_di.Bam nut ket thuc cuoc goi

BP-3390
    [Documentation]    Check goi di tu giao dien Tab gan day
    [Tags]     High    BP-3390    BP-3034    1sim
    Log To Console    Check goi di tu giao dien Tab gan day
    Mo Btalk
    Bam sdt test
    Bam nut quay so
    Goi_dien_goi_di.Bam nut ket thuc cuoc goi
    Mo Btalk roi mo Gan day
    ${get_text}=  AppiumLibrary.Get Text    ${dt_tab_gan_day_lien_he1}
    Sleep    2
    Bam vao lien he dau tien trong tab gan day
    Page Should Contain Element    ${dt_tab_dien_thoai_loa}
    ${get_text_1}=  AppiumLibrary.Get Text    ${dt_man_hinh_hien_thi_so_da_luu_khi_goi}
    Should Be Equal    ${get_text}    ${get_text_1}
    Close Application

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================