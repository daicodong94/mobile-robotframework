*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/bo_suu_tap/album.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/bo_suu_tap/album.robot
Resource    ../page/bo_suu_tap/thumbnail.robot
Resource    ../page/bo_suu_tap/thumbnail.robot

Suite Setup       Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-1244
    [Documentation]    Mo bo suu tap tu Launcher
    [Tags]     High    BP-1244
    Mo Bo suu tap tu Launcher
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-1245
    [Documentation]    Cham vao tab Dong thoi gian, Album, Video, Thung rac
    [Tags]     High    BP-1245
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Log To Console    Mo giao dien Album    
    Mo giao dien Album
    Log To Console    Mo giao dien Dong thoi gian    
    Mo giao dien Dong thoi gian
    Log To Console    Mo giao dien Quay phim
    Mo giao dien Quay phim
    Log To Console    Mo giao dien Thung rac
    Mo giao dien Thung rac
    Close All Applications

BP-1262
    [Documentation]    Check hien thi danh cac app ho tro khi cham vao icon chia se
    [Tags]             High    BP-1262         
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon giu 1 anh trong Dong thoi gian
    Nhan chon chia se
    Sleep    5    
    Vuot mo rong man chia se tu duoi len tren
    Vuot thu nho man chia se tu tren xuong

BP-1246
    [Documentation]    Vuot chuyen qua cac tab Dong thoi gian, Album, Video, Thung rac
    [Tags]           BP-1246
    Close All Applications
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Log To Console     Vuot sang giao dien Album    
    Vuot man tu phai qua trai
    Page Should Contain Element    ${bst_album_nut_them_moi}
    Sleep    2    
    Log To Console     Vuot sang giao Quay phim
    Vuot man tu phai qua trai
    Page Should Not Contain Element    ${bst_album_nut_them_moi} 
    Sleep    2    
    Log To Console    Vuot sang giao dien Thung rac   
    Vuot man tu phai qua trai
    Page Should Contain Element    ${bst_album_thung_rac_mess}
    Close All Applications
    
BP-1248
    [Documentation]    Check hien thi chuc nang tren thanh edit tabbar khi chon mot anh
    [Tags]             BP-1248
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon giu 1 anh trong Dong thoi gian
    Log To Console    Hien thi chuc nang Chia se
    Page Should Contain Element    ${bst_TN_nut_chia_se}
    Log To Console    Hien thi chuc nang Them vao album
    Page Should Contain Element    ${bst_TN_nut_them_vao_album}
    Log To Console    Hien thi chuc nang Xoa
    Page Should Contain Element    ${bst_TN_nut_xoa}
    Log To Console    Hien thi chuc nang Dat lam            
    Page Should Contain Element    ${bst_TN_nut_dat_lam} 
    Close All Applications
    
BP-1256
    [Documentation]    Check hien thi chuc nang tren thanh edit tabbar khi chon nhieu anh
    [Tags]             BP-1256
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon giu 1 anh trong Dong thoi gian
    Chon nhieu anh trong Dong thoi gian
    Page Should Contain Element    ${bst_TN_nut_chia_se}  
    Log To Console    Hien thi chuc nang Them vao album
    Page Should Contain Element    ${bst_TN_nut_them_vao_album}
    Log To Console    Hien thi chuc nang Xoa
    Page Should Contain Element    ${bst_TN_nut_xoa}
    Sleep    1    
 
BP-1258
    [Documentation]    Check bo chon tat ca anh khi thuc hien vuot back
    [Tags]             BP-1258
    Vuot man tu trai sang phai
    Log To Console    Tro ve man Dong thoi gian    
    Page Should Contain Element    ${bst_TN_nut_ngay}
    Close All Applications
    
BP-1259
    [Documentation]    Check bo chon tat ca anh khi tick bo chon tren thanh toolbar
    [Tags]             BP-1259
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon giu 1 anh trong Dong thoi gian
    Chon tat ca anh tren thanh toolbat    
    Bo chon tat ca anh tren thanh toolbar
    Sleep    1    
    Log To Console    Tro ve man Dong thoi gian   
    Page Should Not Contain Element    ${bst_TN_toolbar_tick _chon_tat_ca} 
    Page Should Not Contain Element    ${bst_TN_nut_chia_se}
    Close All Applications
    
BP-1260
    [Documentation]    Check bo chon tat ca khi cham vao icon X tren thanh toolbar
    [Tags]             BP-1260    BP-1261
    Log To Console    Check cap nhat bo dem anh khi chon them anh tu thanh toolbar    
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon giu 1 anh trong Dong thoi gian
    Page Should Contain Element    ${bst_TN_chon_1_anh}
    Sleep    2    
    Chon nhieu anh trong Dong thoi gian
    Page Should Contain Element   ${bst_TN_chon_4_anh}
    Log To Console    Check cap nhat bo dem anh khi bo chon anh tu thanh toolbar  
    Bo chon 1 anh trong Dong thoi gian
    Page Should Contain Element    ${bst_TN_anh_3}
    Log To Console    Check nut [X] bo chon toan bo anh       
    Chon tat ca anh tren thanh toolbat
    Sleep    1 
    Bo chon tat ca anh bang icon [X]
    Log To Console    Tro ve man Dong thoi gian   
    Page Should Contain Element    ${bst_TN_nut_ngay}
    Page Should Not Contain Element    ${bst_TN_toolbar_tick _chon_tat_ca}
    Page Should Not Contain Element    ${bst_TN_nut_chia_se} 
    Close All Applications 
    
BP-1263
    [Documentation]    Check mo rong/thu nho man danh sach cac app chia se khi vuot
    [Tags]             BP-1263    BP-1264
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon giu 1 anh trong Dong thoi gian
    Nhan chon chia se
    Vuot mo rong man chia se tu duoi len tren
    Vuot thu nho man chia se tu tren xuong
    Log To Console    Check dong cac app trong man chia se khi bam vao khoang trong        
    Nhan chon vao khoang trong
    Close All Applications
           
BP-1271
    [Documentation]    Check chuc nang them vao album khi cham Them vao album tren edit tabbar
    [Tags]             BP-1271    BP-1272
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon giu 1 anh trong Dong thoi gian
    Nhan chon Them vao album trong Dong thoi gian
    Sleep    3    
    Log To Console    Check quay lai thumbnail khi cham vao nut Huy tren thanh toolbar    
    Nhan chon Huy tren giao dien them vao album

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================