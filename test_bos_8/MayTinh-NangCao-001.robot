*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library    AppiumLibrary

Resource    ../page/may_tinh/mt_common.robot
Resource    ../page/may_tinh/nang_cao.robot
Resource    ../page/common.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-559
    [Documentation]    Check man hinh hien thi man hinh giao dien nang cao
    [Tags]    BP-559    BP-1115    BP-1116
    Log To Console    Check man hinh hien thi man hinh giao dien nang cao
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    AppiumLibrary.Page Should Contain Element   ${mt_nut_nang_cao}
    Sleep    3
    An nut man hinh nang cao
    AppiumLibrary.Page Should Contain Element    ${mt_nut_bang}
    Sleep    3
    Vuot ra giao dien nang cao
    Sleep    2
    Vuot an giao dien nang cao

BP-977
    [Documentation]    Check chuc nang An nut cos
    [Tags]    BP-977
    Log To Console    Check chuc nang An nut cos
    Sleep    2
    An nut man hinh nang cao
    An nut cos
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut so 5
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0**28366219*
    Sleep    2
    Close Application
    Sleep    2

BP-561
    [Documentation]    Kiem tra giao dien nang cao cua ung dung May tinh
    [Tags]    BP-561    BP-562    BP-563    BP-564    BP-565    BP-566    BP-567    BP-568
    ...    BP-575    BP-587
    Mo May tinh roi mo giao dien nang cao
    Chup anh man hinh va so sanh voi anh mau    ${mt_alias}    kc_d_ttdh_d_d_nang_cao    3
    Log To Console    Check hien thi cac phep tinh nang cao
    Thuc hien cac phep tinh sin
    Close Application

BP-967
    [Documentation]    Check chuc nang (sin)
    [Tags]    BP-967
    Log To Console    Check chuc nang (sin)
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut sin
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut so 5
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *−0**9589242*
    Sleep    2
    Close Application

BP-986
    [Documentation]    Check chuc nang An nut tan
    [Tags]    BP-986
    Log To Console    Check chuc nang An nut tan
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut tan
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut so 5
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *−3**3805150*
    Close Application

BP-993
    [Documentation]    Check chuc nang An nut In
    [Tags]    BP-993     BP-997
    Log To Console    Check chuc nang An nut In
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut In
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut so 5
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *1**60943791*
    Close Application

BP-1002
    [Documentation]    Check chuc nang An nut log
    [Tags]    BP-1002    BP-1003
    Log To Console    Check chuc nang An nut log
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut log
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut so 5
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0**69897000*


BP-1006
    [Documentation]    Check chuc nang An nut cham than (!)
    [Tags]    BP-1006
    Log To Console    Check chuc nang An nut cham than (!)
    Sleep    2
    Xoa man hinh
    An nut man hinh nang cao
    Sleep    2
    An nut dau cham than
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Close Application

BP-1007
    [Documentation]    Check chuc nang An nut Pi (π)
    [Tags]    BP-1007
    Log To Console    Check chuc nang An nut Pi (π)
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut Pi
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *3**14159265*
    Close Application
BP-1008
    [Documentation]    Check chuc nang An nut (e)
    [Tags]    BP-1008
    Log To Console    Check chuc nang An nut (e)
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut e
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *2**71828183*
    Close Application
    Sleep    2
BP-1009
    [Documentation]    Check chuc nang An nut luy thua (^)
    [Tags]    BP-1009
    Log To Console    Check chuc nang An nut luy thua (^)
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut luy thua
    Sleep    2
    AppiumLibrary.Element Should Be Enabled    ${mt_nut_luy_thua}
    Close Application
BP-1010
    [Documentation]    Check chuc nang An nut dau ngoac trai
    [Tags]    BP-1010
    Log To Console    Check chuc nang An nut dau ngoac trai
    Mo May tinh
    An nut man hinh nang cao
    Sleep    2
    An nut dau ngoac trai
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    (
    Sleep    2
    Close Application

BP-1011
    [Documentation]    Check chuc nang An nut dau ngoac phai
    [Tags]    BP-1011     BP-1012
    Log To Console    Check chuc nang An nut dau ngoac phai
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    An nut dau ngoac phai
    AppiumLibrary.Element Should Be Enabled    ${mt_nut_ngoac_phai}
    ${get_text1}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal   ${get_text1}    )
    Sleep    2
    An nut khai can
    AppiumLibrary.Element Should Be Enabled    ${mt_nut_can_bac_2}
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal   ${get_text}    )√
    Sleep    2
    Close Application

BP-1013
    [Documentation]    Check chuc nang hoat dong cua khai can
    [Tags]    BP-1013
    Log To Console    Check chuc nang hoat dong cua khai can
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut khai can
    Sleep    2
    An nut man hinh nang cao
    An nut so 4
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Should Be Equal   ${get_text}    2
    Close Application

BP-1014
    [Documentation]    Check chuc nang dau %
    [Tags]    BP-1014    BP-1015
    Log To Console    Check chuc nang dau % - chua nhap so
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut phan tram
    ${get_text1}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *%*
    Sleep    2
    Xoa man hinh
    Sleep    2
    An nut so 2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut phan tram
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0**02*
    Close Application

BP-1016
    [Documentation]    Check phep tinh m(-)
    [Tags]    BP-1016
    Log To Console    Check phep tinh m(-)
    Mo May tinh
    Sleep    2
    An phep tinh nhan 2x2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut m tru
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An phep tinh nhan 3x3
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut m tru
    Sleep    2
    An nut mr
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    −13
    Close Application

BP-1017
    [Documentation]    Check phep tinh m(+)
    [Tags]    BP-1017
    Log To Console    Check phep tinh m(+)
    Mo May tinh
    Sleep    2
    An phep tinh nhan 2x2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut m cong
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An phep tinh nhan 3x3
    An nut man hinh nang cao
    Sleep    2
    An nut m cong
    An nut mr
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    13
    Close Application

BP-1019
    [Documentation]    Check hien thi ket qua phep tinh m- dang luu trong bo nho
    [Tags]    BP-1019
    Log To Console    Check hien thi ket qua phep tinh m- dang luu trong bo nho
    Mo May tinh
    Sleep    2
    An phep tinh nhan 2x2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut m tru
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut Clear xoa ket qua
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut mr
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    −4
    Close Application

BP-1022
    [Documentation]    Check xoa ket qua phep tinh m+ dang luu trong bo nho bang mc
    [Tags]    BP-1022
    Log To Console    Check xoa ket qua phep tinh m+ dang luu trong bo nho bang mc
    Mo May tinh
    Sleep    2
    An phep tinh nhan 2x2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut mc
    Sleep    2
    An nut m cong
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut Clear xoa ket qua
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut mc
    Sleep    2
    An nut mr
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    ${EMPTY}
    Close Application

BP-1024
    [Documentation]    Check xoa ket qua phep tinh m- dang luu trong bo nho bang mc
    [Tags]    BP-1024
    Log To Console    Check xoa ket qua phep tinh m- dang luu trong bo nho bang mc
    Mo May tinh
    Sleep    2
    An phep tinh nhan 2x2
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut m tru
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut Clear xoa ket qua
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut mc
    Sleep    2
    An nut mr
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    ${EMPTY}


#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================