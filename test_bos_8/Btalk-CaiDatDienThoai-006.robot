*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary
   
Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/cai_dat/cd_common.robot

*** Test Cases ***
#======================================================CHUP ANH 2SIM=============================================================
# BP-4919
    # [Documentation]    Check giao dien cai dat dien thoai khi lap 2sim
    # [Tags]    BP-4919
    # Mo dien thoai roi vao cai dat dien thoai
    # Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_cai_dat_dt_2sim    3

# BP-4920
    # [Documentation]    Check giao dien tuy chon hien thi
    # [Tags]    BP-4920
    # Mo dien thoai roi vao cai dat dien thoai
    # An nut tuy chon hien thi
    # Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_tuy_chon_hien_thi    3
    
# BP-4922
    # [Documentation]    Check giao dien am thanh va rung trong TH may lap 2sim
    # [Tags]    BP-4922
    # Mo dien thoai vao cai dat va vao am thanh va rung
    # Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_am_thanh_va_rung_2sim    3
    
# BP-4923
    # [Documentation]    Check giao dien Chinh sua tin nhan tra loi nhanh
    # [Tags]    BP-4923
    # Mo cai dat dien thoai va vao tra loi nhanh
    # Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_chinh_sua_tin_nhan_tra_loi_nhanh    3
    
BP-4925
    [Documentation]    Check giao dien tai khoan goi trong TH may lap 2sim
    [Tags]    BP-4925
    Mo cai dat dien thoai va vao Tai khoan goi
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_tai_khoan_goi_2sim    3
    
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
