*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
ThinhLQ         | 2021/07/23 | update bo comment va thay doi video test, thay doi anh giao dien so sanh
----------------|------------|--------------------------------------------------------------------

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/xem_phim/tuy_chon.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup       Mo Xem phim
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5125
    [Documentation]    Check mo danh sach cac Tuy chon khi cham icon ... canh icon Play/Pause trong giao dien trinh phat video
    [Tags]    BP-5125
    Cho phep cap quyen cho ung dung
    Click vao bo danh sach video1
    Click vao video 1
    Cham vao man hinh video
    Click vao nut play
    Vuot thanh thoi gian ve ban dau
    Click vao tuy chon
    Page Should Contain Element    ${xp_tuy_chon_tre_phu_de}
    Page Should Contain Element    ${xp_tuy_chon_tua_den_vi_tri}
    Page Should Contain Element    ${xp_tuy_chon_so_giay_back}
    Page Should Contain Element    ${xp_text_lap_lai_AB}


BP-5126
    [Documentation]    Check kiem tra man hinh tuy chon
    [Tags]    BP-5126
    Kiem tra giao dien tuy chon
    Go Back
    Go Back
    Click vao video 5
    Cham vao man hinh video
    Click vao nut play
    Vuot thanh thoi gian ve ban dau
    Click vao tuy chon
BP-5231
    [Documentation]    Check Toc do phat mac dinh 1.00x
    [Tags]    BP-5231
    Kiem tra trang thai mac dinh toc do phat la 1_00x
    Kiem tra hien thi progressbar toc do phat
    Click vao tab thoi gian cho

BP-5127
    [Documentation]    Check giao dien dong ho kim khi thiet lap Thoi gian cho
    [Tags]    BP-5127
    Kiem tra giao dien dong ho kim thoi gian cho
    Click vao kieu dong ho

BP-5244
    [Documentation]    Check chuc nang chuyen sang giao dien tao thoi gian cho theo dong ho so
    [Tags]    BP-5244
    Kiem tra trang thai hien thi tai dong ho so

BP-5128
    [Documentation]    Check giao dien dong ho so khi thiet lap Thoi gian cho
    [Tags]    BP-5128
    Kiem tra giao dien dong ho so thoi gian cho

BP-5129
    [Documentation]    Check kiem tra man hinh pop up phu de
    [Tags]    BP-5129
    Go Back
    Cham vao man hinh video
    Click vao tuy chon
    Click vao tuy chon phu de

BP-5250
    [Documentation]    Check hien thi Popup Tuy chon phu de khi cham label Tuy chon phu de
    [Tags]    BP-5250
    Kiem tra giao dien pop up phu de
    Go Back

BP-5222
    [Documentation]    Check kiem tra man hinh pup up tua den vi tri
    [Tags]    BP-5222
    Click vao tua den vi tri
    Kiem tra giao dien pop up tua den vi tri

BP-5262
    [Documentation]    Check hien thi Popup Tua den vi tri khi cham label Tua den vi tri
    [Tags]    BP-5262
    Kiem tra trang thai hien nut dong y
    Go Back
    Go Back

BP-5273
    [Documentation]    Check mac dinh hien thi chuc nang Hien phim Back (10 giay) o man hinh Tuy chon
    [Tags]    BP-5273
    Cham vao man hinh video
    Click vao tuy chon
    Page Should Contain Text    ${xp_tuy_text_so_giay_back}
    Sleep    2
    Click vao so giay muon back

BP-5274
    [Documentation]    Check hien thi Popup Chon thoi gian Back khi cham "10 giay" o label Hien phim Back
    [Tags]    BP-5274
    Kiem tra hien thi mac dinh so giay back
    Kiem tra trang thai hien nut dong y

BP-5224
    [Documentation]    Check giao dien Popup Chon thoi gian Back khi cham "10 giay"
    [Tags]    BP-5224
    Kiem tra giao dien pop up so thoi gian back

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================