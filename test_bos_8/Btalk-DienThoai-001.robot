*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/btalk/dien_thoai.robot
Resource    ../page/btalk/gui_nhan_tin_nhan.robot
Resource    ../page/btalk/nhat_ki_cuoc_goi.robot
Resource    ../page/cai_dat/cd_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup    Mo Launcher

*** Test Cases ***
#luu i cai dat mo ung dung tat
BP-2898
    [Documentation]    Check TH bam vao icon btalk tren hot seat
    [Tags]    BP-2898    High    1sim
    Mo Dien thoai tu Launcher
    An nut cai dat dien thoai
    An nut cai dat mo ung dung
    Element Attribute Should Match         ${dt_cd_checkbox_luu_vi_tri_tab_khi_thoat_ung_dung}   checked    	false
    Sleep    2
    Vuot quay ve man hinh Home
    Mo Btalk
    Page Should Contain Element    ${dt_nut_quay_so}
    Close Application

BP-2945
    [Documentation]    Check mo giao dien goi di
    [Tags]    BP-2945     High    1sim
    Mo Btalk
    Bam sdt test
    ${get_text}=  AppiumLibrary.Get Text    ${dt_man_hinh_hien_thi_nhap_sdt}
    Bam nut quay so
    Page Should Contain Element    ${dt_tab_dien_thoai_loa}
    ${get_text_1}=  AppiumLibrary.Get Text    ${dt_man_hinh_hien_thi_so_da_luu_khi_goi}
    Should Be Equal    ${get_text}    ${get_text_1}
    Close Application

BP-2894
    [Documentation]    Check TH mo tu giao dien tin nhan
    [Tags]    BP-2894     1sim
	Mo Btalk vao tin nhan
	An chon man dien thoai tren giao dien chinh btalk
    Page Should Contain Element    ${dt_nut_quay_so}
    Vuot quay ve man hinh Home

BP-2895
    [Documentation]    Check TH mo tu giao dien Tin nhan bang thao tac vuot
    [Tags]    BP-2895    1sim
    Mo Btalk vao tin nhan
    Vuot sang man hinh ben trai
    Page Should Contain Element    ${dt_nut_quay_so}
    Vuot quay ve man hinh Home

BP-2896
    [Documentation]    Check TH mo tu giao dien gan day
    [Tags]    BP-2896    1sim
    Bam mo app dien thoai vao gan day
    An chon man dien thoai tren giao dien chinh btalk
    Page Should Contain Element    ${dt_nut_quay_so}
    Vuot quay ve man hinh Home

BP-2897
    [Documentation]    Check TH mo tu giao dien danh ba
    [Tags]    BP-2897    1sim
    Mo Danh ba tu Launcher
    # An nut ngung tim kiem danh ba
    An chon man dien thoai tren giao dien chinh btalk
    Page Should Contain Element    ${dt_nut_quay_so}
    Sleep    2

BP-2914
    [Documentation]    Check mo giao dien Them vao lien he khi cham vao icon Them lien he
    [Tags]    BP-2914    1sim
    Mo Btalk
    Bam sdt 0977888888
    Bam nut them lien he moi
    Page Should Contain Text    Thêm vào liên hệ
    Page Should Contain Element    ${dt_tab_them_lien_he_nut_tao_lien_he_moi}


BP-2924
    [Documentation]    Check hien thi phan hien thi sdt khi bam icon xoa 1 lan
    [Tags]    BP-2924        BP-2926    1sim
    Mo Btalk
    Bam sdt test
    Element Attribute Should Match    ${dt_man_hinh_hien_thi_nhap_sdt}    text    0823 390 409
    An vao icon xoa 01 lan
    Element Attribute Should Match    ${dt_man_hinh_hien_thi_nhap_sdt}    text    0823 390 40
    Log To Console    Check hien thi khi bam va giu icon xoa
    Long Press    ${dt_icon_xoa_khi_nhap_so}
    ${get_text}=  AppiumLibrary.Get Text   ${dt_man_hinh_hien_thi_nhap_sdt}
    Should Be Equal    ${EMPTY}     ${get_text}
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-2925
    [Documentation]    Check hien thi phan hien thi SDT khi bam icon Xoa den khi het
    [Tags]     BP-2925    1sim
    Mo Btalk
    Bam sdt test
    Element Attribute Should Match    ${dt_man_hinh_hien_thi_nhap_sdt}    text    0823 390 409
    An vao icon xoa 01 lan
    An vao icon xoa 01 lan
    An vao icon xoa 01 lan
    An vao icon xoa 01 lan
    An vao icon xoa 01 lan
    An vao icon xoa 01 lan
    An vao icon xoa 01 lan
    An vao icon xoa 01 lan
    An vao icon xoa 01 lan
    An vao icon xoa 01 lan
    ${get_text}=  AppiumLibrary.Get Text   ${dt_man_hinh_hien_thi_nhap_sdt}
    Should Be Equal    ${EMPTY}     ${get_text}

BP-2927
    [Documentation]    Check TH dan SDT
    [Tags]    BP-2927    1sim
    Mo Btalk roi mo Gan day
    ${get_text}=     AppiumLibrary.Get Text   ${dt_gd_sdt1}
    Sao chep sdt dau tien o tab gan day
    Bam tab dien thoai
    Long Press    ${dt_man_hinh_hien_thi_nhap_sdt}
    Sleep    2
    ${get_text2}=   AppiumLibrary.Get Text   ${dt_man_hinh_hien_thi_nhap_sdt}
    Should Be Equal     ${get_text}   ${get_text2}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================