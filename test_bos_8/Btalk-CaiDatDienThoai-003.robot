*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/cai_dat/cd_common.robot

*** Test Cases ***
#======================================================case chup anh 1sim viettel khay sim1=====================================================================
BP-4918
    [Documentation]    Check giao dien cai dat dien thoai TH  may khong lap sim hoac lap 1sim
    [Tags]    BP-4918    1sim
    Mo dien thoai roi vao cai dat dien thoai
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_cai_dat_dien_thoai   3

BP-4921
    [Documentation]    Check giao dien Am thanh va rung trong TH may lap 1sim
    [Tags]    BP-4921     1sim
    Mo dien thoai vao cai dat va vao am thanh va rung
    Chup anh man hinh va so sanh voi anh mau     ${btalk_alias}   kc_d_ttdh_d_d_am_thanh_va_rung    3

BP-4924
    [Documentation]    Check giao dien tai khoan goi trong truong hop may lap 1sim
    [Tags]    BP-4924
    Mo cai dat dien thoai va vao Tai khoan goi
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}       kc_d_ttdh_d_d_tai_khoan_goi    3

BP-4927
    [Documentation]    Check giao dien popup Thoi gian tu dong xoa ghi am cuoc goi
    [Tags]    BP-4927
    Mo cai dat dien thoai va vao Tai khoan goi
    An nut Thoi gian tu dong xoa ghi am cuoc goi
    Capture Page Screenshot    anh.png
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_popup_xoa_ghi_am_cuoc_goi    3

BP-4926
    [Documentation]    Check giao dien hen gio cuoc goi
    [Tags]    BP-4926    1sim
    Mo cai dat dien thoai va vao Tai khoan goi
    An nut hen gio cuoc goi
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_hen_gio_goi    3

BP-4930
    [Documentation]    Check giao dien Cai dat mo ung dung
    [Tags]    BP-4930
    Mo dien thoai roi vao cai dat dien thoai
    An nut cai dat mo ung dung
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_cai_dat_mo_ung_dung    3

BP-4931
    [Documentation]    Check giao dien Cai dat quay so nhanh
    [Tags]    BP-4931
    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_cai_dat_quay_so_nhanh    4

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================