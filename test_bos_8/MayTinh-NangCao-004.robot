*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/may_tinh/mt_common.robot
Resource    ../page/may_tinh/nang_cao.robot

# Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-1085
    [Documentation]    Check chuc nang RAD voi phep tinh nhan ki hieu sin
    [Tags]    BP-1085
    Log To Console    Check chuc nang RAD voi phep tinh nhan ki hieu sin
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut RAD
    Sleep    2
    Thuc hien va kiem tra phep tinh nhan sin
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0**02447174*
    Close Application


BP-1091
    [Documentation]    Check chuc nang RAD voi phep tinh nhan ki hieu tan
    [Tags]    BP-1091
    Log To Console    Check chuc nang RAD voi phep tinh nhan ki hieu tan
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut RAD
    Sleep    2
    Thuc hien va kiem tra phep tinh nhan tan
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0**02508563*
    Close Application

BP-1082
    [Documentation]    Check chuc nang DEG voi phep tinh nhan ki hieu cos
    [Tags]    BP-1082
    Log To Console     Check chuc nang DEG voi phep tinh nhan ki hieu cos
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh nhan cos
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0**83015835*
    Close Application

BP-1094
    [Documentation]    Check phep tinh nhan ki hieu ln
    [Tags]    BP-1094
    Log To Console    Check phep tinh nhan ki hieu ln
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh nhan ln
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text   *4**82779584*
    Close Application

BP-1075
    [Documentation]    Check chuc nang DEG voi phep tinh nhan ki hieu sin
    [Tags]    BP-1075
    Log To Console    Check chuc nang DEG voi phep tinh nhan ki hieu sin
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh nhan sin
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0**16984165*
    Close Application

BP-1087
    [Documentation]    Check chuc nang RAD voi phep tinh nhan ki hieu cos
    [Tags]    BP-1087
    Log To Console    Check chuc nang RAD voi phep tinh nhan ki hieu cos
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut RAD
    Sleep    2
    Thuc hien va kiem tra phep tinh nhan cos
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0**97552826*
    Close Application

BP-1095
    [Documentation]    Check phep tinh nhan ki hieu log
    [Tags]    BP-1095
    Log To Console    Check phep tinh nhan ki hieu log
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh nhan log
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text   *0**91057877*
    Close Application

BP-1083
    [Documentation]    Check chuc nang DEG voi phep tinh nhan ki hieu tan
    [Tags]    BP-1083
    Log To Console    Check chuc nang DEG voi phep tinh nhan ki hieu tan
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh nhan tan
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0**20458946*
    Close Application

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================