*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
ThinhLQ         | 2021/07/23 | update code , tach nho case rieng biet
----------------|------------|--------------------------------------------------------------------

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/nghe_dai/yeu_thich.robot

Suite Setup       Mo Nghe dai
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-1598
    [Documentation]    Mo Nghe dai
    [Tags]     High    BP-1598
    Cho phep cap quyen cho ung dung
    Sleep    5
    Xac nhan mo app Nghe dai thanh cong

BP-1812
    [Documentation]    Check chuc nang phat kenh khong co mang
    [Tags]    BP-1812
    Mang di dong = 1, data = 0, wifi = 0, may bay = 0
    Page Should Not Contain Element    ${nd_yt_txt_dang_phat}

BP-1814
    [Documentation]    Check chuc nang phat kenh co mang
    [Tags]     High    BP-1814
    Mang di dong = 1, data = 1, wifi = 1, may bay = 0
    Vuot quay ve man hinh Home
    Mo Nghe dai tu Launcher
    Kiem tra nut play co hoat dong khi bat dau mo app
    Cho den khi hien thi text dang phat
    Thuc hien stop kenh dang chay
    Sleep    3

BP-1816
    [Documentation]    Kiem tra hien thi man hinh yeu thich mac dinh
    [Tags]    BP-1816
    Thuc hien kiem tra trang thai khong o tab man hinh tat ca kenh
    Page Should Not Contain Element    ${nd_tctl_btn_tat_cac_the_loai}
    Page Should Contain Element    ${nd_tab_yeu_thich}
    Sleep    3

BP-1823
    [Documentation]    Kiem tra danh sach cac kenh yeu thich mac dinh
    [Tags]    High    BP-1823
    Thuc hien kiem tra danh sach hien thi tai man hinh yeu thich
    Sleep    3



BP-1599
    [Documentation]    Kiem tra giao dien Yeu thich
    [Tags]    BP-1599    BP-1600    BP-1601    BP-1602    BP-1603    BP-1604    BP-1605    BP-1606    BP-1607    BP-1608
    ...    BP-1609    BP-1610    BP-1611    BP-1612    BP-1614    BP-1615    BP-1618    BP-1620    BP-1621    BP-1622    BP-1623
    ...    BP-1624    BP-1625    BP-1722    BP-1723    BP-1725    BP-1726    BP-1729    BP-1731    BP-1733    BP-1734    BP-1735
    ...    BP-1736    BP-1737    BP-1738    BP-1739    BP-1740    BP-1741

    Kiem tra nut play co hoat dong khi bat dau mo app
    Chup anh man hinh va so sanh voi anh mau    ${nd_alias}    kc_d_ttdh_d_d_MacDinh   3

BP-1820
    [Documentation]    Check chuc nang cap quyen mo app vuot ve home
    [Tags]     BP-1820
    Sleep    3
    Thuc hien click chon tab tat ca kenh
    Sleep    3
    AppiumLibrary.Go Back
    Sleep    3
    Mo Nghe dai tu Launcher
    Page Should Contain Element    ${nd_tab_yeu_thich}
    Page Should Not Contain Element    ${nd_tctl_btn_tat_cac_the_loai}
    Thuc hien click chon tab tat ca kenh

BP-1821
    [Documentation]    Check chuc nang cap quyen mo app vuot ve home
    [Tags]    BP-1821
    Vuot quay ve man hinh Home
    Thuc hien kiem tra da an app nghe dai tro ve home
    Mo Nghe dai tu Launcher
    Page Should Contain Element    ${nd_tctl_txt_chon_tat_ca}



#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================