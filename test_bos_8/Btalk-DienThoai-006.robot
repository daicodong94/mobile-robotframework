*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/dien_thoai.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/btalk/nhat_ki_cuoc_goi.robot
Resource    ../page/btalk/gui_nhan_tin_nhan.robot
Resource    ../page/cai_dat/cd_common.robot

*** Test Cases ***
# LƯU Ý KHỞI ĐỘNG Lại  ĐIỆN THOẠI TRƯỚC KHI CHẠY. Mỗi lần chỉ chạy được 1 case .
# Khi sang case tiếp theo lại khởi động lại máy rồi mới tiến hành chạy
BP-2928
    [Documentation]    Check so tong dai co dinh
    [Tags]    BP-2928    BP-2929
    Mo Btalk
    input 900
    Page Should Contain Element    ${dt_lien_he_tong_dai}
    Nhan giu icon xoa
    Input 199
    Page Should Contain Element    ${dt_lien_he_tong_dai}
    Nhan giu icon xoa
    Input 121
    Page Should Contain Element    ${dt_lien_he_tong_dai}
    Nhan giu icon xoa
    Input 1080
    Page Should Contain Element    ${dt_lien_he_tong_dai}
    Nhan giu icon xoa
    Input 197
    Page Should Contain Element    ${dt_lien_he_Viettel}
    Nhan giu icon xoa
    Input 198
    Page Should Contain Element    ${dt_lien_he_Viettel}
    Nhan giu icon xoa
    Input 1222
    Page Should Contain Element    ${dt_lien_he_Viettel}
    Nhan giu icon xoa
    Input 1789
    Page Should Contain Element    ${dt_lien_he_Viettel}
    Nhan giu icon xoa
    Input 9189
    Page Should Contain Element    ${dt_lien_he_Viettel}
    Nhan giu icon xoa
    Input 9198
    Page Should Contain Element    ${dt_lien_he_Viettel}
    Nhan giu icon xoa
    AppiumLibrary.Close All Applications

BP-2930
    [Documentation]    Check hien thi lien he mobiphone
    [Tags]    BP-2930    BP-2931    BP-2932    BP-2933    BP-2934    BP-2935    BP-2936
    Mo Btalk
    Input 9090
    Page Should Contain Element    ${dt_lien_he_mobiphone}
    Nhan giu icon xoa
    Input 9393
    Page Should Contain Element    ${dt_lien_he_mobiphone}
    Nhan giu icon xoa
    Input 9191
    Page Should Contain Element    ${dt_lien_he_vinaphone}
    Nhan giu icon xoa
    Input 9192
    Page Should Contain Element    ${dt_lien_he_vinaphone}
    Nhan giu icon xoa
    Input 888
    Page Should Contain Element    ${dt_lien_he_vinaphone}
    Nhan giu icon xoa
    Input 789
    Page Should Contain Element    ${dt_lien_he_vietnamobile}
    Nhan giu icon xoa
    Input 123
    Page Should Contain Element    ${dt_lien_he_vietnamobile}
    Nhan giu icon xoa
    Input 360
    Page Should Contain Element    ${dt_lien_he_vietnamobile}
    Nhan giu icon xoa
    Input 3636
    Page Should Contain Element    ${dt_lien_he_vietnamobile}
    Nhan giu icon xoa
    Input 366
    Page Should Contain Element    ${dt_lien_he_vietnamobile}
    Nhan giu icon xoa
    Input 112
    Page Should Contain Element    ${dt_lien_he_cuu_nan}
    Nhan giu icon xoa
    Input 113
    Page Should Contain Element    ${dt_lien_he_canh_sat}
    Nhan giu icon xoa
    Input 114
    Page Should Contain Element    ${dt_lien_he_cuu_hoa}
    Nhan giu icon xoa
    Input 115
    Page Should Contain Element    ${dt_lien_he_cuu_thuong}
    AppiumLibrary.Close All Applications

BP-2975
    [Documentation]    Check mo giao dien goi toi lien he dau tien trong danh sach ket qua tim kiem
    [Tags]    BP-2975    BP-2976    BP-2918     1sim    BP-2921
    Mo Btalk
    Bam so 2
    Page Should Contain Element    ${dt_tab_dt_mui_ten_mo_rong_thu_gon}
    ${get_text}=    AppiumLibrary.Get Text    ${dt_tab_dt_ten_lien_he1_trong_danh_sach_tim_kiem}
    An nut quay so
    ${get_text_2}=    AppiumLibrary.Get Text    ${dt_man_hinh_hien_thi_ten_lien_he_da_luu_khi_goi}
    Should Be Equal    ${get_text}    ${get_text_2}
    Sleep    2
    Bam nut ket thuc cuoc goi
    Log To Console    Check mo giao dien goi di khi cham vao lien lac bat ki
    Bam so 2
    ${get_text}=    AppiumLibrary.Get Text    ${dt_tab_dt_ten_lien_he1_trong_danh_sach_tim_kiem}
    Bam vao sdt dau tien trong danh sach tim kiem
    ${get_text_2}=    AppiumLibrary.Get Text    ${dt_man_hinh_hien_thi_ten_lien_he_da_luu_khi_goi}
    Bam nut ket thuc cuoc goi
    Bam so 2
    An mui ten mo rong thu gon
    Page Should Not Contain Element    ${dt_nut_quay_so}
    An mui ten mo rong thu gon
    Page Should Contain Element    ${dt_nut_quay_so}
    Element Attribute Should Match    ${dt_man_hinh_hien_thi_nhap_sdt}    text    1
    AppiumLibrary.Close All Applications

# Khoi dong lai may lap them 1sim
BP-2978
    [Documentation]    Check mo popup lua chon sim khi cham nut goi trong TH may lap 2 sim khi o danh sach tim kiem
    [Tags]    BP-2978    BP-2979    BP-2986     BP-2991   2sim
    Mo Btalk
    Bam so 2
    An nut quay so
    Page Should Contain Element    ${dt_popup_goi_bang_checkbox_luon_sd_sim_nay_de_goi}
    Go Back
    Log To Console   check mo popup chon sim khi cham vao lien he
    Bam vao lien he dau tien trong tab gan day
    Page Should Contain Element    ${dt_popup_goi_bang_checkbox_luon_sd_sim_nay_de_goi}
    Log To Console     check mo popup lua chon sim khi cham vao goi bang sim kha
    Go Back
    Long Press    ${dt_tab_dt_ten_lien_he1_trong_danh_sach_tim_kiem}
    Bam nut goi bang sim khac trong danh sach tim kiem
    Page Should Contain Element        ${dt_popup_goi_bang_checkbox_luon_sd_sim_nay_de_goi}
    Go Back
    Log To Console    Check nut chia se
    Long Press    ${dt_tab_dt_ten_lien_he1_trong_danh_sach_tim_kiem}
    Bam nut chia se trong danh sach tim kiem
    Page Should Contain Text    Chia sẻ
    AppiumLibrary.Close All Applications

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================