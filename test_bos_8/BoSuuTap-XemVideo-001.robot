*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/bo_suu_tap/album.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/bo_suu_tap/xem_video.robot

Suite Setup       Mo Launcher

*** Test Cases ***
BP-1351
    [Documentation]    Mo bo suu tap tu Launcher
    [Tags]             High    BP-1351
    Mo Bo suu tap tu Launcher
    Close All Applications
    Sleep    2    

BP-1352
    [Documentation]    Cham vao tab Dong thoi gian, Album, Video, Thung rac
    [Tags]             High    BP-1352
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Log To Console    Mo giao dien Album    
    Mo giao dien Album
    Log To Console    Mo giao dien Dong thoi gian    
    Mo giao dien Dong thoi gian
    Log To Console    Mo giao dien Quay phim    
    Mo giao dien Quay phim
    Log To Console    Mo giao dien Thung rac    
    Mo giao dien Thung rac
    Sleep    2    
    Close All Applications

BP-1353
    [Documentation]    Vuot chuyen qua cac tab Dong thoi gian, Album, Video, Thung rac
    [Tags]             High    BP-1353
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Log To Console     Vuot sang giao dien Album    
    Vuot man tu phai qua trai
    Page Should Contain Element    ${bst_album_nut_them_moi}
    Sleep    2    
    Log To Console     Vuot sang giao Quay phim
    Vuot man tu phai qua trai
    Page Should Not Contain Element    ${bst_album_nut_them_moi} 
    Sleep    2    
    Log To Console    Vuot sang giao dien Thung rac   
    Vuot man tu phai qua trai
    Page Should Contain Element    ${bst_album_thung_rac_mess}
    Close All Applications
    
BP-1363
    [Documentation]    Check tinh nang phat video khi cham vao icon Play tren video thuong
    [Tags]             High    BP-1363         
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Sleep    2    
    Mo giao dien Quay phim
    Chon 1 video tren man Quay Phim
    Sleep    2    
    Cham vao nut Play tren man hinh
    Sleep    2    
    Cham vao khoang trong
    Page Should Contain Element    ${bst_video_thuong_nut_pause}
    Page Should Contain Element    ${bst_video_thuong_time_bar}
    Sleep    5    
    Go Back    
    Close All Applications    
    
BP-1382
    [Documentation]    Check phat video quay cham khi cham vao icon Play tren preview
    [Tags]             High    BP-1382            
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Mo giao dien Quay phim
    Sleep    2    
    Chon video quay cham - video thu 2
    Sleep    2    
    Cham nut play tren video quay cham
    Sleep    2    
    Cham vao khoang trong
    Log To Console    Hien thi vung dieu khien video, hien thi thnah dieu chinh thoi gian lam cham    
    Page Should Contain Element    ${bst_video_cham_thanh_thoi_gian}
    Page Should Contain Element    ${bst_video_cham_time_bar}    
    Page Should Contain Element    ${bst_video_cham_nut_chup_man_hinh}    
    Page Should Contain Element    ${bst_video_cham_nut_pause}    
    Page Should Contain Element    ${bst_video_cham_nut_vong_lap}    
    Sleep    2    
    Close All Applications

# BP-1355
    # [Documentation]    Check hien thi toolbar, edit tabbar khi cham video 1 lan
    # [Tags]             BP-1355
    # Mo Bo Suu Tap
    # Cho phep cap quyen cho ung dung
    # Mo giao dien Quay phim
    # Chon 1 video tren man Quay Phim
    # Sleep    2
    # Cham vao khoang trong
    # Page Should Contain Element    ${bst_video_nut_thong_tin}
    # Page Should Contain Element    ${bst_video_nut_chia_se}
    # Page Should Contain Element    ${bst_video_nut_cat}
    # Page Should Contain Element    ${bst_video_thung_rac}
    # Sleep    2
    # Close All Applications
    
# BP-1356
    # [Documentation]    Check an toolbar, edit tabbar khi cham video 2 lan
    # [Tags]             BP-1356    BP-1362
    # Mo Bo Suu Tap
    # Cho phep cap quyen cho ung dung
    # Mo giao dien Quay phim
    # Chon 1 video tren man Quay Phim
    # Cham vao khoang trong
    # Cham vao khoang trong
    # Sleep    2
    # Page Should Not Contain Element    ${bst_video_nut_thong_tin}
    # Page Should Not Contain Element    ${bst_video_nut_chia_se}
    # Page Should Not Contain Element    ${bst_video_nut_cat}
    # Page Should Not Contain Element    ${bst_video_thung_rac}
    # Close All Applications
    
# BP-1357
    # [Documentation]    Check hien thi giao dien cat video
    # [Tags]             BP-1357
    # Mo Bo Suu Tap
    # Cho phep cap quyen cho ung dung
    # Mo giao dien Quay phim
    # Chon 1 video tren man Quay Phim
    # Cham vao khoang trong
    # Chon Cat video tren video
    # Sleep    1
    # Page Should Contain Element    ${bst_video_cat_huy}
    # Chon Huy tren man cat video
    # Close All Applications
    
# BP-1364
    # [Documentation]    Check hien vung dieu khien video khi cham vao man hinh - video 4k
    # [Tags]             BP-1364    BP-1368
    # Mo Bo Suu Tap
    # Cho phep cap quyen cho ung dung
    # Sleep    2    
    # Mo giao dien Quay phim
    # Chon 1 video tren man Quay Phim
    # Sleep    2    
    # Cham vao nut Play tren man hinh
    # Sleep    3    
    # Log To Console    Check hien vung dieu khien video khi cham vao man hinh   
    # Cham vao khoang trong
    # ${get_text}=    Get Text    ${get_text_thoi_gian_dung}
    # Page Should Contain Element    ${bst_video_nut_play_tren_video_thuong}
    # Page Should Contain Element    ${nut_lock}
    # Sleep    3 
    # Cham vao khoang trong   
    # Page Should Contain Element    ${nut_track}
    # Page Should Contain Element    ${nut_functions}
    # Page Should Contain Element    ${nut_size}
    # Page Should Contain Element    ${thanh_seebbar}
    # Sleep    2
    # Log To Console    Check tinh nang phat tiep video khi cham vao icon Play tren vung dieu khien   
    # Cham vao khoang trong
    # Page Should Contain Element    ${bst_video_nut_over_play}      
    # ${get_text}=    Get Text    ${get_text_thoi_gian_dung}
    # Sleep    3    
    # Cham vao khoang trong
    # ${get_text}=    Get Text    ${get_text_thoi_gian_dung}
    # Go Back
    # Close All Applications

# BP-1365
    # [Documentation]    Check vung dieu khien video tu an sau 2s
    # [Tags]             BP-1365
    # Mo Bo Suu Tap
    # Cho phep cap quyen cho ung dung
    # Sleep    2    
    # Mo giao dien Quay phim
    # Sleep    2    
    # Chon 1 video tren man Quay Phim
    # Sleep    2    
    # Cham vao nut Play tren man hinh
    # Sleep    3    
    # Cham vao khoang trong
    # Sleep    3
    # Page Should Not Contain Element    ${bst_video_nut_play_tren_video_thuong}
    # Page Should Not Contain Element    ${nut_lock}
    # Page Should Not Contain Element    ${nut_track}
    # Page Should Not Contain Element    ${nut_functions}
    # Page Should Not Contain Element    ${nut_size}
    # Page Should Not Contain Element    ${thanh_seebbar}
    # Close All Applications
 
# BP-1366   
    # [Documentation]    Check an vung dieu khien video khi cham vao man hinh
    # [Tags]             BP-1366    BP-1367
    # Mo Bo Suu Tap
    # Cho phep cap quyen cho ung dung
    # Sleep    2    
    # Mo giao dien Quay phim
    # Sleep    2    
    # Chon 1 video tren man Quay Phim
    # Sleep    2    
    # Cham vao nut Play tren man hinh
    # Sleep    3    
    # Cham vao khoang trong
    # Sleep    3    
    # Cham vao khoang trong
    # Page Should Not Contain Element    ${bst_video_nut_play}
    # Page Should Not Contain Element    ${nut_lock}   
    # Page Should Not Contain Element    ${nut_track}    
    # Page Should Not Contain Element    ${nut_functions}    
    # Page Should Not Contain Element    ${nut_size}    
    # Page Should Not Contain Element    ${thanh_seebbar}
    # Cham vao khoang trong
    # Cham vao nut Over Play tren man hinh
    # # Close All Applications
    
# BP-1367
    # [Documentation]    Check tinh nang tam dung khi cham vao icon tam dung tren vung dieu khien
    # [Tags]            BP-1367
    # Mo Bo Suu Tap
    # Cho phep cap quyen cho ung dung
    # Sleep    2    
    # Mo giao dien Quay phim
    # Sleep    2    
    # Chon 1 video tren man Quay Phim
    # Sleep    2    
    # Cham vao nut Play tren man hinh
    # Sleep    2    
    # Cham vao khoang trong
    # Cham vao nut Over Play tren man hinh
    # Page Should Contain Element    ${bst_video_nut_over_play}    
    # ${get_text_1}=    Get Text    ${get_text_thoi_gian_dung}
    # Sleep    3
    # Cham vao khoang trong
    # ${get_text_2}=    Get Text    ${get_text_thoi_gian_dung}
    # Element Attribute Should Match    ${get_text_1}    text    ${get_text_2}   
    # Close All Applications    
    
# BP-1371
    # [Documentation]    Check dieu chinh tang am luong khi vuot tren man hinh
    # [Tags]             BP-1371    BP-1372    BP-1373    BP-1374    BP-1392
    # Cham vao khoang trong  
    # Cham vao nut Play tren man hinh
    # Vuot tang am luong tren video
    # Sleep    1    
    # Vuot giam am luong tren video
    # Sleep    1    
    # Vuot tang anh sang tren video
    # Sleep    1   
    # Vuot giam anh sang tren video 
    # Close All Applications

# BP-1383
    # [Documentation]    Hien thi vung dieu khien video, hien thi thnah dieu chinh thoi gian lam cham
    # [Tags]             BP-1383        
    # Mo Bo Suu Tap
    # Cho phep cap quyen cho ung dung
    # Mo giao dien Quay phim
    # Sleep    2    
    # Chon video quay cham - video thu 2
    # Sleep    2    
    # Cham nut play tren video quay cham
    # Sleep    2    
    # Cham vao khoang trong
    # Page Should Contain Element    ${bst_video_cham_thanh_thoi_gian}
    # Page Should Contain Element    ${bst_video_cham_time_bar}    
    # Page Should Contain Element    ${bst_video_cham_nut_chup_man_hinh}    
    # Page Should Contain Element    ${bst_video_cham_nut_pause}    
    # Page Should Contain Element    ${bst_video_cham_nut_vong_lap}    
    
# BP-1384 
    # [Documentation]    Check an vung dieu khien khi cham vao man hinh phat video lan 2
    # [Tags]             BP-1384      
    # Sleep    1    
    # Cham vao khoang trong
    # Page Should Not Contain Element    ${bst_video_cham_thanh_thoi_gian}
    # Page Should Not Contain Element    ${bst_video_cham_time_bar}    
    # Page Should Not Contain Element    ${bst_video_cham_nut_chup_man_hinh}    
    # Page Should Not Contain Element    ${bst_video_cham_nut_pause}    
    # Page Should Not Contain Element    ${bst_video_cham_nut_vong_lap}   
    # Close All Applications
     
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================