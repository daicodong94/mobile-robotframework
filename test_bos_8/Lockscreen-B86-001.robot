*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/lockscreen/lockscreen.robot
Resource    ../page/camera/cam_common.robot
Resource    ../page/notification/notification.robot
Suite Setup    Mo Cai dat
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu
*** Test Cases ***
BP-4713
    [Documentation]    Check trang thai icon may anh tren man hinh khoa la icon nam o goc man hinh ben phai, mau trang
    [Tags]    BP-4713    BP-4716
    Log To Console    Check trang thai icon may anh tren man hinh khoa la icon nam o goc man hinh ben phai, mau trang
    Lock    1
    Cham toa do
    Sleep    2
    Kiem tra hien thi date time
    Mo khoa voi mat khau 8888
    Lock    1
    Cham toa do
    Sleep    2
    Vuot sang man hinh ben phai
    Xac nhan mo CHUP ANH thanh cong
    Log To Console    BP-4716: Check tat may anh khi vuot Back/vuot Home
    Vuot back ve giao dien truoc
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_bo_loc}
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_cai_dat}
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_auto_adv}
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_quay_chup}
    Vuot sang man hinh ben duoi
    Bam vao toa do so 8888 tao ma pin

BP-4789
    [Documentation]    Da dat mat khau la Mat khau, check tu dong mo khoa man hinh thanh cong khi nhap dung mat khau da thiet lap
    [Tags]     High    BP-4789    BP-4752    BP-4763    BP-4761 BP-4709    BP-4748    BP-4749
    Vuot toa do de hien thi tab bao mat
    Click vao tab bao mat
    Click vao tab khoa man hinh
    Bam vao toa do so 8888 tao ma pin
    Click vao mat khau
    Mo khoa thiet lap
    Click vao xac nhan tiep theo
    Mo khoa thiet lap
    Click vao xac nhan tao mat khau
    Log To Console    BP-4763: Da dat mat khau la Mat khau, check tu dong mo khoa man hinh thanh cong khi nhap dung mat khau da thiet lap
    Lock    1
    Cham toa do
    Sleep    2
    Log To Console    BP-4749: Check hien thi giao dien nhap khoa man hinh la Mat khau khi vuot tu canh day man hinh len
    AppiumLibrary.Swipe By Percent    50    80    60    40
    Sleep    1
    # Capture Page Screenshot    report/b86_kc_d_ttdh_d_d_mat_khau.png
    Chup anh man hinh va so sanh voi anh mau    ${lockscreen_alias}    ${bphone}_kc_d_ttdh_d_d_mat_khau    6
    Mo khoa voi mat khau text cccc
    Kiem tra mo khoa may va hien thi tai man hinh bao mat
    Kiem tra mo khoa may va hien thi bao mat thiet bi
    Click vao tab khoa man hinh
    Mo khoa thiet lap
    Click vao tab khoa man hinh vuot
    Click vao xoa tinh nang bao ve thiet bi
    Log To Console    BP-4709, BP-4763: Check mo khoa man hinh thanh cong khi vuot len
    Lock    1
    Sleep    1
    Cham toa do
    Cham toa do
    Vuot toa do de mo khoa man hinh
    Kiem tra mo khoa may va hien thi tai man hinh bao mat
    Kiem tra mo khoa may va hien thi bao mat thiet bi
    Click vao tab khoa man hinh
    Click vao ma pin
    Click vao xac nhan co khoi dong che do ma pin
    Click vao xac nhan dong y
    Bam vao toa do so 8888 tao ma pin
    Click vao xac nhan tiep theo
    Bam vao toa do so 8888 tao ma pin
    Click vao xac nhan tao ma pin
    Check hien thi popup nhan xac nhan xong hien thi thong bao
    Log To Console    BP-4761, BP-4789: Check mo duoc man hinh khoa khi nhap dung mat khau/ma PIN da thiet lap sau khi OTA
    Lock    1
    Cham toa do
    Sleep    2
    AppiumLibrary.Swipe By Percent    50    80    60    40
    Log To Console    BP-4748: Check hien thi giao dien nhap khoa man hinh la Ma PIN khi vuot tu canh day man hinh len
    Sleep    1
    # Capture Page Screenshot    report/b86_kc_d_ttdh_d_d_ma_pin.png
    Chup anh man hinh va so sanh voi anh mau    ${lockscreen_alias}    ${bphone}_kc_d_ttdh_d_d_ma_pin    6
    Mo khoa voi mat khau 8888
    Kiem tra mo khoa may va hien thi tai man hinh bao mat
    Kiem tra mo khoa may va hien thi bao mat thiet bi
    Go Back

BP-4718
    [Documentation]    Check bat den pin khi vuot sang phai man hinh
    [Tags]     BP-4718    BP-5098    BP-4720    BP-5100    BP-4721    BP-4723
    Log To Console    Check bat den pin khi vuot sang phai man hinh
    Vuot sang man hinh ben tren
    Lock    1
    Cham toa do
    Sleep    2
    # AppiumLibrary.Swipe By Percent    50    80    60    40
    Vuot sang man hinh ben trai
    Chup anh man hinh va so sanh voi anh mau    ${lockscreen_alias}    ${bphone}_kc_d_ttdh_d_d_bat_den_pin   3
    Go Back
    Log To Console    BP-4721: Check tat den pin khi vuot sang phai man hinh
    Vuot sang man hinh ben trai
    Chup anh man hinh va so sanh voi anh mau    ${lockscreen_alias}    ${bphone}_kc_d_ttdh_d_d_tat_den_pin   3
    Log To Console    BP-5098: Check bat den pin khi cham icon nguon o giao dien den pin tren man hinh khoa
    Log To Console    BP-4720: Check icon den pin tren man hinh khoa chuyen trang thai mau trang sang mau vang khi bat den
    Click bat va tat den pin
    Chup anh man hinh va so sanh voi anh mau    ${lockscreen_alias}    ${bphone}_kc_d_ttdh_d_d_bat_den_pin   3
    Log To Console    BP-5100: Check tat den pin khi cham icon nguon o giao dien den pin tren man hinh khoa
    Click bat va tat den pin
    Log To Console    BP-4723: Check icon den pin tren man hinh khoa chuyen trang thai mau vang sang mau trang khi tat den
    Chup anh man hinh va so sanh voi anh mau    ${lockscreen_alias}    ${bphone}_kc_d_ttdh_d_d_tat_den_pin   3

BP-5097
    [Documentation]    Check bat den pin khi cham icon Den pin tren Dashboard
    [Tags]     BP-5097
    Log To Console    Check bat den pin khi cham icon Den pin tren Dashboard
    Lock    1
    Cham toa do
    Sleep    2
    Vuot sang man hinh ben trai
    Vuot mo bang dieu khien
    Click vao toa do bat den pin
    Chup anh man hinh va so sanh voi anh mau    ${lockscreen_alias}    ${bphone}_kc_d_ttdh_d_d_tat_den_pin   3
    Go Back
    Vuot sang man hinh ben duoi
    Mo khoa voi mat khau 8888

BP-4732
    [Documentation]    Check hien thi mac dinh thong bao tren man hinh khoa la Khong hien thi thong bao nao tren man hinh khoa
    [Tags]     BP-4732    BP-4745
    Log To Console    Check hien thi mac dinh thong bao tren man hinh khoa la Khong hien thi thong bao nao tren man hinh khoa
    Bat cai dat hien thi thong bao
    Kiem tra mac dinh khong hien thi tren man hinh khoa
    Go Back
    Go Back

BP-4762
    [Documentation]    Da dat mat khau la Ma PIN, check hien thi thong bao Ma PIN sai khi nhap sai ma PIN da thiet lap (khong nho hon 4 ky tu)
    [Tags]     BP-4762    BP-4764    BP-4765    BP-5103
    Log To Console    Da dat mat khau la Ma PIN, check hien thi thong bao Ma PIN sai khi nhap sai ma PIN da thiet lap (khong nho hon 4 ky tu)
    Vuot toa do de hien thi tab bao mat
    Lock    1
    Cham toa do
    Sleep    2
    AppiumLibrary.Swipe By Percent    50    99    50    70
    Sleep    1
    Bam vao toa do so 8885 ma pin sai
    Kiem tra nhap ma pin va mat khau sai hien thi thong bao
    Bam vao toa do so 8888 tao ma pin
    Click vao tab bao mat
    Click vao tab khoa man hinh
    Bam vao toa do so 8888 tao ma pin
    Click vao mat khau
    Mo khoa thiet lap
    Click vao xac nhan tiep theo
    Mo khoa thiet lap
    Click vao xac nhan tao mat khau
    Lock    1
    Cham toa do
    Sleep    2
    AppiumLibrary.Swipe By Percent    50    99    50    70
    Log To Console    BP-4765: Da dat mat khau la Mat khau, check hien thi thong bao Mat khau sai khi khong nhap ky tu va cham Xong
    Bam vao toa do xong tren ban phim
    Kiem tra nhap ma pin va mat khau sai hien thi thong bao
    Log To Console    BP-5103: Da dat mat khau la Mat khau, check hien thi thong bao Mat khau sai khi nhap so ky tu nho hon so ky tu da thiet lap va cham Xong
    Bam vao toa do ccc mat khau
    Log To Console    BP-4764: Da dat mat khau la Mat khau, check hien thi thong bao Mat khau sai khi nhap sai mat khau
    Bam vao toa do cccg mat khau
    Kiem tra nhap ma pin va mat khau sai hien thi thong bao
    Mo khoa thiet lap
    Click vao tab khoa man hinh
    Mo khoa thiet lap
    Click vao ma pin
    Click vao xac nhan co khoi dong che do ma pin
    Click vao xac nhan dong y
    Bam vao toa do so 8888 tao ma pin
    Click vao xac nhan tiep theo
    Bam vao toa do so 8888 tao ma pin
    Click vao xac nhan tao ma pin
    Check hien thi popup nhan xac nhan xong hien thi thong bao
    Go Back
    Vuot quay ve man hinh Home
# -----------------------------------------------------------------------------------
# Chưa sử được case này liên quan đến app nghe nhạc
# BP-4739
    # [Documentation]    Da bat Khong hien thi thong bao nao tren popup Tren man hinh khoa, check hien thi giao dien khu dieu khien nhac khi mo 1 bai hat bat ky trong app Nghe nhac
    # [Tags]     BP-4739    BP-4740    BP-4741
    # Log To Console    Da bat Khong hien thi thong bao nao tren popup Tren man hinh khoa, check hien thi giao dien khu dieu khien nhac khi mo 1 bai hat bat ky trong app Nghe nhac
    # Mo Nghe nhac
    # Cho phep cap quyen cho ung dung
    # Click vao icon thu muc danh sach nhac
    # Click vao bai hat
    # Lock    1
    # Capture Page Screenshot    report/b86_kc_d_ttdh_d_d_nhac.png
    # # Chup anh man hinh va so sanh voi anh mau    ${lockscreen_alias}    ${bphone}_kc_d_ttdh_d_d_nhac  3
    # Log To Console    BP-4740, BP-4741    Check chuyen Play/Pause va tiep/lui lai bai hat khi cham icon Next/Previous
    # Click next bai hat
    # Click play va stop nhac
    # Kiem tra next bai hat thanh cong
    # Click play va stop nhac
    # Click back bai hat
    # Click play va stop nhac
    # Kiem tra back bai hat thanh cong
    # Vuot sang man hinh ben duoi
    # Bam vao toa do so 8888 tao ma pin
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================