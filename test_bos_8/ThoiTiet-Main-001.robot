*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/thoi_tiet/tt_common.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup       Mo Thoi tiet
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-2607
    [Documentation]    Check man hinh chinh khi tat quyen vi tri
    [Tags]     High    BP-2607
    Log To Console    Hien thi thong bao hoi Cho phep Thoi tiet truy cap vao vi tri co 2 nut Tu choi va Cho phep
    AppiumLibrary.Page Should Contain Element    ${nut_cho_phep_hoa}
    AppiumLibrary.Page Should Contain Element    ${nut_tu_choi_hoa}

BP-2608
    [Documentation]    Check hien thi khi cham vao nut Tu choi
    [Tags]     High    BP-2608
    Tu choi cap quyen cho ung dung
    Tu choi cap quyen cho ung dung
    Log To Console    Xuat hien hieu ung thu ve icon Thoi tiet tren man hinh Launcher
    AppiumLibrary.Page Should Not Contain Element    ${tt_nut_do_c}
    AppiumLibrary.Page Should Not Contain Element    ${tt_nut_do_f}
    AppiumLibrary.Page Should Not Contain Element    ${tt_nut_them_thanh_pho}
    Vuot quay ve man hinh Home

BP-2628
    [Documentation]    Check direct toi man hinh Thoi tiet
    [Tags]     High    BP-2628
    Mo Thoi tiet tu Launcher

BP-2609
    [Documentation]    Check hien thi khi cham vao nut Chap nhan
    [Tags]     High    BP-2609
    Log To Console    Cham vao Cho phep
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Xac nhan hien thi giao dien man hinh chinh

BP-2614
    [Documentation]    Check chuc nang mo giao dien tim kiem thong tin Thoi tiet khi bam nut Them moi
    [Tags]    BP-2614
    Bam nut them thanh pho
    Log To Console    Ung dung chuyen sang man hinh them thong tin thoi tiet
    AppiumLibrary.Page Should Contain Element    ${tt_tk_nhap_ten_thanh_pho}

BP-2615
    [Documentation]    Check hien thi goi y khi them 1 ki tu
    [Tags]    BP-2615
    Log To Console    Them ki tu "h" vao Text Box
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    h
    Sleep    2
    Log To Console    Man hinh khong hien thi thong tin goi y cac thanh pho co ten bat dau bang ki tu "h"
    AppiumLibrary.Page Should Not Contain Element    ${tt_tk_ds_ten_thanh_pho}    ${EMPTY}
    Sleep    2
    AppiumLibrary.Page Should Not Contain Text    //*[contains(@text,'H')]    ${EMPTY}

BP-2616
    [Documentation]    Check hien thi goi y khi them 2 ki tu
    [Tags]    BP-2616
    Log To Console    Them ki tu "ha" vao Text Box
    AppiumLibrary.Click Element    ${tt_btn_clear_text_tim_kiem}
    AppiumLibrary.Input Text       ${tt_tk_nhap_ten_thanh_pho}    ha
    Sleep    2
    Log To Console    Man hinh hien thi thong tin goi y cac thanh pho lien quan den ki tu "ha" (Ha Noi, Ha Nam...)
    AppiumLibrary.Get Matching Xpath Count    ${tt_tk_ds_ten_thanh_pho}
    Sleep    2
    AppiumLibrary.Element Attribute Should Match    ${tt_tk_ds_ten_thanh_pho}    text    *Hà*

BP-2617
    [Documentation]    Check man hinh chinh khi them 1 thanh pho bang cach nhap 2 ki tu
    [Tags]    BP-2617
    Log To Console    Chon Ha Noi trong danh sach goi y
    AppiumLibrary.Click Element    ${tt_tk_ha_noi}
    Sleep    2
    Bam nut them thanh pho
    AppiumLibrary.Input Text       ${tt_tk_nhap_ten_thanh_pho}    ha
    Sleep    2    
    AppiumLibrary.Click Element    ${tt_tk_ha_nam}
    Log To Console    Dong man hinh tim kiem thong tin Thoi tiet, chuyen sang man hinh chinh
    AppiumLibrary.Page Should Not Contain Element    ${tt_tk_nhap_ten_thanh_pho}
    Sleep    2
    Xac nhan hien thi giao dien man hinh chinh
    Log To Console    thong tin Thoi tiet cua thanh pho Ha Nam đuoc them hien thi duoi cung trong danh sach Thoi tiet cac thanh pho
    AppiumLibrary.Page Should Contain Element    ${tt_tk_ha_nam}

BP-2623
    [Documentation]    Check chuc nang xoa Thoi tiet mac dinh
    [Tags]    BP-2623
    Log To Console    Cham va giu vao Ha Noi
    AppiumLibrary.Long Press    ${tt_tk_ha_noi}    duration=1000
    Sleep    2
    Log To Console    Khong xoa đuoc Thoi tiet mac dinh, chuyen sang man hinh thong tin Thoi tiet cua Thanh pho Ha Noi
    AppiumLibrary.Page Should Not Contain Element    ${tt_nut_xoa}
    AppiumLibrary.Page Should Contain Element    ${tt_ds_gio}
    AppiumLibrary.Page Should Contain Element    ${tt_ds_thu}

BP-2626
    [Documentation]    Check vi tri nut Hom nay
    [Tags]    BP-2626
    Log To Console    Cham vao ngay khac ngay hien tai
    AppiumLibrary.Click Element    ${tt_ds_thu_3}
    Sleep    2
    Log To Console    Hien thi nut Hom nay tren man hinh Thoi tiet cua Thanh pho Ha Noi
    AppiumLibrary.Page Should Contain Element    ${tt_vi_tri_nut_hom_nay}

BP-2627
    [Documentation]    Check chuc nang xem thong tin Thoi tiet ngay Hom nay
    [Tags]     BP-2627
    Log To Console    Cham vao nut Hom nay
    AppiumLibrary.Click Element    ${tt_vi_tri_nut_hom_nay}
    Sleep    2
    Log To Console    Hien thi thong tin Thoi tiet theo gio cua ngay hien tai
    AppiumLibrary.Page Should Not Contain Element    ${tt_vi_tri_nut_hom_nay}
    AppiumLibrary.Page Should Contain Element    ${tt_txt_24h_toi}

BP-1674
    [Documentation]    Check mo giao dien man hinh thong tin chi tiet Thoi tiet cua 1 Thanh pho
    [Tags]    BP-1674    
    Log To Console    Thuc hien vuot len goc trai
    Vuot len de mo man hinh thong tin chi tiet thoi tiet
    Sleep    2
    Log To Console    Giao dien man hinh thong tin chi tiet thoi tiet khong bi xo lech
    AppiumLibrary.Page Should Contain Element    ${tt_ttct_bieu_do_gio}
    AppiumLibrary.Page Should Contain Element    ${tt_ttct_bieu_do_ngay}
    Sleep    2
    Vuot xuong de dong man hinh thong tin chi tiet thoi tiet

BP-2622
    [Documentation]    Check hien thi Button Xoa
    [Tags]    BP-2622
    Log To Console    Quay lai man hinh chinh
    Vuot sang man hinh ben trai
    Xac nhan hien thi giao dien man hinh chinh
    Log To Console    Cham va giu vao Ha Nam
    AppiumLibrary.Long Press    ${tt_tk_ha_nam}    duration=3000
    Sleep    1
    Log To Console    Hien thi Button Xoa tren man hinh chinh
    AppiumLibrary.Page Should Contain Element    ${tt_nut_xoa}

BP-2624
    [Documentation]    Check chuc nang xoa Thoi tiet 1 Thanh pho
    [Tags]    BP-2624
    Log To Console    Cham vao nut Xoa
    AppiumLibrary.Click Element    ${tt_nut_xoa}
    Sleep    1
    Log To Console    Xoa thong tin Thoi tiet Ha Nam thanh cong, man hinh chinh load lai thong tin Thoi tiet cua cac Thanh pho
    AppiumLibrary.Page Should Not Contain Element    ${tt_nut_xoa}
    AppiumLibrary.Page Should Not Contain Element    ${tt_tk_ha_nam}
    AppiumLibrary.Page Should Contain Element    ${tt_tk_ha_noi}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================