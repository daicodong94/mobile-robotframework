*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
ThinhLQ         | 2021/07/23 | update code , tach nho case rieng biet
----------------|------------|--------------------------------------------------------------------

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/nghe_dai/yeu_thich.robot

Suite Setup       Mo Nghe dai
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-2198
    [Documentation]    Thuc hien kiem tra mo giao dien tat ca kenh
    [Tags]    BP-2198    BP-2374    BP-2375    BP-2376    BP-2377    BP-2378    BP-2368    BP-2369    BP-2370
    ...    BP-2371    BP-2372    BP-2340    BP-2341    BP-2342    BP-2343    BP-2344    BP-2198    BP-2199
    ...    BP-2200    BP-2201    BP-2202    BP-2193    BP-2194    BP-2195    BP-2196    BP-2197    BP-2329
    ...    BP-2330    BP-2331    BP-2332    BP-2333    BP-2206    BP-2207    BP-2208    BP-2209    BP-2210
    ...    BP-2211    BP-2212    BP-2213    BP-2214    BP-2214    BP-2413    BP-2230    BP-2231    BP-2232
    ...    BP-2233    BP-2234    BP-2221    BP-2223    BP-2224    BP-2225    BP-2228    BP-2414    BP-2415
    ...    BP-2416    BP-2417    BP-2418    BP-2244    BP-2245    BP-2246    BP-2247    BP-2248    BP-2419
    ...    BP-2420    BP-2421    BP-2422    BP-2423    BP-2264    BP-2265    BP-2266    BP-2267    BP-2268
    ...    BP-2251    BP-2252    BP-2254    BP-2255    BP-2424    BP-2425    BP-2426    BP-2427    BP-2428
    ...    BP-2277    BP-2278    BP-2279    BP-2280    BP-2281    BP-2429    BP-2430    BP-2431    BP-2432
    ...    BP-2433    BP-2297    BP-2298    BP-2299    BP-2300    BP-2301    BP-2285    BP-2286    BP-2287
    ...    BP-2288    BP-2289    BP-2444    BP-2445    BP-2446    BP-2447    BP-2448    BP-2449    BP-2450
    ...    BP-2451    BP-2452    BP-2453    BP-2434    BP-2435    BP-2436    BP-2437    BP-2438    BP-2439
    ...    BP-2440    BP-2441    BP-2442    BP-2443    BP-2392    BP-2393    BP-2394    BP-2395    BP-2396
    ...    BP-2382    BP-2383    BP-2384    BP-2385    BP-2386    BP-2403    BP-2404    BP-2405    BP-2406
    ...    BP-2407    BP-2408    BP-2409    BP-2410    BP-2411    BP-2412    BP-2457    BP-2459    BP-2462
    ...    BP-2463    BP-2464    BP-2500    BP-2501    BP-2502    BP-2503    BP-2504    BP-2495    BP-2496
    ...    BP-2497    BP-2498    BP-2499    BP-2465    BP-2466    BP-2467    BP-2468    BP-2469    BP-2455
    ...    BP-2456    BP-2458    BP-2460    BP-2461    BP-2515    BP-2516    BP-2517    BP-2518    BP-2519
    ...    BP-2510    BP-2511    BP-2512    BP-2513    BP-2514    BP-2505    BP-2506    BP-2507    BP-2508
    ...    BP-2509
    Cho phep cap quyen cho ung dung
    Sleep    10
    Kiem tra nut play co hoat dong khi bat dau mo app
    Thuc hien stop kenh dang chay
    Sleep    2
    Thuc hien click vao icon hen gio
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${nd_alias}    kc_d_ttdh_d_d_HenGio_MacDinh    4
    Go Back
    Thuc hien click button them moi de xuat
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${nd_alias}    kc_d_ttdh_d_d_Icon_ThemMoiKenh_MacDinh    4
    Sleep    2
    Click Element    ${nd_yt_icon_them_moi}
    Cho phep cap quyen cho ung dung
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${nd_alias}    kc_d_ttdh_d_d_ThemMoiKenh_MacDinh    4
    Go Back
    Sleep    2
    Thuc hien click button them moi de xuat
    Click Element    ${nd_yt_icon_de_xuat}
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${nd_alias}    kc_d_ttdh_d_d_DeXuatKenh_MacDinh    4
    Go Back

BP-2192
    [Documentation]    Thuc hien kiem tra mo giao dien nghe dai
    [Tags]    High    BP-2192
    Xac nhan mo app Nghe dai thanh cong
    Sleep    2

BP-2535
    [Documentation]    Thuc hien kiem tra nut them moi
    [Tags]    High    BP-2535
    Page Should Contain Element    ${nd_yt_btn_them_moi}
    Sleep    1
    Click Element    ${nd_yt_btn_them_moi}
    Sleep    1
    Page Should Contain Element    ${nd_yt_icon_them_moi}
    Sleep    1
    Page Should Contain Element    ${nd_yt_icon_de_xuat}
    Sleep    1

BP-2536
    [Documentation]    Thuc hien kiem tra mo giao dien them moi
    [Tags]    High    BP-2536
    Click Element    ${nd_yt_icon_them_moi}
    Cho phep cap quyen cho ung dung
    Sleep    5
    Page Should Contain Element    ${nd_tm_trang_thai_nghe_thu}
    Thuc hien input link kenh moi
    Sleep    2
    Thuc hien click nghe thu
    Sleep    2
    Page Should Not Contain Element    ${nd_tm_trang_thai_nghe_thu}
    Sleep    1

BP-2539
    [Documentation]    Check chuc nang dung phat kenh nghe thu khi cham Dung
    [Tags]    High    BP-2539
    Wait Until Page Contains Element    ${nd_tm_trang_thai_dung}    timeout=50
    Click Element    ${nd_tm_trang_thai_dung}
    Sleep    1
    Page Should Contain Element    ${nd_tm_trang_thai_nghe_thu}
    Page Should Not Contain Element    ${nd_tm_trang_thai_dung}
    Go Back

BP-2561
    [Documentation]    Check mo ra giao dien De xuat kenh khi cham text De xuat kenh/icon "^"
    [Tags]    High    BP-2561
    Go Back
    Thuc hien click button them moi de xuat
    Thuc hien click icon tab de xuat
    Element Attribute Should Match    ${nd_dx_tieu_de_url}    text    ${nd_dx_text_tieu_de_url}
    Element Attribute Should Match    ${nd_dx_tieu_de_ten_kenh}    text    ${nd_dx_text_tieu_de_ten_kenh}
    Element Attribute Should Match    ${nd_dx_tieu_de_mo_ta}    text    ${nd_dx_text_tieu_de_mo_ta}
    Click Element    ${nd_ttct_icon_ttct_nghe_dai}
    Sleep    2
    Page Should Not Contain Element    ${nd_dx_tieu_de_url}
    Page Should Not Contain Element    ${nd_dx_tieu_de_ten_kenh}
    Page Should Not Contain Element    ${nd_dx_tieu_de_mo_ta}
    Thuc hien click button them moi de xuat
    Thuc hien click icon tab de xuat

BP-2568
    [Documentation]    Check dong giao dien De xuat kenh khi cham icon Back (goc tren ben trai)
    [Tags]    High        BP-2568
    Go Back
    Sleep    2
    Page Should Not Contain Element    ${nd_dx_tieu_de_url}
    Page Should Not Contain Element    ${nd_dx_tieu_de_ten_kenh}
    Page Should Not Contain Element    ${nd_dx_tieu_de_mo_ta}
    Sleep    1

BP-2538
    [Documentation]    Check chuc nang them kenh khi nhap dung link radio
    [Tags]    High    BP-2538
    Cho phep cap quyen cho ung dung
    Thuc hien click button them moi
    Cho phep cap quyen cho ung dung
    Thuc hien input link kenh moi
    Thuc hien click nghe thu
    Sleep    4
    Page Should Not Contain Element    ${nd_tm_trang_thai_nghe_thu}
    Page Should Contain Element    ${nut_dung_hoa}

BP-2545
    [Documentation]    Check chuc nang them kenh khi nhap dung link radio
    [Tags]    High    BP-2545
    Sleep    2
    Thuc hien input ten kenh
    Thuc hien input mo ta kenh
    Thuc hien vuot hien thi them kenh
    Thuc hien them kenh moi
    Wait Until Page Contains Element    ${nd_tab_yeu_thich}
    Swipe By Percent     50    75    50    10
    Page Should Contain Element    ${nd_tm_btn_kenh_moi}
    Vuot sang man hinh ben tren


BP-2559
    [Documentation]    Thuc hien thoat ra khoi man hinh them moi hien thi tai man hinh yeu thich
    [Tags]    High    BP-2559
    Thuc hien click button them moi
    Cho phep cap quyen cho ung dung
    Click nut back them moi kenh
    Thuc hien kiem tra da thoat khoi man hinh them moi
    Kiem tra khong hien thi nut them kenh
    Thuc hien click button them moi

BP-2560
    [Documentation]    Check dong giao dien Them kenh moi khi vuot Back
    [Tags]    BP-2560
    Vuot back ve giao dien truoc
    Thuc hien kiem tra da thoat khoi man hinh them moi
    Kiem tra khong hien thi nut them kenh
    Kiem tra nut play co hoat dong khi bat dau mo app
    Thuc hien kiem tra kenh vov3 dang duoc phat

BP-2562
    [Documentation]    Check chuc nang De xuat kenh khi nhap dung link radio
    [Tags]    High    BP-2562
    Log To Console    Check chuc nang stop
    Thuc hien stop kenh dang chay
    Thuc hien click button them moi de xuat
    Thuc hien click vao de xuat kenh
    Sleep    5
    Thuc hien input vao web url
    Thuc hien input vao radio name
    Thuc hien input vao description
    Thuc hien click button de xuat
    Page Should Not Contain Element    ${nd_dx_btn_them_moi_de_xuat}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
