*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/ghi_chep/gc_common.robot

Suite Setup       Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5709
    [Documentation]     Check mo app Ghi Chep tu Launcher
    [Tags]    High    BP-5079
    Mo Ghi chep tu Launcher
    Close All Applications

BP-5711
    [Documentation]    Check tao thanh cong khi chon ghi am
    [Tags]    High BP-5711
    Mo ung dung va cho phep cap quyen    ${gc_alias}    ${gc_package}    ${gc_activity}    2
    Bam nut add
    Bam nut add ghi am
    Cho phep cap quyen cho ung dung
    Bam nut add
    Chup anh man hinh va so sanh voi anh mau    ${gc_alias}    kc_d_ttdh_d_d_Tao_thanhcong_file_ghiam    2
    Close All Applications

BP-5710
    [Documentation]     Check tao thanh cong khi chon Ghi chep
    [Tags]    High    BP-5710
    Mo ung dung va cho phep cap quyen    ${gc_alias}    ${gc_package}    ${gc_activity}    2
    Bam nut add
    Bam nut add ghi chep
    Input Text    ${Tieu_de}    tao ghi chep
    Input Text    ${Ghi_chu}    tao thanh cong ghi chep
    Bam nut add
    Chup anh man hinh va so sanh voi anh mau    ${gc_alias}    kc_d_ttdh_d_d_Tao_thanhcong_ghichep    2
    # Element Should Contain Text    ${Tao_ghi_chep_thanh_cong}    tao ghi chep
    Close All Applications

BP-5712
    [Documentation]    Check tao thanh cong ghi chep khi chon Anh
    [Tags]    High    BP-5712
    Mo ung dung va cho phep cap quyen    ${gc_alias}    ${gc_package}    ${gc_activity}    2
    Bam nut add
    Bam nut add anh
    AppiumLibrary.Click Element    ${Chon_anh}
    Sleep    2
    ${kiem_tra}    AppiumLibrary.Get Matching Xpath Count    ${Chi_mot_lan}
    Run Keyword If    ${kiem_tra}>0    AppiumLibrary.Click Element    ${Chi_mot_lan}
    Sleep    1
    Bam vao chup man hinh
    Chon anh bat ky
    Input Text    ${Tieu_de}     tao anh thanh cong
    Bam nut add
    Page Should Contain Element    ${Tao_anh_thanh_cong}    tao anh thanh cong

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================