*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/dong_ho/dh_common.robot
Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/dong_ho/cai_dat.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-430
    [Documentation]    Kiem tra giao dien dong ho
    [Tags]    High    BP-424    BP-426    BP-430    BP-428     BP-432    BP-433    BP-434
    Mo Dong ho
	Cho phep cap quyen cho ung dung
    Nhan nut tab menu top dong ho
    AppiumLibrary.Page Should Contain Element    ${dh_btn_thanh_pho}
    Log To Console    BP-424: thuc hien kiem tra giao dien dong ho
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_dongho    3
    Nhan nut tab menu top hen gio
    Log To Console    BP-426: Mo tu giao dien Dong ho dem nguoc
    AppiumLibrary.Page Should Contain Element    ${dh_btn_thanh_pho}
    Log To Console    BP-428: Mo tu giao dien Dong ho bam gio
    Nhan nut tab menu top bam gio
    Nhan nut tab menu top dong ho
    AppiumLibrary.Page Should Contain Element    ${dh_btn_thanh_pho}
    Log To Console    BP-433: Thuc hien kiem tra giao dien gio quoc te
    Nhan nut icon gio quoc te
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_gio_quocte   2
    Close Application

BP-436
    [Documentation]    Check chuc nang mo danh sach quoc te
    [Tags]    BP-436    BP-438
    Log To Console    Check chuc nang mo danh sach quoc te
    Mo Dong ho
	Cho phep cap quyen cho ung dung
    Nhan nut tab menu top dong ho
    Nhan nut icon gio quoc te
    Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    Log To Console    BP-438: Check chuc nang bam vao mui ten back
    Nhan nut icon back quay lai
    Check hien thi man hinh dong ho thoi gian
    Close Application

BP-440
    [Documentation]    Check mo o text box tim kiem
    [Tags]    BP-440    BP-442
    Log To Console    Check mo o text box tim kiem
    Mo Dong ho
	Cho phep cap quyen cho ung dung
    Nhan nut tab menu top dong ho
    Nhan nut icon gio quoc te
    Nhan nut icon tim kiem
    Check kiem tra verify o tim kiem
    Log To Console    BP-442: Check truong hop bam dau X khi placeholder tim kiem trong
    Nhan nut xoa text tim kiem
    Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    Close Application

BP-443
    [Documentation]    Check chuc nang tim kiem
    [Tags]    BP-443    BP-445    BP-446    BP-448    BP-449    BP-450    BP-453
    Log To Console    Check truong hop bam dau X khi da nhap du lieu tim kiem
    Mo Dong ho
	Cho phep cap quyen cho ung dung
    Nhan nut tab menu top dong ho
    Nhan nut icon gio quoc te
    Nhan nut icon tim kiem
    Log To Console    BP-446: Check truong hop tim kiem khi nhap sai ket qua tim kiem
    Thuc hien input text tim kiem khong co du lieu
    Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    Nhan nut xoa text tim kiem
    Sleep    1
    Page Should Contain Element    ${dh_text_tim_kiem}
    Log To Console    BP-445: Check chuc nang tim kiem
    Thuc hien input text tim kiem
    Check hien thi ten cua cac nuoc
    Nhan nut xoa text tim kiem
    Log To Console    BP-448: Check truong hop tim kiem khi nhap khong dau
    Thuc hien input text tim kiem ha noi khong dau
    Check hien thi ten cua cac nuoc
    Nhan nut xoa text tim kiem
    Log To Console    BP-449: Check truong hop nhap tim kiem bang chu in hoa
    Thuc hien input text tim kiem chu in hoa
    Check hien thi ten cua cac nuoc
    Nhan nut xoa text tim kiem
    Log To Console    BP-450: Check truong hop nhap tim kiem bang chu in thuong
    Thuc hien input text tim kiem chu thuong co dau
    Check hien thi ten cua cac nuoc
    Nhan nut xoa text tim kiem
    Log To Console    BP-453: Check truong hop khi thuc hien CTRL+V de paste noi dung o noi khac vao placeholder tim kiem
    Copy text Paste noi dung da copy
    Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    Nhan nut xoa text tim kiem
    Close Application

BP-646
    [Documentation]    check chuc nang tich chon thanh pho
    [Tags]    BP-646     BP-647    BP-648     BP-650    BP-651
    Log To Console    check chuc nang tich chon thanh pho
    Mo Dong ho
	Cho phep cap quyen cho ung dung
    Nhan nut tab menu top dong ho
    Nhan nut icon gio quoc te
    Nhan o checkbox tich chon hien thi
    AppiumLibrary.Click Element At Coordinates        577    1696
    Sleep    2
    Nhan nut icon back quay lai
    Check dem phan tu hien thi danh sach gio quoc te da duoc tich chon
    Log To Console    BP-647: Check chuc nang bo tick chon thanh pho
    Nhan nut icon gio quoc te
    AppiumLibrary.Click Element At Coordinates        227    503
    Nhan nut icon back quay lai
    Check dem phan tu hien thi danh sach gio quoc te da duoc tich chon
    Log To Console    BP-648: Check chuc nang hien thi nhieu gio quoc te tren giao dien Dong ho
    Log To Console    BP-651: Check chuc nang vuot len/xuong danh sach gio quoc te
    Nhan nut icon gio quoc te
    Ckeck vuot launcher len tren
    Ckeck vuot launcher xuong
    Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    Log To Console    BP-650: Check chuc nang cham va giu vao thanh pho da them tren giao dien Dong ho
    Nhan nut icon back quay lai
    Nhan cham va giu vao thanh pho da them tren giao dien dong ho
    Check hien thi man hinh dong ho thoi gian
    Close Application

BP-455
    [Documentation]    check chuc nang tuy chon chuyen thoi gian sang ten
    [Tags]    BP-455    BP-456    BP-459
    Log To Console    BP-455: Check chuc nang bam vao dau 3 cham
    Mo Dong ho
	Cho phep cap quyen cho ung dung
    Nhan nut tab menu top dong ho
    Nhan nut icon gio quoc te
    Nhan nut tab menu top tuy chon
    Page Should Contain Element    ${dh_bt_dinh_dang_sap_xep}
    Log To Console    BP-456: Check chuyen danh sach gio quoc te sap xep theo ten sang sap xep theo thoi gian
    Nhan nut chuyen danh sach gio quoc te sang dinh dang khac
    Check kiem tra verify hien thi gio quoc te theo thoi gian
    Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    Log To Console    BP-459: Check chuyen danh sach gio quoc te sap xep theo thoi gian sang sap xep theo ten
    Nhan nut tab menu top tuy chon
    Nhan nut chuyen danh sach gio quoc te sang dinh dang khac
    Check hien thi chu cai ten cua cac nuoc quoc te

BP-461
    [Documentation]    Check mo giao dien cai dat
    [Tags]    BP-461
    Log To Console    Check mo giao dien cai dat
    Mo Dong ho
	Cho phep cap quyen cho ung dung
    Nhan nut tab menu top dong ho
    Cham vao dau ba cham
    Nhan vao nut cai dat
    AppiumLibrary.Page Should Contain Element    ${dh_hien_thi_kieu_dong_ho}
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================