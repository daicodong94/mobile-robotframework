*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
ThinhLQ         | 2021/07/23 | update code , tach nho case rieng biet
----------------|------------|--------------------------------------------------------------------

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/nghe_dai/yeu_thich.robot
Resource    ../page/nghe_dai/tat_ca_kenh.robot

Suite Setup       Mo Nghe dai
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-2055
    [Documentation]    Check chuc nang "Tat ca cac the loai kenh" khi co mang
    [Tags]    BP-2055
    Cho phep cap quyen cho ung dung
    Sleep    10
    Thuc hien click chon tab tat ca kenh
    Thuc hien kiem tra danh sach chuc nang tat ca the loai

BP-2056
    [Documentation]    Check chuc nang chon the loai xem co dung voi the loai khong
    [Tags]    BP-2056
    Thuc hien kiem tra chuc nang chon the loai kenh
    AppiumLibrary.Click Element    ${nd_tctl_txt_chon_chinh_tri}
    Sleep    1
    AppiumLibrary.Click Element    ${nd_tctl_txt_chon_tat_ca}


BP-2057
    [Documentation]    Check chuc nang tim kiem khi bang chu cai
    [Tags]    BP-2057
    Vuot sang man hinh ben trai
    Vuot sang man hinh ben phai
    Thuc hien nhap vao o tim kiem
    Page Should Contain Element    ${nd_txt_vov1}
    Page Should Contain Element    ${nd_txt_vov2}
    Page Should Contain Element    ${nd_txt_vov3}
    Page Should Contain Element    ${nd_txt_vov5}
    thuc hien input vao o tim kiem voi mot chu cai

BP-2059
    [Documentation]    Check chuc nang tim kiem khi khong co noi dung
    [Tags]    BP-2059
    AppiumLibrary.Clear Text    ${nd_tctl_nut_tim_kiem}
    AppiumLibrary.Input Text    ${nd_tctl_nut_tim_kiem}    Thinhlq@
    Page Should Not Contain Element    ${nd_txt_vov1}
    Page Should Not Contain Element    ${nd_txt_vov2}
    Page Should Not Contain Element    ${nd_txt_vov3}
    Page Should Not Contain Element    ${nd_txt_vov5}
    Thuc hien kiem tra man hinh

BP-2058
    [Documentation]    Check chuc nang tim kiem khi co noi dung
    [Tags]    BP-2058
    Go Back
    Log To Console    BP-2058: Check chuc nang tim kiem khi co noi dung
    thuc hien input vao o tim kiem voi noi dung

BP-2060
    [Documentation]    Check chuc nang hien thi cac thong tin
    [Tags]    BP-2060
    Go Back
    Thuc hien click tab 3 cham thong tin lien he

BP-2051
    [Documentation]    thuc hien kiem tra danh sach hien thi tai man hinh tat ca kenh
    [Tags]    High    BP-2051
    Go Back
    Thuc hien bam chon vov1 duoc phat
    Thuc hien kiem tra danh sach hien thi tai man hinh tat ca kenh
    Vuot launcher len nua danh sach
    Thuc hien kiem tra danh sach hien thi tai man hinh tat ca kenh
    Vuot launcher len tren
    Thuc hien kiem tra danh sach hien thi man hinh tat ca kenh

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================