*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library    AppiumLibrary

Resource    ../page/dong_ho/bao_thuc.robot
Resource    ../page/launcher/launcher_common.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***

BP-327
    [Documentation]    Check chuc nang huy bao thuc bang cach vuot tu home ve giua man hinh
    [Tags]    BP-327
    Log To Console    Dong giao dien quan ly bao thuc
    Log To Console    Check chuc nang chuyen qua giao dien bao thuc theo dong ho so
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Bam vao them bao thuc
    vuot tu mep duoi vao giua man hinh
    AppiumLibrary.Page Should not Contain Element    ${dh_bt_nut_dong_y}
    Close Application

BP-330
    [Documentation]    Check chuc nang chuyen qua giao dien bao thuc theo dong ho so
    [Tags]    BP-330    BP-333    BP-336    BP-339
    Log To Console    BP-330: Check chuc nang chuyen qua giao dien bao thuc theo dong ho so
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Bam vao them bao thuc
    Bam vao bao nut ban phim
    AppiumLibrary.Page Should Contain Element    ${dh_text_nhap_thoi_gian}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_nut_in_ban_phim}
    Log To Console    BP-339: Check chuc nang chuyen sang giao dien tao bao thuc theo dong ho kim
    Bam vao bao nut ban phim
    AppiumLibrary.Page Should Not Contain Element    ${dh_text_nhap_thoi_gian}
    AppiumLibrary.Page Should Not Contain Element    ${dh_bt_nut_in_ban_phim}
    Bam vao bao nut ban phim
    Log To Console    BP-333: Check chuc nang chon thoi gian bao thuc
    Dat thoi gian bao thuc
    Log To Console    BP-336: Check chuc nang huy tao bao thuc
    Bam vao huy
    Page Should Not Contain Element    ${nut_huy}
    Page Should Not Contain Element    ${nut_dong_hoa}

BP-338
    [Documentation]    Check chuc nang tao bao thuc dong ho so
    [Tags]    BP-338
    Log To Console    Check chuc nang tao bao thuc dong ho so
    Bam vao them bao thuc
    Bam vao bao nut ban phim
    Dat thoi gian bao thuc
    Page Should Contain Element    ${dh_bt_thong_bao1}
    Page Should Contain Text    9:29
    Close Application

BP-358
    [Documentation]    Check chuc nang tick chon lap lai nhung bo chon tat ca cac ngay trong tuan
    [Tags]    BP-358
    Log To Console    Check chuc nang tick chon lap lai nhung bo chon tat ca cac ngay trong tuan
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Bam vao them bao thuc
    Bam vao dong y bao thuc
    Bam vao lap lai bao thuc
    Bam vao cac thu trong bao thuc 2 3 4
    Bam vao cac thu trong bao thuc 5 6 7 cn
    AppiumLibrary.Page Should not Contain Element    ${dh_bt_txt_hien_thi_thu_hai}
    Close Application

BP-359
    [Documentation]    Check chuc nang mo danh sach Am thanh bao thuc
    [Tags]    BP-359
    Log To Console    Check chuc nang mo danh sach Am thanh bao thuc
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Bam vao them bao thuc
    Bam vao dong y bao thuc
    Bam vao chuong
    AppiumLibrary.Page Should Contain Element    ${dh_bt_kieu_chuong_argon}

BP-360
    [Documentation]    Check chuc nag cho chuong bao thuc tu am thanh cua ban
    [Tags]    BP-360
    Log To Console    Check chuc nag cho chuong bao thuc tu am thanh cua ban
    Bam vao them am thanh moi
    AppiumLibrary.Page Should Not Contain Element    ${dh_bt_them_am_thanh_moi}

BP-365
    [Documentation]    Check chuc nang dong danh sach Am thanh bao thuc
    [Tags]    BP-365
    Log To Console    Check chuc nang dong danh sach Am thanh bao thuc
    vuot dong giao dien
    Bam dong giao dien am thanh
    AppiumLibrary.Page Should Not Contain Element    ${dh_bt_them_am_thanh_moi}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_bao_thuc_nhanh}

BP-367
    [Documentation]    Check chuc nag dong am thanh bang thoa tac vuot back
    [Tags]    BP-367
    Log To Console    Check chuc nag dong am thanh bang thoa tac vuot back
    Bam vao chuong
    vuot dong giao dien
    AppiumLibrary.Page Should Not Contain Element    ${dh_bt_them_am_thanh_moi}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_bao_thuc_nhanh}

BP-368
    [Documentation]    Check chuc nag dong am thanh bang thao tac vuot home
    [Tags]    BP-368
    Log To Console    Check chuc nag dong am thanh bang thao tac vuot home
    Bam vao chuong
    vuot tu mep duoi vao giua man hinh
    Bam dong toan bo app tren da nhiem
    AppiumLibrary.Page Should Not Contain Element    ${dh_bt_bao_thuc_nhanh}
    AppiumLibrary.Page Should Not Contain Element    ${dh_tab_bao_thuc}
    Close Application

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================