*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/tin_nhan.robot

Suite Setup       Mo Launcher
Suite Teardown    Close All Applications

*** Test Cases ***
BP-2070
    [Documentation]    Check mo giao dien cai dat tin nhan tu tab dien thoai
    [Tags]    BP-2070
    Log To Console   Check mo giao dien cai dat tin nhan tu tab dien thoai    
    Mo Dien thoai tu Launcher
    bam vao dau 3 cham trong giao dien dien thoai
    bam vao tin nhan
    Page Should Contain Element    ${tn_cd_btalk}  
    Page Should Contain Element    ${tn_cd_back}    
    Bam nut dong toan bo ung dung trong da nhiem
    
BP-2071
    [Documentation]    Check mo giao dien cai dat tin nhan tu tab tin nhan
    [Tags]     BP-2071   
    Log To Console   Check mo giao dien cai dat tin nhan tu tab tin nhan    
    bam vao tin nhan
    bam vao tin nhan trong dau 3 cham trong tin nhan
    Bam vao cai dat trong tin nhan
    Page Should Contain Element    ${tn_cd_btalk}  
    Page Should Contain Element    ${tn_cd_back}    

BP-2073
    [Documentation]    Check mo giao dien ung dung sms mac dinh
    [Tags]    BP-2073    BP-2074     BP-2080
    Log To Console   Check mo giao dien cai dat tin nhan tu tab dien thoai    
    Check mo giao dien ung dung SMS mac dinh
    Log To Console    Check luu lua chon khi an chon messenger lam ung dung mac dinh    
    Check luu lua chon khi an chon messenger lam ung dung mac dinh
    Log To Console    Check disable cai dat thong bao tin nhan    
    Vuot back ve giao dien truoc
    Check disable cai dat thong bao tin nhan
 
BP-2089
    [Documentation]    Check mo popup ban muon dat btalk lam ung dung sms mac dinh
    [Tags]    BP-2089    BP-2090    BP-2095 
    Log To Console   Check mo popup ban muon dat btalk lam ung dung sms mac dinh    
    Check mo popup
    log to console    Check dong popup ban muon chon btalk lam sms mac dinh khi cham vao huy
    Check dong popup
    log to console    Check luu lua chon khi dat btalk lam ung dung sms mac dinh
    Check luu lua chon khi dat btalk lam ung dung sms mac dinh
    log to console    Check enable cai dat thong bao tin nhan
    Check enable cai dat thong bao tin nhan
   
BP-2104
    [Documentation]    Check hien thi thong bao trong giao dien tin nhan sau khi chon ung dung khac lam ung dung sms mac dinh
    [Tags]    BP-2104    BP-2106    BP-2089    BP-2090    BP-2110
    Log To Console    Check hien thi thong bao trong giao dien tin nhan    
    Check hien thi thong bao trong giao dien tin nhan
    Log To Console    Check mo popup khi cham vao thay doi    
    Check mo popup khi cham vao thay doi
    Log To Console    Check dong popup khi cham vao huy    
    Check dong popup khi cham vao huy
    Log To Console    Check luu lua chon khi chon sms lam ung dung mac dinh    
    Check luu lua chon khi chon sms lam ung dung mac dinh
    Log To Console    Check an thong bao dat btalk lam ung dung mac dinh    
    Check an thong bao dat btalk lam ung dung mac dinh
    Bam nut dong toan bo ung dung trong da nhiem

BP-2115 
    [Documentation]     Check hien thi thong bao de gui tin nhan hay dat btalk lam ung dung mac dinh
    [Tags]       BP-2115    BP-2117     BP-2118   
    Log To Console    Check hien thi thong bao de gui tin nhan hay dat btalk lam ung dung mac dinh    
    bam vao tin nhan
    bam vao tin nhan trong dau 3 cham trong tin nhan
    Bam vao cai dat trong tin nhan
    Log To Console    Check mo popup ban muon chon sms mac dinh khi cham vao thay doi   
    log to console    ckeck dong popup khi cham vao huy 
    Check mo popup ban muon chon sms mac dinh khi cham vao thay doi
    log to console    Check luu lua chon khi dat btalk lam ung dung mac dinh 
    Check luu lua chon khi dat btalk lam ung dung mac dinh

BP-2134
    [Documentation]    Check mo giao dien cai dat thong bao tin nhan
    [Tags]    BP-2134
    Log To Console    Check mo giao dien cai dat thong bao tin nhan    
    bam vao tin nhan trong dau 3 cham trong tin nhan
    Bam vao cai dat trong tin nhan
    Check mo giao dien cai dat thong bao tin nhan
    
BP-2189
    [Documentation]    Check mo rong danh sach cai dat khi cham vao nang cao chon canh bao
    [Tags]        BP-2187     BP-2188     BP-2189 
    Log To Console    Check mo rong danh sach cai dat khi cham vao nang cao chon canh bao    
    Check mo rong danh sach cai dat khi cham vao nang cao chon canh bao
    Log To Console    Check hien thi am thanh thong bao mac dinh khi de mac dinh    
    Check hien thi am thanh thong bao mac dinh khi de mac dinh
    Log To Console    Check mo rong danh sach cai dat khi cham vao nang cao chon im lang    
    Check mo rong danh sach cai dat khi cham vao nang cao chon im lang
    
BP-2222
    [Documentation]    Check an man hinh khoa khi chon im lang va bat che do thu nho
    [Tags]    BP-2222    BP-2226    BP-2227
    Log To Console    Check an man hinh khoa khi chon im lang va bat che do thu nho  
    Check an man hinh khoa khi chon im lang va bat che do thu nho  
    Log To Console    Check mo popup man hinh khoa khi cham vao man hinh khoa  
    Check mo popup man hinh khoa khi cham vao man hinh khoa  
    Log To Console    Check dong popup man hinh khi cham vao huy  
    Check dong popup man hinh khi cham vao huy
      
BP-2302  
    [Documentation]    Check mo giao dien cai dat chung khi cham vao bo sung trong ung dung
    [Tags]    BP-2302
    Log To Console    Check mo giao dien cai dat chung khi cham vao bo sung trong ung dung   
    Check mo giao dien cai dat chung khi cham vao bo sung trong ung dung 
    Bam nut dong toan bo ung dung trong da nhiem

BP-2317
    [Documentation]    Check an avata o moi tin nhan o trang thai mac dinh
    [Tags]    BP-2317    BP-2319    BP-2326
    log to console    Check an avata o moi tin nhan o trang thai mac dinh
    bam vao tin nhan
    Check an avata o moi tin nhan o trang thai mac dinh
    Log To Console    Check hien thi avata o moi tin nhan sau khi bat    
    Check hien thi avata o moi tin nhan sau khi bat
    Log To Console    Check an hien thi avata o moi tin nhan sau khi tat    
    Check an hien thi avata o moi tin nhan sau khi tat
    Vuot back ve giao dien truoc
    Bam nut dong toan bo ung dung trong da nhiem
    
BP-2354 
    [Documentation]    Check mo popup nhan tin theo nhom khi cham vao nhan tin theo nhom
    [Tags]     BP-2354 
    bam vao tin nhan
    bam vao tin nhan trong dau 3 cham trong tin nhan
    Bam vao cai dat trong tin nhan
    Check mo popup nhan tin theo nhom khi cham vao nhan tin theo nhom 
    
BP-2362
    [Documentation]    Check luu thay doi khi chon che do gui mms cho tat ca moi nguoi
    [Tags]     BP-2362    BP-2365
    Log To Console    Check luu thay doi khi chon che do gui mms cho tat ca moi nguoi    
    Check luu thay doi khi chon che do gui mms cho tat ca moi nguoi 
    Log To Console    Check dong popup khi cham vao huy o popup nhan tin theo nhom  
    Check dong popup khi cham vao huy o popup nhan tin theo nhom  
    
BP-2379
    [Documentation]    Check hien thi so dien thoai khong xac dinh khi chua nhap so dien thoai
    [Tags]    BP-2379    BP-2380    
    Log To Console    Check hien thi so dien thoai khong xac dinh khi chua nhap so dien thoai  
    Check hien thi so dien thoai khong xac dinh khi chua nhap so dien thoai 
    Log To Console    Chek dong popup so dien thoai khi cham vao huy    
    Chek dong popup so dien thoai khi cham vao huy
    
BP-2401
    [Documentation]    Check mo giao dien canh bao khan cap khi cham vao canh bao khong day
    [Tags]    BP-2401    BP-2402    
    Log To Console    Check mo giao dien canh bao khan cap khi cham vao canh bao khong day  
    Check mo giao dien canh bao khan cap khi cham vao canh bao khong day 
    Log To Console    check dong giao dien lich su khi cham vao icon mui ten back    
    check dong giao dien lich su khi cham vao icon mui ten back
    Bam nut dong toan bo ung dung trong da nhiem

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================