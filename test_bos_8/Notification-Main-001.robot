*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/notification/notification.robot
Resource    ../page/config/mac_dinh.robot

# Suite Setup       Mo Launcher
# Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-2629
    [Documentation]    Check Vuot mo bang thong bao khi vuot va keo xuong tu noc den qua 1/2 man hinh
    [Tags]    High    BP-2969    BP-2630
    Mo Launcher
    Log To Console    BP-2630    Check Vuot mo bang thong bao khi vuot va keo xuong tu noc man hinh
    Vuot mo bang thong bao
    Chup anh man hinh va so sanh voi anh mau    notification    kc_d_ttdh_d_d_thong_bao_launcher    6
    Xoa toan bo thong bao

BP-2631
    [Documentation]    Check Vuot mo bang thong bao khi vuot cheo xuong tu mep trai man hinh
    [Tags]    High    BP-2631    BP-2632
    Swipe By Percent    5    20    90    80    duration=3000
    Sleep    2
    # Chup anh man hinh va so sanh voi anh mau    notification    kc_d_ttdh_d_d_thong_bao_launcher    6
    Page Should Contain Text    Appium Settings
    Xoa toan bo thong bao
    Log To Console    BP-2632    Check Vuot mo bang thong bao khi vuot cheo xuong tu mep phai man hinh
    Swipe By Percent    90    20    5    80
    Sleep    2
    # Chup anh man hinh va so sanh voi anh mau    notification    kc_d_ttdh_d_d_thong_bao_launcher    6
    Page Should Contain Text    Appium Settings
    Xoa toan bo thong bao

BP-2633
    [Documentation]    Check Vuot mo bang thong bao khi vuot va keo xuong tu diem bat ky duoi Status bar den tren Hotseat.
    [Tags]    High    BP-2633
    Swipe By Percent    50    5    50    80
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    notification    kc_d_ttdh_d_d_thong_bao_launcher    6
    Log To Console    BP-2641    Check mo lai Notification khi vuot tu day len khong qua 1/2 man hinh
    Swipe By Percent    30    80    30    60    duration=5000
    Page Should Contain Text    Appium Settings
    Sleep    2
    Xoa toan bo thong bao
    Close Application

BP-2634
    [Documentation]    Check Vuot mo bang thong bao khi vuot xuong tu noc man hinh
    [Tags]    High    BP-2634    BP-2635    BP-2636    BP-2637
    # Run Keyword If    ${bphone}==b86    Bat hien thi tat ca cac noi dung thong bao    
    # Run Keyword If    ${bphone}==b3    bat hien thi thong bao tren man hinh khoa cho b3    
    Bat hien thi tat ca cac noi dung thong bao
    Vuot mo bang thong bao
    Sleep    2
    Xoa toan bo thong bao
    Log To Console    BP-2635    Check Vuot mo bang thong bao khi vuot cheo xuong tu mep trai man hinh
    Swipe By Percent    5    20    90    80
    Sleep    2
    Page Should Contain Text    Appium Settings
    Xoa toan bo thong bao
    Log To Console    BP-2636    Check Vuot mo bang thong bao khi vuot cheo xuong tu mep phai man hinh
    Swipe By Percent    85    20    30    80
    Sleep    2
    Page Should Contain Text    Appium Settings
    Xoa toan bo thong bao
    Log To Console    BP-2637    Check Vuot mo bang thong bao khi vuot xuong tu cac diem trong pham vi ben duoi khung thoi gian den tren Hot
    Swipe By Percent    50    30    50    80
    Sleep    2
    Page Should Contain Text    Appium Settings
    Xoa toan bo thong bao

BP-2638
    [Documentation]    Check Vuot mo bang thong bao khi cham 2 lan nut mo rong (Ë…) thong bao
    [Tags]    High    BP-2638
    Mo khoa voi mat khau 8888
    AppiumLibrary.Click Element    ${nut_huy_hoa}
    Sleep    2
    Vuot mo bang thong bao
    Sleep    2
    An nut mo rong thong bao
    Sleep    2
    Xoa toan bo thong bao
    Dong toan bo ung dung trong da nhiem
    Close Application

BP-2643
    [Documentation]     Hien thi tren cung ben phai.
    [Tags]    BP-2643    BP-2654    BP-2645    BP-2657    BP-2658    BP-2664
    Mo Launcher
    Vuot mo bang thong bao
    Sleep    2
    Log To Console    BP-2654     Check hien thi mau trang khi cam sac/ket noi voi may tinh qua cable
    Log To Console    BP-2657    Check vi tri cum thong tin thoi gian hien thi ben trai header
    Log To Console    BP-2658    Check noi dung cum thong tin thoi gian hien thi dung theo format [Gio:Phut â†µ Thu, Ngay/Thang]
    Log To Console    BP-2664    Check vi tri cum thong tin BMS hien thi ben phai header
    Log To Console    BP-2672    Check vi tri nut xoa tat ca nam chinh giua duoi day man hinh
    Chup anh man hinh va so sanh voi anh mau    notification    kc_d_ttdh_d_d_thong_bao_launcher    6
    Log To Console    BP-2645     Check mo quan ly Pin khi cham vao cum thong tin Pin
    Click Element    ${notify_icon_pin}
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    notification    kc_d_ttdh_d_d_cai_dat_pin    2
    Dong toan bo ung dung trong da nhiem

BP-2660
    [Documentation]    Check dinh dang thoi gian dung theo dinh dang da chon trong Setting.
    [Tags]    BP-2660    BP-2662
    Bat dinh dang 24h trong cai dat
    Sleep    2
    Vuot mo bang thong bao
    Page Should Contain Text    Appium Settings
    Log To Console    BP-2662    Check mo app Google lich khi cham cum thong tin gioi gian
    Click Element    ${notify_thong_tin_thoi_gian}
    Sleep    2
    Page Should Contain Element    ${notify_lich_google_title}
    Dong toan bo ung dung trong da nhiem

BP-2666
    [Documentation]    Check noi dung cum thong tin BMS hien thi logo BMS va thong tin theo format [Dien thoai â†µ duoc bao v
    [Tags]    BP-2666    BP-2667
    Vuot mo bang thong bao
    Sleep    2
    Page Should Contain Element    ${notify_icon_bms}
    Page Should Contain Text    Điện thoại
    Page Should Contain Text    được bảo vệ
    Log To Console    BP-2667    Check cham cum thong tin BMS mo app BMS
    Click Element    ${notify_icon_bms}
    Sleep    2
    Page Should Contain Element    ${bms_main_nut_diet_virus}
    Dong toan bo ung dung trong da nhiem
    Close Application

BP-2678
    [Documentation]    Check mo ung dung khi cham vao noi dung thong bao
    [Tags]    BP-2678
    Mo Launcher
    Vuot mo bang thong bao
    Sleep    2
    Click Element    ${notify_debug}
    Sleep    2
    Page Should Contain Element    ${notify_cd_tieu_de_nha_phat_trien}
    # Close Application

BP-2679
    [Documentation]    Check cham nut Ë… mo rong noi dung thong bao.
    [Tags]    BP-2679    BP-2680    BP-2682
    Mo Launcher
    Vuot mo bang thong bao
    Sleep    2
    ${so_nut_mo_rong}=    Get Matching Xpath Count    ${notify_nut_mo_rong}
    Run Keyword If    ${so_nut_mo_rong}>1    AppiumLibrary.Click Element    ${notify_nut_mo_rong_1}
    ...    ELSE    AppiumLibrary.Click Element    ${notify_nut_mo_rong}
    Page Should Contain Element    ${notify_txt_bat_truyen_tep}
    Log To Console    BP-2680     Check cham nut Ë„ thu gon noi dung thong bao.
    ${so_nut_thu_gon}=    Get Matching Xpath Count    ${notify_nut_thu_gon}
    Run Keyword If    ${so_nut_thu_gon}>1    AppiumLibrary.Click Element    ${notify_nut_thu_gon_1}
    ...    ELSE    AppiumLibrary.Click Element    ${notify_nut_thu_gon}
    Page Should Contain Text    Appium Settings
    Log To Console    BP-2682     Check cham nut Xoa se xoa thong bao.
    Xoa toan bo thong bao
    Page Should Not Contain Element    ${notify_nut_xoa_thong_bao}
    Dong toan bo ung dung trong da nhiem
    Close Application

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================