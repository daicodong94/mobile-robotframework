*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library    AppiumLibrary

Resource    ../page/dong_ho/bao_thuc.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup       Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-27
    [Documentation]    Mo app dong ho tu launcher
    [Tags]     High    BP-27    BP-316    BP-348    BP-329
    Mo Dong ho tu Launcher
    Cho phep cap quyen cho ung dung
    Log To Console     Xac nhan mo ung dung Dong ho thanh cong
    AppiumLibrary.Page Should Contain Element    ${dh_tab_bao_thuc}
    AppiumLibrary.Page Should Contain Element    ${dh_tab_dong_ho}
    AppiumLibrary.Page Should Contain Element    ${dh_tab_bo_hen_gio}
    AppiumLibrary.Page Should Contain Element    ${dh_tab_bam_gio}
    Log To Console     BP-316 Tao bao thuc nhanh
    Bam vao bao thuc nhanh
    Bam nut tao xong bao thuc
    AppiumLibrary.Page Should not Contain Element    ${dh_bt_btn_xong}
    AppiumLibrary.Page Should Contain Element    ${dh_cd_bao_thuc_ngan}
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_them_moi_bao_thuc_nhanh    16
    Log To Console     BP-348 bat va tat bao thuc nhanh
    Bam vao icon bat tat bao thuc
    Element Attribute Should Match    ${dh_cd_btn_nut_switch}   text    ${dh_bt_text_tat_bao_thuc}
    Bam vao icon tat bao thuc
    Element Attribute Should Match    ${dh_cd_btn_nut_switch_on}    text    ${dh_bt_text_bat_bao_thuc}
    Bam vao xoa
    ${so_phan_tu_hien_thi}    Get Matching Xpath Count    ${dh_bt_dem_phan_tu_hien_thi}
    Should Be Equal    ${so_phan_tu_hien_thi}    2
    Log To Console     BP-329 Tao bao thuc theo giao dien dong ho kim
    Bam vao them bao thuc
    Bam vao dong y bao thuc
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_them_moi_bao_thuc    16
    ${so_phan_tu_hien_thi}    Get Matching Xpath Count    ${dh_bt_dem_phan_tu_hien_thi}
    Should Be Equal    ${so_phan_tu_hien_thi}    3


BP-275
    [Documentation]    Kiem tra giao dien man hinh bao thuc cua app dong ho
    [Tags]    BP-275    BP-278    BP-280    BP-282    BP-285    BP-286    BP-290    BP-291    BP-293    BP-294
    ...    BP-296    BP-297    BP-300    BP-301
    Dong toan bo ung dung va ket thuc phien kiem thu
    Log To Console    kiem tra giao dien man hinh chinh
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_baothuc    4
    Bam vao text dong ho bao thuc hien thi
    Log To Console    kiem tra giao dien man hinh dong ho kim
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_dh_kim_baothuc    4
    Bam vao bao nut ban phim
    Log To Console    kiem tra giao dien man hinh dong ho so
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_dh_so_baothuc    4
    Vuot back ve giao dien truoc
    Bam vao nhan
    Click Element    ${dh_bt_label_nhan_text}
    Vuot back ve giao dien truoc
    Log To Console    kiem tra giao dien man hinh nhan dan
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_nhan_dan_baothuc    4
    Vuot back ve giao dien truoc
    Bam vao chuong
    Log To Console    kiem tra giao dien man hinh danh sach am thanh nhac chuong
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_am_thanh_baothuc    4
    Vuot back ve giao dien truoc
    Bam vao thu gon bao thuc
    Bam vao bao thuc nhanh
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_bt_nhanh_baothuc    4
    Close Application

BP-356
    [Documentation]    Check chuc nang chon cac ngay lap nhung bo tick Lap lai
    [Tags]    BP-356
    Log To Console    BP-356 Check chuc nang chon cac ngay lap nhung bo tick Lap lai
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Bam vao them bao thuc
    Bam vao dong y bao thuc
    Bam vao lap lai bao thuc
    Bam vao cac thu trong bao thuc 2 3 4
    Bam vao lap lai bao thuc
    Page Should Contain Element    ${dh_bt_nut_lap_lai_bao_thuc}
    Close Application

BP-320
    [Documentation]    Check mo tu giao dien quan ly bao thuc
    [Tags]    High    BP-320
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Bam vao bao thuc nhanh
    Log To Console    Check mo tu giao dien quan ly bao thuc
    AppiumLibrary.Page Should Contain Element    ${dh_bt_btn_xong}


BP-309
    [Documentation]    Check chuc nang huy tao bao thuc nhanh bang thao tac vuot back
    [Tags]    BP-309    BP-307
    Log To Console    BP-309: Check chuc nang huy tao bao thuc nhanh bang thao tac vuot back
    Go Back
    Log To Console    BP-307: check chuc nang huy tao bao thuc nhanh
    AppiumLibrary.Page Should not Contain Element    ${dh_bt_btn_xong}


BP-311
    [Documentation]    Check chuc nang huy tao bao thuc nhanh bang thao tac vuot home
    [Tags]    BP-311
    Log To Console    Check chuc nang huy tao bao thuc nhanh bang thao tac vuot home
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Bam vao bao thuc nhanh
    vuot tu mep duoi vao giua man hinh
    AppiumLibrary.Page Should not Contain Element    ${dh_bt_btn_xong}
    Close Application

BP-313
    [Documentation]    Check chuc nang di chuyen len/xuong cac moc thoi gian
    [Tags]    BP-313
    Log To Console    Check chuc nang di chuyen len/xuong cac moc thoi gian
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Bam vao bao thuc nhanh
    Vuot di chuyen cac moc thoi gian trong bao thuc nhanh
    ${get_text_expected}=    AppiumLibrary.Get Element Attribute    ${dh_cd_time_bao_thuc_sau}    text
    Close Application

BP-322
    [Documentation]    Check hien thi dung thoi gian bao thuc da chon
    [Tags]    BP-322
    Log To Console    Check hien thi dung thoi gian bao thuc da chon
    Bam vao them bao thuc
    Chon thoi gian bao thuc trong them bao thuc
    AppiumLibrary.Page Should Contain Element    ${dh_bt_nut_huy}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_hien_thi_thoi_gian_bao_thuc}

BP-324
    [Documentation]    Check chuc nang huy tao bao thuc dong ho kim
    [Tags]    BP-324
    Log To Console    Check chuc nang huy tao bao thuc dong ho kim
    Bam vao huy
    AppiumLibrary.Page Should Contain Element    ${dh_bt_bao_thuc_nhanh}

BP-326
    [Documentation]    Check chuc nang huy bao thuc bang cach vuot back
    [Tags]    BP-326
    Go Back
    AppiumLibrary.Page Should Contain Element    ${dh_bt_bao_thuc_nhanh}
    AppiumLibrary.Page Should Not Contain Element    ${dh_bt_nut_huy}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================