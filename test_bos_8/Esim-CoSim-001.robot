*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/esim/es_common.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup       Mo Launcher

*** Test Cases ***
# truong hop khong mang
BP-229
    [Documentation]    Check mo eSIM  tu Launcher
    [Tags]             High    BP-229
    Mo eSIM tu Launcher
    
BP-69
    [Documentation]    Check thao tac quet QR
    [Tags]             High    BP-69    
    Bam vao them ho so khay sim 1
    Sleep    3
    Page Should Contain Element    ${eSim_co_sim_dang_tai_file_ve}
    Sleep    50

B-221
    [Documentation]    Check xoa ho so dang active
    [Tags]             High    B-221    
    Bam vao button mong muon ho so sim 1
    bam vao profile nha mang - sim 1
    Sleep    2    
    Element Attribute Should Match    ${eSim_co_sim_hs_xoa}   enabled     false
    Sleep    2    
    bam vao huy    
    
BP-223
    [Documentation]    Check thao tac xoa ho so khong active
    [Tags]             High    BP-223    BP-264         
    ${ten_esim_truoc_khi_xoa}=    Get Text    ${eSim_co_lap_sim_ten_sim}
    Bam bo chon checkbox active sim 1
    Sleep    2    
    bam vao profile nha mang - sim 1
    bam vao xoa ho so
    Sleep    2
    Page Should Contain Text    Bạn có chắc chắn muốn xóa SIM này không?
    Sleep    2
    Bam vao Dong y
    Sleep    2    
    Log To Console    Check tai lai thanh cong ho so eSim khi vua xoa
    Sleep    1    
    Bam vao them ho so khay sim 1
    Sleep    3
    Page Should Contain Element    ${eSim_co_sim_dang_tai_file_ve}
    Sleep    50
    ${ten_esim_sau_khi_them}=    Get Text    ${eSim_co_lap_sim_ten_sim}
    Should Be Equal    ${ten_esim_truoc_khi_xoa}    ${ten_esim_sau_khi_them}         
    Sleep    5    

# # TRUONG HOP CO MANG - NHUNG K QUET MA QR
# BP-59
    # [Documentation]    Check dong ma QR bang thao tac vuot mep trai vao giua man hinh
    # [Tags]    BP-59
    # Bam vao them ho so khay sim 1
    # Sleep    2    
    # vuot tu trai sang dong ma QR
    # Sleep    2    
    # Page Should not Contain Element    ${eSim_co_sim_giao_dien_QR}
    # Sleep    3    
    # Bam vao them ho so khay sim 1
    # Sleep    2    
    # Page Should Contain Element    ${eSim_co_sim_giao_dien_QR}
    # Log To Console   Check dong ma QR bang thao tac vuot mep phai vao giua man hinh
    # vuot tu phai sang dong ma QR
    # Sleep    2    
    # Page Should not Contain Element    ${eSim_co_sim_giao_dien_QR}
    # Sleep    3    
    
# BP-62
    # [Documentation]    Check thao tac Bam vao them ho so khay sim 1
    # [Tags]    BP-62
    # Bam vao them ho so khay sim 1
    # Sleep    2    
    # Page Should Contain Element    ${eSim_co_sim_giao_dien_QR}
    # Sleep    20    
    # Bam vao dong
    
# BP-63
    # [Documentation]    Check dong thong bao bang thao tac vuot home
    # [Tags]    BP-63
    # Bam vao them ho so khay sim 1
    # Page Should Contain Element    ${eSim_co_sim_giao_dien_QR}
    # Sleep    2
    # Vuot quay ve man hinh Home
    # Page Should Contain Element      ${app_esim}
    # Bam nut dong toan bo ung dung trong da nhiem
    
#TRUONG HOP CO MANG - CO MA QR DE QUET
    
BP-70
    [Documentation]    Check thao tac quet QR
    [Tags]             BP-70    BP-71 
    Mo eSIM tu Launcher
    Bam vao them ho so khay sim 1
    Sleep    2    
    Page Should Contain Element    ${eSim_co_sim_dang_tai_file_ve}
    Sleep    50
    Page Should Contain Text    ${eSim_co_sim_txt_file_loi}    
    Log To Console    Check dong thong bao bang thao tac bam vao dong    
    Bam vao dong
    Sleep    2    

BP-72
    [Documentation]    Check dong thong bao bang thao tac vuot back
    [Tags]    BP-72
    Bam vao them ho so khay sim 1
    Sleep    2    
    Page Should Contain Element    ${eSim_co_sim_dang_tai_file_ve}
    Sleep    20   
    vuot tu trai sang dong ma QR
    Sleep    2    
    Page Should Contain Element    ${eSim_co_sim_icon_sim}
    Bam vao them ho so khay sim 1
    Sleep    2    
    Page Should Contain Element    ${eSim_co_sim_dang_tai_file_ve}
    Sleep    20    
    vuot tu phai sang dong ma QR
    Sleep    2    
    Page Should Contain Element    ${eSim_co_sim_icon_sim}

BP-74
    [Documentation]    Check dong thong bao bang thao tac vuot home
    [Tags]             BP-74
    Bam vao them ho so khay sim 1
    Sleep    2    
    Page Should Contain Element    ${eSim_co_sim_dang_tai_file_ve}
    Vuot quay ve man hinh Home
    Mang di dong = 1, data = 0, wifi = 0, may bay = 0
    Sleep    2    
    Page Should Not Contain Element     ${eSim_co_sim_dang_tai_file_ve}
    Sleep    3    
    vuot tu phai sang dong ma QR
    Bam vao dong
    Sleep    2    
    
#TRUONG HOP KHONG CO MANG - KHONG CO MA QR    
BP-42
    [Documentation]    Check thao tac Bam vao them ho so khay sim 1
    [Tags]    BP-42
    Mo eSIM tu Launcher
    Bam vao them ho so khay sim 1
    Sleep    2    
    Page Should Contain Element    ${eSim_co_sim_thong_bao}
    Page Should Contain Element    ${eSim_co_sim_dong}
    # Chup anh man hinh va so sanh voi anh mau    ${esim_alias}    kc_d_ttdh_d_d_esim_cosim_them_hs        
    
BP-43
    [Documentation]    Check dong thong bao bang thao tac bam vao dong
    [Tags]    BP-43
    Bam vao dong
    Sleep    2    
    Page Should Not Contain Element    ${eSim_co_sim_dong}
    Page Should Contain Element    ${eSim_co_sim_add_sim}
    # Chup anh man hinh va so sanh voi anh mau    ${esim_alias}    kc_d_ttdh_d_d_esim_cosim_ko_mang_main_sim2    2

BP-50
    [Documentation]    Check dong thong bao bang thao tac vuot mep trai vao giua man hinh
    [Tags]    BP-50
    Bam vao them ho so khay sim 1
    Sleep    2    
    Page Should Contain Element    ${eSim_co_sim_thong_bao}
    Page Should Contain Element    ${eSim_co_sim_dong}
    Sleep    2    
    Vuot back ve giao dien truoc
    Page Should Not Contain Element    ${eSim_co_sim_dong}
    Page Should Contain Element    ${eSim_co_sim_add_sim}
    # Chup anh man hinh va so sanh voi anh mau    ${esim_alias}    kc_d_ttdh_d_d_esim_cosim_ko_mang_main_sim2    2
    Log To Console   Check dong thong bao bang thao tac vuot mep trai vao giua man hinh
    Bam vao them ho so khay sim 1
    Sleep    2    
    Page Should Contain Element    ${eSim_co_sim_thong_bao}
    Page Should Contain Element    ${eSim_co_sim_dong}
    vuot tu mep trai vao giua man hinh
    Sleep    2    
    Page Should Not Contain Element    ${eSim_co_sim_dong}
    Page Should Contain Element    ${eSim_co_sim_add_sim}
    # Chup anh man hinh va so sanh voi anh mau    ${esim_alias}    kc_d_ttdh_d_d_esim_cosim_ko_mang_main_sim2    2

BP-52
    [Documentation]    Check dong thong bao bang thao tac vuot home
    [Tags]    BP-52
    Bam vao them ho so khay sim 1
    Sleep    2
    Page Should Contain Element    ${eSim_co_sim_thong_bao}
    Page Should Contain Element    ${eSim_co_sim_dong}
    Vuot quay ve man hinh Home
    Sleep    2    
    Page Should Not Contain Element     ${eSim_co_sim_dong}
    Page Should Not Contain Element     ${eSim_add_icon_sim_1}  
    Page Should Contain Element      ${app_esim}
    Mang di dong = 1, data = 1, wifi = 1, may bay = 0

BP-87
    [Documentation]    Check chuc nang them so dien thoai
    [Tags]             BP-87
    Mo eSIM tu Launcher
    Bam vao dong
    Sleep    2    
    bam vao profile nha mang - sim 1
    bam vao so dien thoai trong ho so
    clear text so dien thoai
    nhap text
    bam vao luu
    Sleep    2
    Page Should Contain Element     ${eSim_co_sim_icon_sim}
    # Chup anh man hinh va so sanh voi anh mau    ${esim_alias}    kc_d_ttdh_d_d_esim_cosim_sim1_co_mang_hien_sdt    2
    
BP-88
    [Documentation]    Check chuc nang huy them so dien thoai
    [Tags]    BP-88
    bam vao profile nha mang - sim 1
    Sleep    2    
    bam vao so dien thoai trong ho so
    clear text so dien thoai
    nhap text
    bam vao huy
    Sleep    2
    Page Should Contain Element     ${eSim_co_sim_icon_sim}
    # Chup anh man hinh va so sanh voi anh mau    ${esim_alias}    kc_d_ttdh_d_d_esim_cosim_sim1_co_mang_hien_sdt    2

BP-188
    [Documentation]    Check chuc nang huy thao tac xoa het so dien thoai
    [Tags]    BP-188
    bam vao profile nha mang - sim 1
    Sleep    2    
    bam vao so dien thoai trong ho so
    clear text so dien thoai
    bam vao huy
    Sleep    2
    Page Should Contain Element     ${eSim_co_sim_icon_sim}
    # Chup anh man hinh va so sanh voi anh mau    ${esim_alias}    kc_d_ttdh_d_d_esim_cosim_sim1_co_mang_hien_sdt    2

BP-190
    [Documentation]    Check chuc nang chinh so dien thoai
    [Tags]    BP-190
    Mo eSIM tu Launcher
    bam vao profile nha mang - sim 1
    bam vao so dien thoai trong ho so
    clear text so dien thoai
    nhap so dien thoai moi
    bam vao luu
    Sleep    2
    Page Should Contain Element     ${eSim_co_sim_icon_sim}
    # Chup anh man hinh va so sanh voi anh mau    ${esim_alias}    kc_d_ttdh_d_d_esim_cosim_sim1_co_mang_hien_sdt    2

BP-202
    [Documentation]    Check chuc nang huy thao tac chinh so dien thoai
    [Tags]    BP-202
    bam vao profile nha mang - sim 1
    bam vao so dien thoai trong ho so
    clear text so dien thoai
    bam vao huy
    Sleep    2
    Page Should Contain Element     ${eSim_co_sim_icon_sim}
    # Chup anh man hinh va so sanh voi anh mau    ${esim_alias}    kc_d_ttdh_d_d_esim_cosim_sim1_co_mang_hien_sdt    2

BP-89
    [Documentation]    Check chuc nang xoa so dien thoai
    [Tags]    BP-89
    bam vao profile nha mang - sim 1
    bam vao so dien thoai trong ho so
    clear text so dien thoai
    bam vao luu
    Sleep    1
    Page Should Contain Element     ${eSim_co_sim_icon_sim}    
    # Chup anh man hinh va so sanh voi anh mau    ${esim_alias}    kc_d_ttdh_d_d_esim_cosim_ko_mang_main_sim2    2

BP-203
    [Documentation]    Check thao tac bam vao combobox
    [Tags]    BP-203
    bam vao profile nha mang - sim 1
    bam vao combobox mau sac
    Sleep    2
    Page Should Contain Element    ${eSim_ma_mau_2}  
    Page Should Contain Element    ${eSim_ma_mau_3}  
    Page Should Contain Element    ${eSim_ma_mau_4}  
    Page Should Contain Element    ${eSim_ma_mau_5}  
    Page Should Contain Element    ${eSim_ma_mau_6}
    Page Should Contain Element    ${eSim_ma_mau_7}  
    Page Should Contain Element    ${eSim_ma_mau_8}  
    Page Should Contain Element    ${eSim_ma_mau_9}  
    Sleep    2    
    # Chup anh man hinh va so sanh voi anh mau    ${esim_alias}    kc_d_ttdh_d_d_esim_cosim_sim1_co_mang_mau    2
    Vuot back ve giao dien truoc
    Vuot back ve giao dien truoc

BP-212
    [Documentation]    Check chuc nang huy bang vuot back
    [Tags]    BP-214    BP-212
    Log To Console   Check thao tac xoa ho so khong active
    Bam vao button mong muon ho so sim 1
    Sleep    2    
    bam vao xoa ho so
    Page Should Contain Element    ${eSim_co_sim_hs_sim_nut_huy}
    vuot tu trai sang dong ma QR
    Page Should Contain Element   ${eSim_co_sim_icon_sim}
    bam vao xoa ho so
    Page Should Contain Element    ${eSim_co_sim_hs_sim_nut_huy}
    vuot tu phai sang dong ma QR
    Page Should Contain Element   ${eSim_co_sim_icon_sim}
    
BP-215
    [Documentation]    Check chuc nang huy bang vuot home
    [Tags]   BP-215
    Log To Console   Check chuc nang huy bang vuot home
    Vuot quay ve man hinh Home

BP-219
    [Documentation]    Check thao tac bam vao dong y
    [Tags]   BP-219
    Log To Console   Check thao tac bam vao dong y
    Mo eSIM tu Launcher
    bam vao esim 2 viettel
    bam vao xoa ho so
    Page Should Contain Element    ${eSim_co_sim_hs_sim_nut_huy}
    Bam vao Dong y
    Page Should Contain Element   ${eSim_co_sim_icon_sim}
    
# #Khi lap 2 eSim - SIM 1(Viettel), SIM 2(Mobile)    
# BP-236
    # [Documentation]    Check chuc nang thay doi ho so active
    # [Tags]             Co-2-eSim    BP-236
    # Log To Console   Check chuc nang thay doi sang ho so sim 1
    # Sleep    2    
    # Bam vao button mong muon ho so sim 1
    # Element Attribute Should Match    ${eSim_co_sim_chon_checkbox_sim_1}    checked    True
    # Element Attribute Should Match    ${eSim_co_sim_chon_checkbox_sim_2}    checked    false
    # Sleep    2    
    # Log To Console   Check chuc nang thay doi sang ho so sim 2
    # Bam vao button mong muon ho so sim 2
    # Element Attribute Should Match    ${eSim_co_sim_chon_checkbox_sim_1}    checked    false
    # Element Attribute Should Match    ${eSim_co_sim_chon_checkbox_sim_2}    checked    true    
    # Sleep    2    
    

#=================================================================================================
#    HET NOI DUNG TAP TIN
# =================================================================================================