*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/dien_thoai.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/btalk/nhat_ki_cuoc_goi.robot
Resource    ../page/btalk/gui_nhan_tin_nhan.robot
Resource    ../page/cai_dat/cd_common.robot

# Suite Setup    Mo Launcher

*** Test Cases ***
BP-3013
    [Documentation]    Check chuc nang hien cac ma an cua he dieu hanh android
    [Tags]    BP-3013     1sim
    Log To Console    Check hien thi IMEI
    Mo Btalk
    Nhap ma *#06#
    Page Should Contain Text    IMEI
    Bam nut dong y
    Log To Console    Check hien thi thong tin dien thoai, thong tin pin, nguon goc
    Nhap ma *#*#4636#*#*
    Page Should Contain Text    Thông tin điện thoại
    Vuot quay ve man hinh Home
    Bam mo app dien thoai
    Log To Console    Check ma *#301279#
    Nhap ma *#301279#
    Page Should Contain Text    *#301279#
    Long Press    ${dt_icon_xoa_khi_nhap_so}
    Log To Console   check ma *#*#1111#*#*
    Nhap ma *#*#1111#*#*
    Page Should Not Contain Text    *#*#1111#*#*
    Log To Console    check ma*#*#2222#*#*
    Nhap ma *#*#2222#*#*
    Page Should Not Contain Text    *#*#2222#*#*
    Log To Console    check ma*#*#2663#*#*
    Nhap ma *#*#2663#*#*
    Page Should Not Contain Text    *#*#2663#*#*

BP-5788
    [Documentation]    Check ma an
    [Tags]    BP-5788
    Mo Btalk
    Log To Console     *#*#232331#*#*
    nhap ma *#*#232331#*#*
    Page Should Not Contain Text    *#*#2663#*#*
    Log To Console    \#*#232337#*#
    Nhap ma #*#232337#*#
    Page Should Contain Text    \#*#232337#*#
    Long Press    ${dt_icon_xoa_khi_nhap_so}
    Log To Console    *#*#1472365#*#*
    nhap ma *#*#1472365#*#*
    Page Should Not Contain Text    *#*#1472365#*#*
    Log To Console    *#*#1575#*#*
    Nhap ma *#*#1575#*#*
    Page Should Not Contain Text    *#*#1575#*#*
    Nhap ma *#*#528#*#*
    Page Should Not Contain Text    *#*#528#*#*
    Log To Console      *#*#526#*#*
    Nhap ma *#*#526#*#*
    Page Should Not Contain Text    *#*#526#*#*
    Log To Console    *#*#232338#*#*
    nhap ma *#*#232338#*#*
    Page Should Not Contain Text    *#*#232338#*#*
    Log To Console    *#*#0842#*#*
    nhap ma *#*#0842#*#*
    Page Should Not Contain Text    *#*#0842#*#*
    Log To Console    *#*#0588#*#*
    Nhap ma *#*#0588#*#*
    Page Should Not Contain Text    *#*#0588#*#*
    Log To Console    *#*#0289#*#*
    Nhap ma *#*#0289#*#*
    Page Should Not Contain Text    *#*#0289#*#*
    Log To Console    *#*#0673#*#*
    Nhap ma *#*#0673#*#*
    Page Should Not Contain Text    *#*#0673#*#*
    Log To Console    *#*#197328640#*#*
    Nhap ma *#*#197328640#*#*
    Page Should Not Contain Text    *#*#197328640#*#*
    Log To Console    *#*#7262626#*#*
    nhap ma *#*#7262626#*#*
    Page Should Not Contain Text    *#*#7262626#*#*
    Log To Console    *2767*3855#
    Nhap ma *2767*3855#
    Page Should Contain Text    *2767*3855#
    Long Press    ${dt_icon_xoa_khi_nhap_so}
    Log To Console    *#*#7780#*#*
    Nhap ma *#*#7780#*#*
    Page Should Not Contain Text    *#*#7780#*#*
    Log To Console    *#*#273282*255*663282*#*#*
    Nhap ma *#*#273282*255*663282*#*#*
    Page Should Not Contain Text    *#*#273282*255*663282*#*#*
    Log To Console    *#*#34971539#*#*
    Nhap ma *#*#34971539#*#*
    Page Should Not Contain Text    *#*#34971539#*#*

BP-5789
    [Documentation]    Check ma an tiep
    [Tags]    BP-5789
    Mo Btalk
    Log To Console    *#12580*369#
    Nhap ma *#12580*369#
    Page Should Contain Text    *#12580*369#
    Long Press    ${dt_icon_xoa_khi_nhap_so}
    Log To Console    *#*#8255#*#*
    Nhap ma *#*#8255#*#*
    Page Should Not Contain Text    *#*#8255#*#*
    Log To Console    *#*#4986*2650468#*#*
    Nhap ma *#*#4986*2650468#*#*
    Page Should Not Contain Text    *#*#4986*2650468#*#*
    Log To Console     *#*#44336#*#*
    Nhap ma *#*#44336#*#*
    Page Should Not Contain Text    *#*#44336#*#*
    Log To Console    *#*#0283#*#*
    Nhap ma *#*#0283#*#*
    Page Should Not Contain Text    *#*#0283#*#*
    Log To Console    *#*#34971539#*#*
    Nhap ma *#*#34971539#*#*
    Page Should Not Contain Text    *#*#34971539#*#*

BP-5790
    [Documentation]    check ma an part 4
    [Tags]    BP-5790
    Mo Btalk
    Log To Console    *#12580*369#
    Nhap ma *#12580*369#
    Page Should Contain Text    *#12580*369#
    Long Press     ${dt_icon_xoa_khi_nhap_so}
    Log To Console    *#301279#
    Nhap ma *#301279#
    Page Should Contain Text    *#301279#
    Long Press     ${dt_icon_xoa_khi_nhap_so}
    Log To Console    *#*#8255#*#*
    Nhap ma *#*#8255#*#*
    Page Should Not Contain Text    *#*#8255#*#*
    Log To Console    *#*#4986*2650468#*#*
    Nhap ma *#*#4986*2650468#*#*
    Page Should Not Contain Text    *#*#4986*2650468#*#*
    Log To Console     *#*#44336#*#*
    Nhap ma *#*#44336#*#*
    Page Should Not Contain Text    *#*#44336#*#*
    Log To Console    *#*#0283#*#*
    Nhap ma *#*#0283#*#*
    Page Should Not Contain Text    *#*#0283#*#*
    Log To Console    *#*#0*#*#*
    Nhap ma *#*#0*#*#*
    Page Should Not Contain Text    *#*#0*#*#*
    Log To Console    *#0*#
    Nhap ma *#0*#
    Page Should Contain Text    *#0*#
    Long Press     ${dt_icon_xoa_khi_nhap_so}
    Log To Console    *#9090#
    Nhap ma *#9090#
    Page Should Contain Text    *#9090#
    Long Press     ${dt_icon_xoa_khi_nhap_so}
    Log To Console    *#872564#
    Nhap ma *#872564#
    Page Should Contain Text    *#872564#
    Long Press     ${dt_icon_xoa_khi_nhap_so}
    Log To Console      *#9900#
    Nhap ma *#9900#
    Page Should Contain Text    *#9900#
    Long Press     ${dt_icon_xoa_khi_nhap_so}
    Log To Console    *#7465625#
    Nhap ma *#7465625#
    Page Should Contain Text    *#7465625#
    Long Press     ${dt_icon_xoa_khi_nhap_so}
    Log To Console    **05**#
    Nhap ma **05**#
    An nut OK
    Log To Console    *#*#8351#*#*
    Nhap ma *#*#8351#*#*
    Page Should Not Contain Text    *#*#8351#*#*
    Log To Console    *#*#8350#*#*
    Nhap ma *#*#8350#*#*
    Page Should Not Contain Text    *#*#8350#*#*
    Log To Console    *#*#7594#*#*
    Nhap ma *#*#7594#*#*
    Page Should Not Contain Text    *#*#7594#*#*
    Log To Console    \##7764726
    Nhap ma ##7764726
    Page Should Contain Text    \##7764726

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================