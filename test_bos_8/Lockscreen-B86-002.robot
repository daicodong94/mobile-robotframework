*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/lockscreen/lockscreen.robot
Resource    ../page/camera/cam_common.robot
Resource    ../page/notification/notification.robot
Suite Setup    Mo Cai dat
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu
*** Test Cases ***

BP-4775
    [Documentation]    Check hien thi giao dien THONG TIN KHAN CAP (ban phim so Btalk) khi cham KHAN CAP
    [Tags]    BP-4775    BP-5104
    Log To Console    Check hien thi giao dien THONG TIN KHAN CAP (ban phim so Btalk) khi cham KHAN CAP
    Lock    1
    Cham toa do
    Sleep    2
    vuot mo nhap ma pin va mat khau
    Click vao nut khan cap
    Chup anh man hinh va so sanh voi anh mau    ${lockscreen_alias}    ${bphone}_kc_d_ttdh_d_d_khan_cap    4
    Log To Console    BP-5104: Check chuc nang button THONG TIN KHAN CAP khi cham 1 lan
    Click vao nut thong tin khan cap
    # Capture Page Screenshot    report/b86_kc_d_ttdh_d_d_thong_tin_khan_cap.png
    Chup anh man hinh va so sanh voi anh mau    ${lockscreen_alias}    ${bphone}_kc_d_ttdh_d_d_thong_tin_khan_cap    4
    Click drop vao nut thong tin khan cap
    Kiem tra hien thi man hinh chu so huu
    Click quay lai man hin thong tin khan cap
    vuot mo nhap ma pin va mat khau
    Click vao nut khan cap
    Cham drop vao nut thong tin khan cap
    Kiem tra hien thi man hinh chu so huu

BP-5111
    [Documentation]    Check bat camera khi vuot cheo tu ben phai man hinh
    [Tags]    BP-5111
    Log To Console    Check bat camera khi vuot cheo tu ben phai man hinh
    Click quay lai man hin thong tin khan cap
    Vuot cheo man hinh khoa mo may anh
    Xac nhan mo CHUP ANH thanh cong
    Go Back
    Mo khoa voi mat khau 8888

BP-5124
    [Documentation]    Check hien thi khoi thoi gian tren man hinh khoa khi bat su dung dinh dang 24h
    [Tags]     BP-5124
    Log To Console    Check hien thi khoi thoi gian tren man hinh khoa khi bat su dung dinh dang 24h
    Vuot toa do de hien thi tab he thong
    Vuot toa do de hien thi tab he thong
    Nhan vao tab he thong
    Nhan vao tab cai dat ngay va gio
    Kiem tra hien thi trang thai mac dinh gio theo dia phuong
    Click bat tat hien thi thoi gian 24h
    Lock    1
    Cham toa do
    Sleep    2
    Kiem tra gio dinh dang 24h them CH hoac SA
    Mo khoa voi mat khau 8888
    Kiem tra hien thi trang thai mac dinh gio theo dia phuong

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================