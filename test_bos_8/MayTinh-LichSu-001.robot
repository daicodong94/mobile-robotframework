*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library    AppiumLibrary

Resource    ../page/may_tinh/lich_su.robot
Resource    ../page/may_tinh/mt_common.robot

Suite Setup    Mo May tinh
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-385
    [Documentation]    Check mo lich su bang thao tac cham vao lich su
    [Tags]    BP-385
    An nut lich su
    Page Should Contain Element    ${mt_txt_khong_co_lich_su}

BP-387
    [Documentation]    Kiem tra giao dien lich su cua ung dung May tinh khi chua co du lieu
    [Tags]    BP-387    BP-388    BP-393    BP-395    BP-397    BP-399    BP-401    BP-403    BP-405
    ...    BP-435    BP-457    BP-471
    Chup anh man hinh va so sanh voi anh mau    ${mt_alias}    kc_d_ttdh_d_d_ls_ko_data    2

BP-416
    [Documentation]    Check giao dien lich su khi co du lieu
    [Tags]    BP-416    BP-413    BP-414    BP-415    BP-417    BP-418    BP-419
    ...    BP-420    BP-421    BP-422    BP-423    BP-425    BP-427    BP-429    BP-431
    Bam vao nut lich su
    An nut 456
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    An nut Clear xoa ket qua
    An nut 456
    An nut dau nhan (x)
    An nut 123
    An nut dau (=)
    An nut Clear xoa ket qua
    An nut 456
    An nut dau cong (+)
    An nut 123
    An nut dau (=)
    An nut Clear xoa ket qua
    An nut 456
    An nut dau chia (÷)
    An nut 123
    An nut dau (=)
    An nut Clear xoa ket qua
    An nut 456
    An nut dau tru (-)
    An nut 456
    An nut dau (=)
    An nut Clear xoa ket qua
    Bam vao nut lich su
    Chup anh man hinh va so sanh voi anh mau    ${mt_alias}    kc_d_ttdh_d_d_ls_co_data    2
    Swipe By Percent    65    50    65    85
    ${get_text}=    Get Text    ${mt_text_hien_thi_phep_tinh_ls}
    Element Attribute Should Match    ${mt_text_hien_thi_phep_tinh_ls}    text    *456−123*
    Swipe By Percent    65    85   65    50
    ${get_text1}=    Get Text    ${mt_text_hien_thi_phep_tinh_ls1}
    Element Attribute Should Match    ${mt_text_hien_thi_phep_tinh_ls1}    text    456−456
    Page Should Contain Element    ${mt_nut_xoa_lich_su}
    Xoa lich su

BP-557
    [Documentation]    Check dong Lich su bang thao tac cham vao Lich su
    [Tags]    BP-557
    Bam vao nut lich su
    Sleep    2
    Page Should Not Contain Text    ${mt_txt_khong_co_lich_su}

BP-386
    [Documentation]    Check mo lich su bang thao tac vuot
    [Tags]    BP-386
    Vuot mo giao dien lich su
    Sleep    2
    Page Should Contain Element    ${mt_txt_khong_co_lich_su}
    sleep    2

BP-558
    [Documentation]    Check dong lich su bang thao tac vuot
    [Tags]    BP-558
    Dong lich su bang thao tac vuot
    Page Should Not Contain Text    ${mt_txt_khong_co_lich_su}

BP-551
    [Documentation]    Check chuc nang Xoa lich su
    [Tags]    BP-551
    Thuc hien phep tinh
    Bam vao nut lich su
    Xoa lich su
    AppiumLibrary.Page Should Not Contain Element    ${mt_nut_xoa_lich_su}
    Page Should Contain Element    ${mt_txt_khong_co_lich_su}

BP-552
    [Documentation]    Check chon ket qua trong lich su
    [Tags]    BP-552
    Dong lich su bang thao tac vuot
    Thuc hien phep tinh
    Bam vao nut lich su
    Bam ket qua trong lich su
    Get Element Attribute    ${mt_nut_ket_qua_ls}    text
    ${get_text_expected}=    AppiumLibrary.Get Element Attribute     ${mt_hien_thi_phep_tinh}    text
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    4


BP-553
    [Documentation]    Check ghep nhieu ket qua tu lich su
    [Tags]    BP-553
    Bam vao nut lich su
    Thuc hien phep tinh
    Bam vao nut lich su
    Bam ket qua trong lich su
    Bam ket qua trong lich su
    Bam ket qua trong lich su
    Bam vao nut lich su
    ${get_text_expected}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    444


BP-555
    [Documentation]    Check thuc hien phep tinh voi cac ket qua tu lich su
    [Tags]    BP-555
    An nut dau cong (+)
    An nut 123
    Bam vao nut bang
    ${get_text_expected}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    567
    An nut Clear xoa ket qua

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================