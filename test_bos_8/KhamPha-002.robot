*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/chim_lac/cl_common.robot
Resource    ../page/kham_pha/kp_common.robot

Suite Setup       Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-1
    [Documentation]    Check mo app Kham pha tu Launcher
    [Tags]     High    BP-1    BP-4    BP-7    BP-16    BP-18
    Mo Kham pha tu Launcher
    Log To Console     BP-4 : Check vi tri nut [>] tren giao dien huong dan
    Log To Console     BP-7 : Check thiet ke cua cac huong dan
    Log To Console     BP-16 : Kiem tra huong dan mac dinh khi moi mo app
    Log To Console     BP-18 : Check chuyen qua lai giua cac huong dan dung theo thu tu
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_01    2

BP-11
    [Documentation]    Check khong chuyen sang huong dan khac khi vuot tu trai sang phai tai giao dien huong dan dau tien
    [Tags]    BP-11
    Vuot tu trai sang phai de chuyen ve huong dan truoc do

BP-9
    [Documentation]    Check khong hien thi nut [<] o giao dien huong dan dau tien
    [Tags]     BP-9
    AppiumLibrary.Page Should Not Contain Element    ${kp_nut_hd_truoc}

BP-6
    [Documentation]    Check chuyen sang huong dan tiep theo khi Bam nut [>]
    [Tags]     BP-6    BP-4    BP-7    BP-8    BP-18
    Log To Console     BP-4 : Check vi tri nut [>] tren giao dien huong dan
    Log To Console     BP-7 : Check thiet ke cua cac huong dan
    Log To Console     BP-8 : Check vi tri nut [<] tren giao dien huong dan
    Log To Console     BP-18 : Check chuyen qua lai giua cac huong dan dung theo thu tu
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_02    2
    AppiumLibrary.Page Should Contain Element   ${kp_nut_hd_truoc}
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_03    2
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_04    2
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_05    2
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_06    2
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_07    2
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_08    2
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_09    2
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_10    2
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_11    2
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_12    2
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_13    2
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_14    2
    Bam nut [>] de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_15    2

BP-19
    [Documentation]    Check bam mo trang web huong dan su dung tinh nang chong trom bang cach bam vao vung duoi hinh anh dien thoai
    [Tags]    BP-19
    Bam vao vung duoi hinh anh dien thoai de xem huong dan
    Bat Che do thu gon cua Chim Lac bang popup
    Truy cap trang web co canh bao khong an toan
    AppiumLibrary.Page Should Contain Text    ${kp_txt_chong_trom}
    Quay ve ung dung Kham pha

BP-14
    [Documentation]    Check chuyen sang huong dan sau khi vuot tu phai sang trai tai giao dien huong dan khac
    [Tags]    BP-14    BP-4    BP-7    BP-8    BP-18
    Log To Console     BP-4 : Check vi tri nut [>] tren giao dien huong dan
    Log To Console     BP-7 : Check thiet ke cua cac huong dan
    Log To Console     BP-8 : Check vi tri nut [<] tren giao dien huong dan
    Log To Console     BP-18 : Check chuyen qua lai giua cac huong dan dung theo thu tu
    Vuot tu phai sang trai de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_16    2
    Vuot tu phai sang trai de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_17    2
    Vuot tu phai sang trai de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_18    2

BP-21
    [Documentation]    Check bam mo trang web huong dan Chuyen doi du lieu tu Android sang BOS bang cach bam vao vung duoi hinh anh dien thoai
    [Tags]    BP-21
    Bam vao vung duoi hinh anh dien thoai de xem huong dan
    AppiumLibrary.Page Should Contain Text    ${kp_txt_android}
    Quay ve ung dung Kham pha

BP-23
    [Documentation]    Check bam mo trang web huong dan Chuyen doi du lieu tu IOS sang BOS bang cach bam vao vung duoi hinh anh dien thoai
    [Tags]    BP-23    BP-4    BP-7    BP-8    BP-14    BP-18
    Log To Console     BP-4 : Check vi tri nut [>] tren giao dien huong dan
    Log To Console     BP-7 : Check thiet ke cua cac huong dan
    Log To Console     BP-8 : Check vi tri nut [<] tren giao dien huong dan
    Log To Console     BP-14 : Check chuyen sang huong dan sau khi vuot tu phai sang trai tai giao dien huong dan khac
    Log To Console     BP-18 : Check chuyen qua lai giua cac huong dan dung theo thu tu
    Vuot tu phai sang trai de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_19    2
    Log To Console     BP-23 : Check bam mo trang web huong dan Chuyen doi du lieu tu IOS sang BOS bang cach bam vao vung duoi hinh anh dien thoai
    Bam vao vung duoi hinh anh dien thoai de xem huong dan
    AppiumLibrary.Page Should Contain Text    ${kp_txt_ios}
    Quay ve ung dung Kham pha

BP-25
    [Documentation]    Check bam mo trang web huong dan Mot so luu y khi sac pin bang cach bam vao vung duoi hinh anh dien thoai
    [Tags]    BP-25    BP-4    BP-7    BP-8    BP-14    BP-18
    Log To Console     BP-4 : Check vi tri nut [>] tren giao dien huong dan
    Log To Console     BP-7 : Check thiet ke cua cac huong dan
    Log To Console     BP-8 : Check vi tri nut [<] tren giao dien huong dan
    Log To Console     BP-14 : Check chuyen sang huong dan sau khi vuot tu phai sang trai tai giao dien huong dan khac
    Log To Console     BP-18 : Check chuyen qua lai giua cac huong dan dung theo thu tu
    Vuot tu phai sang trai de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_20    2
    Vuot tu phai sang trai de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_21    2
    Vuot tu phai sang trai de chuyen sang huong dan tiep theo
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_22    2
    Log To Console     BP-25 : Check bam mo trang web huong dan Mot so luu y khi sac pin bang cach bam vao vung duoi hinh anh dien thoai
    Bam vao vung duoi hinh anh dien thoai de xem huong dan
    AppiumLibrary.Page Should Contain Text    ${kp_txt_sac_pin}

BP-2
    [Documentation]    Check mo app Kham pha tu da nhiem
    [Tags]     BP-2    BP-17
    Mo da nhiem
    Bam mo ung dung trai trong 2 ung dung tren da nhiem
    Log To Console     BP-17 : Kiem tra huong dan khi mo lai app
    AppiumLibrary.Page Should Contain Element    ${kp_nut_xem_huong_dan}
    AppiumLibrary.Page Should Contain Element    ${kp_nut_hd_truoc}

BP-5
    [Documentation]    Check khong hien thi nut [>] o giao dien huong dan cuoi cung
    [Tags]     BP-5    BP-13
    Log To Console     BP-13 : Check khong chuyen sang huong dan khac khi vuot tu phai sang trai tai giao dien huong dan cuoi cung
    Vuot tu phai sang trai de chuyen sang huong dan tiep theo
    Log To Console     BP-5 : Check khong hien thi nut [>] o giao dien huong dan cuoi cung
    AppiumLibrary.Page Should Not Contain Element    ${kp_nut_hd_sau}

BP-10
    [Documentation]    Check chuyen ve huong dan truoc do khi bam nut [<]
    [Tags]    BP-10    BP-4    BP-7    BP-8    BP-18
    Log To Console     BP-4 : Check vi tri nut [>] tren giao dien huong dan
    Log To Console     BP-7 : Check thiet ke cua cac huong dan
    Log To Console     BP-8 : Check vi tri nut [<] tren giao dien huong dan
    Log To Console     BP-18 : Check chuyen qua lai giua cac huong dan dung theo thu tu
    Bam nut [<] de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_21    2
    Bam nut [<] de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_20    2
    Bam nut [<] de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_19    2
    Bam nut [<] de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_18    2
    Bam nut [<] de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_17    2
    Bam nut [<] de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_16    2
    Bam nut [<] de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_15    2
    Bam nut [<] de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_14    2
    Bam nut [<] de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_13    2
    Bam nut [<] de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_12    2
    Bam nut [<] de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_11    2
    Bam nut [<] de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_10    2

BP-12
    [Documentation]    Check chuyen ve huong dan truoc khi vuot tu trai sang phai tai giao dien huong dan khac
    [Tags]    BP-12    BP-4    BP-7    BP-8    BP-18
    Log To Console     BP-4 : Check vi tri nut [>] tren giao dien huong dan
    Log To Console     BP-7 : Check thiet ke cua cac huong dan
    Log To Console     BP-8 : Check vi tri nut [<] tren giao dien huong dan
    Log To Console     BP-18 : Check chuyen qua lai giua cac huong dan dung theo thu tu
    Vuot tu trai sang phai de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_09    2
    Vuot tu trai sang phai de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_08    2
    Vuot tu trai sang phai de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_07    2
    Vuot tu trai sang phai de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_06    2
    Vuot tu trai sang phai de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_05    2
    Vuot tu trai sang phai de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_04    2
    Vuot tu trai sang phai de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_03    2
    Vuot tu trai sang phai de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_02    2
    Vuot tu trai sang phai de chuyen ve huong dan truoc do
    Chup anh man hinh va so sanh voi anh mau    ${kp_alias}    kc_d_ttdh_d_d_01    2
    AppiumLibrary.Page Should Not Contain Element    ${kp_nut_hd_truoc}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================