*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary
   
Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/cai_dat/cd_common.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-4932
    [Documentation]    Check dong cac giao dien trong Cai dat dien thoai khi cham vao icon mui ten back
    [Tags]     High    1sim    BP-4932    BP-4917
    Mo dien thoai roi vao cai dat dien thoai   
    Page Should Contain Element        ${dt_cd_back}
    An nut back tro ve giao dien truoc
    Sleep    2    
    Page Should Contain Element    ${tn_btn_tin_nhan}
    Close Application
    
BP-4940
    [Documentation]    Check hien thi nhac chuong dien thoai mac dinh
    [Tags]    BP-4940    1sim
    Mo dien thoai roi vao cai dat dien thoai
    An vao nut am thanh va rung
    Page Should Contain Text    Đàn T'rưng 1  
    Close Application  
    
BP-4943
    [Documentation]    Check hien 2 lua chon CHI MOT LAN va LUON CHON khi cham vao Bo nho da phuong tien
    [Tags]    BP-4943     1sim
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong dien thoai khi co 1 sim
    Sleep    2    
    Page Should Contain Element    ${dt_cd_nut_luon_chon}
    Sleep    2    
    Page Should Contain Element    ${dt_cd_nut_chi_mot_lan}    
    Close Application

BP-4944
    [Documentation]    Check mo danh sach nhac chuong dien thoai bo nho da phuong tien
    [Tags]    BP-4944    1sim
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong dien thoai khi co 1 sim
    An vao nut chon chi mot lan
    Page Should Contain Element    ${dt_cd_nut_dong_y}    
    Page Should Contain Element    ${dt_cd_nut_huy}    
    Close Application
    
BP-4952 
    [Documentation]    Check mo giao dien Hoan tat tac vu bang bo nho phuong tien sau khi da chon Chi mot lan
    [Tags]    BP-4952  1sim    
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong dien thoai khi co 1 sim
    An vao nut chon chi mot lan
    Close Application
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong dien thoai khi co 1 sim
    Page Should Contain Text    Hoàn tất tác vụ bằng Bộ nhớ phương tiện 
        
BP-4953
    [Documentation]    Check mo danh sach Nhac chuong dien thoai sau khi da chon LUON chon
    [Tags]    BP-4953     1sim    
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong dien thoai khi co 1 sim
    An vao nut luon chon
    Page Should Contain Text    Nhạc chuông điện thoại    
    Close Application
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong dien thoai khi co 1 sim
    Page Should Contain Text    Nhạc chuông điện thoại    
    Close Application
                 
BP-4945
    [Documentation]    Check luu lua chon khi chon nhac chuong Khong
    [Tags]    BP-4945     1sim    BP-4946    
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong dien thoai khi co 1 sim
    Vuot launcher danh sach nhac xuong down
    Bam vao nhac chuong khong
    Element Attribute Should Match    ${dt_cd_nut_nhac chuong_khong}    checked    true
    Bam vao dong y
    Page Should Contain Text    Âm thanh và rung 
    Page Should Contain Text    Không có    
    Close All Applications      
    
BP-4949
    [Documentation]    Check luu nhac chuong da chon tu danh sach nhac chuong Am thanh trong may
    [Tags]    BP-4949     1sim    
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong dien thoai khi co 1 sim
    An vao nhac chuong hop nhac
    Bam vao dong y
    Page Should Contain Text    Hộp nhạc   
    Close Application
    
BP-4968
    [Documentation]    Check trang thai cua check box Rung khi co cuoc goi khi o che do mac dinh 
    [Tags]    BP-4968       BP-4970        BP-4972      1sim    
    Log To Console     Check trang thai cua check box Rung khi co cuoc goi khi o che do mac dinh  
    Mo dien thoai vao cai dat va vao am thanh va rung
    Element Attribute Should Match    ${dt_cd_checkbox_rung_khi_co_cuoc_goi}    checked    true      
    Log To Console     Check trang thai cua checkbox khi tat Rung khi co cuoc goi          
    An checkbox Rung khi co cuoc goi
    Element Attribute Should Match    ${dt_cd_checkbox_rung_khi_co_cuoc_goi}   checked    false        
    Log To Console     Check trang thai cua check box khi bat Rung khi co cuoc goi     
    An checkbox Rung khi co cuoc goi
    Element Attribute Should Match    ${dt_cd_checkbox_rung_khi_co_cuoc_goi}    checked    true   
    Close Application

BP-4974
    [Documentation]    Check trang thai cua checkbox am ban phim khi o che do mac dinh 
    [Tags]    BP-4974    BP-4976        BP-4978    1sim
    Log To Console     Check trang thai cua checkbox am ban phim khi o che do mac dinh   
    Mo dien thoai vao cai dat va vao am thanh va rung
    Element Attribute Should Match    ${dt_cd_checkbox_am_ban_phim_so}   checked    true   
    Log To Console    Check tranng thai cua Checkbox khi tat Am ban phim so            
    An checkbox Am ban phim so
    Element Attribute Should Match    ${dt_cd_checkbox_am_ban_phim_so}   checked    false  
    Log To Console     Check checkbox khi bat Am ban phim so    
    An checkbox Am ban phim so
    Element Attribute Should Match    ${dt_cd_checkbox_am_ban_phim_so}   checked    true
    Close Application   
    Xoa mac dinh bo nho phuong tien
    Close All Applications 

BP-4954
    [Documentation]    Check mo giao dien Quan li file khi cham vao Quan li file
    [Tags]    BP-4954         1sim
    Log To Console     Check mo giao dien Quan li file khi cham vao Quan li file    
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong dien thoai khi co 1 sim
    An vao nut Quan li file
    Page Should Contain Text    Quản lý file    
    Sleep    2    
    Page Should Contain Text    Android    
    Close Application    
  
BP-4958
    [Documentation]    Check mo giao dien Hoan tat tac vu bang Quan ly file sau khi da chon  CHI MOT LAN
    [Tags]    BP-4958       1sim
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong dien thoai khi co 1 sim
    An vao nut chon chi mot lan
    Page Should Contain Text    Quản lý file    
    Page Should Contain Text    Android   
    Close Application
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong dien thoai khi co 1 sim
    Page Should Contain Element    ${dt_cd_nut_chi_mot_lan}    
    Page Should Contain Element    ${dt_cd_nut_luon_chon}    
    Close Application
        
BP-4959 
    [Documentation]    Check mo giao dien quan li file  sau khi da chon LUON CHON
    [Tags]    BP-4959  1sim
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong dien thoai khi co 1 sim
    An vao nut luon chon
    Page Should Contain Text    Quản lý file    
    Page Should Contain Text    Android   
    Close Application
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong dien thoai khi co 1 sim
    Page Should Contain Text    Quản lý file    
    Page Should Contain Text    Android 
    Xoa mac dinh Quan ly file
    
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
