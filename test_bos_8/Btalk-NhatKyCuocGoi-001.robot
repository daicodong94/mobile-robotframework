*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary
Library     String    
   
Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/dien_thoai.robot
Resource    ../page/btalk/nhat_ki_cuoc_goi.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Suite Setup    Mo Launcher
Suite Teardown    Close Application

*** Test Cases ***
BP-2639
    [Documentation]    Check TH mo tu tab dien thoai
    [Tags]    BP-2639    High
    Log To Console      Check TH mo tu tab dien thoai
    Mo Dien thoai tu Launcher
    Bam vao tab gan day
    Page Should Contain Element    ${dt_tab_gan_day_nut_goi_nho}     
    Close Application
    
BP-2642
    [Documentation]    Check TH mo tu tin nhan
    [Tags]    BP-2642     High
    Log To Console     Check TH mo tu tin nhan    
    Mo Btalk vao tin nhan
    Bam vao tab gan day
    Page Should Contain Element    ${dt_tab_gan_day_nut_goi_nho}     
    Close Application
    
BP-2644
    [Documentation]    Check TH mo tu giao dien tab dien thoai
    [Tags]    BP-2644    High
    Log To Console     Check TH mo tu giao dien tab dien thoai   
    Mo Btalk
    Bam vao tab gan day
    Page Should Contain Element    ${dt_tab_gan_day_nut_goi_nho}     
    
    
BP-2765
    [Documentation]     Check luu lai nhat ki dien thoai di sau khi ket thuc cuoc goi
    [Tags]    BP-2765    High    BP-3034    1sim
    Log To Console    Check luu lai nhat ki dien thoai di sau khi ket thuc cuoc goi     
    Mo Btalk
    Bam sdt test
    ${get_text}=  AppiumLibrary.Get Text    ${dt_man_hinh_hien_thi_nhap_sdt}
    Bam nut quay so
    Bam nut ket thuc cuoc goi
    Mo Dien thoai tu Launcher
    Bam vao tab gan day
    ${get_text_1}=  AppiumLibrary.Get Text    ${dt_tab_gan_day_lien_he1}
    Should Be Equal    ${get_text_1}   ${get_text}

BP-3038
    [Documentation]    Check mo giao dien soan tin nhan khi cham vao icon Tin nhan ben phai moi cuoc goi
    [Tags]    BP-3038     High     2sim
    Log To Console      Check mo giao dien soan tin nhan khi cham vao icon Tin nhan ben phai moi cuoc goi
    Mo Btalk roi mo Gan day
    Bam icon tin nhan dau tien trong tab gan day
    Page Should Contain Element    ${tn_go_noi_dung_tin_nhan}    
       

    
BP-3035
    [Documentation]    Check mo popup lua chon Sim trong TH may lap 2sim va dat che do hoi truoc khi goi
    [Tags]    BP-3035    High    2sim
    Log To Console        Check mo popup lua chon Sim trong TH may lap 2sim va dat che do hoi truoc khi goi
    Mo cai dat dien thoai va vao Tai khoan goi
    An nut thuc hien cuoc goi bang
    An nut hoi truoc tai popup thuc hien cuoc goi bang
    Mo Btalk roi mo Gan day
    Bam vao lien he dau tien trong tab gan day
    Page Should Contain Text    	Gọi bằng     
    Page Should Contain Element    ${dt_popup_goi_bang_checkbox_luon_sd_sim_nay_de_goi}     
  
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================