*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/cai_dat/che_do_dem.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/cai_dat/cd_common.robot

Suite Setup    Mo Cai dat
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu
*** Test Cases ***
#tat anh sang dem
CDD_020101
    [Documentation]    Check tong quan giao dien Anh sang dem
    [Tags]    CDD_020101
    An vao Hien thi
    Nhap vao tab che do dem hien thi cai dat
    Nhan nut bat check list lich bieu
    Nhan nut bat che do khong co tai lich bieu
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    kc_d_ttdh_d_tong_quan    3

CDD_030101
    [Documentation]    Check hien thi popup khi cham lich bieu
    [Tags]    CDD_030101
    Nhan nut bat check list lich bieu
    Nhan nut bat che do khong co tai lich bieu
    Nhan nut bat check list lich bieu
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    kc_d_ttdh_d_danh_sach_lich_bieu    3
    
CDD_030102
    [Documentation]    Check giao dien khi bat thoi gian tuy chinh
    [Tags]    CDD_030102
    Nhan nut bat che do thoi gian tuy chinh tai lich bieu
    Bam vao thoi gian bat dau
    Bam chon 22h
    Bam vao dong y
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    kc_t_ttdh_d_bat_vao_thoi_gian_tuy_chinh    3
    
CDD_030103
    [Documentation]    Check giao dien khi chon lich bieu Bat tu hoang hon den binh minh
    Nhan nut bat check list lich bieu
    Nhan nut bat che tu hoang hon den binh minh tai lich bieu
    Chup anh man hinh va so sanh voi anh mau     ${cd_alias}    kc_t_ttdh_d_bat_tu_hoang_hon_den_binh_minh    3
    
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================