*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/dien_thoai.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/btalk/nhat_ki_cuoc_goi.robot
Resource    ../page/btalk/gui_nhan_tin_nhan.robot
Resource    ../page/cai_dat/cd_common.robot

# Suite Setup    Mo Launcher

*** Test Cases ***
# Chua thiet lap thu thoai chup anh 1sim
BP-2910
    [Documentation]    Check hien thi dinh dang sdt khi nhap so tu ban phim
    [Tags]    BP-2910      BP-2911     BP-2912    1sim
    Mo Btalk
    Bam sdt 0977888888
    Sleep    2
    ${get_text}=  AppiumLibrary.Get Text    ${dt_man_hinh_hien_thi_nhap_sdt}
    Should Be Equal    ${get_text}      097 788 88 88
    Log To Console    Check hien thi icon Tin nhan va icon xoa khi nhap so
    Page Should Contain Element       ${dt_icon_tin_nhan_khi_nhap_so}
    Sleep    2
    Page Should Contain Element       ${dt_icon_xoa_khi_nhap_so}
    Log To Console    Check hien thi icon them lien he khi sdt chua luu
    Page Should Contain Element    ${dt_tab_dt_them_lien_he}
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_dien_thoai_nhap_so    4
    Close Application

BP-2899
    [Documentation]    Check tong quan giao dien tab Dien thoai
    [Tags]    BP-2899     1sim
    Mo Btalk
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_tong_quan_tab_dien_thoai    4

BP-2900
    [Documentation]    Check giao dien hien thi menu tuy chon cai dat
    [Tags]    BP-2900    1sim
    Mo Btalk
    Click Element    ${dt_cai_dat}
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_menu_tuy_chon_cai_dat    4

BP-2901
    [Documentation]    Check mo giao dien popup thiet lap thu thoai phim 1 khi chua thiet lap thu thoai
    [Tags]    BP-2901     1sim
    Mo Btalk
    Long Press    ${nut_1}
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}     kc_d_ttdh_d_d_popup_thiet_lap_thu_thoai_phim1    4

BP-2902
    [Documentation]    Check giao dien popup phim chua duoc thiet lap
    [Tags]    BP-2902    1sim
    Mo Btalk
    Nhan giu phim 2
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}     kc_d_ttdh_d_d_popup_phim2_chua_thiet_lap_quay_so_nhanh    4

BP-2905
    [Documentation]    Check giao dien dien thoai khi su dung kich thuoc phong va kich thuoc hien thi min
    [Tags]    BP-2905
    Mo Cai dat
    Bam chuyen kich thuoc phong va hien thi ve min
    Vuot quay ve man hinh Home
    Bam mo app dien thoai
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_dien_thoai_kich_thuoc_min    3
    Close Application

BP-2913
    [Documentation]    Checkhien thi icon Them lien he khi nhap sdt chua luu trong danh ba sau khi thy doi kich thuoc phong
    [Tags]    BP-2913
    Mo Cai dat
    Bam chuyen kich thuoc phong va hien thi ve min
    Close Application
    Mo Btalk
    Bam sdt 0977888888
    Sleep    2
    Page Should Contain Element    ${dt_tab_dt_them_lien_he}
    Dong toan bo ung dung trong da nhiem

BP-2906
    [Documentation]    Check giao dien tab dien thoai khi su dung kich thuoc phong va kich thuoc hien thi max
    [Tags]    BP-2906
    Mo cai dat
    Bam chuyen kich thuoc phong chu va hien thi ve max
    Vuot quay ve man hinh Home
    Bam mo app dien thoai
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_ttdh_d_d_dien_thoai_kich_thuoc_max    2
    Close Application

# Kich thuoc phong va hien thi dang o min
BP-2908
    [Documentation]    Check giao dien Dien thoai khi chia doi man hinh
    [Tags]    BP-2908
    Mo Cai dat
    #Bam chuyen kich thuoc phong va hien thi ve mac dinh
    Dong toan bo ung dung trong da nhiem
    Mo Btalk
    Mo da nhiem
    Click Element    ${dn_icon_ung_dung}
    Sleep    2
    AppiumLibrary.Click Element    ${dn_chia_doi_man_hinh}
    ${hien_thi_app_cai_dat}    Run Keyword And Return Status    AppiumLibrary.Page Should Contain Element    ${app_cai_dat}
    Run Keyword If    ${hien_thi_app_cai_dat}==True    AppiumLibrary.Click Element    ${app_cai_dat}
    ...    ELSE    Vuot sang trai launcher va click app cai dat
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_tthd_d_d_danh_sach_lua_chon_sim    3

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================