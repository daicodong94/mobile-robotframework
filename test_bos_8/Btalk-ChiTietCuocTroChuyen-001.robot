*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/btalk/tn_cuoc_tro_chuyen.robot
Resource    ../page/btalk/tin_nhan.robot

Suite Setup       Mo Launcher
Suite Teardown    Bam nut dong toan bo ung dung trong da nhiem

*** Test Cases ***
BP-1786
    [Documentation]    Check mo giao dien chi tiet cuoc tro truyen
    [Tags]    BP-1786
    Log To Console     Check mo giao dien chi tiet cuoc tro truyen    
    Mo Btalk
    Click Element    ${tn_btn_tin_nhan}
    Bam vao chi tiet cuoc tro truyen
    
BP-1842 
    [Documentation]    Check mo giao dien goi di khi cham vao lien he tin nhan
    [Tags]    BP-1842        BP-1847
    Log To Console     Check mo giao dien goi di khi cham vao lien he tin nhan    
    Check mo giao dien goi di khi cham vao lien he tin nhan
    
BP-1848
    [Documentation]    check mo menu tuy chon khi cham vao dau bam cham
    [Tags]    BP-1848    BP-1849
    Log To Console     check mo menu tuy chon khi cham vao dau bam cham    
    Check cham vao dau ba cham
    Log To Console    check cham vao goi dien    
    Check cham vao goi dien
    
BP-1850
    [Documentation]    check cham vao lien he
    [Tags]    BP-1850    BP-1851
    Log To Console     check cham vao lien he  
    Check cham vao dau ba cham  
    Check cham vao lien he
    Log To Console    check dong giao dien lien he    
    Go Back
    Page Should Not Contain Element    ${dh_btn_huy}    
    
BP-1852
    [Documentation]    Check cham vao tim kiem tin nhan
    [Tags]    BP-1852    BP-1854    BP-1855    BP-1857
    Log To Console     Check cham vao tim kiem tin nhan  
    Check cham vao dau ba cham  
    Check cham vao tim kiem tin nhan
    Log To Console    Check hien thi thong bao khong tim thay tin nhan    
    Check hien thi thong bao khong tim thay tin nhan
    Log To Console    Check xoa noi dung tim kiem da nhap khi cham vao x    
    Check xoa noi dung tim kiem da nhap khi cham vao x
    Log To Console    Check dong giao dien tim kiem tin nhan khi cham vao vao dau x    
    Check dong giao dien tim kiem tin nhan khi cham vao vao dau x
    Bam nut dong toan bo ung dung trong da nhiem
    
BP-1859
    [Documentation]    Check mo giao dien lien he khi cham vao dinh kem lien he
    [Tags]    BP-1859       BP-1860    BP-1861     BP-1865
    ...        BP-1866    BP-1868
    Log To Console     Check mo giao dien lien he khi cham vao dinh kem lien he  
    Mo Btalk
    bam vao tin nhan
    Bam vao chi tiet cuoc tro truyen
    Check cham vao dau ba cham 
    Check mo giao dien lien he khi cham vao dinh kem lien he
    Log To Console    Check mo giao dien tim kiem lien he khi cham vao kinh lup    
    Check mo giao dien tim kiem lien he khi cham vao kinh lup   
    Log To Console    Check hien thi thong bao khong tim thay lien he  
    Check hien thi thong bao khong tim thay lien he
    Log To Console    Check xoa noi dung tim kiem da nhap khi cham vao dau x  
    Check xoa noi dung tim kiem da nhap khi cham vao dau x    
    Log To Console    Check dong giao dien lien he khi cham vao dau x    
    Check dong giao dien lien he khi cham vao dau x
    Log To Console    Check dong giao dien lien he khi cham vao mui ten Back    
    Check dong giao dien lien he khi cham vao mui ten Back
    Bam nut dong toan bo ung dung trong da nhiem
    
BP-1869 
    [Documentation]    Check them chu de vao go noi dung tin nhan khi cham vao them chu de
    [Tags]      BP-1869     BP-1870      BP-1871    BP-1873    BP-1875
    Log To Console     Check them chu de vao go noi dung tin nhan khi cham vao them chu de    
    Mo Btalk
    Bam vao tin nhan
    Bam vao chi tiet cuoc tro truyen
    Check cham vao dau ba cham
    Check them chu de vao go noi dung tin nhan khi cham vao them chu de
    Log To Console    Check hien thi trang thai tin nhan MMS khi nhap noi dung chu de    
    Check hien thi trang thai tin nhan MMS khi nhap noi dung chu de
    Log To Console    Check so ki tu cho phep nhap toi da o phan chu de    
    Check so ki tu cho phep nhap toi da o phan chu de
    Log To Console    Check dong truong hop nhap du lieu chu de khi cham vao x
    Check dong truong hop nhap du lieu chu de khi cham vao x    
    Log To Console    Check gui tin nhan co chu de    
    Check cham vao dau ba cham
    Check them chu de vao go noi dung tin nhan khi cham vao them chu de
    Check gui tin nhan co chu de
    Bam nut dong toan bo ung dung trong da nhiem
    
# BP-1883
    # [Documentation]    Check luu tru cuoc tro chuyen khi cham vao luu tru
    # [Tags]      BP-1883     BP-1885    BP-1886   
    # Log To Console     Check luu tru cuoc tro chuyen khi cham vao luu tru 
    # Mo Btalk
    # Bam vao tin nhan
    # Bam vao chi tiet cuoc tro truyen
    # Check cham vao dau ba cham
    # Check luu tru cuoc tro chuyen khi cham vao luu tru
    # Log To Console    Check hien thi cuoc tro chuyen da luu trong danh sach luu tru
    # Check cham vao dau ba cham
    # Check hien thi cuoc tro chuyen da luu trong danh sach luu tru   
    # Log To Console    Check huy cuoc tro chuyen da luu trong danh sach luu tru
    # Check huy cuoc tro chuyen da luu trong danh sach luu tru
    # # Bam nut dong toan bo ung dung trong da nhiem
   
BP-1887
    [Documentation]    Check mo giao dien cai dat khi cham vao cai dat
    [Tags]      BP-1887     BP-1889     BP-1901     BP-1905
    ...    BP-1907    BP-1908
    Log To Console     Check mo giao dien cai dat khi cham vao cai dat 
    Log To Console     Check hien thi am thanh mac dinh la chim hot    
    Mo Btalk
    Bam vao tin nhan
    Bam vao chi tiet cuoc tro truyen
    Check cham vao dau ba cham  
    Check mo giao dien cai dat khi cham vao cai dat
    Log To Console     Check dong giao dien cai dat khi cham vao mui ten back    
    Check dong giao dien cai dat khi cham vao mui ten back
    Check cham vao dau ba cham
    Check mo giao dien cai dat khi cham vao cai dat
    Sleep    2    
    Log To Console    Check mo giao dien chi tiet lien he khi cham vao sdt hoac avata  
    Check mo giao dien chi tiet lien he khi cham vao sdt hoac avata
    Sleep    2    
    Log To Console    Check hien thi popup chan x khi cham vao chan X    
    Check hien thi popup chan x khi cham vao chan X
    Sleep    2    
    Log To Console    Check dong popup chan x khi cham vao huy    
    Check dong popup chan x khi cham vao huy
    Sleep    2    
    Log To Console    Check chan so dien thoai x khi cham vao dong y    
    Check hien thi popup chan x khi cham vao chan X
    Check chan so dien thoai x khi cham vao dong y
    Bam nut dong toan bo ung dung trong da nhiem

BP-1914
    [Documentation]    Check mo giao dien chinh sua phan hoi nhanh
    [Tags]      BP-1914     BP-1915     BP-1922    BP-1918
    ...     BP-1926    BP-1927    BP-1929    
    Log To Console     Check mo giao dien chinh sua phan hoi nhanh 
    Mo Btalk
    Bam vao tin nhan
    Bam vao chi tiet cuoc tro truyen
    Check cham vao dau ba cham  
    Log To Console    Check mo giao dien chinh sua phan hoi nhanh trong truong hop co du lieu    
    Check mo giao dien chinh sua phan hoi nhanh
    Log To Console    Check dong popup phan hoi nhanh khi cham vao huy    
    Check dong popup phan hoi nhanh khi cham vao huy
    Log To Console    Check mo popup phan hoi nhanh khi cham vao +    
    Check mo popup phan hoi nhanh khi cham vao +
    Log To Console    Check luu noi dung phan hoi nhanh
    Check luu noi dung phan hoi nhanh
    Log To Console    Check luu sau khi thay doi noi dung phan hoi nhanh    
    Check luu sau khi thay doi noi dung phan hoi nhanh
    Log To Console    Check xoa noi dung phan hoi nhanh    
    Check xoa noi dung phan hoi nhanh
    Log To Console    Check dong giao dien phan hoi nhanh khi cham    
    Check dong giao dien phan hoi nhanh khi cham
    Bam nut dong toan bo ung dung trong da nhiem

BP-1973  
    [Documentation]    Check mo popup xoa nhung cuoc tro chuyen khi cham vao xoa
    [Tags]    BP-1973      BP-1970    BP-1972
    Log To Console    Check mo popup xoa nhung cuoc tro chuyen khi cham vao xoa    
    Mo Btalk
    Bam vao tin nhan
    Bam vao chi tiet cuoc tro truyen
    Check cham vao dau ba cham  
    Check mo popup xoa nhung cuoc tro chuyen khi cham vao xoa
    Log To Console    Check dong popup xoa khi cham vao huy    
    Check dong popup xoa khi cham vao huy
    Log To Console    Check xoa cuoc tro chuyen khi cham vao xoa   
    Check cham vao dau ba cham   
    Check mo popup xoa nhung cuoc tro chuyen khi cham vao xoa
    Check xoa cuoc tro chuyen khi cham vao xoa
     Bam nut dong toan bo ung dung trong da nhiem
     
BP-1976
    [Documentation]    Check hien thi mac dinh cua cuoc tro chuyen khi loc tap ghi am
    [Tags]    BP-1976    BP-1981    BP-1985 
    Log To Console    Check hien thi mac dinh cua cuoc tro chuyen khi loc tap ghi am
    Mo Btalk
    Bam vao tin nhan
    Bam vao chi tiet cuoc tro truyen
    Check cham vao dau ba cham  
    Check hien thi mac dinh cua cuoc tro chuyen khi loc tap ghi am
    Log To Console    Check hien thi cuoc tro chuyen khi chon loc tep ghi am trong truong hop cuoc tro chuyen chi co sms         
    Check hien thi cuoc tro chuyen khi chon loc tep ghi am trong truong hop cuoc tro chuyen chi co sms
    Log To Console    Check hien thi cuoc tro truyen khi chon loc tin nhan sms         
    Check hien thi cuoc tro truyen khi chon loc tin nhan sms
    

BP-2011
    [Documentation]    Check mo popup chuyen tiep thu khi cham vao icon chuyen tiep
    [Tags]   BP-2011         BP-2013    BP-2015    BP-2018
    ...    BP-2019     BP-2021
    Log To Console    Check mo popup chuyen tiep thu khi cham vao icon chuyen tiep
    Mo Btalk
    Bam vao tin nhan
    Bam vao chi tiet cuoc tro truyen
    Check mo popup chuyen tiep thu khi cham vao icon chuyen tiep
    Log To Console    Check cham vao giao dien tim kiem tin nhan khi cham vao kinh lup
    Check cham vao giao dien tim kiem tin nhan khi cham vao kinh lup
    Log To Console    Check hien thi thong bao khong tim thay tin nhan trong chuyen tiep
    Check hien thi thong bao khong tim thay tin nhan trong chuyen tiep
    Log To Console    Check xoa noi dung da nhap khi cham vao x
    Check xoa noi dung da nhap khi cham vao x
    Log To Console    Check dong giao dien tim kien tn khi cham vao x
    Check dong giao dien tim kien tn khi cham vao x
    Log To Console    Check mo giao dien soan tin nhan khi cham vao icon them moi
    Check mo giao dien soan tin nhan khi cham vao icon them moi
    Bam nut dong toan bo ung dung trong da nhiem
    
BP-2029 
    [Documentation]    Check luu noi dung sao chep khi cham vao icon sao chep
    [Tags]   BP-2029       BP-2031       BP-2034    BP-2036
    Log To Console    Check luu noi dung sao chep khi cham vao icon sao chep
    Mo Btalk
    Bam vao tin nhan
    Bam vao chi tiet cuoc tro truyen
    Check mo popup chi tiet tin nhan
    Go Back
    Log To Console    Check mo tuy chon khi cham vao dau ba cham truong hop dang chon 1 tin nhan    
    Check mo tuy chon khi cham vao dau ba cham truong hop dang chon 1 tin nhan
    Log To Console    Check mo popup xoa tin nhan nay khi cham vao xoa    
    Check mo popup xoa tin nhan nay khi cham vao xoa
    Log To Console      Check dong popup khi cham vao huy     
    Check dong popup khi cham vao huy 
    Log To Console    Check xoa tin nhan khi cham vao xoa    
    Check xoa tin nhan khi cham vao xoa

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================