*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/dong_ho/hen_gio.robot
Resource    ../page/common.robot
Resource    ../page/dong_ho/dh_common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-659
    [Documentation]    Check mo giao dien hen gio
    [Tags]    BP-659    BP-660    BP-661
    Log To Console    Check mo giao dien Bo hen gio tu giao dien Bao thuc
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut tab menu top hen gio
    Kiem tra emlement man hinh hen gio
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_hengio    3
    Nhan nut tab menu top dong ho
    Log To Console    BP-660: Check mo giao dien Bo hen gio tu giao dien Dong ho
    Nhan nut tab menu top hen gio
    Kiem tra emlement man hinh hen gio
    Nhan nut tab menu top bam gio
    Log To Console    BP-661: Check mo giao dien Bo hen gio tu giao dien Dong ho bam gio
    Nhan nut tab menu top hen gio
    Kiem tra emlement man hinh hen gio
    Close Application

BP-666
    [Documentation]    Check giao dien hien thi Bo hen gio khi chua nhap gia tri thoi gian tu ban phim so
    [Tags]    BP-666
    Log To Console    Check giao dien hien thi Bo hen gio khi chua nhap gia tri thoi gian tu ban phim so
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Kiem tra emlement man hinh hen gio
    Close Application

BP-662
    [Documentation]    Check mo giao dien Bo hen gio tu giao dien Bao thuc
    [Tags]    BP-662    BP-663    BP-664
    Log To Console    Check mo giao dien Bo hen gio tu giao dien Bao thuc
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Thiet lap cai bo hen gio
    Nhan nut bao thuc
    Nhan nut menu top hen gio
    Kiem tra veryfile man hinh hen gio da bat
    Log To Console    BP-663: Check da bat hen gio tai man hinh dong ho
    Nhan nut dong ho
    Nhan nut menu top hen gio
    Sleep    2
    Kiem tra veryfile man hinh hen gio da bat
    Log To Console    BP-664: Check da bat hen gio tai man hinh bam gio
    Nhan nut dong ho bam gio
    Nhan nut menu top hen gio
    Sleep    2
    Kiem tra veryfile man hinh hen gio da bat
    Xoa bo hen gio
    Close Application

BP-673
    [Documentation]    Check TH thiet lap thoi gian cho Bo hen gio
    [Tags]    BP-673     BP-674    BP-678    BP-679    BP-680    BP-681
    ...    BP-683    BP-684     BP-685    BP-686    BP-687
    ...    BP-690     BP-694
    Log To Console    Check TH thiet lap thoi gian cho Bo hen gio
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Thuc hien kiem tra cac chuc nang nhap tao bo hen gio
    Kiem tra play va pause bo hen gio
    Kiem tra emlement man hinh hen gio
    Close Application

BP-688
    [Documentation]    Check TH hien thi chuc nang DAT LAI bang thao tac bam vao icon Pause
    [Tags]    BP-688
    Log To Console    Check TH hien thi chuc nang DAT LAI bang thao tac bam vao icon Pause
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Thuc hien tao hen gio check dat lai hen gio nhu ban dau khi nhan Pause

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================