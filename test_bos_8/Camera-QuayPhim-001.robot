*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/camera/cam_common.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-4121
    [Documentation]    Check vuot den tab Quay phim khi dang o tab khac
    [Tags]     High    BP-4121
    Mo ung dung va cho phep cap quyen    ${cam_alias}    ${cam_package}    ${cam_activity}    2
    AppiumLibrary.Swipe By Percent    70    90    50    90
    Xac nhan mo QUAY PHIM thanh cong

BP-4122
    [Documentation]    Check chon den tab Quay phim khi dang o tab khac
    [Tags]     High    BP-4122
    AppiumLibrary.Close All Applications
    Mo May anh roi mo QUAY PHIM

BP-5776
    [Documentation]    Check hien thi tab Quay phim da chon truoc do sau khi vuot ve home roi mo lai
    [Tags]     High    BP-5776
    Vuot quay ve man hinh Home
    Mo May anh tu Launcher
    Xac nhan mo QUAY PHIM thanh cong

BP-4165
    [Documentation]    Check mo video vua quay khi bam vao icon Bo suu tap
    [Tags]     High    BP-4165
    Bam nut quay phim chup anh    2
    Bam nut quay phim chup anh    2
    Mo che do xem cuon phim
    Bam de hien thi ten va tuy chon cua file
    Log To Console     Xac nhan hien thi video vua quay
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_bo_loc}
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_cai_dat}
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_auto_adv}
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_quay_chup}
    AppiumLibrary.Page Should Contain Element        ${cam_nut_thong_tin}
    AppiumLibrary.Page Should Contain Element        ${cam_nut_xoa}
    Log To Console     Xoa du lieu truoc khi ket thuc phien kiem thu
    AppiumLibrary.Click Element    ${cam_nut_xoa}
    Sleep    2

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================