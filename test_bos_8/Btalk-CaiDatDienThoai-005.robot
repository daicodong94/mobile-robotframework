*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/cai_dat/cd_common.robot
Resource    ../page/btalk/dien_thoai.robot

*** Test Cases ***
BP-5056
    [Documentation]    Check trang thai cua checkbox luu vi tri tab khi thoat ung dung khi o trang thai mac dinh
    [Tags]    BP-5056    BP-5057
    Log To Console     Check trang thai cua checkbox luu vii tri tab khi thoat ung dung khi o trang thai mac dinh
    Mo dien thoai roi vao cai dat dien thoai
    An nut cai dat mo ung dung
    Element Attribute Should Match         ${dt_cd_checkbox_luu_vi_tri_tab_khi_thoat_ung_dung}   checked    	true
    Log To Console     Check mo vi tri tab da luu truoc khi thoat ung dung khi o trang thai mac dinh
    Mo Btalk
    An sang man tin nhan tren man hinh chinh btalk
    Sleep    2
    Go Back
    Bam vao Dien thoai tren Launcher
    Page Should Contain Element        ${text_tin_nhan}
    An chon man dien thoai tren giao dien chinh btalk

BP-5058
    [Documentation]    Check trang thai cua checkbox khi Tat chuc nang Luu vi tri tab khi thoat ung dung
    [Tags]    BP-5058    BP-5059    1sim
    Log To Console    Check trang thai cua checkbox khi Tat chuc nang Luu vi tri tab khi thoat ung dung
    Mo dien thoai roi vao cai dat dien thoai
    An nut cai dat mo ung dung
    An nut checkbox luu vi tri tab khi thoat ung dung
    Element Attribute Should Match         ${dt_cd_checkbox_luu_vi_tri_tab_khi_thoat_ung_dung}   checked    	false
    Log To Console    Check mo giao dien dien thoai sau khi Tat luu vi tri tab khi thoat ung dung
    Mo Btalk
    An sang man tin nhan tren man hinh chinh btalk
    Go Back
    Bam vao Dien thoai tren Launcher
    Sleep    1
    Page Should Contain Element        ${dt_nut_quay_so}

BP-5060
    [Documentation]    Check trang thai cua checkbox khi bat chuc nang luu vi tri tab khi thoat ung dung
    [Tags]    BP-5060    BP-5061    1sim
    Log To Console    Check trang thai cua checkbox khi bat chuc nang luu vi tri tab khi thoat ung dung
    Mo dien thoai roi vao cai dat dien thoai
    An nut cai dat mo ung dung
    Element Attribute Should Match         ${dt_cd_checkbox_luu_vi_tri_tab_khi_thoat_ung_dung}   checked    	false
    An nut checkbox luu vi tri tab khi thoat ung dung
    Element Attribute Should Match         ${dt_cd_checkbox_luu_vi_tri_tab_khi_thoat_ung_dung}   checked    	true
    Log To Console    Check mo vi tri tab da luu truoc khi thoat ung dung sau khi bat luu vi tri tab khi thoat ung dung
    Mo Btalk
    An sang man tin nhan tren man hinh chinh btalk
    Sleep    2
    Go Back
    Bam vao Dien thoai tren Launcher
    Page Should Contain Element        ${text_tin_nhan}
    An chon man dien thoai tren giao dien chinh btalk

BP-5062
    [Documentation]    Check mo giao dien Thu thoai khi cham vao thu thoai
    [Tags]    BP-5062    1sim
    Log To Console     Check mo giao dien Thu thoai khi cham vao thu thoai
    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    An nut thu thoai trong cai dat quay so nhanh
    Page Should Contain Element    ${dt_cd_nut_dich_vu}

BP-5063
    [Documentation]    Check mo popup Dich vu khi cham vao Dich vu
    [Tags]    BP-4933     BP-5063    1sim
    Log To Console     Check mo popup Dich vu khi cham vao Dich vu
    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    An nut thu thoai trong cai dat quay so nhanh
    An nut dich vu trong thu thoai
    Page Should Contain Element    ${dt_cd_checkbox_dich_vu_nha_mang_cua_ban}
    Element Attribute Should Match    ${dt_cd_checkbox_dich_vu_nha_mang_cua_ban}    checked    true
    Log To Console    Check dong popup khi vham vao button HUY
    Bam vao huy
    Page Should Contain Element    ${dt_cd_nut_thiet_lap}

BP-5064
    [Documentation]    Check mo giao dien Thiet lap khi cham vao Thiet lap
    [Tags]    BP-5064     1sim
    Log To Console     Check mo giao dien Thiet lap khi cham vao Thiet lap
    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    An nut thu thoai trong cai dat quay so nhanh
    An nut thiet lap trong thu thoai
    Page Should Contain Text    Chưa đặt

BP-5065
    [Documentation]    Check mo popup So thu thoai khi cham vao So thu thoai
    [Tags]    BP-5065    1sim
    Log To Console     Check mo popup So thu thoai khi cham vao So thu thoai
    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    An nut thu thoai trong cai dat quay so nhanh
    An nut thiet lap trong thu thoai
    An nut so thu thoai
    Page Should Contain Text    Số thư thoại
    Page Should Contain Element    ${dt_cd_nut_dong_y}
    Page Should Contain Element    ${dt_cd_nut_huy}

BP-5066
    [Documentation]    Check hien thi popup Thu thoai khi them so dien thoai
    [Tags]    BP-5066     BP-5067  BP-5068  1sim
    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    An nut thu thoai trong cai dat quay so nhanh
    An nut thiet lap trong thu thoai
    An nut so thu thoai
    An nut nhap so thu thoai
    An nut 900 o thiet lap so thu thoai
    Bam vao dong y
    Page Should Contain Element    ${dt_cd_title_popup_thu_thoai}
    Page Should Contain Element    ${dt_cd_nut_ok}
    Log To Console     Check popup khi cham vao ok
    An nut OK
    Page Should Contain Element    ${dt_cd_nut_quay_so_nhanh_thu_thoai}
    Log To Console    Check mo giao dien goi so thu thoai
    Go Back
    Go Back
    Long Press   ${nut_1}
    Sleep    2
    Page Should Contain Element    ${dt_man_hinh_hien_thi_ten_lien_he_da_luu_khi_goi}
    Element Attribute Should Match    ${dt_man_hinh_hien_thi_ten_lien_he_da_luu_khi_goi}    text    Thư thoại

BP-5069
    [Documentation]    Check mo giao dien Loai thong bao khi cham vao thong bao
    [Tags]    BP-5069    1sim
    Log To Console     Check mo giao dien Loai thong bao khi cham vao thong bao
    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    An nut thu thoai trong cai dat quay so nhanh
    An nut thong bao trong thu thoai
    Page Should Contain Text    Loại thông báo

BP-5070
    [Documentation]    Check mo popup Cai dat quay so nhanh khi cham vao phim bat ky trong danh sach tu phim so 2 den phim so 9
    [Tags]    BP-5070    1sim
    Log To Console     Check mo popup Cai dat quay so nhanh khi cham vao phim bat ky trong danh sach tu phim so 2 den so 9
    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    Log To Console     Check mo popup cai dat quay so nhanh khi bam phim 2
    An so 2 cai dat quay so nhanh
    Page Should Contain Element   ${dt_cd_nut_dong_y}
    Page Should Contain Element    ${dt_cd_nut_huy_bo}
    Bam vao dong y
    Log To Console     Check mo popup cai dat quay so nhanh khi bam phim 3
    An so 3 cai dat quay so nhanh
    Page Should Contain Element   ${dt_cd_nut_dong_y}
    Page Should Contain Element    ${dt_cd_nut_huy_bo}
    Bam vao dong y
    Log To Console     Check mo popup cai dat quay so nhanh khi bam phim 4
    An so 4 cai dat quay so nhanh
    Page Should Contain Element   ${dt_cd_nut_dong_y}
    Page Should Contain Element    ${dt_cd_nut_huy_bo}
    Bam vao dong y
    Log To Console     Check mo popup cai dat quay so nhanh khi bam phim 5
    An so 5 cai dat quay so nhanh
    Page Should Contain Element   ${dt_cd_nut_dong_y}
    Page Should Contain Element    ${dt_cd_nut_huy_bo}
    Bam vao dong y
    Log To Console     Check mo popup cai dat quay so nhanh khi bam phim 6
    An so 6 cai dat quay so nhanh
    Page Should Contain Element   ${dt_cd_nut_dong_y}
    Page Should Contain Element    ${dt_cd_nut_huy_bo}
    Bam vao dong y
    Log To Console     Check mo popup cai dat quay so nhanh khi bam phim 7
    An so 7 cai dat quay so nhanh
    Page Should Contain Element   ${dt_cd_nut_dong_y}
    Page Should Contain Element    ${dt_cd_nut_huy_bo}
    Bam vao dong y
    Log To Console     Check mo popup cai dat quay so nhanh khi bam phim 8
    An so 8 cai dat quay so nhanh
    Page Should Contain Element   ${dt_cd_nut_dong_y}
    Page Should Contain Element    ${dt_cd_nut_huy_bo}
    Bam vao dong y
    Log To Console     Check mo popup cai dat quay so nhanh khi bam phim 9
    An so 9 cai dat quay so nhanh
    Page Should Contain Element   ${dt_cd_nut_dong_y}
    Page Should Contain Element    ${dt_cd_nut_huy_bo}
    Bam vao dong y

BP-5071
    [Documentation]    Check luu gan quay so nhanh trong TH
    [Tags]    BP-5071    1sim
    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    An so 3 cai dat quay so nhanh
    Nhap so 900 vao popup cai dat quay so nhanh
    Bam vao dong y
    Element Attribute Should Match    ${dt_cd_quay_so_nhanh_text_phim_3}    text    900

BP-5072
    [Documentation]    Check mo giao dien Chon nguoi lien he khi cham vao icon Danh ba
    [Tags]    BP-5072    BP-5073    BP-5075        1sim
    Log To Console     Check mo giao dien Chon nguoi lien he khi cham vao icon Danh ba
    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    An so 2 cai dat quay so nhanh
    An vao icon danh ba o cai dat quay so nhanh
    Page Should Contain Text    Chọn người liên hệ
    Log To Console    Check luu gan quay so nhanh trong TH chon lien he tu danh ba
    ${text}=  AppiumLibrary.Get Text   ${dt_cd_quay_so_nhanh_chon_lien_he1}
    An vao lien he dau tien trong Chon nguoi lien he quay so nhanh
    ${text2}=  AppiumLibrary.Get Text   ${dt_cd_sdt}
    Should Be Equal     ${text}    ${text2}
    Log To Console     Check  mo menu tuy chon khi cham vao phim so 2 khi da thiet lap quay so nhanh cho phimn 2
    An so 2 cai dat quay so nhanh
    Page Should Contain Element    ${dt_cd_thay_doi}
    Page Should Contain Element    ${dt_cd_xoa_bo}

BP-5079
    [Documentation]    Check Xoa cai dat quay so nhanh khi cham vao xoa bo
    [Tags]   BP-5079     1sim
    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    An so 2 cai dat quay so nhanh
    An vao nut xoa bo
    Element Attribute Should Match     ${dt_cd_quay_so_nhanh_text_phim_2}   text     (chưa cài)

BP-5080
    [Documentation]    Check hien thi popup Phim chua duoc thiet lap sau khi xoa quay so nhanh cho phim so 2
    [Tags]    BP-5080    1sim
    Mo Btalk
    Long Press         ${nut_2}     2
    Page Should Contain Element    ${dt_popup_phim_chua_dc_thiet_lap}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================