*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/may_tinh/mt_common.robot
Resource    ../page/may_tinh/nang_cao.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***

BP-1032
    [Documentation]    Check chuc nang DEG voi phep tinh cong ki hieu sin
    [Tags]    BP-1032
    Log To Console    Check chuc nang DEG voi phep tinh cong ki hieu sin
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh cong sin
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0**82423697*
    Close Application

BP-1039
    [Documentation]    Check chuc nang RAD voi phep tinh cong ki hieu sin
    [Tags]    BP-1039
    Log To Console    Check chuc nang RAD voi phep tinh cong ki hieu sin
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut RAD
    Sleep    2
    Thuc hien va kiem tra phep tinh cong sin
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text     *0**31286893*
    Sleep    2
    Close Application

BP-1034
    [Documentation]    Check chuc nang DEG voi phep tinh cong ki hieu cos
    [Tags]    BP-1034
    Log To Console     Check chuc nang DEG voi phep tinh cong ki hieu cos
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh cong cos
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *−1**8222605*
    Close Application

BP-1041
    [Documentation]    Check chuc nang RAD voi phep tinh cong ki hieu cos
    [Tags]    BP-1041
    Log To Console    Check chuc nang RAD voi phep tinh cong ki hieu cos
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut RAD
    Sleep    2
    Thuc hien va kiem tra phep tinh cong cos
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *1**97537668*
    Close Application

BP-1036
    [Documentation]    Check chuc nang DEG voi phep tinh cong ki hieu tan
    [Tags]    BP-1036
    Log To Console    Check chuc nang DEG voi phep tinh cong ki hieu tan
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Thuc hien va kiem tra phep tinh cong tan
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *−0**9046313*
    Close Application

BP-1042
    [Documentation]    Check chuc nang RAD voi phep tinh cong ki hieu tan
    [Tags]    BP-1042
    Log To Console    Check chuc nang RAD voi phep tinh cong ki hieu tan
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut RAD
    Sleep    2
    Thuc hien va kiem tra phep tinh cong tan
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0**31676888*
    Close Application

BP-1043
    [Documentation]    Check phep tinh cong ki hieu ln
    [Tags]    BP-1043
    Log To Console    Check phep tinh cong ki hieu ln
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh cong ln
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *4**39444915*
    Close Application

BP-1044
    [Documentation]    Check phep tinh cong ki hieu log
    [Tags]    BP-1044
    Log To Console    Check phep tinh cong ki hieu log
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh cong log
    Sleep    2
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *1**90848502*

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================