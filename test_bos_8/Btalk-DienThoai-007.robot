*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/btalk/dien_thoai.robot
Resource    ../page/btalk/gui_nhan_tin_nhan.robot
Resource    ../page/btalk/nhat_ki_cuoc_goi.robot
Resource    ../page/cai_dat/cd_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot

# Suite Setup    Mo Launcher

*** Test Cases ***
#luu i cai dat mo ung dung tat
BP-2937
    [Documentation]    Phim 1 check mo popoup de thiet lap thu thooai,di toi menu khi chua gan so thu thoai cho phim 1
    [Tags]    BP-2937     BP-2938    1sim
    Mo Btalk
    Long Press    ${nut_1}
    Page Should Contain Element    ${nut_dong_y_hoa}
    Bam vao dong y
    Page Should Contain Element    ${dt_nut_quay_so}

BP-2939
    [Documentation]    Check mo giao dien goi toi Thu thoai khi cham va giu phim 1 trong TH da thiet lap thu thoai
    [Tags]    BP-2939    1sim
    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    An nut thu thoai trong cai dat quay so nhanh
    An nut thiet lap trong thu thoai
    An nut so thu thoai
    An nut nhap so thu thoai
    An nut 900 o thiet lap so thu thoai
    Bam vao dong y
    Page Should Contain Element    ${dt_cd_title_popup_thu_thoai}
    Page Should Contain Element    ${dt_cd_nut_ok}
    An nut OK
    Page Should Contain Element    ${dt_cd_nut_quay_so_nhanh_thu_thoai}
    Vuot quay ve man hinh Home
    Bam mo app dien thoai
    Long Press    ${nut_1}
    Page Should Contain Text    Thư thoại
    Sleep    3
    Page Should Contain Element    ${dt_tab_dt_nut_tat_popup_ban_phim_khi_goi_thu_thoai}
    Page Should Contain Element    ${nut_1}
    An nut tat popup ban phim tu dong
    Bam nut ket thuc cuoc goi

BP-2940
    [Documentation]    Check hien thi popup phim chua thiet lap khi cham va giu cac phim tu 2->9
    [Tags]    BP-2940    BP-2941    1sim
    Mo Btalk
    Nhan giu phim 2
    Page Should Contain Element    ${dt_tab_dt_popup_phim_chua_duoc_thiet_lap}
    Page Should Contain Element    ${nut_dong_y_hoa}
    Bam nut de sau popup phim chua duoc thiet lap
    
input data vao quay so nhanh
    [Documentation]    Check mo giao dien goi dii khi cham va giu cac phim tu 2->9
    [Tags]    1sim
    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    An so 2 cai dat quay so nhanh
    input sdt
    Bam vao dong y
    Close Application

BP-2943
    [Documentation]    Check  mo giao dien goi di
    [Tags]    BP-2943    1sim
    Mo Btalk
    Nhan giu phim 2
    Page Should Contain Element    ${dt_ket_thuc_cuoc_goi}
    Bam nut ket thuc cuoc goi
   

BP-2944
    [Documentation]    Check Th bam va giu phim so 0
    [Tags]    BP-2944    1sim
    Mo Btalk
    Long Press    ${nut_0}
    Element Attribute Should Match    ${dt_man_hinh_hien_thi_nhap_sdt}    text    +


BP-2973
    [Documentation]   goi so vua lien he  Check Th bam 1 lan vao tab dien thoai/nut goi
    [Tags]    BP-2973     1sim
    Mo Btalk roi mo Gan day
    ${get_text}=    AppiumLibrary.Get Text   ${dt_gd_sdt1}
    Bam tab dien thoai
    Bam tab dien thoai
    ${get_text_2}=     AppiumLibrary.Get Text   ${dt_man_hinh_hien_thi_nhap_sdt}
    Should Be Equal    ${get_text}     ${get_text_2}
    Dong toan bo ung dung va ket thuc phien kiem thu
    Mo Btalk
    An nut quay so
    ${get_text_3}=    AppiumLibrary.Get Text   ${dt_man_hinh_hien_thi_nhap_sdt}
    Should Be Equal    ${get_text}     ${get_text_3}
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
