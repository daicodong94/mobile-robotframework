*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
ThinhLQ         | 2021/07/23 | update code , tach nho case rieng biet
----------------|------------|--------------------------------------------------------------------

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/nghe_dai/yeu_thich.robot
Resource    ../page/nghe_dai/tat_ca_kenh.robot

Suite Setup       Mo Nghe dai
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-2045
    [Documentation]    Kiem tra chuc nang luu kenh
    [Tags]    BP-2045
    Cho phep cap quyen cho ung dung
    Sleep    10
    Thuc hien click chon tab tat ca kenh
    Thuc hien bam chon vov1 duoc phat tat ca kenh
    Kiem tra nut play co hoat dong khi bat dau mo app
    Vuot quay ve man hinh Home
    Mo Nghe dai tu Launcher
    Thuc hien kiem tra kenh vov1 dang duoc phat

BP-2047
    [Documentation]    Kiem tra chuc nang luu gia tri tab khi back app
    [Tags]    BP-2047
    AppiumLibrary.Go Back
    Thuc hien kiem tra da an app nghe dai tro ve home
    Thuc hien kiem tra trang thai khong o tab man hinh tat ca kenh
    Mo Nghe dai tu Launcher
    Page Should Contain Element    ${nd_tab_yeu_thich}
    Page Should Not Contain Element    ${nd_tctl_btn_tat_cac_the_loai}
    Thuc hien stop kenh dang chay

BP-2038
    [Documentation]    Check chuc nang phat kenh khong co mang
    [Tags]    BP-2038
    Mang di dong = 1, data = 0, wifi = 0, may bay = 0
    Kiem tra nut play co hoat dong khi bat dau mo app
    Page Should Not Contain Element    ${nd_yt_txt_dang_phat}
    Page Should Contain Element    ${nd_yt_txt_khong_co_ket_noi}
    Sleep    1

BP-2040
    [Documentation]    Check chuc nang phat kenh co mang
    [Tags]    High    BP-2040
    Mang di dong = 1, data = 1, wifi = 1, may bay = 0
    Kiem tra nut play co hoat dong khi bat dau mo app
    Page Should Contain Element    ${nd_yt_txt_dang_phat}
    Sleep    1
    Thuc hien click chon tab tat ca kenh

BP-2043
    [Documentation]   Kiem tra hien thi man hinh Tat ca kenh
    [Tags]    BP-2043
    Thuc hien stop kenh dang chay
    Page Should Contain Element    ${nd_tctl_btn_tat_cac_the_loai}
    Sleep    1
    Thuc hien kiem tra danh sach hien thi tai man hinh tat ca kenh

BP-1858
    [Documentation]    Thuc hien thao tac vuot mo giao dien tat ca kenh
    [Tags]    BP-1858
    Vuot sang man hinh ben trai
    Vuot sang man hinh ben phai
    Page Should Contain Element    ${nd_tctl_btn_tat_cac_the_loai}
    Sleep    1
    Thuc hien kiem tra dang o tai man hinh tat ca kenh

BP-1876
    [Documentation]    Thuc hien kiem tra mo giao dien tat ca kenh
    [Tags]    BP-1876    BP-5085    BP-5086    BP-5087    BP-5088    BP-5089    BP-1877    BP-1878    BP-1879    BP-1880    BP-1881
    ...    BP-1884    BP-1888    BP-1890    BP-1891    BP-1894    BP-1906    BP-1909    BP-1916    BP-1919    BP-1920    BP-1923    BP-1872
    ...    BP-1928    BP-1930    BP-1931    BP-1932    BP-1933    BP-1934    BP-1935    BP-1936    BP-1937    BP-1938    BP-1939    BP-1940
    ...    BP-1942    BP-1943    BP-1944    BP-1945    BP-1946    BP-1947    BP-1948    BP-1949    BP-1950    BP-1951    BP-1952    BP-1953
    ...    BP-1954    BP-1955    BP-1956    BP-1957    BP-1958    BP-1959    BP-1960    BP-1961    BP-1962    BP-1963    BP-1964    BP-1965
    ...    BP-1966    BP-1967    BP-1968    BP-1969    BP-1971    BP-1974    BP-1975    BP-1977    BP-1978    BP-1980    BP-1982    BP-1990
    ...    BP-1991    BP-1992    BP-1993    BP-1994    BP-1995    BP-1997    BP-1998    BP-1999    BP-2000    BP-2001    BP-2003    BP-1941
    ...    BP-2005    BP-2006    BP-2007    BP-2008    BP-2009    BP-2010    BP-2012    BP-2014    BP-2017    BP-2020    BP-2022    BP-2024
    ...    BP-2026    BP-2028
    Kiem tra nut play co hoat dong khi bat dau mo app
    Chup anh man hinh va so sanh voi anh mau    ${nd_alias}    kc_d_ttdh_d_d_TatCaKenh_MacDinh   4
    Thuc hien stop kenh dang chay
    Chup anh man hinh va so sanh voi anh mau    ${nd_alias}    kc_d_ttdh_d_d_Stop_TatCaKenh   4
    Thuc hien play kenh dang dung
    Thuc hien click tab 3 cham
    Chup anh man hinh va so sanh voi anh mau    ${nd_alias}    kc_d_ttdh_d_d_TatCaKenh_ttct   4

BP-2052
    [Documentation]    check chuc nang chuyen kenh
    [Tags]    BP-2052
    Go Back
    Thuc hien stop kenh dang chay
    Thuc hien click chon tab tat ca kenh
    Thuc hien bam chon vov1 duoc phat
    Thuc hien bam chon vov2 duoc phat

BP-2053
    [Documentation]    Kiem tra chuc nang chon yeu thich
    [Tags]    BP-2053
    Vuot sang man hinh ben trai
    Click Element    ${nd_txt_vov3}
    Sleep    2
    Thuc hien stop kenh dang chay
    Thuc hien click chon tab tat ca kenh
    Vuot launcher len tren
    Sleep    5
    Thuc hien click them yeu thich
    Thuc hien kiem tra da them kenh vao man hinh yeu thich

BP-2054
    [Documentation]    Kiem tra chuc nang huy yeu thich
    [Tags]    BP-2054
    Thuc hien click chon tab tat ca kenh
    Vuot launcher len tren
    Thuc hien click bo chon yeu thich
    Thuc hien kiem tra da huy kenh tai man hinh yeu thich

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================