*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/may_tinh/mt_common.robot

Suite Setup    Mo May tinh
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu
*** Test Cases ***

BP-209
    [Documentation]    Check chuc nang hien thi kich thuoc khi nhap 8 so tro len
    [Tags]    BP-209    BP-224    BP-225  BP-226    BP-227    BP-228    BP-230    BP-233    BP-235     BP-237    BP-238
    An nut 12345678901234556
    Sleep    2
    ${get_text}=   AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text     *12**345**678**901*

BP-211
    [Documentation]    Check chuc nang hien thi du lieu loi
    [Tags]    BP-211
    Xoa man hinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    1
    An nut ham In trong nang cao
    Sleep    1
    An nut man hinh nang cao
    An nut so 0
    An nut dau (=)
    Element Attribute Should Match    ${mt_loi_ko_phai_so}    text    *${text_khong}**${text_phai}**${text_so}*
    Sleep    2
    Xoa man hinh

BP-220
    [Documentation]    Check chuc nang khi cham vao lich su
    [Tags]    BP-220
    An nut lich su
    AppiumLibrary.Element Should Be Enabled    ${mt_nut_lich_su}
    An nut lich su

BP-241
    [Documentation]    Check nut dau phay thap phan
    [Tags]    BP-241
    Log To Console    Check nut dau phay thap phan
    An nut phay thap phan
    AppiumLibrary.Element Should Be Enabled    ${mt_nut_phay}
    ${get_text}=    AppiumLibrary.Get Text   ${mt_hien_thi_phep_tinh}

BP-222
    [Documentation]    Check chuc nang khi cham vao nang cao
    [Tags]    BP-222
    An nut man hinh nang cao
    AppiumLibrary.Element Should Be Enabled    ${mt_nut_nang_cao}
    Sleep    2
    An nut man hinh nang cao
    Close Application

BP-244
    [Documentation]    Check nut dau chia (:) khi da nhap so
    [Tags]    BP-244
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut dau chia (÷)
    AppiumLibrary.Page Should Contain Element    ${mt_nut_chia}
    Sleep    2

BP-246
    [Documentation]    Check nut dau (:) khi co nhap so
    [Tags]    BP-246
    An nut 1234567
    Sleep    2
    An nut dau chia (÷)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *1**234**567÷*

BP-247
    [Documentation]    Check nhap dau tru (-) va dau chia (:)
    [Tags]    BP-247
    Xoa man hinh
    Sleep    2
    An nut dau tru (-)
    Sleep    2
    An nut dau chia (÷)
    Log To Console    ket qua mong muon hien thi dau -
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}     −

BP-259
    [Documentation]    Check nhap dau nhan (x) chua nhap so
    [Tags]    BP-259
    Xoa man hinh
    Sleep    2
    An nut dau nhan (x)
    AppiumLibrary.Page Should Contain Element    ${mt_nut_nhan}
    Sleep    2
    An nut dau tru (-)
    An nut dau nhan (x)

BP-261
    [Documentation]    ket qua mong muon hien thi dau -
    [Tags]    BP-261
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}     −

BP-260
    [Documentation]    Check nut dau nhan (x) khi da nhap so
    [Tags]    BP-260
    Xoa man hinh
    Sleep    2
    An nut 1234567
    Sleep    2
    An nut dau nhan (x)
    Log To Console    ket qua mong muon hien thi dau chia truoc so da nhap
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *1**234**567×*

BP-265
    [Documentation]    Check nhap dau nhan (+) khi chua nhap so
    [Tags]    BP-265
    Xoa man hinh
    Sleep    2
    An nut dau cong (+)
    AppiumLibrary.Page Should Contain Element    ${mt_nut_cong}
    Sleep    2


BP-267
    [Documentation]    03-Man hinh da nhap - khi da nhap so
    [Tags]    BP-267
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}     −

BP-266
    [Documentation]    Check nut dau cong (+) khi da nhap so
    [Tags]    BP-266
    Xoa man hinh
    Sleep    2
    An nut 1234567
    An nut dau cong (+)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *1**234**567+*

BP-268
    [Documentation]    Check nhap dau tru (-) khi khong nhap so
    [Tags]    BP-268
    Sleep    2
    Xoa man hinh
    Sleep    2
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    −

BP-270
    [Documentation]    Check nhap dau tru (−)
    [Tags]    BP-270
    Xoa man hinh
    Sleep    2
    An nut 1234567
    Sleep    2

BP-272
    [Documentation]    Check nut dau tru (−) khi da nhap so
    [Tags]    BP-272
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *1**234**567−*
    Xoa man hinh
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
