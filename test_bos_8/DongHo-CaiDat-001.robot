*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary
Library     String

Resource    ../page/dong_ho/cai_dat.robot
Resource    ../page/dong_ho/dh_common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/common.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***

BP-783
    [Documentation]    Check mo giao dien Cai dat khi cham vao bieu tuong dau ... tu giao dien Quan ly bao thuc
    [Tags]    BP-783    BP-784    BP-785    BP-786
    Log To Console    Check mo giao dien Cai dat khi cham vao bieu tuong dau ... tu giao dien Quan ly bao thuc
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Cham vao dau ba cham
    Nhan vao nut cai dat
    AppiumLibrary.Page Should Contain Element    ${dh_hien_thi_kieu_dong_ho}
    Log To Console    BP-784: Check mo giao dien Cai dat khi cham vao bieu tuong dau ... tu giao dien Dong ho
    Go Back
    Giao dien dong ho
    Cham vao dau ba cham
    Nhan vao nut cai dat
    AppiumLibrary.Page Should Contain Element    ${dh_hien_thi_kieu_dong_ho}
    Log To Console    BP-785: Check mo giao dien Cai dat khi cham vao bieu tuong dau ... tu giao dien Bo hen gio
    Go Back
    Giao dien bo hen gio
    Cham vao dau ba cham
    Nhan vao nut cai dat
    AppiumLibrary.Page Should Contain Element    ${dh_hien_thi_kieu_dong_ho}
    Log To Console    BP-786: Check mo giao dien Cai dat khi cham vao bieu tuong dau ... tu giao dien Bam gio
    Go Back
    Giao dien dong ho bam gio
    Cham vao dau ba cham
    Nhan vao nut cai dat
    AppiumLibrary.Page Should Contain Element    ${dh_hien_thi_kieu_dong_ho}
    Close Application

BP-787
    [Documentation]    Check hien thi text dong ho
    [Tags]    BP-789    BP-788    BP-790    BP-791    BP-792    BP-798
    Log To Console    Check hien thi text dong ho
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Cham vao dau ba cham
    Nhan vao nut cai dat
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_dau_caidat    3
    Log To Console    Check hien thi cham vao kieu
    Kieu hien thi
    AppiumLibrary.Page Should Contain Element    ${dh_hien_thi_kieu_dong_ho_kim}
    AppiumLibrary.Page Should Contain Element    ${dh_btn_check_kieu_dong_ho}
    Log To Console    Check hien thi kieu dong ho kim tren tab dong ho khi cho kieu dong ho kim
    Hien thi dong ho kim
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_dh_kim_dongho    5
    Vuot back ve giao dien truoc
    Giao dien dong ho
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_dh_kim_caidat    5
    Log To Console    Check hien thi kieu dong ho so tren tab dong ho khi chon kieu dong ho so
    Cham vao dau ba cham
    Nhan vao nut cai dat
    Kieu hien thi
    Hien thi dong ho so
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_dau_caidat    3
    Vuot back ve giao dien truoc
    Giao dien dong ho
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_dongho    3
    Close Application

BP-793
    [Documentation]    Check hien thi thoi gian voi so giay mac dinh tat
    [Tags]    BP-793    BP-797    BP-796
    Log To Console    Check Hien thi thoi gian voi so giay khi mac dinh tat
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Cham vao dau ba cham
    Nhan vao nut cai dat
    Element Should Contain Text    ${dh_on_off_hien_thi_thoi_gian}    TẮT
    Log To Console    BP-797: Check hien thi dong ho tren tab Dong ho khi Hien thi thoi gian voi so giay bat va cai dat kieu dong ho so
    AppiumLibrary.Click Element    ${dh_on_off_hien_thi_thoi_gian}
    Vuot back ve giao dien truoc
    Giao dien dong ho
    AppiumLibrary.Page Should Contain Element    ${dh_time_seconds}
    Log To Console    BP-796: Check hien thi thoi gian tren tab dong ho voi kieu dong ho kim va hien thi thoi gian voi so giay bat
    Cham vao dau ba cham
    Nhan vao nut cai dat
    Kieu hien thi
    Hien thi dong ho kim
    Vuot back ve giao dien truoc
    Giao dien dong ho
    AppiumLibrary.Page Should not Contain Element    ${dh_time_seconds}
    Close Application

BP-805
    [Documentation]    Check hien thi Mui gio chinh bi chim khi tat thoat ra vao lai cai dat
    [Tags]    BP-804    BP-805    BP-806    BP-807
    ...    BP-808    BP-809    BP-811    BP-812
    Log To Console    BP-804: Check Mui gio chinh hien thi chim khi Dong ho tu dong tat
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Cham vao dau ba cham
    Nhan vao nut cai dat
    cham vao nut on off dong ho tu dong
    Chup anh man hinh va so sanh voi anh mau    ${dh_alias}    kc_d_ttdh_d_d_dau_tat_gtd_caidat    3
    ${get_text}=    AppiumLibrary.Get Text    ${dh_check_mui_gio_ho_chi_minh}
    Element Should Contain Text    ${dh_check_mui_gio_ho_chi_minh}    ${get_text}
    Log To Console    BP-805: Check hien thi mui gio chinh bi chim khi thoat ra va vao lai cai dat
    Vuot back ve giao dien truoc
    Cham vao dau ba cham
    Nhan vao nut cai dat
    ${get_text}=    AppiumLibrary.Get Text    ${dh_check_mui_gio_ho_chi_minh}
    Element Should Contain Text    ${dh_check_mui_gio_ho_chi_minh}    ${get_text}
    Log To Console    BP-806: Check hien thi mui gio ro net khi bat dong ho tu dong
    cham vao nut on off dong ho tu dong
    ${get_text}=    AppiumLibrary.Get Text    ${dh_check_mui_gio_ho_chi_minh}
    Element Should Contain Text    ${dh_check_mui_gio_ho_chi_minh}    ${get_text}
    Log To Console    BP-807: Check hien thi mui gio chinh hien thi khi thoat ra va vao lai cai dat
    Log To Console    BP-808: Check hien thi mui gio chinh mac dinh: (GMT+7:00) Ho Chi Minh
    Vuot back ve giao dien truoc
    Cham vao dau ba cham
    Nhan vao nut cai dat
    ${get_text}=    AppiumLibrary.Get Text    ${dh_check_mui_gio_ho_chi_minh}
    Element Should Contain Text    ${dh_check_mui_gio_ho_chi_minh}    ${get_text}
    AppiumLibrary.Click Element    ${dh_check_mui_gio_chinh}
    Vuot xuong
    Log To Console    BP-811: Check hien thi mui gio chinh mac dinh khac vd: jakarta
    Chon mui gio Jakarta
    AppiumLibrary.Page Should Contain Element    ${dh_check_mui_gio_hien_thi_jakarta}
    Log To Console    BP-812: Check dong popup, quay ve man hinh Cai dat khi cham button Huy o man hinh danh sach mui gio
    AppiumLibrary.Click Element    ${dh_check_mui_gio_chinh}
    Cham vao huy
    AppiumLibrary.Page Should Not Contain Element    ${dh_btn_huy}
    Close Application


BP-813

    [Documentation]    Check hien thi ngay va gio cua Cai dat he thong khi cham vao thay doi ngay gio
    [Tags]    BP-813    BP-814    BP-815
    Log To Console    Check hien thi ngay va gio cua Cai dat he thong khi cham vao thay doi ngay gio
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Cham vao dau ba cham
    Nhan vao nut cai dat
    Cham vao thay doi ngay gio
    AppiumLibrary.Page Should Contain Element    ${dh_cd_thoi_gian}
    Log To Console    BP-814: Check hien thi thu, ngay thang hien tai tren tab Dong ho khi Dat ngay mac dinh
    Vuot back ve giao dien truoc
    Vuot back ve giao dien truoc
    Giao dien dong ho
    AppiumLibrary.Page Should Contain Element    ${dh_ngay_trong_dong_ho}
    AppiumLibrary.Page Should Contain Element    ${dh_dong_ho_so_trong_dong_ho}
    Log To Console    BP-815: Check hien thi thu, ngay thang hien tai tren tab Dong ho khi thay doi ngay thang o Dat ngay
    Cham vao dau ba cham
    Nhan vao nut cai dat
    Cham vao thay doi ngay gio
    Click icon tat tu dong cap nhat ngay theo mang
    ngay trong thay doi ngay va gio
    Cham vao ngay 1 trong thay doi ngay gio
    AppiumLibrary.Get Element Attribute    ${dh_cd_ngay_thang}    text
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_ngay_thang}
    ${get_text_hoa}=    Convert To Upper Case    ${get_text}
    cham vao dong y
    Vuot back ve giao dien truoc
    Vuot back ve giao dien truoc
    Giao dien dong ho
    Element Should Contain Text    ${dh_ngay_trong_dong_ho}    ${get_text_hoa}
    Close Application

BP-816

    [Documentation]    Check hien thi ngay va gio cua Cai dat he thong khi cham vao thay doi ngay gio
    [Tags]    BP-816    BP-817    BP-818     BP-819
    Log To Console    BP-816, BP-817: Check hien thi tren tab dong ho khi thay doi gio mac dinh
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Cham vao dau ba cham
    Nhan vao nut cai dat
    Cham vao thay doi ngay gio
    Click icon tat tu dong cap nhat ngay theo mang
    Cham vao thoi gian trong ngay va gio
    AppiumLibrary.Click Element    ${dh_cd_check_1h}
    Sleep    1
    AppiumLibrary.Click Element    ${dh_cd_check_5min}
    AppiumLibrary.Get Element Attribute    ${dh_cd_gio}    text
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_gio}
    cham vao dong y
    Vuot back ve giao dien truoc
    Vuot back ve giao dien truoc
    Giao dien dong ho
    Element Should Contain Text    ${dh_dong_ho_so_trong_dong_ho}    ${get_text}
    Log To Console    BP-819: Check hien thi thoi gian khi tao bao thuc tran tab quan ly bao thuc
    giao dien bao thuc
    Log To Console    Check hien thi thoi gian khi toa bao thuc nhanh
    cham vao mo rong trong bao thuc
    Cham vao xoa trong bao thuc
    cham vao mo rong trong bao thuc
    Cham vao xoa trong bao thuc
    AppiumLibrary.Click Element    ${dh_bt_bao_thuc_nhanh}
    Sleep    1
    AppiumLibrary.Click Element    ${dh_bt_btn_xong}
    AppiumLibrary.Page Should Contain Element    ${dh_cd_loai_bo}


#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================