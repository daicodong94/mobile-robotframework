*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/chim_lac/cl_common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/xem_phim/thu_muc.robot

Suite Setup       Mo Xem phim
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
# DA LAP THEM SIM
BP-3596
    [Documentation]    Check mo giao dien cuoc goi,goi duoc so dien thoai khi cham vao sdt
    [Tags]             BP-3596
    Cho phep cap quyen cho ung dung
    Cham icon ba cham
    Nhan chon Thong tin
    Nhan chon so dien thoai 024 3763 2552
    Click Chi mot lan khi hien popup
    Page Should Contain Element    ${dt_tab_dt_them_lien_he}
    Page Should Contain Element    ${xp_tm_goi_dien_nut_quay_so}
    Cham nut Goi de thuc hien cuoc goi di
    Page Should Contain Element    ${xp_tm_bkav_photo_sdt}
    Page Should Contain Element    ${xp_tm_goi_dien_nut_loa}
    Page Should Contain Element    ${xp_tm_goi_dien_nut_ban_phim}
    Sleep    1
    Cham nut Ket thuc cuoc goi de huy
    Vuot back ve giao dien truoc

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================