*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary
Library     DateTime

Resource    ../page/common.robot
Resource    ../page/btalk/tn_cuoc_tro_chuyen.robot
Resource    ../page/btalk/tin_nhan.robot
Resource    ../page/btalk/gui_nhan_tin_nhan.robot

Suite Setup    Mo tin nhan tu Btalk

*** Test Cases ***

STN_04
    [Documentation]    Check mo giao dien Them tin nhan moi khi cham vao tab Tin nhan tu giao dien Danh sach cuoc chuyen
    [Tags]    STN_04    High    STN_66
    Bam vao tab tin nhan
    Input Text    ${tn_themmoi_nhap_sdt_gui}    0823
    Bam vao sdt Sim Test
    Bam vao go noi dung tin nhan
    ${noi_dung_tin_nhan}=    Get Current Date
    Input Text    ${tn_go_noi_dung_tin_nhan}    ${noi_dung_tin_nhan}
    Click Element    ${tn_nut_gui}
    Page Should Contain Text    ${noi_dung_tin_nhan}    
    
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================