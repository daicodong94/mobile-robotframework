*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/cai_dat/che_do_dem.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/cai_dat/cd_common.robot

Suite Setup    Mo Cai dat
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu
*** Test Cases ***

CDD_061401
    [Documentation]    Check noi dung chuc nang anh sang dem theo thoi gian khi dang o trang thai tat    
    [Tags]    CDD_061401
    Vuot sang man hinh ben duoi
    Vuot sang man hinh ben duoi
    Bam vao He thong
    Bam vao Ngay va gio
    Bat dinh dang 24h
    Go Back
    Go Back
    Vuot sang man hinh ben tren
    Vuot sang man hinh ben tren
    An vao Hien thi
    Nhap vao tab che do dem hien thi cai dat
    Nhan nut bat check list lich bieu
    Nhan nut bat che do thoi gian tuy chinh tai lich bieu
    Bam vao thoi gian bat dau
    Bam chon 22h
    Bam vao dong y
    Bam vao thoi gian ket thuc
    Bam chon 6h
    Bam vao dong y
    Go Back
    Page Should Contain Text    Đang tắt / Sẽ tự động bật lúc 22:00    
    
CDD_061501
    [Documentation]    Check noi dung chuc nang anh sang dem theo thoi gian khi dang o trang thai bat
    [Tags]    CDD_061501
    Vuot mo bang dieu khien
    Click toa do bat che do dem
    Page Should Contain Text    Bật / Sẽ tự động tắt lúc 6:00    

CDD_070107
    [Documentation]     Check noi dung chuc nang anh sang dem binh minh hoang hon dang o trang thai bat
    [Tags]    CDD_070107
    Nhap vao tab che do dem hien thi cai dat
    Nhan nut bat check list lich bieu
    Nhan nut bat che tu hoang hon den binh minh tai lich bieu
    Go Back
    Page Should Contain Text    Đang tắt / Sẽ tự động bật lúc hoàng hôn    

CDD_070108
    [Documentation]    Check noi dung chuc nang anh sang dem binh minh hoang hon khi dang o trang thai tat
    [Tags]    CDD_070108
    Vuot mo bang dieu khien
    Click toa do bat che do dem
    Page Should Contain Text    Bật / Sẽ tự động tắt lúc bình minh  
   

    

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================    
      