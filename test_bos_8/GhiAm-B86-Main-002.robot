*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/ghi_am/ga_common.robot
Resource    ../page/notification/notification.robot

Suite Setup        Mo Ghi am
Suite Teardown     Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-3988
    [Documentation]    Kiem tra luu file khi nhap chu in hoa vao textbox Ghi ra file
    [Tags]    BP-3988
    Cho phep cap quyen cho ung dung
    Vuot quay ve man hinh Home
    Xoa toan bo du lieu app ghi am trong quan ly file
    Mo Ghi am tu Launcher
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Click Element At Coordinates    ${ga_nut_danh_sach_ban_ghi_x}    ${ga_nut_danh_sach_ban_ghi_y}
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}         AAAA
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    ${get_text_hoa}=    AppiumLibrary.Get Text    ${ga_verify_text_hoa}
    Should Be Equal    ${get_text_hoa}    AAAA
    Sleep    2
    Vuot back ve giao dien truoc

BP-3990
    [Documentation]    Kiem tra luu file khi nhap so vao textbox Ghi ra file
    [Tags]    BP-3990
    An nut bat dau ghi am
    Click Element At Coordinates    ${ga_nut_danh_sach_ban_ghi_x}    ${ga_nut_danh_sach_ban_ghi_y}
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}         123456
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    ${get_text_so}=    AppiumLibrary.Get Text    ${ga_verify_text_so}
    Should Be Equal    ${get_text_so}    123456
    Vuot back ve giao dien truoc

BP-3994
    [Documentation]     Kiem tra nhap cac ky tu dac biet (ki tu @, ~!<>*) vao textbox Ghi ra file
    [Tags]    BP-3994
    An nut bat dau ghi am
    Click Element At Coordinates    ${ga_nut_danh_sach_ban_ghi_x}    ${ga_nut_danh_sach_ban_ghi_y}
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}    !@#$%^&*
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    ${get_text_db}=    AppiumLibrary.Get Text    ${ga_verify_text_ki_tu_dac_biet}
    Should Be Equal    ${get_text_db}    !@#$%^&*
    Sleep    2
    Vuot back ve giao dien truoc

BP-3992
    [Documentation]    Kiem tra chuc nang trim space textbox Ghi ra file
    [Tags]    BP-3992
    An nut bat dau ghi am
    Click Element At Coordinates    ${ga_nut_danh_sach_ban_ghi_x}    ${ga_nut_danh_sach_ban_ghi_y}
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}       ${SPACE}daitvb${SPACE}
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    ${text_cat_space}=    AppiumLibrary.Get Text    ${ga_ban_ghi_list}
    Should Not Be Equal    ${text_cat_space}   ${SPACE}daitvb${SPACE}    
    Vuot back ve giao dien truoc

BP-4009
    [Documentation]    Kiem tra app Ghi am khi tat wifi hoac du lieu di dong
    [Tags]    BP-4009
    Vuot mo bang dieu khien
    Click Element At Coordinates    ${db_nut_wifi_x}    ${db_nut_wifi_y}
    Sleep    2
    Vuot back ve giao dien truoc
    An nut bat dau ghi am
    Page Should Contain Element    ${ga_main_text_trang_thai_ghi_am}
    An nut tam dung ghi am

BP-4004
    [Documentation]    Kiem tra app ghi am khi bat airplane mode
    [Tags]    BP-4004
    Vuot mo bang dieu khien
    Click Element At Coordinates    ${db_nut_cd_may_bay_x}    ${db_nut_cd_may_bay_y}
    Sleep    2
    An nut bat dau ghi am
    Page Should Contain Element    ${ga_main_text_trang_thai_ghi_am}
    An nut danh sach ban ghi
    An nut dong y
    Vuot back ve giao dien truoc
    Log To Console    Bat lai ket noi internet    
    Vuot mo bang dieu khien
    Click Element At Coordinates    ${db_nut_cd_may_bay_x}    ${db_nut_cd_may_bay_y}
    Vuot back ve giao dien truoc

BP-4013
    [Documentation]     Kiem tra ghi am khi vuot ve home
    [Tags]    BP-4013
    Vuot quay ve man hinh Home
    Vuot mo bang dieu khien
    Click Element At Coordinates    ${db_nut_wifi_x}    ${db_nut_wifi_y}
    Vuot back ve giao dien truoc
    Mo Ghi am tu Launcher
    An nut bat dau ghi am
    Vuot quay ve man hinh Home
    Vuot mo bang thong bao
    Sleep    2
    Click Element    ${ga_thongbao_dung}
    Sleep    2
    Page Should Contain Text    ${ga_text_luu_file_ga_thanh_cong}
    Vuot back ve giao dien truoc

BP-4014
    [Documentation]    Kiem tra ghi am khi ve home qua da nhiem
    [Tags]    BP-4014
    An nut bat dau ghi am
    Mo da nhiem
    Sleep    2
    Click Element At Coordinates    926    1924
    Vuot mo bang thong bao
    Sleep    2
    Click Element    ${ga_thongbao_dung}
    Page Should Contain Text    ${ga_text_luu_file_ga_thanh_cong}
    Vuot back ve giao dien truoc
    Vuot quay ve man hinh Home

BP-4025
    [Documentation]    Kiem tra ghi am khi mo che do chia doi man hinh
    [Tags]    BP-4025
    Mo Ghi am tu Launcher
    An nut bat dau ghi am
    Mo da nhiem
    AppiumLibrary.Click Element    ${dn_icon_ung_dung}
    Sleep    2
    AppiumLibrary.Click Element    ${dn_chia_doi_man_hinh}
    ${hien_thi_app_cai_dat}    Run Keyword And Return Status    AppiumLibrary.Page Should Contain Element    ${app_cai_dat}        
    Run Keyword If    ${hien_thi_app_cai_dat}==True    AppiumLibrary.Click Element    ${app_cai_dat}    
    ...    ELSE    Vuot sang trai launcher va click app cai dat
    Sleep    2
    Swipe By Percent    50    50    50    5    duration=2000
    Page Should Contain Element    ${cd_tieu_de}       
    Sleep    2
    Dong toan bo ung dung trong da nhiem

BP-4011
    [Documentation]    Kiem tra ghi am khi tat man hinh
    [Tags]    BP-4011
    Mo Ghi am tu Launcher
    An nut bat dau ghi am
    Vuot quay ve man hinh Home
    Bat hien thi tat ca cac noi dung thong bao
    Sleep    2
    ${get_text_lockscreen}    Get Text    ${ga_lockscreen_dang_ghi_am}
    Page Should Contain Text    ${get_text_lockscreen}
    Mo khoa va dong popup man hinh khoa

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================