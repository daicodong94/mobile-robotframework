*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary
   
Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/dien_thoai.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/btalk/nhat_ki_cuoc_goi.robot
Resource    ../page/btalk/gui_nhan_tin_nhan.robot
Resource    ../page/cai_dat/cd_common.robot

# Suite Setup    Mo Launcher

*** Test Cases ***

BP-2913
    [Documentation]    Checkhien thi icon Them lien he khi nhap sdt chua luu trong danh ba sau khi thy doi kich thuoc phong
    [Tags]    BP-2913
    Mo Cai dat
    Bam chuyen kich thuoc phong va hien thi ve min
    Close Application
    Mo Btalk
    Bam sdt 0977888888
    Sleep    2     
    Page Should Contain Element    ${dt_tab_dt_them_lien_he}    
    Dong toan bo ung dung trong da nhiem
    
BP-2907
    [Documentation]    Check giao dien tab dien thoai khi bat thanh dieu huong
    [Tags]    BP-2907
    Mo Cai dat
    Bam chuyen kich thuoc phong va hien thi ve mac dinh
    Go Back
    Go Back
    Bat thanh dieu huong
    Mo Btalk
    Chup anh man hinh va so sanh voi anh mau    ${btalk_alias}    kc_d_trai_d_d_bat_thanh_dieu_huong    2
        