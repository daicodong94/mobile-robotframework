*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/xem_phim/xem_video.robot

Suite Setup       Mo Xem phim
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-3678
    [Documentation]    Check mo xem video khi bam app Xem phim tu man hinh da nhiem
    [Tags]     High    BP-3678
    Cho phep cap quyen cho ung dung
    Chon video mp4
    Sleep    3
    Bam mo xem phim tu man hinh da nhiem
    
BP-3681
    [Documentation]    Check giao dien hien thi khi cham xem video lan dau tien 
    [Tags]    BP-3681    BP-3694
    Sleep    2    
    Click Element    ${xp_v_sap_xep_theo}
    Sleep    2    
    ${trang_thai}    Run Keyword And Return Status    AppiumLibrary.Page Should Contain Element    ${xp_tm_sap_xep_theo_ngay_moi_nhat} 
    Run Keyword If    ${trang_thai}==True    AppiumLibrary.Click Element    ${xp_tm_sap_xep_theo_ngay_moi_nhat}    
    ...    ELSE    Vuot back ve giao dien truoc   
    Click Element    ${xp_v_video_t5_trong_thu_muc_vd}
    Sleep    4    
    Log To Console    BP-3694    Check hien thi huong dan su dung cac thao tac khi xem video trong app lan dau    
    Page Should Contain Element    ${xp_tm_text_da_nam_duoc}  
    Dong toan bo ung dung trong da nhiem  
    
BP-3683
    [Documentation]    Check giao dien man hinh trinh phat video khi hien thi khu dieu khien
    [Tags]    BP-3683    BP-3682
    Mo app xem phim va mo xem video
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_XemPhim_XemVideo_khu_dieu_khien    2
    Log To Console    BP-3682    Check giao dien man hinh fullscreen khi xoay doc dien thoai
    Click Element At Coordinates    540    1220
    Sleep    2    
    Page Should Not Contain Element    ${xp_v_nut_overplay}   
    Dong toan bo ung dung va ket thuc phien kiem thu
        
BP-3696
    [Documentation]    Check van hien thi huong dan su dung cac thao tac khi chua cham Da nam duoc
    [Tags]    BP-3696
    Mo Xem phim
    Cho phep cap quyen cho ung dung
    Click Element    ${xp_v_mo_video_file_mp4}
    Sleep    2    
    Click Element    ${xp_v_video_t5_trong_thu_muc_vd}
    Sleep    2
    Click Element At Coordinates    ${xp_v_khoang_trong_ngoai_v_x}    ${xp_v_khoang_trong_ngoai_v_y}     
    Sleep    1    
    Vuot back ve giao dien truoc
    Click Element    ${xp_v_video_t5_trong_thu_muc_vd}
    Sleep    1    
    Page Should Contain Element    ${xp_tm_text_da_nam_duoc}    
    Dong toan bo ung dung trong da nhiem    
    
BP-3697
    [Documentation]    Check dong giao dien huong dan su dung khi cham Da nam duoc
    [Tags]    BP-3697    BP-3698
    Mo app xem phim va mo xem video   
    Page Should Not Contain Element    ${xp_tm_text_da_nam_duoc}
    Log To Console    BP-3698    Check khong hien thi lai giao dien huong dan su dung khi xem video lan tiep theo         
    Vuot back ve giao dien truoc
    Click Element    ${xp_v_video_t5_trong_thu_muc_vd}
    Sleep    1    
    Page Should Not Contain Element    ${xp_tm_text_da_nam_duoc}    
    Dong toan bo ung dung trong da nhiem
      
BP-3868
    [Documentation]    Check hien thi kich co, ti le man hinh mac dinh la Tu can chinh
    [Tags]    BP-3868    BP-3873    BP-3874    BP-3901
    Mo app xem phim va mo xem video
    Sleep    2    
    Click Element At Coordinates    ${xp_v_khoang_trong_ngoai_v_x}    ${xp_v_khoang_trong_ngoai_v_y}     
    Click Element    ${xp_v_nut_size}
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_XemPhim_XemVideo_tu_canh_chinh    5
    Log To Console    BP-3873    Check dong video khi vuot Back   
    Log To Console    BP-3901     Check dong man hinh trinh phat video khi thuc hien vuot Back      
    Vuot back ve giao dien truoc
    Sleep    2    
    Page Should Contain Element    ${xp_v_textview}
    Log To Console    BP-3874    Check dong video khi vuot Home
    Click Element    ${xp_v_video_t5_trong_thu_muc_vd}
    Sleep    2    
    Vuot quay ve man hinh Home
    ${hien_thi_xp}=    Get Matching Xpath Count    ${app_xem_phim}
    Run Keyword If    ${hien_thi_xp}>0    AppiumLibrary.Page Should Contain Element    ${app_xem_phim}    
    ...    ELSE    Vuot sang phai man launcher va kiem tra hien thi app xem phim    

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================