*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/btalk/btalk_common.robot
Resource    ../page/btalk/danh_ba.robot
Resource    ../page/btalk/goi_dien_goi_di.robot
Resource    ../page/btalk/dien_thoai.robot

Suite Setup       Mo Launcher

*** Test Cases ***
#data dien thoai luon gom  Sim Test 0823 390 409 A Cuong 0357 360020
DB_010101
    [Documentation]    Kiem tra mo Danh ba tu Launcher
    [Tags]     High    DB_010101
    Mo Danh ba tu Launcher
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-3848
    [Documentation]    Check mo giao dien cuoc goi den lien he khi cham vao icon "dien thoai" voi may lap 1sim
    [Tags]     High    BP-3848    1sim
    Mo Danh ba
    ${get_text}=    AppiumLibrary.Get Text    ${dt_db_ten_lien_he_1}
    An nut dien thoai cua lien he dau tien
    ${get_text2}=  AppiumLibrary.Get Text    ${dt_man_hinh_hien_thi_ten_lien_he_da_luu_khi_goi}
    Should Be Equal    ${get_text}    ${get_text2}    
    
DB_020101
    [Documentation]    Mo danh ba tu tab gan day
    [Tags]             DB_020101
    Mo Btalk roi mo Gan day
    An nut danh ba
    Close All Applications
    
DB_020102
    [Documentation]    Kiem tra vuot mo Danh ba tu tab Gan day
    [Tags]             DB_020102
    Mo Btalk roi mo Gan day
    Vuot sang man hinh ben trai
    Close All Applications

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================