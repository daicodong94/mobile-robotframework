*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary
   
Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/cai_dat/cd_common.robot

*** Test Cases ***
#========================HAI SIM====================================================================================================
BP-4934
    [Documentation]    Check trang thai cua checkbox nut goi khi o che do mac dinh
    [Tags]    BP-4934    2sim
    Mo dien thoai roi vao cai dat dien thoai
    An nut tuy chon hien thi
    Element Attribute Should Match    ${dt_cd_checkbox_tuy_chon_hien_thi}    checked    false   
    Close All Applications

BP-4935
    [Documentation]    Check hien thi 01 nut Goi khi o che do mac dinh
    [Tags]    BP-4935    2sim
    Mo Btalk
    Sleep    2    
    ${kiem_tra_nut_quay_so}=  AppiumLibrary.Get Matching Xpath Count  //*[@content-desc="quay số"]
    Sleep    3    
    Should Be Equal     ${kiem_tra_nut_quay_so}     1  
    Close All Applications
 
BP-4936
    [Documentation]     Check trang thai cua checkbox khi bat hien thi 2 sim
    [Tags]    BP-4936    2sim
    Mo dien thoai roi vao cai dat dien thoai
    An nut tuy chon hien thi
    An vao checkbox hien thi 2 nut goi cho 2 sim o man tuy chon hien thi
    Element Attribute Should Match    ${dt_cd_checkbox_tuy_chon_hien_thi}    checked    true   
    Close All Applications   
       
BP-4937 
    [Documentation]    Check hien thi 02 nut Goi sau khi bat chuc nang 2 nut goi cho 2 sim
    [Tags]    BP-4937     2sim
    Mo Btalk
    Sleep    2    
    ${kiem_tra_nut_quay_so}=  AppiumLibrary.Get Matching Xpath Count  //*[@content-desc="quay số"]
    Sleep    3    
    Should Be Equal     ${kiem_tra_nut_quay_so}     2  
    Close Application
    
BP-4938
    [Documentation]    Check trang thai cua checkbox khi tat hien thi nut goi cho 2 sim
    [Tags]    BP-4938        2sim
    Mo dien thoai roi vao cai dat dien thoai
    An nut tuy chon hien thi
    An vao checkbox hien thi 2 nut goi cho 2 sim o man tuy chon hien thi
    Element Should Be Enabled         ${dt_cd_checkbox_tuy_chon_hien_thi}
    Element Attribute Should Match    ${dt_cd_checkbox_tuy_chon_hien_thi}    checked    false   
    Close Application
    
BP-4939
    [Documentation]    Check hien thi 01 nut Goi sau khi tat chuc nang 2 nut goi cho 2 sim
    [Tags]    BP-4939    2sim
    Mo Btalk
    Sleep    2    
    ${kiem_tra_nut_quay_so}=  AppiumLibrary.Get Matching Xpath Count  //*[@content-desc="quay số"]
    Sleep    3    
    Should Be Equal     ${kiem_tra_nut_quay_so}     1  
    Close All Applications
   
BP-4962
    [Documentation]    Check nhac chuong mac dinh cua Sim1
    [Tags]    BP-4962    2sim
    Log To Console     Check nhac chuong mac dinh cua Sim1
    Mo dien thoai vao cai dat va vao am thanh va rung
    Element Attribute Should Match    ${dt_cd_ten_nhac_chuong_sim1}    text    Đàn T'rưng 1    
    
BP-5176
    [Documentation]    Check hien 2 lua chon CHI MOT LAN va LUON CHON khi cham vao Bo nho da phuong tien sim 1
    [Tags]    BP-5176    2sim
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim1
    Page Should Contain Element    ${dt_cd_nut_luon_chon}
    Page Should Contain Element    ${dt_cd_nut_chi_mot_lan}    
    An vao nut chon chi mot lan
    Page Should Contain Element    ${dt_cd_nut_dong_y}    
    Page Should Contain Element    ${dt_cd_nut_huy}            
    
BP-5175 
    [Documentation]    Check mo giao dien Hoan tat tac vu cua sim 1
    [Tags]    BP-5175      2im
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim1
    Page Should Contain Text    Hoàn tất tác vụ bằng Bộ nhớ phương tiện   
    Page Should Contain Element    ${dt_cd_nut_luon_chon}
    Page Should Contain Element    ${dt_cd_nut_chi_mot_lan} 
    Close Application    
    

BP-5186
    [Documentation]    Check mo danh sach Nhac chuong dien thoai sau khi da chon LUON chon da phuong tien
    [Tags]    BP-5186     2sim    BP-5177
    Log To Console     Check mo danh sach nhac chuong dien thoai sau khi da chon luon chon    
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim1
    Sleep    1    
    An vao nut luon chon
    Page Should Contain Element    ${dt_cd_nut_dong_y}    
    Page Should Contain Element    ${dt_cd_nut_huy}              
    Sleep   2   
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim1
    Page Should Contain Element    ${dt_cd_nut_dong_y}    
    Page Should Contain Element    ${dt_cd_nut_huy}    
    Close Application      

BP-5178
    [Documentation]    Check luu lua chon khi chon nhac chuong Khong o sim 1
    [Tags]    BP-5178     2sim    BP-5179     
    Log To Console     Check luu lua chon khi chon nhac chuong khong    
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim1
    Vuot launcher danh sach nhac xuong down
    Bam vao nhac chuong khong
    Element Attribute Should Match    ${dt_cd_nut_nhac chuong_khong}    checked    true
    Bam vao dong y
    Page Should Contain Text    Âm thanh và rung 
    Page Should Contain Text    Không có    
    Dong toan bo ung dung va ket thuc phien kiem thu    
 

BP-5182
    [Documentation]    Check luu nhac chuong da chon tu danh sach nhac chuong Am thanh trong may
    [Tags]    BP-5182     2sim    
    Log To Console     Check luu nhac chuong da chon tu danh sach nhac chuong Am thanh trong may  
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim1
    An vao nhac chuong hop nhac
    Bam vao dong y
    Page Should Contain Text    Hộp nhạc   
    Vuot quay ve man hinh Home  
    
BP_XOA  
    [Documentation]    XOA MAC DINH BO NHO PHUONG TIEN
    [Tags]    BP_XOA
    Xoa mac dinh bo nho phuong tien
         
BP-5187
    [Documentation]    Check mo giao dien Quan li file khi cham vao Quan li file
    [Tags]    BP-5187         2sim
    Log To Console     Check mo giao dien Quan li file khi cham vao Quan li file    
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim1
    An vao nut Quan li file
    Page Should Contain Text    Quản lý file    
    Sleep    2    
    Page Should Contain Text    Android    
    Close Application    
  
BP-5191 
    [Documentation]    Check mo giao dien Hoan tat tac vu bang Quan ly file sau khi da chon  CHI MOT LAN
    [Tags]    BP-5191      2sim
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim1
    An vao nut chon chi mot lan
    Page Should Contain Text    Quản lý file    
    Page Should Contain Text    Android   
    Close Application
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim1
    Page Should Contain Element    ${dt_cd_nut_chi_mot_lan}    
    Page Should Contain Element    ${dt_cd_nut_luon_chon}    
    Close Application
  
BP-5192 
    [Documentation]    Check mo giao dien quan li file  sau khi da chon LUON CHON sim 1
    [Tags]    BP-5192  2sim
    Log To Console    Check mo giao dien quan li file sau khi da chon LUON CHON sim 1   
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim1
    An vao nut luon chon
    Page Should Contain Text    Quản lý file    
    Page Should Contain Text    Android   
    Close Application
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim1
    Page Should Contain Text    Quản lý file    
    Page Should Contain Text    Android 
    Close All Applications
    Xoa mac dinh Quan ly file
    Close Application
  
BP-4965
    [Documentation]    Check nhac chuong mac dinh cua sim 2
    [Tags]    BP-4965    2sim    BP-5197 
    Mo dien thoai vao cai dat va vao am thanh va rung
    Element Attribute Should Match    ${dt_cd_ten_nhac_chuong_sim2}  text   Đàn T'rưng 1
  
 
BP-5196 
    [Documentation]    Check mo giao dien Hoan tat tac vu cua sim 2
    [Tags]    BP-5196      2im
    Log To Console    Check mo giao dien Hoan tat tac vu cua sim2  
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim2
    An vao nut bo nho phuong tien
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim2
    Page Should Contain Text    Hoàn tất tác vụ bằng Bộ nhớ phương tiện   
    Page Should Contain Element    ${dt_cd_nut_luon_chon}
    Page Should Contain Element    ${dt_cd_nut_chi_mot_lan} 
        
BP-5198
    [Documentation]    Check mo danh sach nhac chuong dien thoai bo nho da phuong tien chi mot lan sim 2
    [Tags]    BP-5198    2sim
    Log To Console     Check mo danh sach nhac chuong dien thoai  bo nho da phuong tien chi mot lan  sim 2
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim2
    An vao nut chon chi mot lan
    Page Should Contain Element    ${dt_cd_nut_dong_y}    
    Page Should Contain Element    ${dt_cd_nut_huy}    
    Close Application
 
BP-5207
    [Documentation]    Check mo danh sach Nhac chuong dien thoai sau khi da chon LUON chon da phuong tien sim 2
    [Tags]    BP-5207     2sim    
    Log To Console     Check mo danh sach nhac chuong dien thoai sau khi da chon luon chon    sim 2
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim2
    An vao nut luon chon
    Page Should Contain Text    Nhạc chuông Sim 2  
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim2
    Page Should Contain Text    Nhạc chuông Sim 2    
    Close Application      

BP-5199
    [Documentation]    Check luu lua chon khi chon nhac chuong Khong o sim 2
    [Tags]    BP-5199     2sim    BP-5200     
    Log To Console     Check luu lua chon khi chon nhac chuong khong   sim 2 
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim2
    Vuot sang man hinh ben tren
    Bam vao nhac chuong khong
    Element Attribute Should Match    ${dt_cd_nut_nhac chuong_khong}    checked    true
    Bam vao dong y
    Page Should Contain Text    Âm thanh và rung 
    Page Should Contain Text    Không có    
    Close Application      
    
BP-5203
    [Documentation]    Check luu nhac chuong da chon tu danh sach nhac chuong Am thanh trong may sim 2
    [Tags]    BP-5203     2sim    
    Log To Console     Check luu nhac chuong da chon tu danh sach nhac chuong Am thanh trong may  sim 2
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim2
    An vao nhac chuong hop nhac
    Bam vao dong y
    Page Should Contain Text    Hộp nhạc   
    Sleep    2    
    Xoa mac dinh bo nho phuong tien
    Close Application    

BP-5208 
    [Documentation]    Check mo giao dien Quan li file khi cham vao Quan li file
    [Tags]    BP-5208          2sim
    Log To Console     Check mo giao dien Quan li file khi cham vao Quan li file    
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim2
    An vao nut Quan li file
    Page Should Contain Text    Quản lý file    
    Sleep    2    
    Page Should Contain Text    Android    
    Close Application    
  
BP-5212 
    [Documentation]    Check mo giao dien Hoan tat tac vu bang Quan ly file sau khi da chon CHI MOT LAN sim 2
    [Tags]    BP-5212      2sim
    Log To Console    Check mo giao dien Hoan tat tac vu bang Quan ly file sau khi da chon CHI MOT LAN sim 2    
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim2
    An vao nut chon chi mot lan
    Page Should Contain Text    Quản lý file    
    Page Should Contain Text    Android   
    Close Application
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim2
    Page Should Contain Element    ${dt_cd_nut_chi_mot_lan}    
    Page Should Contain Element    ${dt_cd_nut_luon_chon}    
    Close Application
  
BP-5213 
    [Documentation]    Check mo giao dien quan li file sau khi da chon LUON CHON sim 2
    [Tags]    BP-5213  2sim
    Log To Console     Check mo giao dien quan li file sau khi da chon LUON CHON sim 2    
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim2
    An vao nut luon chon
    Page Should Contain Text    Quản lý file    
    Page Should Contain Text    Android   
    Close Application
    Mo dien thoai vao cai dat va vao am thanh va rung
    An nut nhac chuong sim2
    Page Should Contain Text    Quản lý file    
    Page Should Contain Text    Android 
    Close All Applications
    Xoa mac dinh Quan ly file
    Close Application
  
BP-5049  
    [Documentation]    Check mo popup Thuc hien cuoc goi bang
    [Tags]    BP-5049  2sim    BP-5050
    Log To Console     Check mo popup Thuc hien cuoc goi bang    
    Mo cai dat dien thoai va vao Tai khoan goi   
    An nut thuc hien cuoc goi bang
    Page Should Contain Text    Thực hiện cuộc gọi bằng    
    Page Should Contain Element       ${dt_cd_nut_huy}
    An nut hoi truoc tai popup thuc hien cuoc goi bang
    Go Back
    Go Back
    Log To Console  Check hien thi popup lua chon sim o che do mac dinh hoi truoc      
    Nhap sđt 0385986157
    An nut quay so
    Page Should Contain Text    Gọi bằng  
      
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
