*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
DaiTVb          | 2021/07/02 | Tao tap tin
----------------|------------|--------------------------------------------------------------------
CuongNHd        | 2021/07/20 | Refactor
----------------|------------|--------------------------------------------------------------------

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/suc_khoe/sk_common.robot

Suite Setup       Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5707
    [Documentation]    Mo ung dung suc khoe tu Launcher
    [Tags]     High    BP-5707
    Mo Suc khoe tu Launcher

BP-5708
    [Documentation]    Kiem tra tao bai tap thanh cong
    [Tags]     High    BP-5708
    Tao bai tap voi muc tieu thoi gian    59
    Log To Console     Kiem tra tao bai tap thanh cong
    AppiumLibrary.Page Should Contain Element    ${sk_txt_thoi_gian}
    AppiumLibrary.Page Should Contain Element    ${nut_tam_dung}
    AppiumLibrary.Page Should Contain Element    ${nut_ket_thuc}
    Bam nut ket thuc bai tap

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================