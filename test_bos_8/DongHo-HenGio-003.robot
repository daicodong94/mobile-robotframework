*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/dong_ho/hen_gio.robot
Resource    ../page/common.robot
Resource    ../page/dong_ho/dh_common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-706
    [Documentation]    Check TH bam icon Thung rac khi Bo hen gio dang chay
    [Tags]    BP-706
    Log To Console    Check TH bam icon Thung rac khi Bo hen gio dang chay
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Thuc hien thiet lap bo hen gio kiem tra nut xoa khi bo hen gio dang chay
    Close Application

BP-707
    [Documentation]    Check TH bam icon Thung rac khi Bo hen gio dang dung
    [Tags]    BP-707
    Log To Console    Check TH bam icon Thung rac khi Bo hen gio dang dung
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Thuc hien thiet lap bo hen gio kiem tra nut xoa khi bo hen gio da tam dung
    Close Application

BP-708
    [Documentation]    Check TH bam vao icon them Bo hen gio
    [Tags]    BP-708    BP-709    BP-710
    Log To Console    Check TH bam vao icon them Bo hen gio
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Thuc hien thiet lap bo hen gio moi
    Close Application

BP-711
    [Documentation]    Check vi tri cua bo hen gio khi vuot thay doi vi tri cho nhau
    [Tags]    BP-712    BP-713     BP-714
    Log To Console    Check vi tri cua bo hen gio khi vuot thay doi vi tri cho nhau
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Thuc hien vuot chuyen tu giao dien bo hen gio 1 den giao dien 2 va 3
    Close Application

BP-715
    [Documentation]    Check TH xoa Bo hen gio 2, giu lai Bo hen gio 1 va Bo hen gio 3
    [Tags]    BP-715    BP-710    BP-716
    Log To Console    Check TH xoa Bo hen gio 2, giu lai Bo hen gio 1 va Bo hen gio 3
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Thuc hien them 3 bo hen gio va xoa bo 2 giu 1 va 3

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================