*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/may_tinh/mt_common.robot

Suite Setup    Mo May tinh
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu
*** Test Cases ***

BP-331
    [Documentation]    kiem tra phep tinh nhan (x) voi 2 loai phep tinh
    [Tags]    BP-331
    An nut 123
    An nut dau chia (÷)
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    123÷−

BP-332
    [Documentation]    kiem tra phep tinh chia (:) voi phep tinh cong
    [Tags]    BP-332
    Xoa man hinh
    An nut 123
    An nut dau chia (÷)
    An nut dau cong (+)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    123+

BP-334
    [Documentation]    kiem tra phep tinh chia (:) voi phep tinh nhan
    [Tags]    BP-334
    An nut dau chia (÷)
    An nut dau nhan (x)
    ${get_textt}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_textt}    123×
# __________ UPDATE DEN DAY______________________
BP-335
    [Documentation]    kiem tra phep tinh nhan (x) voi phep tinh cong
    [Tags]    BP-335
    Xoa man hinh
    An nut 123
    An nut dau nhan (x)
    An nut dau cong (+)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    123+

BP-337
    [Documentation]    kiem tra phep tinh nhan (x) voi phep tinh chia
    [Tags]    BP-337
    An nut dau nhan (x)
    An nut dau chia (÷)
    ${get_textt}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_textt}    123÷

BP-340
    [Documentation]    kiem tra phep tinh nhan (x) voi 2 loai phep tinh
    [Tags]    BP-340
    Log To Console    kiem tra phep tinh chia (x) voi 2 loai phep tinh
    Xoa man hinh
    An nut 123
    An nut dau nhan (x)
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    123×−

BP-342
    [Documentation]    kiem tra phep tinh cong (+) voi phep tinh tru
    [Tags]    BP-342    BP-343    BP-344
    Xoa man hinh
    An nut 123
    An nut dau cong (+)
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    123−

BP-343
    [Documentation]    kiem tra phep tinh cong (+) voi phep tinh nhan
    [Tags]    BP-343
    An nut dau cong (+)
    An nut dau nhan (x)
    ${get_text1}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text1}    123×

BP-344
    [Documentation]    kiem tra phep tinh cong (+) voi phep tinh chia
    [Tags]    BP-344
    An nut dau cong (+)
    An nut dau chia (÷)
    ${get_text2}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text2}    123÷

BP-345
    [Documentation]    kiem tra phep tinh tru (-) voi phep tinh chia
    [Tags]    BP-345    BP-347    BP-350
    Xoa man hinh
    An nut 123
    An nut dau tru (-)
    An nut dau chia (÷)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    123÷

BP-347
    [Documentation]    kiem tra phep tinh tru (-) voi phep tinh nhan
    [Tags]    BP-347
    An nut dau tru (-)
    An nut dau nhan (x)
    ${get_text1}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text1}    123×

BP-350
    [Documentation]    kiem tra phep tinh tru (-) voi pheo tinh cong
    [Tags]    BP-350
    An nut dau cong (+)
    ${get_text2}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text2}    123+

BP-357
    [Documentation]    kiem tra nhap dau phay (,) vao giua cac so
    [Tags]    BP-357
    Log To Console    kiem tra nhap dau phay (,) vao giua cac so
    Xoa man hinh
    An nut 123
    An nut phay thap phan
    An nut 456
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *123**456*

BP-363
    [Documentation]    Kiem tra them so vao sau ket qua phep tinh da thuc hien
    [Tags]    BP-363
    Log To Console    Kiem tra them so vao sau ket qua phep tinh da thuc hien
    Xoa man hinh
    An nut 12345
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    Thuc hien nhap so
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    45

BP-364
    [Documentation]    Kiem tra them nut phep tinh cong vao sau ket qua phep tinh da thuc hien
    [Tags]    BP-364
    Xoa man hinh
    An nut 12345
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    An nut dau cong (+)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    12**222+

BP-366
    [Documentation]    Kiem tra them nut phep tru vao sau ket qua phep tinh da thuc hien
    [Tags]    BP-366
    Xoa man hinh
    An nut 12345
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    An nut dau tru (-)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    12**222−

BP-369
    [Documentation]    Kiem tra them nut phep nhan vao sau ket qua phep tinh da thuc hien
    [Tags]    BP-369
    Xoa man hinh
    An nut 12345
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    An nut dau nhan (x)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    12**222×

BP-370
    [Documentation]    Kiem tra them nut phep chia vao sau ket qua phep tinh da thuc hien
    [Tags]    BP-370
    Xoa man hinh
    An nut 12345
    An nut dau tru (-)
    An nut 123
    An nut dau (=)
    An nut dau chia (÷)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    12**222÷

BP-354
    [Documentation]    Kiem tra hien thi so 0 dang truoc dau phay
    [Tags]    BP-354
    Xoa man hinh
    AppiumLibrary.Click Element    ${mt_nut_phay}
    ${get_text}=    Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *0*

BP-213
    [Documentation]    Check chuc nang luu phep tinh gan nhat
    [Tags]    BP-213    BP-216    BP-218
    Log To Console    Check chuc nang hien thi phep tinh gan nhat
    Xoa man hinh
    Thuc hien nhap phep tinh

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================