*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/chim_lac/cl_common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/xem_phim/thu_muc.robot

Suite Setup       Mo Xem phim
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
# Da dang nhap mail
BP-3594
    [Documentation]    Check khong goi duoc sdt, khong mo giao dien cuoc goi khi cham vao sdt
    [Tags]             BP-3594    BP-3597
    Cho phep cap quyen cho ung dung
    Cham icon ba cham
    Nhan chon Thong tin
    Element Attribute Should Match    ${xp_tm_text_so_dien_thoai}    clickable    false
    Log To Console    BP-3597 : Check hien thi thong tin phan mem khi cham Thong tin
    Page Should Contain Element    ${xp_tm_text_hop_thu}
    Page Should Contain Element    ${xp_tm_text_trang_chu}
    Page Should Contain Element    ${xp_tm_text_dien_thoai}

BP-3598
    [Documentation]    Check mo giao dien soan thu cua Gmail khi cham link bkav
    [Tags]             BP-3598
    Nhan chon hop thu bkav
    Page Should Contain Element    ${xp_tm_text_bkav@bkav.com.vn}
    Page Should Contain Element    ${xp_tm_gui_thu_text_soan_thu}
    Page Should Contain Element    ${xp_tm_gui_thu_nut_gui}
    Sleep    2
    Vuot back ve giao dien truoc
    Vuot back ve giao dien truoc

BP-3601
    [Documentation]    Check dong giao dien Thong tin Xem phim khi vuot back
    [Tags]             BP-3601
    Vuot back ve giao dien truoc
    Page Should Contain Element    ${xp_tm_nut_tim_kiem}
    Page Should Contain Element    ${xp_tm_xem_video_gan_day}
    Sleep    2

BP-3602
    [Documentation]    Check hien thi menu sap xep theo khi cham Sap xep theo...
    [Tags]             BP-3602    BP-3604
    Cham icon ba cham
    Nhan chon Sap xep theo
    Page Should Contain Element    ${xp_tm_nut_sap_xep_theo}
    Page Should Contain Element    ${xp_tm_sap_xep_theo_ten}
    Page Should Contain Element    ${xp_tm_sap_xep_theo_do_dai}
    Page Should Contain Element    ${xp_tm_sap_xep_theo_ngay_cu_nhat}
    Log To Console    BP-3604 : Check sap xep theo ten tu a-z khi cham Ten
    Nhan chon Sap xep theo - ten
    Element Attribute Should Match    ${xp_tm_ten_vd_stt_1}    text    SampleVideosMP4
    Element Attribute Should Match    ${xp_tm_ten_vd_stt_2}    text    SampleVideosOtherFormats

BP-3605
    [Documentation]    Check thoi luong tang dan khi cham Do dai
    [Tags]             BP-3605
    Cham icon ba cham
    Nhan chon Sap xep theo
    Nhan chon Sap xep theo - do dai
    Element Attribute Should Match    ${xp_tm_thoi_luong_vd_1}    text    1:42
    Element Attribute Should Match    ${xp_tm_thoi_luong_vd_2}    text    1:40:28
    Sleep    1

BP-3607
    [Documentation]    Check thoi luong giam dan khi cham Do dai (giam dan)
    [Tags]             BP-3607
    Cham icon ba cham
    Nhan chon Sap xep theo
    Nhan chon Sap xep theo - do dai (giam dan)
    Element Attribute Should Match    ${xp_tm_thoi_luong_vd_1}    text    1:40:28
    Element Attribute Should Match    ${xp_tm_thoi_luong_vd_2}    text    1:42

BP-3609
    [Documentation]    Check hien thi danh sach thu muc theo thu tu ngay quay, ngay tai phim tang dan
    [Tags]             BP-3609
    Cham icon ba cham
    Nhan chon Sap xep theo
    Nhan chon Sap xep theo - ngay moi nhat
    Element Attribute Should Match    ${xp_tm_ten_vd_stt_1}    text    SampleVideosMP4
    Element Attribute Should Match    ${xp_tm_ten_vd_stt_2}    text    SampleVideosOtherFormats

BP-3610
    [Documentation]    Check hien thi danh sach thu muc theo thu tu ngay quay, ngay tai phim giam dan
    [Tags]             BP-3610
    Cham icon ba cham
    Nhan chon Sap xep theo
    Nhan chon Sap xep theo - ngay cu nhat
    Element Attribute Should Match    ${xp_tm_ten_vd_stt_1}    text    SampleVideosOtherFormats
    Element Attribute Should Match    ${xp_tm_ten_vd_stt_2}    text    SampleVideosMP4

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================