*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/may_tinh/mt_common.robot

Suite Setup    Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-28
    [Documentation]    Check mo May tinh tu Launcher
    [Tags]     High    BP-28
    Mo May tinh tu Launcher

BP-51
    [Documentation]    Check noi dung text Lich su
    [Tags]     High    BP-51
    AppiumLibrary.Page Should Contain Element    ${mt_nut_lich_su}

BP-75
    [Documentation]    Check noi dung DEL
    [Tags]     High    BP-75
    AppiumLibrary.Page Should Contain Element    ${mt_nut_xoa}

BP-83
    [Documentation]    Check noi dung nut dau phay
    [Tags]     High    BP-83
    AppiumLibrary.Page Should Contain Element    ${mt_nut_phay}
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-48
    [Documentation]    Check man hinh hien thi trong khi mo app lan dau
    [Tags]     High    BP-48
    Mo ung dung    ${mt_alias}    ${mt_package}    ${mt_activity}    2
    Element Should Contain Text    ${mt_hien_thi}              ${EMPTY}
    Element Should Contain Text    ${mt_hien_thi_phep_tinh}    ${EMPTY}
    Element Should Contain Text    ${mt_hien_thi_ket_qua}      ${EMPTY}
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-44
    [Documentation]    Kiem tra giao dien chinh cua ung dung May tinh
    [Tags]    BP-44    BP-46    BP-47    BP-49    BP-53   BP-54    BP-55    BP-56    BP-58
    ...    BP-60    BP-61    BP-64    BP-65    BP-66    BP-67    BP-68    BP-73    BP-76    BP-78
    ...    BP-79    BP-82    BP-86    BP-90    BP-91    BP-92    BP-93    BP-94    BP-95    BP-95
    ...    BP-96    BP-97    BP-98    BP-99    BP-100    BP-108
    Mo ung dung    ${mt_alias}    ${mt_package}    ${mt_activity}    2
    Chup anh man hinh va so sanh voi anh mau    ${mt_alias}    kc_d_ttdh_d_d_main    2
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-205
    [Documentation]    Check chuc nang hien thi noi dung tu 1-7
    [Tags]    BP-205    BP-207
    Mo ung dung    ${mt_alias}    ${mt_package}    ${mt_activity}    2
    An nut 1234567
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *1**234**567*

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================