*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
ThinhLQ         | 2021/07/23 | update bo comment va thay doi video test, thay doi anh giao dien so sanh
----------------|------------|--------------------------------------------------------------------

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/xem_phim/tuy_chon.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup       Mo Xem phim
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5247
    [Documentation]    Check hien thi thoi gian da chon va chuc nang Huy bo thoi gian cho tren label Thoi gian cho khi cham DONG Y
    [Tags]    BP-5247
    Cho phep cap quyen cho ung dung
    Click vao bo danh sach video1
    Click vao video 5
    Cham vao man hinh video
    Click vao nut play
    Vuot thanh thoi gian ve ban dau
    Click vao tuy chon
    Click vao tab thoi gian cho
    Click vao nut dong y
    Cham vao man hinh video
    Click vao tuy chon
    Kiem tra trang thai hien thi thoi gian cho

BP-5248
    [Documentation]    Check chuc nang huy bo thoi gian cho khi cham vao Huy bo thoi gian cho
    [Tags]    BP-5248
    Click vao huy thoi gian cho
    Kiem tra trang thai mac dinh thoi gian cho
    Click vao tab thoi gian cho

BP-5249
    [Documentation]    Check chuc nang huy tao thoi gian cho bang thao tac vuot Back
    [Tags]    BP-5249
    Vuot back ve giao dien truoc
    Kiem tra trang thai khong hien tab thoi gian cho

BP-5272
    [Documentation]    Check kiem tra chuc nang tua vi tri thoi gian
    [Tags]    BP-5272
    Cham vao man hinh video
    Click vao tuy chon
    Click vao tua den vi tri
    Cham vao man hinh video
    Kiem tra trang thai khong hien nut dong y
    Cham vao man hinh video
    Click vao tuy chon

BP-5264
    [Documentation]    Check tua video dang dung den vi tri 0s khi bo trong thoi gian khong nhap
    [Tags]    BP-5264
    Click vao tua den vi tri
    Click vao nut dong y
    Cham vao man hinh video
    Kiem tra hien thi so thoi gian vua tua ve 0s
    Cham vao man hinh video

BP-5265
    [Documentation]    Check tua video (dang dung) khi nhap thoi gian tua den vi tri lon hon tong thoi luong cua video
    [Tags]    BP-5265
    Click vao tuy chon
    Click vao tua den vi tri
    Nhap so thoi gian vao phut tua den vi tri 15 phut
    Cham vao man hinh video
    Kiem tra hien thi so thoi gian vua tua den vi tri
    Cham vao man hinh video

BP-5266
    [Documentation]    Check tua video (dang dung) khi nhap thoi gian 00 m 00s cham DONG Y
    [Tags]    BP-5266
    Click vao tuy chon
    Click vao tua den vi tri
    Nhap so thoi gian tua den vi tri 0 giay
    Cham vao man hinh video
    Kiem tra hien thi so thoi gian vua tua ve 0s

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================