*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library    Collections
Library     AppiumLibrary
Library     String    

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/thoi_tiet/tt_common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/ghi_am/ga_common.robot


Suite Setup       Mo Thoi tiet
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
TT_220108
    [Documentation]    Check hien thi day du thong tin do am, suc gio, ap suat, tam nhin tren man hinh chi tiet thoi tiet
    [Tags]    TT_220108    TT_030701    
    Cho phep cap quyen cho ung dung
    Sleep    2    
    Log To Console    TT_030701    Check xem thong tin Thoi tiet cua 1 Thanh pho    
    AppiumLibrary.Click Element    ${tt_tk_ha_noi}
    Sleep    2    
    Vuot len de mo man hinh thong tin chi tiet thoi tiet
    Sleep    5    
    Page Should Contain Element    ${tt_ttct_do_am}
    ${do_am}=    Get Text    ${tt_ttct_do_am}
    ${kiem_tra_do_am}=    Get Length    ${do_am}
    Run Keyword If    ${kiem_tra_do_am}<${min_length_do_am}    Fail    Khong hien thi du lieu mong muon
    Page Should Contain Element    ${tt_ttct_suc_gio}
    ${suc_gio}=    Get Text    ${tt_ttct_suc_gio}
    ${kiem_tra_suc_gio}=    Get Length    ${suc_gio}
    Run Keyword If    ${kiem_tra_suc_gio}<${min_length_suc_gio}    Fail    Khong hien thi du lieu mong muon
    Page Should Contain Element    ${tt_ttct_ap_suat}
    ${ap_suat}=    Get Text    ${tt_ttct_ap_suat}
    ${kiem_tra_ap_suat}=    Get Length    ${ap_suat}
    Run Keyword If    ${kiem_tra_ap_suat}<${min_length_ap_suat}    Fail    Khong hien thi du lieu mong muon
    Page Should Contain Element    ${tt_ttct_tam_nhin}
    ${tam_nhin}=    Get Text    ${tt_ttct_tam_nhin}
    ${kiem_tra_tam_nhin}=    Get Length    ${tam_nhin}
    Run Keyword If    ${kiem_tra_tam_nhin}<${min_length_tam_nhin}    Fail    Khong hien thi du lieu mong muon
    Dong toan bo ung dung trong da nhiem
    
TT_150101
    [Documentation]    Check nhiet do hien thi day du tren man hinh chi tiet thoi tiet cua 1 thanh pho khi chia doi man hinh 
    [Tags]     TT_150101    
    Mo Thoi tiet tu Launcher
    Sleep    2    
    Vuot sang man hinh ben trai
    AppiumLibrary.Click Element    ${tt_tk_ha_noi}
    Sleep    2    
    Mo da nhiem
    AppiumLibrary.Click Element    ${dn_icon_ung_dung}
    Sleep    2
    AppiumLibrary.Click Element    ${dn_chia_doi_man_hinh}
    ${hien_thi_app_cai_dat}    Run Keyword And Return Status    AppiumLibrary.Page Should Contain Element    ${app_cai_dat}        
    Run Keyword If    ${hien_thi_app_cai_dat}==True    AppiumLibrary.Click Element    ${app_cai_dat}    
    ...    ELSE    Vuot sang trai launcher va click app cai dat
    Swipe    526    685    526    175    
    Sleep    2    
    AppiumLibrary.Click Element    ${tt_ttct_cum_nhiet_do_tt_ten}    
    AppiumLibrary.Click Element    ${tt_ttct_cum_nhiet_do_tt_ten}  
    Sleep    2    
    Page Should Contain Element    ${tt_ttct_cum_nhiet_do_tt_ten}            
    Swipe By Percent    50    50    50    5    duration=3000    
    Sleep    2
    Dong toan bo ung dung trong da nhiem  
        
TT_070104
    [Documentation]    Check kiem tra du lieu do am, suc gio, ap suat, tam nhin tren nguon Openweather
    [Tags]    TT_070104    TT_070105
    Mo OpenWeather tu Launcher
    Sleep    5    
    Page Should Contain Element    ${tt_open_weather_suc_gio}    
    Page Should Contain Element    ${tt_open_weather_do_am}
    Page Should Contain Element    ${tt_open_weather_ap_suat}
    Page Should Contain Element    ${tt_open_weather_tam_nhin}
    Sleep    2    
    Log To Console    TT_070105    Check hien thi thong tin do am, suc gio, ap suat, tam nhin tren man hinh chi tiet thoi tiet    
    # ${ep_suc_gio}=      Get Text    ${tt_open_weather_suc_gio}
    ${ep_do_am}=        Get Text    ${tt_open_weather_do_am}
    ${ep_do_am_1}=      Remove String    ${ep_do_am}    Humidity:${SPACE}         
    ${ep_ap_suat}=      Get Text    ${tt_open_weather_ap_suat}
    ${ep_ap_suat_1}=    Remove String    ${ep_ap_suat}    Pressure:${SPACE}    
    ${ep_tam_nhin}=     Get Text    ${tt_open_weather_tam_nhin}
    ${ep_tam_nhin_1}=    Remove String    ${ep_tam_nhin}    Visibility:${SPACE}    .   
    Vuot quay ve man hinh Home 
    Mo Thoi tiet tu Launcher
    Vuot len de mo man hinh thong tin chi tiet thoi tiet
    Sleep    2    
    # ${ac_suc_gio}=      Get Text    ${tt_ttct_suc_gio}
    ${ac_do_am}=        Get Text    ${tt_ttct_do_am}
    ${ac_ap_suat}=      Get Text    ${tt_ttct_ap_suat}
    ${ac_ap_suat1}=    Remove String    ${ac_ap_suat}    ,0${SPACE}
    ${ac_tam_nhin}=     Get Text    ${tt_ttct_tam_nhin}
    ${ac_tam_nhin_1}=    Remove String    ${ac_tam_nhin}    ,    ${SPACE}
    # Should Be Equal    ${ep_suc_gio}    ${ac_suc_gio}
    Should Be Equal    ${ep_do_am_1}    ${ac_do_am}
    Should Be Equal    ${ep_ap_suat_1}    ${ac_ap_suat1}
    Should Be Equal    ${ep_tam_nhin_1}    ${ac_tam_nhin_1}
    Dong toan bo ung dung trong da nhiem
    
BP-1702
    [Documentation]    Check dong nhat thong tin do am tren man hinh thoi tiet cua 1 thanh pho trung khop thong tin do am tren man hinh thoi tiet chi tiet cua 1 thanh pho
    [Tags]    BP-1702
    Mo Thoi tiet tu Launcher
    Sleep    2    
    Vuot sang man hinh ben trai
    Click Element    ${tt_tk_ha_noi}
    Sleep    2
    ${do_am_mh_tp}=    Get Text    ${tt_do_am_trong_ngay}
    Vuot len de mo man hinh thong tin chi tiet thoi tiet
    ${do_am_ct_tp}=    Get Text    ${tt_ttct_do_am}
    Should Be Equal    ${do_am_ct_tp}    ${do_am_mh_tp}  
    Vuot xuong de dong man hinh thong tin chi tiet thoi tiet
    Vuot sang man hinh ben trai
    Vuot quay ve man hinh Home    
    
TT_041001
    [Documentation]    Check khong lap ten thanh pho khi thuc hien tim kiem thanh pho tren thanh tim kiem
    [Tags]    TT_041001
    Mo Thoi tiet tu Launcher
    Sleep    2    
    Bam nut them thanh pho
    Click Element    ${tt_tk_nhap_ten_thanh_pho}
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Bình        
    Sleep    5    
    Xpath Should Match X Times    ${tt_tk_binh_dinh}     1    
    Xpath Should Match X Times    ${tt_tk_binh_duong}    1    
    Xpath Should Match X Times    ${tt_tk_binh_thuan}    1    
    Xpath Should Match X Times    ${tt_tk_thai_binh}     1    
    Xpath Should Match X Times    ${tt_tk_ninh_binh}     1    
    Xpath Should Match X Times    ${tt_tk_hoa_binh}      1    
    AppiumLibrary.Click Element    ${tt_btn_clear_text_tim_kiem}
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Bình Phước
    Sleep    2    
    Xpath Should Match X Times    ${tt_tk_binh_phuoc}    1    
    Sleep    2    
    AppiumLibrary.Click Element    ${tt_btn_clear_text_tim_kiem}
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Quảng Bình
    Sleep    2    
    Xpath Should Match X Times    ${tt_tk_quang_binh}    1
    AppiumLibrary.Click Element    ${tt_btn_clear_text_tim_kiem}
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Hòa Bình
    Sleep    2    
    Xpath Should Match X Times    ${tt_tk_hoa_binh}    1      
    
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================