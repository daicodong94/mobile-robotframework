*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/bo_suu_tap/album.robot
Resource    ../page/bo_suu_tap/thumbnail.robot
Resource    ../page/bo_suu_tap/thung_rac.robot

Suite Setup       Mo Launcher
# Suite Teardown    Run Keywords    Quit Application    Close All Applications

*** Test Cases ***
BP-1326
    [Documentation]    Mo bo suu tap tu Launcher
    [Tags]     High    BP-1326
    Mo Bo suu tap tu Launcher
    Close All Applications
    
BP-1327
    [Documentation]    Cham vao tab Dong thoi gian, Album, Video, Thung rac
    [Tags]     High    BP-1327
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Log To Console    Mo giao dien Album
    Mo giao dien Album
    Log To Console    Mo giao dien Dong thoi gian
    Mo giao dien Dong thoi gian
    Log To Console    Mo giao dien Quay phim
    Mo giao dien Quay phim
    Log To Console    Mo giao dien Thung rac
    Mo giao dien thung rac rong
    Close All Applications

BP-1328
    [Documentation]    Vuot chuyen qua cac tab Dong thoi gian, Album, Video, Thung rac
    [Tags]     High    BP-1328    
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Log To Console     Vuot sang giao dien Album    
    Vuot man tu phai qua trai
    Page Should Contain Element    ${bst_album_nut_them_moi}
    Sleep    2    
    Log To Console     Vuot sang giao Quay phim
    Vuot man tu phai qua trai
    Page Should Not Contain Element    ${bst_album_nut_them_moi}
    Sleep    2    
    Log To Console    Vuot sang giao dien Thung rac 
    Vuot man tu phai qua trai
    Page Should Contain Element    ${bst_album_thung_rac_mess}
    Close All Applications

BP-1338
    [Documentation]    Check chon xoa anh khi cham vao icon xoa tren thanh edit tabbar
    [Tags]             High    BP-1338
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Chon giu 1 anh trong Dong thoi gian
    Chon nhieu anh trong Dong thoi gian
    Chon xoa anh tren Dong thoi gian
    Sleep    2
    Mo giao dien Thung rac khi co anh
    Sleep    2
    Log To Console     Tich chon/bo chon anh trong giao dien Thung rac
    Chon 2 anh trong Thung rac
    Chon xoa anh trong Thung rac
    Sleep    2
    Log To Console    Check hien popup Xoa khoi thung rac
    Page Should Contain Element    ${bst_TR_title_xoa_khoi_thung_rac}
    Page Should Contain Element    ${bst_TR_nut_dong_y}
    Page Should Contain Element    ${bst_TR_nut_huy}
    Sleep    2
    Close All Applications

BP-1329
    [Documentation]    Check hien thi giao dien khi thung rac rong
    [Tags]             BP-1329
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Mo giao dien thung rac rong
    Close All Applications

BP-1331
    [Documentation]    Check kich ban chon/bo chon anh o giao dien thumbnail
    [Tags]             BP-1331    BP-1332
    Mo Bo Suu Tap
    Cho phep cap quyen cho ung dung
    Log To Console     Xoa anh tren dong thoi gian
    Sleep    2
    Chon giu 1 anh trong Dong thoi gian
    Chon nhieu anh trong Dong thoi gian
    Sleep    2
    Chon xoa anh tren Dong thoi gian
    Sleep    2
    Mo giao dien Thung rac khi co anh
    Sleep    2
    Log To Console     Tich chon/bo chon anh trong giao dien Thung rac
    Chon 2 anh trong Thung rac
    Log To Console    Check cac chuc nang hien thi tren edit tabbar Khoi phuc, Xoa
    Page Should Contain Element    ${bst_TR_nut_khoi_phuc}
    Page Should Contain Element    ${bst_TR_nut_xoa}
    Bo chon nhieu anh trong Thung rac

BP-1340
    [Documentation]    Check dong popup khi bam Huy xoa anh 
    [Tags]             BP-1340
    Chon 2 anh trong Thung rac
    Chon xoa anh trong Thung rac
    Sleep    2
    Log To Console    Check hien popup Xoa khoi thung rac
    Page Should Contain Element    ${bst_TR_title_xoa_khoi_thung_rac}
    Page Should Contain Element    ${bst_TR_nut_dong_y}
    Page Should Contain Element    ${bst_TR_nut_huy}
    Sleep    2
    Log To Console    Check dong popup khi bam Huy
    Chon Huy tren popup Xoa khoi thung rac
    Page Should Not Contain Element    ${bst_TR_title_xoa_khoi_thung_rac}

BP-1339
    [Documentation]    Check xoa toan bo anh trong Thung rac khi bam icon xoa tat ca
    [Tags]             BP-1339
    Chon 2 anh trong Thung rac
    Bam icon chon tat ca tren thanh toolbar
    Chon xoa anh trong Thung rac
    Sleep    2
    Page Should Contain Element    ${bst_TR_title_xoa_khoi_thung_rac}
    Page Should Contain Element    ${bst_TR_nut_dong_y}
    Page Should Contain Element    ${bst_TR_nut_huy}
    Sleep    2
    Chon Huy tren popup Xoa khoi thung rac
    Page Should Not Contain Element    ${bst_TR_title_xoa_khoi_thung_rac}

BP-1344
    [Documentation]    Check lai giao dien thung rac sau khi da Khoi phuc/Xoa toan bo anh
    [Tags]             BP-1344
    Log To Console    Check khi chon khoi phuc toan bo anh
    Chon khoi phuc anh trong Thung rac
    Sleep    2
    Mo giao dien Dong thoi gian
    Chon giu 1 anh trong Dong thoi gian
    Chon nhieu anh trong Dong thoi gian
    Sleep    2
    Chon xoa anh tren Dong thoi gian
    Sleep    2
    Log To Console    Check khi chon xoa toan bo anh
    Mo giao dien Thung rac khi co anh
    Sleep    2
    Chon 2 anh trong Thung rac
    Bam icon chon tat ca tren thanh toolbar
    Chon xoa anh trong Thung rac
    Sleep    2
    Chon Dong y tren popup Xoa khoi thung rac

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================