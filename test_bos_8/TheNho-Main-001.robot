*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library    AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/the_nho/tn_common.robot
Resource    ../page/notification/notification.robot
Resource    ../page/quan_ly_file/qlf_common.robot
Resource    ../page/ghi_am/ga_common.robot

Suite Setup        Mo Quan ly file
Suite Teardown     Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5733
    [Documentation]     Check di chuyen noi dung tu dien thoai vao the nho khi cham Di chuyen noi dung
    [Tags]    High    BP-5733
    Cho phep cap quyen cho ung dung
    Click Element At Coordinates    168    664
    Sleep    2
    Click Element At Coordinates    975    479
    Sleep    2
    Click Element    ${qlf_nut_nhieu_hon}
    Sleep    2
    Click Element    ${qlf_nut_copy}
    Sleep    2
    Click Element    ${qlf_the_nho_SD}
    Sleep    2
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    Click Element At Coordinates    498    2076
    Sleep    2
    Click Element    ${nut_cho_phep_hoa}
    Sleep    2
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    Click Element    ${nut_dong_y_hoa}
    Sleep    10
    Page Should Contain Text    Android

TN_040101
    [Documentation]    Check mo thong bao Di chuyen noi dung the SD khi cham Dinh dang the SD
    [Tags]    TN_040101    TN_040103    TN_040105    TN_040201    TN_020102    TN_020103
    Vuot quay ve man hinh Home
    Mo cai dat tu launcher va navigate den luu tru
    Click the nho SD va mo tuy chon
    Sleep    2
    Click Element    ${tn_bn_dt_dinh_dang_bo_nho_di_dong}
    Sleep    2
    Click Element    ${tn_nut_dinh_dang}
    Sleep    10
    AppiumLibrary.Click Element    ${tn_nut_xong}
    Vuot mo bang thong bao
    Log To Console    TN_020102    Check hien thi thong bao trong Notification khi lap the nho
    Page Should Contain Element    ${tn_sd}
    Click Element    ${tn_sd}
    Sleep    2
    Click Element    ${tn_bn_dien_thoai}
    Sleep    2
    Click Element    ${tn_bn_dt_dinh_dang_the_sd}
    Sleep    25
    # AppiumLibrary.Wait Until Page Contains Element    ${tn_di_chuyen_noi_dung}    timeout=25
    AppiumLibrary.Click Element    ${tn_di_chuyen_noi_dung}
    Sleep    30
    Log To Console    TN_040103    Check mo thong bao "The SD da san sang su dung" khi cham Di chuyen noi dung sau
    # AppiumLibrary.Wait Until Page Contains Element    ${tn_nut_xong}    timeout=30
    Log To Console    TN_040105    Check cham Xong tai thong bao "The SD da san sang su dung"
    AppiumLibrary.Click Element    ${tn_nut_xong}
    Sleep    2
    Page Should Not Contain Element    ${tn_di_chuyen_noi_dung}
    Log To Console    TN_040201    Check khong hien thi lai thong bao Thiet lap the nho khi da thiet lap xong
    Vuot mo bang thong bao
    Page Should Not Contain Element    ${tn_sd}
    Xoa toan bo thong bao
    Log To Console    TN_020103    Check khong hien thi lai thong bao thiet lap the nho khi da xoa thong bao

TN_040102
    [Documentation]    Check vuot back khi dang thuc hien Dinh dang the SD
    [Tags]      TN_040102
    Vuot quay ve man hinh Home
    Mo cai dat tu launcher va navigate den luu tru
    Click the nho SD va mo tuy chon
    Sleep    2
    Click Element    ${tn_bn_dt_dinh_dang_bo_nho_di_dong}
    Sleep    2
    Click Element    ${tn_nut_dinh_dang}
    Sleep    10
    AppiumLibrary.Click Element    ${tn_nut_xong}
    Sleep    2
    Vuot mo bang thong bao
    Click Element    ${tn_sd}
    Sleep    2
    Click Element    ${tn_bn_dien_thoai}
    Sleep    2
    Click Element    ${tn_bn_dt_dinh_dang_the_sd}
    Vuot back ve giao dien truoc
    Page Should Contain Element    ${tn_bn_dien_thoai}

TN_040301
    [Documentation]    Check hien thi menu chuc nang khi cham vao icon ...
    [Tags]    TN_040301    TN_040401    TN_040402    TN_040406    TN_040403    TN_040404
    Vuot quay ve man hinh Home
    Mo cai dat tu launcher va navigate den luu tru
    Click the nho SD va mo tuy chon
    Sleep    2
    Page Should Contain Element    ${tn_bn_dt_doi_ten}
    Page Should Contain Element    ${tn_bn_dt_ngat_ket_noi}
    Page Should Contain Element    ${tn_bn_dt_dinh_dang_bo_nho_di_dong}
    Click Element    ${tn_bn_dt_doi_ten}
    Sleep    2
    Log To Console    TN_040401    Check mo popup Doi ten bo nho khi cham vao Doi ten
    Page Should Contain Element    ${tn_bn_dt_textbox_doi_ten}
    Page Should Contain Element    ${tn_bn_dt_title}
    Log To Console    TN_040402    Check nhap ten co khoang trang roi cham Luu
    Log To Console    TN_040406    Check Doi ten thanh cong khi nhap ten nho hon 50 ky tu roi an Luu
    Clear Text    ${tn_bn_dt_textbox_doi_ten}
    Input Text    ${tn_bn_dt_textbox_doi_ten}    Dai${SPACE}tv
    ${text_ten}=    AppiumLibrary.Get Text    ${tn_bn_dt_textbox_doi_ten}
    Sleep    2
    Click Element    ${nut_luu}
    Sleep    2
    Page Should Contain Text    ${text_ten}
    Log To Console    TN_040403    Check nhap ten co chua chu so roi cham Luu
    Click Element    ${cd_nut_tuy_chon_khac}
    Sleep    2
    Click Element    ${tn_bn_dt_doi_ten}
    Sleep    2
    Clear Text    ${tn_bn_dt_textbox_doi_ten}
    Input Text    ${tn_bn_dt_textbox_doi_ten}    123456
    ${ten_so}=    AppiumLibrary.Get Text    ${tn_bn_dt_textbox_doi_ten}
    Sleep    2
    Click Element    ${nut_luu}
    Sleep    2
    Page Should Contain Text    ${ten_so}
    Log To Console    TN_040404    Check nhap ten co ky tu dac biet roi cham Luu
    Click Element    ${cd_nut_tuy_chon_khac}
    Sleep    2
    Click Element    ${tn_bn_dt_doi_ten}
    Sleep    2
    Clear Text    ${tn_bn_dt_textbox_doi_ten}
    Input Text    ${tn_bn_dt_textbox_doi_ten}    !@#$%^
    ${ten_ki_tu_db}=    AppiumLibrary.Get Text    ${tn_bn_dt_textbox_doi_ten}
    Sleep    2
    Click Element    ${nut_luu}
    Sleep    2
    Page Should Contain Text    ${ten_ki_tu_db}
    Log To Console    Doi ten the nho ve mac dinh
    Click Element    ${cd_nut_tuy_chon_khac}
    Sleep    2
    Click Element    ${tn_bn_dt_doi_ten}
    Sleep    2
    Clear Text    ${tn_bn_dt_textbox_doi_ten}
    Input Text    ${tn_bn_dt_textbox_doi_ten}    Thẻ SD SanDisk
    Sleep    2
    Click Element    ${nut_luu}
    Vuot quay ve man hinh Home

TN_040501
    [Documentation]    Check hien thi giao dien thong bao Ngat ket noi khi cham Ngat ket noi
    [Tags]    TN_040501    TN_040502    TN_040602
    Mo cai dat tu launcher va navigate den luu tru
    Click the nho SD va mo tuy chon
    Click Element    ${tn_bn_dt_ngat_ket_noi}
    Sleep    2
    Page Should Contain Element    ${tn_nut_ngat_ket_noi}
    Log To Console    TN_040502    Check ngat ket noi the nho khi cham button Ngat ket noi
    AppiumLibrary.Click Element    ${tn_nut_ngat_ket_noi}
    Sleep    1
    Page Should Contain Text    ${tn_text_thao_tn_ra}
    Log To Console    TN_040602    Check ket noi lai the nho khi cham Gan tai popup
    Click Element    ${tn_sd}
    Sleep    2
    Click Element    ${tn_nut_gan}
    Sleep    1
    Page Should Contain Text    ${tn_text_gan_tn_vao}
    Vuot quay ve man hinh Home

TN_040701
    [Documentation]    Check hien thi thong bao dinh dang khi cham Dinh dang
    [Tags]    TN_040701    TN_040702    TN_090104    TN_090108    TN_090109
    Mo cai dat tu launcher va navigate den luu tru
    Click the nho SD va mo tuy chon
    Sleep    2
    Click Element    ${tn_bn_dt_dinh_dang_bo_nho_di_dong}
    Page Should Contain Element    ${tn_nut_dinh_dang}
    Sleep    2
    Log To Console    TN_040702    Check dinh dang thanh cong khi cham button Dinh dang
    Log To Console    TN_090104    Check thiet lap the nho lam Bo nho di dong khi cham Bo nho di dong
    Click Element    ${tn_nut_dinh_dang}
    Sleep    10
    Page Should Contain Element    ${tn_nut_xong}
    Click Element    ${tn_nut_xong}
    Sleep    2
    Log To Console    TN_090108    Check ngat ket noi the nho khi cham vao button Ngat canh folder The SD SanDisk
    ${hien_thi_app_cai_dat}    Run Keyword And Return Status    AppiumLibrary.Page Should Contain Element    ${app_cai_dat}
    Run Keyword If    ${hien_thi_app_cai_dat}==True    AppiumLibrary.Click Element    ${app_cai_dat}
    ...    ELSE    Vuot sang trai launcher va click app cai dat
    Sleep    2
    Swipe By Percent    50    25    50    10
    Sleep    2
    AppiumLibrary.Click Element    ${nut_luu_tru}
    Sleep    2
    Click Element    ${tn_nut_ngat_ket_noi_bn}
    Sleep    2
    Log To Console    TN_090109    Check hien thi popup gan the nho khi cham vao Thu muc the nho trong Cai dat Luu tru
    Click Element    ${tn_sd}
    Sleep    2
    Click Element    ${tn_nut_gan}
    Sleep    2
    Page Should Contain Element    ${tn_nut_ngat_ket_noi_bn}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================