 *** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/xem_phim/thu_muc.robot
Resource    ../page/xem_phim/video.robot

Suite Setup       Mo Xem phim
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-3656
    [Documentation]    Check hien text Cap nhat lai khi cham giu icon Back (icon mui ten quay)
    [Tags]             BP-3656
    Cho phep cap quyen cho ung dung
    Chon thu muc video mp4
    Cham giu icon Back - icon mui ten quay
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_video_text_icon_mui_ten_back    3

BP-3659
    [Documentation]    Check hien thi text Sap xep theo... khi cham giu icon sap xep trong ds video
    [Tags]             BP-3659    BP-3660
    Chon thu muc video mp4
    Nhan icon Sap xep theo trong danh sach video
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_video_popup_icon_sap_xep    3
    Page Should Contain Element    ${xp_tm_sap_xep_theo_ten}
    Page Should Contain Element    ${xp_tm_sap_xep_theo_do_dai}
    Page Should Contain Element    ${xp_tm_sap_xep_theo_ngay_cu_nhat}
    Log To Console    BP-3660 : Check tuong tu chuc nang Sap xep thu muc
    Nhan chon Sap xep theo - ten
    Element Attribute Should Match    ${xp_tm_ten_vd_stt_1}    text    1_1920x1080*
    Element Attribute Should Match    ${xp_tm_ten_vd_stt_2}    text    2_1920x1080*
    Element Attribute Should Match    ${xp_tm_ten_vd_stt_3}    text    3_360x240*
    Sleep    2
    Nhan icon Sap xep theo trong danh sach video
    Nhan chon Sap xep theo - do dai
    Element Attribute Should Match    ${xp_tm_thoi_luong_vd_1}    text    5s
    Element Attribute Should Match    ${xp_tm_thoi_luong_vd_2}    text    10s
    Element Attribute Should Match    ${xp_tm_thoi_luong_vd_3}    text    13s
    Sleep    2
    Nhan icon Sap xep theo trong danh sach video
    Nhan chon Sap xep theo - do dai (giam dan)
    Element Attribute Should Match    ${xp_tm_thoi_luong_vd_1}    text    30s
    Element Attribute Should Match    ${xp_tm_thoi_luong_vd_2}    text    26s
    Element Attribute Should Match    ${xp_tm_thoi_luong_vd_3}    text    15s

BP-3661
    [Documentation]    Check hien thi popup gom cac chuc nang thong tin, xoa, chia se khi cham giu
    [Tags]             BP-3661    BP-3662    BP-3646
    Chon giu video dau tien
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_video_popup_khi_cham_giu_1_video    2
    Page Should Contain Element    ${xp_v_nut_thong_tin}
    Page Should Contain Element    ${xp_v_nut_xoa}
    Page Should Contain Element    ${xp_v_nut_chia_se}
    Sleep    1
    Log To Console   Check hien thi thong tin chi tiet cua video khi cham Thong tin
    Chon xem thong tin cua video
    Page Should Contain Element    ${xp_v_thong_tin_nut_play}
    Page Should Contain Element    ${xp_v_thong_tin_nut_xoa}
    Page Should Contain Element    ${xp_v_text_tap_tin_video}
    Page Should Contain Element    ${xp_v_text_tap_tin_audio}
    Go Back

BP-3663
    [Documentation]    Check hien thi popup ban co chac khong? khi cham icon Thung rac trong giao dien Thong tin
    [Tags]     BP-3663    BP-3664    BP-3666    BP-3649
    Chon giu video dau tien
    Chon xem thong tin cua video
    Cham icon xoa video
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_video_popup_xoa_gd_thong_tin    2
    Page Should Contain Element    ${xp_v_text_ban_co_chac_khong}
    Page Should Contain Element    ${xp_v_nut_huy}
    Page Should Contain Element    ${xp_v_nut_dong_y}
    Log To Console    Check dong popup, khong xoa video khi bam huy
    Chon nut HUY de dong popup
    Page Should Contain Element    ${xp_v_thong_tin_nut_play}
    Page Should Contain Element    ${xp_v_thong_tin_nut_xoa}
    Page Should Contain Element    ${xp_v_text_tap_tin_video}
    Page Should Contain Element    ${xp_v_text_tap_tin_audio}
    Log To Console    BP-3666 : Check phat video khi cham icon Play trong giao dien Thong tin video
    Chon nut play trong thong tin video
    Element Attribute Should Match    ${xp_v_ten_video}    text    1_1920x1080*
    Page Should Contain Element    ${xp_v_nut_overplay}
    Page Should Contain Element    ${xp_v_thanh_seekbar}
    Sleep    2
    Vuot back ve giao dien truoc

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================