*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/cai_dat/khong_lam_phien.robot
Resource    ../page/launcher/launcher_common.robot

*** Test Cases ***
BP-5138
    [Documentation]    Check mo duoc che do khong lam phien tu bang dieu khien
    [Tags]    High    BP-5138    BP-5140    BP-5141    BP-5143
    Mo Cai dat
    Mo kich hoat che do khong lam phien trong cai dat
    Vuot sang man hinh ben duoi
    Kiem tra hien thi mac dinh bat che do lam phien luon hoi
    Vuot mo bang dieu khien
    Vuot sang man hinh ben phai
    Cham vao nut khong lam phien bang toa do
    Bat khong lam phien neu hien thi popup
    Kiem tra hien thi da bat khong lam phien
    Vuot mo bang dieu khien
    Vuot sang man hinh ben phai
    Cham vao nut khong lam phien bang toa do
    Kiem tra hien thi da tat khong lam phien
    Log To Console    Check mo duoc che do khong lam phien tu cai dat
    Click vao bat khong lam phien
    Bat khong lam phien neu hien thi popup
    Kiem tra hien thi da bat khong lam phien
    Click vao tat khong lam phien
    Kiem tra hien thi da tat khong lam phien
    Close Application

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================