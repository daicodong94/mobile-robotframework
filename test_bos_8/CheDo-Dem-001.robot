*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/cai_dat/che_do_dem.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/cai_dat/cd_common.robot

Suite Setup       Mo Cai dat
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5767
    [Documentation]    Kiem tra che do dem
    [Tags]    High    BP-5767    BP-5768    BP-4427
    Nhap vao tab hien thi cai dat
    Nhap vao tab che do dem hien thi cai dat
    Kiem tra lich bieu mac dinh hien thi khong co
    Nhan nut bat che do khong co tai lich bieu
    Nhan nut bat ngay che do dem
    Kiem tra hien thi da bat che do dem
    Nhan nut tat ngay che do dem
    Kiem tra hien thi da tat che do dem
    Kiem tra lich bieu hien thi thoi gian tuy chinh
    Nhan nut bat che do thoi gian tuy chinh tai lich bieu
    Kiem tra hien thi che do bat theo thoi gian tuy chinh
    Kiem tra lich bieu hien thi hoang hon den binh minh
    Nhan nut bat che tu hoang hon den binh minh tai lich bieu
    Kiem tra hien thi che do bat hoang hon den binh minh
    Nhan nut bat check list lich bieu
    Nhan nut bat che do khong co tai lich bieu
    Vuot mo bang dieu khien
    Click toa do bat che do dem
    Kiem tra hien thi da bat che do dem
    Vuot mo bang dieu khien
    Click toa do tat che do dem
    Kiem tra hien thi da tat che do dem

# ANH SANG DEM DA TAT
CDD_050501
    [Documentation]    Check noi dung chuc nang Anh sang dem khi dang o trang thai tat
    [Tags]    CDD_050501
    Vuot sang man hinh ben duoi
    Vuot sang man hinh ben duoi
    Bam vao He thong
    Bam vao Ngay va gio
    ${kiem_tra}    Run Keyword And Return Status    Element Attribute Should Match    ${cd_su_dung_dinh_dang_24_gio}    checked    true
    Run Keyword If   ${kiem_tra}==False     Click Element   ${cd_su_dung_dinh_dang_24_gio}
    Go Back
    Go Back
    Vuot sang man hinh ben tren
    Vuot sang man hinh ben tren
    An vao Hien thi
    Nhap vao tab che do dem hien thi cai dat
    Nhan nut bat check list lich bieu
    Nhan nut bat che do khong co tai lich bieu
    Go Back
    Page Should Contain Element    ${cdd_text_Dang_tat_khong_bao_gio_tu_bat}

CDD_050601
    [Documentation]    Check noi dung chuc nang Anh sang dem khi dang o trang thai bat
    [Tags]    CDD_050601
    Vuot mo bang dieu khien
    Click toa do bat che do dem
    Page Should Contain Element    ${cdd_text_Bat_se_tu_dong_tat_luc_4:30SA}
    Vuot mo bang dieu khien
    Click toa do tat che do dem

CDD_040301
    [Documentation]  Check dong bo trong Cai dat giao dien anh sang chuyen sang dem chuyen sang trang thai bat khi bat che do Anh sang dem tren Dashboard
    [Tags]    CDD_040301
    Nhap vao tab che do dem hien thi cai dat
    Page Should Contain Element    ${cdd_bat_che_do_dem}
    Vuot mo bang dieu khien
    Click toa do bat che do dem
    Page Should Contain Element   ${cdd_tat_che_do_dem}
    Nhan nut tat ngay che do dem

CDD_040302
    [Documentation]    Check dong bo trong Cai dat giao dien Anh sang dem chuyen sang trang thai tat khi tat che do Anh sang dem tren Dashboard
    [Tags]    CDD_040302
    Nhan nut bat ngay che do dem
    Page Should Contain Element    ${cdd_tat_che_do_dem}
    Vuot mo bang dieu khien
    Click toa do tat che do dem
    Page Should Contain Element    ${cdd_bat_che_do_dem}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================