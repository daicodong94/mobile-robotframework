*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/dien_thoai.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/btalk/nhat_ki_cuoc_goi.robot
Resource    ../page/btalk/gui_nhan_tin_nhan.robot
Resource    ../page/cai_dat/cd_common.robot

*** Test Cases ***
# lap 2simm= lap 2sim = lap 2sim #2sim SIM1 LAP VIETTELL SIM2 LAP VINAPHONE     dat lien he yeu thich la cHI THU voi 2 sdt luu ngoai sim
BP-2946
    [Documentation]    Check hien thi danh sach lua chon sim
    [Tags]    BP-2946     High    2sim
    Log To Console    Check hien thi danh sach lua chon sim
    Mo Btalk
    Bam sdt test
    Long Press        ${dt_nut_quay_so}
    Page Should Contain Element    ${dt_danh_sach_sim}
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-2947
    [Documentation]    Check mo popup lua chon sim
    [Tags]    BP-2947    High    2sim
    Log To Console    Check mo popup lua chon sim
    Mo Btalk
    Bam sdt test
    Bam nut quay so
    Page Should Contain Text    	Gọi bằng
    Page Should Contain Element    ${dt_popup_goi_bang_checkbox_luon_sd_sim_nay_de_goi}
    Chon sim1 de quay so TH 2sim popup goi bang
    Bam nut ket thuc cuoc goi

BP-2955
    [Documentation]    chon mac dinh  trong 2sim check hien thi danh sach lua chon sim khi cham va giu nut goi
    [Tags]    BP-2955    2sim
    Mo dien thoai roi vao cai dat dien thoai
    An nut dat mac dinh thuc hien cuoc goi bang sim 1
    Mo Btalk
    Bam sdt test
    Long Press    ${dt_nut_quay_so}
    Page Should Contain Element    ${dt_danh_sach_sim}

BP-2993
    [Documentation]    Check hien thi 2 nut goi khi bat chuc nang hien thi nut goi 2sim
    [Tags]    BP-2993    2sim
    Mo dien thoai roi vao cai dat dien thoai
    An nut tuy chon hien thi
    An vao checkbox hien thi 2 nut goi cho 2 sim o man tuy chon hien thi
    Vuot quay ve man hinh Home
    Mo Dien thoai tu Launcher
    Sleep    2
    ${kiem_tra_nut_quay_so}=  AppiumLibrary.Get Matching Xpath Count  //*[@content-desc="quay số"]
    Sleep    3
    Should Be Equal     ${kiem_tra_nut_quay_so}     2
    Sleep    2
    Element Attribute Should Match    ${dt_tab_dt_quay_so_sim1}    text    Viettel
    Sleep    2
    Element Attribute Should Match    ${dt_tab_dt_quay_so_sim2}    text    VINAPHONE

BP-3006
    [Documentation]    Check mo popup lua chon sim khi cham vao lien he co 1 sdt khi may lap 2sim
    [Tags]    BP-3006    2sim
    Bam cai dat dien thoai va vao Tai khoan goi
    An nut thuc hien cuoc goi bang
    An nut hoi truoc tai popup thuc hien cuoc goi bang
    Mo Btalk
    Vuot len launcher de vao danh ba quay so nhanh
    An vao lien he thu 2 trong db quay so nhanh
    Page Should Contain Text    Gọi bằng
    Page Should Contain Element    ${dt_popup_goi_bang_checkbox_luon_sd_sim_nay_de_goi}

BP-3008
    [Documentation]    Check mo popup lua chon SDT khi cham vao lien he co 2 sdt tro len
    [Tags]    BP-3008    2sim
    Mo Btalk
    Vuot len launcher de vao danh ba quay so nhanh
    An vao lien he dau tien db quay so nhanh
    Page Should Contain Text    Chọn số
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-3009
    [Documentation]    Check mo giao dien chi tiet lien he  khi cham vao dau 3cham tren moi lien he
    [Tags]    BP-3009     2sim
    Mo Btalk
    Vuot len launcher de vao danh ba quay so nhanh
    An vao nut 3cham db quay so nhanh lien he1
    Page Should Contain Element    ${dt_db_chi_tiet_lien_he_nut_sua}

BP-3011
    [Documentation]    Check luu vi tri khi thoat danh ba quay so nhanh
    [Tags]    BP-3011    2sim
    Mo Btalk
    Vuot len launcher de vao danh ba quay so nhanh
    Vuot quay ve man hinh Home
    Bam mo app dien thoai
    Page Should Contain Element    ${dt_nut_quay_so}

BP-3012
    [Documentation]    Check mo giao dien dien thoai khi cham vao icon Ban phim so
    [Tags]    BP-3012     2sim
    Mo Btalk
    Vuot len launcher de vao danh ba quay so nhanh
    An vao icon bam phim so db quay so nhanh
    Page Should Contain Element        ${dt_nut_quay_so}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================