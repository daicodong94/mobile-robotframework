*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/chim_lac/cl_common.robot
Resource    ../page/kham_pha/kp_common.robot

Suite Setup       Mo Chim lac
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-20
    [Documentation]    Check mo link trang web huong dan su dung tinh nang chong trom khi nhap link hien thi tren huong dan vao trinh duyet
    [Tags]    BP-20
    Bat Che do thu gon cua Chim Lac bang popup
    Go To Url    ${kp_url_chong_trom}
    Sleep    5
    Log To Console     Xac nhan mo huong dan su dung tinh nang chong trom thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cl_url_bar}    text    ${kp_url_chong_trom_sau_dieu_huong}

BP-22
    [Documentation]    Check mo link trang web huong dan Chuyen doi du lieu tu Android sang BOS khi nhap link hien thi tren huong dan vao trinh duyet
    [Tags]    BP-22
    Go To Url    ${kp_url_android}
    Sleep    5
    Log To Console     Xac nhan mo huong dan Chuyen doi du lieu tu Android sang BOS thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cl_url_bar}    text    ${kp_url_android_sau_dieu_huong}
    AppiumLibrary.Page Should Contain Text    ${kp_txt_android}

BP-24
    [Documentation]    Check mo link trang web huong dan Chuyen doi du lieu tu IOS sang BOS khi nhap link hien thi tren huong dan vao trinh duyet
    [Tags]    BP-24
    Go To Url    ${kp_url_ios}
    Sleep    5
    Log To Console     Xac nhan mo huong dan Chuyen doi du lieu tu IOS sang BOS thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cl_url_bar}    text    ${kp_url_ios_sau_dieu_huong}
    AppiumLibrary.Page Should Contain Text    ${kp_txt_ios}

BP-26
    [Documentation]    Check mo link trang web huong dan Mot so luu y khi sac pin khi nhap link hien thi tren huong dan vao trinh duyet
    [Tags]    BP-26
    Go To Url    ${kp_url_sac_pin}
    Sleep    5
    Log To Console     Xac nhan mo huong dan Mot so luu y khi sac pin thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cl_url_bar}    text    ${kp_url_sac_pin_sau_dieu_huong}
    AppiumLibrary.Page Should Contain Text    ${kp_txt_sac_pin}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================