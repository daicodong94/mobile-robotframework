*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
ThinhLQ         | 2021/07/23 | update bo comment va thay doi video test, thay doi anh giao dien so sanh
----------------|------------|--------------------------------------------------------------------

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/xem_phim/tuy_chon.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup       Mo Xem phim
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5292
    [Documentation]    Check hien thi phim A-B khi cham chuc nang Lap doan A-B
    [Tags]    BP-5292
    Cho phep cap quyen cho ung dung
    Click vao bo danh sach video1
    Click vao video 5
    Cham vao man hinh video
    Click vao nut play
    Vuot thanh thoi gian ve ban dau
    Click vao tuy chon
    Click vao lap lai AB
    Kiem tra giao dien pop up lap lai AB
    Go Back
    Go Back

BP-5270
    [Documentation]    Check tro ve giao dien danh sach video khi nhap thoi gian tua den vi tri lon hon tong thoi luong cua video va cham DONG Y
    [Tags]    BP-5270
    Click vao bo danh sach video1
    Vuot sang man hinh ben duoi
    Click vao video cuoi danh sach
    Cham vao man hinh video
    Click vao tuy chon
    Click vao tua den vi tri
    Nhap so thoi gian vao phut tua den vi tri 15 phut
    Kiem tra hien thi text vew tai man hinh danh sach video
    Kiem tra hien thi nut tim kiem tai man hinh danh sach video
    Kiem tra khong hien thi nut tuy chon
    Go Back

BP-5275
    [Documentation]    Check chi cho phep nhap gia tri thoi gian tu 0s den 99s
    [Tags]    BP-5275
    Click vao bo danh sach video1
    Click vao video 5
    Cham vao man hinh video
    Click vao nut play
    Vuot thanh thoi gian ve ban dau
    Click vao tuy chon
    Click vao so giay muon back
    input vao so giay back 99
    Kiem tra hien thi nhap so giay back la 99s

BP-5276
    [Documentation]    Check khong cho phep nhap gia tri > 99s
    [Tags]    BP-5276
    input vao so giay back la 100
    Kiem tra nhap 100s hien thi tren man hinh chi nhap duoc 10s


BP-5278
    [Documentation]    Check disable button DONG Y khi de trong thoi gian
    [Tags]    BP-5278
    Clear Text    ${xp_text_gio_giay_back}
    Kiem tra trang thai hien nut dong y


BP-5279
    [Documentation]    Check disable button DONG Y khi gia tri thoi gian nhap la 0s
    [Tags]    BP-5279
    input vao so giay back la 00
    Kiem tra trang thai hien nut dong y


BP-5280
    [Documentation]    Check disable button DONG Y khi gia tri thoi gian nhap > 30s va < 100s
    [Tags]    BP-5280
    input vao so giay back la 75
    Kiem tra trang thai hien nut dong y

BP-5277
    [Documentation]    Check enable button DONG Y khi gia tri thoi gian nhap > 0s va < 31s
    [Tags]    BP-5277
    input vao so giay back 30
    Cham vao man hinh video
    Click vao tuy chon
    Kiem tra nhap 30 giay dong y thoi gian back
    Go Back

BP-5310
    [Documentation]    Check chuc nang tre am thanh
    [Tags]    BP-5310    BP-5312
    Cham vao man hinh video
    Click vao tuy chon
    Click vao tre am thanh
    Kiem tra hien thi tren man hinh tre am thanh

BP-5313
    [Documentation]    Check tang thoi gian tre tieng khi cham icon dau +
    [Tags]    BP-5313    BP-5316
    Click vao nut cong tre am thanh
    Kiem tra hien thi tre am thanh tang len 50s

BP-5315
    [Documentation]    Check tang thoi gian tre tieng khi bam lien tuc icon dau +
    [Tags]    BP-5315
    Click vao nut cong tre am thanh
    Kiem tra hien thi tre am thanh tang dan 100s

BP-5314
    [Documentation]    Check giam thoi gian tre tieng khi cham icon dau -
    [Tags]    BP-5314    BP-5318
    Click Element    ${xp_nut_tru_tre_am_thanh}
    Kiem tra hien thi tre am thanh tu 100s giam xuong 50s

BP-5317
    [Documentation]    Check giam thoi gian tre tieng khi cham icon dau -
    [Tags]    BP-5317
    Click vao nut tru tre am thanh
    Kiem tra hien thi tre am thanh bam lien tuc giam xuong -200s

BP-5319
    [Documentation]    Check chuc nang tre am thanh
    [Tags]        BP-5319
    Kiem tra khong dong chuc nang tre tieng khi khong cham man hinh

BP-5321
    [Documentation]    Check chuc nang tre am thanh
    [Tags]    BP-5321
    Cham vao man hinh video
    Kiem tra dong chuc nang tre tieng khi cham man hinh video

BP-5322
    [Documentation]    Check chuc nang tre tieng
    [Tags]    BP-5322
    Cham vao man hinh video
    Click vao tuy chon
    Click vao tre phu de
    Kiem tra hien thi tren man hinh tre phu de
    Click vao nut cong tre phu de
    Kiem tra hien thi tre phu de tang len 50s
    Click vao nut cong tre phu de
    Kiem tra hien thi tre phu de tang dan 100s
    Click Element    ${xp_nut_tru_tre_phu_de}
    Kiem tra hien thi tre phu de tu 100s xuong 50s
    Click vao nut tru tre phu de
    Kiem tra hien thi tre phu de bam lien tuc giam xuong -200s
    Kiem tra khong dong chuc nang tre tieng khi khong cham man hinh
    Cham vao man hinh video
    Kiem tra dong chuc nang tre tieng khi cham man hinh video

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================