*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/lockscreen/lockscreen.robot

*** Test Cases ***
BP-4789
    [Documentation]    Check mo duoc che do khong lam phien tu bang dieu khien
    [Tags]     High    BP-4789    BP-4752    BP-4763    BP-4761
    Mo Cai dat
    Vuot toa do de hien thi tab bao mat
    Click vao tab bao mat
    Lock    1
    Mo khoa voi mat khau text cccc
    Kiem tra mo khoa may va hien thi tai man hinh bao mat
    Kiem tra mo khoa may va hien thi bao mat thiet bi
    Click vao tab khoa man hinh
    Mo khoa thiet lap
    Click vao tab khoa man hinh vuot
    Click vao xoa tinh nang bao ve thiet bi
    Lock    1
    Cham toa do
    Vuot toa do de mo khoa man hinh
    Kiem tra mo khoa may va hien thi tai man hinh bao mat
    Kiem tra mo khoa may va hien thi bao mat thiet bi
    Click vao tab khoa man hinh
    Click vao ma pin
    Click vao xac nhan co khoi dong che do ma pin
    Click vao xac nhan dong y
    Bam vao toa do so 8888 tao ma pin
    Click vao xac nhan tiep theo
    Bam vao toa do so 8888 tao ma pin
    Click vao xac nhan tao ma pin
    Check hien thi popup nhan xac nhan xong hien thi thong bao
    Lock    1
    Cham toa do
    Mo khoa voi mat khau 8888
    Kiem tra mo khoa may va hien thi tai man hinh bao mat
    Kiem tra mo khoa may va hien thi bao mat thiet bi
    Close Application

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================