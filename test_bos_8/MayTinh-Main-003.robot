*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/may_tinh/mt_common.robot

Suite Setup    Mo May tinh
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu
*** Test Cases ***

BP-274
    [Documentation]    Check nhan nut bang (=) khi chua nhap phep tinh
    [Tags]    BP-274
    Sleep    2
    An nut dau (=)
    Sleep    2
    AppiumLibrary.Page Should Contain Element    ${mt_nut_bang}

BP-279
    [Documentation]    Check nhan nut bang (=) khi da nhap phep tinh
    [Tags]    BP-279
    Log To Console    Check chuc nang xoa 1 so
    Xoa man hinh
    Sleep    2
    An nut 123
    Sleep    2
    Click xoa so
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    12

BP-277
    [Documentation]    Check chuc nang xoa nhieu so
    [Tags]    BP-277
    An nut 123
    Click xoa so
    Click xoa so
    Click xoa so
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Should Be Equal    ${get_text}    1


BP-281
    [Documentation]    Check chuc nang xoa het - Check chuc nang DEL
    [Tags]    BP-281
    Xoa man hinh
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    ${EMPTY}

BP-276
    [Documentation]    Check nhan nut bang (=) khi da nhap phep tinh
    [Tags]    BP-276
    An nut 123
    An nut dau cong (+)
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Should Be Equal    ${get_text}    579

BP-284
    [Documentation]    Check chuc nang xoa ket qua tinh vua thuc hien
    [Tags]    BP-284
    An nut Clear xoa ket qua
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    ${EMPTY}

BP-287
    [Documentation]    Check chuc nang doi vi tri con tro chuot va them so
    [Tags]    BP-287    BP-288
    An nut 123456789 check vi tri con tro chuot

BP-288
    [Documentation]    Check chuc nang doi vi tri con tro chuot xoa so
    [Tags]    BP-288
    Xoa so theo vi tri da chon

BP-289
    [Documentation]    kiem tra phep tinh cong (+) giua cac so nguyen
    [Tags]    BP-289
    Xoa man hinh
    An nut 123
    An nut dau cong (+)
    An nut 456
    Chup anh man hinh va so sanh voi anh mau    ${mt_alias}    kc_d_ttdh_d_d_main_phep_tinh_co_data    2
    An nut dau (=)
    Chup anh man hinh va so sanh voi anh mau    ${mt_alias}    kc_d_ttdh_d_d_main_ket_qua_co_data    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Should Be Equal    ${get_text}    579
    Element Attribute Should Match    ${mt_nut_clr}    text    CLR

BP-292
    [Documentation]    kiem tra phep tinh cong (+) giua so am (-) va cac so nguyen
    [Tags]    BP-292
    An nut Clear xoa ket qua
    An nut dau tru (-)
    An nut 123
    An nut dau cong (+)
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Should Be Equal    ${get_text}    333
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

BP-295
    [Documentation]    kiem tra phep tinh cong (+) giua cac so thap phan
    [Tags]    BP-295
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau cong (+)
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *246**912*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    5

BP-298
    [Documentation]    kiem tra phep tinh cong (+) giua cac so thap phan va cac so nguyen
    [Tags]    BP-298
    An nut Clear xoa ket qua
    An nut 123
    An nut phay thap phan
    An nut 456
    An nut dau cong (+)
    An nut 123
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *246**456*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2

BP-299
    [Documentation]    Kiem tra phep tinh cong lon
    [Tags]    BP-299
    An nut Clear xoa ket qua
    An nut 12345678901234556
    An nut dau cong (+)
    An nut 12345678901234556
    An nut dau (=)
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *2**469135*
    Element Attribute Should Match     ${mt_nut_clr}    text    CLR
    Sleep    2
    An nut Clear xoa ket qua

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================