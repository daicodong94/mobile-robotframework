 *** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/xem_phim/thu_muc.robot
Resource    ../page/xem_phim/video.robot
Resource    ../page/camera/cam_common.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
# Test Case check xoa video
BP-3667
    [Documentation]    Check hien thi popup Ban co chac khong? khi cham giu video va cham xoa
    [Tags]             BP-3667    BP-3668    BP-3669
    Mo Xem phim
    Chon giu video quay bang camera
    Mo May anh roi mo QUAY PHIM
    Bam nut quay phim chup anh    10
    Sleep    7
    Bam nut quay phim chup anh    0
    Dong toan bo ung dung va ket thuc phien kiem thu
    Mo Xem phim
    Cho phep cap quyen cho ung dung
    Chon giu video quay bang camera
    Page Should Contain Element    ${xp_v_nut_thong_tin}
    Page Should Contain Element    ${xp_v_nut_xoa}
    Page Should Contain Element    ${xp_v_nut_chia_se}
    Log To Console    BP-3668 : Check khong xoa Video khi bam Huy
    Chon nut xoa Video
    Page Should Contain Element    ${xp_v_text_ban_co_chac_khong}
    Page Should Contain Element    ${xp_v_nut_huy}
    Page Should Contain Element    ${xp_v_nut_dong_y}
    Chon nut HUY de dong popup
    Log To Console    BP-3669 : Check xoa Video khoi sanh sach video khi cham Dong y
    Chon giu video quay bang camera
    Chon nut xoa Video
    Chon nut Dong y de xoa video
    Page Should Not Contain Element    ${xp_v_video_quay_man_hinh}
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-3665
    [Documentation]    Chech xoa video khoi danh sach video khi bam DONG Y
    [Tags]             BP-3665
    Mo Chim lac
    Chon di toi duong dan
    Go To Url    https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4
    Sleep    4
    Bam vao man hinh
    Cham toa do dung phat video tren Chim lac
    Sleep    10
    Download video tu chim lac
    Download video tu chim lac
    Dong toan bo ung dung va ket thuc phien kiem thu
    Mo Xem phim
    Cho phep cap quyen cho ung dung
    Chon video download
    Chon giu video dau tien
    Chon xem thong tin cua video
    Cham icon xoa video
    Chon nut Dong y de xoa video
    Sleep    2
    Page Should Contain Element    ${xp_v_mo_video_file_mp4}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================