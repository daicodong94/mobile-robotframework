*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library    AppiumLibrary

Resource    ../page/dong_ho/bao_thuc.robot
Resource    ../page/launcher/launcher_common.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu
*** Test Cases ***

BP-372
    [Documentation]    Check chuc nang tat va bat rung
    [Tags]    BP-372
    Log To Console    Check chuc nang tat va bat rung
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Bam vao them bao thuc
    Bam vao dong y bao thuc
    Bam vao rung
    Log To Console    BP-372: Check chuc nang tat va bat rung
    Element Attribute Should Match    ${dh_bt_cd_rung}    checked    false
    Sleep    1


BP-374
    [Documentation]    Check chuc nag tao nhan
    [Tags]    BP-374
    Log To Console    Check chuc nag tao nhan
    Bam vao nhan
    Bam vao thanh nhap text nhan
    AppiumLibrary.Input Text    ${dh_bt_label_nhan_text}    text
    Bam vao dong y bao thuc
    Page Should Contain Text    text


BP-376
    [Documentation]    Check chuc nang huy tao nhan
    [Tags]    BP-376    BP-377
    Log To Console    Check chuc nang huy tao nhan
    Bam vao nhan
    Click xoa chu tren text nhan
    AppiumLibrary.Input Text    ${dh_bt_label_nhan_text}    text
    Bam vao huy
    AppiumLibrary.Page Should Not Contain Element    ${dh_bt_label_nhan_text}

BP-378
    [Documentation]    Check chuc nang xoa bao thuc
    [Tags]    BP-378
    Log To Console    Check chuc nang xoa bao thuc
    Bam vao xoa
    Bam vao mo rong bao thuc
    Bam vao xoa
    Bam vao mo rong bao thuc
    Bam vao xoa
    AppiumLibrary.Page Should Contain Element    ${dh_text_da_xoa_bao_thuc}
    AppiumLibrary.Page Should Contain Element    ${dh_text_hoan_tac}


BP-383
    [Documentation]    Check mo rong giao dien chinh sua bao thuc
    [Tags]    BP-383
    Log To Console    Check mo rong giao dien chinh sua bao thuc
    Bam vao them bao thuc
    Bam vao dong y bao thuc
    AppiumLibrary.Page Should Contain Element    ${dh_bt_cd_rung}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_label_nhan_dan}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_nut_xoa_bao_thuc}

BP-384
    [Documentation]    Check thu gon giao dien chinh sua bao thuc
    [Tags]    BP-384
    Log To Console    Check thu gon giao dien chinh sua bao thuc
    Bam vao thu gon bao thuc
    AppiumLibrary.Page Should Not Contain Element    ${dh_bt_cd_rung}
    AppiumLibrary.Page Should Not Contain Element    ${dh_bt_label_nhan_dan}
    AppiumLibrary.Page Should Not Contain Element    ${dh_bt_nut_xoa_bao_thuc}
    Close Application

BP-351
    [Documentation]    Check chuc nang lap lai bao thuc
    [Tags]    BP-351
    Log To Console    Check chuc nang lap lai bao thuc
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Bam vao them bao thuc
    Bam vao dong y bao thuc
    Bam vao lap lai bao thuc
    AppiumLibrary.Page Should Contain Element    ${dh_bt_txt_hien_thi_thu_hai}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_txt_hien_thi_thu_ba}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_txt_hien_thi_thu_tu}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_txt_hien_thi_thu_nam}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_txt_hien_thi_thu_sau}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_txt_hien_thi_thu_bay}
    AppiumLibrary.Page Should Contain Element    ${dh_bt_txt_hien_thi_chu_nhat}
    Element Attribute Should Match    ${dh_bt_nut_lap_lai_bao_thuc}    checked    true
    Close Application

BP-346
    [Documentation]    Check chuc nang lap lai bao thuc
    [Tags]    BP-351
    Log To Console    Check chuc nang lap lai bao thuc
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Bam vao text dong ho bao thuc hien thi
    Page Should Contain Element    ${nut_huy}
    Page Should Contain Element    ${nut_dong_hoa}
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================