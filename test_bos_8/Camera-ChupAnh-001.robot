*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/camera/cam_common.robot

Suite Setup       Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5752
    [Documentation]    Mo May anh tu Dashboard khi mo chuc nang quet QR code
    [Tags]     High    BP-5752
    Mo quet QR code tu Dashboard
    AppiumLibrary.Page Should Contain Text    ${db_txt_quet_qr_code}
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-5216
    [Documentation]    Mo May anh tu man hinh khoa
    [Tags]     High    BP-5216
    Mo Launcher
    Lock    1
    Mo May anh tu man hinh khoa
    Xac nhan mo CHUP ANH thanh cong
    Vuot back ve giao dien truoc
    Mo khoa voi mat khau 8888

BP-5215
    [Documentation]    Mo May anh tu Launcher
    [Tags]     High    BP-5215
    Dong toan bo ung dung trong da nhiem
    Mo May anh tu Launcher
    Cho phep cap quyen cho ung dung
    Xac nhan mo CHUP ANH thanh cong
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-5750
    [Documentation]    Vuot mo che do Chup anh tu che do chup khac
    [Tags]     High    BP-5750
    # Mo May anh roi mo che do chup Chan dung
    # Vuot sang man hinh ben phai
    # Xac nhan mo CHUP ANH thanh cong
    # AppiumLibrary.Close All Applications
    Mo May anh roi mo QUAY PHIM
    Vuot sang man hinh ben trai
    Xac nhan mo CHUP ANH thanh cong
    AppiumLibrary.Close All Applications

BP-5751
    [Documentation]    Bam mo che do Chup anh tu che do chup khac
    [Tags]     High    BP-5751
    # Mo May anh roi mo che do chup Chan dung
    # Bam nut mo che do CHUP ANH
    # AppiumLibrary.Close All Applications
    Mo May anh roi mo QUAY PHIM
    Bam nut mo che do CHUP ANH
    Xac nhan mo CHUP ANH thanh cong

BP-5756
    [Documentation]    Chuyen doi Camera truoc/sau khi bam nut Chuyen doi Camera
    [Tags]     High    BP-5756
    Bam nut chuyen doi cam truoc sau
    Log To Console     Xac nhan doi sang Camera truoc thanh cong
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_hdr}
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_flash}
    AppiumLibrary.Page Should Contain Element        ${cam_nut_lam_dep}
    Bam nut chuyen doi cam truoc sau
    Xac nhan mo CHUP ANH thanh cong

BP-5753
    [Documentation]    Bam nut Chup anh
    [Tags]     High    BP-5753    BP-5757
    Bam nut quay phim chup anh    2
    Mo che do xem cuon phim
    Bam de hien thi ten va tuy chon cua file
    Log To Console     BP-5757 - Mo Che do xem cuon phim tu chuc nang Chup anh
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_bo_loc}
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_cai_dat}
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_auto_adv}
    AppiumLibrary.Page Should Not Contain Element    ${cam_nut_quay_chup}
    AppiumLibrary.Page Should Contain Element        ${cam_nut_thong_tin}
    AppiumLibrary.Page Should Contain Element        ${cam_nut_xoa}
    Log To Console     Xoa du lieu truoc khi ket thuc phien kiem thu
    AppiumLibrary.Click Element    ${cam_nut_xoa}
    Sleep    2

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================