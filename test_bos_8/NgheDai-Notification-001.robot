*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
ThinhLQ         | 2021/07/23 | update code , tach nho case rieng biet
----------------|------------|--------------------------------------------------------------------
*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/nghe_dai/notification.robot
Resource    ../page/nghe_dai/yeu_thich.robot
Resource    ../page/cai_dat/cu_chi_dieu_huong.robot

Suite Setup    Mo Nghe dai
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***

BP-2591
    [Documentation]    Check thanh notification khi khong co mang
    [Tags]    High    BP-2591
    Cho phep cap quyen cho ung dung
    Sleep    5
    Xac nhan mo app Nghe dai thanh cong
    Mang di dong = 1, data = 0, wifi = 0, may bay = 0
    Kiem tra nut play co hoat dong khi bat dau mo app
    Cho den khi hien thi thong bao khong co ket noi mang wf

BP-2592
    [Documentation]    Check thanh notification khi co mang
    [Tags]    High    BP-2592
    Mang di dong = 1, data = 0, wifi = 1, may bay = 0
    Kiem tra nut play co hoat dong khi bat dau mo app
    Cho den khi ket noi kenh dang phat Vuot mo bang thong bao
    Kiem tra hien thi kenh vov3 dang phat tren notification

BP-2593
    [Documentation]    Check chuc nang dung nghe
    [Tags]    High    BP-2593
    Click bat phat kenh dang nghe tren notification
    Page Should Contain Text    ${nd_vov3_tam_dung}

BP-2594
    [Documentation]    Check chuc nang back kenh
    [Tags]    High    BP-2594
    Vuot dong bang thong bao
    Click phat kenh vov2
    Cho den khi ket noi kenh dang phat Vuot mo bang thong bao
    Click back chuyen kenh dang phat
    Vuot dong bang thong bao
    Cho den khi ket noi kenh dang phat Vuot mo bang thong bao
    Click dung phat kenh dang nghe tren notification
    Kiem tra hien thi kenh vov3 dang dung tren notification

BP-2595
    [Documentation]    Check chuc nang next kenh
    [Tags]    High    BP-2595
    Click next chuyen kenh dang phat
    Kiem tra hien thi kenh vov2 dang phat tren notification
    Click back chuyen kenh dang phat
    Kiem tra hien thi kenh vov3 dang phat tren notification

BP-2596
    [Documentation]    Check chuc nang tat thanh hien thi tren Notification
    [Tags]    High    BP-2596    BP-5787    BP-5798
    Click tat nhanh nghe dai tren thanh notify
    Vuot dong bang thong bao
    Kiem tra hien thi khong co dich vu khi thoat kenh dang phat tren notification

BP-2597
    [Documentation]    Check phat duoc cac kenh khi tat thanh hien thi tren Notification
    [Tags]    High    BP-2597
    Click phat kenh vov2
    Cho den khi ket noi kenh dang phat Vuot mo bang thong bao
    Click tat nhanh nghe dai tren thanh notify
    Vuot dong bang thong bao
    Click phat kenh vov2
    Cho den khi ket noi kenh dang phat Vuot mo bang thong bao
    Kiem tra hien thi kenh vov2 dang phat tren notification
    Vuot dong bang thong bao

BP-2571
    [Documentation]    Check giao dien man hinh notification
    [Tags]    BP-2570    BP-2571    BP-2572    BP-2573    BP-2574    BP-2575    BP-2576
    ...    BP-2578    BP-2579  BP-2580    BP-2581    BP-2582    BP-2583    BP-2584
    ...    BP-2585    BP-2586    BP-2587    BP-2588
    Vuot mo bang thong bao
    Sleep    3
    Chup anh man hinh va so sanh voi anh mau    ${nd_alias}    ${bphone}_kc_d_ttdh_d_d_Notification    16
    Vuot dong bang thong bao

BP-2598
    [Documentation]    Check phat duoc cac kenh khi tat thanh hien thi tren Notification
    [Tags]    High    BP-2598
    Thuc hien click button them moi
    Cho phep cap quyen cho ung dung
    Vuot mo bang thong bao
    Click tat nhanh nghe dai tren thanh notify
    Vuot dong bang thong bao
    Page Should Not Contain Element    ${nd_notification_text_kenh_dang_phat1}
    Vuot dong bang thong bao
    Go Back
    Click Element    ${nd_txt_vov3}
    Sleep    3
    Vuot quay ve man hinh Home

BP-2599
    [Documentation]    Check khong tu dong phat nhac khi thay doi Cach dieu huong
    [Tags]    BP-2599
    Mo Cai dat
    Nhan vao tab dieu huong
    Nhan vao tab dieu huong bang cu chi
    Nhan vao tab dieu huong bang thanh dieu huong
    Vuot mo bang thong bao
    Kiem tra hien thi kenh vov3 dang phat tren notification
    Vuot dong bang thong bao
    Nhan vao tab dieu huong bang thanh dieu huong
    Nhan vao tab dieu huong bang cu chi


#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================