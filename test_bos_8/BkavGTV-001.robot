*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/bkav_gtv/bkav_gtv.robot

*** Test Cases ***
BP-5157
    [Documentation]    Check mo giao dien ban phim khi cham vao vung soan tin nhan
    [Tags]    High    BP-5157    BP-5158    BP-5760    BP-5761
    ...    BP-5762    BP-5763    BP-5764    BP-5765    BP-5766
    Check mo giao dien ban phim khi cham vao vung soan tin nhan
    Thuc nhap noi dung chu thuong tren ban phim
    # Lock    1
    # Mo khoa voi mat khau text cccc
    Close Application
    Mo Ghi chep
    Check mo giao dien ban phim khi mo ghi chep
    Dong toan bo ung dung va ket thuc phien kiem thu

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================