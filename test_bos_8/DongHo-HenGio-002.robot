*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/dong_ho/hen_gio.robot
Resource    ../page/common.robot
Resource    ../page/dong_ho/dh_common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-689
    [Documentation]    Check TH hien thi chuc nang DAT LAI bang thao tac bam vao thoi gian dang chay trong giao dien Bo hen gio
    [Tags]    BP-689    BP-691    BP-692
    Log To Console    Check TH hien thi chuc nang DAT LAI bang thao tac bam vao thoi gian dang chay trong giao dien Bo hen gio
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Log To Console    BP-691: Check TH tiếp tục chạy Bộ hẹn giờ bằng thao tác bấm vào thời gian trên giao diện Bộ hẹn giờ
    Thuc hien nhan dong ho hen gio dang chay
    Close Application

BP-695
    [Documentation]    Thuc hien kiem tra text dat lai hen gio khi nhan vao dong ho hen gio dang chay
    [Tags]    BP-695    BP-693
    Log To Console    Thuc hien kiem tra text dat lai hen gio khi nhan vao dong ho hen gio dang chay
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Thuc bam vao dat lai restet lai bo hen gio ve ban dau
    Close Application

BP-697
    [Documentation]    Check TH Them nhan thanh cong
    [Tags]    BP-697    BP-698     BP-699    BP-700
    Log To Console    Check TH Them nhan thanh cong
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Thuc hien nhap vao nhan comment va thiet lap
    Close Application

BP-702
    [Documentation]    Thuc hien kiem tra bo hen gio qua han
    [Tags]    BP-701    BP-702    BP-703
    Log To Console    Thuc hien kiem tra bo hen gio qua han
    Mo Dong ho
    Sleep    2
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Thuc hien kiem tra notify bo hen gio qua han thong bao
    Close Application

BP-704
    [Documentation]    Thuc hien kiem tra bo hen gio qua han nhan them 1 phut
    [Tags]    BP-704
    Log To Console    Thuc hien kiem tra bo hen gio qua han nhan them 1 phut
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Nhan nut menu top hen gio
    Thuc hien kiem tra bo hen gio qua han nhan cong them thoi gian 1 phut

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================