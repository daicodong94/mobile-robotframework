*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/thoi_tiet/tt_common.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup       Mo Thoi tiet
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-2605
    [Documentation]    Check mo ung dung khi khong co mang lan dau
    [Tags]    BP-2605     BP-2620
    Log To Console    Hien thi man hinh khong co thong tin Thoi tiet cua thanh pho nao
    Log To Console    Tat ket noi internet
    Mang di dong = 1, data = 0, wifi = 0, may bay = 0
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    AppiumLibrary.Page Should Not Contain Element    ${tt_ds_dia_diem}
    AppiumLibrary.Page Should Not Contain Element    ${tt_ds_nhiet_do}

BP-2613
    [Documentation]    Check chuc nang them moi khi may khong co mang
    [Tags]    BP-2613
    # Vuot back ve giao dien truoc
    # Click Element    ${tt_nut_back}
    Sleep    2    
    Bam nut them thanh pho
    AppiumLibrary.Page Should Contain Text    ${tt_txt_ket_noi_mang}
  
BP-2602
    [Documentation]    Check thong bao khi cham nut Tien ich con
    [Tags]    BP-2602
    Log To Console    Bat ket noi internet
    Mang di dong = 1, data = 1, wifi = 1, may bay = 0
    Vuot back ve giao dien truoc
    Mo Thoi tiet tu Launcher
    Sleep    5    
    Vuot quay ve man hinh Home
    Vuot sang man hinh ben trai
    Page Should Contain Element    ${app_thoi_tiet}    
    Log To Console    Cham va giu vao ung dung Thoi tiet
    AppiumLibrary.Long Press    ${app_thoi_tiet}    duration=2000
    Log To Console    Cham vao nut Tao tien ich con
    AppiumLibrary.Click Element    ${tt_tk_text_icon_tien_ich}
    Sleep    2
    Log To Console    Hien thi thong bao huong dan tao Tien ich con
    AppiumLibrary.Page Should Contain Element    ${tt_tk_txt_tien_ich_thoi_tiet}

BP-2603
    [Documentation]    Check hien thi khi thuc hien tao Tien ich con
    [Tags]    BP-2603
    Log To Console    Cham va keo ra man hinh
    AppiumLibrary.Long Press    ${tien_ich_thoi_tiet_preview}    duration=2000
    Log To Console    Tien ich con Thoi tiet hien thi tren man hinh
    AppiumLibrary.Page Should Contain Element    ${tien_ich_thoi_tiet}

BP-2604
    [Documentation]    Check thong tin Thoi tiet khi tao Tien ich con
    [Tags]    BP-2604    BP-2606    BP-2625    TT_240101    BP-1709
    Log To Console    Kiem tra tien ich con tren man hinh
    ${dia_diem}=    AppiumLibrary.Get Text    ${tien_ich_thoi_tiet_dia_diem}
    ${nhiet_do}=    AppiumLibrary.Get Text    ${tien_ich_thoi_tiet_nhiet_do}
    Log To Console    Check mo ung dung lan tiep theo khi da co dia diem mac dinh va ngat ket noi mang
    Log To Console    Tat ket noi internet
    Mang di dong = 1, data = 0, wifi = 0, may bay = 0
    Sleep    2
    Mo Thoi tiet tu Launcher
    Sleep    2
    Log To Console    Quay lai man hinh chinh
    Vuot sang man hinh ben trai
    Mo Thoi tiet tu Launcher
    Xac nhan hien thi giao dien man hinh chinh
    Log To Console    Tien ich con hien thi thong tin Thoi tiet dung voi thong tin Thoi tiet cua Thanh pho mac dinh
    AppiumLibrary.Page Should Contain Text    ${dia_diem}
    AppiumLibrary.Element Attribute Should Match    ${tt_nhiet_do_mac_dinh}    text    ${nhiet_do}
    Log To Console    Kiem tra danh sach Thoi tiet khi thuc hien xoa thong tin Thanh pho
    Log To Console    Thong tin Thoi tiet Thanh pho Ha Nam khong ton tai tren man hinh chinh
    AppiumLibrary.Page Should Not Contain Element    ${tt_tk_ha_nam}
    Sleep    2
    Dong toan bo ung dung trong da nhiem

BP-2618
    [Documentation]    Check man hinh tim kiem khi dan text tu ngoai vao
    [Tags]    BP-2618
    Mo Thoi tiet tu Launcher
    Sleep    2
    Mang di dong = 1, data = 0, wifi = 1, may bay = 0
    Bam nut them thanh pho
    Log To Console    Dan ki tu Ha Nam tu ben ngoai vao TextBox
    Click Element    ${tt_tk_nhap_ten_thanh_pho}
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Hà Nam
    Sleep    2
    Log To Console    Man hinh tim kiem Thoi tiet hien thi goi y thong tin Thanh pho Ha Nam
    Xpath Should Match X Times    ${tt_tk_ha_nam}    0
    Element Attribute Should Match    ${tt_tk_ha_nam}    text    *Hà**Nam*

BP-2619
    [Documentation]    Check man hinh chinh khi dan text tu ngoai vao man hinh tim kiem Thoi tiet
    [Tags]    BP-2619
    Log To Console    Chon Ha Nam trong danh sach goi y
    Sleep    2
    AppiumLibrary.Click Element    ${tt_tk_text_ha_nam}
    Sleep    2
    Log To Console    Dong man hinh tim kiem thong tin Thoi tiet, chuyen sang man hinh chinh
    AppiumLibrary.Page Should Not Contain Element    ${tt_tk_nhap_ten_thanh_pho}
    Sleep    5
    Xac nhan hien thi giao dien man hinh chinh
    Sleep    2
    Log To Console    thong tin Thoi tiet cua thanh pho Ha Nam đuoc them hien thi duoi cung trong danh sach Thoi tiet cac thanh pho
    AppiumLibrary.Page Should Contain Element    ${tt_tk_ha_nam}

BP-2621
    [Documentation]    Check thong tin Thoi tiet man hinh chinh khi them 1 Thanh pho trung
    [Tags]    BP-2621
    Log To Console    Them thong tin Thoi tiet Ha Noi
    Bam nut them thanh pho
    Click Element    ${tt_tk_nhap_ten_thanh_pho}
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Hà Nội
    Sleep    1
    AppiumLibrary.Click Element    ${tt_tk_text_ha_noi}
    Sleep    1
    Log To Console    Giao dien tim kiem thong tin Thoi tiet Thanh pho dong, chuyen sang man hinh chinh voi thong tin thoi tiet nhu cu khong thay doi
    Xac nhan hien thi giao dien man hinh chinh
    Xpath Should Match X Times    ${tt_tk_ha_noi}        0
    Element Attribute Should Match    ${tt_tk_ha_noi}    text    *Hà**Nội*
    Xpath Should Match X Times    ${tt_tk_ha_nam}        0
    Element Attribute Should Match    ${tt_tk_ha_nam}    text    *Hà**Nam*
    Dong toan bo ung dung trong da nhiem

BP-2610
    [Documentation]    Check gia tri nhiet do mac dinh cua ung dung Thoi tiet
    [Tags]    BP-2610    BP-2620
    Mo Thoi tiet tu Launcher
    Log To Console    Check nhiet do tai Ha Noi
    Sleep    2    
    Vuot sang man hinh ben trai
    ${nhiet_do}=    AppiumLibrary.Get Text    ${tt_nhiet_do_mac_dinh}
    Vuot quay ve man hinh Home
    Mo OpenWeather tu Launcher
    Run Keyword If    '${ngon_ngu}'=='en'    Doi don vi nhiet do sang do C trong ung dung OpenWeather
    Log To Console    BP-2610 - Nhiet do thoi tiet Ha Noi hien thi o nhiet do C
    Log To Console    BP-2620 - Thong tin Thoi tiet Thanh pho Ha Noi trung khop voi nguon OpenWeatherMap
    Page Should Contain Text    ${nhiet_do}
    Dong toan bo ung dung trong da nhiem

BP-2611
    [Documentation]    Check chuc nang chuyen nhiet do tu do C sang do F
    [Tags]    BP-2611    BP-2620
    Mo Thoi tiet tu Launcher
    Sleep    20
    Log To Console    Cham vao nut do F
    AppiumLibrary.Click Element    ${tt_nut_do_f}
    Sleep    1
    Log To Console    Check nhiet do tai Ha Noi
    ${nhiet_do}=    AppiumLibrary.Get Text    ${tt_nhiet_do_mac_dinh}
    Vuot quay ve man hinh Home
    Mo OpenWeather tu Launcher
    Run Keyword If    '${ngon_ngu}'!='en'    Doi don vi nhiet do sang do F trong ung dung OpenWeather
    Log To Console    BP-2611 - Nhiet do chuyen tu do C sang do F
    Log To Console    BP-2620 - Thong tin Thoi tiet Thanh pho Ha Noi trung khop voi nguon OpenWeatherMap
    Page Should Contain Text    ${nhiet_do}F
    Vuot quay ve man hinh Home

BP-2612
    [Documentation]    Check chuc nang chuyen nhiet do tu do F sang do C
    [Tags]    BP-2612    BP-2620
    Mo Thoi tiet tu Launcher
    Sleep    2
    Log To Console    Cham vao nut do C
    AppiumLibrary.Click Element    ${tt_nut_do_c}
    Sleep    1
    Log To Console    Check nhiet do tai Ha Noi
    ${nhiet_do}=    AppiumLibrary.Get Text    ${tt_nhiet_do_mac_dinh}
    Vuot quay ve man hinh Home
    Mo OpenWeather tu Launcher
    Doi don vi nhiet do sang do C trong ung dung OpenWeather
    Log To Console    BP-2612 - Nhiet do chuyen tu do F sang do C
    Log To Console    BP-2620 - Thong tin Thoi tiet Thanh pho Ha Noi trung khop voi nguon OpenWeatherMap
    AppiumLibrary.Page Should Contain Text    ${nhiet_do}    C

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================