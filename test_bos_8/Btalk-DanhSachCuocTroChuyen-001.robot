*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/btalk/tn_cuoc_tro_chuyen.robot
Resource    ../page/btalk/tin_nhan.robot
Resource    ../page/btalk/tn_ds_cuoc_tro_chuyen.robot
Suite Setup       Mo Launcher

*** Test Cases ***
BP-1628
    [Documentation]    Check mo danh sach cuoc tro chuyen
    [Tags]    BP-1628    BP-1630    BP-1631    BP-1633     High
    # Log To Console     Check mo danh sach cuoc tro chuyen khi bam vao icon tin nhan tren hotseat    
    # Check mo danh sach cuoc tro chuyen 
    # Log To Console    Check mo danh sach cuoc tro chuyen tu giao dien thoai bang thao tac vuot    
    # Check mo danh sach cuoc tro chuyen tu giao dien thoai bang thao tac vuot
    # Log To Console    Check mo danh sach cuoc tro chuyen tu giao gan day bang thao tac vuot    
    # Check mo danh sach cuoc tro chuyen tu giao gan day bang thao tac vuot
    Log To Console     Check mo danh sach cuoc tro chuyen khi bam vao icon tin nhan tren hotseat    
    Check mo danh sach cuoc tro chuyen 
    Log To Console    Check mo danh sach cuoc tro chuyen tu giao dien thoai bang thao tac vuot    
    Check mo danh sach cuoc tro chuyen tu giao dien thoai bang thao tac vuot
    Log To Console    Check mo danh sach cuoc tro chuyen tu giao gan day bang thao tac vuot    
    Check mo danh sach cuoc tro chuyen tu giao gan day bang thao tac vuot
    Log To Console    Check mo danh sach cuoc tro chuyen tu giao dien danh ba bang thao tac cham vao tab    
    Check mo danh sach cuoc tro chuyen tu giao dien danh ba bang thao tac cham vao tab

BP-1657
    [Documentation]    Check hien thi thong bao khong tim thay tin nhan khi tim khiem tin nhan khong ra kq
    [Tags]    BP-1657     BP-1658    BP-1659
    Log To Console    Check hien thi thong bao khong tim thay tin nhan khi tim khiem tin nhan khong ra kq    
    Check hien thi thong bao khong tim thay tin nhan khi tim khiem tin nhan khong ra kq
    Log To Console    Check truong hop xoa noi dung tin nhan da nhan    
    Check truong hop xoa noi dung tin nhan da nhan
    Log To Console    Check dong giao dien tim kiem tin nhan    
    Check dong giao dien tim kiem tin nhan
    
TN_040104
    [Documentation]    Ckeck mo giao dien cuoc tro chuyen khi cham vao cuoc tro truyen bat ki
    [Tags]    TN_040104
    Log To Console    Ckeck mo giao dien cuoc tro chuyen khi cham vao cuoc tro truyen bat ki    
    Bam vao chi tiet cuoc tro truyen
    Vuot back ve giao dien truoc
    
TN_040204
    [Documentation]    Check bo chon tat ca cuoc tro truyen da chon
    [Tags]    TN_040204
    Log To Console    Check bo chon tat ca cuoc tro truyen da chon
    Check bo chon tat ca cuoc tro truyen da chon
    Bam nut dong toan bo ung dung trong da nhiem
    
TN_040205    
    [Tags]    TN_040205    TN_040206
    Log To Console    Check hien thi luu tru cuoc tro chuyen khi cham vao icon luu tru
    Mo Tin nhan tu Launcher
    Check hien thi luu tru cuoc tro chuyen khi cham vao icon luu tru    
    Log To Console    Check hien thi cuoc tro truyen trong danh sach luu tru   
    Check hien thi cuoc tro truyen trong danh sach luu tru

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================