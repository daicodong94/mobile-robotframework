*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary
   
Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/btalk/cai_dat_dien_thoai.robot
Resource    ../page/cai_dat/cd_common.robot

*** Test Cases ***
BP-4989
    [Documentation]    Check trang thai cua checkbox hien thi tra loi nhanh khi o che do mac dinh
    [Tags]    BP-4989     1sim        BP-4991
    Log To Console     Check trang thai cua checkbox hien thi tra loi nhanh khi o che do mac dinh    
    Mo cai dat dien thoai va vao tra loi nhanh
    Element Attribute Should Match    ${dt_cd_nut_checkbox_tra_loi_nhanh}    checked    true 
    Log To Console    Check trang thai checkbox khi tat Hien thi tra loi nhanh       
    An checkbox Hien thi tra loi nhanh
    Element Attribute Should Match    ${dt_cd_nut_checkbox_tra_loi_nhanh}    checked    false 
    Close Application
    
BP-4995   
    [Documentation]     Check trang thai checkbox mac dinh tu dong ghi am dien thoai
    [Tags]       BP-4995    1sim        BP-4998    BP-5001
    Log To Console      Check trang thai checkbox mac dinh tu dong ghi am dien thoai    
    Mo cai dat dien thoai va vao Tai khoan goi
    Element Attribute Should Match     ${dt_cd_checkbox_tu_dong_ghi_am_cuoc_goi}       checked    false
    Log To Console     Check trang thai cua checkbox khi Bat chuc nang Tu dong ghi am cuoc goi    
    An nut Tu dong ghi am cuoc goi
    Element Attribute Should Match     ${dt_cd_checkbox_tu_dong_ghi_am_cuoc_goi}       checked    true
    Log To Console        Check trang thai checkbox khi Tat chuc nang tu dong ghi am cuoc goi
    An nut Tu dong ghi am cuoc goi
    Element Attribute Should Match     ${dt_cd_checkbox_tu_dong_ghi_am_cuoc_goi}       checked    false
    Close Application
    
BP-5004
    [Documentation]    Check trang thai cua checkbox Hinh nen cuoc goi khi o trang thai mac dinh
    [Tags]    BP-5004    BP-5006    BP-5008    1sim     
    Log To Console     Check trang thai cua checkbox Hinh nen cuoc goi khi o trang thai mac dinh
    Mo cai dat dien thoai va vao Tai khoan goi
    Element Attribute Should Match    ${dt_cd_checkbox_Hinh_nen_cuoc_goi}    checked    true         
    Log To Console     Check trang thai cua checkbox khi Tat chuc nang Hinh nen cuoc goi    
    An nut Hinh nen cuoc goi
    Element Attribute Should Match    ${dt_cd_checkbox_Hinh_nen_cuoc_goi}    checked    false         
    Log To Console     Check trang thai cua checkbox khi Bat chuc nang Hinh nen cuoc goi   
    An nut Hinh nen cuoc goi   
    Element Attribute Should Match    ${dt_cd_checkbox_Hinh_nen_cuoc_goi}    checked    true         
    Close Application   
    
BP-5010
    [Documentation]    Check trang thai cua checkbox hien thi nha mang khi o trang thai mac dinh
    [Tags]    BP-5010    BP-5012    BP-5014    1sim     
    Log To Console     Check trang thai cua checkbox hien thi nha mang khi o trang thai mac dinh    
    Mo cai dat dien thoai va vao Tai khoan goi
    Element Attribute Should Match    ${dt_cd_Hien_thi_thong_tin_nha_mang_trong_cuoc_goi}    checked    false         
    Log To Console    Check trang thai cua checkbox khi bat chuc nang Hien thi thong tin nha mang trong cuoc goi    
    An nut Hien thi thong tin nha mang trong cuoc goi   
    Element Attribute Should Match    ${dt_cd_Hien_thi_thong_tin_nha_mang_trong_cuoc_goi}     checked    true         
    Log To Console     Check trang thai cua Checkbox khi tat chuc nang Hien thi thong tin nha mang trong cuoc goi    
    An nut Hien thi thong tin nha mang trong cuoc goi   
    Element Attribute Should Match    ${dt_cd_Hien_thi_thong_tin_nha_mang_trong_cuoc_goi}     checked    false         
    Close Application 
    
BP-5016
    [Documentation]    Check mo giao dien cai dat Hen gio khi cham vao Hen gio cai dat
    [Tags]    BP-5016     BP-5017      1sim
    Log To Console     Check mo giao dien cai dat Hen gio khi cham vao Hen gio cai dat
    Mo cai dat dien thoai va vao Tai khoan goi
    An nut hen gio cuoc goi
    Page Should Contain Text    Hẹn giờ cuộc gọi       
    Log To Console     Check mo giao dien cai dat Hen gio cuoc goi
    An nut dat thoi gian      
    Element Attribute Should Match    ${dt_cd_Title_popup_dat_thoi_gian}    text    Thời gian (phút:giây)10:00  
    Sleep    2          
    Element Attribute Should Match    ${dt_cd_dat_thoi_gian_input_gio}    text    	10   
    Sleep    2     
    Element Attribute Should Match    ${dt_cd_dat_thoi_gian_input_phut}    text    	0
    Page Should Contain Element    ${dt_cd_nut_dat_thoi_gian_trong_popup_dat_thoi_gian}    
    Page Should Contain Element    ${dt_cd_nut_huy}    
    Close Application
    
BP-5019
    [Documentation]    Check trang thai cua checkbox Tat cuoc goi tu dong khi o trang thai mac dinh
    [Tags]    BP-5019    BP-5021     BP-5023    1sim
    Log To Console     Check trang thai cua checkbox Tat cuoc goi tu dong khi o trang thai mac dinh    
    Mo cai dat dien thoai va vao Tai khoan goi
    An nut hen gio cuoc goi
    Element Attribute Should Match     ${dt_cd_checkbox_tat_cuoc_goi_tu_dong}   checked    false    
    Log To Console     Check trang thai cua checkbox Tat cuoc goi tu dong khi Bat    
    An nut Tat cuoc goi tu dong
    Element Attribute Should Match     ${dt_cd_checkbox_tat_cuoc_goi_tu_dong}   checked    true
    Log To Console     Check trang thai checkbox Tat cuoc goi thu dong khi Tat    
    An nut Tat cuoc goi tu dong
    Element Attribute Should Match     ${dt_cd_checkbox_tat_cuoc_goi_tu_dong}   checked    false    
    Close Application
    
BP-5026
    [Documentation]    Check trang thai cua checkbox Rung thong bao truoc 30s khi o trang thai mac dinh
    [Tags]    BP-5026     BP-5028    BP-5030    1sim
    Log To Console     Check trang thai cua checkbox Rung thong bao truoc 30s khi o trang thai mac dinh    
    Mo cai dat dien thoai va vao Tai khoan goi
    An nut hen gio cuoc goi
    Element Attribute Should Match     ${dt_cd_checkbox_rung_thong_bao_truoc_30s}   checked    false    
    Log To Console     Check trang thai cua checkbox Rung thong bao truoc 30s  khi Bat    
    An nut Rung thong bao truoc 30s
    Element Attribute Should Match     ${dt_cd_checkbox_rung_thong_bao_truoc_30s}   checked    true
    Log To Console     Check trang thai cua checkbox Rung thong bao truoc 30s khi Tat    
    An nut Rung thong bao truoc 30s
    Element Attribute Should Match    ${dt_cd_checkbox_rung_thong_bao_truoc_30s}   checked    false    
    Close Application
   
BP-5038
    [Documentation]    Check trang thai cua checkbox Ap dung cho cuoc goi den khi o trang thai mac dinh
    [Tags]    BP-5038       BP-5042    BP-5040     1sim
    Log To Console     Check trang thai cua checkbox Ap dung cho cuoc goi den khi o trang thai mac dinh    
    Mo cai dat dien thoai va vao Tai khoan goi
    An nut hen gio cuoc goi 
    Element Attribute Should Match     ${dt_cd_checkbox_ap_dung_cho_ca_cuoc_goi_den}   checked    false    
    Log To Console     Check trang thai cua checkbox Ap dung cho cuoc goi den khi bat    
    An nut Ap dung cho cuoc goi den
    Element Attribute Should Match    ${dt_cd_checkbox_ap_dung_cho_ca_cuoc_goi_den}    checked    true  
    Log To Console     Check trang thai cua checkbox Ap dung cho  cuoc goi den khi tat 
    An nut Ap dung cho cuoc goi den
    Element Attribute Should Match    ${dt_cd_checkbox_ap_dung_cho_ca_cuoc_goi_den}    checked    false     
    
BP-5044
    [Documentation]    Check mo popup Thoi gian tu dong xoa ghi am cuoc goi khi cham vao Thoi gian tu dong xoa ghi am cuoc goi
    [Tags]    BP-5044    1sim
    Log To Console     Check mo popup Thoi gian tu dong xoa ghi am cuoc goi khi cham vao Thoi gian tu dong ghi am cuoc goi    
    Mo cai dat dien thoai va vao Tai khoan goi
    An nut Thoi gian tu dong xoa ghi am cuoc goi
    Page Should Contain Text    Thời gian tự động xóa ghi âm cuộc gọi  
    Element Attribute Should Match    ${dt_cd_nut_tat_thoi_gian_tu_dong_xoa_ghi_am_cuoc_goi}    checked    true     
    Page Should Contain Element       ${dt_cd_nut_huy}
    Element Should Be Enabled    ${dt_cd_nut_huy}    
    Close Application
     
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
