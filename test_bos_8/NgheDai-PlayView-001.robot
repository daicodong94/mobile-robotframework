*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
ThinhLQ         | 2021/07/23 | update code , tach nho case rieng biet
----------------|------------|--------------------------------------------------------------------
*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/nghe_dai/yeu_thich.robot

Suite Setup       Mo Nghe dai
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu
*** Test Cases ***
BP-2454
    [Documentation]    Check mo ung dung
    [Tags]    High    BP-2454
    Cho phep cap quyen cho ung dung
    Sleep    5
    Kiem tra nut play co hoat dong khi bat dau mo app
    Xac nhan mo app Nghe dai thanh cong

BP-2523
    [Documentation]    Check chuc nang stop
    [Tags]    High    BP-2523
    Thuc hien stop kenh dang chay
    Page Should Not Contain Element    ${nd_yt_txt_dang_phat}
    Sleep    2

BP-2524
    [Documentation]    Check chuc nang play
    [Tags]    High    BP-2524
    Thuc hien play kenh dang dung
    Sleep    2
    Page Should Not Contain Element    ${nd_yt_txt_ngung_phat}

BP-2521
    [Documentation]    Check chuc nang thanh button player view khi khong co mang
    [Tags]    BP-2521
    Mang di dong = 1, data = 0, wifi = 0, may bay = 0
    Kiem tra nut play co hoat dong khi bat dau mo app
    Page Should Not Contain Element    ${nd_yt_txt_dang_phat}
    Page Should Contain Element    ${nd_yt_txt_khong_co_ket_noi}

BP-2522
    [Documentation]    Check chuc nang thanh button player view khi co mang
    [Tags]    High    BP-2522
    Mang di dong = 1, data = 1, wifi = 1, may bay = 0
    Kiem tra nut play co hoat dong khi bat dau mo app
    Thuc hien kiem tra kenh vov3 dang duoc phat
    Page Should Contain Element    ${nd_yt_txt_dang_phat}

BP-2525
    [Documentation]    Check chuc nang bat thoi gian hen gio
    [Tags]    BP-2525     BP-2526    BP-2528
    Thuc hien click vao icon hen gio
    Thuc hien kiem tra hien thi notify hen gio

BP-2526
    [Documentation]    Check chuc nang tat thoi gian hen gio
    [Tags]    BP-2526
    Thuc hien huy bo hen gio
    Thuc hien kiem tra khong co notify hen gio

BP-2528
    [Documentation]    Check chuc nang HUY thoi gian hen gio
    [Tags]    BP-2528
    Thuc hien click vao icon hen gio
    Swipe By Percent    50    70    50    55
    Sleep    2
    Click Element    ${nut_huy_hoa}
    Sleep    1
    Page Should Not Contain Element    ${nut_huy_hoa}
    Page Should Not Contain Element    ${nd_yt_btn_Xong_hen_gio}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
