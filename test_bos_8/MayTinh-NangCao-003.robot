*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/may_tinh/mt_common.robot
Resource    ../page/may_tinh/nang_cao.robot

Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-1053
    [Documentation]    Check chuc nang DEG voi phep tinh tru ki hieu sin
    [Tags]    BP-1053
    Log To Console    Check chuc nang DEG voi phep tinh tru ki hieu sin
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    1
    An nut RAD
    Sleep    2
    Thuc hien va kiem tra phep tinh tru sin
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Should Be Equal    ${get_text}    0
    Close Application

BP-1056
    [Documentation]    Check chuc nang RAD voi phep tinh tru ki hieu sin
    [Tags]    BP-1056
    Log To Console    Check chuc nang RAD voi phep tinh tru ki hieu sin
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh tru sin
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Should Be Equal    ${get_text}    0
    Close Application

BP-1054
    [Documentation]    Check chuc nang DEG voi phep tinh tru ki hieu cos
    [Tags]    BP-1054
    Log To Console     Check chuc nang DEG voi phep tinh tru ki hieu cos
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut RAD
    Sleep    2
    Thuc hien va kiem tra phep tinh tru cos
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Should Be Equal    ${get_text}    0
    Close Application

BP-1059
    [Documentation]    Check chuc nang RAD voi phep tinh tru ki hieu cos
    [Tags]    BP-1059
    Log To Console    Check chuc nang RAD voi phep tinh tru ki hieu cos
    Sleep    2
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh tru cos
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Should Be Equal    ${get_text}    0
    Close Application

BP-1055
    [Documentation]    Check chuc nang DEG voi phep tinh tru ki hieu tan
    [Tags]    BP-1055
    Log To Console    Check chuc nang DEG voi phep tinh tru ki hieu tan
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    An nut RAD
    Sleep    1
    Thuc hien va kiem tra phep tinh tru tan
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Should Be Equal    ${get_text}    0
    Close Application

BP-1062
    [Documentation]    Check chuc nang RAD voi phep tinh tru ki hieu tan
    [Tags]    BP-1062
    Log To Console    Check chuc nang RAD voi phep tinh tru ki hieu tan
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh tru tan
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Should Be Equal    ${get_text}    0
    Close Application

BP-1066
    [Documentation]    Thuc hien va kiem tra phep tinh tru ln
    [Tags]    BP-1066
    Log To Console    Thuc hien va kiem tra phep tinh tru ln
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh tru ln
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Element Attribute Should Match    ${mt_hien_thi_ket_qua}    text    *0*
    Close Application

BP-1070
    [Documentation]    Check phep tinh tru ki hieu log
    [Tags]    BP-1070
    Log To Console    Check phep tinh tru ki hieu log
    Mo May tinh
    Sleep    2
    An nut man hinh nang cao
    Sleep    2
    Thuc hien va kiem tra phep tinh tru log
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${mt_hien_thi_ket_qua}
    Should Be Equal    ${get_text}    0

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================