*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/ghi_am/ga_common.robot
Resource    ../page/notification/notification.robot

Suite Setup        Mo Ghi am
Suite Teardown     Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-3930
    [Documentation]    Kiem tra mo app Ghi am qua Launcher
    [Tags]     High    BP-3930
    Cho phep cap quyen cho ung dung
    Vuot quay ve man hinh Home
    Mo Ghi am tu Launcher

BP-3946
    [Documentation]    Kiem tra giao dien khi chuyen sang che do Dang ghi am
    [Tags]    High    BP-3946
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Chup anh man hinh va so sanh voi anh mau    ${ga_alias}    kc_d_ttdh_d_d_Dang_ghi_am    2

BP-3973
    [Documentation]    Kiem tra hien thi popup Ghi ra file khi cham button Dung
    [Tags]    High    BP-3973    BP-3976    BP-4350
    An nut danh sach ban ghi
    Log To Console    BP-3976    Kiem tra luu file khi bam DONG Y tren popup Ghi ra file
    An nut dong y
    Log To Console    BP-4350    Kiem tra hien thi cua screen Danh sach ban ghi khi da luu file
    Chup anh man hinh va so sanh voi anh mau    ${ga_alias}    kc_d_ttdh_d_d_Ds_ban_ghi_data    6
    Vuot back ve giao dien truoc
    Vuot quay ve man hinh Home
    Xoa toan bo du lieu app ghi am trong quan ly file

BP-3932
    [Documentation]      Kiem tra mo app Ghi am qua Multitask
    [Tags]    High    BP-3932    BP-3935    BP-3937
    Mo Ghi am tu Launcher
    Mo da nhiem
    Mo ghi am tu da nhiem
    Log To Console    BP-3935     Kiem tra hien thi day du cac thong tin trong screen Ghi am
    Log To Console    BP-3937     Kiem tra tong the giao dien man hinh ghi am
    Log To Console    BP-3938     Kiem tra khi doi sang ngon ngu Tieng Viet
    Chup anh man hinh va so sanh voi anh mau    ${ga_alias}    kc_d_ttdh_d_d_main    2

BP-3960
    [Documentation]    Kiem tra tam dung ghi am khi an button Tam dung
    [Tags]    BP-3960    BP-3964    BP-3966    BP-3968
    An nut bat dau ghi am
    An nut tam dung ghi am
    Page Should Contain Text    ${ga_text_tam_dung}
    Log To Console    BP-3964    Kiem tra tiep tuc ghi am sau khi tam dung
    An nut bat dau ghi am
    Page Should Contain Text    ${ga_text_dang_ghi_am}
    Log To Console    BP-3966         Kiem tra chuc nang tam dung ghi am khi an tam dung o notify
    Vuot mo bang thong bao
    Click Element    ${ga_thongbao_tam_dung}
    Sleep    2
    Page Should Contain Text    ${ga_text_tam_dung}
    Log To Console    BP-3968    Kiem tra chuc nang dung ghi am khi an dung o notify
    An nut bat dau ghi am
    Vuot mo bang thong bao
    Click Element    ${ga_thongbao_dung}
    Page Should Contain Text    ${ga_text_luu_file_ga_thanh_cong}
    Vuot back ve giao dien truoc

BP-3969
    [Documentation]     Kiem tra back 1 lan khi dang ghi am
    [Tags]    BP-3969    BP-3971    BP-3972
    An nut bat dau ghi am
    Swipe By Percent    0    50    10    50
    Sleep    2
    Page Should Contain Text    ${ga_text_dang_ghi_am}
    Log To Console    BP-3972    Kiem tra back 2 lan khong lien tiep khi dang ghi am
    Swipe By Percent    0    50    10    50
    Sleep    4
    Swipe By Percent    0    50    10    50
    Page Should Contain Text    ${ga_text_dang_ghi_am}
    Log To Console    BP-3971    Kiem tra back 2 lan lien tiep khi dang ghi am
    Swipe By Percent    0    50    10    50
    Swipe By Percent    0    50    10    50
    # Vuot sang man hinh ben phai
    Page Should Contain Element    ${app_ghi_am}
    Sleep    2

BP-3978
    [Documentation]     Kiem tra dong popup, khong luu file khi bam HUY BO tren popup Ghi ra file
    [Tags]    BP-3978
    Mo Ghi am tu Launcher
    An nut bat dau ghi am
    An nut danh sach ban ghi
    Sleep    2
    Click Element    ${dt_cd_nut_huy_bo}
    Sleep    2
    Page Should Contain Text    ${ga_text_chuan_bi_ghi_am}

BP-3980
    [Documentation]    Kiem tra luu file khong hoi lai khi tick Khong hoi lai tren popup Ghi ra file
    [Tags]    BP-3980
    An nut bat dau ghi am
    An nut danh sach ban ghi
    Click Element    ${ga_checkbox_khong_hoi_lai}
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    Vuot back ve giao dien truoc
    Sleep    2
    An nut bat dau ghi am
    Click Element At Coordinates    ${ga_nut_danh_sach_ban_ghi_x}    ${ga_nut_danh_sach_ban_ghi_y}
    Sleep    2
    Page Should Contain Text    ${ga_tieu_de_ds_ban_ghi}
    Vuot back ve giao dien truoc
    An nut tuy chon
    Click Element    ${ga_cai_dat_tu_dat_ten_file}
    Vuot back ve giao dien truoc

BP-3985
    [Documentation]    Kiem tra luu file khi nhap gia tri space/bo trong textbox Ghi ra file
    [Tags]    BP-3985
    An nut bat dau ghi am
    Sleep    10
    An nut danh sach ban ghi
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Click Element    ${nut_dong_y_hoa}
    Page Should Contain Text    ${ga_text_dat_ten_trong}
    Sleep    2
    Vuot back ve giao dien truoc

BP-3987
    [Documentation]    Kiem tra luu file khi nhap chu thuong vao textbox Ghi ra file
    [Tags]    BP-3987    BP-3998
    An nut bat dau ghi am
    Click Element At Coordinates    ${ga_nut_danh_sach_ban_ghi_x}    ${ga_nut_danh_sach_ban_ghi_y}
    Log To Console    BP-3998    Kiem tra luu file khac ten mac dinh tai popup Ghi ra file
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}         abcd
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    ${get_text_thuong}=    AppiumLibrary.Get Text    ${ga_verify_text_thuong}
    Should Be Equal    ${get_text_thuong}    abcd
    Sleep    2
    Vuot back ve giao dien truoc
    Vuot quay ve man hinh Home
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-3933
    [Documentation]    Kiem tra mo app Ghi am tu giao dien Dashboard
    [Tags]    High    BP-3933
    Bat hien thi ghi am len dashboard
    Cho phep cap quyen cho ung dung
    Page Should Contain Element    ${ga_tieu_de}
    Page Should Contain Element    ${app_ghi_am}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================