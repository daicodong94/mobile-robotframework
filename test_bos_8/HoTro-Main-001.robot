*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/ho_tro/ht_common.robot

Suite Setup       Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-5151
    [Documentation]    Check giao dien app ho tro tong the
    [Tags]    High    BP-5151    BP-5327    BP-5328    BP-5330    BP-5331    BP-5335    BP-5360    BP-5361
    Log To Console    Mo giao dien tu launcher
    Mo Ho tro tu Launcher
    Mo da nhiem
    Click Element At Coordinates    550    880
    Sleep    1
    Click 7 lan vao avatar dong & mo tuy chon
    Click vao tuy chon cai dat
    Click vao tab xuat trang thai may
    Vuot mo bang thong bao
    Kiem tra hien thi text dang tao loi 1
    Sleep    2
    Vuot dong bang thong bao
    Close Application
    Mo Ho tro
    Check kiem tra giao dien man hinh tong the tu launcher

BP-5334
    [Documentation]    Kiem tra hien thi so dien thoai ho tro
    [Tags]    BP-5334
    Dong toan bo ung dung va ket thuc phien kiem thu
    Click vao tab icon goi dien
    Cho phep chon trinh duyet chim lac & luon chon
    Cho phep luon chon trinh duyet chim lac
    Bat che do thu gon trong chim lac
    Kiem tra hien thi so dien thoai ho tro
    Go Back
    Sleep    2

BP-5354
    [Documentation]    Kiem tra link sang giao dien web bphone.vn/bphoto khi touch button Bphoto
    [Tags]    BP-5354
    Click vao tab icon bphoto
    Cho phep chon trinh duyet chim lac & luon chon
    Cho phep luon chon trinh duyet chim lac
    Bat che do thu gon trong chim lac
    check hien thi link Bphoto tai chim lac
    Go Back
    Sleep    2

BP-5355
    [Documentation]    Kiem tra link sang giao dien web bphone.vn/san-pham#phukien khi touch button phu kien
    [Tags]    BP-5354
    Click vao tab icon phu kien
    Cho phep chon trinh duyet chim lac & luon chon
    Cho phep luon chon trinh duyet chim lac
    Bat che do thu gon trong chim lac
    check hien thi text phu kien op lung
    Go Back
    Sleep    2

BP-5357
    [Documentation]    Kiem  tra link sang giao dien web bphone.vn/bphone-stores khi touch button Bstore
    [Tags]    BP-5354
    Click vao tab icon Bstore
    Cho phep chon trinh duyet chim lac & luon chon
    Cho phep luon chon trinh duyet chim lac
    Bat che do thu gon trong chim lac
    check hien thi link Bstore tai chim lac
    Go Back
    Sleep    2

BP-5152
    [Documentation]    Kiem  tra link sang giao dien web bphone.vn/bphone-stores khi touch button Bstore
    [Tags]    BP-5152    BP-5153    BP-5154    BP-5364    BP-5366    BP-5367
    Click vao tuy chon cai dat
    Click vao tab xuat trang thai may
    Vuot mo bang thong bao
    Kiem tra hien thi text dang tao loi 1
    Sleep    2
    Vuot dong bang thong bao
    Click vao tuy chon cai dat
    Click vao tab xuat trang thai may
    Vuot mo bang thong bao
    Kiem tra hien thi text dang tao loi 2
    Sleep    2
    Vuot dong bang thong bao
    Click vao tuy chon cai dat
    Click vao tab cap nhat he thong offline
    Click vao dong thoat cap nhat offline
    Kiem tra khong hien thi quan ly files
    Click vao tuy chon cai dat
    Click vao tab cap nhat he thong offline
    Click vao dong y cap nhat offline
    Kiem tra hien thi vi tri quan ly files
    Close Application

BP-5370
    [Documentation]    Kiem tra chuc nang ghi nhat ky & xoa nhat ky
    [Tags]    BP-5370    BP-5372    BP-5374
    Mo Ho tro
    Sleep    1
    Click 7 lan vao avatar dong & mo tuy chon
    Click vao tuy chon cai dat
    Click vao tab nhat ky QXDM
    Click vao btn bat dau chay diag_logs
    Sleep    3
    Click vao btn dung lai chay diag_logs
    Go Back
    Go Back
    Mo Quan ly file tu Launcher
    Click toa do vao tab tat ca
    Kiem tra hien thi diag_logs tren quan ly file
    Go Back
    Go Back
    Mo Ho tro tu Launcher
    Click vao tuy chon cai dat
    Click vao tab nhat ky QXDM
    Click vao tuy chon cai dat
    Click vao btn dung xoa nhat ky
    Go Back
    Go Back
    Mo Quan ly file tu Launcher
    Click toa do vao tab tat ca
    Kiem tra khong hien thi diag_logs
    Close Application

BP-5359
    [Documentation]    kiem tra khong hien thi chuc nang tuy chon khi cham vao avatar co gai
    [Tags]    BP-5359    BP-5376    BP-5379    BP-5381    BP-5384
    Mo Ho tro
    Sleep    1
    Click 6 lan vao avatar check khong hien thi icon tuy chon
    Click vao tuy chon cai dat
    Kiem tra hien thi check box bao loi
    Kiem tra hien thi check thong bao bkav
    Click vao btn dung bao cao loi
    Click vao tuy chon cai dat
    Kiem tra khong hien thi check box bao loi
    Click vao btn thong bao loi bkav
    Click vao tuy chon cai dat
    Kiem tra khong hien thi check thong bao bkav
    Close Application

BP-5386
    [Documentation]    kiem tra khong bi crash khi cham lien tuc vao avatar
    [Tags]    BP-5386
    Mo Ho tro
    Click lien tuc vao avatar kiem tra crash

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================