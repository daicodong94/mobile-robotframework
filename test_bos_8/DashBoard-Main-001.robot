*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary   

Resource    ../page/common.robot
Resource    ../page/dashboard/db_common.robot
Resource    ../page/config/mac_dinh.robot

Suite Setup            Mo Cai dat
Suite Teardown         Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
DB_010101
    [Documentation]    Kiem tra mo Dashboard khi vuot tu 2/3 man hinh tinh tu Hotseat len
    [Tags]    High    DB_010101    DB_020101    DB_030301    DB_030701    DB_040202
    Log To Console    DB_030301    Kiem tra mo Dashboard tu man hinh chinh    
    Hien thi tat ca cac chuc nang len dashboard
    Vuot quay ve man hinh Home
    Vuot mo bang dieu khien
    Chup anh man hinh va so sanh voi anh mau    dashboard    kc_d_ttdh_d_d_db_main    5
    Log To Console    DB_020101    Kiem tra mo Dashboard khi vuot tu 2/3 man hinh tinh tu Hotseat len    
    Log To Console    DB_030701    Kiem tra mo Dashboard tu lockscreen    
    AppiumLibrary.Lock    
    Vuot mo bang dieu khien
    Chup anh man hinh va so sanh voi anh mau    dashboard    kc_d_ttdh_d_d_db_lockscreen    10
    Log To Console    DB_040202    Kiem tra hanh dong dong Dashboard khi Dashboard dang hien thi o man hinh cho
    Click Element At Coordinates    ${db_khoang_trong_x}    ${db_khoang_trong_y}        
    Mo khoa voi mat khau 8888
    
DB_030501
    [Documentation]    Kiem tra mo Dashboard tu giao dien bat ky
    [Tags]    DB_030501    DB_030101    
    Mo Tin nhan tu Launcher
    Sleep    2    
    Vuot mo bang dieu khien
    Vuot back ve giao dien truoc 
    Log To Console    DB_030101    Kiem tra khong cho mo Dashboard khi dang o giao dien hien thi ban phim    
    Click Element    ${logo_tn}
    Sleep    2   
    Vuot mo bang dieu khien
    Page Should Contain Element    ${tn_themmoi_nhap_sdt_gui}
    Vuot quay ve man hinh Home
    
DB_040102
    [Documentation]    Kiem tra dong Dashboard khi cham va giu vao icon Che do rieng tu khi chua bat che do rieng tu
    [Tags]    DB_040102    DB_040104    DB_040105    DB_040106
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_che_do_rieng_tu_x}    ${db_nut_che_do_rieng_tu_y}    duration=3000
    Sleep    2    
    Page Should Contain Element    ${db_nut_hoan_thanh}    
    Vuot back ve giao dien truoc
    Log To Console    DB_040104    Kiem tra dong Dashboard khi cham va giu vao icon Anh sang dem    
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_che_do_dem_x}    ${db_nut_che_do_dem_y}    duration=2000
    Sleep    2    
    Page Should Contain Element    ${cdd_bat_che_do_dem}   
    Log To Console    DB_040105    Kiem tra dong Dashboard khi cham va giu vao icon Chuong
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_chuong_x}    ${db_nut_chuong_y}    duration=2000
    Sleep    2    
    Page Should Contain Element    ${cd_am_thanh_tieu_de}   
    Log To Console    DB_040106    Kiem tra dong Dashboard khi cham va giu vao icon Cai dat
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_cai_dat_x}    ${db_nut_cai_dat_y}    duration=2000
    Sleep    2    
    Page Should Contain Element    ${app_cai_dat}    
    
DB_040107
    [Documentation]    Kiem tra dong Dashboard khi cham va giu vao icon Che do may bay
    [Tags]      DB_040107    DB_040108    DB_040109    DB_040110
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_cd_may_bay_x}    ${db_nut_cd_may_bay_y}    duration=2000
    Sleep    2
    Page Should Contain Element    ${wifi_text_mang_va_internet}    
    Log To Console    DB_040108    Kiem tra dong Dashboard khi cham va giu vao icon Vi tri
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_vi_tri_x}    ${db_nut_vi_tri_y}    duration=2000
    Sleep    2    
    Page Should Contain Element    ${cd_tieu_de_vi_tri}    
    Log To Console    DB_040109    Kiem tra dong Dashboard khi cham va giu vao icon Game mode
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_game_mode_x}    ${db_nut_game_mode_y}    duration=2000
    Sleep    2    
    Page Should Contain Text    Game Mode
    Log To Console    DB_040110    Kiem tra dong Dashboard khi cham va giu vao icon Xoay ngang
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_xoay_ngang_x}    ${db_nut_xoay_ngang_y}    duration=2000
    Sleep    2    
    Page Should Contain Element    ${cdd_text_hien_thi}  
    
DB_040111
    [Documentation]    Kiem tra dong Dashboard khi cham va giu vao icon Wifi
    [Tags]    DB_040111    DB_040112    DB_040113    DB_040114
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_wifi_x}    ${db_nut_wifi_y}    duration=2000
    Sleep    2    
    Page Should Contain Text    Wi-Fi
    Log To Console    DB_040112    Kiem tra dong Dashboard khi cham va giu vao icon Bluetooth
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_bluetooth_x}    ${db_nut_bluetooth_y}    duration=2000
    Sleep    2    
    Page Should Contain Text    Bluetooth    
    Log To Console    DB_040113    Kiem tra dong Dashboard khi cham va giu vao icon Hotspot
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_hotspot_x}    ${db_nut_hotspot_y}    duration=2000
    Sleep    2 
    Page Should Contain Element    ${wifi_text_diem_phat_song_va_chia_se_kn}    
    Log To Console    DB_040114    Kiem tra dong Dashboard khi cham va giu vao icon Du lieu di dong    
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_du_lieu_x}    ${db_nut_du_lieu_y}    duration=2000
    Sleep    2    
    Page Should Contain Element    ${db_title_the_sim}    
    
DB_040115
    [Documentation]    Kiem tra dong Dashboard khi cham va giu vao icon Truyen man hinh
    [Tags]    DB_040115    DB_040116    DB_040117    DB_040118
    Vuot mo bang dieu khien
    Vuot mo phan trang dashboard ben phai       
    Click A Point    ${db_nut_truyen_man_hinh_x}    ${db_nut_truyen_man_hinh_y}    duration=2000
    Sleep    2    
    Page Should Contain Element    ${cd_tieu_de_truyen}    
    Log To Console    DB_040116    Kiem tra dong Dashboard khi cham va giu vao icon Tiet kiem pin
    Vuot mo bang dieu khien
    Vuot mo phan trang dashboard ben phai
    Click A Point    ${db_nut_tiet_kiem_pin_x}    ${db_nut_tiet_kiem_pin_y}    duration=2000
    Sleep    2    
    Page Should Contain Element    ${cd_tieu_de_pin}    
    Log To Console    DB_040117    Kiem tra dong Dashboard khi cham va giu vao icon Nguoi dung
    Vuot mo bang dieu khien
    Vuot mo phan trang dashboard ben phai
    Click A Point    ${db_nut_nguoi_dung_x}    ${db_nut_nguoi_dung_y}    duration=2000
    Sleep    2    
    Page Should Contain Element    ${cd_tieu_de_nguoi_dung}    
    Log To Console    DB_040118    Kiem tra dong Dashboard khi cham giu vao icon Khong lam phien
    Vuot mo bang dieu khien
    Vuot mo phan trang dashboard ben phai
    Click A Point    ${db_nut_khong_lam_phien_x}    ${db_nut_khong_lam_phien_y}    duration=2000
    Sleep    2    
    Page Should Contain Element    ${cd_tieu_de_klp}
    
DB_040120
    [Documentation]    Kiem tra dong Dashboard khi cham va giu vao icon Bao thuc
    [Tags]    DB_040120    DB_040121    DB_040122    DB_040124    DB_040125
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_bao_thuc_x}    ${db_nut_bao_thuc_y}    duration=2000
    Cho phep cap quyen cho ung dung
    Sleep    2 
    Page Should Contain Element    ${dh_tab_bao_thuc}   
    Log To Console    DB_040121    Kiem tra dong Dashboard khi cham va giu vao icon May tinh    
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_may_tinh_x}    ${db_nut_may_tinh_y}    duration=2000
    Cho phep cap quyen cho ung dung
    Sleep    2    
    Page Should Contain Element    ${mt_nut_lich_su}    
    Log To Console    DB_040122    Kiem tra dong Dashboard khi cham va giu vao icon Ghi chep
    Vuot mo bang dieu khien
    Click A Point    ${db_nut_ghi_chep_x}    ${db_nut_ghi_chep_y}    duration=2000
    Cho phep cap quyen cho ung dung
    Sleep    2    
    Page Should Contain Element    ${nut_menu_trai}    
    Log To Console    DB_040124    Kiem tra dong Dashboard khi cham va giu vao icon QR code
    Vuot mo bang dieu khien
    Vuot mo phan trang dashboard ben phai
    Click A Point    ${db_nut_quet_qr_code_x}    ${db_nut_quet_qr_code_y}    duration=2000
    Sleep    2    
    Page Should Contain Element    ${db_qr_code_huong_dan}  
    Log To Console    DB_040125    Kiem tra dong Dashboard khi cham va giu vao icon Nhac viec
    Vuot mo bang dieu khien
    Vuot mo phan trang dashboard ben phai
    Vuot mo phan trang dashboard ben phai
    Click A Point    ${db_nut_nhac_viec_x}    ${db_nut_nhac_viec_y}    duration=2000
    Cho phep cap quyen cho ung dung
    Sleep    2    
    Page Should Contain Element    ${nut_menu_trai}    
    
        
    
    
    
                  
    
            
        
    
     
       
    
    
        
    










#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================