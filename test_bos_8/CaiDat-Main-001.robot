*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/cai_dat/cd_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/launcher/launcher_common.robot

*** Test Cases ***
BP-3625
    [Documentation]    Kiem tra giao dien khi nhap ma pin
    [Tags]    High    BP-3625    BP-3626    BP-5777
    Mo Cai dat
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    ${bphone}_kc_d_ttdh_d_d_cd_mang_internet    4
    Click toa do vao cai dat tai dashboard
    Chup anh man hinh va so sanh voi anh mau    ${cd_alias}    ${bphone}_kc_d_ttdh_d_d_cd_mang_internet    4
    Click tab mo mang va internet
    Click vao tab wifi trong cai dat
    Click vao tat wifi trong cai dat
    Click vao bat wifi trong cai dat
    cho ket noi wifi
    Close Application

BP-5778
    [Documentation]    Kiem tra giao dien khi nhap ma pin
    [Tags]    High    BP-5778    BP-5779    BP-5780    BP-5781    BP-5785
    Mo Cai dat
    Vuot mo bang dieu khien
    Cham giu toa do bat wifi vao trong cai dat
    Click vao tat wifi trong cai dat
    Check kiem tra nut bat wifi dang khong hoat dong
    Click vao bat wifi trong cai dat
    Check kiem tra nut bat wifi dang hoat dong
    cho ket noi wifi
    Close Application

BP-5784
    [Documentation]    Kiem tra giao dien khi nhap ma pin
    [Tags]    High    BP-5784    BP-5782   BP-5783
    Mo Cai dat
    Vuot mo bang dieu khien
    Cham giu toa do bat wifi vao trong cai dat
    cho hien thi danh sach wifi
    Input password ket noi wifi
    Click checkbox hien thi password kieu du lieu text
    Kiem tra hien thi dung du lieu da nhap trong password
    Click vao huy ket noi wifi
    Kiem tra da an pop up mat khau
    Click vao ten wifi muon ket noi trong cai dat
    Input password ket noi wifi
    Click vao dong y ket noi wifi
    Kiem tra da an pop up mat khau
    Close Application

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================