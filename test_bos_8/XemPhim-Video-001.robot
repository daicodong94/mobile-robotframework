 *** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/xem_phim/thu_muc.robot
Resource    ../page/xem_phim/video.robot

Suite Setup       Mo Xem phim
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-3671
    [Documentation]    Check phat video quay camera khi cham vao video .mp4 trong danh sach
    [Tags]     High    BP-3671
    Cho phep cap quyen cho ung dung
    Sleep    3
    Chon video quay tu camera
    Bam vao man hinh
    Element Attribute Should Match    ${xp_v_ten_video}    text    *mp4
    Page Should Contain Element    ${xp_v_thanh_seekbar}
    Page Should Contain Element    ${xp_v_nut_khoa}
    Page Should Contain Element    ${xp_v_nut_track}
    Page Should Contain Element    ${xp_v_nut_overplay}
    Page Should Contain Element    ${xp_v_nut_function}
    Page Should Contain Element    ${xp_v_nut_size}
    Vuot quay ve man hinh Home
    Log To Console    Check phat video quay man hinh khi cham vao video .mp4 trong danh sach
    Bam mo xem phim tu man hinh da nhiem
    Sleep    3
    Chon video quay man hinh
    Element Attribute Should Match    ${xp_v_ten_video}    text    *mp4
    Page Should Contain Element    ${xp_v_thanh_seekbar}
    Page Should Contain Element    ${xp_v_nut_khoa}
    Page Should Contain Element    ${xp_v_nut_track}
    Page Should Contain Element    ${xp_v_nut_overplay}
    Page Should Contain Element    ${xp_v_nut_function}
    Page Should Contain Element    ${xp_v_nut_size}
    Sleep    2
    Close All Applications

BP-3672
    [Documentation]    Check phat video download khi cham vao video .mp4 trong danh sach
    [Tags]     High    BP-3672
    Mo Xem phim
    Cho phep cap quyen cho ung dung
    Chon video download
    Element Attribute Should Match    ${xp_v_ten_video}    text    *mp4
    Page Should Contain Element    ${xp_v_thanh_seekbar}
    Page Should Contain Element    ${xp_v_nut_khoa}
    Page Should Contain Element    ${xp_v_nut_track}
    Page Should Contain Element    ${xp_v_nut_overplay}
    Page Should Contain Element    ${xp_v_nut_function}
    Page Should Contain Element    ${xp_v_nut_size}
    Close All Applications

BP-3673
    [Documentation]    Check phat video copy tu ngoai khi cham vao video .mp4 trong danh sach
    [Tags]             High    BP-3672    BP-3673
    Mo Xem phim
    Cho phep cap quyen cho ung dung
    Chon thu muc video mp4
    Bam vao man hinh
    Element Attribute Should Match    ${xp_v_ten_video}    text    *mp4
    Page Should Contain Element    ${xp_v_thanh_seekbar}
    Page Should Contain Element    ${xp_v_nut_khoa}
    Page Should Contain Element    ${xp_v_nut_track}
    Page Should Contain Element    ${xp_v_nut_overplay}
    Page Should Contain Element    ${xp_v_nut_function}
    Page Should Contain Element    ${xp_v_nut_size}
    Close All Applications

BP-3643
    [Documentation]    Check mo giao dien danh sach video khi cham vao tung Thu muc
    [Tags]             BP-3643
    Cho phep cap quyen cho ung dung
    Chon thu muc video mp4
    Chup anh man hinh va so sanh voi anh mau         ${xp_alias}    kc_d_ttdh_d_d_video_giao_dien_ds    3
    Page Should Contain Element    ${xp_v_sap_xep_theo}
    Vuot back ve giao dien truoc

BP-3644
    [Documentation]    Check giao dien hien thi danh sach cac video trong tung thu muc, text, icon, bo cuc sap xep
    [Tags]             BP-3644
    Chon thu muc video mp4
    Chup anh man hinh va so sanh voi anh mau         ${xp_alias}    kc_d_ttdh_d_d_video_giao_dien_ds    3
    Log To Console    Check hien thi do phan giai
    Page Should Contain Element    ${xp_v_thoi_luong_video_1}
    Page Should Contain Element    ${xp_v_thoi_luong_video_2}
    Page Should Contain Element    ${xp_v_thoi_luong_video_3}
    Log To Console    Check hien thi thoi luong video
    Page Should Contain Element    ${xp_tm_thoi_luong_vd_1}
    Page Should Contain Element    ${xp_tm_thoi_luong_vd_2}
    Page Should Contain Element    ${xp_tm_thoi_luong_vd_3}
    Log To Console    Check hien thi ten video
    Page Should Contain Element    ${xp_tm_ten_vd_stt_1}
    Page Should Contain Element    ${xp_tm_ten_vd_stt_2}
    Page Should Contain Element    ${xp_tm_ten_vd_stt_3}
    Sleep    2

BP-3645
    [Documentation]    Check giao dien hien thi khi cham icon kinh lup
    [Tags]             BP-3645
    Cham icon tim kiem hinh kinh lup
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_video_gd_icon_kinh_lup    2

BP-3650
    [Documentation]    Check mo giao dien cac ung dung ket noi khi cham Chia se
    [Tags]             BP-3650
    ${ten_v_can_chia_se}    Element Attribute Should Match    ${xp_tm_ten_vd_stt_1}    text    6_1280x720*
    Chon giu video dau tien
    Chon nut chia se
    Page Should Contain Element    ${xp_v_chia_se_app_youtobe}
    ${ten_v_tren_man_chia_se}    Element Attribute Should Match    ${xp_v_content_preview_filename}    text    6_1280x720*
    Should Be Equal    ${ten_v_can_chia_se}    ${ten_v_tren_man_chia_se}

BP-3651
    [Documentation]    Check tuong tu chuc nang tim kiem trong thu muc
    [Tags]             BP-3651    BP-3652    BP-3655
    Page Should Contain Element    ${xp_tm_text_tim_kiem_phim}
    Log To Console     Check hien thi icon xoa [X] khi nhap du lieu
    Nhap text ten phim hshfj
    Page Should Contain Element    ${xp_tm_nut_xoa_truy_van}
    Sleep    2
    Log To Console     Check TH bam dau [X] khi da nhap du lieu tim kiem
    Cham icon xoa [X] de xoa text
    Page Should Contain Element    ${xp_tm_text_tim_kiem_phim}
    Sleep    3
    Log To Console     Check quay lai giao dien Danh sach video khi bam vao mui ten back [<]
    Nhan chon nut [<] mui back - thu gon
    Page Should Not Contain Element    ${xp_tm_text_tim_kiem_phim}
    Sleep    3
    Log To Console    Check quay lai giao dien Danh sach thu muc khi cham icon back [<]
    Nhan chon nut [<] mui back - di chuyen len
    Page Should Contain Element    ${xp_tm_xem_video_gan_day}
    Page Should Not Contain Element    ${xp_v_sap_xep_theo}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================