*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/nghe_nhac/nn_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/ghi_am/ga_common.robot

Suite Setup       Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
# Khong co du lieu
BP-5137
    [Documentation]    Kiem tra mo app Nghe nhac khi chon ung dung Nghe nhac tren launcher
    [Tags]    High    BP-5137
    Mo Nghe nhac tu Launcher
    Page Should Contain Element    ${nn_nut_thu_muc}
    Close Application

BP-5530
    [Documentation]    Check hien thi giao dien cua cac tab chuc nang
    [Tags]    High    BP-5530
    Mo Nghe nhac
    Cho phep cap quyen cho ung dung
    Sleep    2
    An nut thu muc
    Chup anh man hinh va so sanh voi anh mau    ${nn_alias}    kc_d_ttdh_d_d_Thu_Muc    5
    An nut dang phat
    Chup anh man hinh va so sanh voi anh mau    ${nn_alias}    kc_d_ttdh_d_d_Dang_Phat    5
    An nut danh sach
    Chup anh man hinh va so sanh voi anh mau    ${nn_alias}    kc_d_ttdh_d_d_Danh_Sach    5
    An nut nghe si
    Chup anh man hinh va so sanh voi anh mau    ${nn_alias}    kc_d_ttdh_d_d_Nghe_Si    5
    An nut album
    Chup anh man hinh va so sanh voi anh mau    ${nn_alias}    kc_d_ttdh_d_d_Album    5
    Close Application

BP-5139
    [Documentation]    Kiem tra phat nhac khi click vao icon hinh tam giac (phat nhac)
    [Tags]    High    BP-5139     BP-5142
    Tao ban ghi va tick checkbox an khoi trinh choi nhac
    Mo Nghe nhac
    Cho phep cap quyen cho ung dung
    An nut tuy chon nghe nhac
    An checkbox an bai hat dung luong thap
    An nut dung hoac phat nhac
    Chup anh man hinh va so sanh voi anh mau    ${nn_alias}    kc_d_ttdh_d_d_Phat_nhac    2
    Log To Console    BP-5142    Kiem tra dung nhac khi click vao icon 2 soc (dung nhac)
    An nut dung hoac phat nhac
    Chup anh man hinh va so sanh voi anh mau    ${nn_alias}    kc_d_ttdh_d_d_Dung_nhac    2

BP-5149
    [Documentation]    Kiem tra dung nhac khi click vao icon 2 soc (dung nhac)
    [Tags]    High    BP-5149     BP-5148
    Vuot mo bang thong bao
    Vuot back ve giao dien truoc
    Chup anh man hinh va so sanh voi anh mau    ${nn_alias}    kc_d_ttdh_d_d_Dung_nhac    2
    Log To Console    BP-5148     Kiem tra phat nhac khi click vao icon hinh tam giac (phat nhac)
    Vuot mo bang thong bao
    An phat nhac va dung nhac tren notify
    Vuot back ve giao dien truoc
    Chup anh man hinh va so sanh voi anh mau    ${nn_alias}    kc_d_ttdh_d_d_Phat_nhac    2

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================