*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library    AppiumLibrary

Resource    ../page/launcher/launcher_common.robot
Resource    ../page/common.robot
*** Test Cases ***
BP-1020
    [Documentation]    Check direct toi man hinh launcher
    [Tags]    BP-1020
    Log To Console    Check direct toi man hinh launcher
    Mo Launcher
    Sleep    2

BP-1122
    [Documentation]    Check mo popup khi cham vao khoang trang trong man hinh chinh
    [Tags]    BP-1122
    Log To Console    Check mo popup khi cham vao khoang trang trong man hinh chinh
    Mo popup khi cham vao khoang trong man hinh chinh
    Page Should Contain Element    ${popup_cai_dat}
    Page Should Contain Element    ${popup_tien_ich}
    Page Should Contain Text    Cài đặt
    sleep    2
    
BP-1123 
    [Documentation]    Check mo popup khi cham vao khoang trang trong man hinh launcher
    [Tags]    BP-1123 
    Log To Console    Check mo popup khi cham vao khoang trang trong man hinh launcher
    Vuot sang man hinh ben phai
    Vuot sang man hinh ben phai
    Mo popup khi cham vao khoang trong man hinh launcher
    Page Should Contain Element    ${chon_lam_man_hinh_chinh}

BP-1125
    [Documentation]    Check mo giao dien cai dat khi cham vao cai dat tren popup
    [Tags]    BP-1125
    Log To Console    Check mo giao dien cai dat khi cham vao cai dat tren popup
    Cham vao popup cai dat

BP-1130
    [Documentation]    Check mo popup hieu ung xuat hien khi thuc hien cham hieu ung xuat hien
    [Tags]    BP-1130
    Log To Console    Check mo popup hieu ung xuat hien khi thuc hien cham hieu ung xuat hien
    Cham vao hieu ung xuat hien
    ${get_checked_expected}=    Get Element Attribute    ${hieu_ung_3}    checked
    Element Attribute Should Match    ${hieu_ung_3}    checked    ${get_checked_expected}
    sleep    3
    Go Back

BP-1135
    [Documentation]    Check mo popup hieu  ung khi cham vao hieu ung
    [Tags]    BP-1135
    Log To Console    Check mo giao dien cai dat khi cham vao cai dat tren popup
    Cham vao hieu ung
    sleep    1
    Page Should Contain Element    ${hieu_ung_mac_dinh}
    ${get_checked_expect}=    Get Element Attribute    ${hieu_ung_mac_dinh}    checked
    Element Attribute Should Match    ${hieu_ung_mac_dinh}    checked    ${get_checked_expect}
    Sleep    1    
    Vuot back ve giao dien truoc
    # Swipe By Percent    100    80    20    80    

BP-1140
    [Documentation]    Check su thay doi Switch sang trang thai bat khi cham bat an hoac hien thanh trang thai
    [Tags]    BP-1140
    Log To Console    Check su thay doi Switch sang trang thai bat khi cham bat an hoac hien thanh trang thai
    Mo popup khi cham vao khoang trong man hinh chinh
    Page Should Contain Element    ${popup_cai_dat}
    Cham vao popup cai dat
    # Vuot sang man hinh ben phai
    Cham vao nut on off thanh trang thai
    ${get_text_expected}=    AppiumLibrary.Get Element Attribute    ${on_off_an_or_hien_thanh_trang_thai}    text
    Element Attribute Should Match    ${on_off_an_or_hien_thanh_trang_thai}    text    ${get_text_expected}
    AppiumLibrary.Element Text Should Be    ${on_off_an_or_hien_thanh_trang_thai}    ${get_text_expected}    ${False}
    sleep    2
    Go Back
    Go Back

BP-1158
    [Documentation]    Check hien thi giao dien tim kiem khi bam giu vao chu google
    [Tags]    BP-1158    BP-1159 
    Log To Console    Check hien thi giao dien tim kiem khi bam giu vao chu google
    Bam chu google tren thanh tim kiem google
    # Click Element At Coordinates    112    1587
    sleep    3
    Page Should Contain Element    ${logo_google}
    Log To Console    Check hien thi giao dien dang nghe khi bam vao icon mic
    sleep    5
    Go Back
    Bam vao icon Mic
    sleep    2
    Page Should Contain Text    Đang nghe
    sleep    1
    Vuot back ve giao dien truoc
    Vuot back ve giao dien truoc

BP-1160
    [Documentation]    Check khong hien thi popup tim kiem ung dung khi switch dang o trang thai tat vuot len
    [Tags]    BP-1160
    Log To Console    Check khong hien thi popup tim kiem ung dung khi switch dang o trang thai tat vuot len
    Vuot len tim kiem ung dung
    Page Should Contain Element    ${chu_google}

BP-1164
    [Documentation]    Check thanh cuon tren popup them ung dung khi may ton tai nhieu ung dung
    [Tags]    BP-1164    BP-1161    BP-1171 
    Log To Console    Check thanh cuon tren popup them ung dung khi may ton tai nhieu ung dung
    Mo popup khi cham vao khoang trong man hinh chinh
    Cham vao popup cai dat
    SLEEP    2
    Bam vao vuot len de tim kiem ung dung
    ${get_text}=    AppiumLibrary.Get Element Attribute    ${vuot_len_de_tim_kiem_ung_dung}    text
    AppiumLibrary.Element Text Should Be    ${vuot_len_de_tim_kiem_ung_dung}    ${get_text}
    Go Back
    Go Back
    Vuot len tim kiem ung dung
    Page Should Contain Element    ${app_cai_dat}
    SLEEP    2
    Page Should Not Contain Element    ${chu_google}
    Vuot thanh cuon tren popup
    Sleep    2
    Page Should not Contain Element    ${app_cai_dat}
    Log To Console    Check dong popup khi thuc hien Vuot quay ve man hinh Home
    Vuot quay ve man hinh Home
    Go Back
    
BP-1175
    [Documentation]    Check gia tri mac dinh cua kick thuoc thu muc khi cham vao kich thuoc thu muc
    [Tags]    BP-1175
    Log To Console    Check gia tri mac dinh cua kick thuoc thu muc khi cham vao kich thuoc thu muc
    Mo popup khi cham vao khoang trong man hinh chinh
    Cham vao popup cai dat
    Page Should Contain Text    3x3

BP-1177
    [Documentation]    Check kich thuoc thu muc khi cham vao kich thuoc 4x4 trong popup kich thuoc thu muc
    [Tags]    BP-1177
    Log To Console    Check kich thuoc thu muc khi cham vao kich thuoc 4x4 trong popup kich thuoc thu muc
    Bam vao kich thuoc thu muc
    Bam vao kich thuoc thu muc 4x4
    Page Should Contain Text    4x4
    Go Back

BP-1433
    [Documentation]    Check hien thi giao dien tien ich khi thuc hien cham giu vao khoang trong tren man hinh launcher
    [Tags]    BP-1433
    Log To Console    Check hien thi giao dien tien ich khi thuc hien cham giu vao khoang trong tren man hinh launcher
    Mo popup khi cham vao khoang trong man hinh chinh
    Page Should Contain Element    ${popup_tien_ich}

BP-1439
    [Documentation]    Check mo ung dung khi cham vao tien ich con tren man hinh launcher
    [Tags]    BP-1439
    Log To Console    Check mo ung dung khi cham vao tien ich con tren man hinh launcher
    Bam vao tien ich
    Sleep    2
    Page Should Contain Element    ${Btalk}
    Go Back
    
BP-1474
    [Documentation]    Check hien thi bo suu tap khi thuc hien cham vao bo suu tap
    [Tags]    BP-1474
    Log To Console    Check hien thi bo suu tap khi thuc hien cham vao bo suu tap
    Bam vao icon bo suu tap
    Sleep    2
    Page Should Not Contain Element    ${app_cai_dat}
    Go Back
    Bam nut dong toan bo ung dung trong da nhiem
BP-1484
    [Documentation]    Check hien thi popup khi cham vao icon hinh nen lan dau tien
    [Tags]    BP-1484
    Log To Console    Check hien thi popup khi cham vao icon hinh nen lan dau tien
    Bam vao icon hinh nen
    Sleep    2
    Page Should Contain Element    ${dh_btn_dong_y}
    Close All Applications

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================