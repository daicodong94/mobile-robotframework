*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/chim_lac/cl_common.robot

Suite Setup       Mo Launcher
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-4169
    [Documentation]    Mo Chim lac tu Launcher
    [Tags]     High    BP-4169
    Mo Chim lac tu Launcher
    Bat Che do thu gon cua Chim Lac bang popup
    Log To Console     Xac nhan mo ung dung Chim lac thanh cong
    AppiumLibrary.Page Should Contain Element    ${cl_url_bar}
    Dong toan bo ung dung va ket thuc phien kiem thu

BP-5724
    [Documentation]    Check mo thanh cong tab an danh moi 
    [Tags]    High    BP-5724
    Mo Chim lac
    Bat Che do thu gon cua Chim Lac bang popup
    Mo chuc nang tab an danh moi
    Vuot back ve giao dien truoc
    Go Back
    Sleep    2    
    Page Should Contain Element    ${cl_url_bar}    
    Close All Applications
    
BP-5727
    [Documentation]     Check mo thanh cong tab moi
    [Tags]    High    BP-5727    BP-5725    BP-5726    BP-5728
    Mo Chim lac
    Bat Che do thu gon cua Chim Lac bang popup
    Mo chuc nang tab moi
    Page Should Contain Element    ${cl_trang_mac_dinh}    
    Log To Console    BP-5725        Kiem tra hien thi cac chuc nang khi cham giu icon so trang 
    Mo popup khi cham giu o vuong so trang
    AppiumLibrary.Click Element    ${cl_tab_moi}
    Sleep    2    
    Page Should Contain Element    ${cl_url_bar}    
    Log To Console    BP-5726        Check dong giao dien tab moi khi nhan nut X
    Mo chuc nang tab moi
    AppiumLibrary.Click Element    ${cl_closed_tab}
    Sleep    1
    Page Should Contain Text    Đã đóng Tab mới    
    Log To Console    BP-5728    Check mo thanh cong trang web bat ky tren chim lac
    AppiumLibrary.Click Element    ${cl_url_bar}
    Input Text    ${cl_url_bar}    dantri.com.vn
    Sleep    2    
    Click Element At Coordinates    348    1159
    Sleep    2    
    Element Should Contain Text    ${cl_url_bar}    dantri.com.vn  
    Swipe    505    1704    554    984      
    Swipe    554    984    505    1750      
    Sleep    2    
    Page Should Contain Element    ${cl_url_bar}           

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================