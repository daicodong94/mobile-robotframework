*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/thoi_tiet/tt_common.robot
Resource    ../page/launcher/launcher_common.robot

Suite Setup       Mo Thoi tiet
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***

BP-1655
    [Documentation]    Check man hinh thoi tiet bố cục sắp xếp, hình nền, các icon, text và mã màu
    [Tags]    BP-1655    BP-1654    BP-1653    BP-1660    BP-1661    BP-1662    BP-1663   BP-1664    BP-1665    BP-1666
    Cho phep cap quyen cho ung dung
    Bam nut them thanh pho
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Bình Dương
    Sleep    1
    Click Element    ${tt_tk_binh_duong}
    Sleep    1
    Bam nut them thanh pho
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Hồ Chí Minh
    Sleep    1
    Click Element    ${tt_tk_ho_chi_minh}
    Sleep    1
    Bam nut them thanh pho
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Cần Thơ
    Sleep    1
    Click Element    ${tt_tk_can_tho}
    Sleep    1
    Bam nut them thanh pho
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Đà Nẵng
    Sleep    1
    Click Element    ${tt_tk_da_nang}
    Sleep    1
    Bam nut them thanh pho
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Hải Phòng
    Sleep    1
    Click Element    ${tt_tk_hai_phong}
    Sleep    1
    Bam nut them thanh pho
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Thừa Thiên Huế
    Sleep    1
    Click Element    ${tt_tk_hue}
    Sleep    1
    Bam nut them thanh pho
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Nha Trang
    Sleep    1
    Click Element    ${tt_tk_nha_trang}
    Sleep    1
    Bam nut them thanh pho
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Quảng Ninh
    Sleep    1
    Click Element    ${tt_tk_quang_ninh}
    Sleep    1
    Bam nut them thanh pho
    AppiumLibrary.Input Text    ${tt_tk_nhap_ten_thanh_pho}    Bà Rịa
    Sleep    1
    Click Element    ${tt_tk_vung_tau}
    Sleep    2
    Vuot sang man hinh ben tren
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${tt_alias}    kc_d_ttdh_d_d_ThoiTiet_Main   18
    Vuot sang man hinh ben phai
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${tt_alias}    kc_d_ttdh_d_d_tp_HaNoi_Main   18
    AppiumLibrary.Swipe By Percent    80    30    10    30
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${tt_alias}    kc_d_ttdh_d_d_tp_BinhDuong   18
    AppiumLibrary.Swipe By Percent    80    30    10    30
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${tt_alias}    kc_d_ttdh_d_d_tp_HoChiMinh   18
    AppiumLibrary.Swipe By Percent    80    30    10    30
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${tt_alias}    kc_d_ttdh_d_d_tp_CanTho   18
    AppiumLibrary.Swipe By Percent    80    30    10    30
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${tt_alias}    kc_d_ttdh_d_d_tp_DaNang   18
    AppiumLibrary.Swipe By Percent    80    30    10    30
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${tt_alias}    kc_d_ttdh_d_d_tp_HaiPhong   18
    AppiumLibrary.Swipe By Percent    80    30    10    30
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${tt_alias}    kc_d_ttdh_d_d_tp_Hue   18
    AppiumLibrary.Swipe By Percent    80    30    10    30
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${tt_alias}    kc_d_ttdh_d_d_tp_NhaTrang   18
    AppiumLibrary.Swipe By Percent    80    30    10    30
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${tt_alias}    kc_d_ttdh_d_d_tp_QuangNinh   18
    AppiumLibrary.Swipe By Percent    80    30    10    30
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${tt_alias}    kc_d_ttdh_d_d_tp_VungTau   18

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================