*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/ghi_am/ga_common.robot
Resource    ../page/config/mac_dinh.robot
Resource    ../page/notification/notification.robot

Suite Setup        Mo Launcher
Suite Teardown     Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-3930
    [Documentation]    Kiem tra mo app Ghi am qua Launcher
    [Tags]     High    BP-3930
    Mo Ghi am tu Launcher
    ${hien_thi}    Run Keyword And Return Status    Page Should Contain Element    ${nut_cho_phep_hoa}
    Run Keyword If    ${hien_thi}==True     AppiumLibrary.Click Element    ${nut_cho_phep_hoa}
    ...    ELSE    Sleep    2
    Close Application

BP-3946
    [Documentation]    Kiem tra giao dien khi chuyen sang che do Dang ghi am
    [Tags]     High    BP-3946
    Mo Ghi am
    Click Element    ${nut_cho_phep_hoa}
    Sleep    2
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Chup anh man hinh va so sanh voi anh mau    ${ga_alias}    kc_d_ttdh_d_d_Dang_ghi_am    2

BP-3973
    [Documentation]    Kiem tra hien thi popup Ghi ra file khi cham button Dung
    [Tags]    High    BP-3973    BP-3976    BP-4350
    An nut danh sach ban ghi
    Log To Console    BP-3976    Kiem tra luu file khi bam DONG Y tren popup Ghi ra file
    An nut dong y
    Log To Console    BP-4350    Kiem tra hien thi cua screen Danh sach ban ghi khi da luu file
    Chup anh man hinh va so sanh voi anh mau    ${ga_alias}    kc_d_ttdh_d_d_Ds_ban_ghi_data    6
    Xoa toan bo du lieu app ghi am trong quan ly file
    Close Application

BP-3932
    [Documentation]    Kiem tra mo app Ghi am qua Multitask
    [Tags]     High    BP-3932    BP-3935    BP-3937
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    Mo da nhiem
    Mo ghi am tu da nhiem
    Log To Console    BP-3935     Kiem tra hien thi day du cac thong tin trong screen Ghi am
    Log To Console    BP-3937     Kiem tra tong the giao dien man hinh ghi am
    Log To Console    BP-3938     Kiem tra khi doi sang ngon ngu Tieng Viet
    Chup anh man hinh va so sanh voi anh mau    ${ga_alias}    kc_d_ttdh_d_d_main    2
    Close Application

BP-3960
    [Documentation]    Kiem tra tam dung ghi am khi an button Tam dung
    [Tags]    BP-3960    BP-3964    BP-3966    BP-3968
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    An nut tam dung ghi am
    Page Should Contain Text    Tạm dừng
    Log To Console    BP-3964    Kiem tra tiep tuc ghi am sau khi tam dung
    An nut bat dau ghi am
    Page Should Contain Text    Đang ghi âm
    Log To Console    BP-3966         Kiem tra chuc nang tam dung ghi am khi an tam dung o notify
    Vuot mo bang thong bao
    Click Element    ${ga_thongbao_tam_dung}
    Sleep    2
    Page Should Contain Text    Tạm dừng
    Log To Console    BP-3968    Kiem tra chuc nang dung ghi am khi an dung o notify
    An nut bat dau ghi am
    Vuot mo bang thong bao
    Click Element    ${ga_thongbao_dung}
    Page Should Contain Text    File ghi âm đã được lưu
    Sleep    2
    Close Application

BP-3969
    [Documentation]    Kiem tra back 1 lan khi dang ghi am
    [Tags]    BP-3969    BP-3971    BP-3972
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Swipe By Percent    0    50    10    50
    Sleep    2
    Page Should Contain Text    Đang ghi âm
    Log To Console    BP-3972    Kiem tra back 2 lan khong lien tiep khi dang ghi am
    Swipe By Percent    0    50    10    50
    Sleep    4
    Swipe By Percent    0    50    10    50
    Page Should Contain Text    Đang ghi âm
    Log To Console    BP-3971    Kiem tra back 2 lan lien tiep khi dang ghi am
    Swipe By Percent    0    50    10    50
    Swipe By Percent    0    50    10    50
    Vuot sang man hinh ben phai
    Page Should Contain Element    ${app_ghi_am}
    Sleep    2
    Close Application

BP-3978
    [Documentation]     Kiem tra dong popup, khong luu file khi bam HUY BO tren popup Ghi ra file
    [Tags]    BP-3978
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Click Element At Coordinates    154    2050
    Sleep    2    
    Click Element    ${dt_cd_nut_huy_bo}
    Sleep    2
    Page Should Contain Text    Chuẩn bị ghi âm

BP-3980
    [Documentation]    Kiem tra luu file khong hoi lai khi tick Khong hoi lai tren popup Ghi ra file
    [Tags]    BP-3980
    An nut bat dau ghi am
    Click Element At Coordinates    154    2050
    Click Element    ${ga_checkbox_khong_hoi_lai}
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    Vuot back ve giao dien truoc
    Sleep    2
    An nut bat dau ghi am
    Click Element At Coordinates    154    2050
    Sleep    2
    Page Should Contain Text    7 NGÀY GẦN NHẤT
    Close Application

BP-3983
    [Documentation]    Kiem tra dong bo khi doi ten file tai Quan ly file
    [Tags]    BP-3983    BP-3999
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Click Element    ${nut_cho_phep_hoa}
    Click Element    ${nut_cho_phep_hoa}
    Sleep    2
    Click Element At Coordinates    154    2050
    An nut dong y
    Vuot back ve giao dien truoc
    Sleep    2
    An nut tuy chon
    Sleep    2
    Click Element    ${ga_checkbox_an_khoi_trinh_choi_nhac}
    Vuot back ve giao dien truoc
    Sleep    2
    Mo quan ly file va doi ten tep ghi am
    Mo da nhiem
    Bam mo ung dung trai trong 2 ung dung tren da nhiem
    Click Element    ${nut_danh_sach_ban_ghi}
    Sleep    2
    Page Should Contain Text    DaiTVb
    Log To Console    BP-3999    Kiem tra luu file trung ten voi file da co trong Danh sach ban ghi tai popup Ghi ra file
    Vuot back ve giao dien truoc
    An nut bat dau ghi am
    Click Element At Coordinates    154    2050
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}    DaiTVb
    Click Element    ${nut_dong_y_hoa}
    Sleep    3    
    Page Should Contain Text    Tên đã được sử dụng
    Close Application

BP-4000
    [Documentation]    Kiem tra luu ten file vua bi trung khi da xoa ten file bi trung trong danh sach ban ghi
    [Tags]    BP-4000    BP-4001
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    Sleep    2
    An nut danh sach ban ghi
    Sleep    5    
    Long Press    ${ga_doi_ten_file}
    Sleep    2
    Click Element    ${ga_ban_ghi_xoa}
    Sleep    2
    Vuot back ve giao dien truoc
    An nut bat dau ghi am
    Click Element At Coordinates    538    2038
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Sleep    10
    An nut tam dung ghi am
    Click Element    ${nut_danh_sach_ban_ghi}
    Sleep    2
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}    DaiTVb
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    Page Should Contain Element    ${ga_doi_ten_file}
    Log To Console    BP-4001    Kiem tra thu muc luu file tren dien thoai
    Mo thu muc ghi am trong quan ly file
    Page Should Contain Text    DaiTVb
    Close Application

BP-3985
    [Documentation]    Kiem tra luu file khi nhap gia tri space/bo trong textbox Ghi ra file
    [Tags]    BP-3985
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Sleep    10
    An nut danh sach ban ghi
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Click Element    ${nut_dong_y_hoa}
    Page Should Contain Text    Tên bị trống, tự động đặt tên theo thời gian hiện tại
    Sleep    2
    Close Application

BP-3987
    [Documentation]    Kiem tra luu file khi nhap chu thuong vao textbox Ghi ra file
    [Tags]    BP-3987    BP-3998
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Click Element At Coordinates    154    2050
    Log To Console    BP-3998    Kiem tra luu file khac ten mac dinh tai popup Ghi ra file
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}         abcd
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    ${get_text_thuong}=    AppiumLibrary.Get Text    ${ga_verify_text_thuong}
    Should Be Equal    ${get_text_thuong}    abcd
    Sleep    2
    Close Application

BP-3988
    [Documentation]    Kiem tra luu file khi nhap chu in hoa vao textbox Ghi ra file
    [Tags]    BP-3988
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Click Element At Coordinates    154    2050
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}         AAAA
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    ${get_text_hoa}=    AppiumLibrary.Get Text    ${ga_verify_text_hoa}
    Should Be Equal    ${get_text_hoa}    AAAA
    Sleep    2
    Close Application

BP-3990
    [Documentation]    Kiem tra luu file khi nhap so vao textbox Ghi ra file
    [Tags]    BP-3990
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Click Element At Coordinates    154    2050
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}         123456
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    ${get_text_so}=    AppiumLibrary.Get Text    ${ga_verify_text_so}
    Should Be Equal    ${get_text_so}    123456
    Sleep    2
    Close Application

BP-3994
    [Documentation]     Kiem tra nhap cac ky tu dac biet (ki tu @, ~!<>*) vao textbox Ghi ra file
    [Tags]    BP-3994
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Click Element At Coordinates    154    2050
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}    !@#$%^&*
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    ${get_text_db}=    AppiumLibrary.Get Text    ${ga_verify_text_ki_tu_dac_biet}
    Should Be Equal    ${get_text_db}    !@#$%^&*
    Sleep    2
    Close Application

BP-3992
    [Documentation]    Kiem tra chuc nang trim space textbox Ghi ra file
    [Tags]    BP-3992
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Click Element At Coordinates    154    2050
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}       ${SPACE}daitvb${SPACE}
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    ${text_cat_space}=    AppiumLibrary.Get Text    ${ga_ban_ghi_list}
    Should Not Be Equal    ${text_cat_space}   ${SPACE}daitvb${SPACE}    
    Close Application

BP-4009
    [Documentation]    Kiem tra app Ghi am khi tat wifi hoac du lieu di dong
    [Tags]    BP-4009
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    Vuot mo bang dieu khien
    Click Element At Coordinates    208    1508
    Sleep    2
    Vuot back ve giao dien truoc
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    AppiumLibrary.Click Element    ${nut_cho_phep_hoa}
    Sleep    10
    Page Should Contain Element    ${ga_main_text_trang_thai_ghi_am}
    Close Application

BP-4004
    [Documentation]    Kiem tra app ghi am khi bat airplane mode
    [Tags]    BP-4004
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    Vuot mo bang dieu khien
    Click Element At Coordinates    192    1234
    Sleep    2
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    AppiumLibrary.Click Element    ${nut_cho_phep_hoa}
    Sleep    10
    An nut tam dung ghi am
    Page Should Contain Element    ${ga_main_text_trang_thai_ghi_am}
    Close Application

BP-4013
    [Documentation]     Kiem tra ghi am khi vuot ve home
    [Tags]    BP-4013
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Vuot quay ve man hinh Home
    Vuot mo bang thong bao
    Sleep    2
    Click Element    ${ga_thongbao_dung}
    Sleep    2
    Page Should Contain Text    File ghi âm đã được lưu
    Close Application

BP-4014
    [Documentation]    Kiem tra ghi am khi ve home qua da nhiem
    [Tags]    BP-4014
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Mo da nhiem
    Sleep    2
    Click Element At Coordinates    926    1924
    Vuot mo bang thong bao
    Sleep    2
    Click Element    ${ga_thongbao_dung}
    Page Should Contain Text    File ghi âm đã được lưu
    Dong toan bo ung dung trong da nhiem
    Close Application

BP-4025
    [Documentation]    Kiem tra ghi am khi mo che do chia doi man hinh
    [Tags]    BP-4025
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Mo da nhiem
    AppiumLibrary.Click Element    ${dn_icon_ung_dung}
    Sleep    2
    AppiumLibrary.Click Element    ${dn_chia_doi_man_hinh}
    ${hien_thi_app_cai_dat}    Run Keyword And Return Status    AppiumLibrary.Page Should Contain Element    ${app_cai_dat}        
    Run Keyword If    ${hien_thi_app_cai_dat}==True    AppiumLibrary.Click Element    ${app_cai_dat}    
    ...    ELSE    Vuot sang trai launcher va click app cai dat
    Sleep    2
    Swipe By Percent    50    50    50    5    duration=2000
    Sleep    2
    Page Should Contain Element    ${cd_tieu_de}    
    Close Application

BP-4011
    [Documentation]    Kiem tra ghi am khi tat man hinh
    [Tags]    BP-4011
    Mo Ghi am
    Cho phep cap quyen cho ung dung
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Mo Cai dat
    Sleep    2    
    AppiumLibrary.Click Element    ${cd_nut_ung_dung_va_thong_bao}
    Sleep    2    
    AppiumLibrary.Click Element    ${cd_nut_nang_cao}
    Sleep    2    
    Click Element    ${cd_thong_bao}
    Sleep    3        
    AppiumLibrary.Click Element    ${cd_nut_tren_man_hinh_khoa}
    Sleep    2    
    ${trang_thai_hien_thi}   Run Keyword And Return Status     Element Attribute Should Match    ${cd_cb_hien_thi_tatca_noidung_thongbao}    checked    false
    Run Keyword If    ${trang_thai_hien_thi}==True     AppiumLibrary.Click Element    ${cd_cb_hien_thi_tatca_noidung_thongbao}
    Run Keyword If    ${trang_thai_hien_thi}==True     AppiumLibrary.Lock               
    Run Keyword If    ${trang_thai_hien_thi}==False    AppiumLibrary.Lock    
    AppiumLibrary.Wait Until Page Contains Element    ${notify_nut_mo_rong}    timeout=7 
    Sleep    2
    ${get_text_lockscreen}    Get Text    ${ga_lockscreen_dang_ghi_am}
    Page Should Contain Text    ${get_text_lockscreen}
    Mo khoa va dong popup man hinh khoa
    Close Application

BP-3933
    [Documentation]    Kiem tra mo app Ghi am tu giao dien Dashboard
    [Tags]    High    BP-3933
    Mo Cai dat
    Sleep    5    
    Click Element    ${cd_nut_bang_dieu_khien}
    Sleep    2    
    Vuot back ve giao dien truoc
    Sleep    2    
    Click Element    ${cd_nut_bang_dieu_khien}
    Sleep    5    
    Swipe By Percent    50    80    50    30
    Sleep    2        
    Swipe By Percent    50    80    50    30    
    Swipe By Percent    50    80    50    30    
    Sleep    2
    AppiumLibrary.Click Element    ${cd_db_b_3_nut_add_chuc_nang_ghi_am}
    Sleep    2    
    Vuot mo bang dieu khien
    Vuot sang man hinh ben phai             
    Vuot sang man hinh ben phai  
    Sleep    2    
    Click Element At Coordinates    180    996    
    Sleep    2    
    Cho phep cap quyen cho ung dung
    Page Should Contain Element    ${ga_tieu_de}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================