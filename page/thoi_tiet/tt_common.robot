*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot

*** Variables ***
${ow_alias}       open_weather
${ow_package}     uk.co.openweather
${ow_activity}    .MainActivity

*** Keywords ***
Mo OpenWeather
    [Documentation]    Mo ung dung OpenWeather va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console     Mo ung dung OpenWeather va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${ow_alias}   automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${ow_package}    appActivity=${ow_activity}
    Sleep    5

Dong ung dung Thoi tiet roi mo va cap quyen cho ung dung OpenWeather
    [Documentation]    Dong ung dung Thoi tiet roi mo va cap quyen cho ung dung OpenWeather
    Log To Console    Dong ung dung Thoi tiet
    Quit Application
    Close All Applications
    Sleep    5
    Log To Console    Mo ung dung OpenWeather
    Mo OpenWeather
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Sleep    30

Dong ung dung OpenWeather
    [Documentation]    Dong ung dung OpenWeather
    Quit Application
    Close All Applications
    Sleep    1

Vuot len de mo man hinh thong tin chi tiet thoi tiet
    [Documentation]    Vuot len de mo man hinh thong tin chi tiet thoi tiet
    Swipe By Percent    50    20    50    5
    Sleep    1

Vuot xuong de dong man hinh thong tin chi tiet thoi tiet
    [Documentation]    Vuot xuong de dong man hinh thong tin chi tiet thoi tiet
    Swipe By Percent    50    5    50    35
    Sleep    1

Bam nut them thanh pho
    [Documentation]    Bam nut them thanh pho
    AppiumLibrary.Click Element    ${tt_nut_them_thanh_pho}
    Sleep    2

Xac nhan hien thi giao dien man hinh chinh
    [Documentation]    Xac nhan hien thi giao dien man hinh chinh
    AppiumLibrary.Page Should Contain Element    ${tt_nut_do_c}
    AppiumLibrary.Page Should Contain Element    ${tt_nut_do_f}
    AppiumLibrary.Page Should Contain Element    ${tt_nut_them_thanh_pho}

Doi don vi nhiet do sang do C trong ung dung OpenWeather
    [Documentation]    Doi don vi nhiet do sang do C trong ung dung OpenWeather
    Log To Console     Bam nut cai dat trong ung dung OpenWeather
    AppiumLibrary.Click Element    ${tt_open_weather_nut_cai_dat}
    Sleep    1
    Log To Console     Bam nut thay doi don vi do trong cai dat ung dung OpenWeather
    AppiumLibrary.Click Element    ${tt_open_weather_nut_thay_doi_don_vi_do}
    Sleep    1
    Log To Console     Bam vao tuy chon do C trong don vi nhiet do cua ung dung OpenWeather
    AppiumLibrary.Click Element    ${tt_open_weather_opt_do_c_open_weather}
    Sleep    1
    Log To Console     Quay lai man hinh chinh trong ung dung OpenWeather
    Bam nut quay lai man hinh truoc do
    Sleep    1
    Bam nut quay lai man hinh truoc do
    Sleep    5

Doi don vi nhiet do sang do F trong ung dung OpenWeather
    [Documentation]    Doi don vi nhiet do sang do F trong ung dung OpenWeather
    Log To Console     Bam nut cai dat trong ung dung OpenWeather
    AppiumLibrary.Click Element    ${tt_open_weather_nut_cai_dat}
    Sleep    1
    Log To Console     Bam nut thay doi don vi do trong cai dat ung dung OpenWeather
    AppiumLibrary.Click Element    ${tt_open_weather_nut_thay_doi_don_vi_do}
    Sleep    1
    Log To Console     Bam vao tuy chon do F trong don vi nhiet do cua ung dung OpenWeather
    AppiumLibrary.Click Element    ${tt_open_weather_opt_do_f_open_weather}
    Sleep    1
    Log To Console     Quay lai man hinh chinh trong ung dung OpenWeather
    Bam nut quay lai man hinh truoc do
    Sleep    1
    Bam nut quay lai man hinh truoc do
    Sleep    5

Bat vi tri cua app thoi tiet
    Log To Console    Bat vi tri cua app thoi tiet
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    Click Element    ${tt_bat_vi_tri}
    Vuot back ve giao dien truoc
    Sleep    2

Kiem tra va bat vi tri cua thoi tiet
    Log To Console    Kiem tra va bat vi tri cua thoi tiet
    ${trang_thai_vi_tri}    Get Matching Xpath Count    ${tt_message_bat_vi_tri}
    Run Keyword If    ${trang_thai_vi_tri}>0    Bat vi tri cua app thoi tiet
    ...    ELSE    Sleep    2

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================