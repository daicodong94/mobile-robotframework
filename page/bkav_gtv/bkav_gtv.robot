*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot
Resource    ../btalk/tin_nhan.robot

*** Keywords ***
Check mo giao dien ban phim khi cham vao vung soan tin nhan
    Mo Btalk
    Sleep    1
    bam vao tin nhan
    Sleep    1
    Click vao icon soan tin nhan moi
    Chup anh man hinh va so sanh voi anh mau    ${gtv_alias}    kc_d_ttdh_d_d_tn_GTV    4


Check mo giao dien ban phim khi mo ghi chep
    Click vao icon them moi ghi chep
    Click vao icon them moi checklist ghi chep
    Chup anh man hinh va so sanh voi anh mau    ${gtv_alias}    kc_d_ttdh_d_d_gc_GTV    4

Click vao icon soan tin nhan moi
    Click Element    ${tn_icon_soan_tin_nhan_moi}
    Sleep    2

Click vao icon them moi ghi chep
    Click Element    ${gtv_icon_them_moi_ghi_chep}
    Sleep    2

Click vao icon them moi checklist ghi chep
    Click Element    ${gtv_icon_checklist_ghi_chep}
    Sleep    2

Thuc nhap noi dung chu thuong tren ban phim
    Click vao hop soan van ban
    Click Element At Coordinates    ${gtv_chu_hoa_x}    ${gtv_chu_hoa_y}
    ${get_text}    Get Text    ${gtv_message_text}
    Sleep    1
    Should Be Equal   ${get_text}    A
    Sleep    1
    Click Element At Coordinates    ${gtv_chu_thuong_x}    ${gtv_chu_thuong_y}
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Click Element At Coordinates    ${gtv_space_x}    ${gtv_space_y}
    Click Element At Coordinates    ${gtv_chu_thuong2_x}    ${gtv_chu_thuong2_y}
    ${get_text}    Get Text    ${gtv_message_text}
    Sleep    1
    Should Be Equal    ${get_text}    Abc d
    Sleep    1
    Click xoa chu tren text vew
    Click xoa chu tren text vew
    ${get_text}    Get Text    ${gtv_message_text}
    Sleep    1
    Should Be Equal    ${get_text}    Abc
    Click xoa chu tren text vew
    Click xoa chu tren text vew
    Click xoa chu tren text vew
    Sleep    1
    Click Element At Coordinates    ${gtv_ky_tu_so_x}    ${gtv_ky_tu_so_y}
    Chup anh man hinh va so sanh voi anh mau    ${gtv_alias}    kc_d_ttdh_d_d_ky_tu_db_GTV    4
    Click Element At Coordinates    ${gtv_alt_x}    ${gtv_alt_y}
    Chup anh man hinh va so sanh voi anh mau    ${gtv_alias}    kc_d_ttdh_d_d_ky_tu_nc_GTV    4

Click vao hop soan van ban
    Click Element    ${gtv_message_text}
    Sleep    1

Click xoa chu tren text vew
    Click Element At Coordinates    ${gtv_nut_xoa_x}    ${gtv_nut_xoa_y}
    Sleep    1

Mo khoa voi mat khau text cccc
    Log To Console    Mo khoa voi mat khau text cccc
    AppiumLibrary.Swipe By Percent    50    99    50    60
    Sleep    2
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    2
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    2
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    2
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    5

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================