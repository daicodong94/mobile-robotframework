*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot

*** Variables ***
# Cac tham so mo ung dung Kham pha

${qmh_alias}       quay_man_hinh
${qmh_package}     recordscreen.bkav.com.recordscreen
${qmh_activity}    recordscreen.bkav.com.recordscreen.activity.StartActivity


${tim_kiem_qmh}    //*[@content-desc="Tìm kiếm trong các mục cài đặt"]


*** Keywords ***
Mo Quay man hinh
    [Documentation]    Mo ung dung Quay man hinh va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console     Mo ung dung Quay man hinh va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${qmh_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${qmh_package}   appActivity=${qmh_activity}
    Sleep    2

