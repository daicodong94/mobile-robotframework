*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot
Resource    ../cai_dat/cd_common.robot
Resource    ../dong_ho/cai_dat.robot

*** Keywords ***
Bat ket noi wifi
    Log To Console    Bat ket noi wifi
    ${kiem_tra}    Run Keyword And Return Status    Element Attribute Should Match    ${wifi_nut_bat_wifi_tat}     checked    false
    Run Keyword If    ${kiem_tra}==True    Click element    ${wifi_nut_bat_wifi_tat}
    Sleep    1
    Element Attribute Should Match    ${wifi_nut_bat_wifi_bat}    checked    true
    Sleep    2

Tat chia se ket noi wifi
    Log To Console    Bat ket noi wifi
    ${kiem_tra}    Run Keyword And Return Status    Element Attribute Should Match    ${wifi_nut_bat_wifi_bat}     checked    false
    Run Keyword If    ${kiem_tra}==True    Click element    ${wifi_nut_bat_wifi_tat}
    Sleep    1
    Element Attribute Should Match    ${wifi_nut_bat_wifi_tat}    checked    false
    Sleep    2

Chon mang va internet
    Log To Console    Chon mang va internet
    Click Element    ${wifi_text_mang_va_internet}
    Sleep    2

Chon wifi trong cai dat
    Log To Console    Chon wifi trong cai dat
    Click Element    ${wifi_text_wifi}
    Sleep    10

Chon diem phat song va chia se ket noi
    Log To Console    Chon diem phat song va chia se ket noi
     Click Element    ${wifi_text_diem_phat_song_va_chia_se_kn}
     Sleep    1

Mo giao dien chi tiet mang - icon cai dat
    Log To Console    Mo giao dien chi tiet mang - icon cai dat
    Click Element    ${wifi_icon_cai_dat_chi_tiet_wifi}
    Sleep    1

Chon xoa luu mang wifi
    Log To Console    Chon xoa luu mang wifi
    Click Element    ${Wifi_nut_xoa}

Chon mang da luu
    Log To Console    Chon mang da luu
    Click Element    ${wifi_text_mang_da_luu}

Nhan bat nut switch wifi
    Log To Console    Chon bat nut switch
    Click Element    ${wifi_nut_bat_wifi_tat}

Nhan tat nut switch wifi
    Log To Console    Nhan tat nut switch wif
    Click Element    ${wifi_nut_bat_wifi_bat}

Chon tuy chon wifi
    Log To Console    Chon tuy chon wifi
    Click Element    ${wifi_text_tuy_chon_wifi}

Chon wifi Bkav-QA-50GHz
    Log To Console    Chon wifi Bkav-QA-50GHz
    Click Element    ${wifi_bkav_qa_50GHz}
    Sleep    1

# Chon wifi Gem
    # Log To Console    Chon wifi Gem
    # click element

Nhap mat khau Bkav@2021 - mat khau dung
    Log To Console    Nhap mat khau Bkav@2021 - mat khau dung
    Click Element    ${wifi_o_nhap_password}
    Sleep    1
    Clear Text    ${wifi_o_nhap_password}
    Input Text    ${wifi_o_nhap_password}    Bkav@2021
    Sleep    3

Nhap mat khau duoi 8 ky tu
    Log To Console    Nhap mat khau duoi 8 ky tu
    Click Element    ${wifi_o_nhap_password}
    Sleep    1
    Clear Text    ${wifi_o_nhap_password}
    Input Text    ${wifi_o_nhap_password}    Bkav@20
    Sleep    3

Nhap mat khau tren 8 ky tu - mat khau sai
    Log To Console    Nhap mat khau duoi 8 ky tu
    Click Element    ${wifi_o_nhap_password}
    Sleep    1
    Clear Text    ${wifi_o_nhap_password}
    Input Text    ${wifi_o_nhap_password}    Bkav@20211
    Sleep    3

Nhao mat khau co 8 ky tu - mat khau sai
    Log To Console    Nhao mat khau co 8 ky tu - mat khau sai
    Click Element    ${wifi_o_nhap_password}
    Sleep    1
    Clear Text    ${wifi_o_nhap_password}
    Input Text    ${wifi_o_nhap_password}    Bkav@202
    Sleep    3

Hien thi mat khau
    Log To Console     Hien thi mat khau
    Click Element    ${wifi_checkbox_hien_thi_mk}
    Sleep    2
    Element Attribute Should Match    ${wifi_o_nhap_password}    text    Bkav@2021
    Sleep    1

Chon nut Ket noi
    Log To Console    Chon nut Ket noi
    Click Element    ${wifi_nut_ket_noi}
    Sleep    15

Chon mo Them mang
    Log To Console    Chon mo Them mang
    Click element    ${wifi_nut_them_mang}
    Sleep    1

Vuot tim kiem mo giao dien Them mang
    Log To Console    Vuot tim kiem mo giao dien Them mang
    ${tim_kiem} =    AppiumLibrary.Get Matching Xpath Count    ${wifi_nut_them_mang}
    Run Keyword If    ${tim_kiem}==0    Vuot sang man hinh ben tren

Chon Diem phat song Wifi
    Log To Console    Chon Diem phat song Wifi
    Click Element    ${wifi_text_diem_phat_song_wifi}
    Sleep    1

Click Ten diem phat song
    Log To Console    Click ten diem phat song
    Click Element    ${wifi_dps_text_ten_diem_phat_song}
    Sleep    1

Click mat khau diem phat song
    Log To Console    Click mat khau diem phat song
    Click Element    ${wifi_dps_mat_khau_diem_phat_song}
    Sleep    1

Thay doi Ten diem phat song -> BkavAutoTest
    Log To Console    Thay doi Ten diem phat song
    Clear Text    ${wifi_vi_tri_thay_doi_text_trong_popup}
    Input Text    ${wifi_vi_tri_thay_doi_text_trong_popup}    BkavAutoTest
    Sleep    1

Thay doi Ten diem phat song -> Bphone_7779
    Log To Console    Thay doi Ten diem phat song
    Clear Text    ${wifi_vi_tri_thay_doi_text_trong_popup}
    Input Text    ${wifi_vi_tri_thay_doi_text_trong_popup}    Bphone_7779
    Sleep    1

Click DONG Y thay doi
    Log To Console    Click DONG Y thay doi
    Click Element    ${wifi_nut_DONG_Y}
    Sleep    1

Click HUY thay doi
    Log To Console    Click HUY thay doi
    Click Element    ${wifi_nut_HUY}
    Sleep    1

Click chon dang Bao mat
    Log To Console    Click chon dang Bao mat
    Click Element    ${wifi_dps_text_bao_mat}
    Sleep    1

Click chon WPA2-Personal
    Log To Console    Click chon WPA2-Personal
    Click Element    ${wifi_dps_bao_mat_text_wpa2_perrsonl}
    Sleep    1

Click chon Khong - Bao mat
    Log To Console    Click chon Khong - Bao mat
    Click Element    ${wifi_dps_bap_mat_text_khong}
    Sleep    1

Thay doi mat khau diem phat song -> AutoTest123
    Log To Console    Thay doi mat khau diem phat song
    Clear Text    ${wifi_vi_tri_thay_doi_text_trong_popup}
    Input Text    ${wifi_vi_tri_thay_doi_text_trong_popup}    AutoTest123
    Sleep    1

Vuot sang man hinh ben trai dong giao dien wifi
    Log To Console    Vuot sang man hinh ben trai dong giao dien wifi
    AppiumLibrary.Swipe By Percent    0    50    80    50

Click chon Nang cao
    Log To Console    Click chon Nang cao
    Click Element    ${wifi_text_nang_cao}
    Sleep    1

Click chon bang tan AP
    Log To Console    Click chon bang tan AP
    Click Element    ${wifi_text_bang_tan_AP}
    Sleep    1

Click chon bang tan 2,4GHz
    Log To Console    Click chon bang tan 2,4GHz
    Click Element    ${wifi_text_bang_tan_2,4GHz}
    Sleep    1

Click chon bang tan 5HGz
    Log To Console    Click chon bang tan 5HGz
    Click Element    ${wifi_text_bang_tan_5GHz}
    Sleep    1

Click chon icon cai dat xem thong tin wifi
    Log To Console    Click chon icon cai dat - wifi da ket noi thanh cong
    Click Element    ${wifi_icon_cai_dat_chi_tiet_wifi}
    Sleep    1

Bam vao tuy chon wifi trong giao dien Wifi
    Log To Console    Bam vao tuy chon wifi trong giao dien Wifi
    AppiumLibrary.Click Element    ${wifi_text_tuy_chon_wifi}
    Sleep    1
    Exit For Loop

Bam vao Them mang trong giao dien wifi
    Log To Console    Bam vao Them mang trong giao dien wifi
    Click Element    ${wifi_nut_them_mang}
    Sleep    1
    Exit For Loop

Bam vao Mang da luu trong giao dien wifi
    Log To Console    Bam vao Mang da luu trong giao dien wifi
    Click Element    ${wifi_text_mang_da_luu}
    Sleep    1
    Exit For Loop

Vuot xuong duoi tim kiem
    Log To Console    Vuot xuong duoi tim kiem
    Swipe By Percent    50    80    50    20

Vuot man xuong duoi tim kiem Tuy chon wifi
    Log To Console    Vuot man xuong duoi tim kiem Tuy chon wifi
    FOR    ${i}    IN RANGE    0    5
    ${kiem_tra}    AppiumLibrary.Get Matching Xpath Count    //*[contains(@text,'Tùy chọn Wi‑Fi')]
    Run Keyword If    ${kiem_tra}==0    Vuot xuong duoi tim kiem
    ...    ELSE    Bam vao tuy chon wifi trong giao dien Wifi
    END

Vuot man xuong duoi tim kiem Them mang
    Log To Console    Vuot man xuong duoi tim kiem Them mang
    FOR    ${i}    IN RANGE    0    5
    ${kiem_tra}    Get Matching Xpath Count    //*[@text="Thêm mạng"]
    Run Keyword If    ${kiem_tra}==0    Vuot xuong duoi tim kiem
    ...    ELSE    Bam vao Them mang trong giao dien wifi
    END

Vuot man xuong duoi tim kiem Mang da luu
    Log To Console    Vuot man xuong duoi tim kiem Mang da luu
    FOR    ${i}    IN RANGE    0    5
    ${kiem_tra}    Get Matching Xpath Count    //*[contains(@text,'Mạng đã lưu')]
    Run Keyword If    ${kiem_tra}==0    Vuot xuong duoi tim kiem
    ...    ELSE    Bam vao Mang da luu trong giao dien wifi
    END

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================