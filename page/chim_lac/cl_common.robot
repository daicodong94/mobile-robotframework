*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot

*** Keywords ***
Bat Che do thu gon cua Chim Lac bang popup
    Log To Console    Bat Che do thu gon cua Chim Lac bang popup
    ${kiem_tra_popup}    AppiumLibrary.Get Matching Xpath Count    ${nut_bat_che_do_thu_gon}
    Run Keyword If    ${kiem_tra_popup}>0    AppiumLibrary.Click Element    ${nut_bat_che_do_thu_gon}
    Sleep    10

Khong bat Che do thu gon cua Chim Lac bang popup
    Log To Console    Khong bat Che do thu gon cua Chim Lac bang popup
    ${kiem_tra_popup}    AppiumLibrary.Get Matching Xpath Count    ${nut_khong_cam_on}
    Run Keyword If    ${kiem_tra_popup}>0    AppiumLibrary.Click Element    ${nut_khong_cam_on}
    Sleep    5

Cho phep su dung thong tin vi tri thiet bi
    Log To Console    Cho phep su dung thong tin vi tri thiet bi
    ${kiem_tra_popup}    AppiumLibrary.Get Matching Xpath Count    ${nut_cho_phep}
    Run Keyword If    ${kiem_tra_popup}>0    AppiumLibrary.Click Element    ${nut_cho_phep}
    Sleep    10

Chan su dung thong tin vi tri thiet bi
    Log To Console    Chan su dung thong tin vi tri thiet bi
    ${kiem_tra_popup}    AppiumLibrary.Get Matching Xpath Count    ${nut_chan}
    Run Keyword If    ${kiem_tra_popup}>0    AppiumLibrary.Click Element    ${nut_chan}
    Sleep    5

Truy cap trang web co canh bao khong an toan
    [Documentation]    Kiem tra canh bao khong an toan.
    ...                Neu co canh bao thi tiep tuc truy cap trang web co canh bao khong an toan
    ${kiem_tra}    AppiumLibrary.Get Matching Xpath Count    ${nut_nang_cao}
    Run Keyword If    ${kiem_tra}>0    Tiep tuc truy cap trang web co canh bao khong an toan
    Sleep    10

Tiep tuc truy cap trang web co canh bao khong an toan
    Log To Console    Tiep tuc truy cap trang web co canh bao khong an toan
    AppiumLibrary.Click Element    ${nut_nang_cao}
    Sleep    1
    AppiumLibrary.Click Element    ${cl_nut_tiep_tuc_truy_cap}

Mo chuc nang tab moi
    [Documentation]    Mo tab moi
    Mo menu tuy chon
    Click Element    ${cl_tab_moi}
    Sleep    2

Mo chuc nang tab an danh moi
    [Documentation]    Mo tab an danh moi
    Mo menu tuy chon
    Click Element    ${cl_tab_andanh_moi}
    Sleep    2

Mo menu tuy chon
    [Documentation]    An vao nut 3 cham
    Click Element    ${dh_cd_tuy_chon_khac}
    Sleep    2

Mo popup khi cham giu o vuong so trang
    [Documentation]    Mo popup khi cham giu
    Long Press    ${cl_o_vuong}
    Sleep    2
    Page Should Contain Element    ${cl_tab_moi}
    Page Should Contain Element    ${cl_tab_andanh_moi}
    Page Should Contain Element    ${cl_popup_dong_tab}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================