*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library    AppiumLibrary    

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot


*** Keywords ***
Xoa toan bo thong bao
    [Documentation]    Nhan nut xoa toan bo thong bao
    AppiumLibrary.Click Element    ${notify_nut_xoa_thong_bao}
    Sleep    2    
    
An nut mo rong thong bao
    [Documentation]    An nut mo rong thong bao  
    ${trang_thai_noti}    Run Keyword And Return Status   Page Should Contain Element    ${notify_nut_mo_rong}    
    Run Keyword If    ${trang_thai_noti}==True    AppiumLibrary.Click Element    ${notify_nut_mo_rong}
    Sleep    2    
    Run Keyword If    ${trang_thai_noti}==True    AppiumLibrary.Page Should Contain Element    ${notify_txt_bat_truyen_tep}    
     
Bat hien thi tat ca cac noi dung thong bao
    [Documentation]    bat hien thi tat ca cac noi dung thong bao trong cai dat
    Mo Cai dat tu Launcher
    Sleep    2    
    AppiumLibrary.Click Element    ${cd_nut_hien_thi}
    Sleep    2    
    AppiumLibrary.Click Element    ${cd_nut_nang_cao}
    Sleep    2    
    Swipe    556    1522    556    1050    
    Sleep    2    
    AppiumLibrary.Click Element    ${cd_nut_hien_thi_tren_man_hinh_khoa}
    Sleep    2    
    AppiumLibrary.Click Element    ${cd_nut_man_hinh_khoa}
    Sleep    2    
    ${trang_thai_hien_thi}   Run Keyword And Return Status     Element Attribute Should Match    ${cd_cb_hien_thi_tatca_noidung_thongbao}    checked    false
    Run Keyword If    ${trang_thai_hien_thi}==True     AppiumLibrary.Click Element    ${cd_cb_hien_thi_tatca_noidung_thongbao}
    Run Keyword If    ${trang_thai_hien_thi}==True     AppiumLibrary.Lock               
    Run Keyword If    ${trang_thai_hien_thi}==False    AppiumLibrary.Lock    
    AppiumLibrary.Wait Until Page Contains Element    ${notify_nut_mo_rong}    timeout=7 
    Sleep    2  

Bat dinh dang 24h trong cai dat
    Log To Console    Bat dinh dang 24h trong cai dat    
    Mo Cai dat
    Sleep    2    
    Swipe    484    1807    484    676        
    Swipe    484    1807    484    676        
    Sleep    2    
    Click Element    ${cd_nut_he_thong}
    Sleep    1    
    Click Element    ${dh_cd_ngay_va_gio} 
    Sleep    1    
    ${trang_thai_dinh_dang}    Run Keyword And Return Status    Element Attribute Should Match    ${cd_he_thong_ngay_gio_dinh_dang_24h}    checked    true        
    Run Keyword If    ${trang_thai_dinh_dang}==True    Sleep    2        
    ...    ELSE    AppiumLibrary.Click Element    ${cd_he_thong_ngay_gio_dinh_dang_24h}
   
Tao notify cho app dong ho
    Log To Console    Tao notify cho app dong ho
    Mo Dong ho
    Cho phep cap quyen cho ung dung
    Sleep    2    
    Click Element    ${dh_bt_bao_thuc_nhanh}
    Sleep    2    
    Click Element    ${dh_bt_btn_xong}
    Sleep    2     
    
bat hien thi thong bao tren man hinh khoa cho b3
    Log To Console    bat hien thi thong bao tren man hinh khoa cho b3    
    Mo Cai dat
    Sleep    2    
    AppiumLibrary.Click Element    ${cd_nut_ung_dung_va_thong_bao}
    Sleep    2    
    AppiumLibrary.Click Element    ${cd_nut_nang_cao}
    Sleep    2    
    Click Element    ${cd_thong_bao}
    Sleep    3        
    AppiumLibrary.Click Element    ${cd_nut_tren_man_hinh_khoa}
    Sleep    2    
    ${trang_thai_hien_thi}   Run Keyword And Return Status     Element Attribute Should Match    ${cd_cb_hien_thi_tatca_noidung_thongbao}    checked    false
    Run Keyword If    ${trang_thai_hien_thi}==True     AppiumLibrary.Click Element    ${cd_cb_hien_thi_tatca_noidung_thongbao}
    Run Keyword If    ${trang_thai_hien_thi}==True     AppiumLibrary.Lock               
    Run Keyword If    ${trang_thai_hien_thi}==False    AppiumLibrary.Lock    
    AppiumLibrary.Wait Until Page Contains Element    ${notify_nut_mo_rong}    timeout=7    
    

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================