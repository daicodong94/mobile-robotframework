*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Mo BMS tu Launcher roi mo Diet virus
    [Documentation]    Mo BMS tu Launcher roi mo Diet virus
    Mo Launcher
    Mo BMS tu Launcher
    AppiumLibrary.Click Element    ${bms_main_nut_diet_virus}
    Sleep    1

Tiep tuc va chi bao cao khi quet xong
    [Documentation]    Tich chon Chi bao cao khi quet xong roi bam nut Tiep tuc
    ${trang_thai}    Run Keyword And Return Status    AppiumLibrary.Wait Until Page Contains Element    ${nut_tiep_tuc}    timeout=10
    Run Keyword If    ${trang_thai}==True    AppiumLibrary.Click Element    ${bms_dvr_cb_chi_bao_cao_khi_quet_xong}
    Sleep    1
    Run Keyword If    ${trang_thai}==True    AppiumLibrary.Click Element    ${nut_tiep_tuc}

An nut quet virus
    [Documentation]    An nut quet virus
    AppiumLibrary.Click Element    ${bms_dvr_nut_quet_virus}
    
An nut quet virus tung phan
    [Documentation]    An nut quet virus tung phan
    AppiumLibrary.Click Element    ${nut_quet_tung_phan}

An nut dat lich quet
    [Documentation]    An nut dat lich quet
    AppiumLibrary.Click Element    ${nut_dat_lich_quet}
    
An nut tuy chon man diet virus
    [Documentation]    An nut tuy chon man diet virus
    AppiumLibrary.Click Element    ${nut_diet_virrus_tuy_chon}
   
An nut back danh sach bo qua canh bao
    [Documentation]    An nut back danh sach bo qua canh bao
    AppiumLibrary.Click Element    ${nut_back_danh_sach_bo_qua_quang_cao}
        
An nut chan tin nhan rac
    [Documentation]    an nut chan tin nhan rac
    Click Element    ${nut_chan_tin_nhan_rac}
    
An nut danh sach den man chan tin nhan rac
    [Documentation]    An nut danh sach den man chan tin nhan rac
    AppiumLibrary.Click Element    ${nut_tin_nhan_danh_sach_den}
    
An nut danh sach trang man chan tin nhan rac
    [Documentation]   An nut danh sach trang man chan tin nhan rac
    Click Element    ${nut_tin_nhan_danh_sach_trang}
    
An nut tranh lam phien trong man chan tin nhan rac
    [Documentation]   An nut tranh lam phien trong man chan tin nhan rac
    Click Element    ${nut_tin_nhan_tranh_lam_phien}
    
An nut chan theo noi dung man chan tin nhan rac
    [Documentation]   An nut chan theo noi dung man chan tin nhan rac
    Click Element    ${nut_tin_nhan_chan_thao_noi_dung}
    
An nut nhat ki chan man chan tin nhan rac
    [Documentation]   An nut nhat ki chan man chan tin nhan rac
    Click Element    ${nut_tin_nhan_nhat_ki_chan}
    
An nut huong dan man chan tin nhan rac
    [Documentation]    An nut huong dan man chan tin nhan rac
    Click Element    ${nut_tin_nhan_huong_dan_su_dung}
  
An nut chan cuoc goi rac
    [Documentation]    An nut chan cuoc goi rac
    Click Element    ${nut_chan_cuoc_goi_rac}
    
An nut danh sach den man chan cuoc goi rac
    [Documentation]    An nut danh sach den man chan cuoc goi rac
    Click Element    ${nut_cuoc_goi_rac_danh_sach_den}
    
An nut danh sach trang man chan cuoc goi rac
    [Documentation]   An nut danh sach trang man chan cuoc goi rac
    Click Element    ${nut_cuoc_goi_rac_danh_sach_trang}
    
An nut tranh lam phien man chan cuoc goi rac
    [Documentation]   An nut tranh lam phien man chan cuoc goi rac
    Click Element    ${nut_cuoc_goi_rac_tranh_lam_phien}
    
An nut nhat ky chan man chan cuoc goi rac
    [Documentation]   An nut nhat ky chan man chan cuoc goi rac
    Click Element    ${nut_cuoc_goi_rac_nhat_ky_chan}
    
An nut huong dan su dung man chan cuoc goi rac
    [Documentation]   An nut huong dan su dung man chan cuoc goi rac
    Click Element    ${nut_cuoc_goi_rac_huong_dan_su_dung}     
    
An nut bao ve giao dich ngan hang
    [Documentation]    An nut bao ve giao dich ngan hang
    Click Element    ${nut_bao_ve_giao_dich_ngan_hang}
 
An nut quet trong bao ve giao dich ngan hang
    [Documentation]    An nut quet trong bao ve giao dich ngan hang
    Click Element    ${nut_bao_ve_giao_dich_ngan_hang_quet}
        
An nut chong trom
    [Documentation]    An nut chong trom 
    Click Element    ${nut_chong_trom}
    
An nut nhap so dien thoai du phong trong man chong trom
    [Documentation]    An nut nhap so dien thoai du phong trong man chong trom
    Click Element    ${nut_so_dien_thoai_du_phong}
  
An nut back o man them so dien thoai trong man chong trom
    [Documentation]    An nut back o man them so dien thoai trong man chong trom    
    Click Element    ${nut_back_sdt_du_phong}
    
An nut noi dung man hinh khoa trong man chong trom
    [Documentation]  An nut noi dung man hinh khoa trong man chong trom
    Click Element    ${nut_chon_noi_dung_man_hinh_khoa_chong_trom} 
 
An nut dong chon man hinh khoa chong trom
    [Documentation]   An nut dong chon man hinh khoa chong trom
    Click Element    ${nut_close_chon_noi_dung_man_hinh_khoa_chong_trom}
        
An nut don rac toi uu
    [Documentation]    An nut ron rac toi uu
    Click Element    ${nut_don_rac_toi_uu}
    
An nut chan lam phien
    [Documentation]    An nut chan lam phien
    Click Element    ${nut_chan_lam_phien}
    
An nut an noi dung rieng tu
    [Documentation]    An nut an noi dung rieng tu
    Click Element    ${nut_an_noi_dung_rieng_tu}
    
An nut sao luu du lieu
    [Documentation]    An nut sao luu du lieu
    Click Element    ${nut_sao_luu_du_lieu}
    
An nut ban quyen
    [Documentation]    An nut ban quyen
    Click Element    ${nut_ban_quyen}
    
# An nut tuy chon
    # [Documentation]    An nut tuy chon
    # Click Element    ${nut_tuy_chon}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================