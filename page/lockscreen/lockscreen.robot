*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Click vao hop soan van ban
    Click Element    ${gtv_message_text}
    Sleep    1

Click xoa chu tren text vew
    Click Element At Coordinates    ${gtv_nut_xoa_x}    ${gtv_nut_xoa_y}
    Sleep    1

Click vao mat khau
    Click Element    ${cd_tab_mat_khau}
    Sleep    1

Mo khoa voi mat khau text cccc
    Log To Console    Mo khoa voi mat khau text cccc
    AppiumLibrary.Swipe By Percent    50    99    50    70
    Sleep    2
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    2
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    2
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    2
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    5

Mo khoa thiet lap
    Log To Console    Mo khoa thiet lap cccc
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    1
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    1
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    1
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    1

Vuot toa do de hien thi tab he thong
    Swipe By Percent    50    90    50    40
    Sleep    1

Vuot toa do de hien thi tab bao mat
    Swipe By Percent    50    90    50    40
    Sleep    1

Vuot toa do de mo khoa man hinh
    Swipe By Percent    50    99    50    30
    Sleep    3

Cham toa do
    Click Element At Coordinates    ${lockscreen_cham_man_hinh_toa_do_x}    ${lockscreen_cham_man_hinh_toa_do_y}
    Sleep    1

Bam vao toa do so 8888 tao ma pin
    Click Element At Coordinates    ${lockscreen_nhan_nut_toa_do_x}    ${lockscreen_nhan_nut_toa_do_y}
    Sleep    1
    Click Element At Coordinates    ${lockscreen_nhan_nut_toa_do_x}    ${lockscreen_nhan_nut_toa_do_y}
    Sleep    1
    Click Element At Coordinates    ${lockscreen_nhan_nut_toa_do_x}    ${lockscreen_nhan_nut_toa_do_y}
    Sleep    1
    Click Element At Coordinates    ${lockscreen_nhan_nut_toa_do_x}    ${lockscreen_nhan_nut_toa_do_y}
    Sleep    1

Bam vao toa do so 8885 ma pin sai
    Click Element At Coordinates    ${lockscreen_nhan_nut_toa_do_x}    ${lockscreen_nhan_nut_toa_do_y}
    Sleep    1
    Click Element At Coordinates    ${lockscreen_nhan_nut_toa_do_x}    ${lockscreen_nhan_nut_toa_do_y}
    Sleep    1
    Click Element At Coordinates    ${lockscreen_nhan_nut_toa_do_x}    ${lockscreen_nhan_nut_toa_do_y}
    Sleep    1
    Click Element At Coordinates    ${lockscreen_nhan_nut_toa_do_x_5}    ${lockscreen_nhan_nut_toa_do_y_5}

Kiem tra nhap ma pin va mat khau sai hien thi thong bao
    Page Should Contain Element    ${lockscreen_ma_pin_sai}
    Sleep    1

Bam vao toa do cccg mat khau
    Log To Console    Mo khoa thiet lap cccc
    AppiumLibrary.Swipe By Percent    50    99    50    70
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    1
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    1
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    1
    Click Element At Coordinates    ${gtv_chu_thuong1_x_g}    ${gtv_chu_thuong1_y_g}
    Sleep    1

Bam vao toa do ccc mat khau
    Log To Console    Mo khoa thiet lap cccc
    AppiumLibrary.Swipe By Percent    50    99    50    60
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    1
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Sleep    1
    Click Element At Coordinates    ${gtv_chu_thuong1_x}    ${gtv_chu_thuong1_y}
    Bam vao toa do xong tren ban phim
    Kiem tra nhap ma pin va mat khau sai hien thi thong bao

Bam vao toa do xong tren ban phim
    Click Element At Coordinates    ${gtv_chu_thuong1_x_xong}    ${gtv_chu_thuong1_y_xong}
    Sleep    1

Click vao tab bao mat
    Click Element    ${lockscreen_nut_bao_mat}
    Sleep    1

Click vao tab khoa man hinh
    Click Element    ${lockscreen_nut_khoa_man_hinh}
    Sleep    2

Click vao tab khoa man hinh vuot
    Click Element    ${lockscreen_nut_khoa_man_hinh_vuot}
    Sleep    1

Click vao ma pin
    Click Element    ${lockscreen_nut_ma_pin}
    Sleep    1

Click vao xac nhan co khoi dong che do ma pin
    Log To Console    Cho phep cap quyen cho ung dung
    ${kiem_tra} =  AppiumLibrary.Get Matching Xpath Count    ${lockscreen_nut_co}
    Run Keyword If    ${kiem_tra}>0   AppiumLibrary.Click Element    ${lockscreen_nut_co}
    Sleep    2

Click vao xac nhan dong y
    ${kiem_tra} =  AppiumLibrary.Get Matching Xpath Count    ${nut_dong_y_hoa}
    Run Keyword If    ${kiem_tra}>0   AppiumLibrary.Click Element    ${nut_dong_y_hoa}
    Sleep    1

Click vao xoa tinh nang bao ve thiet bi
    Click Element    ${lockscreen_nut_xoa_tinh_nang_bao_ve}
    Sleep    2

Click vao xac nhan tiep theo
    Click Element    ${lockscreen_nut_tiep_theo}
    Sleep    2

Click vao xac nhan tao ma pin
    Click Element    ${lockscreen_nut_xac_nhan}
    Sleep    2

Click vao xac nhan tao mat khau
    Click Element    ${lockscreen_nut_xac_nhan}
    Sleep    2

Check hien thi popup nhan xac nhan xong hien thi thong bao
    Log To Console    Cho phep cap quyen cho ung dung
    ${kiem_tra} =  AppiumLibrary.Get Matching Xpath Count    ${lockscreen_nut_xong}
    Run Keyword If    ${kiem_tra}>0   AppiumLibrary.Click Element    ${lockscreen_nut_xong}
    Sleep    1

Check hien thi dang o tai man hinh bao mat
    Log To Console    Cho phep cap quyen cho ung dung
    ${kiem_tra} =  AppiumLibrary.Get Matching Xpath Count    ${lockscreen_nut_bao_mat}
    Run Keyword If    ${kiem_tra}==0   Vuot mo da nhiem cham chon cai dat
    Sleep    1

Vuot mo da nhiem cham chon cai dat
   Mo da nhiem
   Click Element At Coordinates    500    1000

Kiem tra mo khoa may va hien thi tai man hinh bao mat
    ${get_text}=    Get Text    ${lockscreen_nut_bao_mat}
    Page Should Contain Text    ${lockscreen_text_hien_thi_bao_mat}
    Sleep    1

Kiem tra mo khoa may va hien thi bao mat thiet bi
    ${get_text1}=    Get Text    ${lockscreen_text_bao_mat_thiet_bi}
    Page Should Contain Text    ${lockscreen_text_hien_thi_bao_mat_thiet_bi}
    Sleep    1

# 07082021
Nhan vao tab he thong
    Click Element    ${lockscreen_nut_tab_he_thong}
    Sleep    1

Nhan vao tab cai dat ngay va gio
    Click Element    ${dh_cd_ngay_va_gio}
    Sleep    1

Kiem tra hien thi trang thai mac dinh gio theo dia phuong
    Log To Console    Kiem tra hien thi trang thai mac dinh gio theo dia phuong
    ${kiem_tra} =    Get Element Attribute    ${dh_cd_icon_bat_gio_dia_phuong}    checked
    Run Keyword If    '${kiem_tra}'=='false'   AppiumLibrary.Click Element    ${dh_cd_icon_bat_gio_dia_phuong}
    Sleep    1

Kiem tra gio dinh dang 24h them CH hoac SA
    ${gio}=    Get Text    ${lockscreen_hien_thi_gio}
    ${kiem_tra}=    Get Length    ${gio}
    Run Keyword If    ${kiem_tra}==8    Kiem tra hien thi date time
    Sleep    1

Kiem tra hien thi date time
    Page Should Contain Element    ${lockscreen_hien_thi_gio}
    Sleep    1
    Page Should Contain Element    ${lockscreen_hien_thi_thu}
    Sleep    1
    Page Should Contain Element    ${lockscreen_hien_thi_ngay_thang}
    Sleep    1

Click bat va tat den pin
    Click Element    ${lockscreen_nut_den_pin}
    Sleep    1

Click vao toa do bat den pin
    Click Element At Coordinates    187    1958
    Sleep    3

Bat cai dat hien thi thong bao
    AppiumLibrary.Click Element    ${cd_nut_hien_thi}
    Sleep    2
    AppiumLibrary.Click Element    ${cd_nut_nang_cao}
    Sleep    2
    Vuot sang man hinh ben duoi
    Sleep    2
    AppiumLibrary.Click Element    ${cd_nut_hien_thi_tren_man_hinh_khoa}

Kiem tra mac dinh khong hien thi tren man hinh khoa
    ${kiem_tra} =    Get Matching Xpath Count    ${lockscreen_khong_hien_thi_thong_bao}
    Run Keyword If    ${kiem_tra}==0   Click nut man hinh khoa
    Sleep    2
    Page Should Contain Element    ${lockscreen_khong_hien_thi_thong_bao}

Click nut man hinh khoa
    AppiumLibrary.Click Element    ${cd_nut_man_hinh_khoa}
    Sleep    1
    Click Element    ${lockscreen_khong_hien_thi_thong_bao}

Click vao icon thu muc danh sach nhac
    Click Element At Coordinates    197    577
    Sleep    1

Click vao bai hat
    Click Element At Coordinates    464    730
    # Click Element    ${nn_ten_bai_hat2}
    Sleep    1

Click play va stop nhac
    Click Element    ${nn_nut_play}
    Sleep    1

Click next bai hat
    Click Element    ${nn_nut_next}
    Sleep    1

Kiem tra next bai hat thanh cong
    Page Should Contain Element    ${nn_ten_bai_hat1}
    Sleep    1

Click back bai hat
    Click Element    ${nn_nut_back}
    Sleep    1

Kiem tra back bai hat thanh cong
    Page Should Contain Element    ${nn_ten_bai_hat2}
    Sleep    1

Vuot mo nhap ma pin va mat khau
    AppiumLibrary.Swipe By Percent    50    85    60    40
    Sleep    1

# ham su ly khan cap
Click vao nut khan cap
    Click Element    ${lockscreen_nut_khan_cap}
    Sleep    1

Click vao nut thong tin khan cap
    Click Element    ${lockscreen_nut_thong_tin_khan_cap}
    Sleep    1

Click drop vao nut thong tin khan cap
    Click Element    ${lockscreen_nut_thong_tin_khan_cap}
    Click Element    ${lockscreen_nut_thong_tin_khan_cap}

Kiem tra hien thi man hinh chu so huu
    Page Should Contain Element    ${lockscreen_chu_so_huu}
    Sleep    1
    Page Should Contain Element    ${lockscreen_thong_tin_chu_so_huu}
    Sleep    1

Click quay lai man hin thong tin khan cap
    Click Element    ${nut_menu_trai}
    Sleep    1

Cham drop vao nut thong tin khan cap
    Click Element At Coordinates    550    222
    Click Element At Coordinates    550    222
    Sleep    1

Vuot cheo man hinh khoa mo may anh
    Swipe By Percent    90    90    20    60
    Sleep    2

Click bat tat hien thi thoi gian 24h
    Click Element    ${dh_cd_icon_bat_gio_dia_phuong}
    Sleep    1
    Click Element    ${dh_cd_icon_bat_24h}
    Sleep    1

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
