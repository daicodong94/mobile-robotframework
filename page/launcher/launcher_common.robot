*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot

*** Keywords ***
Mo thu muc
    [Documentation]    Mo thu muc
    Click Element    ${thu_muc_google}
    Sleep    3

Cham giu ung dung
    [Documentation]    Cham va giu ung dung
    Long Press    ${app_cai_dat}    3000
    Sleep    3

Mo popup khi cham vao khoang trong man hinh chinh
    [Documentation]    Cham vao khoang trong
    Long Press    ${workspace}    3000
    Sleep    3

Mo popup khi cham vao khoang trong man hinh launcher
    [Documentation]    Cham vao khoang trong
    Long Press    ${danh_sach_ung_ung}    3000
    Sleep    3

Cham vao popup cai dat
    [Documentation]    Cham vao popup cai dat
    Click Element    ${popup_cai_dat}
    sleep    2

Cham vao hieu ung xuat hien
    [Documentation]    Cham vao hieu ung xuat hien
    Click Element    ${hieu_ung_xuat_hien}
    sleep    2

Cham vao hieu ung
    [Documentation]    Cham vao hieu ung
    Click Element At Coordinates    140    640
    Sleep    3

Cham vao nut on off thanh trang thai
    [Documentation]    Cham vao nut on of thanh trang thai
    Click Element    ${on_off_an_or_hien_thanh_trang_thai}
    Sleep    3

Cham vao hien thanh tim kiem
    [Documentation]    Cham vao hien thi thanh tim kiem
    Click Element    ${hien_thi_thanh_tim_kiem}
    Sleep    3

Bam chu google tren thanh tim kiem google
    [Documentation]    Cham vao chu google tren thanh tim kiem google
    Click Element    ${chu_google}
    Sleep    3

Bam vao icon Mic
    [Documentation]    Cham vao chu google tren thanh tim kiem google
    Click Element    ${tim_kiem_bang_giong_noi}
    # Click Element At Coordinates    920       1600
    Sleep    2

Vuot len tim kiem ung dung
    [Documentation]    Vuot len tim kiem ung dung
    Swipe By Percent    50    63    50    10
    Sleep    3

Bam vao vuot len de tim kiem ung dung
    [Documentation]    Bam vao Vuot len tim kiem ung dung
    Click Element    ${vuot_len_de_tim_kiem_ung_dung}
    Sleep    3

Vuot thanh cuon tren popup
    [Documentation]    Vuot thanh cuon tren popup
    Swipe By Percent    50    50    50    1
    Sleep    3

Bam vao kich thuoc thu muc
    [Documentation]    Bam vao kich thuoc thu muc
    Click Element    ${kich_thuoc_thu_muc}
    Sleep    3

Bam vao kich thuoc thu muc 4x4
    [Documentation]    Bam vao kich thuoc thu muc 4x4
    Click Element    ${kich_thuoc_4x4}
    Sleep    3

Bam vao tien ich
    [Documentation]    Bam vao tien ich
    Click Element    ${popup_tien_ich}
    Sleep    3

Bam va giu app thoi tiet
    [Documentation]    bam vao app thoi tiet
    Long Press    ${app_thoi_tiet}    2000

Bam vao tien ich con
    [Documentation]    Bam vao tien ich con
    Click Element    ${tt_tk_text_icon_tien_ich}

Bam va giu tien ich con thoi tiet
    [Documentation]    Bam va giu tien ich con thoi tiet
    Long Press     ${tien_ich_thoi_tiet_preview}    2000
    Sleep    2

Bam va giu icon con thoi tiet
    [Documentation]    Bam va giu icon con thoi tiet
    Long Press     ${tien_ich_thoi_tiet}    4000

Bam vao icon bo suu tap
    [Documentation]    Bam vao icon bo suu tap
    Click Element    ${app_bo_suu_tap}

Bam vao icon hinh nen
    [Documentation]    Bam vao icon hinh nen
    Click Element    ${app_hinh_nen}

Bam vao BMS tren Launcher
    [Documentation]    Bam vao BMS tren Launcher
    AppiumLibrary.Click Element    ${app_bms}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung BMS
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung BMS
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console    Tim ung dung BMS tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_bms}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao BMS tren Launcher
    END

Mo BMS tu Launcher
    [Documentation]    Tim va mo ung dung BMS tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung BMS
        Log To Console    Tim ung dung BMS tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_bms}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao BMS tren Launcher
    END
    Log To Console     Xac nhan mo ung dung BMS thanh cong
    AppiumLibrary.Page Should Not Contain Element    ${app_bms}

Bam vao Bo suu tap tren Launcher
    [Documentation]    Bam vao Bo suu tap tren Launcher
    AppiumLibrary.Click Element   ${app_bo_suu_tap}
    Sleep    10
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Bo suu tap
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Bo suu tap
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Bo suu tap tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_bo_suu_tap}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Bo suu tap tren Launcher
    END

Mo Bo suu tap tu Launcher
    [Documentation]    Tim va mo ung dung Bo suu tap tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Bo suu tap
        Log To Console     Tim ung dung Bo suu tap tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_bo_suu_tap}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Bo suu tap tren Launcher
    END

Bam vao Cai dat tren Launcher
    [Documentation]    Bam vao Cai dat tren Launcher
    AppiumLibrary.Click Element    ${app_cai_dat}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Cai dat
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Cai dat
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console    Tim ung dung Cai dat tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_cai_dat}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Cai dat tren Launcher
    END

Mo Cai dat tu Launcher
    [Documentation]    Tim va mo ung dung Cai dat tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Cai dat
        Log To Console    Tim ung dung Cai dat tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_cai_dat}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Cai dat tren Launcher
    END
    Log To Console     Xac nhan mo ung dung Cai dat thanh cong
    AppiumLibrary.Page Should Contain Element    ${app_cai_dat}
    AppiumLibrary.Page Should Contain Element    ${cd_tim_kiem}

Bam vao Chim lac tren Launcher
    [Documentation]    Bam vao Chim lac tren Launcher
    AppiumLibrary.Click Element    ${app_chim_lac}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Chim lac
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Chim lac
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Chim lac tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_chim_lac}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Chim lac tren Launcher
    END

Mo Chim lac tu Launcher
    [Documentation]    Tim va mo ung dung Chim lac tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Chim lac
        Log To Console     Tim ung dung Chim lac tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_chim_lac}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Chim lac tren Launcher
    END

Bam vao Danh ba tren Launcher
    [Documentation]    Bam vao Danh ba tren Launcher
    AppiumLibrary.Click Element    ${app_danh_ba}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Danh ba
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Danh ba
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Danh ba tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_danh_ba}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Danh ba tren Launcher
    END

Mo Danh ba tu Launcher
    [Documentation]    Tim va mo ung dung Danh ba tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Danh ba
        Log To Console     Tim ung dung Danh ba tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_danh_ba}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Danh ba tren Launcher
    END
    # Log To Console     Xac nhan mo ung dung Danh ba thanh cong
    # AppiumLibrary.Page Should Not Contain Element    ${app_danh_ba}

Bam vao Dien thoai tren Launcher
    [Documentation]    Bam vao Dien thoai tren Launcher
    AppiumLibrary.Click Element  ${app_dien_thoai}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Dien thoai
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Dien thoai
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Dien thoai tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_dien_thoai}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Dien thoai tren Launcher
    END

Mo Dien thoai tu Launcher
    [Documentation]    Tim va mo ung dung Dien thoai tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Dien thoai
        Log To Console     Tim ung dung Dien thoai tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_dien_thoai}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Dien thoai tren Launcher
    END
    Log To Console     Xac nhan mo ung dung Dien thoai thanh cong
    ${btalk_cac_phan_tu}    Set Variable    xpath=//*[contains(@resource-id,'${btalk_package}')]
    AppiumLibrary.Page Should Contain Element    ${btalk_cac_phan_tu}

Bam vao Dong ho tren Launcher
    [Documentation]    Bam vao Dong ho tren Launcher
    AppiumLibrary.Click Element   ${app_dong_ho}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Dong ho
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Dong ho
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Dong ho tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_dong_ho}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Dong ho tren Launcher
    END

Mo Dong ho tu Launcher
    [Documentation]    Tim va mo ung dung Dong ho tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Dong ho
        Log To Console     Tim ung dung Dong ho tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_dong_ho}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Dong ho tren Launcher
    END

Bam vao eDict tren Launcher
    [Documentation]    Bam vao eDict tren Launcher
    AppiumLibrary.Click Element    ${app_eDict}
    Sleep    2
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung eDict
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung eDict
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung eDict tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_eDict}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao eDict tren Launcher
    END

Mo eDict tu Launcher
    [Documentation]    Tim va mo ung dung eDict tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung eDict
        Log To Console     Tim ung dung eDict tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_eDict}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao eDict tren Launcher
    END

Bam vao eSIM tren Launcher
    [Documentation]    Bam vao eSIM tren Launcher
    AppiumLibrary.Click Element    ${app_esim}
    Sleep    2
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung eSIM
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung eSIM
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung eSIM tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_esim}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao eSIM tren Launcher
    END

Mo eSIM tu Launcher
    [Documentation]    Tim va mo ung dung eSIM tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung eSIM
        Log To Console     Tim ung dung eSIM tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_esim}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao eSIM tren Launcher
    END
    Log To Console     Xac nhan mo ung dung eSIM thanh cong
    AppiumLibrary.Page Should Contain Element    ${app_esim}

Bam vao Ghi am tren Launcher
    [Documentation]    Bam vao Ghi am tren Launcher
    AppiumLibrary.Click Element   ${app_ghi_am}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Ghi am
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Ghi am
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Ghi am tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_ghi_am}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Ghi am tren Launcher
    END

Mo Ghi am tu Launcher
    [Documentation]    Tim va mo ung dung Ghi am tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Ghi am
        Log To Console     Tim ung dung Ghi am tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_ghi_am}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Ghi am tren Launcher
    END
    Log To Console     Xac nhan mo ung dung Ghi am thanh cong
    AppiumLibrary.Page Should Contain Element    ${app_ghi_am}

Bam vao Ghi chep tren Launcher
    [Documentation]    Bam vao Ghi chep tren Launcher
    AppiumLibrary.Click Element   ${app_ghi_chep}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Ghi chep
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Ghi chep
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Ghi chep tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_ghi_chep}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Ghi chep tren Launcher
    END

Mo Ghi chep tu Launcher
    [Documentation]    Tim va mo ung dung Ghi chep tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Ghi chep
        Log To Console     Tim ung dung Ghi chep tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_ghi_chep}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Ghi chep tren Launcher
    END

Bam vao Hinh nen tren Launcher
    [Documentation]    Bam vao Hinh nen tren Launcher
    AppiumLibrary.Click Element   ${app_hinh_nen}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Hinh nen
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Hinh nen
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Hinh nen tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_hinh_nen}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Hinh nen tren Launcher
    END

Mo Hinh nen tu Launcher
    [Documentation]    Tim va mo ung dung Hinh nen tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Hinh nen
        Log To Console     Tim ung dung Hinh nen tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_hinh_nen}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Hinh nen tren Launcher
    END
    Log To Console     Xac nhan mo ung dung Hinh nen thanh cong
    AppiumLibrary.Page Should Contain Element    ${app_hinh_nen}

Bam vao Ho tro tren Launcher
    [Documentation]    Bam vao Ho tro tren Launcher
    AppiumLibrary.Click Element    ${app_ho_tro}
    Sleep    2
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Ho tro
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Ho tro
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Ho tro tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_ho_tro}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Ho tro tren Launcher
    END

Mo Ho tro tu Launcher
    [Documentation]    Tim va mo ung dung Ho tro tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Ho tro
        Log To Console     Tim ung dung Ho tro tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_ho_tro}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Ho tro tren Launcher
    END
    Log To Console     Xac nhan mo ung dung Ho tro thanh cong
    AppiumLibrary.Page Should Contain Element    ${app_ho_tro}

Bam vao Kham pha tren Launcher
    Log To Console    Bam vao Kham pha tren Launcher
    AppiumLibrary.Click Element    ${app_kham_pha}
    Sleep    2
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Kham pha
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Kham pha
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console    ${\n}Tim ung dung Kham pha tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_kham_pha}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Kham pha tren Launcher
    END

Mo Kham pha tu Launcher
    [Documentation]    Tim va mo ung dung Kham pha tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Kham pha
        Log To Console    ${\n}Tim ung dung Kham pha tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_kham_pha}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Kham pha tren Launcher
    END
    Log To Console     Xac nhan mo ung dung Kham pha thanh cong
    AppiumLibrary.Page Should Not Contain Element    ${app_kham_pha}

Bam vao May anh tren Launcher
    [Documentation]    Bam vao May anh tren Launcher
    AppiumLibrary.Click Element    ${app_may_anh}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung May anh
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung May anh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung May anh tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_may_anh}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao May anh tren Launcher
    END

Mo May anh tu Launcher
    [Documentation]    Tim va mo ung dung May anh tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung May anh
        Log To Console     Tim ung dung May anh tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_may_anh}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao May anh tren Launcher
    END

Bam vao May tinh tren Launcher
    [Documentation]    Bam vao May tinh tren Launcher
    AppiumLibrary.Click Element   ${app_may_tinh}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung May tinh
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung May tinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung May tinh tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_may_tinh}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao May tinh tren Launcher
    END

Mo May tinh tu Launcher
    [Documentation]    Tim va mo ung dung May tinh tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung May tinh
        Log To Console     Tim ung dung May tinh tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_may_tinh}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao May tinh tren Launcher
    END
    Log To Console     Xac nhan mo ung dung May tinh thanh cong
    AppiumLibrary.Page Should Not Contain Element    ${app_may_tinh}

Bam vao Nghe dai tren Launcher
    [Documentation]    Bam vao Nghe dai tren Launcher
    AppiumLibrary.Click Element    ${app_nghe_dai}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Nghe dai
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Nghe dai
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Nghe dai tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_nghe_dai}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Nghe dai tren Launcher
    END

Mo Nghe dai tu Launcher
    [Documentation]    Tim va mo ung dung Nghe dai tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Nghe dai
        Log To Console     Tim ung dung Nghe dai tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_nghe_dai}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Nghe dai tren Launcher
    END

Bam vao Nghe nhac tren Launcher
    [Documentation]    Bam vao Nghe nhac tren Launcher
    AppiumLibrary.Click Element   ${app_nghe_nhac}
     Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Nghe nhac
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Nghe nhac
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Nghe nhac tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_nghe_nhac}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Nghe nhac tren Launcher
    END

Mo Nghe nhac tu Launcher
    [Documentation]    Tim va mo ung dung Nghe nhac tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Nghe nhac
        Log To Console     Tim ung dung Nghe nhac tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_nghe_nhac}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Nghe nhac tren Launcher
    END

Bam vao Nhac viec tren Launcher
    [Documentation]    Bam vao Nhac viec tren Launcher
    AppiumLibrary.Click Element   ${app_nhac_viec}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Nhac viec
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Nhac viec
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Nhac viec tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_nhac_viec}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Nhac viec tren Launcher
    END

Mo Nhac viec tu Launcher
    [Documentation]    Tim va mo ung dung Nhac viec tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Nhac viec
        Log To Console     Tim ung dung Nhac viec tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_nhac_viec}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Nhac viec tren Launcher
    END

Bam vao Quan ly file tren Launcher
    [Documentation]    Bam vao Quan ly file tren Launcher
    AppiumLibrary.Click Element    ${app_quan_ly_file}
    Sleep    2
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Quan ly file
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Quan ly file
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Quan ly file tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_quan_ly_file}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Quan ly file tren Launcher
    END

Mo Quan ly file tu Launcher
    [Documentation]    Tim va mo ung dung Quan ly file tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Quan ly file
        Log To Console     Tim ung dung Quan ly file tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_quan_ly_file}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Quan ly file tren Launcher
    END
    Log To Console    Xac nhan mo ung dung Quan ly file thanh cong   

Bam vao Suc khoe tren Launcher
    [Documentation]    Bam vao Suc khoe tren Launcher
    AppiumLibrary.Click Element  ${app_suc_khoe}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Suc khoe
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Suc khoe
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Suc khoe tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_suc_khoe}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Suc khoe tren Launcher
    END

Mo Suc khoe tu Launcher
    [Documentation]    Tim va mo ung dung Suc khoe tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Suc khoe
        Log To Console     Tim ung dung Suc khoe tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_suc_khoe}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Suc khoe tren Launcher
    END
    Log To Console     Xac nhan mo ung dung Suc khoe thanh cong
    AppiumLibrary.Page Should Not Contain Element    ${app_suc_khoe}

Bam vao Thoi tiet tren Launcher
    [Documentation]    Bam vao Thoi tiet tren Launcher
    AppiumLibrary.Click Element   ${app_thoi_tiet}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Thoi tiet
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Thoi tiet
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Thoi tiet tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_thoi_tiet}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Thoi tiet tren Launcher
    END

Mo Thoi tiet tu Launcher
    [Documentation]    Tim va mo ung dung Thoi tiet tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Thoi tiet
        Log To Console     Tim ung dung Thoi tiet tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_thoi_tiet}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Thoi tiet tren Launcher
    END

Bam vao Tin nhan tren Launcher
    [Documentation]    Bam vao Tin nhan tren Launcher
    AppiumLibrary.Click Element    ${app_tin_nhan}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Tin nhan
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Tin nhan
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Tin nhan tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_tin_nhan}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Tin nhan tren Launcher
    END

Mo Tin nhan tu Launcher
    [Documentation]    Tim va mo ung dung Tin nhan tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Tin nhan
        Log To Console     Tim ung dung Tin nhan tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_tin_nhan}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Tin nhan tren Launcher
    END
    Log To Console     Xac nhan mo ung dung Tin nhan thanh cong
    AppiumLibrary.Page Should Contain Element    ${app_tin_nhan}

Bam vao Xem phim tren Launcher
    [Documentation]    Bam vao Xem phim tren Launcher
    AppiumLibrary.Click Element   ${app_xem_phim}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung Xem phim
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung Xem phim
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung Xem phim tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_xem_phim}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao Xem phim tren Launcher
    END

Mo Xem phim tu Launcher
    [Documentation]    Tim va mo ung dung Xem phim tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung Xem phim
        Log To Console     Tim ung dung Xem phim tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_xem_phim}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao Xem phim tren Launcher
    END
    Log To Console     Xac nhan mo ung dung Xem phim thanh cong
    AppiumLibrary.Page Should Contain Element    ${app_xem_phim}
    
Bam vao OpenWeather tren Launcher
    [Documentation]    Bam vao OpenWeather tren Launcher
    AppiumLibrary.Click Element   ${app_openweather}
    Sleep    5
    Exit For Loop

Vuot sang man hinh ben phai de tim ung dung OpenWeather
    [Documentation]    Vuot sang man hinh ben phai de tim ung dung OpenWeather
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${j}    IN RANGE    0    ${so_man_launcher_int}
        Log To Console     Tim ung dung OpenWeather tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_openweather}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben phai
        ...    ELSE    Bam vao OpenWeather tren Launcher
    END

Mo OpenWeather tu Launcher
    [Documentation]    Tim va mo ung dung OpenWeather tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung OpenWeather
        Log To Console     Tim ung dung OpenWeather tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_openweather}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao OpenWeather tren Launcher
    END
    Log To Console     Xac nhan mo ung dung OpenWeather thanh cong
    Sleep    10    
    Cho phep cap quyen cho ung dung
    AppiumLibrary.Page Should Contain Element    ${tt_open_weather_nut_cai_dat}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================