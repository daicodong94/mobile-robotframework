*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot
Resource    tat_ca_kenh.robot

*** Keywords ***
thuc hien kiem tra tai man hinh kenh vua nghe dang phat nhac
    [Documentation]    thuc hien kiem tra tại man hinh kenh vua nghe dang phat nha
    Sleep    10
    ${get_text}=     AppiumLibrary.Get Text    ${nd_yt_txt_hien_thi_kenh_dang_phat}
    Sleep    10
    AppiumLibrary.Page Should Contain Element    ${nd_yt_txt_hien_thi_kenh_dang_phat}

Thuc hien kiem tra trang thai dang o man hinh yeu thich
    [Documentation]    Thuc hien kiem tra trang thai dang o man hinh yeu thich
    AppiumLibrary.Element Should Be Visible    ${nd_tab_yeu_thich}

Thuc hien kiem tra thay doi vi tri theo trang thai phan tu
    ${get_phan_tu}=    Get Matching Xpath Count    ${nd_txt_hien_thi_2}
    Should Be Equal    0    ${get_phan_tu}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================