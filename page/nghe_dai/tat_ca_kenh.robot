*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot
Resource    yeu_thich.robot

*** Variables ***

*** Keywords ***

Thuc hien dem phan tu hien thi tren danh sach tat ca kenh
    [Documentation]    Thuc hien dem phan tu hien thi tren danh sach tat ca kenh
    ${so_phan_tu_hien_thi_la}=    Get Matching Xpath Count    ${nd_yt_list_phan_tu_hien_thi}
    Should Be Equal    ${so_phan_tu_hien_thi_la}    8

Thuc hien kiem tra danh sach hien thi tai man hinh tat ca kenh
    [Documentation]    Thuc hien kiem tra hien thi man hinh yeu thich
    ${Get_text_1}=    AppiumLibrary.Get Text    ${nd_yt_txt_1}
    ${Get_text_2}=    AppiumLibrary.Get Text    ${nd_yt_txt_2}
    ${Get_text_3}=    AppiumLibrary.Get Text    ${nd_yt_txt_3}
    ${Get_text_4}=    AppiumLibrary.Get Text    ${nd_yt_txt_4}
    ${Get_text_5}=    AppiumLibrary.Get Text    ${nd_yt_txt_5}
    ${Get_text_6}=    AppiumLibrary.Get Text    ${nd_yt_txt_6}


Thuc hien kiem tra danh sach hien thi man hinh tat ca kenh
    [Documentation]    Thuc hien kiem tra
    ${Get_text_1}=    AppiumLibrary.Get Text    ${nd_yt_txt_1}
    ${Get_text_2}=    AppiumLibrary.Get Text    ${nd_yt_txt_2}
    ${Get_text_3}=    AppiumLibrary.Get Text    ${nd_yt_txt_3}
    ${Get_text_4}=    AppiumLibrary.Get Text    ${nd_yt_txt_4}
    ${Get_text_5}=    AppiumLibrary.Get Text    ${nd_yt_txt_5}
    ${Get_text_6}=    AppiumLibrary.Get Text    ${nd_yt_txt_6}

Thuc hien bam chon vov1 duoc phat
    [Documentation]    Thuc hien bam vao kenh vov1
    AppiumLibrary.Click Element    ${nd_txt_vov1}
    Sleep    10
    Thuc hien kiem tra kenh vov1 dang duoc phat

Thuc hien bam chon vov1 duoc phat tat ca kenh
    [Documentation]    Thuc hien bam vao kenh vov1
    AppiumLibrary.Click Element    ${nd_txt_vov1}
    Sleep    10

Thuc hien bam chon vov1 duoc phat tat ca
    [Documentation]    Thuc hien bam vao kenh vov1
    AppiumLibrary.Click Element    ${nd_txt_vov1}
    Sleep    10

Thuc hien bam chon vov2 duoc phat
    [Documentation]    Thuc hien bam vao kenh vov2
    AppiumLibrary.Click Element    ${nd_txt_vov2}
    Sleep    10
    Thuc hien kiem tra kenh vov2 dang duoc phat

Vuot launcher len nua danh sach
    [Documentation]    Check vuot launcher duoi len tren
    Swipe By Percent    50    75    50    25

Vuot launcher len tren
    [Documentation]    Check vuot launcher duoi len tren
    Swipe By Percent    50    85    50    1

Thuc hien click them yeu thich
    [Documentation]    Thuc hien click bo tron yeu thich
    AppiumLibrary.Click Element    ${nd_yt_btn_icon_yeu_thich}
    AppiumLibrary.Page Should Contain Element    ${nd_yt_btn_icon_yeu_thich}

Thuc hien kiem tra da them kenh vao man hinh yeu thich
    [Documentation]    Thuc hien kiem tra da them kenh vao man hinh yeu thich
    Vuot sang man hinh ben trai
    Sleep    2
    Vuot launcher len tren
    AppiumLibrary.Page Should Contain Element    ${nd_yt_txt_chi_tiet}

Thuc hien kiem tra da huy kenh tai man hinh yeu thich
    [Documentation]    Thuc hien kiem tra da them kenh vao man hinh yeu thich
    Vuot sang man hinh ben trai
    Sleep    2
    AppiumLibrary.Page Should Not Contain Text    Cool FM 103,5

Thuc hien kiem tra danh sach chuc nang tat ca the loai
    [Documentation]    Thuc hien kiem tra chuc nang tat ca the loai
    AppiumLibrary.Click Element    ${nd_tctl_txt_chon_tat_ca}
    Sleep    3
    ${get_text_1}=    AppiumLibrary.Get Text    ${nd_tctl_txt_chon_tat_ca}
    AppiumLibrary.Page Should Contain Element    ${nd_tctl_txt_chon_tat_ca}
    Sleep    2
    ${get_text}=    AppiumLibrary.Get Text    ${nd_tctl_txt_chon_chinh_tri}
    AppiumLibrary.Page Should Contain Element    ${nd_tctl_txt_chon_chinh_tri}
    Sleep    2
    ${get_text_2}=    AppiumLibrary.Get Text    ${nd_tctl_txt_chon_tin_tuc}
    AppiumLibrary.Page Should Contain Element    ${nd_tctl_txt_chon_tin_tuc}
    Sleep    2
    ${get_text_3}=    AppiumLibrary.Get Text    ${nd_tctl_txt_chon_giai_tri}
    AppiumLibrary.Page Should Contain Element    ${nd_tctl_txt_chon_giai_tri}
    Sleep    2
    ${get_text_4}=    AppiumLibrary.Get Text    ${nd_tctl_txt_chon_du_lich}
    AppiumLibrary.Page Should Contain Element    ${nd_tctl_txt_chon_du_lich}
    Sleep    2
    ${get_text_6}=    AppiumLibrary.Get Text    ${nd_tctl_txt_chon_tong_ho}
    AppiumLibrary.Page Should Contain Element    ${nd_tctl_txt_chon_tong_ho}
    Sleep    2
    ${get_text_7}=    AppiumLibrary.Get Text    ${nd_tctl_txt_chon_the_thao}
    AppiumLibrary.Page Should Contain Element    ${nd_tctl_txt_chon_the_thao}
    Sleep    2
    ${get_text_8}=    AppiumLibrary.Get Text    ${nd_tctl_txt_chon_van_hoa}
    AppiumLibrary.Page Should Contain Element    ${nd_tctl_txt_chon_van_hoa}
    Sleep    2
    ${get_text_9}=    AppiumLibrary.Get Text    ${nd_tctl_txt_chon_am_nhac}
    AppiumLibrary.Page Should Contain Element    ${nd_tctl_txt_chon_am_nhac}
    Sleep    2
    ${get_text_10}=    AppiumLibrary.Get Text    ${nd_tctl_txt_chon_giao_duc}
    AppiumLibrary.Page Should Contain Element    ${nd_tctl_txt_chon_giao_duc}
    Sleep    2
    ${get_text_11}=    AppiumLibrary.Get Text    ${nd_tctl_txt_chon_giao_thong}
    AppiumLibrary.Page Should Contain Element    ${nd_tctl_txt_chon_giao_thong}

Thuc hien kiem tra chuc nang chon the loai kenh
    [Documentation]    Thuc hien kiem tra chuc nang chon the loai kenh
    AppiumLibrary.Click Element    ${nd_tctl_txt_chon_chinh_tri}
    ${Get_text_1}=    AppiumLibrary.Get Text    ${nd_yt_txt_1}
    Should Be Equal    ${Get_text_1}    VOV1
    ${Get_text_2}=    AppiumLibrary.Get Text    ${nd_yt_txt_2}
    Should Be Equal    ${Get_text_2}    BBC News
    Sleep    2

Thuc hien nhap vao o tim kiem
    [Documentation]    Thuc hien nhap vao o tim kiem
    AppiumLibrary.Click Element    ${nd_tck_icon_search}
    AppiumLibrary.Input Text    ${nd_tctl_tk_input_tim_kiem}    V
    Sleep    2

thuc hien input vao o tim kiem voi mot chu cai
    AppiumLibrary.Clear Text    ${nd_tctl_nut_tim_kiem}
    AppiumLibrary.Input Text    ${nd_tctl_tk_input_tim_kiem}    Vo
    ${get_phan_tu}=    Get Matching Xpath Count    ${nd_list_phan_tu}
    Sleep    2

thuc hien input vao o tim kiem voi noi dung
    AppiumLibrary.Clear Text    ${nd_tctl_nut_tim_kiem}
    AppiumLibrary.Input Text    ${nd_tctl_nut_tim_kiem}    Vov1
    ${Get_text_1}=    AppiumLibrary.Get Text    ${nd_yt_txt_1}
    Should Be Equal    ${Get_text_1}    VOV1
    AppiumLibrary.Page Should Contain Element    ${nd_txt_vov1_chi_tiet}

Thuc hien kiem tra man hinh
    [Documentation]   Thuc hien kiem tra man hinh
    AppiumLibrary.Page Should Not Contain Element    ${nd_list_phan_tu}

Thuc hien click tab 3 cham thong tin lien he
    [Documentation]    Thuc hien click tab 3 cham thong tin lien he
    AppiumLibrary.Click Element    ${nd_btn_tab_about}
    Sleep    2
    ${get_thong_tin}=    AppiumLibrary.Get Text    ${nd_ttct_txt_thong_tin}
    Sleep    2
    AppiumLibrary.Page Should Contain Element    ${nd_ttct_txt_sdt}
    AppiumLibrary.Page Should Contain Element    ${nd_ttct_txt_hop_thu}
    AppiumLibrary.Page Should Contain Element    ${nd_ttct_txt_link_trang_chu}
    Sleep    1

bam thoat o tim kiem
    Click Element    ${nd_tctl_tk_btn_back_tim_kiem}
    Sleep    1

Thuc hien click tab 3 cham
    [Documentation]    Thuc hien click tab 3 cham thong tin lien he
    AppiumLibrary.Click Element    ${nd_btn_tab_about}
    Sleep    1

Thuc hien thoat man hinh thong tin chi tiet
    [Documentation]    Thuc hien click tab 3 cham thong tin lien he
    AppiumLibrary.Click Element    ${nd_ttct_icon_ttct_nghe_dai}
    Sleep    2

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================