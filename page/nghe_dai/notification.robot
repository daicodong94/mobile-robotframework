*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Kiem tra hien thi kenh vov3 dang phat tren notification
    Page Should Contain Element    ${nd_notification_text_kenh_dang_phat}
    Sleep    1

Cho den khi hien thi thong bao khong co ket noi mang wf
    ${trang_thai}    Run Keyword And Return Status    AppiumLibrary.Wait Until Page Contains Element    ${nd_yt_txt_khong_co_ket_noi}    timeout=50
    Run Keyword If    ${trang_thai}==True    Kiem tra hien thi khong co ket noi
    Sleep    2

Kiem tra hien thi khong co ket noi
    Element Attribute Should Match    ${nd_yt_txt_khong_co_ket_noi}    text    ${nd_yt_khong_co_ket_noi}
    Sleep    1

Cho den khi ket noi kenh dang phat Vuot mo bang thong bao
    ${trang_thai}    Run Keyword And Return Status    AppiumLibrary.Wait Until Page Contains Element    ${nd_yt_txt_dang_phat}    timeout=50
    Run Keyword If    ${trang_thai}==True    Vuot mo bang thong bao
    Sleep    2

Kiem tra hien thi kenh vov2 dang phat tren notification
    Page Should Contain Element    ${nd_notification_text_kenh_dang_phat1}
    Sleep    2

Click phat kenh vov2
    Click Element    ${nd_notification_vov2}
    Sleep    1

Click dung phat kenh dang nghe tren notification
    Click Element    ${nd_notification_nut_play}
    Sleep    1

Click bat phat kenh dang nghe tren notification
    Click Element    ${nd_notification_nut_play}
    Sleep    1

Kiem tra hien thi kenh vov3 dang dung tren notification
    Page Should Contain Element    ${nd_notification_text_kenh_dang_dung}
    Sleep    1

Kiem tra hien thi khong co dich vu khi thoat kenh dang phat tren notification
    Page Should Contain Element    ${nd_notification_khong_co_dv}
    Sleep    1

Click next chuyen kenh dang phat
    Click Element    ${nd_notification_nut_next}
    Sleep    10

Click back chuyen kenh dang phat
    Click Element    ${nd_notification_nut_back}
    Sleep    10

Click tat nhanh nghe dai tren thanh notify
    Click Element    ${nd_notification_nut_thoat}

Click clear thong bao tren notification
    Click Element    ${nd_notification_nut_clear_danh_sach}


#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================