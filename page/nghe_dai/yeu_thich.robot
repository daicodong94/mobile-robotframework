*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library    AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Variables ***
${nd_alias}    nghe_dai
${nd_package}    bkav.android.radioonline
${nd_activity}    bkav.android.radioonline.RadioOnline

*** Keywords ***
Mo Nghe dai
    [Documentation]    Mo ung dung Nghe dai va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console     Mo ung dung Nghe dai va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${nd_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${nd_package}    appActivity=${nd_activity}
    Sleep    2

Xac nhan mo app Nghe dai thanh cong
    Log To Console     Xac nhan mo ung dung Nghe dai thanh cong
    AppiumLibrary.Page Should Contain Element    ${nd_tab_yeu_thich}
    AppiumLibrary.Page Should Contain Element    ${nd_tab_tat_ca_kenh}
    AppiumLibrary.Page Should Contain Element    ${nd_tab_kenh_vua_nghe}

Thuc hien click chon tab yeu thich
    [Documentation]   Thuc hien click chon tab tat ca kenh
    AppiumLibrary.Click Element    ${nd_tab_yeu_thich}
    Sleep    2

Thuc hien click chon tab tat ca kenh
    [Documentation]   Thuc hien click chon tab tat ca kenh
    AppiumLibrary.Click Element    ${nd_tab_tat_ca_kenh}
    Sleep    2

Thuc hien click chon tab kenh vua nghe
    [Documentation]   Thuc hien click chon tab tat ca kenh
    AppiumLibrary.Click Element    ${nd_tab_kenh_vua_nghe}
    Sleep    2

Thuc hien kiem tra dang o tai man hinh tat ca kenh
     AppiumLibrary.Page Should Contain Element    ${nd_tctl_txt_chon_tat_ca}
     Sleep    2

Thuc hien kiem tra dang o tai man hinh kenh vua nghe
    Page Should Contain Element    ${nd_txt_vov3}
    # Page Should Contain Element    ${nd_vov3_dang_phat}
    Sleep    2

Thuc hien kiem tra trang thai khong o tab man hinh tat ca kenh
    [Documentation]    Thuc hien kiem trang thai khong o tab man hinh tat ca kenh
    AppiumLibrary.Page Should Not Contain Element    ${nd_tctl_txt_chon_tat_ca}
    Sleep    2
Thuc hien kiem tra da an app nghe dai tro ve home
    [Documentation]    Thuc hien kiem tra da an thong bao xin cap quyen
    AppiumLibrary.Page Should Not Contain Element    ${nd_tab_yeu_thich}
    Sleep    2

Thuc hien kiem tra danh sach hien thi tai man hinh yeu thich
    [Documentation]    Thuc hien kiem tra hien thi man hinh yeu thich
    ${Get_text_1}=    AppiumLibrary.Get Text    ${nd_txt_hien_thi_1}
    ${Get_text_2}=    AppiumLibrary.Get Text    ${nd_txt_hien_thi_2}
    ${Get_text_3}=    AppiumLibrary.Get Text    ${nd_txt_hien_thi_8}
    ${Get_text_4}=    AppiumLibrary.Get Text    ${nd_txt_hien_thi_3}
    ${Get_text_5}=    AppiumLibrary.Get Text    ${nd_txt_hien_thi_4}
    ${Get_text_6}=    AppiumLibrary.Get Text    ${nd_txt_hien_thi_5}
    ${Get_text_7}=    AppiumLibrary.Get Text    ${nd_txt_hien_thi_6}

Thuc hien clear app tai man hinh da nhiem
    [Documentation]    Thuc hien clear app tai man hinh da nhiem
    AppiumLibrary.Click Element    ${nd_yt_btn_clear}
    Sleep    1

Thuc hien stop kenh dang chay
    [Documentation]    Thuc hien stop kenh dang chay
    AppiumLibrary.Click Element    ${nd_yt_btn_stop}
    Sleep    2

Thuc hien play kenh dang dung
    [Documentation]    Thuc hien play kenh dang dung
    AppiumLibrary.Click Element    ${nd_yt_btn_play}
    Sleep    1

Thuc hien kiem tra kenh vov3 dang duoc phat
    [Documentation]    Thuc hien kiem tra kenh vov3 dang duoc phat
    Element Attribute Should Match    ${nd_yt_txt_hien_thi_kenh_dang_phat}    text    *${nd_vov3_dang_phat}*
    Sleep    1

Thuc hien kiem tra kenh vov1 dang duoc phat
    [Documentation]    Thuc hien kiem tra kenh vov3 dang duoc phat
    Element Attribute Should Match    ${nd_yt_txt_hien_thi_kenh_dang_phat}    text    *${nd_vov1_dang_phat}*
    Sleep    1

Thuc hien kiem tra kenh vov2 dang duoc phat
    [Documentation]    Thuc hien kiem tra kenh vov3 dang duoc phat
    Element Attribute Should Match    ${nd_yt_txt_hien_thi_kenh_dang_phat}    text    *${nd_vov2_dang_phat}*
    Sleep    1

Kiem tra nut play co hoat dong khi bat dau mo app
    ${kiem_tra}=    Get Matching Xpath Count    ${nd_stop}
    Run Keyword If    ${kiem_tra}==0    Thuc hien play kenh dang dung
    Sleep    5

Cho den khi hien thi text dang phat
    ${kiem_tra}=    Get Matching Xpath Count    ${nd_yt_txt_dang_phat}
    Run Keyword If    ${kiem_tra}>0    Page Should Contain Element    ${nd_yt_txt_dang_phat}
    Sleep    5


Thuc hien kiem tra kenh dang duoc phat
    [Documentation]    Thuc hien kiem tra kenh dang duoc phat
    ${get_text}=     AppiumLibrary.Get Text    ${nd_yt_txt_dang_phat}
    Sleep    1

Thuc hien kiem tra kenh vov3 dang duoc tam rung
    [Documentation]    Thuc hien kiem tra kenh vov3 dang duoc tam rung
    ${get_text}=     AppiumLibrary.Get Text    ${nd_yt_txt_hien_thi_kenh_dang_phat}
    Sleep    1
Thuc hien chon kenh vov2 duoc phat
    [Documentation]    Thuc hien chon kenh vov2 duoc phat
    AppiumLibrary.Click Element    ${nd_txt_hien_thi_2}
    Sleep    2
    Thuc hien kiem tra kenh vov2 dang duoc phat
    Sleep    1
Thuc hien kiem tra kenh vov2 duoc tam rung
    [Documentation]    Thuc hien kiem tra kenh vov2 duoc tam rung
    Sleep    2
    ${get_text}=     AppiumLibrary.Get Text    ${nd_yt_txt_hien_thi_kenh_dang_phat}
    Sleep    1


Thuc hien click bo chon yeu thich
    [Documentation]    Thuc hien click bo chon yeu thich
    AppiumLibrary.Click Element    ${nd_yt_btn_icon_yeu_thich}
    AppiumLibrary.Page Should Not Contain Text    ${nd_yt_btn_icon_yeu_thich}
    Sleep    1
Thuc hien tai lai danh sach mac dinh yeu thich
    [Documentation]    Thuc hien tai lai danh sach mac dinh yeu thich
    AppiumLibrary.Click Element    ${nd_btn_reset}
    AppiumLibrary.Wait Until Element Is Visible    ${nd_btn_reset}
    Sleep    1
Thuc hien click vao icon hen gio
    [Documentation]    Thuc hien click vao icon hen gio
    Page Should Contain Element    ${nd_yt_btn_hen_gio}
    Click Element    ${nd_yt_btn_hen_gio}
    Sleep    1
Thuc hien kiem tra hien thi notify hen gio
    [Documentation]    Thuc hien kiem tra hien thi notify hen gio
    ${get_text_dong_ho}=    Get Text    ${nd_yt_txt_hien_thi_thoi_gian_hen_gio}
    Page Should Contain Element    ${nd_yt_txt_hien_thi_thoi_gian_hen_gio}
    ${get_text_notify}=    Get Text    ${nd_yt_txt_thong_tin_hen_gio}
    Page Should Contain Element    ${nd_yt_txt_thong_tin_hen_gio}
    Sleep    1
Thuc hien chon thoi gian hen gio tat ung dung nghe dai
    [Documentation]    Thuc hien chon thoi gian hen gio tat ung dung nghe dai
    Click Element    ${nd_yt_so_thoi_gian_hen_gio}
    Sleep    2
    Click Element    ${nd_yt_btn_Xong_hen_gio}
    Sleep    2
    Log To Console    vuot mo notify
    Vuot mo bang thong bao
    Sleep    2
Thuc hien kiem tra hien thi notify hen gio het han
    [Documentation]    Thuc hien kiem tra hien thi notify hen gio het han
    ${get_text_notify_hen_gio}=    Get Text    ${nd_yt_txt_thong_bao_ket_thuc_hen_gio}
    Page Should Contain Element    ${nd_yt_txt_thong_bao_ket_thuc_hen_gio}

Thuc hien an notify
    [Documentation]    Thuc hien an notify
    Log To Console    vuot dong notify
    Vuot dong bang thong bao
    Sleep    2

Thuc hien vuot hien thi them kenh
    [Documentation]    Thuc hien an notify
    Log To Console    vuot dong notify
    Swipe By Percent    50    65    55    2
    Sleep    2

Thuc hien huy bo hen gio
    [Documentation]    Thuc hien huy bo hen gio
    Click Element    ${nd_yt_btn_huy_hen_gio}
    Sleep    2

Thuc hien kiem tra khong co notify hen gio
    [Documentation]    Thuc hien kiem tra khong co notify hen gio
    Log To Console    vuot mo notify
    Swipe By Percent    50    1    50    80
    Sleep    2
    Page Should Not Contain Element    ${nd_yt_txt_thong_bao_ket_thuc_hen_gio}
    Log To Console    vuot dong notify
    Swipe By Percent    50    80    50    1
    Sleep    2

Thuc hien click button them moi de xuat
    [Documentation]    Thuc hien click button them moi de xuat
    Click Element    ${nd_yt_btn_them_moi}
    Sleep    2
Thuc hien click button them moi
    [Documentation]    click button them moi
    Click Element    ${nd_yt_btn_them_moi}
    Sleep    2
    Click Element    ${nd_yt_icon_them_moi}
    Sleep    2

Click vao ten kenh
    Click Element    ${nd_tm_input_ten_kenh}
    Sleep    1

Thuc hien input link kenh moi
    [Documentation]    Thuc hien input link kenh moi
    Click Element    ${nd_tm_input_link_kenh}
    Sleep    1
    Clear Text    ${nd_tm_input_link_kenh}
    Input Text    ${nd_tm_input_link_kenh}    https://www.rmp-streaming.com/media/big-buck-bunny-360p.mp4

Thuc hien input link kenh moi loi
    [Documentation]    Thuc hien input link kenh moi loi
    Click Element    ${nd_tm_input_link_kenh}
    Sleep    1
    Clear Text    ${nd_tm_input_link_kenh}
    Input Text    ${nd_tm_input_link_kenh}    https://www.rmp-streaming.com/media/big-buck-bunny-p.mp4

Thuc hien input cau lenh SQL
    [Documentation]    Thuc hien input cau lenh SQL
    Click Element    ${nd_tm_input_link_kenh}
    Sleep    1
    Clear Text    ${nd_tm_input_link_kenh}
    Sleep    1
    Input Text    ${nd_tm_input_link_kenh}    SELECT age, name FROM people;

Thuc hien input so
    [Documentation]    Thuc hien input so
    Click Element    ${nd_tm_input_link_kenh}
    Sleep    1
    Clear Text    ${nd_tm_input_link_kenh}
    Sleep    1
    Input Text    ${nd_tm_input_link_kenh}    1234567852445

Thuc hien input ky tu dac biet
    [Documentation]    Thuc hien input ky tu dac biet
    Click Element    ${nd_tm_input_link_kenh}
    Sleep    1
    Clear Text    ${nd_tm_input_link_kenh}
    Sleep    1
    Input Text    ${nd_tm_input_link_kenh}    @#!$!%#%!!@

Thuc hien input the html
    [Documentation]    Thuc hien input ky tu dac biet
    Click Element    ${nd_tm_input_link_kenh}
    Sleep    1
    Clear Text    ${nd_tm_input_link_kenh}
    Sleep    1
    Input Text    ${nd_tm_input_link_kenh}    <script src="/xjs/_/js/k=xjs.s.vi.y5FjNz9ahHc.O/m=ZyRBae"></script>

Thuc hien input link rong
    [Documentation]    Thuc hien input link rong
    Click Element    ${nd_tm_input_link_kenh}


Thuc hien click nghe thu
    [Documentation]    Thuc hien click nghe thu
    Click Element    ${nd_tm_trang_thai_nghe_thu}

Thuc hien click dung nghe thu
    [Documentation]    Thuc hien click dung nghe thu
    Click Element    ${nut_dung_hoa}

Thuc hien kiem tra notify mat ket noi
    [Documentation]    Thuc hien kiem tra notify mat ket noi
    ${text_mat_ket_noi}=    Get Text    ${nd_tm_trang_thai_mat_ket_noi}
    Page Should Contain Element    ${nd_tm_trang_thai_mat_ket_noi}

Thuc hien kiem tra da thoat khoi man hinh them moi
    Page Should Not Contain Element    ${nd_tm_nut_back}
    Sleep    1

Click nut back them moi kenh
    Click Element    ${nd_tm_nut_back}
    Sleep    1

Kiem tra khong hien thi nut them kenh
    Page Should Not Contain Element    ${nd_tm_nut_them_kenh}
    Sleep    1

Thuc hien input ten kenh
    [Documentation]    Thuc hien input ten kenh
    Click Element    ${nd_tm_input_ten_kenh}
    Input Text    ${nd_tm_input_ten_kenh}    kenh_moi
    Sleep    1
Thuc hien input mo ta kenh
    [Documentation]    Thuc hien input mo ta kenh
    Click Element    ${nd_tm_input_mo_ta}
    Input Text    ${nd_tm_input_mo_ta}    mo ta kenh
    Sleep    1
Thuc hien them kenh moi
    [Documentation]    Thuc hien them kenh moi
    Click Element    ${nd_tm_btn_them_kenh_moi}
    Sleep    5
Thuc hien click vao de xuat kenh
    Click Element    ${nd_yt_icon_de_xuat}
    Sleep    1
Thuc hien input vao web url
    Click Element    ${nd_dx_web_url}
    AppiumLibrary.Input Text    ${nd_dx_web_url}    https://www.rmp-streaming.com/media/big-buck-bunny-360p.mp4
    Sleep    1
Thuc hien input vao radio name
    Click Element    ${nd_dx_radio_name}
    Input Text    ${nd_dx_radio_name}    kenh moi
    Sleep    1
Thuc hien input vao description
    Click Element    ${nd_dx_description}
    Input Text    ${nd_dx_description}    add123
    Sleep    1
Thuc hien click icon tab de xuat
    Click Element    ${nd_yt_icon_de_xuat}
    Sleep    2
Thuc hien click button de xuat
    Click Element    ${nd_dx_btn_them_moi_de_xuat}
    Sleep    1
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================