*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
An nut menu trai
    [Documentation]    An nut menu trai
    Click Element    ${menu_trai}

An nut tat ca nhac viec
    [Documentation]    An nut tat ca nhac viec
    An nut menu trai
    Sleep    2
    Click Element    ${nut_tat_ca_nhac_viec}

An nut cong viec
    [Documentation]    An nut cong viec
    An nut menu trai
    Sleep    2
    Click Element    ${nut_cong_viec}

An nut dam cuoi
    [Documentation]    An nut dam cuoi
    An nut menu trai
    Sleep    2
    Click Element    ${nut_dam cuoi}

An nut sinh nhat
    [Documentation]    An nut sinh nhat
    An nut menu trai
    Sleep    2
    Click Element    ${nut_sinh_nhat}

An nut luu tru
    [Documentation]    An nut luu tru
    An nut menu trai
    Sleep    2
    Click Element    ${nut_luu_tru}

An nut them muc
    [Documentation]    An nut them muc
    An nut menu trai
    Sleep    2
    Click Element    ${nut_them_muc}

Chon them nhac viec dam cuoi
    [Documentation]    Chon them nhac viec dam cuoi
    Click Element    ${NV_nut_dam cuoi}
    Sleep    2    
    
Chon them nhac viec sinh nhat
    [Documentation]    Chon them nhac viec sinh nhat
    Click Element    ${NV_nut_sinh_nhat}
    Sleep    2    
    
Chon them nhac viec Cong viec
    [Documentation]    Chon tham nhac viec Cong viec
    Click Element    ${NV_nut_cong_viec}
    Sleep    2    
    
An nut huy them muc
    [Documentation]    An nut huy them muc
    Click Element    ${dh_bt_nut_huy}

An nut cai dat
    [Documentation]    An nut cai dat
    An nut menu trai
    Sleep    2
    Click Element    ${nut_cai_dat}

An nut them nhac viec
    [Documentation]    An nut them nhac viec
    Click Element    ${nv_nut_them_moi}  

An nut them dam cuoi
    [Documentation]    An nut them dam cuoi
    Click Element    ${nut_dam cuoi}
    Sleep    2

An nut them sinh nhat
    [Documentation]    An nut them sinh nhat
    Click Element    ${nut_sinh_nhat}
    Sleep    2

An nut them cong viec
    [Documentation]    An nut them cong viec
    Click Element    ${nut_cong_viec}
    Sleep    2    

An nut dieu huong
    [Documentation]    An nut dieu huong
    Click Element    ${nut_dieu_huong}

An nut tim kiem
    [Documentation]    An nut tim kiem
    Click Element    ${nut_tim_kiem}
    
An nut luu
    [Documentation]    An nut luu cong viec
    Click Element    ${nut_luu}
    Sleep    2    

Nhap text them nhac viec dam cuoi
    [Documentation]    Nhap text them nhac viec dam cuoi
    Clear Text    ${NV_damcuoi_plaintext}
    Input Text    ${NV_damcuoi_plaintext}    Dam cuoi nhe
    Sleep    2    
    
Chon sua nhac viec Dam cuoi - text Dam cuoi ne
    [Documentation]    Chon sua nhac viec Dam cuoi - text Dam cuoi ne
    Click Text    Dam cuoi nhe    
    Sleep    2      
    
Nhap tex them nhac viec Sinh nhat
    [Documentation]    Nhap tex them nhac viec Sinh nhat
    Clear Text    ${NV_damcuoi_plaintext}
    Input Text    ${NV_damcuoi_plaintext}    Sinh nhat nho di
    Sleep    2        
    
Chon sua nhac viec Sinh nhat - text Sinh nhat nho di
    [Documentation]    Chon sua nhac viec Sinh nhat - text Sinh nhat nho di
    Click Text    Sinh nhat nho di    
    Sleep    2    
    
Nhap tex them nhac viec Cong viec
    [Documentation]    Nhap tex them nhac viec Sinh nhat
    Clear Text    ${NV_damcuoi_plaintext}
    Input Text    ${NV_damcuoi_plaintext}    Cong viec nho lam
    Sleep    2       
    
Chon sua nhac viec Cong viec - text Cong viec nho lam
    [Documentation]    Chon sua nhac viec Cong viec - text Cong viec nho lam
    Click Text    Cong viec nho lam    
    Sleep    2    
    
Chon ngay - hom nay
    [Documentation]    Chon ngay cuoi - hom nay
    Click Element    ${NV_chon_ngay}
    Sleep    2    
    Click Element    ${NV_date_hom_nay}
    Page Should Contain Element    ${NV_date_hom_nay}    
    Sleep    2    
    
Chon ngay - ngay mai
    [Documentation]    Chon ngay - ngay mai
    Click Element    ${NV_chon_ngay}
    Sleep    2    
    Click Element    ${NV_date_ngay_mai}
    Page Should Contain Element    ${NV_date_ngay_mai}    
    Sleep    5    
    
Chon nhac truoc - khong nhac truoc
    [Documentation]    Chon nhac truoc - nhac truoc 1 ngay
    Click Element    ${NV_cong_viec_nhac_truoc}
    Sleep    2    
    Click Element    ${NV_reminder_khong_nhac_truoc}
    Page Should Contain Element    ${NV_reminder_khong_nhac_truoc}
    Sleep    2    
    
Chon nhac truoc - nhac truoc 1 ngay
    [Documentation]    Chon nhac truoc - nhac truoc 1 ngay
    Click Element    ${NV_nhac_nho_reminder_before}
    Sleep    2    
    Click Element    ${NV_reminder_nhac_truoc_1_ngay}
    Page Should Contain Element    ${NV_reminder_nhac_truoc_1_ngay}    
    Sleep    2    
    
Chon nhac truoc - nhac truoc 2 ngay
    [Documentation]    Chon nhac truoc - nhac truoc 2 ngay
    Click Element    ${NV_nhac_nho_reminder_before}
    Sleep    2    
    Click Element    ${NV_reminder_nhac_truoc_2_ngay}
    Page Should Contain Element    ${NV_reminder_nhac_truoc_2_ngay}
    Sleep    2    
    
Chon nhac lai - khong lap
    [Documentation]    Chon nhac lai - khong lap
    Click Element    ${NV_damcuoi_repeat}
    Sleep    2    
    Click Element    ${NV_repeat_khong_lap}
    Page Should Contain Element    ${NV_repeat_khong_lap}    
    Sleep    2    

Chon nhac lai - hang nam
    [Documentation]    Chon nhac lai - hang nam
    Click Element    ${NV_damcuoi_repeat}
    Sleep    2    
    Click Element    ${NV_repeat_hang_nam}
    Page Should Contain Element    ${NV_repeat_hang_nam} 
    Sleep    2    

Chon nut Luu de them nhac viec moi
    [Documentation]    Chon nut Luu de them nhac viec moi
    Click Element    ${NV_nut_luu}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================