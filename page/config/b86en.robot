*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Variables ***
#=================================================================================================
#    CAC BIEN MOI TRUONG KIEM THU
#=================================================================================================
${appium_port}         4723
${appium_url}          http://localhost:${appium_port}/wd/hub
${adb_port}            5037
${system_port}         8200
${platform_name}       Android
${platform_version}    10
${udid}                19abc458
${bphone}              b86
${bphone_ota}          88110
${ngon_ngu}            en
${dinh_vi}             vi_tri_${ngon_ngu}
${so_man_launcher}     2
${do_phan_giai}        1080x2160

#=================================================================================================
#    CAC THAM SO MO UNG DUNG
#=================================================================================================
# Cac tham so mo ung dung BMS
${bms_alias}              bms
${bms_package}            com.bkav.bphone.bms
${bms_activity}           com.bkav.ui.activity.BMSActivity

# Cac tham so mo ung dung Bo suu tap
${bst_alias}              bo_suu_tap
${bst_package}            org.codeaurora.gallery
${bst_activity}           com.android.gallery3d.app.GalleryActivity

# Cac tham so mo ung dung Btalk
${btalk_alias}            btalk
${btalk_package}          bkav.android.btalk
${btalk_activity}         bkav.android.btalk.activities.BtalkActivity

# Cac tham so mo ung dung Cai dat
${cd_alias}               cai_dat
${cd_package}             com.android.settings
${cd_activity}            com.android.settings.Settings

# Cac tham so mo ung dung Camera
${cam_alias}              camera
${cam_package}            com.bkav.camera
${cam_activity}           com.android.camera.CameraLauncher

# Cac tham so mo ung dung Chim lac
${cl_alias}               chim_lac
${cl_package}             android.bkav.bchrome
${cl_activity}            com.google.android.apps.chrome.Main

# Cac tham so mo ung dung Dong ho
${dh_alias}               dong_ho
${dh_package}             bkav.android.deskclock
${dh_activity}            com.android.deskclock.DeskClock

# Cac tham so mo ung dung eDict
${eDict_alias}            eDict
${eDict_package}          bkav.android.edict
${eDict_activity}         .EdictMainActivity

# Cac tham so mo ung dung eSim
${esim_alias}             eSim
${esim_package}           bkav.android.esim
${esim_activity}          bkav.android.esim.activity.esimAcitivity

# Cac tham so mo ung dung Ghi am
${ga_alias}               ghi_am
${ga_package}             bkav.android.recorder
${ga_activity}            bkav.android.recorder.activity.SoundRecorder

# Cac tham so mo ung dung Ghi chep
${gc_alias}               ghi_chep
${gc_package}             bkav.android.bkavnote
${gc_activity}            bkav.android.bkavnote.BkavNoteActivity

# Cac tham so mo ung dung Ho tro
${ht_alias}               ho_tro
${ht_package}             bkav.android.connect
${ht_activity}            bkav.android.connect.MainActivity

# Cac tham so mo ung dung Kham pha
${kp_alias}               kham_pha
${kp_package}             bkav.android.explorer
${kp_activity}            bkav.android.explorer.ExploreActivity

# Cac tham so mo ung dung Launcher
${launcher_alias}         launcher
${launcher_package}       bkav.android.launcher3
${launcher_activity}      com.android.launcher3.bkav.BkavLauncher

# Cac tham so mo ung dung May tinh
${mt_alias}               may_tinh
${mt_package}             com.bkav.calculator2
${mt_activity}            com.android.calculator2.bkav.BkavCalculator

# Cac tham so mo ung dung Nghe dai
${nd_alias}               nghe_dai
${nd_package}             bkav.android.radioonline
${nd_activity}            bkav.android.radioonline.RadioOnline

# Cac tham so mo ung dung Nghe nhac
${nn_alias}               nghe_nhac
${nn_package}             bkav.android.music
${nn_activity}            com.android.music.ui.MainActivity

# Cac tham so mo ung dung Nhac viec
${nv_alias}               nhac_viec
${nv_package}             bkav.android.reminder
${nv_activity}            .ReminderActivity

# Cac tham so mo ung dung Quan ly file
${qlf_alias}              quan_ly_file
${qlf_package}            com.bkav.android.bexplorer
${qlf_activity}           .main.BkavExplorerHomeActivity

# Cac tham so mo ung dung Quay man hinh
${qmh_alias}              quay_man_hinh
${qmh_package}            recordscreen.bkav.com.recordscreen
${qmh_activity}           recordscreen.bkav.com.recordscreen.activity.StartActivity

# Cac tham so mo ung dung Suc khoe
${sk_alias}               suc_khoe
${sk_package}             bkav.fitness.activity
${sk_activity}            bkav.fitness.activity.BkavHealthCareActivity

# Cac tham so mo ung dung Thoi tiet
${tt_alias}               thoi_tiet
${tt_package}             bkav.android.weather
${tt_activity}            bkav.android.weather.BkavWeatherActivity

# Cac tham so mo ung dung Xem phim
${xp_alias}               xem_phim
${xp_package}             com.bkav.video
${xp_activity}            bkav.android.video.activiry.MainVideosActivity

${gtv_alias}              bkav_gtv

#=================================================================================================
#    CAC PHAN TU PHO THONG
#=================================================================================================
${nut_cho_phep_hoa}        //*[contains(@text,'ALLOW')]
${nut_cho_phep}            //*[contains(@text,'Cho phép')]
${nut_tu_choi_hoa}         //*[contains(@text,'TỪ CHỐI')]
${nut_tu_choi}             //*[contains(@text,'DENY')]
${nut_dong_y_hoa}          //*[contains(@text,'ĐỒNG Ý')]
${nut_dong_y}              //*[contains(@text,'Đồng ý')]
${nut_dong_hoa}            //*[contains(@text,'ĐÓNG')]
${nut_huy_hoa}             //*[contains(@text,'HỦY')]
${nut_huy}                 //*[contains(@text,'Hủy')]
${nut_bat_dau}             //*[contains(@text,'Bắt đầu')]
${nut_ket_thuc}            //*[contains(@text,'Kết thúc')]
${nut_tam_dung}            //*[contains(@text,'Tạm dừng')]
${nut_dung_hoa}            //*[contains(@text,'STOPPED')]
${nut_dung}                //*[contains(@text,'Dừng')]
${nut_tiep_tuc}            //*[contains(@text,'Tiếp tục')]
${nut_bo_qua}              //*[contains(@text,'Bỏ qua')]
${nut_quay_lai}            //*[@content-desc="Di chuyển lên"]
${nut_khong_cam_on}        //*[contains(@text,'Không, cảm ơn')]

# Cac phan tu trong da nhiem
${dn_icon_ung_dung}        //*[@resource-id="bkav.android.launcher3:id/icon"]
${dn_2_ung_dung_trai}      xpath=(//*[@resource-id="bkav.android.launcher3:id/snapshot"])[1]
${dn_2_ung_dung_phai}      xpath=(//*[@resource-id="bkav.android.launcher3:id/snapshot"])[2]
${dn_3_ung_dung_trai}      xpath=(//*[@resource-id="bkav.android.launcher3:id/snapshot"])[1]
${dn_3_ung_dung_giua}      xpath=(//*[@resource-id="bkav.android.launcher3:id/snapshot"])[2]
${dn_3_ung_dung_phai}      xpath=(//*[@resource-id="bkav.android.launcher3:id/snapshot"])[3]
${dn_chia_doi_man_hinh}    //*[contains(@text,'Chia đôi màn hình')]
${nut_dong_toan_bo_app}    //*[@resource-id="bkav.android.launcher3:id/clear_all_button"]

${nut_bat}                 //*[@text="BẬT"]
${nut_bat_thuong}          //*[@text="Bật"]
${nut_tat}                 //*[contains(@text,'TẮT')]

# Cac so duoc su dung trong nhieu ung dung
${nut_sao}    xpath=(//*[@resource-id="bkav.android.btalk:id/dialpad_key_number"])[10]
${nut_0}    xpath=//*[@text="0"]
${nut_1}    xpath=//*[@text="1"]
${nut_2}    xpath=//*[@text="2"]
${nut_3}    xpath=//*[@text="3"]
${nut_4}    xpath=//*[@text="4"]
${nut_5}    xpath=//*[@text="5"]
${nut_6}    xpath=//*[@text="6"]
${nut_7}    xpath=//*[@text="7"]
${nut_8}    xpath=//*[@text="8"]
${nut_9}    xpath=//*[@text="9"]
${nut_thang}    xpath=(//*[@resource-id="bkav.android.btalk:id/dialpad_key_number"])[12]

#=================================================================================================
#    CAC PHAN TU TRONG CHUC NANG SUBSIDY
#=================================================================================================


#=================================================================================================
#    CAC PHAN TU TRONG GIAO DIEN DASHBOARD
#=================================================================================================
${db_nut_quet_qr_code_x}    890
${db_nut_quet_qr_code_y}    1240
${db_txt_quet_qr_code}      Hướng máy ảnh vào mã QR hoặc mã vạch

#=================================================================================================
#    CAC PHAN TU TRONG GIAO DIEN LAUNCHER
#=================================================================================================
# Ten cac ung dung trong giao dien launcher
${app_bms}              //*[contains(@text,'BMS')]
${app_bo_suu_tap}       //*[contains(@text,'Bộ sưu tập')]
${app_cai_dat}          //*[contains(@text,'Cài đặt')]
${app_chim_lac}         //*[contains(@text,'Chim Lạc')]
${app_danh_ba}          //*[contains(@text,'Danh bạ')]
${app_dien_thoai}       //*[contains(@text,'Điện thoại')]
${app_dong_ho}          //*[contains(@text,'Đồng hồ')]
${app_eDict}            //*[contains(@text,'eDict')]
${app_esim}             //*[contains(@text,'eSIM')]
${app_ghi_am}           //*[contains(@text,'Ghi âm')]
${app_ghi_chep}         //*[contains(@text,'Ghi chép')]
${app_hinh_nen}         //*[contains(@text,'Hình nền')]
${app_ho_tro}           //*[contains(@text,'Hỗ trợ')]
${app_kham_pha}         //*[contains(@text,'Explore')]
${app_may_anh}          //*[contains(@text,'Máy ảnh')]
${app_may_tinh}         //*[contains(@text,'Máy tính')]
${app_nghe_dai}         //*[contains(@text,'Nghe đài')]
${app_nghe_nhac}        //*[contains(@text,'Nghe nhạc')]
${app_nhac_viec}        //*[contains(@text,'Nhắc việc')]
${app_quan_ly_file}     //*[contains(@text,'Quản lý file')]
${app_quay_man_hinh}    //*[contains(@text,'Quay m.hình')]
${app_suc_khoe}         //*[contains(@text,'Sức khỏe')]
${app_thoi_tiet}        //*[contains(@text,'Thời tiết')]
${app_tin_nhan}         //*[contains(@text,'Tin nhắn')]
${app_xem_phim}         //*[contains(@text,'Xem phim')]

# Cac phan tu launcher
${thu_muc}                            //*[@content-desc="Thư mục: "]
${thu_muc_google}                     bkav.android.launcher3:id/preview_background
${ten_thu_muc}                        bkav.android.launcher3:id/folder_name
${popup_khi_cham_giu_khoang_trang}    bkav.android.launcher3:id/deep_shortcuts_container
${popup_cai_dat}                      //*[contains(@text,'Cài đặt')]
${popup_tien_ich}                     //*[contains(@text,'Tiện ích')]
${popup_hinh_nen}                     //*[contains(@text,'Hình nền')]
${workspace}                          bkav.android.launcher3:id/search_container_workspace
${danh_sach_ung_ung}                  //*[@content-desc="Danh sách ứng dụng"]
${chon_lam_man_hinh_chinh}            //*[contains(@text,'Chọn làm màn hình chính')]
${thanh_tim_kiem}                     com.google.android.googlequicksearchbox:id/default_search_widget
${chu_google}                         com.google.android.googlequicksearchbox:id/search_widget_super_g
${tim_kiem_bang_giong_noi}            com.google.android.googlequicksearchbox:id/search_widget_voice_btn
${logo_google}                        com.google.android.googlequicksearchbox:id/google_logo
${tien_ich_thoi_tiet_preview}         bkav.android.launcher3:id/widget_preview
${tien_ich_thoi_tiet}                 bkav.android.weather:id/widget_layout
${tien_ich_thoi_tiet_dia_diem}        bkav.android.weather:id/name_location
${tien_ich_thoi_tiet_nhiet_do}        bkav.android.weather:id/temp_current
${xoa}                                bkav.android.launcher3:id/delete_target_text

# Cac phan tu trong popup cai dat
${hien_thi_thanh_tim_kiem}               //*[contains(@text,'Hiện Thanh tìm kiếm')]
${vuot_len_de_tim_kiem_ung_dung}         //*[contains(@text,'Vuốt lên để tìm kiếm ứng dụng')]
${hieu_ung_xuat_hien}                    //*[contains(@text,'Hiệu ứng xuất hiện')]
${an_hoac_hien_trang_thai}               //*[contains(@text,'Ẩn hoặc hiện thanh trạng thái')]
${kich_thuoc_thu_muc}                    //*[contains(@text,'Kích thước thư mục')]
${hieu_ung_1}                            //*[contains(@text,'Hiệu ứng 1')]
${hieu_ung_2}                            //*[contains(@text,'Hiệu ứng 2')]
${hieu_ung_3}                            //*[contains(@text,'Hiệu ứng ')]
${hieu_ung_mac_dinh}                     //*[@text="Mặc định"]
${on_off_an_or_hien_thanh_trang_thai}    //*[contains(@resource-id,'android:id/switch_widget')]
${kich_thuoc_4x4}                        //*[@text="4x4"]
${Btalk}                                 //*[@content-desc="Btalk"]

#=================================================================================================
#    CAC PHAN TU TRONG GIAO DIEN NOTIFICATION
#=================================================================================================
${notify_nut_xoa_thong_bao}                         com.android.systemui:id/bkav_remove_notify
${notify_nut_mo_rong}                               xpath=//android.widget.Button[@content-desc="Mở rộng"]
${notify_nut_mo_rong_1}                             xpath=(//android.widget.Button[@content-desc="Mở rộng"])[1]
${notify_nut_thu_gon}                               xpath=//android.widget.Button[@content-desc="Thu gọn"]
${notify_nut_thu_gon_1}                             xpath=(//android.widget.Button[@content-desc="Thu gọn"])[1]
${notify_txt_bat_truyen_tep}                        //*[contains(@text,'Đã bật truyền tệp qua USB')]
${notify_icon_pin}                                  xpath=//android.widget.LinearLayout[@content-desc="Đang sạc pin, 100%."]/android.widget.ImageView
${notify_thong_tin_thoi_gian}                       com.android.systemui:id/date_view_container
${notify_lich_google_title}                         com.google.android.calendar:id/header
${notify_icon_bms}                                  com.android.systemui:id/icon
${notify_debug}                                     //*[contains(@text,'Đã kết nối chế độ gỡ lỗi qua USB')]
${notify_cd_tieu_de_nha_phat_trien}                 com.android.settings:id/action_bar

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BMS
#=================================================================================================
# Cac phan tu trong giao dien chinh
${bms_main_logo}      	              com.bkav.bphone.bms:id/logo
${bms_main_nut_diet_virus}            xpath=//*[contains(@text,'Diệt virus')]

${bms_noti_icon_bms}                  com.android.systemui:id/icon
${bms_dvr_mau_test}                   //*[contains(@text,'BMS_MauTest_2018')]
${bms_app_test_quet_dam_may}          //*[contains(@text,'appvn.apk')]
${bms_nut_cai_dat_hoa}                //*[contains(@text,'CÀI ĐẶT')]

# Cac phan tu trong giao dien Diet virus
${bms_dvr_nut_quet_virus}                  id=com.bkav.bphone.bms:id/scan_button
${nut_quet_toan_bo}                        id=com.bkav.bphone.bms:id/fullScan_Radio
${nut_quet_tung_phan}                      id=com.bkav.bphone.bms:id/scanInstalled_Radio
${bms_dvr_nut_quet}                        xpath=(//*[contains(@text,'Quét')])[4]
${bms_dvr_txt_khuyen_cao1}                 xpath=//*[contains(@text,'Khuyến cáo !')]
${bms_dvr_txt_khuyen_cao2}                 xpath=//*[contains(@text,'Bạn nên bật kết nối mạng (Wifi/3G) để quét hiệu quả hơn với Công nghệ điện toán đám mây!')]
${bms_dvr_cb_chi_bao_cao_khi_quet_xong}    xpath=//*[contains(@text,'Chỉ báo cáo khi quét xong')]
${bms_dvr_pb_thanh_tien_trinh_quet}        id=com.bkav.bphone.bms:id/progressbar_scan_Horizontal
${bms_dvr_txt_dang_quet}                   xpath=//*[contains(@text,'Đang quét')]
${bms_dvr_txt_tong_so_file_da_quet}        xpath=//*[contains(@text,'Tổng số file đã quét:')]
${bms_dvr_txt_so_virus_phat_hien}          xpath=//*[contains(@text,'Số virus phát hiện:')]
${bms_dvr_txt_tu_dong_bao_ve}              xpath=//*[contains(@text,'Tự động bảo vệ!')]

${nut_dat_lich_quet}                                    id=com.bkav.bphone.bms:id/scan_schedule_button
${nut_diet_virrus_tuy_chon}                             id=com.bkav.bphone.bms:id/scan_technology_button
${nut_diet_virus_danh_sach_bo_qua_quang_cao}            id=com.bkav.bphone.bms:id/view_ignore_apps_list_btn
${nut_back_danh_sach_bo_qua_quang_cao}                  id=com.bkav.bphone.bms:id/ib_banner_bms_back_back
${nut_diet_virus_nhat_ky}                               id=com.bkav.bphone.bms:id/history_virus_button

# Cac phan tu trong chan tin nhan va cuoc goi rac
${nut_chan_tin_nhan_rac}                                id=com.bkav.bphone.bms:id/tv_layout_spam_title
${nut_tin_nhan_danh_sach_den}                           id=com.bkav.bphone.bms:id/ll_fragment_spam_black_list_select
${nut_tin_nhan_danh_sach_trang}                         id=com.bkav.bphone.bms:id/ll_fragment_spam_white_list_select
${nut_tin_nhan_tranh_lam_phien}                         id=com.bkav.bphone.bms:id/ll_fragment_spam_private_select
${nut_tin_nhan_chan_thao_noi_dung}                      id=com.bkav.bphone.bms:id/ll_fragment_spam_block_content_select
${nut_tin_nhan_nhat_ki_chan}                            id=com.bkav.bphone.bms:id/ll_fragment_spam_diary_select
${nut_tin_nhan_huong_dan_su_dung}                       id=com.bkav.bphone.bms:id/ib_layout_spam_help

# Cac phan tu trong man chan cuoc goi rac
${nut_chan_cuoc_goi_rac}                                id=com.bkav.bphone.bms:id/tv_layout_spam_call_title
${nut_cuoc_goi_rac_danh_sach_den}                       id=com.bkav.bphone.bms:id/ll_fragment_spam_call_black_list_select
${nut_cuoc_goi_rac_danh_sach_trang}                     id=com.bkav.bphone.bms:id/ll_fragment_spam_call_white_list_select
${nut_cuoc_goi_rac_tranh_lam_phien}                     id=com.bkav.bphone.bms:id/ll_fragment_spam_call_private_select
${nut_cuoc_goi_rac_nhat_ky_chan}                        id=com.bkav.bphone.bms:id/ll_fragment_spam_call_diary_select
${nut_cuoc_goi_rac_huong_dan_su_dung}                   id=com.bkav.bphone.bms:id/ib_layout_spam_call_help

# Cac phan tu trong giao dien bao ve giao dich
${nut_bao_ve_giao_dich_ngan_hang}                       id=com.bkav.bphone.bms:id/tv_layout_privacy_title
${nut_bao_ve_giao_dich_ngan_hang_quet}                  id=com.bkav.bphone.bms:id/b_fragment_privacy_scan

#sau khi quet xong o man baop ve tai khoan ngan hang hien thi
${nut_truy_cap_danh_ba}                                 id=com.bkav.bphone.bms:id/contactsnumbertit

# Cac phan tu trong man chong trom
${nut_chong_trom}                                       id=com.bkav.bphone.bms:id/tv_layout_anti_theft_title
${nut_so_dien_thoai_du_phong}                           id=com.bkav.bphone.bms:id/iv_fragment_anti_theft_change_phone_second
${nut_back_sdt_du_phong}                                id=com.bkav.bphone.bms:id/ib_banner_bms_notice_back
${nut_chon_noi_dung_man_hinh_khoa_chong_trom}           id=com.bkav.bphone.bms:id/iv_fragment_anti_theft_change_content_lock_screen
${nut_close_chon_noi_dung_man_hinh_khoa_chong_trom}     id=com.bkav.bphone.bms:id/b_dialog_info_close

# Cac phan tu man khac
${nut_don_rac_toi_uu}                                   id=com.bkav.bphone.bms:id/tv_layout_clean_title
${nut_chan_lam_phien}                                   id=com.bkav.bphone.bms:id/tv_layout_disturb_block_title
${nut_an_noi_dung_rieng_tu}                             id=com.bkav.bphone.bms:id/tv_layout_private_content_title
${nut_sao_luu_du_lieu}                                  id=com.bkav.bphone.bms:id/tv_layout_backup_title
${nut_ban_quyen}                                        id=com.bkav.bphone.bms:id/tv_layout_license_title

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BO SUU TAP
#=================================================================================================
${btn_dong_thoi_gian}        //*[@content-desc="Ảnh"]
# ${btn_dong_thoi_gian}        //*[@resource-id="org.codeaurora.gallery:id/action_camera"]
# ${bst_xem_anh}      //*[@content-desc="Ảnh"]
${bst_xem_anh}      //*[@text="Dòng thời gian"]

# Cac phan tu TRONG MAN ALBUM
${bst_album_dtg_nut_anh}            xpath=//*[@content-desc="Ảnh"]
${bst_album_man_album}              xpath=//*[@content-desc="Album"]
${bst_album_man_quay_phim}          xpath=//*[@content-desc="Quay phim"]
${bst_album_man_thung_rac}          xpath=//*[@content-desc="Thùng rác"]
${bst_album_thung_rac_mess}         id=org.codeaurora.gallery:id/tv_empty_folder

${bst_album_dtg_nut_ngay}           id=org.codeaurora.gallery:id/tvDay
${bst_album_nut_them_moi}           id=org.codeaurora.gallery:id/iv_floating_button
${bst_album_nut_chia_se}            id=org.codeaurora.gallery:id/share_button
${bst_album_nut_xoa}                id=org.codeaurora.gallery:id/delete_button

${bst_album_chup_man_hinh}          xpath=//*[contains(@text,'Chụp màn hình')]
${bst_album_may_anh}                xpath=//*[contains(@text,'Máy ảnh')]
${bst_album_test}                   xpath=//*[contains(@text,'test')]
${bst_album_test@😂}                xpath=//*[contains(@text,'test@😂')]
${bst_album_Download}                          xpath=//*[contains(@text,'Download')]

${bst_album_tabbar_them_vao_album}             id=org.codeaurora.gallery:id/add_to_album_button_container
${bst_album_tabbar_xoa}                        id=org.codeaurora.gallery:id/delete_button
${bst_album_title_1_muc_duoc_chon}             xpath=//*[contains(@text,'1 mục đã được chọn')]
${bst_album_title_2_muc_duoc_chon}             xpath=//*[contains(@text,'2 mục đã được chọn')]
${bst_album_toolbar_nut_bo_chon_tat_ca}        id=org.codeaurora.gallery:id/iv_exit_toolbar
${bst_album_tick_chon_dat_ten_album}           id=org.codeaurora.gallery:id/iv_done
${bst_album_nhap_ten_album}                    id=org.codeaurora.gallery:id/edt_tv_title
${bst_album_anh_vi_tri_1}                      xpath=(//*[@resource-id="org.codeaurora.gallery:id/image"])[1]

${bst_album_text_di_chuyen_hay_sao_chep}       id=org.codeaurora.gallery:id/vw_dialog_tv_title
${bst_album_nut_sao_chep}                      id=org.codeaurora.gallery:id/vw_dialog_tv_cancel
${bst_album_nut_di_chuyen}                     id=org.codeaurora.gallery:id/vw_dialog_tv_ok

${bst_album_app_bluetooth}           xpath=//*[contains(@text,'Bluetooth')]
${bst_album_app_maps}                xpath=//*[contains(@text,'Maps')]
${bst_album_app_btalk}               xpath=//*[contains(@text,'Btalk')]
${bst_album_app_facebook}            xpath=//*[contains(@text,'Facebook')]

${bst_QLF_anh}                       xpath=//*[contains(@text,'Ảnh')]
${bst_QLF_nut_nhieu_hon}             id=com.bkav.android.bexplorer:id/main_fl_more
${bst_QLF_doi_ten}                   xpath=//*[contains(@text,'Đổi tên')]
${bst_QLF_text_bbb}                  xpath=//*[contains(@text,'bbb')]
${bst_QLF_nhap_ten_album_moi}        id=com.bkav.android.bexplorer:id/etFileName
${bst_QLF_nut_dong_y}                id=com.bkav.android.bexplorer:id/bt_ok_dialog
${bst_album_camera3}                 xpath=//*[contains(@text,'camera3')]
${bst_album_dem_anh}                 xpath=//android.widget.ImageView[@content-desc="Ảnh"]

# Cac phan tu TRONG MAN DONG THOI GIAN
${bst_dtg_hom_qua}                   xpath=//*[contains(@text,'Hôm qua')]
${bst_dtg_hom_nay}                   xpath=//*[contains(@text,'Hôm nay')]
${bst_dtg_nam_2021}                  xpath=//*[contains(@text,'2021')]

${bst_dtg_text_nam}                  id=org.codeaurora.gallery:id/tvYear
${bst_dtg_text_thang}                id=org.codeaurora.gallery:id/tvMonth
${bst_dtg_nut_selectall_toolbar}     id=org.codeaurora.gallery:id/iv_selectall_toolbar
${bst_dtg_nut_exit_toolbar}          id=org.codeaurora.gallery:id/iv_exit_toolbar
${bst_dtg_title_toolbar}             id=org.codeaurora.gallery:id/tv_title_toolbar

# Cac phan tu TRONG MAN THUMBNAIL
${bst_TN_nut_chia_se}                    id=org.codeaurora.gallery:id/share_button
${bst_TN_nut_them_vao_album}             id=org.codeaurora.gallery:id/add_album_button
${bst_TN_nut_xoa}                        id=org.codeaurora.gallery:id/delete_button
${bst_TN_nut_dat_lam}                    id=org.codeaurora.gallery:id/set_as_button
${bst_TN_nut_ngay}                       id=org.codeaurora.gallery:id/tvDay
${bst_TN_toolbar_tick _chon_tat_ca}      id=org.codeaurora.gallery:id/iv_selectall_toolbar
${bst_TN_toolbar_exit_x}                 id=org.codeaurora.gallery:id/exit_toolbar_container
${bst_TN_chon_1_anh}                     xpath=//*[contains(@text,'1 mục đã được chọn')]
${bst_TN_chon_3_anh}                     xpath=//*[contains(@text,'3 mục đã được chọn')]
${bst_TN_chon_4_anh}                     xpath=//*[contains(@text,'4 mục đã được chọn')]
${bst_TN_anh_1}                          xpath=(//*[@resource-id="org.codeaurora.gallery:id/image"])[1]
${bst_TN_anh_2}                          xpath=(//*[@resource-id="org.codeaurora.gallery:id/image"])[2]
${bst_TN_anh_3}                          xpath=(//*[@resource-id="org.codeaurora.gallery:id/image"])[3]
${bst_TN_anh_4}                          xpath=(//*[@resource-id="org.codeaurora.gallery:id/image"])[4]
${bst_TN_anh_5}                          xpath=(//*[@resource-id="org.codeaurora.gallery:id/image"])[5]
${bst_TN_anh_6}                          xpath=(//*[@resource-id="org.codeaurora.gallery:id/image"])[6]
${bst_TN_video_20}                       xpath=(//android.widget.ImageView[@content-desc="Ảnh"])[20]

${bst_TN_nut_huy}                        id=org.codeaurora.gallery:id/tv_cancel
${bst_thumbnail_btn_dong_y}              id=org.codeaurora.gallery:id/tv_ok
${bst_thumbnail_nutt_album_moi}          id=org.codeaurora.gallery:id/tv_new_album
${bst_thumbnail_nut_album_moi}           xpath=//*[@text="ALBUM MỚI"]
${bst_thumbnail_title_anh}               id=org.codeaurora.gallery:id/tv_title_toolbar

${bst_TN_text_video_dat_lam}             xpath=//*[contains(@text,'Không ứng dụng nào có thể thực hiện tác vụ này.')]
${bst_TN_text_dat_lam_hinh_nen}          id=bkav.android.launcher3:id/set_wallpaper_crop
${bst_TN_text_dat_lam_ca_hai}            id=bkav.android.launcher3:id/set_both_wallpaper_crop
${bst_TN_text_dat_lam_man_hinh_khoa}     id=bkav.android.launcher3:id/set_lockscreen_wallpaper_crop

${bst_TN_album_moi_nut_huy}              id=org.codeaurora.gallery:id/tv_negative
${bst_TN_album_moi_nut_tao}              id=org.codeaurora.gallery:id/tv_positive
${bst_TN_album_moi_edit_text}            id=org.codeaurora.gallery:id/edit_text
${bst_TN_popup_di_chuyen_sao_chep}       id=org.codeaurora.gallery:id/vw_dialog_tv_title

${bst_album_moi_nut_sao_chep}                         id=org.codeaurora.gallery:id/vw_dialog_tv_cancel
${bst_album_moi_nut_di_chuyen}                        id=org.codeaurora.gallery:id/vw_dialog_tv_ok

${bst_thumbnail_album_thu_1}                          xpath=(//*[contains(@resource-id,'org.codeaurora.gallery:id/count')])[1]
${bst_thumnail_co_3_anh}                              xpath=//*[contains(@text,'3')]
${bst_thumnail_co_2_anh}                              xpath=//*[contains(@text,'2')]
${bst_thumbnail_album_thu_1_sau_khi_tao_1_album}      xpath=(//*[contains(@resource-id,'org.codeaurora.gallery:id/count')])[2]
${bst_thumbnail_album_thu1_sau_khi_tao_2_album}       xpath=(//*[contains(@resource-id,'org.codeaurora.gallery:id/count')])[3]
${bst_thumbnail_album_tien@😂}                        xpath=//*[contains(@text,'tien@😂')]
${bst_thumbnail_dat_lam_mess}                         xpath=//*[contains(@text,'Không ứng dụng nào có thể thực hiện tác vụ này.')]

# Cac phan tu TRONG MAN THUNG RAC
${bst_TR_anh_7}                           xpath=(//*[@resource-id="org.codeaurora.gallery:id/image"])[7]
${bst_TR_anh_8}                           xpath=(//*[@resource-id="org.codeaurora.gallery:id/image"])[8]
${bst_TR_anh_9}                           xpath=(//*[@resource-id="org.codeaurora.gallery:id/image"])[9]
${bst_TR_anh_10}                          xpath=(//*[@resource-id="org.codeaurora.gallery:id/image"])[10]

${bst_TR_thung_rac_rong}                  id=org.codeaurora.gallery:id/tv_empty_folder
${bst_TR_thung_rac_anh_1}                 xpath=(//android.widget.ImageView[@content-desc="Ảnh"])[1]
${bst_TR_thung_rac_anh_2}                 xpath=(//android.widget.ImageView[@content-desc="Ảnh"])[2]
${bst_TR_thung_rac_anh_3}                 xpath=(//android.widget.ImageView[@content-desc="Ảnh"])[3]
${bst_TR_thung_rac_anh_4}                 xpath=(//android.widget.ImageView[@content-desc="Ảnh"])[4]

${bst_TR_nut_xoa}                         xpath=//android.widget.ImageView[@content-desc="Xóa"]
${bst_TR_nut_dong_y}                      id=org.codeaurora.gallery:id/vw_dialog_tv_ok
${bst_TR_nut_huy}                         id=org.codeaurora.gallery:id/vw_dialog_tv_cancel
${bst_TR_title_xoa_khoi_thung_rac}        id=org.codeaurora.gallery:id/vw_dialog_tv_title
${bst_TR_selectall_toolbar}               id=org.codeaurora.gallery:id/iv_selectall_toolbar
${bst_TR_nut_khoi_phuc}                   id=org.codeaurora.gallery:id/restore_button

# Cac phan tu TRONG MAN XEM ANH
${bst_xem_anh_anh_1}                       xpath=(//*[@resource-id="org.codeaurora.gallery:id/image"])[1]
${bst_xem_anh_nut_exit_toolbar}            id=org.codeaurora.gallery:id/rl_exit_toolbar_container
${bst_xem_anh_nut_more_func}               id=org.codeaurora.gallery:id/rl_more_func_container
${bst_xem_anh_nut_thong_tin}               xpath=//android.widget.ImageView[@content-desc="Thông tin"]
${bst_xem_anh_nut_chia_se}                 xpath=//android.widget.ImageView[@content-desc="Chia sẻ"]
${bst_xem_anh_nut_chinh_sua}               id=org.codeaurora.gallery:id/edit_button

${bst_xem_anh_nut_xoa}                     xpath=//android.widget.ImageView[@content-desc="Xóa"]
${bsT_xem_anh_nut_khoi_phuc}               xpath=//android.widget.ImageView[@content-desc="Khôi phục"]

${bst_xem_anh_popup_thong_tin}                 id=org.codeaurora.gallery:id/alertTitle
${bst_xem_anh_thong_tin_ten}                   xpath=//*[contains(@text,'Tên:')]
${bst_xem_anh_thong_tin_ngay}                  xpath=//*[contains(@text,'Ngày:')]
${bst_xem_anh_thong_tin_kich_thuoc}            xpath=//*[contains(@text,'Kích thước:')]
${bst_xem_anh_thong_tin_dung_luong}            xpath=//*[contains(@text,'Dung lượng:')]
${bst_xem_anh_thong_tin_trinh_tao}             xpath=//*[contains(@text,'Trình tạo:')]
${bst_xem_anh_thong_tin_thiet_bi}              xpath=//*[contains(@text,'Thiết bị:')]
${bst_xem_anh_thong_tin_chieu}                 xpath=//*[contains(@text,'Chiều:')]
${bst_xem_anh_thong_tin_man_chap}              xpath=//*[contains(@text,'Màn trập:')]
${bst_xem_anh_thong_tin_iso}                   xpath=//*[contains(@text,'ISO:')]
${bst_xem_anh_thong_tin_den_flash}             xpath=//*[contains(@text,'Đèn flash:')]
${bst_xem_anh_thong_tin_khau_do}               xpath=//*[contains(@text,'Khẩu độ:')]
${bst_xem_anh_thong_tin_tieu_cu}               xpath=//*[contains(@text,'Tiêu cự:')]
${bst_xem_anh_thong_tin_duong_dan}             xpath=//*[contains(@text,'Đường dẫn:')]
${bst_xem_anh_thong_tin_toc_do_khung_hinh}     xpath=//*[contains(@text,'Tốc độ khung hình:')]
${bst_xem_anh_app_facebook}                    xpath=//*[contains(@text,'Facebook')]
${bst_xem_anh_thong_tin_dong}                  xpath=//*[contains(@text,'ĐÓNG')]

${bst_xem_anh_chinh_anh_nut_imgComparison}        id=org.codeaurora.gallery:id/imgComparison
${bst_xem_anh_chinh_anh_nut_huy}                  id=org.codeaurora.gallery:id/exit
${bst_xem_anh_chinh_anh_xuat}                     id=org.codeaurora.gallery:id/export
${bst_xem_anh_chinh_anh_dat_lai}                  id=org.codeaurora.gallery:id/reset
${bst_xem_anh_chinh_anh_luu}                      id=org.codeaurora.gallery:id/save
${bst_xem_anh_chinh_anh_nut_fxbutton}             id=org.codeaurora.gallery:id/fxButton
${bst_xem_anh_chinh_anh_nut_geometry}             id=org.codeaurora.gallery:id/geometryButton
${bst_xem_anh_chinh_anh_nut_colors}               id=org.codeaurora.gallery:id/colorsButton
${bst_xem_anh_chinh_anh_nut_waterMark}            id=org.codeaurora.gallery:id/waterMarkButton
${bst_xem_anh_chinh_sua_nut_edit_photo}           id=org.codeaurora.gallery:id/editphoto
${bst_xem_anh_chinh_sua_nut_cat}                  xpath=//*[contains(@text,'Cắt')]
${bst_xem_anh_chinh_sua_text_khong_co}            xpath=//*[contains(@text,'Không có')]
${bst_xem_anh_chinh_sua_text_goc}                 xpath=//*[contains(@text,'Gốc')]
${bst_xem_anh_chinh_sua_text_11}                  xpath=//*[contains(@text,'1:1')]
${bst_xem_anh_chinh_sua_text_huy_bo}              id=org.codeaurora.gallery:id/cancel
${bst_xem_anh_chinh_sua_text_luu}                 id=org.codeaurora.gallery:id/done
${bst_xem_anh_chinh_Sua_nut_lam_phang}            xpath=//*[contains(@text,'Làm thẳng')]
${bst_xem_anh_chinh_sua_nut_xoay}                 xpath=//*[contains(@text,'Xoay')]

${bst_xem_anh_do_sang_tu_dong}                    xpath=//android.view.View[@content-desc="Màu tự động"]
${bst_xem_anh_so_sang_phoi_sang}                  xpath=//android.view.View[@content-desc="Độ phơi sáng"]
${bst_xem_anh_do_sang_tuong_phan}                 xpath=//android.view.View[@content-desc="Độ tương phản"]
${bst_xem_anh_do_sang_dao_dong}                   xpath=//android.view.View[@content-desc="Dao động"]

${bst_xem_anh_icon_vi_tri}                        xpath=//android.view.View[@content-desc="Vị trí"]
${bst_xem_anh_icon_thoi_gian}                     xpath=//android.view.View[@content-desc="Thời gian"]
${bst_xem_anh_icon_thoi_tiet}                     xpath=//android.view.View[@content-desc="Thời tiết"]
${bst_xem_anh_chen_icon_cam_xuc}                  xpath=//android.view.View[@content-desc="Cảm xúc"]
${bst_xem_anh_chen_icon_1}                        xpath=//android.widget.HorizontalScrollView/android.widget.LinearLayout/android.view.View[1]
${bst_xem_anh_chen_icon_1_da_chen}                id=org.codeaurora.gallery:id/image
${bst_xem_anh_chen_icon_2}                        xpath=//android.widget.HorizontalScrollView/android.widget.LinearLayout/android.view.View[2]
${bst_xem_anh_chen_anh_title_huy}                 id=android:id/alertTitle
${bst_xem_anh_chen_icon_mess}                     id=android:id/message
${bst_xem_anh_chen_icon_nut_Huy}                  id=android:id/button2
${bst_xem_anh_chen_icon_nut_huy_va_thoat}         id=android:id/button1

${bst_xem_anh_mau_khong_co}                       xpath=//android.view.View[@content-desc="Không có"]
${bst_xem_anh_punch}                              xpath=//android.view.View[@content-desc="Punch"]
${bst_xem_anh_vintage}                            xpath=//android.view.View[@content-desc="Vintage"]
${bst_xem_anh_BM}                                 xpath=//android.view.View[@content-desc="B/W"]

${bst_xem_anh_brush_control}            id=bkav.android.editphoto:id/brush_controls
${bst_xem_anh_color_select}             id=bkav.android.editphoto:id/color_select_controls
${bst_xem_anh_erase_control}            id=bkav.android.editphoto:id/erase_control
${bst_xem_anh_icon_control}             id=bkav.android.editphoto:id/erase_control
${bst_xem_anh_text_control}             id=bkav.android.editphoto:id/text_control
${bst_xem_anh_menu_control}             id=bkav.android.editphoto:id/menu_control

${bst_xem_anh_cat_anh_leftText}         id=org.codeaurora.gallery:id/leftButton
${bst_xem_anh_ba_cham_mo_voi}           id=org.codeaurora.gallery:id/open_with_button
${bst_xem_anh_ba_cham_dung_lam}         id=org.codeaurora.gallery:id/set_as_button
${bst_xem_anh_ba_cham_in}               id=org.codeaurora.gallery:id/print_button
${bst_xem_anh_ba_cham_doi_ten}          id=org.codeaurora.gallery:id/rename_button

${bst_xem_anh_mo_voi_app_chia_se_bst}        xpath=//*[contains(@text,'Bộ sưu tập')]
${bst_xem_anh_mo_voi_app_chia_se_anh}        xpath=//*[contains(@text,'Ảnh')]
${bst_xem_anh_dat_lam_hinh_nen}              id=bkav.android.launcher3:id/set_wallpaper_crop
${bst_xem_anh_dat_lam_hinh_khoa}             id=bkav.android.launcher3:id/set_lockscreen_wallpaper_crop
${bst_xem_anh_dat_lam_ca_hai}                id=bkav.android.launcher3:id/set_both_wallpaper_crop
${bst_xem_anh_in_chon_may_in}                xpath=//*[contains(@text,'Chọn máy in')]
${bst_xem_anh_in_ban_sao}                    xpath=//*[contains(@text,'Bản sao:')]
${bst_xem_anh_in_kho_giay}                   xpath=//*[contains(@text,'Khổ giấy:')]
${bst_xem_anh_doi_ten}                       xpath=//*[contains(@text,'Đổi tên')]
${bst_Xem_anh_edit_text}                     id=org.codeaurora.gallery:id/edit_text
${bst_Xem_anh_dat_ten_nut_HUY}               id=org.codeaurora.gallery:id/tv_negative
${bst_Xem_anh_dat_ten_nut_DONG_Y}            id=org.codeaurora.gallery:id/tv_positive
${bst_xem_anh_dat_ten_ky_tu_dac_biet}        xpath=//*[contains(@text,'test@😂')]

# Cac phan tu TRONG MAN VIDEO
${bst_video_nut_play}                    id=org.codeaurora.gallery:id/play_pause_button
${bst_video_nut_over_play}               id=com.bkav.video:id/bkav_player_overlay_play
${bst_video_nut_thong_tin}               id=org.codeaurora.gallery:id/info_button
${bst_video_nut_chia_se}                 id=org.codeaurora.gallery:id/share_button
${bst_video_nut_cat}                     id=org.codeaurora.gallery:id/trim_button
${bst_video_thung_rac}                   id=org.codeaurora.gallery:id/delete_button

${bst_video_thu_1}                       xpath=(//android.widget.ImageView[@content-desc="Ảnh"])[1]
${bst_video_video_quay_cham_2}           xpath=(//android.widget.ImageView[@content-desc="Ảnh"])[2]

${bst_video_cat_handler_top}             id=org.codeaurora.gallery:id/handlerTop
${bst_video_cat_huy}                     id=org.codeaurora.gallery:id/rl_btn_cancel
${bst_video_cat_luu}                     id=org.codeaurora.gallery:id/rl_btn_done

${bst_video_nut_play_tren_video_thuong}           id=com.bkav.video:id/bkav_player_overlay_play
${nut_lock}                                       id=com.bkav.video:id/bkav_lock_overlay_button
${nut_track}                                      id=com.bkav.video:id/bkav_player_overlay_tracks
${nut_functions}                                  id=com.bkav.video:id/bkav_player_overlay_adv_function
${nut_size}                                       id=com.bkav.video:id/bkav_player_overlay_size
${thanh_seebbar}                                  id=com.bkav.video:id/bkav_player_overlay_seekbar
${get_text_thoi_gian_dung}                        id=com.bkav.video:id/bkav_player_overlay_time
${get_text_am_luong}                              id=com.bkav.video:id/bkav_player_overlay_info
${bst_video_do_sang_man_hinh}                     id=com.bkav.video:id/bkav_player_overlay_info
${bst_video_cham_thanh_thoi_gian}                 xpath=//android.widget.FrameLayout[2]/android.view.View[3]
${bst_video_cham_time_bar}                        xpath=//android.view.View[@content-desc="Video player time bar"]
${bst_video_cham_nut_chup_man_hinh}               xpath=//android.widget.FrameLayout[2]/android.widget.ImageView[2]
${bst_video_cham_nut_play}                        xpath=//android.widget.ImageView[@content-desc="Play video"]
${bst_video_cham_nut_pause}                       xpath=//android.widget.ImageView[@content-desc="Pause video"]
${bst_video_cham_nut_vong_lap}                    xpath=//android.widget.FrameLayout[2]/android.widget.ImageView[4]

${bst_video_thuong_nut_play}                      xpath=//android.widget.ImageView[@content-desc="Play video"]
${bst_video_thuong_time_bar}                      xpath=//android.view.View[@content-desc="Video player time bar"]
${bst_video_thuong_nut_pause}                     xpath=//android.widget.ImageView[@content-desc="Pause video"]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BTALK - CHUC NANG DIEN THOAI
#=================================================================================================
# Cac chuc nang trong ung dung Btalk
${btalk_dien_thoai}    xpath=//*[@text="Điện thoại"]
${btalk_tin_nhan}      xpath=//*[@text="Tin nhắn"]
${btalk_gan_day}       xpath=//*[@text="Gần đây"]
${btalk_danh_ba}       xpath=//*[@text="Danh bạ"]

${dt_cai_dat}          xpath=//*[@content-desc="Thêm tùy chọn"]
${dt_nut_sao}          xpath=(//*[@content-desc="*"])[1]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BTALK - CHUC NANG TIN NHAN
#=================================================================================================
${tn_btn_tin_nhan}                   //*[@text="Tin nhắn"]
${tn_txt_huy}                        HỦY
${tn_txt_dong y}                     ĐỒNG Ý
${tn_txt_dat_lam_mac_dinh}           ĐẶT LÀM MẶC ĐỊNH
${tn_txt_dat_sms_mac_dinh}           Bạn muốn đặt Btalk làm ứng dụng SMS mặc định?
${tn_txt_am_thanh_mac_dinh}          Âm thanh thông báo mặc định
${tn_txt_gui_mms}                    Gửi một MMS cho tất cả người nhận
${tn_txt_khong_co_thong_bao}         Không có thông báo nào
# ${tn_pupup}               bkav.android.btalk:id/message_setting
${tn_tim_kiem}               bkav.android.btalk:id/img_ic_search_expand
${tn_3_cham}                 bkav.android.btalk:id/imb_over_flow
${tn_3_da_luu_tru}           //*[@text="Đã lưu trữ"]
${tn_3_danh_dau_da_doc}      //*[@text="Đánh dấu tất cả đã đọc"]
${tn_3_cai_dat}              //*[@text="Cài đặt"]
${dt_3_cham}                 bkav.android.btalk:id/dialpad_overflow

# Giao dien cai dat chung
${tn_cd_back}              //*[@content-desc="Điều hướng lên trên"]
${tn_cd_sms_mac_dinh}      //*[@text="Ứng dụng SMS mặc định"]
${tn_cd_btalk}             //*[@text="Btalk"]
${tn_cd_am_thanh_tn}       //*[@text="Âm thanh tin nhắn đi"]
${tn_cd_thong_bao_tn}      //*[@text="Cài đặt thông báo tin nhắn"]
${tn_cd_bao_cao_sms}       //*[@text="Báo cáo gửi SMS"]
${tn_cd_bao_cao_yc}        //*[@text="Yêu cầu báo cáo gửi cho mỗi SMS bạn gửi"]
${tn_cd_hien_avata}        //*[@text="Hiện avatar ở mỗi tin nhắn"]
${tn_cd_thong_tin_ct}      //*[@text="Hiện thông tin chi tiết ở mỗi tin nhắn"]
${tn_cd_thong_tin_sim}     //*[@text="Hiện thông tin sim của cuộc hội thoại"]
${tn_cd_nang cao}          //*[@text="Nâng cao"]
${tn_cd_nut_am_thanh}      xpath=(//*[@resource-id="android:id/switch_widget"])[1]
${tn_cd_nut_bao_cao}       xpath=(//*[@resource-id="android:id/switch_widget"])[2]
${tn_cd_nut_avata}         xpath=(//*[@resource-id="android:id/switch_widget"])[3]
${tn_cd_nut_thong_tin}     xpath=(//*[@resource-id="android:id/switch_widget"])[4]
${tn_cd_nut_thong_tin_sim}         xpath=(//*[@resource-id="android:id/switch_widget"])[5]
${tn_logo}                 bkav.android.btalk:id/img_background_expand_layout
${tn_cd_sms_md_messenger}                 //*[@text="Messenger"]
${tn_cd_sms_md_tim_kiem}                  //*[@content-desc="Tìm kiếm trong các mục cài đặt"]
${tn_cd_sms_btalk_mac_dinh}               //*[@text="Để gửi tin nhắn hãy đặt Btalk là ứng dụng nhắn tin SMS mặc định"]
${tn_cd_sms_thay_doi}                     //*[@text="Thay đổi"]

# Cac phan tu cai dat thong bao tin nhan
${tn_cd_tb_loai_tb}                           //*[@text="Loại thông báo"]
${tn_cd_tb_canh bao}                          //*[@text="Cảnh báo"]
${tn_cd_tb_im_lang}                           //*[@text="Im lặng"]
${tn_cd_tb_nang_cao}                          //*[@text="Nâng cao"]
${tn_cd_tb_hien_thi_tren_mh}                  //*[@text="Hiển thị trên màn hình"]
${tn_cd_tb_am_bao}                            //*[@text="Âm báo"]
${tn_cd_tb_rung}                              //*[@text="Rung"]
${tn_cd_tb_man_hinh khoa}                     //*[@text="Màn hình khóa"]
${tn_cd_tb_nhap_nhay_den}                     //*[@text="Nhấp nháy đèn"]
${tn_cd_tb_ht_dc_tn}                          //*[@text="Hiển thị dấu chấm thông báo"]
${tn_cd_tb_ghi_de}                            //*[@text="Ghi đè Không làm phiền"]
${tn_cd_tb_bo_sung_ung_dung}                  //*[@text="Cài đặt bổ sung trong ứng dụng"]
${tn_cd_tb_thu_nho}                           //*[@text="Thu nhỏ"]
${tn_cd_tb_mhk_text1}                         //*[@text="Không hiển thị thông báo nào"]

# Hien avata o moi tin nhan
${tn_cd_avata_icon}                 bkav.android.btalk:id/conversation_photo_icon
${tn_lien_he}                      //*[@resource-id="bkav.android.btalk:id/swipeableContent"]
${tn_chi_tiet_tn}                  //*[@resource-id="bkav.android.btalk:id/message_status"]

# Cac phan tu tin nhan cai dat nang cao
${tn_cd_nc_nhan_tin_theo_nhom}               //*[@text="Nhắn tin theo nhóm"]
${tn_cd_nc_so_dien_thoai}                    //*[@text="Số điện thoại của bạn"]
${tn_cd_nc_tu_dong_truy_xuat}                //*[@text="Tự động truy xuất"]
${tn_cd_nc_tu_dong_chuyen_vung}              //*[@text="Tự động truy xuất khi chuyển vùng"]
${tn_cd_nc_canh_bao_khong_day}               //*[@text="Cảnh báo không dây"]
${tn_cd_nc_gui tung_tn_sms}                  bkav.android.btalk:id/disable_group_mms_button
${tn_cd_nc_gui_mot_mms}                      bkav.android.btalk:id/enable_group_mms_button
${tn_cd_nc_text_sdt}                         android:id/edit

# Tin nhan man nhan tin /danh sach cuoc tro chuyen
${tn_go_noi_dung_tin_nhan}                   id=bkav.android.btalk:id/compose_message_text
${tn_them_tin_nhan}                          xpath=//android.widget.ImageView[@content-desc="Bắt đầu cuộc trò chuyện mới"]
${tn_nut_gui}                                xpath=//android.widget.ImageButton[@content-desc="Gửi tin nhắn"]
${tn_themmoi_nhap_sdt_gui}                           id=bkav.android.btalk:id/recipient_text_view

# Data luu tai may
${dt_sdt_sim_test}             xpath=//*[@text="0823390409"]

# Cac phan tu sau khi bam nut them tuy chon
${dt}                                                      xpath=//*[@text="Điện thoại"]
${cd_nut_dien_thoai}                                       id=bkav.android.btalk:id/phone_setting
${dt_cd_nut_tuy_chon_hien_thi}                             xpath=//*[@text="Tùy chọn hiển thị"]
${dt_cd_nut_am_thanh_va_rung}                              xpath=//*[@text="Âm thanh và rung"]
${dt_cd_nut_tra_loi_nhanh}                                 xpath=//*[@text="Trả lời nhanh"]
${dt_cd_tai_khoan_goi}                                     xpath=//*[@text="Tài khoản gọi"]
${dt_cd_nut_tro_nang}                                      xpath=//*[@text="Trợ năng"]
${dt_cd_nut_cd_mo_ung_dung}                                xpath=//*[@text="Cài đặt mở ứng dụng"]
${dt_cd_nut_cd_quay _so_nhanh}                             xpath=//*[@text="Cài đặt quay số nhanh"]
${dt_cd_nut_chan_cuoc_goi}                                 xpath=//*[@text="Chặn cuộc gọi"]
${dt_cd_back}                                              xpath=//*[@content-desc="Điều hướng lên trên"]

# Cac phan tu sau khi chon am thanh rung
${dt_cd_checkbox_tuy_chon_hien_thi}                        id=android:id/checkbox
${dt_cd_nut_nhac_chuong_dien_thoai}                        xpath=//*[@text="Nhạc chuông điện thoại"]
${dt_cd_nut_nhac chuong_khong}                             xpath=//*[@text="Không"]
${dt_cd_nut_dong_y}                                        xpath=//*[@text="ĐỒNG Ý"]
${dt_cd_nut_huy}                                           xpath=//*[@text="HỦY"]
${dt_cd_nut_nut_nhac_chuong_hop_nhac}                      xpath=//*[@text="Hộp nhạc"]
${dt_cd_nut_chi_mot_lan}                                   xpath=//*[@text="CHỈ MỘT LẦN"]
${dt_cd_nut_luon_chon}                                     xpath=//*[@text="LUÔN CHỌN"]
${dt_cd_nut_quan_li_file}                                  xpath=//*[@text="Quản lý file"]
${dt_cd_nut_bo_nho_phuong_tien}                            xpath=//*[@text="Bộ nhớ phương tiện"]
${dt_cd_nut_nhac_chuong_sim1}                              xpath=//*[@text="Nhạc chuông Sim 1"]
${dt_cd_ten_nhac_chuong_sim1}                              xpath=//android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.TextView[2]
${dt_cd_ten_nhac_chuong_sim2}                              xpath=//android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.TextView[2]
${dt_cd_nut_nhac_chuong_sim2}                              xpath=//*[@text="Nhạc chuông Sim 2"]
${dt_cd_checkbox_rung_khi_co_cuoc_goi}                     xpath=(//*[@resource-id="android:id/checkbox"])[1]
${dt_cd_checkbox_am_ban_phim_so}                           xpath=(//*[@resource-id="android:id/checkbox"])[2]

# Cac phan tu sau khi bam tra loi nhanh
${dt_cd_nut_checkbox_tra_loi_nhanh}                        xpath=(//*[@resource-id="android:id/checkbox"])

# Cac phan tu sau khi bam tai khoan goi
${dt_cd_checkbox_tu_dong_ghi_am_cuoc_goi}                  xpath=(//*[@resource-id="android:id/checkbox"])[1]
${dt_cd_checkbox_Hinh_nen_cuoc_goi}                        xpath=(//*[@resource-id="android:id/checkbox"])[2]
${dt_cd_Hien_thi_thong_tin_nha_mang_trong_cuoc_goi}        xpath=(//*[@resource-id="android:id/checkbox"])[3]
${dt_cd_nut_hen_gio_cuoc_goi}                              xpath=//*[@text="Hẹn giờ cuộc gọi"]
${dt_cd_nut_dat_thoi_gian}                                 xpath=//*[@text="Đặt thời gian"]
${dt_cd_checkbox_tat_cuoc_goi_tu_dong}                     xpath=(//*[@resource-id="android:id/checkbox"])[1]
${dt_cd_checkbox_rung_thong_bao_truoc_30s}                 xpath=(//*[@resource-id="android:id/checkbox"])[2]
${dt_cd_checkbox_ap_dung_cho_ca_cuoc_goi_den}              xpath=(//*[@resource-id="android:id/checkbox"])[3]
${dt_cd_Title_popup_dat_thoi_gian}                         id=android:id/alertTitle
${dt_cd_dat_thoi_gian_input_gio}                           xpath=//android.widget.NumberPicker[1]/android.widget.EditText
${dt_cd_dat_thoi_gian_input_phut}                          xpath=//android.widget.NumberPicker[2]/android.widget.EditText
${dt_cd_nut_dat_thoi_gian_trong_popup_dat_thoi_gian}       xpath=//*[@text="ĐẶT THỜI GIAN"]
${dt_cd_nut_thoi_gian_tu_Dong_xoa_ghi_am_cuoc_goi}         xpath=//*[@text="Thời gian tự động xóa ghi âm cuộc gọi"]
${dt_cd_nut_tat_thoi_gian_tu_dong_xoa_ghi_am_cuoc_goi}     xpath=//*[@text="Tắt"]
${dt_cd_nut_thoi_gian_tu_dong_xoa_ghi_am_cuoc_goi_30ngay}        xpath=//*[@text="30 ngày"]

# May lap 2 sim. Cac phan tu thuc hien cuoc goi bang
${dt_cd_nut_thuc_hien_cuoc_goi_bang}                             xpath=//*[@text="Thực hiện cuộc gọi bằng"]
${dt_cd_checkbox_thuc_hien_cuoc_goi_bang_sim1}                  xpath=(//*[@resource-id="android:id/text1"])[1]
${dt_cd_checkbox_thuc_hien_cuoc_goi_bang_sim2}                  xpath=(//*[@resource-id="android:id/text1"])[2]
${dt_cd_checkbox_thuc_hien_cuoc_goi_bang_hoi_truoc}             xpath=(//*[@resource-id="android:id/text1"])[3]

# May lap 2sim cac phan tu popup goi bang khi cham nut goi
${dt_popup_goi_bang_sim1}                                        xpath=(//*[@resource-id="bkav.android.btalk:id/icon_sim"])[1]
${dt_popup_goi_bang_sim2}                                        xpath=(//*[@resource-id="bkav.android.btalk:id/icon_sim"])[2]
${dt_popup_goi_bang_checkbox_luon_sd_sim_nay_de_goi}        id=bkav.android.btalk:id/chk_always_use

# Cac phan tu tab dien thoai
${dt_nut_quay_so}                   xpath=//*[@content-desc="quay số"]
${dt_man_hinh_hien_thi_nhap_sdt}                          id=bkav.android.btalk:id/digits
${dt_icon_tin_nhan_khi_nhap_so}                           xpath=//android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.ImageView
${dt_icon_xoa_khi_nhap_so}                                xpath=//android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.ImageView
${dt_tab_dt_them_lien_he}                                 id=bkav.android.btalk:id/add_new_contact
${dt_tab_them_lien_he_nut_tao_lien_he_moi}                xpath=//android.widget.TextView[@content-desc="TẠO LIÊN HỆ MỚI"]
${dt_tab_dt_mui_ten_mo_rong_thu_gon}                      xpath=//android.widget.ImageButton[@content-desc="Show more contact"]
${dt_man_hinh_hien_thi_so_da_luu_khi_goi}                 id=com.android.dialer:id/number_contact_bkav
${dt_tab_dien_thoai_loa}                                  xpath=//*[@text="Loa"]
${dt_tab_dien_thoai_nut_giu_cuoc_goi}                     xpath=//*[@text="Giữ cuộc gọi"]
${dt_danh_sach_sim}                                       id=bkav.android.btalk:id/list_sim
${dt_ket_thuc_cuoc_goi}                                   id=com.android.dialer:id/endButtonBkav_incall
${dt_man_hinh_hien_thi_ten_lien_he_da_luu_khi_goi}        id=com.android.dialer:id/name_contact_bkav
${dt_tab_dt_nut_tat_popup_ban_phim_khi_goi_thu_thoai}     id=com.android.dialer:id/dialpad_back
${dt_tab_dt_popup_phim_chua_duoc_thiet_lap}               xpath=//*[@text="Phím chưa được thiết lập"]
${dt_tab_dt_nut_de_sau}                                   xpath=//*[@text="ĐỂ SAU"]
${dt_tab_dt_quay_so_sim1}                                 xpath=(//*[@resource-id="bkav.android.btalk:id/label_sim_first"])
${dt_tab_dt_quay_so_sim2}                                 xpath=(//*[@resource-id="bkav.android.btalk:id/label_sim_second"])
${dt_tab_dt_db_quay_so_nhanh_lienhe1}                     xpath=(//*[@resource-id="bkav.android.btalk:id/contact_tile_push_state"])[1]
${dt_tab_dt_nut_them_lien_he_yeu_thich}                   xpath=//*[@text="THÊM LIÊN HỆ YÊU THÍCH"]
${dt_tab_dt_chua_co_ai_trong_db_quay_so_nhanh}            xpath=//*[@text="Chưa có ai trong danh bạ quay số nhanh của bạn"]
${dt_tab_dt_chon_nguoi_lien_he}                           xpath=//*[@text="Chọn người liên hệ"]
${dt_db_quay_so_nhanh_ten_lien_he2}                       xpath=(//*[@resource-id="bkav.android.btalk:id/contact_tile_name"])[2]
${dt_tab_dt_nut_3cham_db_quay_so_nhanh_lien_he1}          xpath=(//android.widget.ImageButton[@content-desc="Xem người liên hệ"])[1]
${dt_tab_dt_icon_bam_phim_so_quay_so_nhanh}               xpath=//android.widget.ImageView[@content-desc="bàn phím số"]
${dt_tab_dt_ten_lien_he1_trong_danh_sach_tim_kiem}        id=bkav.android.btalk:id/suggested_contact_name
${dt_tab_dt_lien_he2_danh_sach_tim_kiem}                  xpath=(//*[@resource-id="bkav.android.btalk:id/cliv_data_view"])[1]
${dt_tab_dt_danh_sach_tim_kiem_nut_goi_bang_sim_khac}     xpath=//*[@text="Gọi bằng SIM khác"]
${dt_tab_dt_nut_chia_se}                                  xpath=//*[@text="Chia sẻ"]
#che do trai nghiem
${dt_input_ma_pin}    com.android.systemui:id/password_entry
${dt_text_hien_thi_ma_pin}    xpath=//*[contains(@text,'Nhập mã pin')]
${dt_text_ma_pin}    Nhập mã pin
${dt_nut_chon_sim_goi}    //*[@resource-id='bkav.android.btalk:id/name_profile']
${dt_txt_loi_ko_lap_sim}     //*[contains(@text,'Không thể')]
${dt_nut_da_hieu_huong_dan}    com.android.systemui:id/understood

# Cac phan tu Cai dat mo ung dung
${dt_cd_checkbox_luu_vi_tri_tab_khi_thoat_ung_dung}    xpath=(//*[@resource-id="android:id/checkbox"])[1]

# Cac phan tu cai dat quay so nhanh
${dt_cd_nut_quay_so_nhanh_thu_thoai}                       xpath=//*[@text="Thư thoại"]
${dt_cd_nut_dich_vu}                                       xpath=//*[@text="Dịch vụ"]
${dt_cd_nut_thiet_lap}                                     xpath=//*[@text="Thiết lập"]
${dt_cd_nut_thong bao}                                     xpath=//*[@text="Thông báo"]
${dt_cd_checkbox_dich_vu_nha_mang_cua_ban}                 id=android:id/text1
${dt_cd_nut_so_thu_thoai}                                  xpath=//*[@text="Số thư thoại"]
${dt_cd_nhap_so_thu_thoai}                                 id=android:id/edit
${dt_cd_title_popup_thu_thoai}                             id=android:id/alertTitle
${dt_cd_nut_ok}                                            xpath=//*[@text="OK"]
${dt_cd_nut_huy_bo}                                        xpath=//*[@text="HỦY BỎ"]
${dt_cd_cai_dat_quay_so_nhanh_nhap_sdt}                    id=bkav.android.btalk:id/edit_container
${dt_cd_quay_so_nhanh_text_phim_2}                         xpath=//android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.TextView
${dt_cd_quay_so_nhanh_text_phim_3}                         xpath=//android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView
${dt_cd_quay_so_nhanh_icon_danh_ba}                        id=bkav.android.btalk:id/select_contact
${dt_cd_quay_so_nhanh_chon_lien_he1}     xpath=//android.widget.ListView/android.view.ViewGroup[1]/android.widget.TextView[4]
${dt_cd_sdt}             id=bkav.android.btalk:id/number
${dt_cd_thay_doi}        xpath=//*[@text="Thay đổi"]
${dt_cd_xoa_bo}          xpath=//*[@text="Xóa bỏ"]
${dt_popup_phim_chua_dc_thiet_lap}     xpath=//*[@text="Phím chưa được thiết lập"]

&{tn}                    vi_tri_vn=//*[@text="Tin nhắn"]
...                      vi_tri_en=//*[@text="Messages"]
...                      vn=Tin nhắn
...                      en=Messages

&{tuy_chon_khac}         //*[@content-desc="Tùy chọn khác"]
...                      //*[@content-desc="More options"]
...                      vn=Tùy chọn khác
...                      en=More options
# Cac phan tu tuy chon khac
&{tck_goi_dien}          vi_tri_vn=//*[@text="Gọi điện"]
...                      vi_tri_en=//*[@text="Make a call"]
...                      vn=Gọi điện
...                      en=Make a call

&{tck_them_lh}           vi_tri_vn=//*[@text="Thêm liên hệ"]
...                      vi_tri_en=//*[@text="Add contact"]
...                      vn=Gọi điện
...                      en=Add contact

&{tck_tim_kiem_tn}       vi_tri_vn=//*[@text="Tìm kiếm tin nhắn"]
...                      vi_tri_en=//*[@text="Search sms"]
...                      vn=Tìm kiếm tin nhắn
...                      en=Search sms

&{tck_dimh_kem_lh}       vi_tri_vn=//*[@text="Đính kèm liên hệ"]
...                      vi_tri_en=//*[@text="Attach contact"]
...                      vn=Đính kèm liên hệ
...                      en=Attach contact

&{tck_them_chu_de}       vi_tri_vn=//*[@text="Thêm chủ đề"]
...                      vi_tri_en=//*[@text="Add subject"]
...                      vn=Thêm chủ đề
...                      en=Add subject

&{tck_luu_tru}           vi_tri_vn=//*[@text="Lưu trữ"]
...                      vi_tri_en=//*[@text="Archive"]
...                      vn=Lưu trữ
...                      en=Archive

&{tck_cai_dat}           vi_tri_vn=//*[@text="Cài đặt"]
...                      vi_tri_en=//*[@text="Settings"]
...                      vn=Cài đặt
...                      en=Settings

&{tck_chinh_sua_phn}     vi_tri_vn=//*[@text="Chỉnh sửa phản hồi nhanh"]
...                      vi_tri_en=//*[@text=Edit quick responses"]
...                      vn=Chỉnh sửa phản hồi nhanh
...                      en=Edit quick responses

&{tck_xoa}               vi_tri_vn=//*[@text="Xóa"]
...                      vi_tri_en=//*[@text="Delete"]
...                      vn=Xóa
...                      en=Delete

&{tck_loc_tap_ga}        vi_tri_vn=//*[@text="Lọc tệp ghi âm"]
...                      vi_tri_en=//*[@text="Filter recording file"]
...                      vn=Lọc tệp ghi âm
...                      en=Filter recording file

&{tck_loc_tn_sms}        vi_tri_vn=//*[@text="Lọc tin nhắn SMS"]
...                      vi_tri_en=//*[@text="Filter SMS"]
...                      vn=Lọc tin nhắn SMS
...                      en=Filter SMS

&{khong_tim_thay}        vi_tri_vn=//*[@text="Không tìm thấy tin nhắn!"]
...                      vi_tri_en=//*[@text="Not found messages!"]
...                      vn=Lọc tin nhắn SMS
...                      en=Not found messages!
&{back_exit_lh}          vi_tri_vn=//*[@content-desc="Điều hướng lên trên"]
...                      vi_tri_en=//*[@content-desc="Navigate up"]

&{da_luu_tru}            vi_tri_vn=//*[@text="Đã lưu trữ"]
...                      vi_tri_en=//*[@text="Archived"]

&{all_da_doc}            vi_tri_vn=//*[@text="Đánh dấu tất cả đã đọc"]
...                      vi_tri_en=//*[@text="Mark all as read"]

&{cai_dat}               vi_tri_vn=//*[@text="Cài đặt"]
...                      vi_tri_en=//*[@text="Settings"]

&{huy_luu_tru}           vi_tri_vn=//*[@text="Hủy lưu trữ"]
...                      vi_tri_en=//*[@text="Unarchive"]

${dau_ba_cham}           bkav.android.btalk:id/imb_over_flow


# Cac phan tu giao dien tin nhan
${logo_tn}               bkav.android.btalk:id/img_background_expand_layout
${lien_he_tn}            bkav.android.btalk:id/conversation_title
${noi_dung_ctc}          bkav.android.btalk:id/message_text_and_info
${thu}                   bkav.android.btalk:id/day_label
${thu_gio}               bkav.android.btalk:id/message_status
${them_tep}              bkav.android.btalk:id/attach_media_button
${go_noi_dung}           bkav.android.btalk:id/compose_message_text
${icon}                  bkav.android.btalk:id/img_add_emoticon
${gui_tin_nhan}          bkav.android.btalk:id/send_message_button
${incall}                com.android.dialer:id/endButtonBkav_incall
${avata_lh}              xpath=(//*[@resource-id="bkav.android.btalk:id/conversation_icon"])[1]
${name_lh}               xpath=(//*[@resource-id="bkav.android.btalk:id/conversation_name"])[1]
${name_lh_1}             xpath=(//*[@resource-id="bkav.android.btalk:id/conversation_name"])[2]
# Cac phan tu them lien he
${them_lh}               android:id/button1
${xoa_truy_van}          bkav.android.btalk:id/search_close_btn
${xoa_truy_van_lh}       android:id/search_close_btn
${tim_kiem_tn}           bkav.android.btalk:id/search_src_text
${tim_kiem_lh}           android:id/action_bar
${tim_kiem_lup}          bkav.android.btalk:id/menu_search
${khong_thay_Lh}         bkav.android.btalk:id/empty_list_title
${tim_lien_he}           android:id/search_src_text
# Cac phan tu them chu de
${xoa_chu_de}            bkav.android.btalk:id/delete_subject_button
${txt_chu_de}            bkav.android.btalk:id/compose_subject_text
${MMS}                   bkav.android.btalk:id/mms_indicator
${text1}                 Mylove
${text2}                 5555555555222222222211111111113333333333888
${text3}                 5555555555222222222211111111113333333333
${tn_lu_tru}             bkav.android.btalk:id/swipeableContent
# elemet cai dat
${nut_bat_tat}           bkav.android.btalk:id/switch_button

&{cd_chung}              vi_tri_vn=//*[@text="Chung"]
...                      vi_tri_en=//*[@text="General"]

&{cd_chan}               vi_tri_vn=//*[@text="Chặn"]
...                      vi_tri_en=//*[@text="Block "]

&{cd_am_thanh}           vi_tri_vn=//*[@text="Sound"]
...                      vi_tri_en=//*[@text="Settings"]

&{cd_moi_nguoi}          vi_tri_vn=//*[@text="Mọi người trong cuộc hội thoại này"]
...                      vi_tri_en=//*[@text="People in this conversation"]

${avata_lien_he}         bkav.android.btalk:id/contact_icon
${ten_lien_he}           bkav.android.btalk:id/title_gradient

# Cac phan tu phan hoi nhanh
${add_phan_hoi}          bkav.android.btalk:id/action_add
${Noi_dung_phn}          bkav.android.btalk:id/response
&{Nut_back_phn}          vi_tri_vn=//*[@content-desc="Điều hướng lên trên"]
...                      vi_tri_en=//*[@content-desc="Navigate up"]
&{da_xoa_tb}             vi_tri_vn=Đã xóa cuộc hội thoại
...                      vi_tri_en=Conversation deleted
# Cac phan tu tep ghi am
${phat_tep_ga}           bkav.android.btalk:id/play_button
${noi_dung_tn}           bkav.android.btalk:id/message_text

# Cac phan tu thanh cong cu
${chia_se}               bkav.android.btalk:id/share_message_menu
${chuyen_tiep}           bkav.android.btalk:id/forward_message_menu
${sao_chep}              bkav.android.btalk:id/copy_text
${chi_tiet}              bkav.android.btalk:id/details_menu
${text_ko_thay_tn}       bkav.android.btalk:id/empty_text_hint
${them_moi}              bkav.android.btalk:id/start_new_conversation_button
${popup_chi_tiet}        android:id/alertTitle
&{xoa_thanh_cc}          vi_tri_vn=//*[@text="Xóa"]
...                      vi_tri_en=//*[@text="delete"]

${tim_kiem_ct}           bkav.android.btalk:id/img_ic_search_expand
${text_tin_nhan}         bkav.android.btalk:id/title_tab_message

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BTALK - CHUC NANG NHAT KI CUOC GOI
#=================================================================================================
${text_title_gan_day}                id=bkav.android.btalk:id/title_tab_calllog
${dt_tab_gan_day_lien_he1}           xpath=(//*[@resource-id="bkav.android.btalk:id/name"])[1]
${dt_gd_sdt1}                        xpath=(//*[@resource-id="bkav.android.btalk:id/number"])[1]
${dt_tab_gan_day_nut_goi_nho}        id=bkav.android.btalk:id/btnMissExpand
${dt_gd_icon_tin_nhan1}              xpath=(//*[@resource-id="bkav.android.btalk:id/primary_action_button"])[1]
${dt_gd_sao_chep_sdt}                xpath=//*[@text="Sao chép số"]
${dt_gd_nut_ba_cham_nang_cao}        id=bkav.android.btalk:id/ivMoreAction
${dt_gd_nut_xoa_nhat_ky}             xpath=//*[@text="Xóa nhật ký"]
${dt_gd_nut_chap_nhan}               xpath=//*[@text="CHẤP NHẬN"]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BTALK - CHUC NANG DANH BA
#=================================================================================================
${btalk_db_tim_lien_he}     id=bkav.android.btalk:id/search_view
${db_add_danh_ba}           bkav.android.btalk:id/search_add_contact
${db_nut_ngung_tim_kiem}    id=bkav.android.btalk:id/search_back_button
${dt_db_ten_lien_he_1}      xpath=(//*[@resource-id="bkav.android.btalk:id/cliv_name_textview"])[1]
${dt_db_nut_goi_lien_he_1}                    xpath=//android.widget.LinearLayout[1]/android.view.ViewGroup/android.widget.ImageButton[2]
${dt_db_nut_them_lien_he}                     id=bkav.android.btalk:id/action_add_contact
${dt_db_nut_lien_he_yeu_thich}                id=bkav.android.btalk:id/action_filter_favorites
${dt_db_tab_tuy_chon_cai_dat}                 id=bkav.android.btalk:id/action_more_tool
${dt_db_nut_them_lien_he_khi_tim_kiem}        id=bkav.android.btalk:id/search_add_contact
${dt_db_nut_tim_kiem}                         id=bkav.android.btalk:id/floating_action_button
${dt_db_nut_ngung_tim_kiem}                   id=bkav.android.btalk:id/search_back_button
${dt_db_nut_thoat_them_lien_he}               id=bkav.android.btalk:id/action_home_up
${dt_db_chi_tiet_lien_he_nut_sua}             xpath=//android.widget.TextView[@content-desc="Chỉnh sửa"]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BTALK - CHUC NANG GTV
#=================================================================================================
${tn_icon_soan_tin_nhan_moi}    xpath=//*[@resource-id="bkav.android.btalk:id/img_background_expand_layout"]

${gtv_icon_them_moi_ghi_chep}    bkav.android.bkavnote:id/fab
${gtv_icon_checklist_ghi_chep}    xpath=(//*[@resource-id="bkav.android.bkavnote:id/mini_fab"])[3]
${gtv_text_vew}    	            bkav.android.btalk:id/recipient_text_view
${gtv_message_text}                 bkav.android.btalk:id/compose_message_text

${gtv_chu_thuong_x}        646
${gtv_chu_thuong_y}        1761
${gtv_chu_thuong1_x}        424
${gtv_chu_thuong1_y}        1765
${gtv_chu_thuong2_x}        325
${gtv_chu_thuong2_y}        1613

${gtv_chu_hoa_x}        108
${gtv_chu_hoa_y}        1622

${gtv_ky_tu_so_x}        74
${gtv_ky_tu_so_y}        1899

${gtv_alt_x}        84
${gtv_alt_y}        1761

${gtv_icon_emoji_x}    99
${gtv_icon_emoji_y}    2066

${gtv_space_x}    533
${gtv_space_y}    1904

${gtv_nut_xoa_x}    996
${gtv_nut_xoa_y}    1756

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG CAI DAT
#=================================================================================================
${cd_tim_kiem}    xpath=//*[contains(@content-desc,'Tìm kiếm')]
${ht_avatar}           bkav.android.connect:id/avatar

# Cac phan tu trong man hinh chinh
${cd_tieu_de}                     xpath=//*[@text="Cài đặt"]
${cd_nut_mang_va_internet}        xpath=//*[contains(@text,'Mạng và Internet')]
${cd_nut_thiet_bi_da_ket_noi}     xpath=//*[contains(@text,'Thiết bị đã kết nối')]
${cd_nut_ung_dung_va_thong_bao}   xpath=//*[contains(@text,'Ứng dụng và thông báo')]
${cd_nut_pin}                     xpath=//*[contains(@text,'Pin')]
${cd_nut_hinh_nen}                xpath=//*[contains(@text,'Hình nền')][1]
${cd_nut_bang_dieu_khien}         xpath=//*[contains(@text,'Bảng điều khiển')]
${cd_nut_hien_thi}                xpath=//*[contains(@text,'Hiển thị')]
${cd_nut_dieu_huong}              xpath=//*[contains(@text,'Điều hướng')]
${cd_nut_am_thanh}                xpath=//*[contains(@text,'Âm thanh')]
${cd_nut_he_thong}                xpath=//*[contains(@text,'Hệ thống')]
${cd_tab_bang_cu_chi}             xpath=//*[contains(@text, 'Bằng cử chỉ')]
${cd_text_bang_cu_chi}            Bằng cử chỉ
${cd_tab_bang_thanh_dieu_huong}            xpath=//*[contains(@text, 'Bằng thanh điều hướng')]

${bam_cu_chi_mo_thanh_tang_giam_do_sang_x}    70
${bam_cu_chi_mo_thanh_tang_giam_do_sang_y}    1300

${bam_cu_chi_mo_thanh_tang_giam_am_thanh_x}    1000
${bam_cu_chi_mo_thanh_tang_giam_am_thanh_y}    1345

${cu_chi_bat_dau_giam_do_sang_toa_do_x}    170
${cu_chi_bat_dau_giam_do_sang_toa_do_y}    1095
${cu_chi_ket_thuc_giam_do_sang_toa_do_x}    170
${cu_chi_ket_thuc_giam_do_sang_toa_do_y}    1350

${cu_chi_bat_dau_tang_do_sang_toa_do_x}    170
${cu_chi_bat_dau_tang_do_sang_toa_do_y}    1350
${cu_chi_ket_thuc_tang_do_sang_toa_do_x}    170
${cu_chi_ket_thuc_tang_do_sang_toa_do_y}    1095

${cu_chi_bat_dau_giam_am_thanh_toa_do_x}    912
${cu_chi_bat_dau_giam_am_thanh_toa_do_y}    1360
${cu_chi_ket_thuc_giam_am_thanh_toa_do_x}    912
${cu_chi_ket_thuc_giam_am_thanh_toa_do_y}    1570

${cu_chi_bat_dau_tang_am_thanh_toa_do_x}    912
${cu_chi_bat_dau_tang_am_thanh_toa_do_y}    1570
${cu_chi_ket_thuc_tang_am_thanh_toa_do_x}    912
${cu_chi_ket_thuc_tang_am_thanh_toa_do_y}    1360

# Cac phan tu khac
${cd_nut_nang_cao}                xpath=//*[@text="Nâng cao"]
${cd_text_cai_dat}                xpath=//*[@text="Cài đặt"]
${cd_hien_thi_text_cai_dat}        Cài đặt

# Cac phan tu ung dung va thong bao
${cd_xem_tat_ca_ung_dung}         id=com.android.settings:id/header_details
${cd_nut_tuy_chon_khac}           xpath=//android.widget.ImageButton[@content-desc="Tùy chọn khác"]
${cd_hien_thi_he_thong}           xpath=//*[@text="Hiển thị hệ thống"]
${cd_nut_tim_kiem_thong_tin_ung_dung}        xpath=//android.widget.TextView[@content-desc="Tìm kiếm"]
${cd_thanh_tim_kiem}                 id=android:id/search_src_text
${cd_ung_dung_bo_nho_phuong_tien}       xpath=//*[@text="Bộ nhớ phương tiện"]
${cd_mo_theo_mac_dinh}                  xpath=//*[contains(@text,'Mở theo mặc định')]
${cd_nut_xoa_mac_dinh}                    xpath=//*[@text="XÓA MẶC ĐỊNH"]
${cd_ung_dung_quan_ly_file}            xpath=//*[@text="Quản lý file"]
${cd_thong_bao}                        //*[contains(@text,'Thông báo')]

# cac phan tu trong he thong
${cd_nut_ngon_ngu_va_nhap_lieu}         xpath=//*[@text="Ngôn ngữ và nhập liệu"]
${cd_nut_ngon_ngu}                      xpath=//*[@text="Ngôn ngữ"]
${cd_nut_tieng_viet}                    xpath=//android.widget.TextView[@content-desc="Tiếng Việt (Việt Nam)"]
${cd_nut_english}                       xpath=//android.widget.TextView[@content-desc="Tiếng Anh (Hoa Kỳ)"]
${cd_nhan_toa_do_x}    883
${cd_nhan_toa_do_y}    996
${cd_wifi_nhan_toa_do_x}    207
${cd_wifi_nhan_toa_do_y}    1489
${cd_wifi_name}                xpath=//*[contains(@text,'Bkav-QA-2.4GHz')]
${cd_wifi_name1}                xpath=(//*[@resource-id="android:id/icon"])[2]
${cd_wifi_show_password}        xpath=//*[@resource-id="com.android.settings:id/show_password"]
${cd_wifi_password}            xpath=//*[@resource-id="com.android.settings:id/password"]
${cd_nut_ket_noi_wifi}          xpath=//*[@text="KẾT NỐI"]

${cd_nhan_mang_va_internet}    xpath=//*[contains(@text,'Mạng và Internet')]
${cd_tab_wifi}                     xpath=(//*[@resource-id="android:id/icon"])[1]
${cd_text_da_ket_noi}     xpath=//*[@text="Đã kết nối"]
${cd_hien_thi_text_da_ket_noi}    Đã kết nối

# Cac phan tu trong hien thi
${cd_nut_hien_thi_tren_man_hinh_khoa}        //*[contains(@text, 'Hiển thị trên màn hình khóa')]
${cd_nut_tren_man_hinh_khoa}                    //*[contains(@text,'Trên màn hình khóa')]
${cd_nut_man_hinh_khoa}                        //*[contains(@text,'Màn hình khóa')]
${cd_cb_hien_thi_tatca_noidung_thongbao}                  //*[contains(@text, 'Hiển thị tất cả nội dung thông báo')]

# Cac phan tu trong bang dieu khien
${cd_db_nut_add_chuc_nang_ghi_am}                        xpath=(//*[@resource-id="com.android.settings:id/img_add"])[5]
${cd_db_b_3_nut_add_chuc_nang_ghi_am}                        xpath=(//*[@resource-id="com.android.settings:id/img_add"])[2]

# Cac phan tu cua ngay va gio trong he thong
${cd_he_thong_ngay_gio_dinh_dang_24h}    (//*[@resource-id="android:id/switch_widget"])[4]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG CHE DO DEM
#=================================================================================================
${cdd_text_hien_thi}    //*[contains(@text,'Hiển thị')]
${cdd_tab_che_do_dem}    xpath=(//*[@resource-id="android:id/title"])[2]
${cdd_bat_che_do_dem}    //*[contains(@text,'BẬT NGAY')]
${cdd_bat_vao_thoi_gian_tuy_chinh}    //*[contains(@text,'BẬT CHO ĐẾN')]
${cdd_bat_den_binh_minh}    //*[contains(@text,'BẬT CHO ĐẾN BÌNH MINH')]
${cdd_text_bat_che_do_dem}    BẬT NGAY
${cdd_text_thoi_gian_tuy_chinh}        BẬT CHO ĐẾN 6:00 SA
${cdd_text_den_binh_minh}    BẬT CHO ĐẾN BÌNH MINH
${cdd_che_do_khong_co}    //*[contains(@text,'Không có')]
${cdd_che_do_thoi_gian}    //*[contains(@text,'Bật vào thời gian')]
${cdd_che_do_hoang_hon}    //*[contains(@text,'Bật từ')]
${cdd_lich_bieu}    //*[contains(@text,'Lịch biểu')]
${cdd_tat_che_do_dem}    //*[contains(@text,'TẮT NGAY')]
${cdd_text_tat_che_do_dem}    TẮT NGAY
${cdd_nhan_toa_do_x}    424
${cdd_nhan_toa_do_y}    1000

${cdd_rieng_tu_nhan_toa_do_x}    187
${cdd_rieng_tu_nhan_toa_do_y}    1000

${cdd_nut_hoan_thanh}    //*[contains(@text,'HOÀN THÀNH')]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG KHONG LAM PHIEN
#=================================================================================================
${klp_cham_vao_toa_do_x}    192
${klp_cham_vao_toa_do_y}    1233
${klp_cham_vao_text_khong_lam_phien}    xpath=//*[contains(@text,'Không làm phiền')]
${klp_text_bat_ngay_bay_gio}           xpath=//*[@resource-id="com.android.settings:id/zen_mode_settings_turn_on_button"]
${klp_text_hien_thi_bat}                BẬT NGAY
${klp_text_tat_ngay_bay_gio}           xpath=//*[@resource-id="com.android.settings:id/zen_mode_settings_turn_off_button"]
${klp_text_hien_thi_tat}    TẮT NGAY BÂY GIỜ
${klp_bat_dau_toa_do_x}    725
${klp_bat_dau_toa_do_y}    1455
${klp_text_luon_hoi}                //*[contains(@text,'Luôn hỏi')]
${klp_text_thoi_luong_mac_dinh}                xpath=//*[contains(@text,'Thời lượng mặc định')]

${klp_vuot_vao_bat_dau_toa_do_x}    90
${klp_vuot_vao_bat_dau_toa_do_y}    60
${klp_vuot_vao_ket_thuc_toa_do_x}    15
${klp_vuot_vao_ket_thuc_toa_do_y}    50

#=================================================================================================
#    CAC PHAN TU LOCKSCREEN
#=================================================================================================
${lockscreen_nut_bao_mat}    //*[contains(@text,'Bảo mật')]
${lockscreen_text_hien_thi_bao_mat}    Bảo mật
${lockscreen_nut_khoa_man_hinh}    //*[contains(@text,'Khóa màn hình')]
${lockscreen_text_bao_mat_thiet_bi}    //*[contains(@text,'BẢO MẬT THIẾT BỊ')]
${lockscreen_text_hien_thi_bao_mat_thiet_bi}    BẢO MẬT THIẾT BỊ
${lockscreen_nut_khoa_man_hinh_vuot}    //*[contains(@text,'Vuốt')]
${lockscreen_nut_xoa_tinh_nang_bao_ve}    //*[contains(@text,'XÓA')]
${lockscreen_nut_ma_pin}    //*[contains(@text,'Mã PIN')]
${lockscreen_nut_co}    //*[contains(@text,'CÓ')]
${lockscreen_nut_tiep_theo}    //*[contains(@text,'Tiếp theo')]
${lockscreen_nut_xac_nhan}    //*[contains(@text,'Xác nhận')]
${lockscreen_nut_xong}    //*[contains(@text,'Xong')]
${lockscreen_cham_man_hinh_toa_do_x}    538
${lockscreen_cham_man_hinh_toa_do_y}    1844
${lockscreen_nhan_nut_toa_do_x}    533
${lockscreen_nhan_nut_toa_do_y}    1761

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG CAMERA
#=================================================================================================
# Vi tri nut mo camera tren man hinh khoa
${cam_mh_khoa_start_x}     980
${cam_mh_khoa_start_y}     2040
${cam_mh_khoa_offset_x}    540
${cam_mh_khoa_offset_y}    2040

# Cac phan tu pho thong trong ung dung may anh
${cam_xem_cuon_phim}    xpath=//*[@content-desc="Chế độ xem cuộn phim"]
${cam_nut_quay_chup}    xpath=//*[@content-desc="Màn trập"]
${cam_nut_truoc_sau}    id=com.bkav.camera:id/btn_switch_camera

# Cac phan tu trong menu duoi
${cam_toan_canh}        xpath=//*[@text="TOÀN CẢNH"]
${cam_ai_mode}          xpath=//*[@text="AI MODE"]
${cam_chup_anh}         xpath=//*[@text="CHỤP ẢNH"]
${cam_quay_phim}        xpath=//*[@text="QUAY PHIM"]
${cam_them_nua}         xpath=//*[@text="THÊM NỮA"]

# Cac phan tu trong AI MODE
${cam_scene_mode_text}    id=com.bkav.camera:id/scene_mode_text
${cam_chan_dung}          xpath=(//*[@resource-id="com.bkav.camera:id/border"])[1]
${cam_chan_dung_txt}      Chân dung
${cam_s_macro}            xpath=(//*[@resource-id="com.bkav.camera:id/border"])[2]
${cam_s_macro_txt}        sMacro
${cam_san_pham}           xpath=(//*[@resource-id="com.bkav.camera:id/border"])[3]
${cam_san_pham_txt}       Sản phẩm
${cam_khoanh_khac}        xpath=(//*[@resource-id="com.bkav.camera:id/border"])[4]
${cam_khoanh_khac_txt}    Khoảnh khắc
${cam_s_night}            xpath=(//*[@resource-id="com.bkav.camera:id/border"])[5]
${cam_s_night_txt}        sNight 2.0
${cam_goc_rong}           xpath=(//*[@resource-id="com.bkav.camera:id/border"])[6]
${cam_goc_rong_txt}       Góc rộng

# Cac phan tu trong che do chup sMacro
${cam_s_macro_dual_view}    //*[@resource-id="org.codeaurora.gallery:id/image_dual_view"]

# Cac phan tu trong CHUP ANH
${cam_nut_hdr}              id=com.bkav.camera:id/btn_hdr
${cam_nut_flash}            id=com.bkav.camera:id/btn_flash
${cam_nut_bo_loc}           id=com.bkav.camera:id/btn_filters
${cam_nut_lam_dep}          id=com.bkav.camera:id/btn_makeup
${cam_nut_cai_dat}          id=com.bkav.camera:id/btn_settings
${cam_nut_auto_adv}         id=com.bkav.camera:id/btn_switch_adv

# Cac phan tu trong QUAY PHIM
${cam_nut_video_fps}        id=com.bkav.camera:id/btn_video_fps
${cam_nut_ghi_am}           id=com.bkav.camera:id/btn_mute

# Cac phan tu trong THEM NUA
${cam_chuyen_nghiep}          xpath=(//*[@resource-id="com.bkav.camera:id/border"])[2]
${cam_chuyen_nghiep_txt}      Chuyên nghiệp

# Cac phan tu trong che do chup Chuyen nghiep
${cam_nut_lay_net}           //*[@text="Lấy nét"]
${cam_nut_can_bang_trang}    //*[@text="C.bằng trắng"]
${cam_nut_iso}               //*[@text="ISO"]
${cam_nut_thoi_gian}         //*[@text="Thời gian"]

# Cac phan tu trong Che do xem cuon phim
${cam_snackbar_container}    id=org.codeaurora.gallery:id/snackbar_container
${cam_nut_thong_tin}         id=org.codeaurora.gallery:id/info_button
${cam_nut_xoa}               id=org.codeaurora.gallery:id/delete_button

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG CHIM LAC
#=================================================================================================
${cl_url_bar}                    android.bkav.bchrome:id/url_bar
${cl_tab_moi}                    //*[contains(@text,'Tab mới')]
${cl_tab_andanh_moi}             //*[contains(@text,'Tab ẩn danh mới')]
${cl_o_vuong}                    android.bkav.bchrome:id/tab_switcher_button
${cl_closed_tab}                 android.bkav.bchrome:id/tab_closer_button
${cl_popup_dong_tab}             //*[contains(@text,'Đóng tab')]
${cl_trang_mac_dinh}             //*[@resource-id="android.bkav.bchrome:id/tile_view_default_layout"]

# Cac phan tu trong popup Bat che do thu gon
${nut_bat_che_do_thu_gon}    //*[contains(@text,'Bật Chế độ thu gọn')]

# Cac phan tu trong popup trang web muon su dung thong tin vi tri thiet bi
${nut_chan}    //*[contains(@text,'Chặn')]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG DONG HO
#=================================================================================================
# Cac phan tu tab menu
${dh_tab_bao_thuc}         xpath=//*[@content-desc="Alarm"]
${dh_tab_dong_ho}          xpath=//*[@content-desc="Clock"]
${dh_tab_bo_hen_gio}       xpath=//*[@content-desc="Timer"]
${dh_tab_bam_gio}          xpath=//*[@content-desc="Stopwatch"]
${dh_tab_tuy_chon_khac}    xpath=//*[@content-desc="More options"]

# man hinh bao thuc
${dh_bt_khong_co_bao_thuc}    bkav.android.deskclock:id/alarms_empty_view
${dh_bt_check_text_ngay_thu}    //android.widget.TextView[@content-desc="Thứ Hai, Thứ Ba, Thứ Tư, Thứ Năm, Thứ Sáu"]
${dh_bt_txt_hien_thi_thu_hai}    //android.widget.CheckBox[@content-desc="Monday"]
${dh_bt_txt_hien_thi_thu_ba}    //android.widget.CheckBox[@content-desc="Tuesday"]
${dh_bt_txt_hien_thi_thu_tu}    //android.widget.CheckBox[@content-desc="Wednesday"]
${dh_bt_txt_hien_thi_thu_nam}    //android.widget.CheckBox[@content-desc="Thursday"]
${dh_bt_txt_hien_thi_thu_sau}    //android.widget.CheckBox[@content-desc="Friday"]
${dh_bt_txt_hien_thi_thu_bay}    //android.widget.CheckBox[@content-desc="Saturday"]
${dh_bt_txt_hien_thi_chu_nhat}    //android.widget.CheckBox[@content-desc="Sunday"]
${dh_bt_cd_rung}    bkav.android.deskclock:id/vibrate_onoff
${dh_bt_label_nhan_dan}    bkav.android.deskclock:id/edit_label
${dh_bt_tab_thu_gon_bao_thuc}    //*[@content-desc="Collapse alarm"]
${dh_bt_btn_nut_them_bao_thuc}    bkav.android.deskclock:id/fab

# Cac phan tu o nut them bao thuc
${dh_bt_nut_dong_y}    android:id/button1
${dh_bt_nut_huy}    android:id/button2
${dh_bt_nut_huy_nhan}    android:id/button2
${dh_bt_nut_lap_lai_bao_thuc}    bkav.android.deskclock:id/repeat_onoff

#các element trong ban phim them bao thuc
${dh_bt_nut_ban_phim}    android:id/toggle_mode
${dh_bt_nut_in_ban_phim}    android:id/input_header

# Cac phan tu chuong bao thuc
${dh_bt_chuong_mac_dinh}    bkav.android.deskclock:id/choose_ringtone
${dh_bt_kieu_chuong_argon}     //*[contains(@text,'Argon')]
${dh_bt_kieu_chuong_tititit}     //*[contains(@text,'Tit tit tit')]
${dh_bt_kieu_chuong_neptunium}     //*[contains(@text,'Neptunium')]
${dh_bt_mo_danh_sach_nhac_chuong}      //*[contains(@text,'Ringtone')]
${dh_bt_chon_file_nhac_chuong}      //*[contains(@text,'Sounds in device')]
${dh_bt_them_am_thanh_moi}      //*[contains(@text,'Thêm âm thanh mới')]
${dh_bt_back_am_thanh_bao_thuc}      //*[@content-desc="Di chuyển lên"]
${dh_bt_nut_xoa_bao_thuc}    id=bkav.android.deskclock:id/delete
${dh_bt_nut_1}     //*[@content-desc="1"]
${dh_bt_nut_2}     //*[@content-desc="2"]
${dh_bt_nut_3}     //*[@content-desc="3"]
${dh_bt_nut_4}     //*[@content-desc="4"]
${dh_bt_nut_5}     //*[@content-desc="5"]
${dh_bt_nut_6}     //*[@content-desc="6"]
${dh_bt_nut_7}     //*[@content-desc="7"]
${dh_bt_nut_8}     //*[@content-desc="8"]
${dh_bt_nut_9}     //*[@content-desc="9"]
${dh_bt_nut_10}    //*[@content-desc="10"]
${dh_bt_nut_11}    //*[@content-desc="11"]
${dh_bt_nut_12}    //*[@content-desc="12"]
${dh_bt_hien_thi_thoi_gian_bao_thuc}    //*[@text='6']
${dh_bt_nut_mo_popup_sua_bao_thuc}    (//android.widget.ImageButton[@content-desc="Expand alarm"])[3]

# Cac phan tu nhan
${dh_bt_label_nhan_text}    //*[contains(@class,'android.widget.EditText')]

# Cac phan tu dong ho
${dh_hien_thi_text_thoi_gian}    id=bkav.android.deskclock:id/date
${dh_btn_thanh_pho}             xpath=//*[@content-desc="Cities"]
${dh_btn_nut_back}                  xpath=//*[@content-desc="Navigate up"]
${dh_btn_icon_search}     	id=bkav.android.deskclock:id/search_button
${dh_text_tim_kiem}                 xpath=//*[contains(@text,'Search…')]
${dh_icon_xoa_text_tim_kiem}    id=bkav.android.deskclock:id/search_close_btn
${dh_hien_thi_text_chu_cai}    id=bkav.android.deskclock:id/index
${dh_check_box_chon_quoc_gia}   xpath=//android.widget.CheckBox[@content-desc="Abidjan"]
${dh_check_box_chon_quoc_gia_theo_danh_sach}    xpath=//*[contains(@resource-id,'bkav.android.deskclock:id/city_onoff')]
${dh_dh_input_tim_thanh_pho}    //*[contains(@resource-id,'bkav.android.deskclock:id/city_name')]
${dh_dh_input_ten_hoa}    HANOI
${dh_dh_input_ten_co_dau}        HaNoi
${dh_dh_input_ten_khong_dau}    hanoi
${dh_dh_input_ten_chuoi_ky_tu}        @#$%%^&^%
${dh_dh_input_ten_mot_tu}    H
${dh_hien_thi_time}        xpath=//*[contains(@resource-id,'bkav.android.deskclock:id/city_time')]
${dh_btn_sap_xep_dinh_dang_thoi_gian}    xpath=//*[contains(@text,'Sort by time')]
${dh_copy_text}    id=com.android.permissioncontroller:id/permission_message

# Check hien thi cai dat
${dh_check_hien_thi_mui_gio}    xpath=//*[contains(@text,'(GMT+7:00) Hồ Chí Minh')]
${dh_hien_thi_text_dong_ho}        //*[contains(@text,'Clock')]
${dh_hien_thi_kieu_dong_ho}        //*[contains(@text,'Style')]
${dh_btn_check_kieu_dong_ho}        //*[contains(@text,'Digital')]
${dh_hien_thi_kieu_dong_ho_kim}        //*[contains(@text,'Analog')]
${dh_btn_dong_ho_so}    //*[contains(@resource-id,'bkav.android.deskclock:id/digital_clock')]
${dh_btn_dong_ho_kim}    (//*[contains(@class,'android.widget.ImageView')])[6]
${dh_text_hien_thi_thoi_gian_voi_so_giay}    //*[contains(@text,'Hiển thị thời gian với số giây')]
${dh_time_seconds}    bkav.android.deskclock:id/digital_clock
${dh_on_off_hien_thi_thoi_gian}    //*[contains(@class,'android.widget.Switch')]
${dh_on_off_dong_ho_tu_dong}    //*[contains(@class,'android.widget.Switch')]

# Cac phan tu mui gio
${dh_check_mui_gio_chinh}     //*[@text='Home time zone']
${dh_check_mui_gio_dong_au}       xpath=(//*[@resource-id='android:id/summary'])[5]
${dh_check_mui_gio_du_bai}    //*[@text='(GMT+4:00) Dubai']
${mui_gio_thanh_pho}    (GMT+7:00) Hồ Chí Minh')
${dh_check_mui_gio_ho_chi_minh}    //*[contains(@text,'(GMT+7:00) Hồ Chí Minh')]
${dh_check_mui_gio_hien_thi_jakarta}    //*[contains(@text,'(GMT+7:00) Jakarta')]
${dh_cd_chon_mui_gio_jakarta}    //*[contains(@text,'Jakarta')]
${dh_btn_huy}    android:id/button2
${dh_btn_dong_y}    android:id/button1
${dh_cd_thay_doi_ngay_gio}    //*[contains(@text,'Change Date & time')]
${dh_cd_su_dung_do_mang_cung_cap}    //*[contains(@text,'Automatic time & time')]
${dh_cd_ngay_va_gio}    //*[contains(@text,'Ngày và giờ')]
${dh_cd_su_dung_dinh_dang_cua_dia_phuong}    //*[contains(@text,'Sử dụng định dạng của địa phương')]
${dh_cd_su_dung_dinh_dang_24_gio}    //*[contains(@text,'Sử dụng định dạng 24 giờ')]
${dh_cd_thoi_gian}    //*[contains(@text,'Set time')]
${dh_cd_ngay}    //*[contains(@text,'Set date')]

# Cac phan tu tab dong ho
${dh_ngay_trong_dong_ho}    bkav.android.deskclock:id/date
${dh_dong_ho_so_trong_dong_ho}    bkav.android.deskclock:id/digital_clock
${dh_cd_nha_rieng}    bkav.android.deskclock:id/city_name

# Cac elemet trong ngay o ngay va gio
${dh_cd_ngay_thang}    android:id/date_picker_header_date
${dh_cd_ngay_mot}     //*[@text="1"]

# Cac phan tu trong mui gio trong ngày va gio
${dh_cd_mui_gio}    //*[contains(@text,'Select time zone')]
${dh_cd_mui_gio_b60}    //*[@text='Chọn múi giờ']
${dh_cd_khu_vuc}    //*[@text='Khu vực']
${dh_cd_khu_vuc_cairo}    //*[@text='Cairo (GMT+02:00)']
${dh_cd_khu_vuc_ai_cap}    //*[@text='Ai Cập']

# Cac phan tu thoi gian trong thay doi ngay gio
${dh_cd_gio}    android:id/hours
${dh_cd_check_1h}    //*[@content-desc="1"]
${dh_cd_check_5min}    //*[@content-desc="5"]
${dh_bt_nut_tat_da_nhiem_x}    540
${dh_bt_nut_tat_da_nhiem_y}    1735
${dh_bt_nut_tat_bao_thuc_nhanh_x}    150
${dh_bt_nut_tat_bao_thuc_nhanh_y}    175
${start_do_x}    0
${start_do_y}    60
${end_do_x}    30
${end_do_y}    60
${dh_bt_bao_thuc_nhanh}    bkav.android.deskclock:id/left_button
${dh_bt_btn_xong}     bkav.android.deskclock:id/done_button
${dh_bt_thoi_gian_bao_thuc}    //*[@content-desc="8:30 SA"]
${dh_cd_mo_rong_bao_thuc}    //*[@content-desc="Expand alarm"]
${dh_cd_loai_bo}    bkav.android.deskclock:id/preemptive_dismiss_button
${dh_bt_mo_rong_bao_thuc}    //android.widget.ImageButton[@content-desc="Expand alarm"]

# Cac phan tu trong cai dat bao thuc
${dh_cd_im_lang_sau}    //*[contains(@text,'Silence after')]
${dh_cd_im_lang_sau_10_phut}    //*[contains(@text,'10 minutes')]
${dh_cd_get_so_phut}    android:id/numberpicker_input
${dh_cd_im_lang_sau_5_phut}    //*[contains(@text,'5 minutes')]
${dh_cd_im_lang_sau_9_phut}    //*[contains(@text,'9 minutes')]
${dh_cd_im_lang_sau_13_phut}    //*[contains(@text,'13 minutes')]
${dh_cd_thoi_luong_bao_lai}    //*[contains(@text,'Snooze length')]
${dh_cd_tang_dan_am_luong}    //*[contains(@text,'Gradually increase volume')]
${dh_cd_thoi_luong_5_giay}    //*[contains(@text,'5 seconds')]
${dh_cd_thoi_luong_60_giay}    //*[contains(@text,'60 seconds')]
${dh_cd_icon_bat_mui_gio_tu_dong}    xpath=(//*[@resource-id="android:id/switch_widget"])[2]
${dh_cd_icon_bat_ngay_tu_dong}    xpath=(//*[@resource-id="android:id/switch_widget"])[1]

# Cac phan tu bo hen gio
${dh_cd_txt_bo_hen_gio_het_han}    xpath=//*[@text="Timer ringtone"]
${dh_cd_txt_Argon_am_nhac}       xpath=//*[@text="Argon"]
${dh_cd_icon_hien_thi_duoc_chon}    id=bkav.android.deskclock:id/sound_image_selected
${dh_cd_txt_am_thanh_hen_gio}    xpath=//*[@text="Timer Expired"]
${dh_cd_txt_tang_am_luong}       xpath=//*[@text="Gradually increase volume"]
${dh_cd_txt_bo_hen_gio_rung}        xpath=//*[contains(@text,"Bộ hẹn giờ rung")]
${dh_cd_txt_them_am_thanh_moi}    xpath=//*[contains(@text,'Thêm âm thanh mới')]
${dh_cd_btn_nut_switch}    xpath=//*[contains(@text,'Off')]
${dh_cd_btn_nut_switch_on}    xpath=//*[contains(@text,'On')]

# Cac phan tu bao thuc
${dh_cd_txt_tuan_bat_dau_vao}    xpath=//*[contains(@text,'Start week on')]
${dh_cd_txt_thu_hai}        xpath=//*[@text="Monday"]
${dh_cd_txt_thu_bay}        xpath=//*[@text="Saturday"]
${dh_cd_txt_chu_nhat}       xpath=//*[@text="Sunday"]
${dh_cd_text_thu_hai}        Monday
${dh_cd_text_thu_bay}        Saturday
${dh_cd_text_chu_nhat}       Sunday
${dh_cd_txt_hanh_dong_lac_may}     xpath=//*[contains(@text,'Hành động lắc máy')]
${dh_cd_txt_lac_may_khong_lam_gi}     xpath=//*[contains(@text,'Lắc máy sẽ không làm gì')]
${dh_cd_txt_khong_lam_gi}     xpath=//*[contains(@text,'Không làm gì')]
${dh_cd_txt_bao_lai}     xpath=//*[contains(@text,'Báo lại')]
${dh_cd_txt_hien_thi_bao_lai}     xpath=//*[contains(@text,'Lắc máy sẽ nhắc lại báo thức')]
${dh_cd_txt_loai_bo}     xpath=//*[contains(@text,'Loại bỏ')]
${dh_cd_tuy_chon_khac}    xpath=//*[@content-desc="‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‏‎‏‏‏‏‏‏‏‏‎‏‏‎‏‏‏‏‎‏‎‎‎‏‏‎‏‏‏‎‏‏‎‏‎‏‎‎‎‎‎‎‏‎‎‎‏‏‏‎‏‏‏‏‎‎‎‏‎‎‎‏‎‏‎More options‎‏‎‎‏‎"]
${dh_cd_cho_phep_dong_ho_set}    com.android.permissioncontroller:id/permission_allow_button
${dh_cd_time_bao_thuc_sau}    android:id/numberpicker_input
${dh_cd_bao_thuc_ngan}    bkav.android.deskclock:id/edit_label

# ...                                     # XPATH CUA APP DONG HO MAN HINH HEN GIO
${dh_hg_hien_thi_dong_ho_da_hen_gio}    id=bkav.android.deskclock:id/timer_time
${dh_hg_nut_notyfi}    id=bkav.android.deskclock:id/chronometer
${dh_hg_btn_huy_them_bo_hen_gio}        xpath=//*[@content-desc="Cancel"]

# Cac phan tu hen thoi gian
${dh_hg_time_setup}    id=bkav.android.deskclock:id/timer_setup_digit
${dh_hg_hien_thi_thoi_gian_thiet_lap}    id=bkav.android.deskclock:id/timer_setup_time
${dh_hg_nut_xoa_thoi_gian_thiet_lap}    id=bkav.android.deskclock:id/timer_setup_delete
${dh_hg_check_thanh_ngang}    id=bkav.android.deskclock:id/timer_setup_divider
${dh_hg_nut_1}        id=bkav.android.deskclock:id/timer_setup_digit_1
${dh_hg_nut_2}        id=bkav.android.deskclock:id/timer_setup_digit_2
${dh_hg_nut_3}        id=bkav.android.deskclock:id/timer_setup_digit_3
${dh_hg_nut_4}        id=bkav.android.deskclock:id/timer_setup_digit_4
${dh_hg_nut_5}        id=bkav.android.deskclock:id/timer_setup_digit_5
${dh_hg_nut_6}        id=bkav.android.deskclock:id/timer_setup_digit_6
${dh_hg_nut_7}        id=bkav.android.deskclock:id/timer_setup_digit_7
${dh_hg_nut_8}        id=bkav.android.deskclock:id/timer_setup_digit_8
${dh_hg_nut_9}        id=bkav.android.deskclock:id/timer_setup_digit_9
${dh_hg_nut_0}        id=bkav.android.deskclock:id/timer_setup_digit_0
# Cac phan tu chuc nang bat dau chay thoi gian
${dh_hg_bat_dau}                        xpath=//*[@content-desc="Start"]
${dh_hg_stop}                           xpath=//*[@content-desc="Stop"]
${dh_hg_nut_dat_lai_gio_hen}        id=bkav.android.deskclock:id/reset_add
${dh_hg_them_phut}                      xpath=//*[@content-desc="Add 1 Minute"]
${dh_hg_nhan_comment}        id=bkav.android.deskclock:id/timer_label
${dh_hg_nhan_o_text_comment}        id=bkav.android.deskclock:id/custom
${dh_hg_input_text}            xpath=//android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.EditText
${dh_hg_btn_huy_nhan_comment}        id=android:id/button2
${dh_hg_btn_dong_y_nhan_comment}        id=android:id/button1
${dh_hg_check_notify_text}               id=bkav.android.deskclock:id/content
${dh_hg_dong_ho_hien_thi}        id=bkav.android.deskclock:id/timer_time_text
${dh_hg_xoa_thoi_gian_thiet_lap_hen_gio}        xpath=//*[@content-desc="Delete"]
${dh_hg_them_gio_hen}        id=bkav.android.deskclock:id/right_button
${dh_hg_huy_them_gio_hen}        id=bkav.android.deskclock:id/left_button

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG EDICT
#=================================================================================================
${edict_tim_kiem}                  bkav.android.edict:id/txtInput
${edict_menu}                      bkav.android.edict:id/arrow_left
${edict_lichsu}                    bkav.android.edict:id/smHistory
${edict_tim_theo_lich_su}          (//*[@resource-id="bkav.android.edict:id/text"])[2]
${edict_toa_do_x}                  475
${edict_toa_do_y}                  271

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG ESIM
#=================================================================================================
${esim_txt_quan_ly}    xpath=//*[contains(@text,'Quản lý eSIM')]

# Cac phan tu eSim chua lap
${eSim_khong_sim_hotline}                xpath=//*[@text="Hotline:"]
${eSim_khong_sim_so_hotline}             xpath=//*[@text="1800 54 54 48"]
${eSim_khong_sim_link_Bphone.vn}         id=bkav.android.esim:id/link_web
${eSim_khong_sim_bphone_b86}             xpath=//*[@text="1. eSIM Bphone B86 có gì đặc biệt?"]
${eSim_khong_sim_text_dang_trong}        xpath=//*[contains(@text,'Thẻ eSIM đang trống')]
${eSim_khong_sim_view_hdsd}              id=com.android.chrome:id/coordinator
${sSim_khong_sim_text_hdsd}              xpath=//*[contains(@text,'Hướng dẫn sử dụng eSIM Bphone B86')]
${eSim_ma_mau_1}                         xpath=//*[contains(@text,'Màu 1')]
${eSim_ma_mau_2}                         xpath=//*[contains(@text,'Màu 2')]
${eSim_ma_mau_3}                         xpath=//*[contains(@text,'Màu 3')]
${eSim_ma_mau_4}                         xpath=//*[contains(@text,'Màu 4')]
${eSim_ma_mau_5}                         xpath=//*[contains(@text,'Màu 5')]
${eSim_ma_mau_6}                         xpath=//*[contains(@text,'Màu 6')]
${eSim_ma_mau_7}                         xpath=//*[contains(@text,'Màu 7')]
${eSim_ma_mau_8}                         xpath=//*[contains(@text,'Màu 8')]
${eSim_ma_mau_9}                         xpath=//*[contains(@text,'Màu 9')]
${eSim_ma_mau_10}                        xpath=//*[contains(@text,'Màu 10')]

# Cac phan tu eSim da lap
${eSim_co_sim_ten_sim}                   xpath=//*[contains(@resource-id,'bkav.android.esim:id/name_sim')]
${eSim_co_sim_so_dt_sim}                 xpath=//*[contains(@resource-id,'bkav.android.esim:id/number_sim')]
${eSim_co_sim_icon_sim}                  xpath=//*[contains(@resource-id,'bkav.android.esim:id/icon_sim')]
${eSim_co_sim_chon_checkbox_sim_1}       xpath=(//*[contains(@resource-id,'bkav.android.esim:id/radio_enable')])[1]
${eSim_co_sim_chon_checkbox_sim_2}       xpath=(//*[contains(@resource-id,'bkav.android.esim:id/radio_enable')])[2]
${eSim_co_sim_add_sim}                   id=bkav.android.esim:id/text_add_sim
${eSim_add_icon_sim_1}                   id=bkav.android.esim:id/icon_add
${eSim_co_sim_hs_name_sim}               id=bkav.android.esim:id/sim_name
${eSim_co_sim_hs_so_dt_sim}              id=bkav.android.esim:id/number
${eSim_co_sim_hs_icon_mau_sim}           id=bkav.android.esim:id/color_icon
${eSim_co_sim_hs_mau_sim}                id=bkav.android.esim:id/color_text
${eSim_co_sim_hs_nah_cung_cap}           id=bkav.android.esim:id/carrier
${eSim_co_sim_hs_xoa}                    xpath=//*[contains(@text,'XÓA HỒ SƠ')]
${eSim_co_sim_hs_sim_nut_huy}            xpath=//*[contains(@text,'HỦY')]
${eSim_co_sim_hs_sim_nut_dong_y}         xpath=//*[contains(@text,'ĐỒNG Ý')]
${eSim_co_sim_hs_sim_nut_luu}            xpath=//*[contains(@text,'LƯU')]
${eSim_co_sim_hs_mau_sim_5}              xpath=//*[@text="Màu 5"]
${eSim_co_sim_thong_bao}                 id=android:id/message
${eSim_co_sim_dong}                      xpath=//*[contains(@text,'ĐÓNG')]
${eSim_co_sim_giao_dien_QR}              id=bkav.android.esim:id/zxing_viewfinder_view
${eSim_co_sim_sdt}                       0977888999
${eSim_co_sim_sdt_moi}                   0988999999
${eSim_co_sim_dang_tai_file_ve}          xpath=//*[contains(@text,'Đang tải hồ sơ ...')]
${eSim_co_sim_txt_file_loi}              Tải hồ sơ bị lỗi, bạn cần kiểm tra lại eSIM
${eSim_lap_2_sm_text_khay_sim_1}         xpath=//*[contians(@text,'Khay SIM 1')]
${eSim_lap_2_sm_text_khay_sim_2}         xpath=//*[contians(@text,'Khay SIM 2')]
${eSim_co_lap_sim_ten_sim}               id=bkav.android.esim:id/name_sim

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG GHI AM
#=================================================================================================
${ga_btn}                  bkav.android.recorder:id/recordButton
${ga_btn_tam_dung}         bkav.android.recorder:id/recordButton
${ga_dang_ghi_am}          bkav.android.recorder:id/record_state
${ga_tam_dung}             //*[@text="Tạm dừng"]

${ga_tab_tuy_chon_khac}                      id=bkav.android.recorder:id/setting
${nut_bat_dau_ghi_am}                        id=bkav.android.recorder:id/recordButton
${nut_danh_sach_ban_ghi}                     bkav.android.recorder:id/show_list_record
${ga_mo_da_nhiem}                            bkav.android.launcher3:id/snapshot
${ga_thongbao_tam_dung}                      //*[contains(@text,'Tạm dừng')]
${ga_thongbao_dung}                          //*[contains(@text,'Dừng')]
${ga_checkbox_khong_hoi_lai}                 //*[contains(@text,'Không hỏi lại')]
${ga_checkbox_an_khoi_trinh_choi_nhac}       xpath=(//*[@resource-id="android:id/checkbox"])[3]
${ga_textbox_ten_ban_ghi}                    bkav.android.recorder:id/rename_id
${ga_verify_text_hoa}                        //*[contains(@text,'AAAA')]
${ga_verify_text_thuong}                     //*[contains(@text,'abcd')]
${ga_verify_text_so}                         //*[contains(@text,'123456')]
${ga_verify_text_ki_tu_dac_biet}             //*[contains(@text,'!@#$%^&*')]
${ga__txt_chon_am_nhac}                      xpath=//*[contains(@text,'Âm nhạc')]
${ga_doi_ten_file}                           //*[contains(@text,'DaiTVb')]
${ga_ban_ghi_doi_ten}                        //*[@text="Đổi tên"]
${ga_ban_ghi_xoa}                            //*[contains(@text,'Xóa')]
${ga_ban_ghi_chia_se}                        //*[@text="Chia sẻ"]
${ga_ban_ghi_tao_ghi_am}                     bkav.android.recorder:id/create_new_record
${ga_cb_doi_ten_file_ga}                     xpath=(//*[@resource-id="com.bkav.android.bexplorer:id/listview_checkbox"])[1]

${ga_lockscreen_dang_ghi_am}                 //*[contains(@text,'Đang ghi')]

${ga_nut_back}                               bkav.android.recorder:id/back_button
${ga_tieu_de}                                bkav.android.recorder:id/title_bar
${ga_ban_ghi_list}                           xpath=(//*[@resource-id="bkav.android.recorder:id/layout_item"])[1]
${ga_nut_hoan_tac}                           //*[contains(@text,'Hoàn tác')]
${ga_popup_chia_se_btalk}                    xpath=//*[contains(@text,'Btalk')]
${ga_popup_chia_se_btalk_1}                  xpath=(//*[contains(@text,'Btalk')])[1]
${ga_popup_chia_se_bms}                      xpath=//*[contains(@text,'Bkav Mobile Security')]
${ga_popup_chia_se_bms_1}                    xpath=(//*[contains(@text,'Bkav Mobile Security')])[1]
${ga_popup_chia_se_gmail}                    //*[contains(@text,'Gmail')]
${ga_popup_chia_se_gmail_1}                  xpath=(//*[contains(@text,'Gmail')])[1]
${ga_popup_chia_se_bluetooth}                //*[contains(@text,'Bluetooth')]
${ga_popup_chia_se_bluetooth_1}              xpath=(//*[contains(@text,'Bluetooth')])[1]
${ga_popup_chia_se_messenger}                //*[contains(@text,'Messenger')]
${ga_popup_chia_se_messenger_1}              xpath=(//*[contains(@text,'Messenger')])[1]
${ga_popup_chia_se_chats}                    //*[contains(@text,'Chats')]
${ga_popup_chia_se_chats_1}                  xpath=(//*[contains(@text,'Chats')])[1]
${ga_popup_chia_se_zalo}                     //*[contains(@text,'Zalo')]
${ga_popup_chia_se_zalo_1}                   xpath=(//*[contains(@text,'Zalo')])[1]
${ga_title_bluetooth}                        //*[contains(@text,'Chọn thiết bị Bluetooth')]
${ga_popup_soan_thu_toi}                     xpath=//*[@class="android.widget.EditText"]
${ga_nut_gui_mail}                           com.google.android.gm:id/send
${ga_cai_dat_tu_dat_ten_file}                //*[contains(@text,'Tự đặt tên file')]
${ga_cai_dat_ten_theo_ngay_thang}            //*[contains(@text,'Đặt tên theo ngày tháng')]
${ga_cai_dat_tam_dung_khi_co_cuoc_goi}       //*[contains(@text,'Tạm dừng khi có cuộc gọi đến')]
${ga_cai_dat_giu_sang_man_hinh}              //*[contains(@text,'Giữ sáng màn hình')]
${ga_cai_dat_che_do_im_lang}                 //*[contains(@text,'Chế độ im lặng')]
${ga_ban_ghi_ten}                            xpath=(//*[@resource-id="bkav.android.recorder:id/name_recorder"])[1]
${ga_cai_dat_popup}                          android:id/alertTitle
${ga_truy_cap_khong_lam_phien}               //*[contains(@text,'Truy cập Không làm phiền')]
${ga_bat_ghi_de_khong_lam_phien}             android:id/switch_widget
${ga_popup_nut_cai_dat}                      //*[contains(@text,'CÀI ĐẶT')]
${ga_main_text_trang_thai_ghi_am}            bkav.android.recorder:id/record_state

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG GHI CHEP
#=================================================================================================
${gc_dang_xu_ly}              xpath=//*[contains(@text,'Đang xử lý')]

${nut_menu_trai}              xpath=//android.widget.ImageButton[@content-desc="Điều hướng lên trên"]
${Text_dang_xu_li}            xpath=//*[contains(@text,'Đang xử lý')]
${Text_luu_tru}               xpath=//*[@text="Lưu trữ"]
${Text_thung_rac}             xpath=//*[contains(@text,'Thùng rác')]
${nut_tuy_chon_khac}          xpath=//android.widget.ImageView[@content-desc="Tùy chọn khác"]
${btn_add}                    bkav.android.bkavnote:id/fab
${Text_add_ghi_chep}          xpath=//*[contains(@text,'Ghi chép')]
${Text_add_checklist}         xpath=//*[contains(@text,'Checklist')]
${Text_add_ghi_am}            xpath=//*[contains(@text,'Ghi âm')]
${Text_add_anh}               xpath= //*[contains(@text,'Ảnh')]
${Tieu_de}                    //*[contains(@text,'Tiêu đề')]
${Ghi_chu}                    //*[contains(@text,'Ghi chú')]
${Tao_ghi_chep_thanh_cong}    bkav.android.bkavnote:id/titleTextView
${Tao_anh_thanh_cong}         //*[@text="tao anh thanh cong"]
${Chon_anh}                   //*[contains(@text,'Chọn ảnh')]
${Chi_mot_lan}                //*[contains(@text,'CHỈ MỘT LẦN')]
${Chon_anh_bat_ky}            xpath=(//*[@content-desc="Ảnh"])[1]
${chup_man_hinh}              //*[contains(@text,'Chụp màn hình')]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG HO TRO
#=================================================================================================
${ht_icon_avatar}                  id=bkav.android.connect:id/avatar
${ht_icon_tuy_chon_khac}           xpath=//*[@content-desc="Tùy chọn khác"]
${ht_btn_tab_nhat_ky_QXDM}         xpath=//*[contains(@text,'Nhật ký QXDM')]
${ht_btn_bat_dau}                  xpath=//*[contains(@text,'BẮT ĐẦU')]
${ht_btn_dung_lai}                 xpath=//*[contains(@text,'DỪNG LẠI')]
${ht_btn_xoa_nhat_ky}              xpath=//*[contains(@text,'Xóa nhật ký QXDM')]
${ht_btn_bao_cao_loi}              xpath=//*[contains(@text,'Báo cáo lỗi')]
${ht_btn_thong_bao_bkav}           xpath=//*[contains(@text,'Nhận thông báo của Bkav')]
${ht_btn_tab_cap_nhat_offline}     xpath=//*[contains(@text,'Cập nhật hệ thống offline')]
${ht_btn_xuat_trang_thai_may}      xpath=//*[contains(@text,'Xuất trạng thái máy')]
${ht_btn_tab_bat_nhat_ky}          xpath=//*[contains(@text,'Bật nhật ký')]
${ht_txt_so_dien_thoai}            xpath=//*[contains(@text,'1800 545448')]
${ht_sdt}                          1800 545448
${ht_phu_kien_op_lung}             Ốp lưng B86 nhựa cứng cao cấp
${ht_dang_tao_loi_1}               Báo cáo lỗi
${ht_dang_tao_loi_1a}              đang được tạo
${ht_dang_tao_loi_2}               Báo cáo lỗi
${ht_dang_tao_loi_2a}              đang được tạo
${ht_link_Bstore}                  bphone.vn/bphone-stores
${ht_link_bphoto}                  bphone.vn/bphoto
${ht_btn_icon_Bstore}              id=bkav.android.connect:id/bstore
${ht_btn_icon_chat}                id=bkav.android.connect:id/chat_cskh
${ht_btn_icon_phu_kien}            id=bkav.android.connect:id/accessories
${ht_btn_icon_bphoto}              id=bkav.android.connect:id/bphoto
${ht_btn_icon_goi_dien}            id=bkav.android.connect:id/call_supporters
${ht_txt_chim_lac}                 xpath=//*[contains(@text,'Chim Lạc')]
${ht_btn_luon_chon}                xpath=//*[contains(@text,'LUÔN CHỌN')]
${ht_txt_phu_kien_op_lung}         xpath=//*[contains(@text,'Ốp lưng B86 nhựa cứng cao cấp')]
${ht_txt_bao_loi_1}                xpath=//*[contains(@text,'Báo cáo lỗi')]
${ht_txt_bao_loi_2}                xpath=//*[contains(@text,'Báo cáo lỗi')]
${ht_click_toa_do_x}               182
${ht_click_toa_do_y}               646
${ht_click_check_box_bao_loi}            xpath=(//*[@resource-id="android:id/checkbox"])[1]
${ht_click_check_box_thong_bao_bkav}      xpath=(//*[@resource-id="android:id/checkbox"])[2]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG KHAM PHA
#=================================================================================================
${kp_nut_xem_huong_dan}    bkav.android.explorer:id/start_chimlac_btn
${kp_nut_hd_sau}           bkav.android.explorer:id/next_page_btn
${kp_nut_hd_truoc}         bkav.android.explorer:id/previous_page_btn

# Cac URL huong dan
${kp_url_chong_trom}                   https://bphone.vn/chong-trom
${kp_url_chong_trom_sau_dieu_huong}    bphone.vn/chong-trom
${kp_url_android}                      https://bphone.vn/android
${kp_url_android_sau_dieu_huong}       bphone.vn/huong-dan-su-dung/-/view-content/243772/huong-dan-chuyen-du-lieu-tu-android-sang-bos
${kp_url_ios}                          https://bphone.vn/ios
${kp_url_ios_sau_dieu_huong}           bphone.vn/huong-dan-su-dung/-/view-content/243746/huong-dan-chuyen-du-lieu-tu-ios-sang-bos
${kp_url_sac_pin}                      https://bphone.vn/sac-pin
${kp_url_sac_pin_sau_dieu_huong}       bphone.vn/huong-dan-su-dung/-/view-content/243733/mot-so-luu-y-e-sac-pin-bphone-hieu-qua-hon

# Cac tieu de trong trang web huong dan
${kp_txt_chong_trom}    TÍNH NĂNG CHỐNG TRỘM
${kp_txt_android}       Hướng dẫn chuyển dữ liệu từ Android sang BOS
${kp_txt_ios}           Hướng dẫn chuyển dữ liệu từ IOS sang BOS
${kp_txt_sac_pin}       Một số lưu ý để sạc pin Bphone hiệu quả hơn

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG MAY TINH
#=================================================================================================
# Cac phan tu trong giao dien hien thi
${mt_hien_thi}              id=com.bkav.calculator2:id/display
${mt_hien_thi_phep_tinh}    id=com.bkav.calculator2:id/formula
${mt_hien_thi_ket_qua}      id=com.bkav.calculator2:id/result
${mt_loi_ko_phai_so}        xpath=//*[@text="Not a number"]

${space_cuoi_toa_do_x}    1016
${space_cuoi_toa_do_y}    173
${space_dau_toa_do_x}    178
${space_dau_toa_do_y}    173

# Cac phan tu trong giao dien chinh
${mt_nut_0}       id=com.bkav.calculator2:id/digit_0
${mt_nut_1}       id=com.bkav.calculator2:id/digit_1
${mt_nut_2}       id=com.bkav.calculator2:id/digit_2
${mt_nut_3}       id=com.bkav.calculator2:id/digit_3
${mt_nut_4}       id=com.bkav.calculator2:id/digit_4
${mt_nut_5}       id=com.bkav.calculator2:id/digit_5
${mt_nut_6}       id=com.bkav.calculator2:id/digit_6
${mt_nut_7}       id=com.bkav.calculator2:id/digit_7
${mt_nut_8}       id=com.bkav.calculator2:id/digit_8
${mt_nut_9}       id=com.bkav.calculator2:id/digit_9
${mt_nut_cong}    id=com.bkav.calculator2:id/op_add
${mt_nut_tru}     id=com.bkav.calculator2:id/op_sub
${mt_nut_nhan}    id=com.bkav.calculator2:id/op_mul
${mt_nut_chia}    id=com.bkav.calculator2:id/op_div
${mt_nut_bang}    id=com.bkav.calculator2:id/eq
${mt_nut_phay}    xpath=//*[@text="."]
${mt_nut_clr}     xpath=//*[@text="CLR"]
${mt_nut_xoa}     xpath=//*[@text="DEL"]

# Cac phan tu trong giao dien lich su
${mt_nut_lich_su}             xpath=//*[@text="History"]
${mt_txt_khong_co_lich_su}    xpath=//*[@text="No History"]
${mt_nut_xoa_lich_su}         xpath=//*[@text="Clear"]
${mt_nut_ket_qua_ls}          com.bkav.calculator2:id/bkav_history_result
${mt_nut_bieu_thuc_ls}        com.bkav.calculator2:id/bkav_history_formula
${mt_text_hien_thi_phep_tinh_ls}    xpath=//*[@text="456−123"]
${mt_text_hien_thi_phep_tinh_ls1}    xpath=//*[@text="456−456"]

# Cac phan tu trong giao dien nang cao
${mt_nut_nang_cao}      id=com.bkav.calculator2:id/bt_more
${mt_nut_inv}           id=com.bkav.calculator2:id/toggle_inv
${mt_nut_deg_rad}       id=com.bkav.calculator2:id/toggle_mode
${mt_nut_phan_tram}     id=com.bkav.calculator2:id/op_pct
${mt_nut_mc}            id=com.bkav.calculator2:id/op_m_c
${mt_nut_sin}           id=com.bkav.calculator2:id/fun_sin
${mt_nut_cos}           id=com.bkav.calculator2:id/fun_cos
${mt_nut_tan}           id=com.bkav.calculator2:id/fun_tan
${mt_nut_m_cong}        id=com.bkav.calculator2:id/op_m_plus
${mt_nut_ln}            id=com.bkav.calculator2:id/fun_ln
${mt_nut_log}           id=com.bkav.calculator2:id/fun_log
${mt_nut_giai_thua}     id=com.bkav.calculator2:id/op_fact
${mt_nut_m_tru}         id=com.bkav.calculator2:id/op_m_sub
${mt_nut_pi}            id=com.bkav.calculator2:id/const_pi
${mt_nut_e}             id=com.bkav.calculator2:id/const_e
${mt_nut_luy_thua}      id=com.bkav.calculator2:id/op_pow
${mt_nut_mr}            id=com.bkav.calculator2:id/op_m_r
${mt_nut_ngoac_trai}    id=com.bkav.calculator2:id/lparen
${mt_nut_ngoac_phai}    id=com.bkav.calculator2:id/rparen
${mt_nut_can_bac_2}     id=com.bkav.calculator2:id/op_sqrt

# Cac phan tu trong giao dien nang cao va che do INV
${mt_nut_arcsin}    id=com.bkav.calculator2:id/fun_arcsin
${mt_nut_arccos}    id=com.bkav.calculator2:id/fun_arccos
${mt_nut_arctan}    id=com.bkav.calculator2:id/fun_arctan
${mt_nut_e_mu}      id=com.bkav.calculator2:id/fun_exp
${mt_nut_10_mu}     id=com.bkav.calculator2:id/fun_10pow
${mt_nut_mu_2}      id=com.bkav.calculator2:id/op_sqr

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG NGHE DAI
#=================================================================================================
# Cac phan tu man hinh yeu thich
${nd_phat_dai}    bkav.android.radioonline:id/play
${nd_stop}        bkav.android.radioonline:id/stop
${nd_txt_dang_phat}         Đang phát
${nd_txt_tam_dung}          Tạm dừng
${nd_tab_yeu_thich}                  xpath=//*[contains(@text,'Favorites')]
${nd_tab_tat_ca_kenh}                xpath=//*[contains(@text,'All channels')]
${nd_tab_kenh_vua_nghe}              xpath=//*[contains(@text,'Recent channels')]
${nd_phan_tu_theo_avata}    xpath=//*[contains(@resource-id,'bkav.android.radioonline:id/avatarparent')]

${nd_txt_vov2}    xpath=//*[contains(@text,'VOV2')]
${nd_txt_hien_thi_1}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[1]
${nd_txt_vov2_chi_tiet}    xpath=//*[contains(@text,'Hệ Văn hóa - Đời sống - Khoa giáo')]
${nd_txt_vov1}   xpath=//*[contains(@text,'VOV1')]
${nd_txt_hien_thi_2}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[2]
${nd_txt_vov1_chi_tiet}    xpath=//*[contains(@text,'Events - Politics - Total')]
${nd_txt_vov3}    xpath=//*[contains(@text,'VOV3')]
${nd_txt_hien_thi_8}    xpath=//android.view.ViewGroup[3]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_txt_vov3_chi_tiet}     xpath=//*[contains(@text,'Hệ Âm nhạc - Thông tin - Giải trí')]
${nd_txt_nhac_quoc_te}    xpath=//*[contains(@text,'Nhạc Quốc tế')]
${nd_txt_hien_thi_3}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[3]
${nd_txt_chi_tiet_nhac_quoc_te}    xpath=//*[contains(@text,'Những bản nhạc quốc tế bất hủ')]
${nd_txt_nhac_Audiophile}    xpath=//*[contains(@text,'Nhạc Audiophile')]
${nd_txt_hien_thi_4}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[4]
${nd_txt_chi_tiet_nhac_Audiophile}    xpath=//*[contains(@text,'Âm nhạc – Nhạc cho người nghe chuyên nghiệp')]
${nd_txt_nhac_thieu_nhi_quoc_te}    xpath=//*[contains(@text,'Nhạc Thiếu nhi quốc tế')]
${nd_txt_hien_thi_5}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[5]
${nd_txt_chi_tiet_nhac_thieu_nhi_quoc_te}    xpath=//*[contains(@text,'Tuyển chọn các bài hát thiếu nhi tiếng Anh được yêu...')]
${nd_txt_nhac_Classical_Guitar}    xpath=//*[contains(@text,'Classical Guitar')]
${nd_txt_hien_thi_6}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[6]
${nd_txt_chi_tiet_nhac_Classical_Guitar}    xpath=//*[contains(@text,'Những tác phẩm độc tấu guitar cổ điển hay nhất')]
${nd_txt_nhac_Nhạc Jazz}    xpath=//*[contains(@text,'Nhạc Jazz')]
${nd_txt_hien_thi_7}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[7]
${nd_txt_chi_tiet_Nhạc Jazz}    xpath=//*[contains(@text,'Những bản nhạc Jazz hay nhất')]
${nd_txt_hien_thi_9}    	id=//android.view.ViewGroup[9]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_txt_hien_thi_10}    	id=//android.view.ViewGroup[10]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_txt_hien_thi_11}  	id=//android.view.ViewGroup[11]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_btn_reset}    	id=bkav.android.radioonline:id/reset
${nd_txt_chi_tiet_reset}    xpath=//*[contains(@text,'Tải lại danh sách yêu thích mặc định')]
${nd_list_phan_tu}    xpath=//android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]

# Cac phan tu man hinh tat ca kenh
${nd_tctl_btn_tat_cac_the_loai}    id=bkav.android.radioonline:id/spinner
${nd_tctl_txt_tat_ca_cac_the_loai}    id=id=bkav.android.radioonline:id/name
${nd_tck_icon_search}    id=bkav.android.radioonline:id/btn_search
${nd_btn_tab_about}    id=bkav.android.radioonline:id/btn_about

# Cac phan tu tat ca cac the loai
${nd_tctl_txt_chon_tat_ca}         xpath=//*[contains(@text,'All categories')]
${nd_tctl_txt_chon_chinh_tri}      xpath=//*[contains(@text,'Politics')]
${nd_tctl_txt_chon_tin_tuc}        xpath=//*[contains(@text,'News')]
${nd_tctl_txt_chon_giai_tri}       xpath=//*[contains(@text,'Entertainment')]
${nd_tctl_txt_chon_du_lich}        xpath=//*[contains(@text,'Travel')]
${nd_tctl_txt_chon_xa_hoi}         xpath=//*[contains(@text,'Society')]
${nd_tctl_txt_chon_tong_ho}        xpath=//*[contains(@text,'General')]
${nd_tctl_txt_chon_the_thao}       xpath=//*[contains(@text,'Sports')]
${nd_tctl_txt_chon_van_hoa}        xpath=//*[contains(@text,'Culture')]
${nd_tctl_txt_chon_am_nhac}        xpath=//*[contains(@text,'Music')]
${nd_tctl_txt_chon_giao_duc}       xpath=//*[contains(@text,'Education')]
${nd_tctl_txt_chon_giao_thong}     xpath=//*[contains(@text,'Traffic')]

# Cac phan tu tim kiem
${nd_tctl_tk_btn_back_tim_kiem}    id=bkav.android.radioonline:id/back_button
${nd_tctl_tk_input_tim_kiem}     xpath=//*[contains(@text,'Search')]

# Cac phan tu thong tin chi tiet
${nd_ttct_txt_thong_tin}          xpath=//*[contains(@text,'About:')]
${nd_ttct_txt_tieu_de_dien_thoai}    xpath=//*[contains(@text,'Phone:')]
${nd_ttct_txt_sdt}                xpath=//*[contains(@text,'6779')]
${nd_ttct_txt_tieu_de_hop_thu}    xpath=//*[contains(@text,'Email:')]
${nd_ttct_txt_hop_thu}            xpath=//*[contains(@text,'bkav@bkav.com')]
${nd_ttct_txt_tieu_de_trang_chu}    xpath=//*[contains(@text,'Website:')]
${nd_ttct_txt_link_trang_chu}       xpath=//*[contains(@text,'http://www.bkav.com')]

# Cac phan tu man hinh kenh vua nghe
${nd_yt_btn_clear}    xpath=//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView

# Cac phan tu nut
${nd_yt_hien_thi_txt_nhac_dang_phat}    xpath=//*[contains(@resource-id,'bkav.android.radioonline:id/radio_name')]
${nd_yt_txt_chi_tiet_nhac_quoc_te_dang_phat}    xpath=//*[contains(@text,'Những bản nhạc quốc tế bất hủ')]
${nd_yt_btn_stop}    id=bkav.android.radioonline:id/stop
${nd_yt_btn_play}    id=bkav.android.radioonline:id/play
${nd_yt_btn_time}    id=bkav.android.radioonline:id/timer
${nd_yt_txt_hien_thi_kenh_dang_phat}      //android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView[1]
${nd_yt_btn_icon_yeu_thich}    xpath=(//*[@resource-id="bkav.android.radioonline:id/star"])[2]
${nd_yt_txt_chi_tiet}    xpath=//*[@text="Cool FM 103,5"]
${nd_yt_list_phan_tu_hien_thi}    xpath=//android.widget.RelativeLayout[2]/android.widget.TextView[1]

# Cac phan tu danh sach mac dinh
${nd_yt_txt_1}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[1]
${nd_yt_txt_2}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[2]
${nd_yt_txt_3}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[3]
${nd_yt_txt_4}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[4]
${nd_yt_txt_5}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[5]
${nd_yt_txt_6}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[6]
${nd_yt_txt_7}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[7]
${nd_yt_txt_8}    xpath=//android.widget.RelativeLayout[8]/android.widget.RelativeLayout[2]/android.widget.TextView[1]

${nd_yt_btn_hen_gio}    id=bkav.android.radioonline:id/timer
${nd_yt_txt_hien_thi_thoi_gian_hen_gio}    id=bkav.android.radioonline:id/text_time_cancel
${nd_yt_txt_thong_tin_hen_gio}    xpath=//*[@text="Radio will be off after"]
${nd_yt_so_thoi_gian_hen_gio}    id=android:id/numberpicker_input
${nd_yt_btn_huy_hen_gio}    xpath=//*[contains(@text,'CANCEL')]
${nd_yt_btn_Xong_hen_gio}    xpath=//*[contains(@text,'DONE')]
${nd_yt_txt_thong_bao_ket_thuc_hen_gio}    xpath=//*[contains(@text,'Music will stop at:')]
${nd_yt_txt_bo_hen_gio}    xpath=//*[contains(@text,'Touch to cancel timer')]
${nd_yt_txt_ngung_phat}    xpath=//*[contains(@text,'Paused')]
${nd_yt_txt_dang_phat}    xpath=//*[contains(@text,'Playing')]
${nd_yt_txt_khong_co_ket_noi}    xpath=//*[contains(@text,'Không có kết nối')]
${nd_yt_btn_them_moi}    id=bkav.android.radioonline:id/float_menu
${nd_yt_icon_them_moi}    xpath=(//*[@resource-id="bkav.android.radioonline:id/mini_fab"])[2]
${nd_yt_icon_de_xuat}    xpath=(//*[@resource-id="bkav.android.radioonline:id/mini_fab"])[1]

${nd_tm_input_link_kenh}    id=bkav.android.radioonline:id/stream
${nd_tm_trang_thai_nghe_thu}    xpath=//*[contains(@text,'PREVIEW')]
${nd_tm_trang_thai_mat_ket_noi}    xpath=//*[contains(@text,'DISCONNECT')]
${nd_tm_input_ten_kenh}    xpath=(//*[@resource-id="bkav.android.radioonline:id/name"])[1]
${nd_tm_checklist_the_loai}    xpath=(//*[@resource-id="bkav.android.radioonline:id/name"])[2]
${nd_tm_checklist_quoc_gia}    xpath=(//*[@resource-id="bkav.android.radioonline:id/name"])[3]
${nd_tm_input_mo_ta}    id=bkav.android.radioonline:id/description
${nd_tm_btn_them_kenh_moi}    xpath=//*[contains(@text,'ADD CHANNEL')]
${nd_tm_khu_vuc_quoc_gia}    xpath=//*[contains(@text,'Vietnam')]
${nd_tm_btn_kenh_moi}    xpath=//*[contains(@text,'kenh_moi')]

${nd_dx_web_url}    id=bkav.android.radioonline:id/web_url
${nd_dx_radio_name}    id=bkav.android.radioonline:id/name
${nd_dx_description}    id=bkav.android.radioonline:id/description
${nd_dx_btn_them_moi_de_xuat}    xpath=//*[contains(@text,'RECOMMEND')]

${nd_notification_ten_kenh_dang_phat}     bkav.android.radioonline:id/status_bar_track_name
${nd_notification_text_kenh_dang_phat}    VOV3 - Đang phát
${nd_notification_text_kenh_dang_dung}    VOV3 - Tạm dừng
${nd_notification_text_kenh_dang_phat1}    Nhạc Quốc tế - Đang phát
${nd_notification_nut_play}        bkav.android.radioonline:id/status_bar_play
${nd_notification_nut_next}        bkav.android.radioonline:id/status_bar_next
${nd_notification_nut_back}        bkav.android.radioonline:id/status_bar_prev
${nd_notification_nut_thoat}         bkav.android.radioonline:id/status_bar_collapse
${nd_notification_nut_clear_danh_sach}         com.android.systemui:id/bkav_remove_notify
${nd_notification_khong_co_dv}    xpath=//*[contains(@text,'Không có dịch vụ')]
${nd_notification_text_hien_thi_khong_co_dv}    Không có dịch vụ
${nd_notification_vov2}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[2]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG NGHE NHAC
#=================================================================================================
${nn_view}                         bkav.android.music:id/control_track_view
${nn_nut_phat_dung_nhac}           bkav.android.music:id/button_play_and_pause
${nn_nut_dang_phat}                //*[contains(@text,'Đang phát')]
${nn_nut_thu_muc}                  //*[contains(@text,'Thư mục')]
${nn_nut_danh_sach}                //*[contains(@text,'Danh sách')]
${nn_nut_nghe_si}                  //*[contains(@text,'Nghệ sĩ')]
${nn_nut_album}                    //*[contains(@text,'Album')]
${nn_nut_tuy_chon}                 bkav.android.music:id/toolbar
${nn_checkbox_an_baihat_dl_thap}   //*[contains(@text,'Ẩn bài hát dung lượng thấp')]
${nn_notify_play_pause}            android:id/action1
${nn_danh_sach_my_recording}               //*[contains(@text,'My recordings')]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG NHAC VIEC
#=================================================================================================
${nv_nut_them_moi}    bkav.android.reminder:id/fab

# Cac phan tu trong giao dien hien thi
${NV_nut_menu_trai}				    //*[contains(@content-desc,'Điều hướng lên trên')]
${NV_nut_tim_kiem}					bkav.android.reminder:id/action_search
${NV_nut_them_nhac_viec}			bkav.android.reminder:id/fab

# Cac phan tu trong menu nhac viec
${NV_nut_tat_ca_nhac_viec}			//*[contains(@text,'Tất cả nhắc việc')]
${NV_nut_cong_viec}				xpath=//*[contains(@text,'Công việc')]
${NV_nut_dam cuoi}					xpath=//*[contains(@text,'Đám cưới')]
${NV_nut_sinh_nhat}				xpath=//*[contains(@text,'Sinh nhật')]
${NV_nut_luu_tru}					xpath=//*[contains(@text,'Lưu trữ')]
${NV_nut_cai_dat}					xpath=//*[contains(@text,'Cài đặt')]

${NV_text_nhap_tao_muc_moi}    bkav.android.reminder:id/customPanel
${NV_dh_bt_nut_huy}            android:id/button2
${NV_nut_tao}                  android:id/button1

${NV_nut_dieu_huong}                   //android.widget.ImageButton[@content-desc="Điều hướng lên trên"]
${NV_damcuoi_plaintext}                id=bkav.android.reminder:id/content_plaintext
${NV_chon_ngay}                     id=bkav.android.reminder:id/spinner_pick_date
${NV_date_hom_nay}                     xpath=//*[contains(@text,'Hôm nay')]
${NV_date_ngay_mai}                     xpath=//*[contains(@text,'Ngày mai')]
${NV_nhac_nho_reminder_before}          id=bkav.android.reminder:id/spinner_pick_date_reminder_before
${NV_reminder_khong_nhac_truoc}       xpath=//*[contains(@text,'Không nhắc trước')]
${NV_reminder_nhac_truoc_1_ngay}       xpath=//*[contains(@text,'Nhắc trước 1 ngày')]
${NV_reminder_nhac_truoc_2_ngay}       xpath=//*[contains(@text,'Nhắc trước 2 ngày')]
${NV_damcuoi_repeat}                   id=bkav.android.reminder:id/spinner_repeat
${NV_repeat_khong_lap}                 xpath=//*[contains(@text,'Không lặp')]
${NV_repeat_hang_nam}                 xpath=//*[contains(@text,'Hàng năm')]
${NV_damcuoi_checkbox}                 id=bkav.android.reminder:id/ring_type_checkbox
${NV_nut_luu}                          xpath=//*[contains(@text,'LƯU')]
${NV_cong_viec_nhac_truoc}            id=bkav.android.reminder:id/spinner_pick_time_task

# Cac phan tu TRONG GIAO DIEN HIEN THI
${menu_trai}				    //*[contains(@content-desc,'Điều hướng lên trên')]
${nut_tim_kiem}					bkav.android.reminder:id/action_search

# Cac phan tu TRONG MENU NHAC VIEC
${nut_tat_ca_nhac_viec}			//*[contains(@text,'Tất cả nhắc việc')]
${nut_cong_viec}    xpath=(//*[@resource-id="bkav.android.reminder:id/mini_fab"])[3]
${nut_dam cuoi}					//*[@text="Đám cưới"]
${nut_sinh_nhat}				//*[contains(@text,'Sinh nhật')]
${nut_luu_tru}					//*[contains(@text,'Lưu trữ')]
${nut_them_muc}					//*[contains(@text,'Thêm mục')]
${nut_cai_dat}					//*[contains(@text,'Cài đặt')]

# Cac phan tu TRONG GIAO DIEN THEM MUC MOI TRONG MENU NHAC VIEC
${text_nhap_tao_muc_moi}    bkav.android.reminder:id/customPanel
${nut_tao}                  android:id/button1

# Cac phan tu NUT THEM NHAC VIEC
${nut_dieu_huong}                   //android.widget.ImageButton[@content-desc="Điều hướng lên trên"]
${nhap_ten_cong_viec}             //*[contains(@text,'Tên công việc')]
${nhap_ten_sinh_nhat}             //*[contains(@text,'Sinh nhật của ai?')]
${nhap_ten_ngay_cuoi}             //*[contains(@text,'Ngày cưới của ai?')]
${nut_luu}                        //*[contains(@text,'LƯU')]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG QUAN LY FILE
#=================================================================================================
${qlf_bo_nho}                    com.bkav.android.bexplorer:id/rl_home_activity_storage_layout
${qlf_cd_amthanh}                //*[contains(@text,'Âm thanh')]
${qlf_cd_amthanh_nhac_chuong}    //*[contains(@text,'Nhạc chuông điện thoại')]
${qlf_cd_icon_qlf}               //*[contains(@text,'Quản lý file')]
${qlf_tao_thumuc_moi}            //*[contains(@text,'Tạo thư mục mới')]
${qlf_textbox_ten_thu_muc}       com.bkav.android.bexplorer:id/etFileName
${qlf_checkbox_thu_muc_moi_tao}    (//*[@resource-id="com.bkav.android.bexplorer:id/listview_checkbox"])[1]
${qlf_nut_nhieu_hon}                //*[contains(@text,'Nhiều hơn')]
${qlf_nut_copy}                    //*[contains(@text,'Copy')]
${qlf_nut_doi_ten}                   //*[contains(@text,'Đổi tên')]
${qlf_the_nho_SD}                  //*[contains(@text,'Thẻ nhớ SD')]
${qlf_tn_sd_thu_muc}               //*[contains(@text,'SỬ DỤNG THƯ MỤC NÀY')]
${qlf_ghi_am}                      //*[contains(@text,'Recorders')]
${qlf_tep_ghi_am}                  //*[contains(@text,'SoundRecorder')]
${qlf_chon_tat_ca}                 //*[contains(@text,'Chọn tất cả')]

${ht_txt_Android}                   xpath=//*[contains(@text,'Android')]
${ht_txt_Logs}                      xpath=//*[contains(@text,'Logs')]
${ht_txt_diag_logs}                 xpath=//*[contains(@text,'diag_logs')]
${ht_text_diag_logs}                diag_logs
${ht_txt_files}                     xpath=//*[contains(@text,'files')]
${ht_vi_tri_file_ota}               xpath=//*[contains(@text,'TỆP GẦN ĐÂY')]
${ht_text_file_ota}                 TỆP GẦN ĐÂY

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG QUAY MAN HINH
#=================================================================================================
${cho_phep_quay_man_hinh}    com.android.permissioncontroller:id/permission_allow_button

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG SUC KHOE
#=================================================================================================
# Cac phan tu trong man hinh chinh cua ung dung Suc khoe
${sk_nut_tao_bai_tap}  	   id=bkav.fitness.activity:id/bt_create_work_out

# Cac phan tu trong Muc tieu cho bai tap
${sk_txt_muc_tieu}    	   Mục tiêu cho bài tập
${sk_thoi_gian}            xpath=//*[@text="Thời gian"]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG THOI TIET
#=================================================================================================
# Cac phan tu trong man hinh chinh
${tt_btn_clear_text_tim_kiem}    id=bkav.android.weather:id/btn_clear
${tt_ds_dia_diem}           xpath=//*[@resource-id="bkav.android.weather:id/location_name"]
${tt_ds_nhiet_do}           xpath=//*[@resource-id="bkav.android.weather:id/location_temp"]
${tt_dia_diem_mac_dinh}     xpath=(//*[@resource-id="bkav.android.weather:id/location_name"])[1]
${tt_nhiet_do_mac_dinh}     xpath=(//*[@resource-id="bkav.android.weather:id/location_temp"])[1]
${tt_nut_do_c}              xpath=//*[@text="°C"]
${tt_nut_do_f}              xpath=//*[@text="°F"]
${tt_nut_them_thanh_pho}    id=bkav.android.weather:id/add_location_id
${tt_nut_xoa}               xpath=//*[@text="Delete"]
${tt_txt_ket_noi_mang}      No Internet Connection

# Cac phan tu trong man hinh tim kiem
${tt_tk_nhap_ten_thanh_pho}    xpath=//*[@text="Insert city's name"]
${tt_tk_ds_ten_thanh_pho}      xpath=//*[@resource-id="android:id/text1"]
${tt_tk_ha_noi}                xpath=//*[contains(@text,'Hà Nội')]
${tt_tk_text_ha_noi}           xpath=//*[contains(@text,'Hà Nội, VN')]
${tt_tk_quang_ninh}            xpath=//*[contains(@text,'Quảng Ninh')]
${tt_tk_ha_nam}                xpath=//*[contains(@text,'Hà Nam')]
${tt_tk_text_ha_nam}           xpath=//*[contains(@text,'Hà Nam, VN')]
${tt_tk_text_icon_tien_ich}    xpath=//*[@content-desc="Widgets"]
${tt_tk_txt_tien_ich_thoi_tiet}    xpath=//*[@text="Weather widgets"]

# Cac phan tu trong man hinh thong tin thoi tiet
${tt_txt_24h_toi}    xpath=//*[contains(@text,'Next 24h')]
${tt_ds_gio}         xpath=//*[@resource-id="bkav.android.weather:id/hourly"]
${tt_ds_thu}         xpath=//*[@resource-id="bkav.android.weather:id/daily"]
${tt_ds_thu_3}       xpath=(//*[@resource-id="bkav.android.weather:id/daily"])[3]
${tt_nut_hom_nay}                    xpath=//*[@text="Today")]
${tt_vi_tri_nut_hom_nay}    xpath=//android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.TextView

# Cac phan tu trong man hinh chi tiet thoi tiet
${tt_ttct_bieu_do_gio}     bkav.android.weather:id/chart_hourly_weather
${tt_ttct_bieu_do_ngay}    bkav.android.weather:id/chart_day_weather

# Cac phan tu trong ung dung OpenWeather
${tt_open_weather_nut_cai_dat}    //*[@content-desc="Go to settings"]
${tt_open_weather_nut_thay_doi_don_vi_do}      //*[@text="Customize units"]
${tt_open_weather_opt_do_c_open_weather}       //*[@text="°C"]
${tt_open_weather_opt_do_f_open_weather}       //*[@text="°F"]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG XEM PHIM
#=================================================================================================
# Cac phan tu trong giao dien thu muc
${xp_tm_nut_tim_kiem}                   xpath=//*[@content-desc="Tìm kiếm…"]
${xp_tm_xem_video_gan_day}              xpath=//*[@content-desc="Xem video gần đây"]
${xp_tm_text_tim_kiem_phim}             xpath=//*[contains(@text,'Tìm kiếm phim')]
${xp_tm_nut_xoa_truy_van}               xpath=//*[@content-desc="Xóa truy vấn"]
${xp_tm_nut_thu_gon}                    xpath=//*[@content-desc="Thu gọn"]
${xp_tm_text_hshfj}                     xpath=//*[contains(@text,'hshfj')]
${xp_tm_nut_sap_xep_theo}               xpath=//*[contains(@text,'Sắp xếp theo…')]
${xp_tm_nut_cap_nhat_lai}               xpath=//*[contains(@text,'Cập nhật lại')]
${xp_tm_nut_thong_tin}                  xpath=//*[contains(@text,'Thông tin')]
${xp_tm_text_dien_thoai}                xpath=//*[contains(@text,'Điện thoại:')]
${xp_tm_text_so_dien_thoai}             xpath=//*[contains(@text,'024 3763 2552')]
${xp_tm_text_hop_thu}                   xpath=//*[contains(@text,'Hộp thư:')]
${xp_tm_text_trang_chu}                 xpath=//*[contains(@text,'Trang chủ:')]
${xp_tm_text_bkav@bkav.com.vn}          xpath=//*[contains(@text,'bkav@bkav.com.vn')]
${xp_tm_text_http://www.bkav.com.vn}    xpath=//*[contains(@text,'http://www.bkav.com.vn')]
${xp_tm_man_da_nhiem}                   id=bkav.android.launcher3:id/snapshot
${xp_tm_app_dien_thoai}                 xpath=//*[contains(@text,'Điện thoại')]
${xp_tm_nut_chi_mot_lan}                xpath=//*[contains(@text,'CHỈ MỘT LẦN')]

${xp_tm_goi_dien_nut_quay_so}              xpath=//android.widget.ImageView[@content-desc="quay số"]
${xp_tm_goi_dien_nut_loa}                  id=com.android.dialer:id/speakerButtonBkav
${xp_tm_goi_dien_nut_ban_phim}             id=com.android.dialer:id/dialpadButtonBkav
${xp_tm_goi_dien_nut_ket_thuc_cuoc_goi}    id=com.android.dialer:id/endButtonBkav_incall

${xp_tm_gui_thu_text_gmail_bkav}           xpath=//android.widget.Button[@content-desc="bkav@bkav.com.vn, bkav@bkav.com.vn"]
${xp_tm_gui_thu_text_soan_thu}             xpath=//*[contains(@text,'Soạn thư')]
${xp_tm_gui_thu_nut_gui}                   xpath=//android.widget.TextView[@content-desc="Gửi"]

${xp_tm_link_TC_title}                     xpath=//android.view.View[@content-desc="www.bkav.com"]
${xp_tm_link_TC_text_bkavpro}              xpath=//android.view.View[@content-desc="Bkav Pro"]/android.widget.TextView
${xp_tm_link_TC_url_bar}                   xpath=android.bkav.bchrome:id/url_bar

${xp_tm_sap_xep_theo_ten}                  xpath=//*[contains(@text,'Tên')]
${xp_tm_sap_xep_theo_ten_az}               xpath=//*[contains(@text,'Tên (z-a)')]
${xp_tm_sap_xep_theo_do_dai}               xpath=//*[contains(@text,'Độ dài')]
${xp_tm_sap_xep_theo_do_dai_giam_dan}      xpath=//*[contains(@text,'Độ dài (Giảm dần)')]
${xp_tm_sap_xep_theo_ngay_cu_nhat}         xpath=//*[contains(@text,'Ngày (Cũ nhất)')]
${xp_tm_sap_xep_theo_ngay_moi_nhat}        xpath=//*[contains(@text,'Ngày (Mới nhất)')]

${xp_tm_ten_vd_stt_1}                      xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[1]
${xp_tm_ten_vd_stt_2}                      xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[2]
${xp_tm_ten_vd_stt_3}                      xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[3]

${xp_tm_thoi_luong_vd_1}                   xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_time')])[1]
${xp_tm_thoi_luong_vd_2}                   xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_time')])[2]
${xp_tm_thoi_luong_vd_3}                   xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_time')])[3]

${xp_tm_video_SampleVideosMP4}             xpath=//*[contains(@text,'SampleVideosMP4')]
${xp_tm_video_SampleVideo_360x240_30mb}    xpath=//*[contains(@text,'SampleVideo_360x240_30mb')]

${xp_tm_text_da_nam_duoc}                  xpath=//*[contains(@text,'Đã nắm được')]

${xp_tm_mo_bang_dien_thoai_text_chi_1_lan}    xpath=//*[contains(@text,'CHỈ MỘT LẦN')]
${xp_tm_sdt_bkav}                             xpath=//*[@text="024 3763 2552"]
${xp_tm_bkav_photo_sdt}                       id=com.android.dialer:id/bkav_photo
${xp_tm_quan_ly_cuoc_goi}                     xpath=//*[contains(@text,'Quản lý cuộc gọi')Ư

${xp_tm_cd_checkboc_chim_lac}                 xpath=(//*[contains(@resource-id,'com.android.permissioncontroller:id/radio_button')])[1]
${xp_tm_cd_checkboc_chrome}                   xpath=(//*[contains(@resource-id,'com.android.permissioncontroller:id/radio_button')])[2]
${xp_tm_cd_Ung_dung_trinh_duyet}              xpath=//*[contains(@text,'Ứng dụng trình duyệt')]
${xp_tm_cd_Ung_dung_mac_dinh}                 xpath=//*[contains(@text,'Ứng dụng mặc định')]
${xp_tm_cd_Ung_dung_thong_bao}                xpath=//*[contains(@text,'Ứng dụng và thông báo')]
${xp_tm_cd_nang_cao}                          xpath=//*[contains(@text,'Nâng cao')]

${xp_tm_chrome_nut_home}                      xpath=//android.widget.ImageButton[@content-desc="Trang chủ"]
${xp_tm_chim_lac_url_bar}                     id=android.bkav.bchrome:id/url_bar
${xp_tm_chrome_url_bar}                       id=com.android.chrome:id/url_bar
${xp_gdrm_chim_lac_nut_dong_tab_dang_mo}      xpath=//android.widget.ImageButton[@content-desc="tab đang mở"]

# Cac phan tu trong giao dien video
${xp_v_ten_video}                       id=com.bkav.video:id/bkav_player_overlay_title
${xp_v_thanh_seekbar}                   id=com.bkav.video:id/bkav_player_overlay_seekbar
${xp_v_nut_khoa}                        id=com.bkav.video:id/bkav_lock_overlay_button
${xp_v_nut_track}                       id=com.bkav.video:id/bkav_player_overlay_tracks
${xp_v_nut_overplay}                    id=com.bkav.video:id/bkav_player_overlay_play
${xp_v_nut_function}                    id=com.bkav.video:id/bkav_player_overlay_adv_function
${xp_v_nut_size}                        id=com.bkav.video:id/bkav_player_overlay_size
${xp_v_video_quay_man_hinh}             xpath=//*[contains(@text,'ScreenRecord_')]
${xp_v_video_quay_camera}               xpath=//*[contains(@text,'VID_')]
${xp_v_mo_video_file_mp4}               xpath=//*[contains(@text,'SampleVideosMP4')]
${xp_v_mo_video_file_3gp}               xpath=//*[contains(@text,'SampleVideos3GP')]
${xp_v_mo_video_file_mkv}               xpath=//*[contains(@text,'SampleVideosMKV')]
${xp_v_mo_video_file_flv}               xpath=//*[contains(@text,'SampleVideosFLV')]
${xp_v_video_thu_muc_dowload}           xpath=//*[contains(@text,'Download')]

${xp_v_video_t1_trong_thu_muc_vd}       xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[1]
${xp_v_video_t2_trong_thu_muc_vd}       xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[2]
${xp_v_video_t3_trong_thu_muc_vd}       xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[3]
${xp_v_video_t4_trong_thu_muc_vd}       xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[4]
${xp_v_video_t5_trong_thu_muc_vd}       xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[5]

${xp_v_nut_di_chuyen_len}               xpath=//android.widget.ImageButton[@content-desc="Di chuyển lên"]
${xp_v_nut_thu_gon}                     xpath=//android.widget.ImageButton[@content-desc="Thu gọn"]
${xp_v_sap_xep_theo}                    xpath=//android.widget.TextView[@content-desc="Sắp xếp theo…"]
${xp_v_cap_nhat_lai}                    xpath=//android.widget.TextView[@content-desc="Cập nhật lại"]
${xp_v_textview}                        id=com.bkav.video:id/actionbar_textview

${xp_v_nut_thong_tin}                   xpath=//*[contains(@text,'Thông tin')]
${xp_v_nut_xoa}                         xpath=//*[contains(@text,'Xóa')]
${xp_v_nut_chia_se}                     xpath=//*[contains(@text,'Chia sẻ')]
${xp_v_thong_tin_nut_play}              id=com.bkav.video:id/bkav_play
${xp_v_thong_tin_nut_xoa}               id=com.bkav.video:id/bkav_info_delete
${xp_v_text_tap_tin_video}              xpath=//*[contains(@text,'Tập tin Video')]
${xp_v_text_tap_tin_audio}              xpath=//*[contains(@text,'Tập tin Audio')]
${xp_v_text_ban_co_chac_khong}          xpath=//*[contains(@text,'Bạn có chắc không?')]
${xp_v_nut_huy}                         xpath=//*[contains(@text,'HỦY')]
${xp_v_nut_dong_y}                      xpath=//*[contains(@text,'ĐỒNG Ý')]

${xp_v_main_toolbar}                    id=com.bkav.video:id/bkav_main_toolbar
${xp_v_video1}                          xpath=//*[contains(@text,'6_1280x720')]
${xp_v_thoi_luong_video_1}              xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_resolution')])[1]
${xp_v_thoi_luong_video_2}              xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_resolution')])[2]
${xp_v_thoi_luong_video_3}              xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_resolution')])[3]
${xp_v_text_khoa_video}                 Bị khóa
${xp_v_chia_se_app_youtobe}             xpath=//*[contains(@text,'YouTube')]
${xp_v_chia_se_app_vala}                xpath=//*[contains(@text,'Vala')]
${xp_v_content_preview_filename}        id=android:id/content_preview_filename

${xp_ds_video6}                         xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[6]

# Cac phan tu trong Tuy chon
${xp_tuy_chon_text_toc_do_phat}              xpath=//*[contains(@text,'Tốc độ phát')]
${xp_tuy_chon_thoi_gian_cho}              xpath=//*[contains(@text,'Thời gian chờ')]
${xp_tuy_chon_phu_de}              xpath=//*[contains(@text,'Tùy chọn phụ đề')]
${xp_tuy_chon_tua_den_vi_tri}              xpath=//*[contains(@text,'Tua đến vị trí')]
${xp_tuy_chon_back}              xpath=//*[contains(@text,'Hiện phím Back')]
${xp_tuy_chon_so_giay_back}              id=com.bkav.video:id/bkav_learn_english_number_second
${xp_tuy_text_so_giay_back}                         10
${xp_tuy_chon_toc_do_phat_1}              xpath=//*[contains(@text,'1.00x')]
${xp_tuy_chon_toc_do_phat_071x}              xpath=//*[contains(@text,'0.71x')]
${xp_tuy_chon_toc_do_phat_1x}              xpath=//*[contains(@text,'1X')]
${xp_tuy_chon_toc_do_phat_050x}              xpath=//*[contains(@text,'0.50x')]
${xp_tuy_chon_toc_do_phat_141x}              xpath=//*[contains(@text,'1.41x')]
${xp_tuy_chon_toc_do_phat_200x}              xpath=//*[contains(@text,'2.00x')]
${xp_tuy_chon_toc_do_phat_05}              xpath=//*[contains(@text,'0.5')]
${xp_tuy_chon_toc_do_phat_1.00}              xpath=//*[contains(@text,'1.00')]
${xp_tuy_chon_toc_do_phat_200}              xpath=//*[contains(@text,'2.00')]

${xp_tuy_chon_text_nhap_thoi_gian}              xpath=//*[contains(@text,'Nhập thời gian')]
${xp_tuy_chon_text_dat_gio}              xpath=//*[contains(@text,'Đặt giờ')]

${xp_tuy_chon_toc_do_phat_071x_bd_toa_do_x}              587
${xp_tuy_chon_toc_do_phat_071x_bd_toa_do_y}              618
${xp_tuy_chon_toc_do_phat_071x_toa_do_x}              429
${xp_tuy_chon_toc_do_phat_071x_toa_do_y}              618

${xp_tuy_chon_toc_do_phat_050x_toa_do_x}              276
${xp_tuy_chon_toc_do_phat_050x_toa_do_y}              618
${xp_tuy_chon_toc_do_phat_050x_bd_toa_do_x}              429
${xp_tuy_chon_toc_do_phat_050x_bd_toa_do_y}              618

${xp_tuy_chon_toc_do_phat_141x_toa_do_x}              750
${xp_tuy_chon_toc_do_phat_141x_toa_do_y}              618
${xp_tuy_chon_toc_do_phat_141x_bd_toa_do_x}              276
${xp_tuy_chon_toc_do_phat_141x_bd_toa_do_y}              618

${xp_tuy_chon_toc_do_phat_200x_toa_do_x}              898
${xp_tuy_chon_toc_do_phat_200x_toa_do_y}              618
${xp_tuy_chon_toc_do_phat_200x_bd_toa_do_x}              750
${xp_tuy_chon_toc_do_phat_200x_bd_toa_do_y}              618
# thoi gian cho

${xp_tuy_chon_text_mac_dinh_thoi_gian}    id=com.bkav.video:id/bkav_sleep_timer_value
${xp_tuy_chon_huy_thoi_gian_cho}    com.bkav.video:id/bkav_sleep_timer_cancel
${xp_tuy_chon_nut_dong_ho}        id=android:id/toggle_mode
${nut_huy_bo_hoa}             //*[contains(@text,'HỦY BỎ')]
${xp_thoi_gian_hien_thi_chay}     com.bkav.video:id/bkav_player_overlay_time
# tua den vi tri

${xp_tuy_chon_nhap_so_phut}     id=com.bkav.video:id/bkav_jump_minutes
${xp_tuy_chon_nhap_so_giay}    id=com.bkav.video:id/bkav_jump_seconds
${xp_nut_tim_kiem}    id=com.bkav.video:id/bkav_ml_menu_search

# so giay back
${xp_text_gio_giay_back}    id=com.bkav.video:id/bkav_jump_seconds

# tre am thanh
${xp_tuy_chon_tre_am_thanh}    id=com.bkav.video:id/bkav_audio_delay
${xp_hien_thi_tre_am_thanh}    id=com.bkav.video:id/bkav_player_overlay_info
${xp_text_tre_am_thanh}    Trễ âm thanh
${xp_text_so_ms}    0 ms
${xp_text_tre_am_thanh_50s}    50 ms
${xp_text_tre_am_thanh_-50s}    -50 ms
${xp_text_tre_am_thanh_100s}    100 ms
${xp_nut_cong_tre_am_thanh}    id=com.bkav.video:id/bkav_player_delay_plus
${xp_nut_tru_tre_am_thanh}    id=com.bkav.video:id/bkav_player_delay_minus

# tre phu de
${xp_tuy_chon_tre_phu_de}    id=com.bkav.video:id/bkav_spu_delay
${xp_hien_thi_tre_phu_de}    id=com.bkav.video:id/bkav_player_overlay_info
${xp_text_tre_phu_de}    Trễ phụ đề
${xp_text_so_ms_phu_de}    0 ms
${xp_text_tre_phu_de_50s}    50 ms
${xp_text_tre_phu_de_-50s}    -50 ms
${xp_text_tre_phu_de_100s}    100 ms
${xp_nut_cong_tre_phu_de}    id=com.bkav.video:id/bkav_player_delay_plus
${xp_nut_tru_tre_phu_de}    id=com.bkav.video:id/bkav_player_delay_minus

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
