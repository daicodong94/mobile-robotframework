*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Variables ***
#=================================================================================================
#    CAC BIEN MOI TRUONG KIEM THU
#=================================================================================================
${appium_port}         4723
${appium_url}          http://localhost:${appium_port}/wd/hub
${adb_port}            5037
${system_port}         8200
${platform_name}       Android
${platform_version}    8.1.0
${udid}                B2017G1700074
${bphone}              b2
${bphone_ota}          88
${ngon_ngu}            vn
${dinh_vi}             vi_tri_${ngon_ngu}
${so_man_launcher}     2
${do_phan_giai}        1080x1920

#=================================================================================================
#    CAC THAM SO MO UNG DUNG
#=================================================================================================
# Cac tham so mo ung dung BMS
${bms_alias}              bms
${bms_package}            com.bkav.bphone.bms
${bms_activity}           com.bkav.ui.activity.BMSActivity

# Cac tham so mo ung dung Bo suu tap
${bst_alias}              bo_suu_tap
${bst_package}            org.codeaurora.gallery
${bst_activity}           com.android.gallery3d.app.GalleryActivity

# Cac tham so mo ung dung Btalk
${btalk_alias}            btalk
${btalk_package}          bkav.android.btalk
${btalk_activity}         bkav.android.btalk.activities.BtalkActivity

# Cac tham so mo ung dung Cai dat
${cd_alias}               cai_dat
${cd_package}             com.android.settings
${cd_activity}            com.android.settings.Settings

# Cac tham so mo ung dung Camera
${cam_alias}              camera
${cam_package}            com.bkav.camera
${cam_activity}           com.android.camera.CameraLauncher

# Cac tham so mo ung dung Chim lac
${cl_alias}               chim_lac
${cl_package}             android.bkav.bchrome
${cl_activity}            com.google.android.apps.chrome.Main

# Cac tham so mo ung dung Dong ho
${dh_alias}               dong_ho
${dh_package}             bkav.android.deskclock
${dh_activity}            com.android.deskclock.DeskClock

# Cac tham so mo ung dung eDict
${eDict_alias}            eDict
${eDict_package}          bkav.android.edict
${eDict_activity}         .EdictMainActivity

# Cac tham so mo ung dung eSim
${esim_alias}             eSim
${esim_package}           bkav.android.esim
${esim_activity}          bkav.android.esim.activity.esimAcitivity

# Cac tham so mo ung dung Ghi am
${ga_alias}               ghi_am
${ga_package}             bkav.android.recorder
${ga_activity}            bkav.android.recorder.activity.SoundRecorder

# Cac tham so mo ung dung Ghi chep
${gc_alias}               ghi_chep
${gc_package}             bkav.android.bkavnote
${gc_activity}            bkav.android.bkavnote.BkavNoteActivity

# Cac tham so mo ung dung Ho tro
${ht_alias}               ho_tro
${ht_package}             bkav.android.connect
${ht_activity}            bkav.android.connect.MainActivity

# Cac tham so mo ung dung Kham pha
${kp_alias}               kham_pha
${kp_package}             bkav.android.explorer
${kp_activity}            bkav.android.explorer.ExploreActivity

# Cac tham so mo ung dung Launcher
${launcher_alias}         launcher
${launcher_package}       bkav.android.launcher3
${launcher_activity}      com.android.launcher3.bkav.BkavLauncher

# Cac tham so mo ung dung May tinh
${mt_alias}               may_tinh
${mt_package}             com.bkav.calculator2
${mt_activity}            com.android.calculator2.bkav.BkavCalculator

# Cac tham so mo ung dung Nghe dai
${nd_alias}               nghe_dai
${nd_package}             bkav.android.radioonline
${nd_activity}            bkav.android.radioonline.RadioOnline

# Cac tham so mo ung dung Nghe nhac
${nn_alias}               nghe_nhac
${nn_package}             bkav.android.music
${nn_activity}            com.android.music.ui.MainActivity

# Cac tham so mo ung dung Nhac viec
${nv_alias}               nhac_viec
${nv_package}             bkav.android.reminder
${nv_activity}            .ReminderActivity

# Cac tham so mo ung dung Quan ly file
${qlf_alias}              quan_ly_file
${qlf_package}            com.bkav.android.bexplorer
${qlf_activity}           .main.BkavExplorerHomeActivity

# Cac tham so mo ung dung Quay man hinh
${qmh_alias}              quay_man_hinh
${qmh_package}            recordscreen.bkav.com.recordscreen
${qmh_activity}           recordscreen.bkav.com.recordscreen.activity.StartActivity

# Cac tham so mo ung dung Suc khoe
${sk_alias}               suc_khoe
${sk_package}             bkav.fitness.activity
${sk_activity}            bkav.fitness.activity.BkavHealthCareActivity

# Cac tham so mo ung dung Thoi tiet
${tt_alias}               thoi_tiet
${tt_package}             bkav.android.weather
${tt_activity}            bkav.android.weather.BkavWeatherActivity

# Cac tham so mo ung dung Xem phim
${xp_alias}               xem_phim
${xp_package}             com.bkav.video
${xp_activity}            bkav.android.video.activiry.MainVideosActivity

${gtv_alias}              bkav_gtv

#=================================================================================================
#    CAC PHAN TU PHO THONG
#=================================================================================================
${nut_cho_phep_hoa}        //*[contains(@text,'CHO PHÉP')]
${nut_cho_phep}            //*[contains(@text,'Cho phép')]
${nut_tu_choi_hoa}         //*[contains(@text,'TỪ CHỐI')]
${nut_tu_choi}             //*[contains(@text,'Từ chối')]
${nut_dong_y_hoa}          //*[contains(@text,'ĐỒNG Ý')]
${nut_dong_y}              //*[contains(@text,'Đồng ý')]
${nut_dong_hoa}            //*[contains(@text,'ĐÓNG')]
${nut_huy_hoa}             //*[contains(@text,'HỦY')]
${nut_huy}                 //*[contains(@text,'Hủy')]
${nut_bat_dau}             //*[contains(@text,'Bắt đầu')]
${nut_ket_thuc}            //*[contains(@text,'Kết thúc')]
${nut_tam_dung}            //*[contains(@text,'Tạm dừng')]
${nut_dung_hoa}            //*[contains(@text,'DỪNG')]
${nut_dung}                //*[contains(@text,'Dừng')]
${nut_tiep_tuc}            //*[contains(@text,'Tiếp tục')]
${nut_bo_qua}              //*[contains(@text,'Bỏ qua')]
${nut_quay_lai}            //*[@content-desc="Di chuyển lên"]
${nut_khong_cam_on}        //*[contains(@text,'Không, cảm ơn')]

# Cac phan tu trong da nhiem
${dn_icon_ung_dung}        //*[@resource-id="bkav.android.launcher3:id/icon"]
${dn_2_ung_dung_trai}      xpath=(//*[@resource-id="bkav.android.launcher3:id/snapshot"])[1]
${dn_2_ung_dung_phai}      xpath=(//*[@resource-id="bkav.android.launcher3:id/snapshot"])[2]
${dn_3_ung_dung_trai}      xpath=(//*[@resource-id="bkav.android.launcher3:id/snapshot"])[1]
${dn_3_ung_dung_giua}      xpath=(//*[@resource-id="bkav.android.launcher3:id/snapshot"])[2]
${dn_3_ung_dung_phai}      xpath=(//*[@resource-id="bkav.android.launcher3:id/snapshot"])[3]
${dn_chia_doi_man_hinh}    //*[contains(@text,'Chia đôi màn hình')]
${nut_dong_toan_bo_app}    //*[@resource-id="bkav.android.launcher3:id/clear_all_button"]

${nut_bat}                 //*[@text="BẬT"]
${nut_bat_thuong}          //*[@text="Bật"]
${nut_tat}                 //*[contains(@text,'TẮT')]

# Cac so duoc su dung trong nhieu ung dung
${nut_sao}      xpath=(//*[@resource-id="bkav.android.btalk:id/dialpad_key_number"])[10]
${nut_0}        xpath=//*[@text="0"]
${nut_1}        xpath=//*[@text="1"]
${nut_2}        xpath=//*[@text="2"]
${nut_3}        xpath=//*[@text="3"]
${nut_4}        xpath=//*[@text="4"]
${nut_5}        xpath=//*[@text="5"]
${nut_6}        xpath=//*[@text="6"]
${nut_7}        xpath=//*[@text="7"]
${nut_8}        xpath=//*[@text="8"]
${nut_9}        xpath=//*[@text="9"]
${nut_thang}    xpath=(//*[@resource-id="bkav.android.btalk:id/dialpad_key_number"])[12]
${dt_input_ma_pin}    com.android.systemui:id/password_entry
${dt_text_hien_thi_ma_pin}    xpath=//*[contains(@text,'Nhập mã pin')]

#=================================================================================================
#    CAC PHAN TU TRONG CHUC NANG SUBSIDY
#=================================================================================================


#=================================================================================================
#    CAC PHAN TU TRONG GIAO DIEN LAUNCHER
#=================================================================================================
# Ten cac ung dung trong giao dien launcher
${app_bms}              //*[contains(@text,'BMS')]
${app_bo_suu_tap}       //*[contains(@text,'Bộ sưu tập')]
${app_cai_dat}          //*[contains(@text,'Cài đặt')]
${app_chim_lac}         //*[contains(@text,'Chim Lạc')]
${app_danh_ba}          //*[contains(@text,'Danh bạ')]
${app_dien_thoai}       //*[contains(@text,'Điện thoại')]
${app_dong_ho}          //*[contains(@text,'Đồng hồ')]
${app_eDict}            //*[contains(@text,'eDict')]
${app_esim}             //*[contains(@text,'eSIM')]
${app_ghi_am}           //*[contains(@text,'Ghi âm')]
${app_ghi_chep}         //*[contains(@text,'Ghi chép')]
${app_hinh_nen}         //*[contains(@text,'Hình nền')]
${app_ho_tro}           //*[contains(@text,'Hỗ trợ')]
${app_kham_pha}         //*[contains(@text,'Khám phá')]
${app_may_anh}          //*[contains(@text,'Máy ảnh')]
${app_may_tinh}         //*[contains(@text,'Máy tính')]
${app_nghe_dai}         //*[contains(@text,'Nghe đài')]
${app_nghe_nhac}        //*[contains(@text,'Nghe nhạc')]
${app_nhac_viec}        //*[contains(@text,'Nhắc việc')]
${app_quan_ly_file}     //*[contains(@text,'Quản lý file')]
${app_quay_man_hinh}    //*[contains(@text,'Quay m.hình')]
${app_suc_khoe}         //*[contains(@text,'Sức khỏe')]
${app_thoi_tiet}        //*[contains(@text,'Thời tiết')]
${app_tin_nhan}         //*[contains(@text,'Tin nhắn')]
${app_xem_phim}         //*[contains(@text,'Xem phim')]

# Cac phan tu launcher
${thu_muc}                            //*[@content-desc="Thư mục: "]
${thu_muc_google}                     bkav.android.launcher3:id/preview_background
${ten_thu_muc}                        bkav.android.launcher3:id/folder_name
${popup_khi_cham_giu_khoang_trang}    bkav.android.launcher3:id/deep_shortcuts_container
${popup_cai_dat}                      //*[contains(@text,'Cài đặt')]
${popup_tien_ich}                     //*[contains(@text,'Tiện ích')]
${popup_hinh_nen}                     //*[contains(@text,'Hình nền')]
${workspace}                          bkav.android.launcher3:id/search_container_workspace
${danh_sach_ung_ung}                  //*[@content-desc="Danh sách ứng dụng"]
${chon_lam_man_hinh_chinh}            //*[contains(@text,'Chọn làm màn hình chính')]
${thanh_tim_kiem}                     com.google.android.googlequicksearchbox:id/default_search_widget
${chu_google}                         com.google.android.googlequicksearchbox:id/search_widget_super_g
${tim_kiem_bang_giong_noi}            com.google.android.googlequicksearchbox:id/search_widget_voice_btn
${logo_google}                        com.google.android.googlequicksearchbox:id/google_logo
${tien_ich_thoi_tiet_preview}         bkav.android.launcher3:id/widget_preview
${tien_ich_thoi_tiet_dia_diem}        bkav.android.weather:id/name_location
${tien_ich_thoi_tiet_nhiet_do}        bkav.android.weather:id/temp_current
${xoa}                                bkav.android.launcher3:id/delete_target_text

# Cac phan tu trong popup cai dat
${hien_thi_thanh_tim_kiem}               //*[contains(@text,'Hiện Thanh tìm kiếm')]
${vuot_len_de_tim_kiem_ung_dung}         //*[contains(@text,'Vuốt lên để tìm kiếm ứng dụng')]
${hieu_ung_xuat_hien}                    //*[contains(@text,'Hiệu ứng xuất hiện')]
${an_hoac_hien_trang_thai}               //*[contains(@text,'Ẩn hoặc hiện thanh trạng thái')]
${kich_thuoc_thu_muc}                    //*[contains(@text,'Kích thước thư mục')]
${hieu_ung_1}                            //*[contains(@text,'Hiệu ứng 1')]
${hieu_ung_2}                            //*[contains(@text,'Hiệu ứng 2')]
${hieu_ung_3}                            //*[contains(@text,'Hiệu ứng ')]
${hieu_ung_mac_dinh}                     //*[@text="Mặc định"]
${on_off_an_or_hien_thanh_trang_thai}    //*[contains(@resource-id,'android:id/switch_widget')]
${kich_thuoc_4x4}                        //*[@text="4x4"]
${Btalk}                                 //*[@content-desc="Btalk"]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BMS
#=================================================================================================
# Cac phan tu trong giao dien chinh
${bms_main_logo}      	com.bkav.bphone.bms:id/logo
${bms_main_nut_diet_virus}    xpath=//*[contains(@text,'Diệt virus')]

# Cac phan tu trong giao dien Diet virus
${bms_dvr_nut_quet_virus}                  id=com.bkav.bphone.bms:id/scan_button
${nut_quet_toan_bo}                        id=com.bkav.bphone.bms:id/fullScan_Radio
${nut_quet_tung_phan}                      id=com.bkav.bphone.bms:id/scanInstalled_Radio
${bms_dvr_nut_quet}                        xpath=(//*[contains(@text,'Quét')])[4]
${bms_dvr_txt_khuyen_cao1}                 xpath=//*[contains(@text,'Khuyến cáo !')]
${bms_dvr_txt_khuyen_cao2}                 xpath=//*[contains(@text,'Bạn nên bật kết nối mạng (Wifi/3G) để quét hiệu quả hơn với Công nghệ điện toán đám mây!')]
${bms_dvr_cb_chi_bao_cao_khi_quet_xong}    xpath=//*[contains(@text,'Chỉ báo cáo khi quét xong')]
${bms_dvr_pb_thanh_tien_trinh_quet}        id=com.bkav.bphone.bms:id/progressbar_scan_Horizontal
${bms_dvr_txt_dang_quet}                   xpath=//*[contains(@text,'Đang quét')]
${bms_dvr_txt_tong_so_file_da_quet}        xpath=//*[contains(@text,'Tổng số file đã quét:')]
${bms_dvr_txt_so_virus_phat_hien}          xpath=//*[contains(@text,'Số virus phát hiện:')]

${nut_dat_lich_quet}                                    id=com.bkav.bphone.bms:id/scan_schedule_button
${nut_diet_virrus_tuy_chon}                             id=com.bkav.bphone.bms:id/scan_technology_button
${nut_diet_virus_danh_sach_bo_qua_quang_cao}            id=com.bkav.bphone.bms:id/view_ignore_apps_list_btn
${nut_back_danh_sach_bo_qua_quang_cao}                  id=com.bkav.bphone.bms:id/ib_banner_bms_back_back
${nut_diet_virus_nhat_ky}                               id=com.bkav.bphone.bms:id/history_virus_button

# Cac phan tu trong chan tin nhan va cuoc goi rac
${nut_chan_tin_nhan_rac}                                id=com.bkav.bphone.bms:id/tv_layout_spam_title
${nut_tin_nhan_danh_sach_den}                           id=com.bkav.bphone.bms:id/ll_fragment_spam_black_list_select
${nut_tin_nhan_danh_sach_trang}                         id=com.bkav.bphone.bms:id/ll_fragment_spam_white_list_select
${nut_tin_nhan_tranh_lam_phien}                         id=com.bkav.bphone.bms:id/ll_fragment_spam_private_select
${nut_tin_nhan_chan_thao_noi_dung}                      id=com.bkav.bphone.bms:id/ll_fragment_spam_block_content_select
${nut_tin_nhan_nhat_ki_chan}                            id=com.bkav.bphone.bms:id/ll_fragment_spam_diary_select
${nut_tin_nhan_huong_dan_su_dung}                       id=com.bkav.bphone.bms:id/ib_layout_spam_help

# Cac phan tu trong man chan cuoc goi rac
${nut_chan_cuoc_goi_rac}                                id=com.bkav.bphone.bms:id/tv_layout_spam_call_title
${nut_cuoc_goi_rac_danh_sach_den}                       id=com.bkav.bphone.bms:id/ll_fragment_spam_call_black_list_select
${nut_cuoc_goi_rac_danh_sach_trang}                     id=com.bkav.bphone.bms:id/ll_fragment_spam_call_white_list_select
${nut_cuoc_goi_rac_tranh_lam_phien}                     id=com.bkav.bphone.bms:id/ll_fragment_spam_call_private_select
${nut_cuoc_goi_rac_nhat_ky_chan}                        id=com.bkav.bphone.bms:id/ll_fragment_spam_call_diary_select
${nut_cuoc_goi_rac_huong_dan_su_dung}                   id=com.bkav.bphone.bms:id/ib_layout_spam_call_help

# Cac phan tu trong giao dien bao ve giao dich
${nut_bao_ve_giao_dich_ngan_hang}                       id=com.bkav.bphone.bms:id/tv_layout_privacy_title
${nut_bao_ve_giao_dich_ngan_hang_quet}                  id=com.bkav.bphone.bms:id/b_fragment_privacy_scan

#sau khi quet xong o man baop ve tai khoan ngan hang hien thi
${nut_truy_cap_danh_ba}                                 id=com.bkav.bphone.bms:id/contactsnumbertit

# Cac phan tu trong man chong trom
${nut_chong_trom}                                       id=com.bkav.bphone.bms:id/tv_layout_anti_theft_title
${nut_so_dien_thoai_du_phong}                           id=com.bkav.bphone.bms:id/iv_fragment_anti_theft_change_phone_second
${nut_back_sdt_du_phong}                                id=com.bkav.bphone.bms:id/ib_banner_bms_notice_back
${nut_chon_noi_dung_man_hinh_khoa_chong_trom}           id=com.bkav.bphone.bms:id/iv_fragment_anti_theft_change_content_lock_screen
${nut_close_chon_noi_dung_man_hinh_khoa_chong_trom}     id=com.bkav.bphone.bms:id/b_dialog_info_close

# Cac phan tu man khac
${nut_don_rac_toi_uu}                                   id=com.bkav.bphone.bms:id/tv_layout_clean_title
${nut_chan_lam_phien}                                   id=com.bkav.bphone.bms:id/tv_layout_disturb_block_title
${nut_an_noi_dung_rieng_tu}                             id=com.bkav.bphone.bms:id/tv_layout_private_content_title
${nut_sao_luu_du_lieu}                                  id=com.bkav.bphone.bms:id/tv_layout_backup_title
${nut_ban_quyen}                                        id=com.bkav.bphone.bms:id/tv_layout_license_title

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BO SUU TAP
#=================================================================================================
${btn_dong_thoi_gian}        //*[@content-desc="Ảnh"]
# ${btn_dong_thoi_gian}        //*[@resource-id="org.codeaurora.gallery:id/action_camera"]
# ${bst_xem_anh}      //*[@content-desc="Ảnh"]
${bst_xem_anh}      //*[@text="Dòng thời gian"]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BTALK - CHUC NANG DIEN THOAI
#=================================================================================================
${btalk_dien_thoai}             xpath=(//*[contains(@resource-id,'bkav.android.btalk:id/tabs_icon')])[1]
${btalk_tin_nhan}               xpath=(//*[contains(@resource-id,'bkav.android.btalk:id/tabs_icon')])[2]
${btalk_gan_day}                xpath=//*[@text="Gần đây"]
${btalk_danh_ba}                xpath=(//*[contains(@resource-id,'bkav.android.btalk:id/tabs_icon')])[4]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BTALK - CHUC NANG TIN NHAN
#=================================================================================================
${tn_btn_tin_nhan}                   //*[@text="Tin nhắn"]
${tn_txt_huy}                        HỦY
${tn_txt_dong y}                     ĐỒNG Ý
${tn_txt_dat_lam_mac_dinh}           ĐẶT LÀM MẶC ĐỊNH
${tn_txt_dat_sms_mac_dinh}           Bạn muốn đặt Btalk làm ứng dụng SMS mặc định?
${tn_txt_am_thanh_mac_dinh}          Âm thanh thông báo mặc định
${tn_txt_gui_mms}                    Gửi một MMS cho tất cả người nhận
${tn_txt_khong_co_thong_bao}         Không có thông báo nào
# ${tn_pupup}               bkav.android.btalk:id/message_setting
${tn_tim_kiem}               bkav.android.btalk:id/img_ic_search_expand
${tn_3_cham}                 bkav.android.btalk:id/imb_over_flow
${tn_3_da_luu_tru}           //*[@text="Đã lưu trữ"]
${tn_3_danh_dau_da_doc}      //*[@text="Đánh dấu tất cả đã đọc"]
${tn_3_cai_dat}              //*[@text="Cài đặt"]
${dt_3_cham}                 bkav.android.btalk:id/dialpad_overflow

# Giao dien cai dat chung
${tn_cd_back}              //*[@content-desc="Điều hướng lên trên"]
${tn_cd_sms_mac_dinh}      //*[@text="Ứng dụng SMS mặc định"]
${tn_cd_btalk}             //*[@text="Btalk"]
${tn_cd_am_thanh_tn}       //*[@text="Âm thanh tin nhắn đi"]
${tn_cd_thong_bao_tn}      //*[@text="Cài đặt thông báo tin nhắn"]
${tn_cd_bao_cao_sms}       //*[@text="Báo cáo gửi SMS"]
${tn_cd_bao_cao_yc}        //*[@text="Yêu cầu báo cáo gửi cho mỗi SMS bạn gửi"]
${tn_cd_hien_avata}        //*[@text="Hiện avatar ở mỗi tin nhắn"]
${tn_cd_thong_tin_ct}      //*[@text="Hiện thông tin chi tiết ở mỗi tin nhắn"]
${tn_cd_thong_tin_sim}     //*[@text="Hiện thông tin sim của cuộc hội thoại"]
${tn_cd_nang cao}          //*[@text="Nâng cao"]
${tn_cd_nut_am_thanh}      xpath=(//*[@resource-id="android:id/switch_widget"])[1]
${tn_cd_nut_bao_cao}       xpath=(//*[@resource-id="android:id/switch_widget"])[2]
${tn_cd_nut_avata}         xpath=(//*[@resource-id="android:id/switch_widget"])[3]
${tn_cd_nut_thong_tin}     xpath=(//*[@resource-id="android:id/switch_widget"])[4]
${tn_cd_nut_thong_tin_sim}         xpath=(//*[@resource-id="android:id/switch_widget"])[5]
${tn_logo}                 bkav.android.btalk:id/img_background_expand_layout
${tn_cd_sms_md_messenger}                 //*[@text="Messenger"]
${tn_cd_sms_md_tim_kiem}                  //*[@content-desc="Tìm kiếm trong các mục cài đặt"]
${tn_cd_sms_btalk_mac_dinh}               //*[@text="Để gửi tin nhắn hãy đặt Btalk là ứng dụng nhắn tin SMS mặc định"]
${tn_cd_sms_thay_doi}                     //*[@text="Thay đổi"]

# Cac phan tu cai dat thong bao tin nhan
${tn_cd_tb_loai_tb}                           //*[@text="Loại thông báo"]
${tn_cd_tb_canh bao}                          //*[@text="Cảnh báo"]
${tn_cd_tb_im_lang}                           //*[@text="Im lặng"]
${tn_cd_tb_nang_cao}                          //*[@text="Nâng cao"]
${tn_cd_tb_hien_thi_tren_mh}                  //*[@text="Hiển thị trên màn hình"]
${tn_cd_tb_am_bao}                            //*[@text="Âm báo"]
${tn_cd_tb_rung}                              //*[@text="Rung"]
${tn_cd_tb_man_hinh khoa}                     //*[@text="Màn hình khóa"]
${tn_cd_tb_nhap_nhay_den}                     //*[@text="Nhấp nháy đèn"]
${tn_cd_tb_ht_dc_tn}                          //*[@text="Hiển thị dấu chấm thông báo"]
${tn_cd_tb_ghi_de}                            //*[@text="Ghi đè Không làm phiền"]
${tn_cd_tb_bo_sung_ung_dung}                  //*[@text="Cài đặt bổ sung trong ứng dụng"]
${tn_cd_tb_thu_nho}                           //*[@text="Thu nhỏ"]
${tn_cd_tb_mhk_text1}                         //*[@text="Không hiển thị thông báo nào"]

# Hien avata o moi tin nhan
${tn_cd_avata_icon}                 bkav.android.btalk:id/conversation_photo_icon
${tn_lien_he}                      //*[@resource-id="bkav.android.btalk:id/swipeableContent"]
${tn_chi_tiet_tn}                  //*[@resource-id="bkav.android.btalk:id/message_status"]

# Cac phan tu tin nhan cai dat nang cao
${tn_cd_nc_nhan_tin_theo_nhom}               //*[@text="Nhắn tin theo nhóm"]
${tn_cd_nc_so_dien_thoai}                    //*[@text="Số điện thoại của bạn"]
${tn_cd_nc_tu_dong_truy_xuat}                //*[@text="Tự động truy xuất"]
${tn_cd_nc_tu_dong_chuyen_vung}              //*[@text="Tự động truy xuất khi chuyển vùng"]
${tn_cd_nc_canh_bao_khong_day}               //*[@text="Cảnh báo không dây"]
${tn_cd_nc_gui tung_tn_sms}                  bkav.android.btalk:id/disable_group_mms_button
${tn_cd_nc_gui_mot_mms}                      bkav.android.btalk:id/enable_group_mms_button
${tn_cd_nc_text_sdt}                         android:id/edit

# Giao dien danh sach cuoc tro chuyen
${tn_go_noi_dung_tin_nhan}                   id=bkav.android.btalk:id/compose_message_text
${tn_them_tin_nhan}                          xpath=//android.widget.ImageView[@content-desc="Bắt đầu cuộc trò chuyện mới"]
${tn_nut_gui}                                xpath=//android.widget.ImageButton[@content-desc="Gửi tin nhắn"]
${tn_themmoi_nhap_sdt_gui}                           id=bkav.android.btalk:id/recipient_text_view

# Data luu tai may
${dt_sdt_sim_test}             xpath=//*[@text="0823390409"]

# Cac phan tu sau khi bam nut them tuy chon
${dt}                                                      xpath=//*[@text="Điện thoại"]
${cd_nut_dien_thoai}                                       id=bkav.android.btalk:id/phone_setting
${dt_cd_nut_tuy_chon_hien_thi}                             xpath=//*[@text="Tùy chọn hiển thị"]
${dt_cd_nut_am_thanh_va_rung}                              xpath=//*[@text="Âm thanh và rung"]
${dt_cd_nut_tra_loi_nhanh}                                 xpath=//*[@text="Trả lời nhanh"]
${dt_cd_tai_khoan_goi}                                     xpath=//*[@text="Tài khoản gọi"]
${dt_cd_nut_tro_nang}                                      xpath=//*[@text="Trợ năng"]
${dt_cd_nut_cd_mo_ung_dung}                                xpath=//*[@text="Cài đặt mở ứng dụng"]
${dt_cd_nut_cd_quay_so_nhanh}                             xpath=//*[@text="Cài đặt quay số nhanh"]
${dt_cd_nut_chan_cuoc_goi}                                 xpath=//*[@text="Chặn cuộc gọi"]
${dt_cd_back}                                              xpath=//*[@content-desc="Điều hướng lên trên"]

# Cac phan tu sau khi chon am thanh rung
${dt_cd_checkbox_tuy_chon_hien_thi}                        id=android:id/checkbox
${dt_cd_nut_nhac_chuong_dien_thoai}                        xpath=//*[@text="Nhạc chuông điện thoại"]
${dt_cd_nut_nhac chuong_khong}                             xpath=//*[@text="Không"]
${dt_cd_nut_dong_y}                                        xpath=//*[@text="ĐỒNG Ý"]
${dt_cd_nut_huy}                                           xpath=//*[@text="HỦY"]
${dt_cd_nut_nut_nhac_chuong_hop_nhac}                      xpath=//*[@text="Hộp nhạc"]
${dt_cd_nut_chi_mot_lan}                                   xpath=//*[@text="CHỈ MỘT LẦN"]
${dt_cd_nut_luon_chon}                                     xpath=//*[@text="LUÔN CHỌN"]
${dt_cd_nut_quan_li_file}                                  xpath=//*[@text="Quản lý file"]
${dt_cd_nut_bo_nho_phuong_tien}                            xpath=//*[@text="Bộ nhớ phương tiện"]
${dt_cd_nut_nhac_chuong_sim1}                              xpath=//*[@text="Nhạc chuông Sim 1"]
${dt_cd_ten_nhac_chuong_sim1}                              xpath=//android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.TextView[2]
${dt_cd_ten_nhac_chuong_sim2}                              xpath=//android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.TextView[2]
${dt_cd_nut_nhac_chuong_sim2}                              xpath=//*[@text="Nhạc chuông Sim 2"]
${dt_cd_checkbox_rung_khi_co_cuoc_goi}                     xpath=(//*[@resource-id="android:id/checkbox"])[1]
${dt_cd_checkbox_am_ban_phim_so}                           xpath=(//*[@resource-id="android:id/checkbox"])[2]

# Cac phan tu sau khi bam tra loi nhanh
${dt_cd_nut_checkbox_tra_loi_nhanh}                        xpath=(//*[@resource-id="android:id/checkbox"])

# Cac phan tu sau khi bam tai khoan goi
${dt_cd_checkbox_tu_dong_ghi_am_cuoc_goi}                  xpath=(//*[@resource-id="android:id/checkbox"])[1]
${dt_cd_checkbox_Hinh_nen_cuoc_goi}                        xpath=(//*[@resource-id="android:id/checkbox"])[2]
${dt_cd_Hien_thi_thong_tin_nha_mang_trong_cuoc_goi}        xpath=(//*[@resource-id="android:id/checkbox"])[3]
${dt_cd_nut_hen_gio_cuoc_goi}                              xpath=//*[@text="Hẹn giờ cuộc gọi"]
${dt_cd_nut_dat_thoi_gian}                                 xpath=//*[@text="Đặt thời gian"]
${dt_cd_checkbox_tat_cuoc_goi_tu_dong}                     xpath=(//*[@resource-id="android:id/checkbox"])[1]
${dt_cd_checkbox_rung_thong_bao_truoc_30s}                 xpath=(//*[@resource-id="android:id/checkbox"])[2]
${dt_cd_checkbox_ap_dung_cho_ca_cuoc_goi_den}              xpath=(//*[@resource-id="android:id/checkbox"])[3]
${dt_cd_Title_popup_dat_thoi_gian}                         id=android:id/alertTitle
${dt_cd_dat_thoi_gian_input_gio}                           xpath=//android.widget.NumberPicker[1]/android.widget.EditText
${dt_cd_dat_thoi_gian_input_phut}                          xpath=//android.widget.NumberPicker[2]/android.widget.EditText
${dt_cd_nut_dat_thoi_gian_trong_popup_dat_thoi_gian}       xpath=//*[@text="ĐẶT THỜI GIAN"]
${dt_cd_nut_thoi_gian_tu_Dong_xoa_ghi_am_cuoc_goi}         xpath=//*[@text="Thời gian tự động xóa ghi âm cuộc gọi"]
${dt_cd_nut_tat_thoi_gian_tu_dong_xoa_ghi_am_cuoc_goi}     xpath=//*[@text="Tắt"]
${dt_cd_nut_thoi_gian_tu_dong_xoa_ghi_am_cuoc_goi_30ngay}        xpath=//*[@text="30 ngày"]

# May lap 2 sim. Cac phan tu thuc hien cuoc goi bang
${dt_cd_nut_thuc_hien_cuoc_goi_bang}                             xpath=//*[@text="Thực hiện cuộc gọi bằng"]
${dt_cd_checkbox_thuc_hien_cuoc_goi_bang_sim1}                  xpath=(//*[@resource-id="android:id/text1"])[1]
${dt_cd_checkbox_thuc_hien_cuoc_goi_bang_sim2}                  xpath=(//*[@resource-id="android:id/text1"])[2]
${dt_cd_checkbox_thuc_hien_cuoc_goi_bang_hoi_truoc}             xpath=(//*[@resource-id="android:id/text1"])[3]

# May lap 2sim cac phan tu popup goi bang khi cham nut goi
${dt_popup_goi_bang_sim1}                                        xpath=(//*[@resource-id="bkav.android.btalk:id/icon_sim"])[1]
${dt_popup_goi_bang_sim2}                                        xpath=(//*[@resource-id="bkav.android.btalk:id/icon_sim"])[2]
${dt_popup_goi_bang_checkbox_luon_sd_sim_nay_de_goi}        id=bkav.android.btalk:id/chk_always_use

# Cac phan tu ban phim dien thoai
${dt_nut_quay_so}                   xpath=//*[@content-desc="quay số"]
${dt_man_hinh_hien_thi_nhap_sdt}    id=bkav.android.btalk:id/digits
${dt_icon_tin_nhan_khi_nhap_so}     xpath=//android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.ImageView
${dt_icon_xoa_khi_nhap_so}          xpath=//android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.ImageView

# Cac phan tu Cai dat mo ung dung
${dt_cd_checkbox_luu_vi_tri_tab_khi_thoat_ung_dung}    xpath=(//*[@resource-id="android:id/checkbox"])[1]

# Cac phan tu cai dat quay so nhanh
${dt_cd_nut_quay_so_nhanh_thu_thoai}                       xpath=//*[@text="Thư thoại"]
${dt_cd_nut_dich_vu}                                       xpath=//*[@text="Dịch vụ"]
${dt_cd_nut_thiet_lap}                                     xpath=//*[@text="Thiết lập"]
${dt_cd_nut_thong bao}                                     xpath=//*[@text="Thông báo"]
${dt_cd_checkbox_dich_vu_nha_mang_cua_ban}                 id=android:id/text1
${dt_cd_nut_so_thu_thoai}                                  xpath=//*[@text="Số thư thoại"]
${dt_cd_nhap_so_thu_thoai}                                 id=android:id/edit
${dt_cd_title_popup_thu_thoai}                             id=android:id/alertTitle
${dt_cd_nut_ok}                                            xpath=//*[@text="OK"]
${dt_cd_nut_huy_bo}                                        xpath=//*[@text="HỦY BỎ"]
${dt_cd_cai_dat_quay_so_nhanh_nhap_sdt}                    id=bkav.android.btalk:id/edit_container
${dt_cd_quay_so_nhanh_text_phim_2}                         xpath=//android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.TextView
${dt_cd_quay_so_nhanh_text_phim_3}                         xpath=//android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView
${dt_cd_quay_so_nhanh_icon_danh_ba}                        id=bkav.android.btalk:id/select_contact
${dt_cd_lien_he1}     xpath=//android.widget.ListView/android.view.ViewGroup[1]/android.widget.TextView[4]
${dt_cd_sdt}         id=bkav.android.btalk:id/number
${dt_cd_thay_doi}     xpath=//*[@text="Thay đổi"]
${dt_cd_xoa_bo}        xpath=//*[@text="Xóa bỏ"]
${dt_popup_phim_chua_dc_thiet_lap}     xpath=//*[@text="Phím chưa được thiết lập"]

# Cac phan tu giao dien dang thuc hien cuoc goi
${dt_man_hinh_hien_thi_so_da_luu_khi_goi}        id=com.android.dialer:id/number_contact_bkav
${dt_tab_dien_thoai_loa}        xpath=//*[@text="Loa"]
${dt_tab_dien_thoai_nut_giu_cuoc_goi}            xpath=//*[@text="Giữ cuộc gọi"]
${dt_danh_sach_sim}                id=bkav.android.btalk:id/list_sim
${dt_ket_thuc_cuoc_goi}      id=com.android.dialer:id/endButtonBkav_incall
${dt_man_hinh_hien_thi_ten_lien_he_da_luu_khi_goi}    id=com.android.dialer:id/name_contact_bkav

&{tn}                    vi_tri_vn=//*[@text="Tin nhắn"]
...                      vi_tri_en=//*[@text="Messages"]
...                      vn=Tin nhắn
...                      en=Messages

&{tuy_chon_khac}         //*[@content-desc="Tùy chọn khác"]
...                      //*[@content-desc="More options"]
...                      vn=Tùy chọn khác
...                      en=More options
# Cac phan tu tuy chon khac
&{tck_goi_dien}          vi_tri_vn=//*[@text="Gọi điện"]
...                      vi_tri_en=//*[@text="Make a call"]
...                      vn=Gọi điện
...                      en=Make a call

&{tck_them_lh}           vi_tri_vn=//*[@text="Thêm liên hệ"]
...                      vi_tri_en=//*[@text="Add contact"]
...                      vn=Gọi điện
...                      en=Add contact

&{tck_tim_kiem_tn}       vi_tri_vn=//*[@text="Tìm kiếm tin nhắn"]
...                      vi_tri_en=//*[@text="Search sms"]
...                      vn=Tìm kiếm tin nhắn
...                      en=Search sms

&{tck_dimh_kem_lh}       vi_tri_vn=//*[@text="Đính kèm liên hệ"]
...                      vi_tri_en=//*[@text="Attach contact"]
...                      vn=Đính kèm liên hệ
...                      en=Attach contact

&{tck_them_chu_de}       vi_tri_vn=//*[@text="Thêm chủ đề"]
...                      vi_tri_en=//*[@text="Add subject"]
...                      vn=Thêm chủ đề
...                      en=Add subject

&{tck_luu_tru}           vi_tri_vn=//*[@text="Lưu trữ"]
...                      vi_tri_en=//*[@text="Archive"]
...                      vn=Lưu trữ
...                      en=Archive

&{tck_cai_dat}           vi_tri_vn=//*[@text="Cài đặt"]
...                      vi_tri_en=//*[@text="Settings"]
...                      vn=Cài đặt
...                      en=Settings

&{tck_chinh_sua_phn}     vi_tri_vn=//*[@text="Chỉnh sửa phản hồi nhanh"]
...                      vi_tri_en=//*[@text=Edit quick responses"]
...                      vn=Chỉnh sửa phản hồi nhanh
...                      en=Edit quick responses

&{tck_xoa}               vi_tri_vn=//*[@text="Xóa"]
...                      vi_tri_en=//*[@text="Delete"]
...                      vn=Xóa
...                      en=Delete

&{tck_loc_tap_ga}        vi_tri_vn=//*[@text="Lọc tệp ghi âm"]
...                      vi_tri_en=//*[@text="Filter recording file"]
...                      vn=Lọc tệp ghi âm
...                      en=Filter recording file

&{tck_loc_tn_sms}        vi_tri_vn=//*[@text="Lọc tin nhắn SMS"]
...                      vi_tri_en=//*[@text="Filter SMS"]
...                      vn=Lọc tin nhắn SMS
...                      en=Filter SMS

&{khong_tim_thay}        vi_tri_vn=//*[@text="Không tìm thấy tin nhắn!"]
...                      vi_tri_en=//*[@text="Not found messages!"]
...                      vn=Lọc tin nhắn SMS
...                      en=Not found messages!
&{back_exit_lh}          vi_tri_vn=//*[@content-desc="Điều hướng lên trên"]
...                      vi_tri_en=//*[@content-desc="Navigate up"]

&{da_luu_tru}            vi_tri_vn=//*[@text="Đã lưu trữ"]
...                      vi_tri_en=//*[@text="Archived"]

&{all_da_doc}            vi_tri_vn=//*[@text="Đánh dấu tất cả đã đọc"]
...                      vi_tri_en=//*[@text="Mark all as read"]

&{cai_dat}               vi_tri_vn=//*[@text="Cài đặt"]
...                      vi_tri_en=//*[@text="Settings"]

&{huy_luu_tru}           vi_tri_vn=//*[@text="Hủy lưu trữ"]
...                      vi_tri_en=//*[@text="Unarchive"]

${dau_ba_cham}           bkav.android.btalk:id/imb_over_flow

# Cac phan tu giao dien tin nhan
${logo_tn}               bkav.android.btalk:id/img_background_expand_layout
${lien_he_tn}            bkav.android.btalk:id/conversation_title
${noi_dung_ctc}          bkav.android.btalk:id/message_text_and_info
${thu}                   bkav.android.btalk:id/day_label
${thu_gio}               bkav.android.btalk:id/message_status
${them_tep}              bkav.android.btalk:id/attach_media_button
${go_noi_dung}           bkav.android.btalk:id/compose_message_text
${icon}                  bkav.android.btalk:id/img_add_emoticon
${gui_tin_nhan}          bkav.android.btalk:id/send_message_button
${incall}                com.android.dialer:id/endButtonBkav_incall
${avata_lh}              xpath=(//*[@resource-id="bkav.android.btalk:id/conversation_icon"])[1]
${name_lh}               xpath=(//*[@resource-id="bkav.android.btalk:id/conversation_name"])[1]
${name_lh_1}             xpath=(//*[@resource-id="bkav.android.btalk:id/conversation_name"])[2]
# Cac phan tu them lien he
${them_lh}               android:id/button1
${xoa_truy_van}          bkav.android.btalk:id/search_close_btn
${xoa_truy_van_lh}       android:id/search_close_btn
${tim_kiem_tn}           bkav.android.btalk:id/search_src_text
${tim_kiem_lh}           android:id/action_bar
${tim_kiem_lup}          bkav.android.btalk:id/menu_search
${khong_thay_Lh}         bkav.android.btalk:id/empty_list_title
${tim_lien_he}           android:id/search_src_text
# Cac phan tu them chu de
${xoa_chu_de}            bkav.android.btalk:id/delete_subject_button
${txt_chu_de}            bkav.android.btalk:id/compose_subject_text
${MMS}                   bkav.android.btalk:id/mms_indicator
${text1}                 Mylove
${text2}                 5555555555222222222211111111113333333333888
${text3}                 5555555555222222222211111111113333333333
${tn_lu_tru}             bkav.android.btalk:id/swipeableContent
# elemet cai dat
${nut_bat_tat}           bkav.android.btalk:id/switch_button

&{cd_chung}              vi_tri_vn=//*[@text="Chung"]
...                      vi_tri_en=//*[@text="General"]

&{cd_thong_bao}          vi_tri_vn=//*[@text="Thông báo"]
...                      vi_tri_en=//*[@text="Notifications"]

&{cd_chan}               vi_tri_vn=//*[@text="Chặn"]
...                      vi_tri_en=//*[@text="Block "]

&{cd_am_thanh}           vi_tri_vn=//*[@text="Sound"]
...                      vi_tri_en=//*[@text="Settings"]

&{cd_moi_nguoi}          vi_tri_vn=//*[@text="Mọi người trong cuộc hội thoại này"]
...                      vi_tri_en=//*[@text="People in this conversation"]

${avata_lien_he}         bkav.android.btalk:id/contact_icon
${ten_lien_he}           bkav.android.btalk:id/title_gradient

# Cac phan tu phan hoi nhanh
${add_phan_hoi}          bkav.android.btalk:id/action_add
${Noi_dung_phn}          bkav.android.btalk:id/response
&{Nut_back_phn}          vi_tri_vn=//*[@content-desc="Điều hướng lên trên"]
...                      vi_tri_en=//*[@content-desc="Navigate up"]
&{da_xoa_tb}             vi_tri_vn=Đã xóa cuộc hội thoại
...                      vi_tri_en=Conversation deleted

# Cac phan tu tep ghi am
${phat_tep_ga}           bkav.android.btalk:id/play_button
${noi_dung_tn}           bkav.android.btalk:id/message_text

# Cac phan tu thanh cong cu
${chia_se}               bkav.android.btalk:id/share_message_menu
${chuyen_tiep}           bkav.android.btalk:id/forward_message_menu
${sao_chep}              bkav.android.btalk:id/copy_text
${chi_tiet}              bkav.android.btalk:id/details_menu
${text_ko_thay_tn}       bkav.android.btalk:id/empty_text_hint
${them_moi}              bkav.android.btalk:id/start_new_conversation_button
${popup_chi_tiet}        android:id/alertTitle
&{xoa_thanh_cc}          vi_tri_vn=//*[@text="Xóa"]
...                      vi_tri_en=//*[@text="delete"]

${tim_kiem_ct}           bkav.android.btalk:id/img_ic_search_expand
${text_tin_nhan}               bkav.android.btalk:id/title_tab_message

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BTALK - CHUC NANG NHAT KI CUOC GOI
#=================================================================================================
${text_title_gan_day}    id=bkav.android.btalk:id/title_tab_calllog
${dt_tab_gan_day_lien_he1}    xpath=(//*[@resource-id="bkav.android.btalk:id/name"])[1]
${dt_gd_sdt1}                 xpath=(//*[@resource-id="bkav.android.btalk:id/number"])[1]
${dt_tab_gan_day_nut_goi_nho}        id=bkav.android.btalk:id/btnMissExpand
${dt_gd_icon_tin_nhan1}              xpath=(//*[@resource-id="bkav.android.btalk:id/primary_action_button"])[1]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BTALK - CHUC NANG DANH BA
#=================================================================================================
${btalk_db_tim_lien_he}     id=bkav.android.btalk:id/search_view
${db_add_danh_ba}           bkav.android.btalk:id/search_add_contact
${db_nut_ngung_tim_kiem}    id=bkav.android.btalk:id/search_back_button
${dt_db_ten_lien_he_1}    xpath=(//*[@resource-id="bkav.android.btalk:id/cliv_name_textview"])[1]
${dt_db_nut_goi_lien_he_1}        xpath=//android.widget.LinearLayout[1]/android.view.ViewGroup/android.widget.ImageButton[2]
${dt_db_nut_them_lien_he}                     id=bkav.android.btalk:id/action_add_contact
${dt_db_nut_lien_he_yeu_thich}                id=bkav.android.btalk:id/action_filter_favorites
${dt_db_tab_tuy_chon_cai_dat}              id=bkav.android.btalk:id/action_more_tool
${dt_dbnut_them_lien_he_khi_tim_kiem}        id=bkav.android.btalk:id/search_add_contact
${dt_db_nut_tim_kiem}                         id=bkav.android.btalk:id/floating_action_button
${dt_db_nut_ngung_tim_kiem}                   id=bkav.android.btalk:id/search_back_button
${dt_db_nut_thoat_them_lien_he}               id=bkav.android.btalk:id/action_home_up

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG BTALK - CHUC NANG GTV
#=================================================================================================

${tn_icon_soan_tin_nhan_moi}    xpath=//*[@resource-id="bkav.android.btalk:id/img_background_expand_layout"]

${gtv_icon_them_moi_ghi_chep}    bkav.android.bkavnote:id/fab
${gtv_icon_checklist_ghi_chep}    xpath=(//*[@resource-id="bkav.android.bkavnote:id/mini_fab"])[3]
${gtv_text_vew}    	            bkav.android.btalk:id/recipient_text_view
${gtv_message_text}                 bkav.android.btalk:id/compose_message_text

${gtv_chu_thuong_x}        646
${gtv_chu_thuong_y}        1761
${gtv_chu_thuong1_x}        424
${gtv_chu_thuong1_y}        1765
${gtv_chu_thuong2_x}        325
${gtv_chu_thuong2_y}        1613

${gtv_chu_hoa_x}        108
${gtv_chu_hoa_y}        1622

${gtv_ky_tu_so_x}        74
${gtv_ky_tu_so_y}        1899

${gtv_alt_x}        84
${gtv_alt_y}        1761

${gtv_icon_emoji_x}    99
${gtv_icon_emoji__y}    2066

${gtv_space_x}    533
${gtv_space_y}    1904

${gtv_nut_xoa_x}    996
${gtv_nut_xoa_y}    1756

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG CAI DAT
#=================================================================================================
${cd_tim_kiem}    xpath=//*[contains(@content-desc,'Tìm kiếm trong các mục cài đặt')]

# Cac phan tu trong man hinh chinh
${cd_nut_mang_va_internet}     xpath=//*[contains(@text,'Mạng và Internet')]
${cd_nut_thiet_bi_da_ket_noi}     xpath=//*[contains(@text, 'Thiết bị đã kết nối')]
${cd_nut_ung_dung_va_thong_bao}   xpath=//*[contains(@text, 'Ứng dụng và thông báo')]
${cd_nut_pin}                     xpath=//*[contains(@text, 'Pin')]
${cd_nut_hinh_nen}                xpath=//*[contains(@text, 'Hình nền')][1]
${cd_nut_bang_dieu_khien}         xpath=//*[contains(@text, 'Bảng điều khiển')]
${cd_nut_hien_thi}                xpath=//*[contains(@text, 'Hiển thị')]
${cd_nut_dieu_huong}              xpath=//*[contains(@text, 'Điều hướng')]
${cd_nut_am_thanh}                xpath=//*[contains(@text, 'Âm thanh')]
${cd_nut_he_thong}                xpath=//*[contains(@text, 'Hệ thống')]

# Cac phan tu khac
${cd_nut_nang_cao}                xpath=//*[@text="Nâng cao"]

# Cac phan tu ung dung va thong bao
${cd_xem_tat_ca_ung_dung}         id=com.android.settings:id/header_details
${cd_nut_tuy_chon_khac}           xpath=//android.widget.ImageButton[@content-desc="Tùy chọn khác"]
${cd_hien_thi_he_thong}           xpath=//*[@text="Hiển thị hệ thống"]
${cd_nut_tim_kiem_thong_tin_ung_dung}        xpath=//android.widget.TextView[@content-desc="Tìm kiếm"]
${cd_thanh_tim_kiem}                 id=android:id/search_src_text
${cd_ung_dung_bo_nho_phuong_tien}       xpath=//*[@text="Bộ nhớ phương tiện"]
${cd_mo_theo_mac_dinh}                  xpath=//*[contains(@text,'Mở theo mặc định')]
${cd_nut_xoa_mac_dinh}                    xpath=//*[@text="XÓA MẶC ĐỊNH"]
${cd_ung_dung_quan_ly_file}            xpath=//*[@text="Quản lý file"]

# cac phan tu trong he thong
${cd_nut_ngon_ngu_va_nhap_lieu}         xpath=//*[@text="Ngôn ngữ và nhập liệu"]
${cd_nut_ngon_ngu}                      xpath=//*[@text="Ngôn ngữ"]
${cd_nut_tieng_viet}                    xpath=//android.widget.TextView[@content-desc="Tiếng Việt (Việt Nam)"]
${cd_nut_english}                       xpath=//android.widget.TextView[@content-desc="Tiếng Anh (Hoa Kỳ)"]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG CAMERA
#=================================================================================================
${cam_quay_phim}        //*[@text="QUAY PHIM"]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG CHIM LAC
#=================================================================================================
${cl_url_bar}    android.bkav.bchrome:id/url_bar

# Cac phan tu trong popup Bat che do thu gon
${nut_bat_che_do_thu_gon}    //*[contains(@text,'Bật Chế độ thu gọn')]

# Cac phan tu trong popup trang web muon su dung thong tin vi tri thiet bi
${nut_chan}    //*[contains(@text,'Chặn')]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG DONG HO
#=================================================================================================
# Cac phan tu tab menu
${dh_tab_bao_thuc}         xpath=//*[@content-desc="Báo thức"]
${dh_tab_dong_ho}          xpath=//*[@content-desc="Đồng hồ"]
${dh_tab_bo_hen_gio}       xpath=//*[@content-desc="Bộ hẹn giờ"]
${dh_tab_bam_gio}          xpath=//*[@content-desc="Đồng hồ bấm giờ"]
${dh_tab_tuy_chon_khac}    xpath=//*[@content-desc="Tùy chọn khác"]

${dh_bt_dem_phan_tu_hien_thi}      //*[@resource-id='bkav.android.deskclock:id/digital_clock']
${dh_bt_thong_bao}                 xpath=//*[contains(@text,'Báo thức được đặt đổ chuông sau')]
${dh_bt_thong_bao_hien_thi}        Báo thức được đặt đổ chuông sau
${dh_bt_text_bat_bao_thuc}        BẬT
${dh_bt_text_tat_bao_thuc}        TẮT
# man hinh bao thuc
${dh_bt_khong_co_bao_thuc}    bkav.android.deskclock:id/alarms_empty_view
${dh_bt_check_text_ngay_thu}    //android.widget.TextView[@content-desc="Thứ Hai, Thứ Ba, Thứ Tư, Thứ Năm, Thứ Sáu"]
${dh_bt_txt_hien_thi_thu_hai}    //android.widget.CheckBox[@content-desc="Thứ Hai"]
${dh_bt_txt_hien_thi_thu_ba}    //android.widget.CheckBox[@content-desc="Thứ Ba"]
${dh_bt_txt_hien_thi_thu_tu}    //android.widget.CheckBox[@content-desc="Thứ Tư"]
${dh_bt_txt_hien_thi_thu_nam}    //android.widget.CheckBox[@content-desc="Thứ Năm"]
${dh_bt_txt_hien_thi_thu_sau}    //android.widget.CheckBox[@content-desc="Thứ Sáu"]
${dh_bt_txt_hien_thi_thu_bay}    //android.widget.CheckBox[@content-desc="Thứ Bảy"]
${dh_bt_txt_hien_thi_chu_nhat}    //android.widget.CheckBox[@content-desc="Chủ Nhật"]
${dh_bt_cd_rung}    bkav.android.deskclock:id/vibrate_onoff
${dh_bt_label_nhan_dan}    bkav.android.deskclock:id/edit_label
${dh_bt_tab_thu_gon_bao_thuc}    //*[@content-desc="Thu gọn báo thức"]
${dh_bt_btn_nut_them_bao_thuc}    bkav.android.deskclock:id/fab

# Cac phan tu o nut them bao thuc
${dh_bt_nut_dong_y}    android:id/button1
${dh_bt_nut_huy}    android:id/button2
${dh_bt_nut_huy_nhan}    android:id/button2
${dh_bt_nut_lap_lai_bao_thuc}    bkav.android.deskclock:id/repeat_onoff

#các element trong ban phim them bao thuc
${dh_bt_nut_ban_phim}    android:id/toggle_mode
${dh_bt_nut_in_ban_phim}    android:id/input_header

# Cac phan tu chuong bao thuc
${dh_bt_chuong_mac_dinh}    bkav.android.deskclock:id/choose_ringtone
${dh_bt_kieu_chuong_argon}     //*[contains(@text,'Argon')]
${dh_bt_kieu_chuong_tititit}     //*[contains(@text,'Tit ti tit')]
${dh_bt_kieu_chuong_neptunium}     //*[contains(@text,'Neptunium')]
${dh_bt_mo_danh_sach_nhac_chuong}    mo danh sach nhac chuong
${dh_bt_chon_file_nhac_chuong}      //*[contains(@text,'Sounds in device')]
${dh_bt_them_am_thanh_moi}      //*[contains(@text,'Thêm âm thanh mới')]
${dh_bt_back_am_thanh_bao_thuc}      //*[@content-desc="Di chuyển lên"]
${dh_bt_nut_xoa_bao_thuc}    id=bkav.android.deskclock:id/delete
${dh_bt_nut_1}     //*[@content-desc="1"]
${dh_bt_nut_2}     //*[@content-desc="2"]
${dh_bt_nut_3}     //*[@content-desc="3"]
${dh_bt_nut_4}     //*[@content-desc="4"]
${dh_bt_nut_5}     //*[@content-desc="5"]
${dh_bt_nut_6}     //*[@content-desc="6"]
${dh_bt_nut_7}     //*[@content-desc="7"]
${dh_bt_nut_8}     //*[@content-desc="8"]
${dh_bt_nut_9}     //*[@content-desc="9"]
${dh_bt_nut_10}    //*[@content-desc="10"]
${dh_bt_nut_11}    //*[@content-desc="11"]
${dh_bt_nut_12}    //*[@content-desc="12"]
${dh_bt_hien_thi_thoi_gian_bao_thuc}    //*[@text='6']
${dh_bt_nut_mo_popup_sua_bao_thuc}    (//android.widget.ImageButton[@content-desc="Expand alarm"])[3]

# Cac phan tu nhan
${dh_bt_label_nhan_text}    //*[contains(@class,'android.widget.EditText')]

# Cac phan tu dong ho
${dh_hien_thi_text_thoi_gian}    id=bkav.android.deskclock:id/date
${dh_btn_thanh_pho}    xpath=//*[@content-desc="Thành phố"]
${dh_btn_nut_back}        xpath=//*[@content-desc="Di chuyển lên"]
${dh_btn_icon_search}     	id=bkav.android.deskclock:id/search_button
${dh_text_tim_kiem}        xpath=//*[@text="Tìm kiếm…"]
${dh_icon_xoa_text_tim_kiem}    id=bkav.android.deskclock:id/search_close_btn
${dh_hien_thi_text_chu_cai}    id=bkav.android.deskclock:id/index
${dh_check_box_chon_quoc_gia}   xpath=//android.widget.CheckBox[@content-desc="Abidjan"]
${dh_check_box_chon_quoc_gia_theo_danh_sach}    xpath=//*[contains(@resource-id,'bkav.android.deskclock:id/city_onoff')]
${dh_dh_input_tim_thanh_pho}    //*[contains(@resource-id,'bkav.android.deskclock:id/city_name')]
${dh_dh_input_ten_hoa}    HÀ NỘI
${dh_dh_input_ten_co_dau}        Hà Nội
${dh_dh_input_ten_khong_dau}    hanoi
${dh_dh_input_ten_chuoi_ky_tu}        @#$%%^&^%
${dh_dh_input_ten_mot_tu}    H
${dh_hien_thi_time}        xpath=//*[contains(@resource-id,'bkav.android.deskclock:id/city_time')]
${dh_btn_sap_xep_dinh_dang_thoi_gian}    xpath=//*[@text="Sắp xếp theo thời gian"]
${dh_copy_text}    id=com.android.permissioncontroller:id/permission_message

# Check hien thi cai dat
${dh_check_hien_thi_mui_gio}    xpath=//*[contains(@text,'(GMT+7:00) Hồ Chí Minh')]
${dh_hien_thi_text_dong_ho}     //*[contains(@text,'Đồng hồ')]
${dh_hien_thi_kieu_dong_ho}     //*[contains(@text,'Kiểu')]
${dh_btn_check_kieu_dong_ho}    //*[contains(@text,'Đồng hồ số')]
${dh_hien_thi_kieu_dong_ho_kim}    //*[contains(@text,'Đồng hồ kim')]
${dh_btn_dong_ho_so}    //*[contains(@resource-id,'bkav.android.deskclock:id/digital_clock')]
${dh_btn_dong_ho_kim}    (//*[contains(@class,'android.widget.ImageView')])[6]
${dh_text_hien_thi_thoi_gian_voi_so_giay}    //*[contains(@text,'Hiển thị thời gian với số giây')]
${dh_time_seconds}    bkav.android.deskclock:id/digital_clock
${dh_on_off_hien_thi_thoi_gian}    //*[contains(@class,'android.widget.Switch')]
${dh_on_off_dong_ho_tu_dong}    //*[contains(@class,'android.widget.Switch')]

# Cac phan tu mui gio
${dh_check_mui_gio_chinh}     //*[@text='Múi giờ chính']
${dh_check_mui_gio_dong_au}       xpath=(//*[@resource-id='android:id/summary'])[5]
${dh_check_mui_gio_du_bai}    //*[@text='(GMT+4:00) Dubai']
${mui_gio_thanh_pho}    (GMT+7:00) Hồ Chí Minh
${dh_check_mui_gio_ho_chi_minh}    //*[contains(@text,'(GMT+7:00) Hồ Chí Minh')]
${dh_check_mui_gio_hien_thi_jakarta}    //*[contains(@text,'(GMT+7:00) Jakarta')]
${dh_cd_chon_mui_gio_jakarta}    //*[contains(@text,'Jakarta')]
${dh_btn_huy}    android:id/button2
${dh_btn_dong_y}    android:id/button1
${dh_cd_thay_doi_ngay_gio}    //*[contains(@text,'Thay đổi ngày và giờ')]
${dh_cd_su_dung_do_mang_cung_cap}    //*[contains(@text,'Sử dụng thời gian do mạng cung cấp')]
${dh_cd_ngay_va_gio}    //*[contains(@text,'Ngày và giờ')]
${dh_cd_su_dung_dinh_dang_cua_dia_phuong}    //*[contains(@text,'Sử dụng định dạng của địa phương')]
${dh_cd_su_dung_dinh_dang_24_gio}    //*[contains(@text,'Sử dụng định dạng 24 giờ')]
${dh_cd_thoi_gian}    //*[contains(@text,'Đặt giờ')]
${dh_cd_ngay}    //*[contains(@text,'Đặt ngày')]

# Cac phan tu tab dong ho
${dh_ngay_trong_dong_ho}    bkav.android.deskclock:id/date
${dh_dong_ho_so_trong_dong_ho}    bkav.android.deskclock:id/digital_clock
${dh_cd_nha_rieng}    bkav.android.deskclock:id/city_name

# Cac elemet trong ngay o ngay va gio
${dh_cd_ngay_thang}    android:id/date_picker_header_date
${dh_cd_ngay_mot}     //*[@text="1"]

# Cac phan tu trong mui gio trong ngày va gio
${dh_cd_mui_gio}    //*[@text='Múi giờ']
${dh_cd_mui_gio_b60}    //*[@text='Chọn múi giờ']
${dh_cd_khu_vuc}    //*[@text='Khu vực']
${dh_cd_khu_vuc_cairo}    //*[@text='Cairo (GMT+02:00)']
${dh_cd_khu_vuc_ai_cap}    //*[@text='Ai Cập']

# Cac phan tu thoi gian trong thay doi ngay gio
${dh_cd_gio}    android:id/hours
${dh_cd_check_1h}    //*[@content-desc="1"]
${dh_cd_check_5min}    //*[@content-desc="5"]
${dh_bt_nut_tat_da_nhiem_x}    540
${dh_bt_nut_tat_da_nhiem_y}    1735
${start_do_x}    0
${start_do_y}    60
${end_do_x}    30
${end_do_y}    60
${dh_bt_bao_thuc_nhanh}    bkav.android.deskclock:id/left_button
${dh_bt_btn_xong}     bkav.android.deskclock:id/done_button
${dh_bt_thoi_gian_bao_thuc}    //*[@content-desc="8:30 SA"]
${dh_cd_mo_rong_bao_thuc}    //*[@content-desc="Mở rộng báo thức"]
${dh_cd_loai_bo}    bkav.android.deskclock:id/preemptive_dismiss_button
${dh_bt_mo_rong_bao_thuc}    //android.widget.ImageButton[@content-desc="Expand alarm"]
${dh_cd_get_so_phut}    android:id/numberpicker_input

# Cac phan tu trong cai dat bao thuc
${dh_cd_im_lang_sau}    //*[@text='Im lặng sau']
${dh_cd_im_lang_sau_10_phut}    //*[@text='10 phút']
${dh_cd_im_lang_sau_5_phut}    //*[@text='5 phút']
${dh_cd_im_lang_sau_9_phut}    //*[@text='9 phút']
${dh_cd_im_lang_sau_13_phut}    //*[@text='13 phút']
${dh_cd_thoi_luong_bao_lai}    //*[contains(@text,'Thời lượng báo lại')]
${dh_cd_tang_dan_am_luong}    //*[@text='Tăng dần âm lượng']
${dh_cd_thoi_luong_5_giay}    //*[@text='5 giây']
${dh_cd_thoi_luong_60_giay}    //*[@text='60 giây']
${dh_cd_icon_bat_mui_gio_tu_dong}    xpath=(//*[@resource-id="android:id/switch_widget"])[2]
${dh_cd_icon_bat_ngay_tu_dong}    xpath=(//*[@resource-id="android:id/switch_widget"])[1]

# Cac phan tu bo hen gio
${dh_cd_txt_bo_hen_gio_het_han}    xpath=//*[@text="Bộ hẹn giờ đã hết hạn"]
${dh_cd_txt_Argon_am_nhac}       xpath=//*[@text="Argon"]
${dh_cd_icon_hien_thi_duoc_chon}    id=bkav.android.deskclock:id/sound_image_selected
${dh_cd_txt_am_thanh_hen_gio}    xpath=//*[@text="Âm thanh hẹn giờ"]
${dh_cd_txt_tang_am_luong}       xpath=//*[@text="Tăng dần âm lượng"]
${dh_cd_txt_bo_hen_gio_rung}        xpath=//*[contains(@text,"Bộ hẹn giờ rung")]
${dh_cd_txt_them_am_thanh_moi}    xpath=//*[contains(@text,'Thêm âm thanh mới')]
${dh_cd_btn_nut_switch}    xpath=//*[contains(@text,'TẮT')]
${dh_cd_btn_nut_switch_on}    xpath=//*[contains(@text,'BẬT')]

# Cac phan tu bao thuc
${dh_cd_txt_tuan_bat_dau_vao}    xpath=//*[contains(@text,'Tuần bắt đầu vào')]
${dh_cd_txt_thu_hai}        xpath=//*[@text="Thứ Hai"]
${dh_cd_txt_thu_bay}        xpath=//*[@text="Thứ Bảy"]
${dh_cd_txt_chu_nhat}       xpath=//*[@text="Chủ Nhật"]
${dh_cd_text_thu_hai}        Thứ Hai
${dh_cd_text_thu_bay}        Thứ Bảy
${dh_cd_text_chu_nhat}       Chủ Nhật
${dh_cd_txt_hanh_dong_lac_may}     xpath=//*[contains(@text,'Hành động lắc máy')]
${dh_cd_txt_lac_may_khong_lam_gi}     xpath=//*[contains(@text,'Lắc máy sẽ không làm gì')]
${dh_cd_txt_khong_lam_gi}     xpath=//*[contains(@text,'Không làm gì')]
${dh_cd_txt_bao_lai}     xpath=//*[contains(@text,'Báo lại')]
${dh_cd_txt_hien_thi_bao_lai}     xpath=//*[contains(@text,'Lắc máy sẽ nhắc lại báo thức')]
${dh_cd_txt_loai_bo}     xpath=//*[contains(@text,'Loại bỏ')]
${dh_cd_tuy_chon_khac}    //*[@content-desc="Tùy chọn khác"]
${dh_cd_cho_phep_dong_ho_set}    com.android.permissioncontroller:id/permission_allow_button
${dh_cd_time_bao_thuc_sau}    android:id/numberpicker_input
${dh_cd_bao_thuc_ngan}    bkav.android.deskclock:id/edit_label
${dh_bt_nut_tat_bao_thuc_nhanh_x}    988
${dh_bt_nut_tat_bao_thuc_nhanh_y}    1480

# XPATH CUA APP DONG HO MAN HINH HEN GIO
${dh_hg_hien_thi_dong_ho_da_hen_gio}    id=bkav.android.deskclock:id/timer_time
${dh_hg_nut_notyfi}    id=bkav.android.deskclock:id/chronometer
${dh_hg_btn_huy_them_bo_hen_gio}     xpath=//*[@content-desc="Hủy"]

# Cac phan tu hen thoi gian
${dh_hg_time_setup}    id=bkav.android.deskclock:id/timer_setup_digit
${dh_hg_hien_thi_thoi_gian_thiet_lap}    id=bkav.android.deskclock:id/timer_setup_time
${dh_hg_nut_xoa_thoi_gian_thiet_lap}    id=bkav.android.deskclock:id/timer_setup_delete
${dh_hg_check_thanh_ngang}    id=bkav.android.deskclock:id/timer_setup_divider
${dh_hg_nut_1}        id=bkav.android.deskclock:id/timer_setup_digit_1
${dh_hg_nut_2}        id=bkav.android.deskclock:id/timer_setup_digit_2
${dh_hg_nut_3}        id=bkav.android.deskclock:id/timer_setup_digit_3
${dh_hg_nut_4}        id=bkav.android.deskclock:id/timer_setup_digit_4
${dh_hg_nut_5}        id=bkav.android.deskclock:id/timer_setup_digit_5
${dh_hg_nut_6}        id=bkav.android.deskclock:id/timer_setup_digit_6
${dh_hg_nut_7}        id=bkav.android.deskclock:id/timer_setup_digit_7
${dh_hg_nut_8}        id=bkav.android.deskclock:id/timer_setup_digit_8
${dh_hg_nut_9}        id=bkav.android.deskclock:id/timer_setup_digit_9
${dh_hg_nut_0}        id=bkav.android.deskclock:id/timer_setup_digit_0
# Cac phan tu chuc nang bat dau chay thoi gian
${dh_hg_bat_dau}         xpath=//*[@content-desc="Bắt đầu"]
${dh_hg_stop}            xpath=//*[@content-desc="Dừng"]
${dh_hg_nut_dat_lai_gio_hen}        id=bkav.android.deskclock:id/reset_add
${dh_hg_them_phut}                  xpath=//*[@content-desc="Thêm 1 phút"]
${dh_hg_nhan_comment}        id=bkav.android.deskclock:id/timer_label
${dh_hg_nhan_o_text_comment}        id=bkav.android.deskclock:id/custom
${dh_hg_input_text}            xpath=//android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.EditText
${dh_hg_btn_huy_nhan_comment}        id=android:id/button2
${dh_hg_btn_dong_y_nhan_comment}        id=android:id/button1
${dh_hg_check_notify_text}               id=bkav.android.deskclock:id/content
${dh_hg_dong_ho_hien_thi}        id=bkav.android.deskclock:id/timer_time_text
${dh_hg_xoa_thoi_gian_thiet_lap_hen_gio}        xpath=//*[@content-desc="Xóa"]
${dh_hg_them_gio_hen}        id=bkav.android.deskclock:id/right_button
${dh_hg_huy_them_gio_hen}        id=bkav.android.deskclock:id/left_button
${dh_cai_dat_x}    294
${dh_cai_dat_y}    852

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG EDICT
#=================================================================================================


#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG ESIM
#=================================================================================================


#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG GHI AM
#=================================================================================================
${ga_btn}                  bkav.android.recorder:id/recordButton
${ga_btn_tam_dung}         bkav.android.recorder:id/recordButton
${ga_dang_ghi_am}          bkav.android.recorder:id/record_state
${ga_tam_dung}             //*[@text="Tạm dừng"]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG GHI CHEP
#=================================================================================================


#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG HO TRO
#=================================================================================================
${ht_icon_avatar}                  id=bkav.android.connect:id/avatar
${ht_icon_tuy_chon_khac}           xpath=//*[@content-desc="Tùy chọn khác"]
${ht_btn_tab_nhat_ky_QXDM}         xpath=//*[contains(@text,'Nhật ký QXDM')]
${ht_btn_bat_dau}                  xpath=//*[contains(@text,'BẮT ĐẦU')]
${ht_btn_dung_lai}                 xpath=//*[contains(@text,'DỪNG LẠI')]
${ht_btn_xoa_nhat_ky}              xpath=//*[contains(@text,'Xóa nhật ký QXDM')]
${ht_btn_bao_cao_loi}              xpath=//*[contains(@text,'Báo cáo lỗi')]
${ht_btn_thong_bao_bkav}           xpath=//*[contains(@text,'Nhận thông báo của Bkav')]
${ht_btn_tab_cap_nhat_offline}     xpath=//*[contains(@text,'Cập nhật hệ thống offline')]
${ht_btn_xuat_trang_thai_may}      xpath=//*[contains(@text,'Xuất trạng thái máy')]
${ht_btn_tab_bat_nhat_ky}          xpath=//*[contains(@text,'Bật nhật ký')]
${ht_txt_so_dien_thoai}            xpath=//*[contains(@text,'1900 545499')]
${ht_sdt}                          1900 545499
${ht_phu_kien_op_lung}             Ốp lưng B86 nhựa cứng cao cấp
${ht_dang_tao_loi_1}               Báo cáo lỗi
${ht_dang_tao_loi_1a}              đang được tạo
${ht_dang_tao_loi_2}               Báo cáo lỗi
${ht_dang_tao_loi_2a}              đang được tạo
${ht_link_Bstore}                  bphone.vn/bphone-stores
${ht_link_bphoto}                  bphone.vn/bphoto
${ht_btn_icon_Bstore}              id=bkav.android.connect:id/bstore
${ht_btn_icon_chat}                id=bkav.android.connect:id/chat_cskh
${ht_btn_icon_phu_kien}            id=bkav.android.connect:id/accessories
${ht_btn_icon_bphoto}              id=bkav.android.connect:id/bphoto
${ht_btn_icon_goi_dien}            id=bkav.android.connect:id/call_supporters
${ht_txt_chim_lac}                 xpath=//*[contains(@text,'Chim Lạc')]
${ht_btn_luon_chon}                xpath=//*[contains(@text,'LUÔN CHỌN')]
${ht_txt_phu_kien_op_lung}         xpath=//*[contains(@text,'Ốp lưng B86 nhựa cứng cao cấp')]
${ht_txt_bao_loi_1}                xpath=//*[contains(@text,'Báo cáo lỗi')]
${ht_txt_bao_loi_2}                xpath=//*[contains(@text,'Báo cáo lỗi')]
${ht_click_toa_do_x}               182
${ht_click_toa_do_y}               646
${ht_click_check_box_bao_loi}            xpath=//*[@resource-id="android:id/checkbox"]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG KHAM PHA
#=================================================================================================
${kp_nut_xem_huong_dan}    bkav.android.explorer:id/start_chimlac_btn
${kp_nut_hd_sau}           bkav.android.explorer:id/next_page_btn
${kp_nut_hd_truoc}         bkav.android.explorer:id/previous_page_btn

# Cac URL huong dan
${kp_url_chong_trom}                   https://bphone.vn/chong-trom
${kp_url_chong_trom_sau_dieu_huong}    bphone.vn/chong-trom
${kp_url_android}                      https://bphone.vn/android
${kp_url_android_sau_dieu_huong}       bphone.vn/huong-dan-su-dung/-/view-content/243772/huong-dan-chuyen-du-lieu-tu-android-sang-bos
${kp_url_ios}                          https://bphone.vn/ios
${kp_url_ios_sau_dieu_huong}           bphone.vn/huong-dan-su-dung/-/view-content/243746/huong-dan-chuyen-du-lieu-tu-ios-sang-bos
${kp_url_sac_pin}                      https://bphone.vn/sac-pin
${kp_url_sac_pin_sau_dieu_huong}       bphone.vn/huong-dan-su-dung/-/view-content/243733/mot-so-luu-y-e-sac-pin-bphone-hieu-qua-hon

# Cac tieu de trong trang web huong dan
${kp_txt_chong_trom}    TÍNH NĂNG CHỐNG TRỘM
${kp_txt_android}       Hướng dẫn chuyển dữ liệu từ Android sang BOS
${kp_txt_ios}           Hướng dẫn chuyển dữ liệu từ IOS sang BOS
${kp_txt_sac_pin}       Một số lưu ý để sạc pin Bphone hiệu quả hơn

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG MAY TINH
#=================================================================================================
# Cac phan tu trong giao dien hien thi
${mt_hien_thi}              id=com.bkav.calculator2:id/display
${mt_hien_thi_phep_tinh}    id=com.bkav.calculator2:id/formula
${mt_hien_thi_ket_qua}      id=com.bkav.calculator2:id/result
${mt_loi_ko_phai_so}        xpath=//*[@text="Không phải số"]

# Cac phan tu trong giao dien chinh
${mt_nut_0}       id=com.bkav.calculator2:id/digit_0
${mt_nut_1}       id=com.bkav.calculator2:id/digit_1
${mt_nut_2}       id=com.bkav.calculator2:id/digit_2
${mt_nut_3}       id=com.bkav.calculator2:id/digit_3
${mt_nut_4}       id=com.bkav.calculator2:id/digit_4
${mt_nut_5}       id=com.bkav.calculator2:id/digit_5
${mt_nut_6}       id=com.bkav.calculator2:id/digit_6
${mt_nut_7}       id=com.bkav.calculator2:id/digit_7
${mt_nut_8}       id=com.bkav.calculator2:id/digit_8
${mt_nut_9}       id=com.bkav.calculator2:id/digit_9
${mt_nut_cong}    id=com.bkav.calculator2:id/op_add
${mt_nut_tru}     id=com.bkav.calculator2:id/op_sub
${mt_nut_nhan}    id=com.bkav.calculator2:id/op_mul
${mt_nut_chia}    id=com.bkav.calculator2:id/op_div
${mt_nut_bang}    id=com.bkav.calculator2:id/eq
${mt_nut_phay}    xpath=//*[@text=","]
${mt_nut_clr}     id=com.bkav.calculator2:id/clr
${mt_nut_xoa}     id=com.bkav.calculator2:id/del

# Cac phan tu trong giao dien lich su
${mt_nut_lich_su}             xpath=//*[@text="Lịch sử"]
${mt_txt_khong_co_lich_su}    xpath=//*[@text="Không có lịch sử"]
${mt_nut_xoa_lich_su}         xpath=//*[@text="Xóa lịch sử"]
${mt_nut_ket_qua_ls}          com.bkav.calculator2:id/bkav_history_result
${mt_nut_bieu_thuc_ls}        com.bkav.calculator2:id/bkav_history_formula

# Cac phan tu trong giao dien nang cao
${mt_nut_nang_cao}      id=com.bkav.calculator2:id/bt_more
${mt_nut_inv}           id=com.bkav.calculator2:id/toggle_inv
${mt_nut_deg_rad}       id=com.bkav.calculator2:id/toggle_mode
${mt_nut_phan_tram}     id=com.bkav.calculator2:id/op_pct
${mt_nut_mc}            id=com.bkav.calculator2:id/op_m_c
${mt_nut_sin}           id=com.bkav.calculator2:id/fun_sin
${mt_nut_cos}           id=com.bkav.calculator2:id/fun_cos
${mt_nut_tan}           id=com.bkav.calculator2:id/fun_tan
${mt_nut_m_cong}        id=com.bkav.calculator2:id/op_m_plus
${mt_nut_ln}            id=com.bkav.calculator2:id/fun_ln
${mt_nut_log}           id=com.bkav.calculator2:id/fun_log
${mt_nut_giai_thua}     id=com.bkav.calculator2:id/op_fact
${mt_nut_m_tru}         id=com.bkav.calculator2:id/op_m_sub
${mt_nut_pi}            id=com.bkav.calculator2:id/const_pi
${mt_nut_e}             id=com.bkav.calculator2:id/const_e
${mt_nut_luy_thua}      id=com.bkav.calculator2:id/op_pow
${mt_nut_mr}            id=com.bkav.calculator2:id/op_m_r
${mt_nut_ngoac_trai}    id=com.bkav.calculator2:id/lparen
${mt_nut_ngoac_phai}    id=com.bkav.calculator2:id/rparen
${mt_nut_can_bac_2}     id=com.bkav.calculator2:id/op_sqrt

# Cac phan tu trong giao dien nang cao va che do INV
${mt_nut_arcsin}    id=com.bkav.calculator2:id/fun_arcsin
${mt_nut_arccos}    id=com.bkav.calculator2:id/fun_arccos
${mt_nut_arctan}    id=com.bkav.calculator2:id/fun_arctan
${mt_nut_e_mu}      id=com.bkav.calculator2:id/fun_exp
${mt_nut_10_mu}     id=com.bkav.calculator2:id/fun_10pow
${mt_nut_mu_2}      id=com.bkav.calculator2:id/op_sqr

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG NGHE DAI
#=================================================================================================
# Cac phan tu man hinh yeu thich
${nd_phat_dai}    bkav.android.radioonline:id/play
${nd_stop}        bkav.android.radioonline:id/stop
${nd_txt_dang_phat}         Đang phát
${nd_txt_tam_dung}          Tạm dừng
${nd_tab_yeu_thich}    xpath=//*[contains(@text,'Yêu thích')]
${nd_tab_tat_ca_kenh}    xpath=//*[contains(@text,'Tất cả kênh')]
${nd_tab_kenh_vua_nghe}    xpath=//*[contains(@text,'Kênh vừa nghe')]
${nd_phan_tu_theo_avata}    xpath=//*[contains(@resource-id,'bkav.android.radioonline:id/avatarparent')]
${nd_txt_vov2}    xpath=//*[contains(@text,'VOV2')]
${nd_txt_hien_thi_1}    xpath=//android.view.ViewGroup[1]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_txt_vov2_chi_tiet}    xpath=//*[contains(@text,'Hệ Văn hóa - Đời sống - Khoa giáo')]
${nd_txt_vov1}   xpath=//*[contains(@text,'VOV1')]
${nd_txt_hien_thi_2}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[2]
${nd_txt_vov1_chi_tiet}    xpath=//*[contains(@text,'Hệ thời sự - chính trị - tổng hợp')]
${nd_txt_vov3}    xpath=//*[contains(@text,'VOV3')]
${nd_txt_hien_thi_8}    xpath=//android.view.ViewGroup[3]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_txt_vov3_chi_tiet}     xpath=//*[contains(@text,'Hệ Âm nhạc - Thông tin - Giải trí')]
${nd_txt_nhac_quoc_te}    xpath=//*[contains(@text,'Nhạc Quốc tế')]
${nd_txt_hien_thi_3}   xpath=//android.view.ViewGroup[4]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_txt_chi_tiet_nhac_quoc_te}    xpath=//*[contains(@text,'Những bản nhạc quốc tế bất hủ')]
${nd_txt_nhac_Audiophile}    xpath=//*[contains(@text,'Nhạc Audiophile')]
${nd_txt_hien_thi_4}    xpath=//android.view.ViewGroup[5]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_txt_chi_tiet_nhac_Audiophile}    xpath=//*[contains(@text,'Âm nhạc – Nhạc cho người nghe chuyên nghiệp')]
${nd_txt_nhac_thieu_nhi_quoc_te}    xpath=//*[contains(@text,'Nhạc Thiếu nhi quốc tế')]
${nd_txt_hien_thi_5}    xpath=//android.view.ViewGroup[6]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_txt_chi_tiet_nhac_thieu_nhi_quoc_te}    xpath=//*[contains(@text,'Tuyển chọn các bài hát thiếu nhi tiếng Anh được yêu...')]
${nd_txt_nhac_Classical_Guitar}    xpath=//*[contains(@text,'Classical Guitar')]
${nd_txt_hien_thi_6}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[7]
${nd_txt_chi_tiet_nhac_Classical_Guitar}    xpath=//*[contains(@text,'Những tác phẩm độc tấu guitar cổ điển hay nhất')]
${nd_txt_nhac_Nhạc Jazz}    xpath=//*[contains(@text,'Nhạc Jazz')]
${nd_txt_hien_thi_7}    xpath=//android.view.ViewGroup[8]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_txt_chi_tiet_Nhạc Jazz}    xpath=//*[contains(@text,'Những bản nhạc Jazz hay nhất')]
${nd_txt_hien_thi_9}    	id=//android.view.ViewGroup[9]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_txt_hien_thi_10}    	id=//android.view.ViewGroup[10]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_txt_hien_thi_11}  	id=//android.view.ViewGroup[11]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_btn_reset}    	id=bkav.android.radioonline:id/reset
${nd_txt_chi_tiet_reset}    xpath=//*[contains(@text,'Tải lại danh sách yêu thích mặc định')]
${nd_list_phan_tu}    xpath=//android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]

# Cac phan tu man hinh tat ca kenh
${nd_tctl_btn_tat_cac_the_loai}    id=bkav.android.radioonline:id/spinner
${nd_tctl_txt_tat_ca_cac_the_loai}    id=id=bkav.android.radioonline:id/name
${nd_tck_icon_search}    id=bkav.android.radioonline:id/btn_search
${nd_btn_tab_about}    id=bkav.android.radioonline:id/btn_about

# Cac phan tu tat ca cac the loai
${nd_tctl_txt_chon_tat_ca}    xpath=//*[contains(@text,'Tất cả các thể loại')]
${nd_tctl_txt_chon_chinh_tri}    xpath=//*[contains(@text,'Chính trị')]
${nd_tctl_txt_chon_tin_tuc}    xpath=//*[contains(@text,'Tin tức')]
${nd_tctl_txt_chon_giai_tri}    xpath=//*[contains(@text,'Giải trí')]
${nd_tctl_txt_chon_du_lich}    xpath=//*[contains(@text,'Du lịch')]
${nd_tctl_txt_chon_xa_hoi}     xpath=//*[contains(@text,'Xã hội')]
${nd_tctl_txt_chon_tong_ho}    xpath=//*[contains(@text,'Tổng hợp')]
${nd_tctl_txt_chon_the_thao}    xpath=//*[contains(@text,'Thể thao')]
${nd_tctl_txt_chon_van_hoa}   xpath=//*[contains(@text,'Văn hóa')]
${nd_tctl_txt_chon_am_nhac}    xpath=//*[contains(@text,'Âm nhạc')]
${nd_tctl_txt_chon_giao_duc}    xpath=//*[contains(@text,'Giáo dục')]
${nd_tctl_txt_chon_giao_thong}    xpath=//*[contains(@text,'Giao thông')]

# Cac phan tu tim kiem
${nd_tctl_tk_btn_back_tim_kiem}    id=bkav.android.radioonline:id/back_button
${nd_tctl_tk_input_tim_kiem}     xpath=//*[contains(@text,'Tìm kiếm')]

# Cac phan tu thong tin chi tiet
${nd_ttct_txt_thong_tin}    xpath=//*[contains(@text,'Thông tin:')]
${nd_ttct_txt_tieu_de_dien_thoai}    xpath=//*[contains(@text,'Điện thoại:')]
${nd_ttct_txt_sdt}    xpath=//*[contains(@text,'2552')]
${nd_ttct_txt_tieu_de_hop_thu}    xpath=//*[contains(@text,'Hộp thư:')]
${nd_ttct_txt_hop_thu}    xpath=//*[contains(@text,'bkav@bkav.com.vn')]
${nd_ttct_txt_tieu_de_trang_chu}    xpath=//*[contains(@text,'Trang chủ:')]
${nd_ttct_txt_link_trang_chu}    xpath=//*[contains(@text,'http://www.bkav.com.vn')]

# Cac phan tu man hinh kenh vua nghe
${nd_yt_btn_clear}    xpath=//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView

# Cac phan tu nut
${nd_yt_hien_thi_txt_nhac_dang_phat}    xpath=//*[contains(@resource-id,'bkav.android.radioonline:id/radio_name')]
${nd_yt_txt_chi_tiet_nhac_quoc_te_dang_phat}    xpath=//*[contains(@text,'Những bản nhạc quốc tế bất hủ')]
${nd_yt_btn_stop}    id=bkav.android.radioonline:id/stop
${nd_yt_btn_play}    id=bkav.android.radioonline:id/play
${nd_yt_btn_time}    id=bkav.android.radioonline:id/timer
${nd_yt_txt_hien_thi_kenh_dang_phat}      //android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView[1]
${nd_yt_btn_icon_yeu_thich}    xpath=(//*[@resource-id="bkav.android.radioonline:id/star"])[2]
${nd_yt_txt_chi_tiet}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[6]
${nd_yt_list_phan_tu_hien_thi}    xpath=//android.widget.RelativeLayout[2]/android.widget.TextView[1]

# Cac phan tu danh sach mac dinh
${nd_yt_txt_1}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[1]
${nd_yt_txt_2}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[2]
${nd_yt_txt_3}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[3]
${nd_yt_txt_4}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[4]
${nd_yt_txt_5}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[5]
${nd_yt_txt_6}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[6]
${nd_yt_txt_7}    xpath=(//*[@resource-id="bkav.android.radioonline:id/radio_name"])[7]
${nd_yt_txt_8}    xpath=//android.widget.RelativeLayout[8]/android.widget.RelativeLayout[2]/android.widget.TextView[1]
${nd_yt_btn_hen_gio}    id=bkav.android.radioonline:id/timer
${nd_yt_txt_hien_thi_thoi_gian_hen_gio}    id=bkav.android.radioonline:id/text_time_cancel
${nd_yt_txt_thong_tin_hen_gio}    xpath=//*[@text="Radio sẽ dừng phát sau"]
${nd_yt_so_thoi_gian_hen_gio}    id=android:id/numberpicker_input
${nd_yt_btn_huy_hen_gio}    id=bkav.android.radioonline:id/negative_button
${nd_yt_btn_Xong_hen_gio}    id=bkav.android.radioonline:id/positive_button
${nd_yt_txt_thong_bao_ket_thuc_hen_gio}    xpath=//*[contains(@text,'Nhạc sẽ dừng lúc:')]
${nd_yt_txt_bo_hen_gio}    xpath=//*[contains(@text,'Chạm để bỏ hẹn giờ')]
${nd_yt_txt_ngung_phat}    xpath=//*[contains(@text,'Tạm dừng')]
${nd_yt_txt_dang_phat}    xpath=//*[contains(@text,'Đang phát')]
${nd_yt_txt_khong_co_ket_noi}    xpath=//*[contains(@text,'Không có kết nối')]
${nd_yt_btn_them_moi}    id=bkav.android.radioonline:id/float_menu
${nd_yt_icon_them_moi}    xpath=(//*[@resource-id="bkav.android.radioonline:id/mini_fab"])[2]
${nd_yt_icon_de_xuat}    xpath=(//*[@resource-id="bkav.android.radioonline:id/mini_fab"])[1]

${nd_tm_input_link_kenh}    id=bkav.android.radioonline:id/stream
${nd_tm_trang_thai_nghe_thu}    xpath=//*[contains(@text,'NGHE THỬ')]
${nd_tm_trang_thai_mat_ket_noi}    xpath=//*[contains(@text,'MẤT KẾT NỐI')]
${nd_tm_input_ten_kenh}    xpath=(//*[@resource-id="bkav.android.radioonline:id/name"])[1]
${nd_tm_checklist_the_loai}    xpath=(//*[@resource-id="bkav.android.radioonline:id/name"])[2]
${nd_tm_checklist_quoc_gia}    xpath=(//*[@resource-id="bkav.android.radioonline:id/name"])[3]
${nd_tm_input_mo_ta}    id=bkav.android.radioonline:id/description
${nd_tm_btn_them_kenh_moi}    xpath=//*[contains(@text,'THÊM KÊNH')]
${nd_tm_khu_vuc_quoc_gia}    xpath=//*[contains(@text,'Vietnam')]
${nd_tm_btn_kenh_moi}    xpath=//*[contains(@text,'kenh_moi')]

${nd_dx_web_url}    id=bkav.android.radioonline:id/web_url
${nd_dx_radio_name}    id=bkav.android.radioonline:id/name
${nd_dx_description}    id=bkav.android.radioonline:id/description
${nd_dx_btn_them_moi_de_xuat}    xpath=//*[contains(@text,'ĐỀ XUẤT')]
${nd_dx_tieu_de_url}    xpath=//*[contains(@text,'Web URL')]
${nd_dx_tieu_de_ten_kenh}    xpath=//*[contains(@text,'Tên kênh')]
${nd_dx_tieu_de_mo_ta}    xpath=//*[contains(@text,'Mô tả')]
${nd_dx_text_tieu_de_url}     Web URL
${nd_dx_text_tieu_de_ten_kenh}     Tên kênh
${nd_dx_text_tieu_de_mo_ta}     Mô tả

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG KHONG LAM PHIEN
#=================================================================================================
${klp_cham_vao_toa_do_x}    192
${klp_cham_vao_toa_do_y}    1233
${klp_cham_vao_text_khong_lam_phien}    xpath=//*[contains(@text,'Không làm phiền')]
${klp_text_bat_ngay_bay_gio}           xpath=//*[@resource-id="com.android.settings:id/zen_mode_settings_turn_on_button"]
${klp_text_hien_thi_bat}    BẬT NGAY BÂY GIỜ
${klp_text_tat_ngay_bay_gio}           xpath=//*[@resource-id="com.android.settings:id/zen_mode_settings_turn_off_button"]
${klp_text_hien_thi_tat}    TẮT NGAY BÂY GIỜ

${klp_vuot_vao_bat_dau_toa_do_x}    90
${klp_vuot_vao_bat_dau_toa_do_y}    60
${klp_vuot_vao_ket_thuc_toa_do_x}    15
${klp_vuot_vao_ket_thuc_toa_do_y}    50

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG NGHE NHAC
#=================================================================================================


#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG NHAC VIEC
#=================================================================================================
${nv_nut_them_moi}    bkav.android.reminder:id/fab

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG NOTIFICATION
#=================================================================================================


#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG QUAN LY FILE
#=================================================================================================
${ht_txt_Android}                   xpath=//*[contains(@text,'Android')]
${ht_txt_Logs}                      xpath=//*[contains(@text,'Logs')]
${ht_txt_diag_logs}                 xpath=//*[contains(@text,'diag_logs')]
${ht_text_diag_logs}                diag_logs
${ht_txt_files}                     xpath=//*[contains(@text,'files')]
${ht_vi_tri_file_ota}               xpath=//*[contains(@text,'TỆP GẦN ĐÂY')]
${ht_text_file_ota}                 TỆP GẦN ĐÂY

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG SUC KHOE
#=================================================================================================
${sk_them_muc_tieu}    	bkav.fitness.activity:id/bt_create_work_out

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG THOI TIET
#=================================================================================================
# Cac phan tu trong man hinh chinh
${tt_btn_clear_text_tim_kiem}    id=bkav.android.weather:id/btn_clear
${tt_ds_dia_diem}           xpath=//*[@resource-id="bkav.android.weather:id/location_name"]
${tt_ds_nhiet_do}           xpath=//*[@resource-id="bkav.android.weather:id/location_temp"]
${tt_dia_diem_mac_dinh}     xpath=(//*[@resource-id="bkav.android.weather:id/location_name"])[1]
${tt_nhiet_do_mac_dinh}     xpath=(//*[@resource-id="bkav.android.weather:id/location_temp"])[1]
${tt_nut_do_c}              xpath=//*[contains(@text,'°C')]
${tt_nut_do_f}              xpath=//*[contains(@text,'°F')]
${tt_nut_them_thanh_pho}    id=bkav.android.weather:id/add_location_id
${tt_nut_xoa}               xpath=//*[@text="Xóa"]
${tt_txt_ket_noi_mang}      Vui lòng kết nối mạng

# Cac phan tu trong man hinh tim kiem
${tien_ich_thoi_tiet}                 bkav.android.weather:id/widget_layout
${tt_tk_nhap_ten_thanh_pho}    xpath=//*[@text="Nhập tên thành phố"]
${tt_tk_ds_ten_thanh_pho}      xpath=//*[@resource-id="android:id/text1"]
${tt_tk_ha_noi}                xpath=//*[contains(@text,'Hà Nội')]
${tt_tk_text_ha_noi}           xpath=//*[contains(@text,'Hà Nội, VN')]
${tt_tk_quang_ninh}            xpath=//*[contains(@text,'Quảng Ninh')]
${tt_tk_ha_nam}                xpath=//*[contains(@text,'Hà Nam')]
${tt_tk_text_ha_nam}           xpath=//*[contains(@text,'Hà Nam, VN')]
${tt_tk_text_icon_tien_ich}    xpath=//*[@content-desc="Tiện ích"]
${tt_tk_txt_tien_ich_thoi_tiet}    xpath=//*[@text="Tiện ích của Thời tiết"]

# Cac phan tu trong man hinh thong tin thoi tiet
${tt_txt_24h_toi}    xpath=//*[contains(@text,'24h tới')]
${tt_ds_gio}         xpath=//*[@resource-id="bkav.android.weather:id/hourly"]
${tt_ds_thu}         xpath=//*[@resource-id="bkav.android.weather:id/daily"]
${tt_ds_thu_3}       xpath=(//*[@resource-id="bkav.android.weather:id/daily"])[3]
${tt_nut_hom_nay}    xpath=//*[@text="Hôm nay"]
${tt_vi_tri_nut_hom_nay}    xpath=//android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.TextView

# Cac phan tu trong man hinh chi tiet thoi tiet
${tt_ttct_bieu_do_gio}     bkav.android.weather:id/chart_hourly_weather
${tt_ttct_bieu_do_ngay}    bkav.android.weather:id/chart_day_weather

# Cac phan tu trong ung dung OpenWeather
${tt_open_weather_nut_cai_dat}    //*[@content-desc="Go to settings"]
${tt_open_weather_nut_thay_doi_don_vi_do}      //*[@text="Customize units"]
${tt_open_weather_opt_do_c_open_weather}       //*[@text="°C"]
${tt_open_weather_opt_do_f_open_weather}       //*[@text="°F"]

#=================================================================================================
#    CAC PHAN TU TRONG UNG DUNG XEM PHIM
#=================================================================================================
# Cac phan tu trong giao dien thu muc
${xp_tm_nut_tim_kiem}                   xpath=//*[@content-desc="Tìm kiếm…"]
${xp_tm_xem_video_gan_day}              xpath=//*[@content-desc="Xem video gần đây"]
${xp_tm_text_tim_kiem_phim}             xpath=//*[contains(@text,'Tìm kiếm phim')]
${xp_tm_nut_xoa_truy_van}               xpath=//*[@content-desc="Xóa truy vấn"]
${xp_tm_nut_thu_gon}                    xpath=//*[@content-desc="Thu gọn"]
${xp_tm_text_hshfj}                     xpath=//*[contains(@text,'hshfj')]
${xp_tm_nut_sap_xep_theo}               xpath=//*[contains(@text,'Sắp xếp theo…')]
${xp_tm_nut_cap_nhat_lai}               xpath=//*[contains(@text,'Cập nhật lại')]
${xp_tm_nut_thong_tin}                  xpath=//*[contains(@text,'Thông tin')]
${xp_tm_text_dien_thoai}                xpath=//*[contains(@text,'Điện thoại:')]
${xp_tm_text_so_dien_thoai}             xpath=//*[contains(@text,'024 3763 2552')]
${xp_tm_text_hop_thu}                   xpath=//*[contains(@text,'Hộp thư:')]
${xp_tm_text_trang_chu}                 xpath=//*[contains(@text,'Trang chủ:')]
${xp_tm_text_bkav@bkav.com.vn}          xpath=//*[contains(@text,'bkav@bkav.com.vn')]
${xp_tm_text_http://www.bkav.com.vn}    xpath=//*[contains(@text,'http://www.bkav.com.vn')]
${xp_tm_man_da_nhiem}                   id=bkav.android.launcher3:id/snapshot
${xp_tm_app_dien_thoai}                 xpath=//*[contains(@text,'Điện thoại')]
${xp_tm_nut_chi_mot_lan}                xpath=//*[contains(@text,'CHỈ MỘT LẦN')]

${xp_tm_goi_dien_nut_quay_so}              xpath=//android.widget.ImageView[@content-desc="quay số"]
${xp_tm_goi_dien_nut_loa}                  id=com.android.dialer:id/speakerButtonBkav
${xp_tm_goi_dien_nut_ban_phim}             id=com.android.dialer:id/dialpadButtonBkav
${xp_tm_goi_dien_nut_ket_thuc_cuoc_goi}    id=com.android.dialer:id/endButtonBkav_incall

${xp_tm_gui_thu_text_gmail_bkav}           xpath=//android.widget.Button[@content-desc="bkav@bkav.com.vn, bkav@bkav.com.vn"]
${xp_tm_gui_thu_text_soan_thu}             xpath=//*[contains(@text,'Soạn thư')]
${xp_tm_gui_thu_nut_gui}                   xpath=//android.widget.TextView[@content-desc="Gửi"]

${xp_tm_link_TC_title}                     xpath=//android.view.View[@content-desc="www.bkav.com"]
${xp_tm_link_TC_text_bkavpro}              xpath=//android.view.View[@content-desc="Bkav Pro"]/android.widget.TextView
${xp_tm_link_TC_url_bar}                   xpath=android.bkav.bchrome:id/url_bar

${xp_tm_sap_xep_theo_ten}                  xpath=//*[contains(@text,'Tên')]
${xp_tm_sap_xep_theo_ten_az}               xpath=//*[contains(@text,'Tên (z-a)')]
${xp_tm_sap_xep_theo_do_dai}               xpath=//*[contains(@text,'Độ dài')]
${xp_tm_sap_xep_theo_do_dai_giam_dan}      xpath=//*[contains(@text,'Độ dài (Giảm dần)')]
${xp_tm_sap_xep_theo_ngay}                 xpath=//*[contains(@text,'Ngày (Cũ nhất)')]

${xp_tm_ten_vd_stt_1}                      xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[1]
${xp_tm_ten_vd_stt_2}                      xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[2]
${xp_tm_ten_vd_stt_3}                      xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[3]

${xp_tm_thoi_luong_vd_1}                   xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_time')])[1]
${xp_tm_thoi_luong_vd_2}                   xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_time')])[2]
${xp_tm_thoi_luong_vd_3}                   xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_time')])[3]

${xp_tm_video_SampleVideosMP4}             xpath=//*[contains(@text,'SampleVideosMP4')]
${xp_tm_video_SampleVideo_360x240_30mb}    xpath=//*[contains(@text,'SampleVideo_360x240_30mb')]

${xp_tm_text_da_nam_duoc}                  xpath=//*[contains(@text,'Đã nắm được')]

${xp_tm_mo_bang_dien_thoai_text_chi_1_lan}    xpath=//*[contains(@text,'CHỈ MỘT LẦN')]
${xp_tm_sdt_bkav}                             xpath=//*[@text="024 3763 2552"]
${xp_tm_bkav_photo_sdt}                       id=com.android.dialer:id/bkav_photo
${xp_tm_quan_ly_cuoc_goi}                     xpath=//*[contains(@text,'Quản lý cuộc gọi')Ư

${xp_tm_cd_checkboc_chim_lac}                 xpath=(//*[contains(@resource-id,'com.android.permissioncontroller:id/radio_button')])[1]
${xp_tm_cd_checkboc_chrome}                   xpath=(//*[contains(@resource-id,'com.android.permissioncontroller:id/radio_button')])[2]
${xp_tm_cd_Ung_dung_trinh_duyet}              xpath=//*[contains(@text,'Ứng dụng trình duyệt')]
${xp_tm_cd_Ung_dung_mac_dinh}                 xpath=//*[contains(@text,'Ứng dụng mặc định')]
${xp_tm_cd_Ung_dung_thong_bao}                xpath=//*[contains(@text,'Ứng dụng và thông báo')]
${xp_tm_cd_nang_cao}                          xpath=//*[contains(@text,'Nâng cao')]

${xp_tm_chrome_nut_home}                      xpath=//android.widget.ImageButton[@content-desc="Trang chủ"]
${xp_tm_chim_lac_url_bar}                     id=android.bkav.bchrome:id/url_bar
${xp_tm_chrome_url_bar}                       id=com.android.chrome:id/url_bar
${xp_gdrm_chim_lac_nut_dong_tab_dang_mo}      xpath=//android.widget.ImageButton[@content-desc="tab đang mở"]

# Cac phan tu trong giao dien video
${xp_v_ten_video}                       id=com.bkav.video:id/bkav_player_overlay_title
${xp_v_thanh_seekbar}                   id=com.bkav.video:id/bkav_player_overlay_seekbar
${xp_v_nut_khoa}                        id=com.bkav.video:id/bkav_lock_overlay_button
${xp_v_nut_track}                       id=com.bkav.video:id/bkav_player_overlay_tracks
${xp_v_nut_overplay}                    id=com.bkav.video:id/bkav_player_overlay_play
${xp_v_nut_function}                    id=com.bkav.video:id/bkav_player_overlay_adv_function
${xp_v_nut_size}                        id=com.bkav.video:id/bkav_player_overlay_size
${xp_v_video_quay_man_hinh}             xpath=//*[contains(@text,'ScreenRecord_')]
${xp_v_video_quay_camera}               xpath=//*[contains(@text,'VID_')]
${xp_v_mo_video_file_mp4}               xpath=//*[contains(@text,'SampleVideosMP4')]
${xp_v_mo_video_file_3gp}               xpath=//*[contains(@text,'SampleVideos3GP')]
${xp_v_mo_video_file_mkv}               xpath=//*[contains(@text,'SampleVideosMKV')]
${xp_v_mo_video_file_flv}               xpath=//*[contains(@text,'SampleVideosFLV')]
${xp_v_video_thu_muc_dowload}           xpath=//*[contains(@text,'Download')]

${xp_v_video_t1_trong_thu_muc_vd}       xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[1]
${xp_v_video_t2_trong_thu_muc_vd}       xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[2]
${xp_v_video_t3_trong_thu_muc_vd}       xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[3]
${xp_v_video_t4_trong_thu_muc_vd}       xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[4]
${xp_v_video_t5_trong_thu_muc_vd}       xpath=(//*[contains(@resource-id,'com.bkav.video:id/bkav_ml_item_title')])[5]

${xp_v_nut_di_chuyen_len}               xpath=//android.widget.ImageButton[@content-desc="Di chuyển lên"]
${xp_v_nut_thu_gon}                     xpath=//android.widget.ImageButton[@content-desc="Thu gọn"]
${xp_v_sap_xep_theo}                    xpath=//android.widget.TextView[@content-desc="Sắp xếp theo…"]
${xp_v_cap_nhat_lai}                    xpath=//android.widget.TextView[@content-desc="Cập nhật lại"]
${xp_v_textview}                        id=com.bkav.video:id/actionbar_textview

${xp_v_nut_thong_tin}                   xpath=//*[contains(@text,'Thông tin')]
${xp_v_nut_xoa}                         xpath=//*[contains(@text,'Xóa')]
${xp_v_nut_chia_se}                     xpath=//*[contains(@text,'Chia sẻ')]
${xp_v_thong_tin_nut_play}              id=com.bkav.video:id/bkav_play
${xp_v_thong_tin_nut_xoa}               id=com.bkav.video:id/bkav_info_delete
${xp_v_text_tap_tin_video}              xpath=//*[contains(@text,'Tập tin Video')]
${xp_v_text_tap_tin_audio}              xpath=//*[contains(@text,'Tập tin Audio')]
${xp_v_text_ban_co_chac_khong}          xpath=//*[contains(@text,'Bạn có chắc không?')]
${xp_v_nut_huy}                         xpath=//*[contains(@text,'HỦY')]
${xp_v_nut_dong_y}                      xpath=//*[contains(@text,'ĐỒNG Ý')]

${xp_v_main_toolbar}                    id=com.bkav.video:id/bkav_main_toolbar
${xp_v_video1}                          xpath=//*[contains(@text,'SampleVideo_360x240_30mb')]

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================