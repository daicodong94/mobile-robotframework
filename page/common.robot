*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

LICH SU THAY DOI:
Nguoi thuc hien | YYYY/MM/DD | Noi dung thay doi
----------------|------------|--------------------------------------------------------------------
CuongNHd        | 2021/02/03 | Tao tap tin
----------------|------------|--------------------------------------------------------------------

*** Settings ***
Library     AppiumLibrary
Library     OperatingSystem
Library     ../custom_lib/ExtendedAppiumLibrary.py

Resource    config/mac_dinh.robot
Resource    launcher/launcher_common.robot

*** Variables ***
${config}         mac_dinh
${config_path}    ${CURDIR}${/}config${/}${config}.robot

*** Keywords ***
Kiem tra va nhap cau hinh
    [Documentation]    Kiem tra cau hinh duoc chon.
    ...    Neu cau hinh duoc chon khac voi cau hinh mac dinh, thi nhap cau hinh duoc chon
    Log To Console     ${\n}Kiem tra va nhap cau hinh
    Run Keyword If    '${config}'!='mac_dinh'    Import Resource    ${config_path}

Mo ung dung
    [Documentation]    Kiem tra va nhap cau hinh => Mo ung dung bang ham Open Application => Cho vai giay de ung dung on dinh
    [Arguments]    ${app_alias}    ${app_package}    ${app_activity}    ${thoi_gian_sleep}
    Kiem tra va nhap cau hinh
    Log To Console    Mo ung dung bang ham Open Application va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${app_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${app_package}    appActivity=${app_activity}
    Sleep    ${thoi_gian_sleep}

Mo ung dung va cho phep cap quyen
    [Documentation]    Kiem tra va nhap cau hinh => Mo ung dung bang ham Open Application
    ...    => Cho vai giay de ung dung on dinh => Bam nut CHO PHEP de cap quyen cho ung dung
    [Arguments]    ${app_alias}    ${app_package}    ${app_activity}    ${thoi_gian_sleep}
    Kiem tra va nhap cau hinh
    Log To Console    Mo ung dung bang ham Open Application va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${app_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${app_package}    appActivity=${app_activity}
    Sleep    ${thoi_gian_sleep}
    FOR    ${i}    IN RANGE    0    3
        ${kiem_tra_popup11}    AppiumLibrary.Get Matching Xpath Count    ${nut_cho_phep_hoa}
        Run Keyword If    ${kiem_tra_popup11}>0    Bam nut cho phep hoa
        Sleep    1
        ${kiem_tra_popup12}    AppiumLibrary.Get Matching Xpath Count    ${nut_cho_phep_hoa}
        ${kiem_tra_popup21}    AppiumLibrary.Get Matching Xpath Count    ${nut_cho_phep}
        Run Keyword If    ${kiem_tra_popup21}>0    Bam nut cho phep
        Sleep    ${thoi_gian_sleep}
        ${kiem_tra_popup22}    AppiumLibrary.Get Matching Xpath Count    ${nut_cho_phep}
        Exit For Loop If    ${kiem_tra_popup12}==0 and ${kiem_tra_popup22}==0
    END

Cho phep cap quyen cho ung dung
    [Documentation]    Cho phep cap quyen cho ung dung
    ${kiem_tra_popup1}    AppiumLibrary.Get Matching Xpath Count    ${nut_cho_phep_hoa}
    Run Keyword If    ${kiem_tra_popup1}>0    Bam nut cho phep hoa
    Sleep    1
    ${kiem_tra_popup2}    AppiumLibrary.Get Matching Xpath Count    ${nut_cho_phep}
    Run Keyword If    ${kiem_tra_popup2}>0    Bam nut cho phep
    Sleep    2

Bam nut cho phep hoa
    Log To Console    Bam nut cho phep cap quyen cho ung dung
    ${nut_cho_phep_hoa1}    Set Variable    xpath=(${nut_cho_phep_hoa})[1]
    AppiumLibrary.Click Element    ${nut_cho_phep_hoa1}

Bam nut cho phep
    Log To Console    Bam nut cho phep cap quyen cho ung dung
    AppiumLibrary.Click Element    ${nut_cho_phep}

Tu choi cap quyen cho ung dung
    Log To Console    Tu choi cap quyen cho ung dung
    ${kiem_tra_popup1}    AppiumLibrary.Get Matching Xpath Count    ${nut_tu_choi_hoa}
    Run Keyword If    ${kiem_tra_popup1}>0    AppiumLibrary.Click Element    ${nut_tu_choi_hoa}
    Sleep    1
    ${kiem_tra_popup2}    AppiumLibrary.Get Matching Xpath Count    ${nut_tu_choi}
    Run Keyword If    ${kiem_tra_popup2}>0    AppiumLibrary.Click Element    ${nut_tu_choi}
    Sleep    5

Bam nut huy thay doi hinh nen
    Log To Console    Bam nut huy thay doi hinh nen
    ${kiem_tra_popup}    AppiumLibrary.Get Matching Xpath Count    ${nut_huy_hoa}
    Run Keyword If    ${kiem_tra_popup}>0    AppiumLibrary.Click Element    ${nut_huy_hoa}
    Sleep    1

Bam vao man hinh
    [Documentation]    Bam vao man hinh:
    ...                - Ben ngoai modal de tat modal
    ...                - Xem video de hien menu
    AppiumLibrary.Click Element At Coordinates    500    500
    Sleep    1

Mo Launcher
    Kiem tra va nhap cau hinh
    Log To Console     Mo Launcher va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${launcher_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${launcher_package}    appActivity=${launcher_activity}
    Sleep    2

# Cac ham vuot man hinh
Vuot sang man hinh ben tren
    Log To Console      Vuot sang man hinh ben tren
    AppiumLibrary.Swipe By Percent    50    10    50    90
    Sleep    2

Vuot sang man hinh ben duoi
    Log To Console      Vuot sang man hinh ben duoi
    AppiumLibrary.Swipe By Percent    50    90    50    10
    Sleep    2

Vuot sang man hinh ben trai
    Log To Console      Vuot sang man hinh ben trai
    AppiumLibrary.Swipe By Percent    10    50    90    50
    Sleep    2

Vuot sang man hinh ben phai
    Log To Console      Vuot sang man hinh ben phai
    AppiumLibrary.Swipe By Percent    90    50    10    50
    Sleep    2

Vuot back ve giao dien truoc
    Log To Console      Vuot nhanh tu canh trai hoac canh phai man hinh vao trong de back ve giao dien truoc
    AppiumLibrary.Swipe By Percent    0    50    10    50
    Sleep    2

Vuot quay ve man hinh Home
    Log To Console      Vuot nhanh tu day man hinh len tren de quay ve man hinh Home
    AppiumLibrary.Swipe By Percent    50    99    50    10
    Sleep    2

Vuot mo bang dieu khien
    Log To Console      Vuot tu canh trai hoac canh phai man hinh vao giua va giu de mo bang dieu khien
    AppiumLibrary.Swipe By Percent    99    70    1    70    duration=3000
    Sleep    2

Vuot mo bang thong bao
    Log To Console      Vuot tu tren dinh man hinh xuong de mo bang thong bao
    AppiumLibrary.Swipe By Percent    50    1    50    80
    Sleep    2

Vuot dong bang thong bao
    Log To Console      Vuot tu giua len dinh man hinh de dong bang thong bao
    AppiumLibrary.Swipe By Percent    50    60    50    1
    Sleep    2

Bam nut quay lai man hinh truoc do
    Log To Console      Bam nut quay lai man hinh truoc do
    AppiumLibrary.Click Element    ${nut_quay_lai}
    Sleep    2

Thuc hien doi vi tri space app may tinh len dau dong
    [Documentation]    Thuc hien doi vi tri space len dau dong
    AppiumLibrary.Click Element At Coordinates    28    313

Thuc hien doi vi tri space app may tinh cuoi dong
    [Documentation]    Thuc hien doi vi tri space len dau dong
    AppiumLibrary.Click Element At Coordinates    1029    306

Dong toan bo ung dung va ket thuc phien kiem thu
    Log To Console    Dong toan bo ung dung va ket thuc phien kiem thu
    Dong toan bo ung dung trong da nhiem
    AppiumLibrary.Close All Applications
    Run    TIMEOUT /T 10
    Run    adb -s ${udid} reconnect
    Run    TIMEOUT /T 10
    Run    adb devices
    Run    TIMEOUT /T 10

Dong ung dung va ket thuc phien kiem thu
    Log To Console    Dong ung dung va ket thuc phien kiem thu
    AppiumLibrary.Close All Applications
    Run    TIMEOUT /T 10
    Run    adb -s ${udid} reconnect
    Run    TIMEOUT /T 10
    Run    adb devices
    Run    TIMEOUT /T 10

Lam moi va ping ket noi voi dien thoai
    Log To Console    Lam moi va ping ket noi voi dien thoai
    Run    TIMEOUT /T 20
    Run    adb -s ${udid} reconnect
    Run    TIMEOUT /T 20
    Run    adb devices
    Run    TIMEOUT /T 20

Mo da nhiem
    Log To Console     Vuot tu day man hinh len tren va giu de mo da nhiem
    AppiumLibrary.Swipe By Percent    50    99    50    60    duration=3000
    Sleep    2

Dong toan bo ung dung trong da nhiem
    [Documentation]    Vuot tu day man hinh len tren va giu de mo da nhiem. Neu co nut dong thi bam nut dong toan bo ung dung trong da nhiem
    Mo da nhiem
    ${kiem_tra_nut}    AppiumLibrary.Get Matching Xpath Count    ${nut_dong_toan_bo_app}
    Run Keyword If    ${kiem_tra_nut}>0    Bam nut dong toan bo ung dung trong da nhiem
    ...    ELSE    Vuot back ve giao dien truoc

Bam nut dong toan bo ung dung trong da nhiem
    Log To Console    Bam nut dong toan bo ung dung trong da nhiem
    AppiumLibrary.Click Element    ${nut_dong_toan_bo_app}
    Sleep    2

Bam mo ung dung trai trong 2 ung dung tren da nhiem
    Log To Console    Bam mo ung dung trai trong 3 ung dung tren da nhiem
    AppiumLibrary.Click Element    ${dn_2_ung_dung_trai}
    Sleep    2

Bam mo ung dung phai trong 2 ung dung tren da nhiem
    Log To Console    Bam mo ung dung trai trong 3 ung dung tren da nhiem
    AppiumLibrary.Click Element    ${dn_2_ung_dung_phai}
    Sleep    2

Bam mo ung dung trai trong 3 ung dung tren da nhiem
    Log To Console    Bam mo ung dung trai trong 3 ung dung tren da nhiem
    AppiumLibrary.Click Element    ${dn_3_ung_dung_trai}
    Sleep    2

Bam mo ung dung giua trong 3 ung dung tren da nhiem
    Log To Console    Bam mo ung dung trai trong 3 ung dung tren da nhiem
    AppiumLibrary.Click Element    ${dn_3_ung_dung_giua}
    Sleep    2

Bam mo ung dung phai trong 3 ung dung tren da nhiem
    Log To Console    Bam mo ung dung trai trong 3 ung dung tren da nhiem
    AppiumLibrary.Click Element    ${dn_3_ung_dung_phai}
    Sleep    2

Mang di dong = 1, data = 0, wifi = 0, may bay = 0
    [Documentation]    Thiet lap trang thai ket noi mang cua dien thoai Android.
    ...                Mang di dong    = Bat.
    ...                Du lieu di dong = Tat.
    ...                Wifi            = Tat.
    ...                Che do may bay  = Tat
    Set Network Connection Status    0
    Sleep    10
    ${trang_thai_ket_noi}=    Get Network Connection Status
    Should Be Equal As Integers    ${trang_thai_ket_noi}    4

Mang di dong = 0, data = 0, wifi = 0, may bay = 1
    [Documentation]    Thiet lap trang thai ket noi mang cua dien thoai Android.
    ...                Mang di dong    = Tat.
    ...                Du lieu di dong = Tat.
    ...                Wifi            = Tat.
    ...                Che do may bay  = Bat
    Run Keyword And Continue On Failure    Set Network Connection Status    1
    Sleep    30
    ${trang_thai_ket_noi}=    Get Network Connection Status
    Should Be Equal As Integers    ${trang_thai_ket_noi}    1

Mang di dong = 1, data = 0, wifi = 1, may bay = 0
    [Documentation]    Thiet lap trang thai ket noi mang cua dien thoai Android.
    ...                Mang di dong    = Bat.
    ...                Du lieu di dong = Tat.
    ...                Wifi            = Bat.
    ...                Che do may bay  = Tat
    Set Network Connection Status    2
    Sleep    30
    ${trang_thai_ket_noi}=    Get Network Connection Status
    Should Be Equal As Integers    ${trang_thai_ket_noi}    6

Mang di dong = 1, data = 1, wifi = 0, may bay = 0
    [Documentation]    Thiet lap trang thai ket noi mang cua dien thoai Android.
    ...                Mang di dong    = Bat.
    ...                Du lieu di dong = Bat.
    ...                Wifi            = Tat.
    ...                Che do may bay  = Tat
    Set Network Connection Status    4
    Sleep    30
    ${trang_thai_ket_noi}=    Get Network Connection Status
    Should Be Equal As Integers    ${trang_thai_ket_noi}    4

Mang di dong = 1, data = 1, wifi = 1, may bay = 0
    [Documentation]    Thiet lap trang thai ket noi mang cua dien thoai Android.
    ...                Mang di dong    = Bat.
    ...                Du lieu di dong = Bat.
    ...                Wifi            = Bat.
    ...                Che do may bay  = Tat
    Set Network Connection Status    6
    Sleep    30
    ${trang_thai_ket_noi}=    Get Network Connection Status
    Should Be Equal As Integers    ${trang_thai_ket_noi}    6

Chup anh man hinh va so sanh voi anh mau
    [Documentation]    Chup anh man hinh va so sanh voi anh mau
    [Arguments]    ${ten_ung_dung}    ${ten_file_anh_mau_khong_co_duoi_file}    ${nguong_so_sanh_theo_phan_tram}
    AppiumLibrary.Capture Page Screenshot    ${ten_ung_dung}_${ten_file_anh_mau_khong_co_duoi_file}.png
    Sleep    10
    Compare Two Images    ${CURDIR}${/}${ten_ung_dung}${/}${do_phan_giai}${/}${ten_file_anh_mau_khong_co_duoi_file}.png    ${OUTPUT_DIR}${/}${ten_ung_dung}_${ten_file_anh_mau_khong_co_duoi_file}.png    ${OUTPUT_DIR}${/}${ten_ung_dung}_${ten_file_anh_mau_khong_co_duoi_file}_diff.png    ${nguong_so_sanh_theo_phan_tram}
    Sleep    5

Mo May tinh
    [Documentation]    Mo ung dung may tinh va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console    Mo ung dung may tinh va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${mt_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${mt_package}    appActivity=${mt_activity}
    Sleep    2

Mo May anh
    [Documentation]    Mo ung dung may anh va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console    Mo ung dung may anh va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${cam_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${cam_package}    appActivity=${cam_activity}
    Sleep    2

Mo Bo suu tap
    [Documentation]    Mo ung dung Bo sua tap va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console    Mo ung dung  Bo sua tap va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${bst_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${bst_package}    appActivity=${bst_activity}
    Sleep    3

Xem duoc anh
    Log To Console    Xem duoc anh
    [Tags]     ota_tc_002
    AppiumLibrary.Click Element    ${bst_xem_anh}

Mo Chim lac
    [Documentation]    Mo ung dung Chim lac va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console    Mo ung dung Chim lac va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${cl_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${cl_package}    appActivity=${cl_activity}
    Sleep    2

Mo Dong ho
    [Documentation]    Mo ung dung Dong ho va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console    Mo ung dung Dong ho va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${dh_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${dh_package}    appActivity=${dh_activity}
    Sleep    2
    Cho phep cap quyen cho ung dung

Mo tra tu nhanh
    [Documentation]    Mo ung dung tra tu nhanh va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console    Mo ung dung tra tu nhanh va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${eDict_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${eDict_package}    appActivity=${eDict_activity}
    Sleep    2


Mo Quan ly file
    [Documentation]    Mo ung dung Quan ly file va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console    Mo ung dung Quan ly file  va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${qlf_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${qlf_package}    appActivity=${qlf_activity}
    Sleep    2

Mo Suc khoe
    [Documentation]    Mo ung dung Suc khoe va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console    Mo ung dung Suc khoe va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${sk_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${sk_package}    appActivity=${sk_activity}
    Sleep    2

Mo Nghe nhac
    [Documentation]    Mo ung dung Nghe nhac va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console     Mo ung dung Nghe nhac va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${nn_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${nn_package}    appActivity=${nn_activity}
    Sleep    2

Mo Ghi chep
    [Documentation]    Mo ung dung Ghi chep va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console     Mo ung dung  Ghi chep va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${gc_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${gc_package}    appActivity=${gc_activity}
    Sleep    2

Mo Ghi am
    [Documentation]    Mo ung dung Ghi am va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console     Mo ung dung Ghi am va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${ga_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${ga_package}    appActivity=${ga_activity}
    Sleep    2

Mo Nhac viec
    [Documentation]    Mo ung dung Nhac viec va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console     Mo ung dung Nhac viec va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${nv_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${nv_package}    appActivity=${nv_activity}
    Sleep    2

Mo Xem phim
    [Documentation]    Mo ung dung Xem phim va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console     Mo ung dung Xem phim va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${xp_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${xp_package}    appActivity=${xp_activity}
    Sleep    2
    Cho phep cap quyen cho ung dung

Mo Thoi tiet
    [Documentation]    Mo ung dung Thoi tiet va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console     Mo ung dung Thoi tiet va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${tt_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${tt_package}    appActivity=${tt_activity}
    Sleep    2

Mo Btalk
    [Documentation]    Mo ung dung Tin nhan va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console     Mo ung dung Tin nhan va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${btalk_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${btalk_package}   appActivity=${btalk_activity}   # appWaitDuration=30s    appWaitForLaunch=false
    Sleep    2

Mo esim
    [Documentation]    Mo ung dung esim va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${esim_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${esim_package}   appActivity=${esim_activity}
    Sleep    2

Mo Ho tro
    [Documentation]    Mo ung dung Ho tro va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console    Mo ung dung Ho tro va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${ht_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${ht_package}   appActivity=${ht_activity}
    Sleep    2

Mo Cai dat
    [Documentation]    Mo ung dung Cai dat va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console    Mo ung dung Cai dat va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${cd_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${cd_package}   appActivity=${cd_activity}
    Sleep    2

Mo eDict
    [Documentation]    Mo ung dung Cai dat va cho vai giay de ung dung on dinh
    Kiem tra va nhap cau hinh
    Log To Console    Mo ung dung Cai dat va cho vai giay de ung dung on dinh
    Open Application    ${appium_url}    adbPort=${adb_port}    systemPort=${system_port}    alias=${cd_alias}    automationName=UiAutomator2    platformName=${platform_name}    platformVersion=${platform_version}    udid=${udid}    appPackage=${eDict_package}   appActivity=${eDict_activity}
    Sleep    2

Mo khoa voi mat khau 8888
    Log To Console    Mo khoa voi mat khau 8888
    AppiumLibrary.Swipe By Percent    50    99    50    60
    Sleep    2
    AppiumLibrary.Click Element    ${nut_8}
    Sleep    2
    AppiumLibrary.Click Element    ${nut_8}
    Sleep    2
    AppiumLibrary.Click Element    ${nut_8}
    Sleep    2
    AppiumLibrary.Click Element    ${nut_8}
    Sleep    5

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================