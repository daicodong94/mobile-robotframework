*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
An nut menu trai
    [Documentation]    An nut menu trai
    AppiumLibrary.Click Element    ${nut_menu_trai}

Bam nut dang xu li
    [Documentation]    Bam nut dang xu ly
    An nut menu trai
    AppiumLibrary.Click Element    ${Text_dang_xu_li}

Bam nut luu tru
    [Documentation]    Bam nut luu tru
    An nut menu trai
    AppiumLibrary.Click Element    ${Text_luu_tru}
    Sleep    2

Bam nut thung rac
    [Documentation]    Bam nut thung rac
    An nut menu trai
    AppiumLibrary.Click Element    ${Text_thung_rac}

Bam nut tuy chon khac
    [Documentation]    Bam nut tuy chon khac
    AppiumLibrary.Click Element    ${nut_tuy_chon_khac}
    
Bam nut add
    [Documentation]    Bam nut add
    Click Element    ${btn_add}
    Sleep    2    
    
Bam nut add ghi chep
    [Documentation]    Them ghi chep
    Click Element    ${Text_add_ghi_chep}
    Sleep    2    
    
Bam nut add checklist
    [Documentation]    Them checklist
    Click Element    ${Text_add_checklist}
    Sleep    2    
    
Bam nut add ghi am
    [Documentation]    Them file ghi am
    Click Element    ${Text_add_ghi_am}
    Sleep    2   
     
Bam nut add anh
    [Documentation]    Them file anh
    Click Element    ${Text_add_anh}
    Sleep    2    
    
Bam vao chup man hinh
    [Documentation]    Mo giao dien anh chup man hinh
    Click Element    ${chup_man_hinh}
    Sleep    2    
    
Chon anh bat ky
    [Documentation]    Chon mot anh bat ky
    Click Element    ${Chon_anh_bat_ky}
    Sleep    2    

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================