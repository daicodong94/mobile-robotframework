*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Bam mo xem phim tu man hinh da nhiem
    Log To Console    Bam mo xem phim tu man hinh da nhiem
    Mo da nhiem
    ${kiem_tra}    AppiumLibrary.Get Matching Xpath Count    ${nut_dong_toan_bo_app}
    Run Keyword If    ${kiem_tra}>0    AppiumLibrary.Click Element    ${xp_tm_man_da_nhiem}
    Sleep    2
    Log To Console    Check mo thanh cong ung dung xem phim
    AppiumLibrary.Page Should Contain Element    ${xp_tm_nut_tim_kiem}
    Page Should Contain Element    ${xp_v_textview}    
    Sleep    2
    
Chon video mp4
    Log To Console    Chon video mp4
    AppiumLibrary.Click Element    ${xp_v_mo_video_file_mp4}
    Sleep    2
    
Chon da nam duoc tai huong dan su dung khi xem video
    [Documentation]    An da nam duoc
    AppiumLibrary.Click Element    ${xp_tm_text_da_nam_duoc}
    
Mo app xem phim va mo xem video
    [Documentation]    Mo app va click vao video
    Mo Xem phim tu Launcher
    Click Element    ${xp_v_mo_video_file_mp4}
    Sleep    2    
    Click Element    ${xp_v_sap_xep_theo}
    Sleep    2    
    ${trang_thai}    Run Keyword And Return Status    AppiumLibrary.Page Should Contain Element    ${xp_tm_sap_xep_theo_ngay_moi_nhat} 
    Run Keyword If    ${trang_thai}==True    AppiumLibrary.Click Element    ${xp_tm_sap_xep_theo_ngay_moi_nhat}    
    ...    ELSE    Vuot back ve giao dien truoc 
    Sleep    2    
    Click Element    ${xp_v_video_t5_trong_thu_muc_vd}
    Sleep    2
    ${hien_thi}    Run Keyword And Return Status    Page Should Contain Element    ${xp_tm_text_da_nam_duoc}   
    Run Keyword If    ${hien_thi}==True    Chon da nam duoc tai huong dan su dung khi xem video    
    ...    ELSE    Sleep    1    
    
Vuot sang phai man launcher va kiem tra hien thi app xem phim
    Log To Console    Vuot sang phai man launcher va kiem tra hien thi app xem phim    
    Vuot sang man hinh ben phai
    Page Should Contain Element    ${app_xem_phim}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================
