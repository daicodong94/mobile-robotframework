*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Chon video download
    [Documentation]    Chon video download
    Click Element    ${xp_v_video_thu_muc_dowload}
    Sleep    2
    # Click Element    ${xp_v_video_t1_trong_thu_muc_vd}
    # Sleep    2

Chon video quay tu camera
    [Documentation]    Chon video quay tu camera
    Click Element    ${xp_v_video_quay_camera}
    Sleep    2

Chon video quay man hinh
    [Documentation]    Chon video quay man hinh
    Click Element    ${xp_v_video_quay_man_hinh}
    Sleep    2

Chon video copy tu ben ngoai
    [Documentation]    Chon video copy tu ben ngoai
    Click Element    ${xp_v_mo_video_file_mp4}
    Sleep    2
    Click Element    ${xp_v_video_t1_trong_thu_muc_vd}

Vuot man tu duoi len tren
    [Documentation]    Vuot man tu duoi len tren
    Swipe By Percent    60    70    60    30
    Sleep    2

Vuot man tu duoi len tren va chon thu muc video mp4
    [Documentation]    Vuot man tu duoi len tren va chon thu muc video mp4
    Swipe By Percent    60    70    60    30
    Sleep    2
    Click Element    ${xp_v_mo_video_file_mp4}

Vuot man tu duoi len tren va chon thu muc video 3gp
    [Documentation]    Vuot man tu duoi len tren va chon thu muc video mp4
    Swipe By Percent    60    70    60    30
    Sleep    2
    Click Element    ${xp_v_mo_video_file_3gp}

Vuot man tu duoi len tren va chon thu muc video mkv
    [Documentation]    Vuot man tu duoi len tren va chon thu muc video mp4
    Swipe By Percent    60    70    60    30
    Sleep    2
    Click Element    ${xp_v_mo_video_file_mkv}

Vuot man tu duoi len tren va chon thu muc video flv
    [Documentation]    Vuot man tu duoi len tren va chon thu muc video mp4
    Swipe By Percent    60    70    60    30
    Sleep    2
    Click Element    ${xp_v_mo_video_file_flv}

Chon thu muc video mp4
    ${kiem_tra}    Run Keyword And Return Status    Click Element    ${xp_v_mo_video_file_mp4}
    Run Keyword If    ${kiem_tra}==False        Vuot man tu duoi len tren va chon thu muc video mp4
    Sleep    2

Nhan chon nut [<] mui back - thu gon
    [Documentation]    Nhan chon nut [<] mui ten back
    Click Element    ${xp_v_nut_thu_gon}
    Sleep    2

Nhan chon nut [<] mui back - di chuyen len
    [Documentation]    Nhan chon nut [<] mui ten back
    Click Element    ${xp_v_nut_di_chuyen_len}
    Sleep    2

Nhan icon Sap xep theo trong danh sach video
    [Documentation]    Nhan icon Sap xep theo... trong danh sach video
    Click Element    ${xp_v_sap_xep_theo}
    Sleep    1

Chon giu video dau tien
    [Documentation]    Chon giu video dau tien
    Long Press    ${xp_tm_ten_vd_stt_1}
    Sleep    1

Chon xem thong tin cua video
    [Documentation]    Chon xem thong tin cua video
    Click Element    ${xp_v_nut_thong_tin}
    Sleep    1

Chon nut xoa Video
    [Documentation]    Chon xoa Video
    Click Element    ${xp_v_nut_xoa}

Chon nut chia se
    [Documentation]    Chon nut chia se
    Click Element    ${xp_v_nut_chia_se}
    Sleep    3

Vuot mo rong man chia se tu duoi len tren
    [Documentation]    Vuot mo rong man chia se tu duoi len tren
    Swipe By Percent    60    80    60    20

Vuot thu nho man chia se tu tren xuong
    [Documentation]    Vuot thu nho man chia se tu tren xuong
    Swipe By Percent    60    10    60    50
Chon nut play trong thong tin video
    [Documentation]    Chon nut play trong thong tin video
    Click Element    ${xp_v_thong_tin_nut_play}
    Sleep    2

Cham icon xoa video
    [Documentation]    Cham icon xoa video
    Click Element    ${xp_v_thong_tin_nut_xoa}
    Sleep    1

Chon nut HUY de dong popup
    [Documentation]       Chon nut HUY de dong popup
    Click Element    ${xp_v_nut_huy}
    Sleep    1

Chon nut Dong y de xoa video
    [Documentation]    Chon nut Dong y de xoa video
    Click Element    ${xp_v_nut_dong_y}
    Sleep    1

Chon giu video quay man hinh
    [Documentation]    Chon giu video quay man hinh
    Long Press    ${xp_v_video_quay_man_hinh}
    Sleep    1

Chon giu video quay bang camera
    [Documentation]    Chon giu video quay bang camera
    ${kiem_tra}    Run Keyword And Return Status    Page Should Contain Element  ${xp_v_video_quay_camera}
    Run Keyword If    ${kiem_tra}==True    Long Press    ${xp_v_video_quay_camera}
    Sleep    1

Cham giu icon Back - icon mui ten quay
    [Documentation]    Cham giu icon Back - icon mui ten quay
    Long Press    ${xp_v_cap_nhat_lai}

Cham toa do dung phat video tren Chim lac
    [Documentation]    Cham toa do dung phat video tren Chim lac
    Click Element At Coordinates    503    961
    Sleep    1

Chon di toi duong dan
    [Documentation]    Chon di toi duong dan
    Click Element    ${cl_url_bar}

Download video tu chim lac
    [Documentation]    Download video tu chim lac
    Click Element    ${xp_v_chim_lac_nut_hien_thi_them}
    Sleep    1
    Click Element    ${xp_v_nut_tai_video_xuong}
    Sleep    2

Download lan 2 video tu Chim lac
    [Documentation]    Download lan 2 video tu Chim lac
    Click Element    ${xp_v_chim_lac_nut_hien_thi_them}
    Sleep    1
    Click Element    ${xp_v_nut_tai_video_xuong}
    Sleep    1
    # Click Element    ${xp_v_chim_lac_nut_tai_xuong}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================