*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Cham icon tim kiem hinh kinh lup
    [Documentation]    Nhan chon tim kiem
    Click Element    ${xp_tm_nut_tim_kiem}
    Sleep    2

Nhap text ten phim hshfj
    [Documentation]    Nhap text ten phim
    Click Element    ${xp_tm_text_tim_kiem_phim}
    Clear Text    ${xp_tm_text_tim_kiem_phim}
    Input Text    ${xp_tm_text_tim_kiem_phim}    hshfj
    Page Should Contain Element    ${xp_tm_text_hshfj}

Cham icon xem video gan day hinh mui ten quay vong
    [Documentation]    Nhan chon icon xem video gan day
    Click Element    ${xp_tm_xem_video_gan_day}
    Sleep    1

Cham icon ba cham
    [Documentation]    Cham icon ...
    Click Element At Coordinates    991    131
    Sleep    2

Cham icon xoa [X] de xoa text
    [Documentation]    Cham icon xoa [X] de xoa text
    Click Element    ${xp_tm_nut_xoa_truy_van}

Cham nut [<-] de tro ve giao dien danh sach thu muc
    [Documentation]    Cham nut [<-] de tro ve giao dien danh sach thu muc
    Click Element    ${xp_tm_nut_thu_gon}
    Sleep    2

Nhan chon Sap xep theo
    [Documentation]    Nhan chon Sap xep theo...
    Click Element    ${xp_tm_nut_sap_xep_theo}
    Sleep    1

Nhan chon Sap xep theo - ten
    [Documentation]    Nhan chon Sap xep theo - ten
    Click Element    ${xp_tm_sap_xep_theo_ten}

Nhan chon Sap xep theo - ten az
    [Documentation]    Nhan chon Sap xep theo - ten
    Click Element    ${xp_tm_sap_xep_theo_ten_az}
    Sleep    1

Nhan chon Sap xep theo - do dai
    [Documentation]    Nhan chon Sap xep theo - do dai
    Click Element    ${xp_tm_sap_xep_theo_do_dai}
    Sleep    1

Nhan chon Sap xep theo - do dai (giam dan)
    [Documentation]    Nhan chon Sap xep theo - do dai (giam dan)
    Click Element    ${xp_tm_sap_xep_theo_do_dai_giam_dan}
    Sleep    1

Nhan chon Sap xep theo - ngay cu nhat
    [Documentation]    Nhan chon Sap xep theo - ngay cu nhat
    Click Element    ${xp_tm_sap_xep_theo_ngay_cu_nhat}
    Sleep    1

Nhan chon Sap xep theo - ngay moi nhat
    [Documentation]    Nhan chon Sap xep theo - ngay moi nhat
    Click Element    ${xp_tm_sap_xep_theo_ngay_moi_nhat}
    Sleep    1

Nhan chon Cap nhat lai
    [Documentation]    Nhan chon Cap nhat lai
    Click Element    ${xp_tm_nut_cap_nhat_lai}

Nhan chon Thong tin
    [Documentation]    Nhan chon Thong tin
    Click Element    ${xp_tm_nut_thong_tin}
    Sleep    1
    Page Should Contain Element    ${xp_tm_text_dien_thoai}
    Page Should Contain Element    ${xp_tm_text_hop_thu}
    Page Should Contain Element    ${xp_tm_text_trang_chu}
    Page Should Contain Element    ${xp_tm_text_bkav@bkav.com.vn}
    Page Should Contain Element    ${xp_tm_text_http://www.bkav.com.vn}
    Sleep    2

Nhan chon so dien thoai 024 3763 2552
    [Documentation]    Nhan chon sp dien thoai 024 3763 2552
    Click Element    ${xp_tm_text_so_dien_thoai}
    Sleep    2

Nhan chon hop thu bkav
    [Documentation]    Nhan chon hop thu http://www.bkav.com.vn
    Click Element    ${xp_tm_text_bkav@bkav.com.vn}
    Sleep    5

Nhan chon vao link Trang chu Bkav
    [Documentation]    Nhan chon vao link Trang chu Bkav
    Click Element    ${xp_tm_text_http://www.bkav.com.vn}
    Sleep    7

Chon mo bang app Dien thoai
    [Documentation]    Chon mo bang app dien thoai
    Click Element    ${xp_tm_app_dien_thoai}
    Sleep    1
    Click Element    ${xp_tm_nut_chi_mot_lan}
    Sleep    2

Chon nut goi de thuc hien cuoc goi toi so 024 3763 2552
    [Documentation]    Chon nut goi de thuc hien cuoc goi toi so 024 3763 2552
    Click Element    ${xp_tm_goi_dien_nut_quay_so}

Bam mo xem phim tu man hinh da nhiem
    [Documentation]    Vuot tu day man hinh len tren va giu de mo da nhiem. Neu co nut dong thi bam nut dong toan bo ung dung trong da nhiem
    Log To Console    Bam nut dong toan bo ung dung trong da nhiem
    AppiumLibrary.Swipe By Percent    50    99    50    60    duration=3000
    ${trang_thai}    Run Keyword And Return Status    AppiumLibrary.Wait Until Page Contains Element    ${nut_dong_toan_bo_app}    timeout=5
    Run Keyword If    ${trang_thai}==True    AppiumLibrary.Click Element    ${xp_tm_man_da_nhiem}
    Sleep    2

Chon danh sach video SampleVideosMP4
    [Documentation]    Chon danh sach video SampleVideosMP4
    Click Element    ${xp_tm_video_SampleVideosMP4}
    Sleep    1

Chon video dau tien trong danh sach video
    [Documentation]    Chon video dau tien trong danh sach video
    Click Element    ${xp_v_video_t1_trong_thu_muc_vd}
    Sleep    1

Cham Da nam duoc de tiep tuc xem video
    [Documentation]    Cham Da nam duoc de tiep tuc xem video
    Click Element    ${xp_tm_text_da_nam_duoc}
    Sleep    2

Chon Chi mot lan
    [Documentation]    Chon chi mot lan mo bang Dien thoai
    Click Element    ${xp_tm_mo_bang_dien_thoai_text_chi_1_lan}
    Sleep    2

Cham nut Goi de thuc hien cuoc goi di
    [Documentation]    Cham nut Goi de thuc hien cuoc goi di
    Click Element    ${xp_tm_goi_dien_nut_quay_so}
    Sleep    2

Cham nut Ket thuc cuoc goi de huy
    [Documentation]    Cham nut Ket thuc cuoc goi de huy
    Click Element    ${dt_ket_thuc_cuoc_goi}
    Sleep    2

Chon Quan ly cuoc goi
    [Documentation]    Chon Quan ly cuoc goi
    Click Element    ${xp_tm_quan_ly_cuoc_goi}
    Sleep    1

Chon Ung dung va thong bao
    [Documentation]    Chon Ung dung va thong bao
    Click Element    ${xp_tm_cd_Ung_dung_thong_bao}
    Sleep    1

Chon Nang cao trong ung dung va thong bao
    [Documentation]    Chon Nang cao trong ung dung va thong bao
    Click Element    ${xp_tm_cd_nang_cao}
    Sleep    1

Chon Ung dung mac dinh
    [Documentation]   Chon Ung dung mac dinh
    Click Element    ${xp_tm_cd_Ung_dung_mac_dinh}
    Sleep    1

Chon Ung dung trinh duyet
    [Documentation]      Chon Ung dung trinh duyet
    Click Element    ${xp_tm_cd_Ung_dung_trinh_duyet}
    Sleep    1

Chon checkbox mo bang Chim lac
    [Documentation]      Chon checkbox mo bang Chim lac
    Click Element    ${xp_tm_cd_checkboc_chim_lac}
    Sleep    1

Chon checkbox mo bang Chrome
    [Documentation]      Chon checkbox mo bang Chrome
    Click Element    ${xp_tm_cd_checkboc_chrome}
    Sleep    1

Cham giu icon kinh lup
    [Documentation]    Cham giu icon kinh lup
    Long Press    ${xp_tm_nut_tim_kiem}

Cham giu icon video xem gan day
    [Documentation]    Cham giu icon video xem gan day
    Long Press    ${xp_tm_xem_video_gan_day}

Vuot sang trai launcher va click app cai dat
    [Documentation]    Vuot sang trai launcher va click app cai dat
    Vuot sang man hinh ben phai
    Sleep    2
    Click Element    ${app_cai_dat}
    Sleep    2

Vuot sang phai tro ve giao dien truoc do
    [Documentation]    Vuot sang phai tro ve giao dien truoc do
    Swipe By Percent    0    50    60    50
    Sleep    1

Click Chi mot lan khi hien popup
    [Documentation]    Click Chi mot lan khi hien popup
    ${kiem_tra}    Run Keyword And Return Status    Page Should Contain Element   ${xp_tm_nut_chi_mot_lan}
    Sleep    2
    Run Keyword If    ${kiem_tra}== True    Click Element    ${xp_tm_nut_chi_mot_lan}
    Sleep    2

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================