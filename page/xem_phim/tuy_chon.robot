*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot

*** Keywords ***
Click vao tuy chon
    Click Element    ${nut_track}
    Sleep    2

Click vao bo danh sach video
    Click Element   ${xp_tm_ten_vd_stt_2}
    Sleep    2

Click vao bo danh sach video1
    Click Element   ${xp_tm_ten_vd_stt_1}
    Sleep    2

Click vao video cuoi danh sach
    Click Element   ${xp_ds_video6}
    Sleep    2

Click vao video cuoi danh sach1
    Click Element   ${xp_tc_ten_vd_stt_6}
    Sleep    2

Click vao video 1
    Click Element   ${xp_tc_video_stt4}
    Sleep    2

Click vao video 5
    Click Element   ${xp_tc_video_stt5}
    Sleep    2

Kiem tra giao dien tuy chon
    # Capture Page Screenshot    b86_kc_d_ttdh_d_d_tuy_chon_main.png
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    ${bphone}_kc_d_ttdh_d_d_tuy_chon_main    4

Kiem tra giao dien dong ho kim thoi gian cho
    # Capture Page Screenshot    b86_kc_d_ttdh_d_d_dong_ho_kim.png
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    ${bphone}_kc_d_ttdh_d_d_dong_ho_kim    4

Kiem tra giao dien dong ho so thoi gian cho
    # Capture Page Screenshot    b86_kc_d_ttdh_d_d_dong_ho_so.png
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    ${bphone}_kc_d_ttdh_d_d_dong_ho_so    4

Kiem tra giao dien pop up phu de
    # Capture Page Screenshot    b86_kc_d_ttdh_d_d_pop_up_phu_de.png
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    ${bphone}_kc_d_ttdh_d_d_pop_up_phu_de    4

Kiem tra giao dien pop up tua den vi tri
    # Capture Page Screenshot    b86_kc_d_ttdh_d_d_pop_up_tua_den_vi_tri.png
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    ${bphone}_kc_d_ttdh_d_d_pop_up_tua_den_vi_tri    4

Kiem tra giao dien pop up so thoi gian back
    # Capture Page Screenshot    b86_kc_d_ttdh_d_d_pop_up_so_thoi_gian_back.png
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    ${bphone}_kc_d_ttdh_d_d_pop_up_so_thoi_gian_back    4

Kiem tra giao dien pop up thoi gian back
    # Capture Page Screenshot    b86_kc_d_ttdh_d_d_pop_up_thoi_gian_back.png
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    ${bphone}_kc_d_ttdh_d_d_pop_up_thoi_gian_back    4

Kiem tra giao dien pop up lap lai AB
    # Capture Page Screenshot    kc_d_ttdh_d_d_lap_lai_AB.png
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_lap_lai_AB     4
    Sleep    2

Cham vao man hinh video
    Click Element At Coordinates    500    1000
    Sleep    2

Click vao nut huy bo
    Click Element    ${nut_huy_bo_hoa}
    Sleep    2

Click vao nut dong y
    Click Element    ${nut_dong_y_hoa}
    Sleep    1

Click vao tab thoi gian cho
    Click Element    ${xp_tuy_chon_thoi_gian_cho}
    Sleep    2

Click vao kieu dong ho
    Click Element    ${xp_tuy_chon_nut_dong_ho}
    Sleep    2

Click vao nut play
    Click Element    ${xp_v_nut_overplay}
    Sleep    1

Click vao tuy chon phu de
    Click Element    ${xp_tuy_chon_phu_de}
    Sleep    1

Click vao tua den vi tri
    Click Element    ${xp_tuy_chon_tua_den_vi_tri}
    Sleep    1

Click vao so giay muon back
    Click Element    ${xp_tuy_chon_so_giay_back}
    Sleep    1

Click vao phim back
    Click Element    ${xp_tuy_chon_back}
    Sleep    1

Click vao an phim back
    Click Element    ${xp_tuy_chon_an_back}
    Sleep    1

Click vao lap lai AB
    Click Element    ${xp_text_lap_lai_AB}
    Sleep    1

Click vao nut 1X
    Click Element    ${xp_tuy_chon_toc_do_phat_1x}
    Sleep    1

Click vao huy thoi gian cho
    Click Element    ${xp_tuy_chon_huy_thoi_gian_cho}
    Sleep    1

Click vao tre am thanh
    Click Element    ${xp_tuy_chon_tre_am_thanh}
    Sleep    1

Click vao nut cong tre am thanh
    Click Element    ${xp_nut_cong_tre_am_thanh}
    Sleep    1

Click vao nut tru tre am thanh
    Click Element    ${xp_nut_tru_tre_am_thanh}
    Sleep    1
    Click Element    ${xp_nut_tru_tre_am_thanh}
    Sleep    1
    Click Element    ${xp_nut_tru_tre_am_thanh}
    Sleep    1
    Click Element    ${xp_nut_tru_tre_am_thanh}
    Sleep    1
    Click Element    ${xp_nut_tru_tre_am_thanh}
    Sleep    1

Kiem tra khong dong chuc nang tre tieng khi khong cham man hinh
    Page Should Contain Element    ${xp_hien_thi_tre_am_thanh}
    Sleep    1
    Page Should Contain Element    ${xp_nut_tru_tre_am_thanh}
    Sleep    1
    Page Should Contain Element    ${xp_nut_cong_tre_am_thanh}

Kiem tra dong chuc nang tre tieng khi cham man hinh video
    Page Should Not Contain Element    ${xp_hien_thi_tre_am_thanh}
    Sleep    1
    Page Should Not Contain Element    ${xp_nut_tru_tre_am_thanh}
    Sleep    1
    Page Should Not Contain Element    ${xp_nut_cong_tre_am_thanh}

Kiem tra hien thi tren man hinh tre am thanh
    Page Should Contain Text    ${xp_text_tre_am_thanh}
    Sleep    1
    Page Should Contain Text    ${xp_text_so_ms}
    Sleep    1

Kiem tra hien thi tre am thanh tang dan 100s
    Page Should Contain Text    ${xp_text_tre_am_thanh}
    Sleep    1
    Page Should Contain Text    ${xp_text_tre_am_thanh_100s}
    Sleep    1

Kiem tra hien thi tre am thanh bam lien tuc giam xuong -200s
    Page Should Contain Text    ${xp_text_tre_am_thanh}
    Sleep    1
    Page Should Contain Text    ${xp_text_tre_am_thanh_-200s}
    Sleep    1

Kiem tra hien thi tre am thanh tang len 50s
    Page Should Contain Text    ${xp_text_tre_am_thanh}
    Sleep    1
    Page Should Contain Text    ${xp_text_tre_am_thanh_50s}
    Sleep    1

Kiem tra hien thi tre am thanh tu 100s giam xuong 50s
    Page Should Contain Text    ${xp_text_tre_am_thanh}
    Sleep    1
    Page Should Contain Text    ${xp_text_tre_am_thanh_50s}
    Sleep    1

# tre phu de
Click vao tre phu de
    Click Element    ${xp_tuy_chon_tre_phu_de}
    Sleep    1

Click vao nut cong tre phu de
    Click Element    ${xp_nut_cong_tre_phu_de}
    Sleep    1

Click vao nut tru tre phu de
    Click Element    ${xp_nut_tru_tre_phu_de}
    Sleep    1
    Click Element    ${xp_nut_tru_tre_phu_de}
    Sleep    1
    Click Element    ${xp_nut_tru_tre_phu_de}
    Sleep    1
    Click Element    ${xp_nut_tru_tre_phu_de}
    Sleep    1
    Click Element    ${xp_nut_tru_tre_phu_de}
    Sleep    1

Kiem tra khong dong chuc nang tre phu de khi khong cham man hinh
    Page Should Contain Element    ${xp_hien_thi_tre_phu_de}
    Sleep    1
    Page Should Contain Element    ${xp_nut_tru_tre_phu_de}
    Sleep    1
    Page Should Contain Element    ${xp_nut_cong_tre_phu_de}

Kiem tra dong chuc nang tre phu de khi cham man hinh video
    Page Should Not Contain Element    ${xp_hien_thi_tre_phu_de}
    Sleep    1
    Page Should Not Contain Element    ${xp_nut_tru_tre_phu_de}
    Sleep    1
    Page Should Not Contain Element    ${xp_nut_cong_tre_phu_de}

Kiem tra hien thi tren man hinh tre phu de
    Page Should Contain Text    ${xp_text_tre_phu_de}
    Sleep    1
    Page Should Contain Text    ${xp_text_so_ms_phu_de}
    Sleep    1

Kiem tra hien thi tre phu de tang dan 100s
    Page Should Contain Text    ${xp_text_tre_phu_de}
    Sleep    1
    Page Should Contain Text    ${xp_text_tre_phu_de_100s}
    Sleep    1

Kiem tra hien thi tre phu de bam lien tuc giam xuong -200s
    Page Should Contain Text    ${xp_text_tre_phu_de}
    Sleep    1
    Page Should Contain Text    ${xp_text_tre_phu_de_-200s}
    Sleep    1

Kiem tra hien thi tre phu de tang len 50s
    Page Should Contain Text    ${xp_text_tre_phu_de}
    Sleep    1
    Page Should Contain Text    ${xp_text_tre_phu_de_50s}
    Sleep    1

Kiem tra hien thi tre phu de tu 100s xuong 50s
    Page Should Contain Text    ${xp_text_tre_phu_de}
    Sleep    1
    Page Should Contain Text    ${xp_text_tre_phu_de_50s}
    Sleep    1

Nhap so thoi gian vao phut tua den vi tri 15 phut
    Clear Text    ${xp_tuy_chon_nhap_so_phut}
    Input Text    ${xp_tuy_chon_nhap_so_phut}    15
    Sleep    1
    Click Element    ${nut_dong_y_hoa}
    Sleep    1

Nhap so thoi gian tua den vi tri 0 giay
    Clear Text    ${xp_tuy_chon_nhap_so_phut}
    Input Text    ${xp_tuy_chon_nhap_so_phut}    00
    Sleep    1
    Click Element    ${nut_dong_y_hoa}
    Sleep    1

Click vao nhap so giay back
    Click Element    ${xp_text_gio_giay_back}
    Sleep    1

input vao so giay back 99
    Clear Text    ${xp_text_gio_giay_back}
    Click vao nut dong y
    Kiem tra trang thai hien nut dong y
    Sleep    1
     Clear Text    ${xp_text_gio_giay_back}
    Input Text    ${xp_text_gio_giay_back}    99
    # Click vao nut dong y
    Sleep    1

input vao so giay back 30
    Clear Text    ${xp_text_gio_giay_back}
    Input Text    ${xp_text_gio_giay_back}    30
    Click vao nut dong y
    Sleep    1

input vao so giay back la 00
    Clear Text    ${xp_text_gio_giay_back}
    Input Text    ${xp_text_gio_giay_back}    00
    Click vao nut dong y

input vao so giay back la 100
    Sleep    1
    Clear Text    ${xp_text_gio_giay_back}
    Input Text    ${xp_text_gio_giay_back}    100
    Sleep    1

input vao so giay back la 75
    Sleep    1
    Clear Text    ${xp_text_gio_giay_back}
    Input Text    ${xp_text_gio_giay_back}    75
    Sleep    1


Kiem tra nhap 100s hien thi tren man hinh chi nhap duoc 10s
    ${get_text}    Get Text    ${xp_text_gio_giay_back}
    Should Be Equal    10    ${get_text}

Kiem tra hien thi mac dinh so giay back
    Element Attribute Should Match    ${xp_text_gio_giay_back}    text   *${xp_tuy_text_so_giay_back}*
    Sleep    1

Kiem tra hien thi nhap so giay back la 99s
    ${get_text}    Get Text    ${xp_text_gio_giay_back}
    Element Attribute Should Match    ${xp_text_gio_giay_back}    text    *99*
    Sleep    1

Kiem tra nhap 30 giay dong y thoi gian back
    ${get_text}    Get Text    ${xp_tuy_chon_so_giay_back}
    Element Attribute Should Match    ${xp_tuy_chon_so_giay_back}    text    *30 giây*
    Sleep    1

Kiem tra hien thi so thoi gian vua tua den vi tri
    ${get_text}    Get Text    ${xp_thoi_gian_hien_thi_chay}
    Element Attribute Should Match    ${xp_thoi_gian_hien_thi_chay}    text    *15:00*
    Sleep    1

Kiem tra hien thi so thoi gian vua tua ve 0s
    ${get_text}    Get Text    ${xp_thoi_gian_hien_thi_chay}
    Element Attribute Should Match    ${xp_thoi_gian_hien_thi_chay}    text    *0s*
    Sleep    1

Vuot nut back xoa bo tren man hinh
    Swipe By Percent    8    65    8    98
    Sleep    1
    Swipe By Percent    8    98    53    98

Vuot thanh thoi gian ve ban dau
    Click Element At Coordinates    54    1816

Kiem tra trang thai khong hien nut huy bo
    Page Should Not Contain Element    ${nut_huy_bo_hoa}
    Sleep    1

Kiem tra trang thai khong hien nut dong y
    Page Should Not Contain Element    ${nut_dong_y_hoa}
    Sleep    1

Kiem tra trang thai khong hien thi text phim back
    Page Should Not Contain Element    ${xp_tuy_chon_back}
    Sleep    1

Kiem tra trang thai khong hien thi text an phim back
    Page Should Not Contain Element    ${xp_tuy_chon_back}
    Sleep    1

Kiem tra hien thi text vew tai man hinh danh sach video
    Page Should Contain Element    ${xp_v_textview}
    Sleep    1

Kiem tra hien thi nut tim kiem tai man hinh danh sach video
    Page Should Contain Element    ${xp_nut_tim_kiem}
    Sleep    1

Kiem tra khong hien thi nut tuy chon
    Page Should Not Contain Element    ${nut_track}
    Sleep    1

Kiem tra trang thai hien nut dong y
    Page Should Contain Element    ${nut_dong_y_hoa}
    Sleep    1

Kiem tra trang thai toc do phat la 1X
    ${get_text}    Get Text    ${xp_tuy_chon_toc_do_phat_1x}
    Element Attribute Should Match    ${xp_tuy_chon_toc_do_phat_1x}    text    *1X*
    Sleep    1

Kiem tra trang thai khong hien tab thoi gian cho
    Page Should Not Contain Element    ${xp_tuy_chon_thoi_gian_cho}
    Sleep    1

Kiem tra trang thai khong hien thi toc do phat la 1X
    Page Should Not Contain Element    ${xp_tuy_chon_toc_do_phat_1x}
    Sleep    1

Kiem tra trang thai hien thi toc do phat la 1X
    Page Should Contain Element    ${xp_tuy_chon_toc_do_phat_1x}
    Sleep    1

Kiem tra trang thai hien thi tai dong ho so
    Page Should Contain Element    ${xp_tuy_chon_text_nhap_thoi_gian}
    Sleep    1
    Page Should Contain Element    ${xp_tuy_chon_text_dat_gio}

Kiem tra trang thai khong hien thi thoi gian cho
    Page Should Not Contain Element    ${xp_tuy_chon_huy_thoi_gian_cho}
    Sleep    1

Kiem tra trang thai khong hien thi kieu thoi gian cho mac dinh
    Page Should Not Contain Element    ${xp_tuy_chon_text_mac_dinh_thoi_gian}
    Sleep    1

Kiem tra trang thai hien thi thoi gian cho
    Page Should Contain Element    ${xp_tuy_chon_huy_thoi_gian_cho}
    Sleep    1

Kiem tra trang thai mac dinh thoi gian cho
    ${get_text}    Get Text    ${xp_tuy_chon_text_mac_dinh_thoi_gian}
    Element Attribute Should Match    ${xp_tuy_chon_text_mac_dinh_thoi_gian}    text    *--:--*
    Sleep    1

Kiem tra trang thai mac dinh toc do phat la 1_00x
    ${get_text}    Get Text    ${xp_tuy_chon_toc_do_phat_1}
    Element Attribute Should Match    ${xp_tuy_chon_toc_do_phat_1}    text    *1.00x*
    Sleep    1

Kiem tra trang thai toc do phat la 071x
    ${get_text}    Get Text    ${xp_tuy_chon_toc_do_phat_071x}
    Element Attribute Should Match    ${xp_tuy_chon_toc_do_phat_071x}    text    *0.71x*
    Sleep    1

Kiem tra trang thai toc do phat la 050x
    ${get_text}    Get Text    ${xp_tuy_chon_toc_do_phat_050x}
    Element Attribute Should Match    ${xp_tuy_chon_toc_do_phat_050x}    text    *0.50x*
    Sleep    1

Kiem tra trang thai toc do phat la 141x
    ${get_text}    Get Text    ${xp_tuy_chon_toc_do_phat_141x}
    Element Attribute Should Match    ${xp_tuy_chon_toc_do_phat_141x}    text    *1.41x*
    Sleep    1

Kiem tra trang thai toc do phat la 200x
    ${get_text}    Get Text    ${xp_tuy_chon_toc_do_phat_200x}
    Element Attribute Should Match    ${xp_tuy_chon_toc_do_phat_200x}    text    *2.00x*
    Sleep    1

Cham vuot toa do thay doi toc do phat
    Swipe    ${xp_tuy_chon_toc_do_phat_071x_bd_toa_do_x}    ${xp_tuy_chon_toc_do_phat_071x_bd_toa_do_y}    ${xp_tuy_chon_toc_do_phat_071x_toa_do_x}    ${xp_tuy_chon_toc_do_phat_071x_toa_do_y}
    Sleep    1
    Kiem tra trang thai toc do phat la 071x
    Swipe    ${xp_tuy_chon_toc_do_phat_050x_bd_toa_do_x}    ${xp_tuy_chon_toc_do_phat_050x_bd_toa_do_y}    ${xp_tuy_chon_toc_do_phat_050x_toa_do_x}    ${xp_tuy_chon_toc_do_phat_050x_toa_do_y}
    Sleep    1
    Kiem tra trang thai toc do phat la 050x
    Swipe    ${xp_tuy_chon_toc_do_phat_141x_bd_toa_do_x}    ${xp_tuy_chon_toc_do_phat_141x_bd_toa_do_y}    ${xp_tuy_chon_toc_do_phat_141x_toa_do_x}    ${xp_tuy_chon_toc_do_phat_141x_toa_do_y}
    Sleep    1
    Kiem tra trang thai toc do phat la 141x
    Swipe    ${xp_tuy_chon_toc_do_phat_200x_bd_toa_do_x}    ${xp_tuy_chon_toc_do_phat_200x_bd_toa_do_y}    ${xp_tuy_chon_toc_do_phat_200x_toa_do_x}    ${xp_tuy_chon_toc_do_phat_200x_toa_do_y}
    Sleep    1
    Kiem tra trang thai toc do phat la 200x
    Sleep    1

Kiem tra hien thi progressbar toc do phat
    ${get_text}    Get Text    ${xp_tuy_chon_toc_do_phat_05}
    Element Attribute Should Match    ${xp_tuy_chon_toc_do_phat_05}    text    *0.5*
    Sleep    1
    ${get_text}    Get Text    ${xp_tuy_chon_toc_do_phat_1.00}
    Element Attribute Should Match    ${xp_tuy_chon_toc_do_phat_1.00}    text    *1.00*
    Sleep    1
    ${get_text}    Get Text    ${xp_tuy_chon_toc_do_phat_200}
    Element Attribute Should Match    ${xp_tuy_chon_toc_do_phat_200}    text    *2.00*
    Sleep    1
    Kiem tra trang thai khong hien thi toc do phat la 1X
    Sleep    1
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================