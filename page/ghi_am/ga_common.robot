*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot
Resource    ../dashboard/db_common.robot

*** Keywords ***
An nut tuy chon
    [Documentation]    An nut tuy chon
    Click Element    ${ga_tab_tuy_chon_khac}
    Sleep    2    

An nut danh sach ban ghi
    [Documentation]    An nut danh sach ban ghi
    Click Element    ${nut_danh_sach_ban_ghi}
    Sleep    2    
    
An nut bat dau ghi am
    [Documentation]    An nut ghi am
    Click Element    ${nut_bat_dau_ghi_am}
    Sleep    2    
    
Mo ghi am tu da nhiem
    [Documentation]    An anh ghi am
    Click Element    ${ga_mo_da_nhiem}
    Sleep    2   
    
An nut dong y
    [Documentation]    An nut dong y
    Click Element    ${nut_dong_y_hoa}
    Sleep    2    
    
An nut tam dung ghi am
    [Documentation]    Tam dung ghi am
    Click Element    ${nut_bat_dau_ghi_am}
    Sleep    2    
    
Mo quan ly file va doi ten tep ghi am
    Log To Console    mo quan ly file va doi ten tep ghi am
    Mo thu muc ghi am trong quan ly file   
    Click Element    ${ga_cb_doi_ten_file_ga}
    Sleep    2    
    Click Element    ${qlf_nut_nhieu_hon} 
    Sleep    2    
    Click Element    ${qlf_nut_doi_ten}
    Sleep    2    
    Clear Text    ${qlf_textbox_ten_thu_muc}
    Input Text    ${qlf_textbox_ten_thu_muc}    DaiTVb
    Click Element    ${nut_dong_y_hoa}
    Sleep    2  
  
Mo thu muc ghi am trong quan ly file
    Log To Console    Mo thu muc ghi am trong quan ly file
    Mo Quan ly file tu Launcher
    Sleep    2    
    Cho phep cap quyen cho ung dung
    Sleep    5    
    Click Element At Coordinates    168    664
    Sleep    5    
    Swipe    505    1837    505    706    
    Sleep    2        
    AppiumLibrary.Click Element    ${qlf_ghi_am}
    Sleep    2    
    AppiumLibrary.Click Element    ${qlf_tep_ghi_am}
    Sleep    2    
    
Xoa toan bo du lieu app ghi am trong quan ly file
    Log To Console    Xoa toan bo du lieu app ghi am trong quan ly file
    Mo Quan ly file tu Launcher
    Cho phep cap quyen cho ung dung
    Sleep    5    
    Click Element At Coordinates    168    664
    Swipe    524    1999    524    900   
    Sleep    2
    ${hien_thi_ga}    Run Keyword And Return Status    Get Matching Xpath Count    ${qlf_ghi_am} 
    Run Keyword If    ${hien_thi_ga}>0    AppiumLibrary.Click Element    ${qlf_ghi_am}    
    ...    ELSE    Dong toan bo ung dung trong da nhiem        
    Sleep    2    
    AppiumLibrary.Click Element    ${qlf_tep_ghi_am}
    Sleep    2    
    AppiumLibrary.Click Element    ${nut_tuy_chon_khac}
    Sleep    2    
    ${trang_thai}    Run Keyword And Return Status    Element Should Be Visible    ${qlf_chon_tat_ca}        
    Run Keyword If    ${trang_thai}==True    AppiumLibrary.Click Element    ${qlf_chon_tat_ca} 
    Sleep    2    
    Run Keyword If    ${trang_thai}==True    AppiumLibrary.Click Element    ${ga_ban_ghi_xoa}
    Run Keyword If    ${trang_thai}==True    AppiumLibrary.Click Element    ${nut_dong_y_hoa}
    Run Keyword If    ${trang_thai}==False   Sleep    2  
    Sleep    1   
    Dong toan bo ung dung trong da nhiem   
    
Mo popup chia se file ghi am
    Log To Console    Mo popup chia se file ghi am
    Long Press    ${ga_ban_ghi_list}    
    Sleep    2    
    Click Element    ${ga_ban_ghi_chia_se}
    Sleep    2    
    Swipe By Percent    50    60    50    10    duration=2000
    Sleep    5   
        
Mo app ghi am va tao ban ghi
    Log To Console    Mo app ghi am va tao ban ghi    
    Mo Ghi am tu Launcher
    An nut bat dau ghi am
    Sleep    15    
    Click Element At Coordinates    ${ga_nut_danh_sach_ban_ghi_x}    ${ga_nut_danh_sach_ban_ghi_y}
    Sleep    2    
    Click Element    ${nut_dong_y_hoa}
    Sleep    2  

Tao file ghi am theo thoi gian
    Log To Console    tao ban ghi theo thoi gian tao    
    Click Element    ${ga_nut_back}
    Sleep    2    
    An nut bat dau ghi am
    Sleep    15    
    Click Element At Coordinates    ${ga_nut_danh_sach_ban_ghi_x}    ${ga_nut_danh_sach_ban_ghi_y}
    Sleep    2 
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}    ban ghi 1
    Sleep    2    
    Click Element    ${nut_dong_y_hoa}
    Sleep    5    
    Click Element    ${ga_ban_ghi_tao_ghi_am}
    Sleep    15    
    Click Element At Coordinates    ${ga_nut_danh_sach_ban_ghi_x}    ${ga_nut_danh_sach_ban_ghi_y}
    Sleep    2 
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}    ban ghi 2
    Sleep    2    
    Click Element    ${nut_dong_y_hoa}
    Sleep    5    
    Click Element    ${ga_ban_ghi_tao_ghi_am}
    Sleep    15    
    Click Element At Coordinates    ${ga_nut_danh_sach_ban_ghi_x}    ${ga_nut_danh_sach_ban_ghi_y}
    Sleep    2 
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}    ban ghi 3
    Sleep    2    
    Click Element    ${nut_dong_y_hoa}
    Sleep    2    
    
Tao ban ghi va tick checkbox an khoi trinh choi nhac
    Log To Console    Tao ban ghi va tick checkbox an khoi trinh choi nhac   
    Mo Ghi am tu Launcher
    An nut bat dau ghi am
    Sleep    15    
    Click Element At Coordinates    ${ga_nut_danh_sach_ban_ghi_x}    ${ga_nut_danh_sach_ban_ghi_y}
    Sleep    2 
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}    ban ghi 1
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    Go Back
    Sleep    2    
    Click Element    ${ga_tab_tuy_chon_khac}
    Sleep    2    
    ${trang_thai}    Element Attribute Should Match    ${ga_checkbox_an_khoi_trinh_choi_nhac}    checked    true    
    Run Keyword If    ${trang_thai}==true    Sleep    2        
    ...    ELSE      Click Element    ${ga_checkbox_an_khoi_trinh_choi_nhac}   
    Vuot back ve giao dien truoc
    
Bat hien thi ghi am len dashboard
    Log To Console    Bat hien thi ghi am len dashboard    
    Mo Cai dat
    Sleep    5    
    Hien thi tat ca cac chuc nang len dashboard
    Vuot mo bang dieu khien
    Vuot sang man hinh ben phai             
    Vuot sang man hinh ben phai  
    Sleep    2    
    Click Element At Coordinates    ${db_nut_ghi_am_x}    ${db_nut_ghi_am_y}    
    Sleep    2    
    
Mo khoa va dong popup man hinh khoa
    Log To Console    Mo khoa va dong toan bo ung dung trong da nhiem    
    Mo khoa voi mat khau 8888
    Click Element At Coordinates    857    1400
    Sleep    5  
    
Vuot sang trai launcher va click app cai dat
    Log To Console    Vuot sang trai launcher va click app cai dat      
    Vuot sang man hinh ben trai
    Sleep    2
    Click Element    ${app_cai_dat}
    
Tao file ghi am trung voi file ghi am da co
    Log To Console    Tao file ghi am trung voi file ghi am da co    
    Vuot back ve giao dien truoc
    An nut bat dau ghi am
    Sleep    2    
    Click Element At Coordinates    ${ga_nut_danh_sach_ban_ghi_x}    ${ga_nut_danh_sach_ban_ghi_y}
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}    DaiTVb
    Click Element    ${nut_dong_y_hoa}
    Sleep    1 

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================