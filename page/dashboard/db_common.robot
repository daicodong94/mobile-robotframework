*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary    

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
An nut them icon
    Log To Console    An nut them icon    
    AppiumLibrary.Click Element    ${db_nut_them_icon_db}
    Sleep    2    

Hien thi tat ca cac chuc nang len dashboard
    Log To Console    Hien thi tat ca cac chuc nang len dashboard    
    Click Element    ${cd_nut_bang_dieu_khien}
    Sleep    2    
    Vuot back ve giao dien truoc
    Sleep    2    
    Click Element    ${cd_nut_bang_dieu_khien}
    Sleep    5    
    Swipe By Percent    50    80    50    30
    Sleep    2        
    Swipe By Percent    50    80    50    30    
    Swipe By Percent    50    80    50    30    
    Sleep    2
    An nut them icon
    Sleep    2    
    An nut them icon
    Sleep    2    
    An nut them icon
    Sleep    2    
    An nut them icon
    Sleep    2    
    An nut them icon
    Sleep    2    
    An nut them icon
    Sleep    2    
    An nut them icon
    Sleep    2    
    An nut them icon
    Sleep    2    
    An nut them icon
    Sleep    2        
    
Vuot mo phan trang dashboard ben trai
    Log To Console    Vuot mo phan trang dashboard ben trai    
    Swipe By Percent    10    60    90    60    
    Sleep    2    
    
Vuot mo phan trang dashboard ben phai
    Log To Console    Vuot mo phan trang dashboard ben trai    
    Swipe By Percent    90    60    10    60    
    Sleep    2    
    

    
    
    

        
    









     
    








#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================