*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot

*** Keywords ***
Vuot tu trai sang phai de chuyen ve huong dan truoc do
    Log To Console    Vuot tu trai sang phai de chuyen ve huong dan truoc do
    AppiumLibrary.Swipe By Percent    10    50    90    50
    Sleep    2

Vuot tu phai sang trai de chuyen sang huong dan tiep theo
    Log To Console    Vuot tu phai sang trai de chuyen sang huong dan tiep theo
    AppiumLibrary.Swipe By Percent    90    50    10    50
    Sleep    2

Bam nut [<] de chuyen ve huong dan truoc do
    Log To Console    Bam nut [<] de chuyen ve huong dan truoc do
    AppiumLibrary.Click Element    ${kp_nut_hd_truoc}
    Sleep    2

Bam nut [>] de chuyen sang huong dan tiep theo
    Log To Console    Bam nut [>] de chuyen sang huong dan tiep theo
    AppiumLibrary.Click Element    ${kp_nut_hd_sau}
    Sleep    2

Bam vao vung duoi hinh anh dien thoai de xem huong dan
    Log To Console    Bam vao vung duoi hinh anh dien thoai de xem huong dan
    AppiumLibrary.Click Element    ${kp_nut_xem_huong_dan}
    sleep    10

Quay ve ung dung Kham pha
    Log To Console    Quay ve ung dung Kham pha
    AppiumLibrary.Go Back
    Sleep    2
    AppiumLibrary.Page Should Contain Element    ${kp_nut_xem_huong_dan}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================