*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot
Resource    ../../page/may_tinh/mt_common.robot

*** Variables ***
# Cac tham so mo ung dung
${mt_alias}    may_tinh
${mt_package}    com.bkav.calculator2
${mt_activity}    com.android.calculator2.bkav.BkavCalculator

*** Keywords ***
# cac ham click nut emlement
An nut INV
    [Documentation]    nhan nut INV
    Log To Console    nhan nut INV
    AppiumLibrary.Click Element    ${mt_nut_inv}

An nut DEG
    [Documentation]    nhan nut DEG
    Log To Console    nhan nut DEG
    AppiumLibrary.Click Element    ${mt_nut_deg_rad}

An nut RAD
    [Documentation]    nhan nut RAD
    Log To Console    nhan nut RAD
    AppiumLibrary.Click Element    ${mt_nut_deg_rad}

An nut phan tram
    [Documentation]    nhan nut phan tram
    Log To Console    nhan nut phan tram
    AppiumLibrary.Click Element    ${mt_nut_phan_tram}

An nut mc
    [Documentation]    nhan nut mc
    Log To Console    nhan nut mc
    AppiumLibrary.Click Element    ${mt_nut_mc}

An nut sin
    [Documentation]    nhan nut sin
    Log To Console    nhan nut sin
    AppiumLibrary.Click Element    ${mt_nut_sin}

An nut cos
    [Documentation]    nhan nut cos
    Log To Console    nhan nut cos
    AppiumLibrary.Click Element    ${mt_nut_cos}
    # AppiumLibrary.Element Should Be Enabled    ${mt_nut_cos}

An nut tan
    [Documentation]    nhan nut tan
    Log To Console    nhan nut tan
    AppiumLibrary.Click Element    ${mt_nut_tan}

An nut m cong
    [Documentation]    nhan nut m+
    Log To Console    nhan nut m+
    AppiumLibrary.Click Element    ${mt_nut_m_cong}

An nut In
    [Documentation]    nhan nut In
    Log To Console    nhan nut In
    AppiumLibrary.Click Element    ${mt_nut_ln}

An nut log
    [Documentation]    nhan nut log
    Log To Console    nhan nut log
    AppiumLibrary.Click Element    ${mt_nut_log}

An nut dau cham than
    [Documentation]    nhan nut dau cham tahn (!)
    Log To Console    nhan nut dau cham than (!)
    AppiumLibrary.Click Element    ${mt_nut_giai_thua}

An nut m tru
    [Documentation]    nhan nut m tru (m-)
    Log To Console    nhan nut m tru (m-)
    AppiumLibrary.Click Element    ${mt_nut_m_tru}

An nut Pi
    [Documentation]    nhan nut Pi π
    Log To Console    nhan nut Pi π
    AppiumLibrary.Click Element    ${mt_nut_pi}

An nut e
    [Documentation]    nhan nut e
    Log To Console    nhan nut e
    AppiumLibrary.Click Element    ${mt_nut_e}

An nut luy thua
    [Documentation]    nhan nut luy thua (^)
    Log To Console    nhan nut luy thua (^)
    AppiumLibrary.Click Element    ${mt_nut_luy_thua}

An nut mr
    [Documentation]    nhan nut mr
    Log To Console    nhan nut mr
    AppiumLibrary.Click Element    ${mt_nut_mr}

An nut dau ngoac trai
    [Documentation]    nhan nut dau ngoac trai
    Log To Console    nhan nut dau ngoac trai
    AppiumLibrary.Click Element    ${mt_nut_ngoac_trai}

An nut dau ngoac phai
    [Documentation]    nhan nut dau ngoac phai
    Log To Console    nhan nut dau ngoac phai
    AppiumLibrary.Click Element    ${mt_nut_ngoac_phai}

An nut khai can
    [Documentation]    nhan nut khai can
    Log To Console    nhan nut khai can
    AppiumLibrary.Click Element    ${mt_nut_can_bac_2}

# cac ham click emlement nut khi click INV
An nut sin INV
    [Documentation]    nhan nut sin INV
    Log To Console    nhan nut sin INV
    AppiumLibrary.Click Element    ${mt_nut_arcsin}

An nut cos INV
    [Documentation]    nhan nut cos INV
    Log To Console    nhan nut cos INV
    AppiumLibrary.Click Element    ${mt_nut_arccos}

An nut tan INV
    [Documentation]    nhan nut tan INV
    Log To Console    nhan nut tan INV
    AppiumLibrary.Click Element    ${mt_nut_arctan}

An nut e INV
    [Documentation]    nhan nut e INV
    Log To Console    nhan nut e INV
    AppiumLibrary.Click Element    ${mt_nut_e_mu}

An nut 10 mu INV
    [Documentation]    nhan nut 10 mu INV (10x)
    Log To Console    nhan nut 10 mu INV    (10x)
    AppiumLibrary.Click Element    ${mt_nut_10_mu}

An nut binh phuong INV
    [Documentation]    nhan nut binh phuong INV
    Log To Console    nhan nut binh phuong INV
    AppiumLibrary.Click Element    ${mt_nut_mu_2}

# cac ham xu ly thao tac man hinh
Vuot ra giao dien nang cao
    [Documentation]    Vuot ra man hinh giao dien nang cao
    Log To Console    Vuot ra man hinh giao dien nang cao
    Swipe    666    1470    30    1479

Vuot an giao dien nang cao
    [Documentation]    Vuot an man hinh giao dien nang cao
    Log To Console    Vuot an man hinh giao dien nang cao
    Swipe    173    1470    873    1480

# Click vao cac element so
An nut so 5
    [Documentation]    An nut 5 tu ban phim so
    AppiumLibrary.Click Element    ${mt_nut_5}

An nut so 2
    [Documentation]    An nut 2 tu ban phim so
    AppiumLibrary.Click Element    ${mt_nut_2}

An nut so 4
    [Documentation]    An nut 4 tu ban phim so
    AppiumLibrary.Click Element    ${mt_nut_4}

An phep tinh nhan 2x2
    [Documentation]    An phep tinh nhan 2x2
    AppiumLibrary.Click Element    ${mt_nut_2}
    AppiumLibrary.Click Element    ${mt_nut_nhan}
    AppiumLibrary.Click Element    ${mt_nut_2}

An phep tinh nhan 3x3
    [Documentation]    An phep tinh nhan 3x3
    AppiumLibrary.Click Element    ${mt_nut_3}
    AppiumLibrary.Click Element    ${mt_nut_nhan}
    AppiumLibrary.Click Element    ${mt_nut_3}

nhan nut xoa lich su
    ${get_text}=    Element Should Contain Text    ${mt_nut_xoa_lich_su}    Xóa lịch sử    ${False}
    Run Keyword If    ${get_text}    AppiumLibrary.Click Element    ${get_text}
    AppiumLibrary.Click Element    ${mt_nut_xoa_lich_su}

Xoa lich su cac phep tinh
    An nut lich su
    Run Keyword If   '${mt_nut_xoa_lich_su}'=='PASS'    nhan nut xoa lich su
    ...    ELSE    Run Keywords     An nut lich su    Xoa man hinh

An dau ba cham
    [Documentation]    An vao dau
    AppiumLibrary.Click Element    ${mt_nut_nang_cao}

An so 9
    [Documentation]    An vao 9
    AppiumLibrary.Click Element    ${mt_nut_9}

An dau )
    [Documentation]    An vao )
    AppiumLibrary.Click Element    ${mt_nut_ngoac_phai}

An vao nhan
    [Documentation]    An vao nhan
    AppiumLibrary.Click Element    ${mt_nut_nhan}

An vao bang
    [Documentation]    An vao bang
    AppiumLibrary.Click Element    ${mt_nut_bang}

# thuc hien phep tinh cong
Thuc hien va kiem tra phep tinh cong sin
    [Documentation]    Thuc hien va kiem tra phep tinh cong sin
    An nut sin
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau cong (+)
    An nut man hinh nang cao
    An nut sin
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh cong cos
    [Documentation]    Thuc hien va kiem tra phep tinh cong cos
    An nut cos
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau cong (+)
    An nut man hinh nang cao
    An nut cos
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh cong tan
    [Documentation]    Thuc hien va kiem tra phep tinh cong tan
    An nut tan
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau cong (+)
    An nut man hinh nang cao
    An nut tan
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh cong ln
    An nut In
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau cong (+)
    An nut man hinh nang cao
    An nut In
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh cong log
    [Documentation]    Thuc hien va kiem tra phep tinh cong ln
    An nut log
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau cong (+)
    An nut man hinh nang cao
    An nut log
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh tru sin
    [Documentation]    Thuc hien va kiem tra phep tinh tru sin
    An nut sin
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau tru (-)
    An nut man hinh nang cao
    An nut sin
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh tru cos
    [Documentation]    Thuc hien va kiem tra phep tinh tru cos
    An nut cos
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau tru (-)
    An nut man hinh nang cao
    An nut cos
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh tru tan
    [Documentation]    Thuc hien va kiem tra phep tinh tru tan
    An nut tan
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau tru (-)
    An nut man hinh nang cao
    An nut tan
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh tru ln
    [Documentation]    Thuc hien va kiem tra phep tinh tru ln
    An nut In
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau tru (-)
    An nut man hinh nang cao
    An nut In
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh tru log
    [Documentation]    Thuc hien va kiem tra phep tinh cong ln
    An nut log
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau tru (-)
    An nut man hinh nang cao
    An nut log
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

# thuc hien pheo tinh nhan
Thuc hien va kiem tra phep tinh nhan sin
    [Documentation]    Thuc hien va kiem tra phep tinh nhan sin
    An nut sin
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau nhan (x)
    An nut man hinh nang cao
    An nut sin
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh nhan cos
    [Documentation]    Thuc hien va kiem tra phep tinh nhan cos
    An nut cos
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau nhan (x)
    An nut man hinh nang cao
    An nut cos
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh nhan tan
    [Documentation]    Thuc hien va kiem tra phep tinh nhan tan
    An nut tan
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau nhan (x)
    An nut man hinh nang cao
    An nut tan
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh nhan ln
    [Documentation]    Thuc hien va kiem tra phep tinh nhan ln
    An nut In
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau nhan (x)
    An nut man hinh nang cao
    An nut In
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh nhan log
    [Documentation]    Thuc hien va kiem tra phep tinh cong ln
    An nut log
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau nhan (x)
    An nut man hinh nang cao
    An nut log
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

# thuc hien phep tinh chia
Thuc hien va kiem tra phep tinh chia sin
    [Documentation]    Thuc hien va kiem tra phep tinh chia sin
    An nut sin
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau chia (÷)
    An nut man hinh nang cao
    An nut sin
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh chia cos
    [Documentation]    Thuc hien va kiem tra phep tinh chia cos
    An nut cos
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau chia (÷)
    An nut man hinh nang cao
    An nut cos
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh chia tan
    [Documentation]    Thuc hien va kiem tra phep tinh chia tan
    An nut tan
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau chia (÷)
    An nut man hinh nang cao
    An nut tan
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh chia ln
    [Documentation]    Thuc hien va kiem tra phep tinh chia ln
    An nut In
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau chia (÷)
    An nut man hinh nang cao
    An nut In
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

Thuc hien va kiem tra phep tinh chia log
    [Documentation]    Thuc hien va kiem tra phep tinh cong ln
    An nut log
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau chia (÷)
    An nut man hinh nang cao
    An nut log
    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_9}
    An nut man hinh nang cao
    An nut dau ngoac phai
    An nut man hinh nang cao
    An nut dau (=)

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================