*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot
Resource    lich_su.robot

*** Keywords ***
Mo May tinh roi mo giao dien lich su
    [Documentation]    Mo May tinh roi mo giao dien lich su
    Mo ung dung    ${mt_alias}    ${mt_package}    ${mt_activity}    2
    AppiumLibrary.Click Element    ${mt_nut_lich_su}
    Sleep    2

Mo May tinh roi mo giao dien nang cao
    [Documentation]    Mo May tinh roi mo giao dien nang cao
    Mo ung dung    ${mt_alias}    ${mt_package}    ${mt_activity}    2
    AppiumLibrary.Click Element    ${mt_nut_nang_cao}
    Sleep    2

An nut 123
    [Documentation]    An nut 123 tu ban phim so
    AppiumLibrary.Click Element    ${mt_nut_1}
    AppiumLibrary.Click Element    ${mt_nut_2}
    AppiumLibrary.Click Element    ${mt_nut_3}
An nut 456
    [Documentation]    An nut 456 tu ban phim so
    AppiumLibrary.Click Element    ${mt_nut_4}
    AppiumLibrary.Click Element    ${mt_nut_5}
    AppiumLibrary.Click Element    ${mt_nut_6}
An nut 1234567
    [Documentation]    An nut 1234567 tu ban phim so
    AppiumLibrary.Click Element    ${mt_nut_1}
    AppiumLibrary.Click Element    ${mt_nut_2}
    AppiumLibrary.Click Element    ${mt_nut_3}
    AppiumLibrary.Click Element    ${mt_nut_4}
    AppiumLibrary.Click Element    ${mt_nut_5}
    AppiumLibrary.Click Element    ${mt_nut_6}
    AppiumLibrary.Click Element    ${mt_nut_7}

An nut 12345678901234556
    [Documentation]    An nut 12345678901234556 tu ban phim so
    AppiumLibrary.Click Element    ${mt_nut_1}
    AppiumLibrary.Click Element    ${mt_nut_2}
    AppiumLibrary.Click Element    ${mt_nut_3}
    AppiumLibrary.Click Element    ${mt_nut_4}
    AppiumLibrary.Click Element    ${mt_nut_5}
    AppiumLibrary.Click Element    ${mt_nut_6}
    AppiumLibrary.Click Element    ${mt_nut_7}
    AppiumLibrary.Click Element    ${mt_nut_8}
    AppiumLibrary.Click Element    ${mt_nut_9}
    AppiumLibrary.Click Element    ${mt_nut_0}
    AppiumLibrary.Click Element    ${mt_nut_1}
    AppiumLibrary.Click Element    ${mt_nut_2}
    AppiumLibrary.Click Element    ${mt_nut_3}
    AppiumLibrary.Click Element    ${mt_nut_4}
    AppiumLibrary.Click Element    ${mt_nut_5}
    AppiumLibrary.Click Element    ${mt_nut_5}
    AppiumLibrary.Click Element    ${mt_nut_6}
An nut 12345
    [Documentation]    An nut 12345 tu ban phim so
    AppiumLibrary.Click Element    ${mt_nut_1}
    AppiumLibrary.Click Element    ${mt_nut_2}
    AppiumLibrary.Click Element    ${mt_nut_3}
    AppiumLibrary.Click Element    ${mt_nut_4}
    AppiumLibrary.Click Element    ${mt_nut_5}

An nut 123456789 mt_main_18_10_1
    [Documentation]    An nut 123456789 tu ban phim so
    AppiumLibrary.Click Element    ${mt_nut_1}
    AppiumLibrary.Click Element    ${mt_nut_2}
    AppiumLibrary.Click Element    ${mt_nut_3}
    AppiumLibrary.Click Element    ${mt_nut_4}
    AppiumLibrary.Click Element    ${mt_nut_5}
    AppiumLibrary.Click Element    ${mt_nut_6}
    AppiumLibrary.Click Element    ${mt_nut_7}
    AppiumLibrary.Click Element    ${mt_nut_8}
    AppiumLibrary.Click Element    ${mt_nut_9}

Click xoa so
    Log To Console    xoa tung so trên phep tinh
    AppiumLibrary.Click Element     ${mt_nut_xoa}

Xoa man hinh
    [Documentation]    xoa man hinh may tinh
    AppiumLibrary.Long Press    ${mt_nut_xoa}     1000

An nut man hinh nang cao
    [Documentation]    An nut man hinh nang cao
    AppiumLibrary.Click Element    ${mt_nut_nang_cao}
    Sleep    2

An nut lich su
    [Documentation]    An nut lich su
    AppiumLibrary.Click Element    ${mt_nut_lich_su}
An nut ham In trong nang cao
    [Documentation]    An nut ham In trong nang cao
    AppiumLibrary.Click Element    ${mt_nut_ln}
An nut phay thap phan
    [Documentation]    An dau phay thap phan
    AppiumLibrary.Click Element    ${mt_nut_phay}
An nut dau chia (÷)
    [Documentation]    nut dau hai cham (÷)
    AppiumLibrary.Click Element    ${mt_nut_chia}
An nut dau nhan (x)
    [Documentation]    nut dau hai cham (x)
    AppiumLibrary.Click Element    ${mt_nut_nhan}
An nut dau cong (+)
    [Documentation]    nut dau hai cham (+)
    AppiumLibrary.Click Element    ${mt_nut_cong}
An nut dau tru (-)
    [Documentation]    nut dau hai cham (-)
    AppiumLibrary.Click Element    ${mt_nut_tru}
An nut dau (=)
    [Documentation]    An nut dau (=)
    AppiumLibrary.Click Element    ${mt_nut_bang}
An nut so 0
    [Documentation]    An so 0
    AppiumLibrary.Click Element    ${mt_nut_0}
An nut Clear xoa ket qua
    [Documentation]    An nut clear
    ${get_text}=    AppiumLibrary.Get Text    ${mt_nut_clr}
    AppiumLibrary.Click Element    ${mt_nut_clr}

Thuc hien nhap phep tinh
    Log To Console    Check chuc nang nhap so thoat home vao lai app
    AppiumLibrary.Click Element    ${mt_nut_1}
    AppiumLibrary.Click Element    ${mt_nut_2}
    AppiumLibrary.Click Element    ${mt_nut_3}
    AppiumLibrary.Click Element    ${mt_nut_4}
    AppiumLibrary.Click Element    ${mt_nut_5}
    AppiumLibrary.Go Back
    Sleep    2
    Mo May tinh tu Launcher
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}    text    *12**345*
    An nut dau cong (+)
    AppiumLibrary.Click Element    ${mt_nut_6}
    AppiumLibrary.Click Element    ${mt_nut_7}
    AppiumLibrary.Click Element    ${mt_nut_8}
    AppiumLibrary.Click Element    ${mt_nut_9}
    Vuot quay ve man hinh Home
    Sleep    2
    Mo May tinh tu Launcher
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}     text    *12**345+6**789*
    Bam vao nut bang
    Vuot quay ve man hinh Home
    Mo Cai dat tu Launcher
    Vuot quay ve man hinh Home
    Mo May tinh tu Launcher
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}     text    *19**134*

Thuc hien phep tinh kiem tra hien thi ket qua
    Xoa man hinh
    Log To Console    check luu phep tinh gan nhat
    AppiumLibrary.Click Element    ${mt_nut_1}
    AppiumLibrary.Click Element    ${mt_nut_2}
    AppiumLibrary.Click Element    ${mt_nut_3}
    AppiumLibrary.Click Element    ${mt_nut_4}
    AppiumLibrary.Click Element    ${mt_nut_5}
    An nut dau cong (+)
    AppiumLibrary.Click Element    ${mt_nut_6}
    AppiumLibrary.Click Element    ${mt_nut_7}
    AppiumLibrary.Click Element    ${mt_nut_8}
    AppiumLibrary.Click Element    ${mt_nut_9}
    Sleep    2
    AppiumLibrary.Go Back
    Sleep    2
    Mo May tinh tu Launcher
    Sleep    2
    ${get_text3}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match    ${mt_hien_thi_phep_tinh}     text    *12**345+6**789*

An nut 123456789 check vi tri con tro chuot
    [Documentation]    An nut 123456789 tu ban phim so
    AppiumLibrary.Click Element    ${mt_nut_1}
    AppiumLibrary.Click Element    ${mt_nut_2}
    AppiumLibrary.Click Element    ${mt_nut_3}
    AppiumLibrary.Click Element    ${mt_nut_4}
    AppiumLibrary.Click Element    ${mt_nut_5}
    AppiumLibrary.Click Element    ${mt_nut_6}
    AppiumLibrary.Click Element    ${mt_nut_7}
    AppiumLibrary.Click Element    ${mt_nut_8}
    AppiumLibrary.Click Element    ${mt_nut_9}
    ${get_text1}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    # AppiumLibrary.Click Element At Coordinates    28    313
    AppiumLibrary.Click Element At Coordinates    20    90
    AppiumLibrary.Click Element    ${mt_nut_9}
    ${get_text2}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match   ${mt_hien_thi_phep_tinh}    text    *9**123**456**789*
    AppiumLibrary.Click Element At Coordinates    1029    306


Xoa so theo vi tri da chon
    Click xoa so
    ${get_text3}=    AppiumLibrary.Get Text    ${mt_hien_thi_phep_tinh}
    Element Attribute Should Match   ${mt_hien_thi_phep_tinh}    text    912**345**678*

Thuc hien nhap so
    AppiumLibrary.Click Element    ${mt_nut_4}
    AppiumLibrary.Click Element    ${mt_nut_5}

Thuc hien click ngoac phai
    Click Element    ${mt_nut_ngoac_phai}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================