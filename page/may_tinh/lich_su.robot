*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    mt_common.robot
Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Vuot mo giao dien lich su
    [Documentation]    Vuot man hinh tu trai sang phai
    Log To Console    Vuot man hinh tu trai sang phai
    Swipe By Percent    15    80    89    80
    sleep    2

Xoa lich su
    [Documentation]    Xoa lich su
    Log To Console     Xoa lich su
    AppiumLibrary.Click Element    ${mt_nut_xoa_lich_su}

Dong lich su bang thao tac vuot
    [Documentation]    vut man hinh tu phai qua trai
    Log To Console    vut man hinh tu phai qua trai
    Swipe By Percent    70    80    15    80


Bam vao nut lich su
    [Documentation]    bam vao lich su
    Log To Console     bam vao lich su
    AppiumLibrary.Click Element      ${mt_nut_lich_su}
    Sleep    2

Bam ket qua trong lich su
    [Documentation]    bam vao ket qua lich su
    Log To Console     bam vao ket qua lich su
    AppiumLibrary.Click Element    ${mt_nut_ket_qua_ls}

Bam vao bieu thuc trong lich su
    [Documentation]    bieu thuc trong lich su
    Log To Console      bieu thuc trong lich su
    AppiumLibrary.Click Element    ${mt_nut_bieu_thuc_ls}

Bam vao nut bang
    [Documentation]    bam nut bang
    Log To Console      bam nut bang
    AppiumLibrary.Click Element    ${mt_nut_bang}

Bam vao phep tinh
    [Documentation]    bam vao phep tinh
    Log To Console     bam vao phep tinh
    AppiumLibrary.Click Element    ${mt_nut_cong}

Thuc hien phep tinh
    AppiumLibrary.Click Element    ${mt_nut_2}
    AppiumLibrary.Click Element    ${mt_nut_cong}
    AppiumLibrary.Click Element    ${mt_nut_2}
    AppiumLibrary.Click Element    ${mt_nut_bang}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================