*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Check kiem tra giao dien man hinh tong the tu launcher
    [Documentation]    Check kiem tra giao dien man hinh tong the
    Log To Console    Check kiem tra giao dien man hinh tong the
    Chup anh man hinh va so sanh voi anh mau    ${ht_alias}    kc_d_ttdh_d_d_Hotro_Main    3
    Sleep    2
    Click 7 lan vao avatar dong & mo tuy chon
    Sleep    2
    Page Should Contain Element    ${ht_icon_tuy_chon_khac}
    Sleep    1
    Click vao tuy chon cai dat
    Sleep    2
    Log To Console    Check kiem tra giao dien man hinh tong the co nut tuy chon
    Chup anh man hinh va so sanh voi anh mau    ${ht_alias}    kc_d_ttdh_d_d_nut_TuyChon    3
    Sleep    2
    Log To Console    Check kiem tra giao dien man hinh tong the tab nhat ky QXDM
    Click vao tab nhat ky QXDM
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${ht_alias}    kc_d_ttdh_d_d_tab_NhatKy_QXDM    3
    Sleep    2
    Go Back
    Log To Console    Check kiem tra giao dien man hinh tong the bat nhat ky
    Click vao tuy chon cai dat
    Sleep    2
    Click vao tab nhat ky
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${ht_alias}    kc_d_ttdh_d_d_tab_Bat_NhatKy    3
    Sleep    2
    Go Back
    Go Back
    Mo Ho tro tu Launcher
    Sleep    1
    Page Should Contain Element    ${ht_icon_tuy_chon_khac}

Click 7 lan vao avatar dong & mo tuy chon
    Click Element    ${ht_icon_avatar}
    Sleep    2
    Click Element    ${ht_icon_avatar}
    Sleep    2
    Click Element    ${ht_icon_avatar}
    Sleep    2
    Click Element    ${ht_icon_avatar}
    Sleep    2
    Click Element    ${ht_icon_avatar}
    Sleep    2
    Click Element    ${ht_icon_avatar}
    Sleep    2
    Click Element    ${ht_icon_avatar}
    Sleep    1

Click 7 lan vao avatar dong
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}

Click lien tuc vao avatar kiem tra crash
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Sleep    1
    Page Should Not Contain Element    ${ht_icon_tuy_chon_khac}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Click Element    ${ht_icon_avatar}
    Sleep    1
    Page Should Contain Element    ${ht_icon_tuy_chon_khac}

Click toa do 7 lan vao avatar dong & mo tuy chon
    Click Element At Coordinates    500    550
    Sleep    2
    Click Element At Coordinates    500    550
    Sleep    2
    Click Element At Coordinates    500    550
    Sleep    2
    Click Element At Coordinates    500    550
    Sleep    2
    Click Element At Coordinates    500    550
    Sleep    2
    Click Element At Coordinates    500    550
    Sleep    2
    Click Element At Coordinates    500    550
    Sleep    1

Click 6 lan vao avatar check khong hien thi icon tuy chon
    Click Element    ${ht_icon_avatar}
    Sleep    2
    Click Element    ${ht_icon_avatar}
    Sleep    2
    Click Element    ${ht_icon_avatar}
    Sleep    2
    Click Element    ${ht_icon_avatar}
    Sleep    2
    Click Element    ${ht_icon_avatar}
    Sleep    2
    Click Element    ${ht_icon_avatar}
    Sleep    2
    Page Should Not Contain Element    ${ht_icon_tuy_chon_khac}
    Sleep    2
    Click Element    ${ht_icon_avatar}
    Sleep    2

Click vao tuy chon cai dat
    Click Element    ${ht_icon_tuy_chon_khac}
    Sleep    1

Click vao tab nhat ky QXDM
    Click Element    ${ht_btn_tab_nhat_ky_QXDM}
    Sleep    1

Click vao tab nhat ky
    Click Element    ${ht_btn_tab_bat_nhat_ky}
    Sleep    1


Click vao tab icon goi dien
    Click Element    ${ht_btn_icon_goi_dien}
    Sleep    1

Click vao tab icon bphoto
    Click Element    ${ht_btn_icon_bphoto}
    Sleep    1

Click vao tab icon Bstore
    Click Element    ${ht_btn_icon_Bstore}
    Sleep    1

Click vao tab icon phu kien
    Click Element    ${ht_btn_icon_phu_kien}
    Sleep    1

Click vao tab xuat trang thai may
    Click Element    ${ht_btn_xuat_trang_thai_may}
    Sleep    1

Click vao tab cap nhat he thong offline
    Click Element    ${ht_btn_tab_cap_nhat_offline}
    Sleep    1

Click vao dong y cap nhat offline
    Click Element    ${nut_dong_y_hoa}
    Sleep    1

Click vao dong thoat cap nhat offline
    Click Element    ${nut_dong_hoa}
    Sleep    1

Click vao btn bat dau chay diag_logs
    Click Element    ${ht_btn_bat_dau}
    Sleep    1

Click vao btn dung lai chay diag_logs
    Click Element    ${ht_btn_dung_lai}
    Sleep    1
Click vao btn dung xoa nhat ky
    Click Element    ${ht_btn_xoa_nhat_ky}
    Sleep    1

Click vao btn dung bao cao loi
    Click Element    ${ht_btn_bao_cao_loi}
    Sleep    1

Click vao btn thong bao loi bkav
    Click Element    ${ht_btn_thong_bao_bkav}
    Sleep    1

Click toa do vao tab tat ca
    Click Element At Coordinates    ${ht_click_toa_do_x}    ${ht_click_toa_do_y}
    Sleep    1

Cho phep chon trinh duyet chim lac & luon chon
    Log To Console    Cho phep cap quyen cho ung dung
    ${kiem_tra_popup1} =    Run Keyword And Return Status    AppiumLibrary.Wait Until Page Contains Element    ${ht_txt_chim_lac}    timeout=20
    Run Keyword If    ${kiem_tra_popup1}==True    AppiumLibrary.Click Element    ${ht_txt_chim_lac}
    Sleep    2

Cho phep luon chon trinh duyet chim lac
    ${kiem_tra_popup2}    AppiumLibrary.Get Matching Xpath Count    ${ht_btn_luon_chon}
    Run Keyword If    ${kiem_tra_popup2}>0    AppiumLibrary.Click Element    ${ht_btn_luon_chon}
    Sleep    3

Bat che do thu gon trong chim lac
    ${kiem_tra_popup1}    AppiumLibrary.Get Matching Xpath Count    ${nut_bat_che_do_thu_gon}
    Run Keyword If    ${kiem_tra_popup1}>0    AppiumLibrary.Click Element    ${nut_bat_che_do_thu_gon}
    Sleep    3

Kiem tra hien thi text phu kien op lung
    ${get_text}=    Get Text    ${ht_txt_phu_kien_op_lung}
    Element Attribute Should Match    ${ht_txt_phu_kien_op_lung}    text    ${ht_phu_kien_op_lung}
    Sleep    1

Kiem tra hien thi so dien thoai ho tro
    ${get_text}=    Get Text    ${ht_txt_so_dien_thoai}
    Element Attribute Should Match    ${ht_txt_so_dien_thoai}    text    ${ht_sdt}
    Sleep    1

check hien thi text phu kien op lung
    Log To Console    Cho phep cap quyen cho ung dung
    ${kiem_tra_popup2} =    Run Keyword And Return Status    AppiumLibrary.Wait Until Page Contains Element    ${ht_phu_kien_op_lung}    timeout=20
    Run Keyword If    ${kiem_tra_popup2}==True    Kiem tra hien thi text phu kien op lung
    Sleep    2

check hien thi link Bphoto tai chim lac
    Log To Console    Cho phep cap quyen cho ung dung
    ${kiem_tra_popup3} =    Run Keyword And Return Status    AppiumLibrary.Wait Until Page Contains Element    ${ht_link_bphoto}    timeout=20
    Run Keyword If    ${kiem_tra_popup3}==True    Kiem tra hien thi text phu kien op lung
    Sleep    2

check hien thi link Bstore tai chim lac
    Log To Console    Cho phep cap quyen cho ung dung
    ${kiem_tra_popup4} =    Run Keyword And Return Status    AppiumLibrary.Wait Until Page Contains Element    ${ht_link_Bstore}    timeout=20
    Run Keyword If    ${kiem_tra_popup4}==True    Kiem tra hien thi link Bstore tai chim lac
    Sleep    2

Kiem tra hien thi link Bstore tai chim lac
    ${get_text}=    Get Text    ${cl_url_bar}
    Element Attribute Should Match    ${cl_url_bar}    text    ${ht_link_Bstore}
    Sleep    1

Kiem tra hien thi link Bphoto tai chim lac
    ${get_text}=    Get Text    ${cl_url_bar}
    Element Attribute Should Match    ${cl_url_bar}    text    ${ht_link_bphoto}
    Sleep    1

Kiem tra khong hien thi nut tuy chon
    Page Should Not Contain Element    ${ht_icon_tuy_chon_khac}
    Sleep    1

Kiem tra khong hien thi diag_logs
    Page Should Not Contain Element    ${ht_txt_diag_logs}
    Sleep    1

Kiem tra khong hien thi quan ly files
    Page Should Not Contain Element    ${ht_vi_tri_file_ota}
    Sleep    1

Kiem tra hien thi text dang tao loi 1
    ${get_text}=    Get Text    ${ht_txt_bao_loi_1}
    Element Attribute Should Match    ${ht_txt_bao_loi_1}    text    *${ht_dang_tao_loi_1}**${ht_dang_tao_loi_1a}*
    Sleep    1

Kiem tra hien thi text dang tao loi 2
    ${get_text}=    Get Text    ${ht_txt_bao_loi_2}
    Element Attribute Should Match    ${ht_txt_bao_loi_2}    text    *${ht_dang_tao_loi_2}**${ht_dang_tao_loi_2a}*
    Sleep    1

Kiem tra hien thi vi tri quan ly files
    ${get_text}=    Get Text    ${ht_vi_tri_file_ota}
    Element Attribute Should Match    ${ht_vi_tri_file_ota}    text    ${ht_text_file_ota}
    Sleep    1

Kiem tra hien thi diag_logs tren quan ly file
    ${get_text}=    Get Text    ${ht_txt_diag_logs}
    Element Attribute Should Match    ${ht_txt_diag_logs}    text    ${ht_text_diag_logs}
    Sleep    1

Kiem tra khong hien thi check box bao loi
    Element Attribute Should Match    ${ht_click_check_box_bao_loi}    checked    false
    Sleep    1

Kiem tra khong hien thi check thong bao bkav
    Element Attribute Should Match    ${ht_click_check_box_thong_bao_bkav}    checked    false
    Sleep    1

Kiem tra hien thi check box bao loi
    Element Attribute Should Match    ${ht_click_check_box_bao_loi}    checked    true
    Sleep    1

Kiem tra hien thi check thong bao bkav
    Element Attribute Should Match    ${ht_click_check_box_thong_bao_bkav}    checked    true
    Sleep    1

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================