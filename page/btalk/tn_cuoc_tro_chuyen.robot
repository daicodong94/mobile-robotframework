*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    tin_nhan.robot

*** Keywords ***
Bam vao chi tiet cuoc tro truyen
    [Documentation]    bam vao chi tiet cuoc tro truyen va xac nhan mo thanh cong
    Click Element      ${name_lh_1}
    Sleep    2
    Page Should Contain Element    ${icon}
Check mo giao dien goi di khi cham vao lien he tin nhan
    [Documentation]    Check mo giao dien goi di khi cham vao lien he tin nhan
    Click Element    ${lien_he_tn}
    sleep    2
    Page Should Contain Element    ${incall}
    Sleep    2
    Click Element    ${incall}

Check cham vao dau ba cham
    [Documentation]    check cham vao dau ba cham
    Click Element At Coordinates    1000    150
    Sleep    2
    Page Should Contain Element     ${tck_them_lh.${dinh_vi}}

Check cham vao goi dien
    [Documentation]    check cham vao goi dien
    Click Element        ${tck_goi_dien.${dinh_vi}}
    Page Should Contain Element    ${incall}
    Click Element    ${incall}
    Sleep    2

Check cham vao lien he
    [Documentation]    check cham vao lien he
    Click Element        ${tck_them_lh.${dinh_vi}}
    Sleep    1
    Element Should Be Visible    ${them_lh}

Check cham vao tim kiem tin nhan
    [Documentation]    Check cham vao tim kiem tin nhan
    Click Element        ${tck_tim_kiem_tn.${dinh_vi}}
    Sleep    1
    Element Should Be Visible    ${xoa_truy_van}
    Page Should Contain Element    ${tim_kiem_tn}

Check hien thi thong bao khong tim thay tin nhan
    [Documentation]    Check hien thi thong bao khong tim thay tin nhan
    Input Text    ${tim_kiem_tn}       @
    Sleep    2
    Element Should Be Visible       ${khong_tim_thay.${dinh_vi}}

Check xoa noi dung tim kiem da nhap khi cham vao x
    [Documentation]     Check xoa noi dung tim kiem da nhap khi cham vao x
    Click Element    ${xoa_truy_van}
    Sleep    2
    Page Should Not Contain Element        ${khong_tim_thay.${dinh_vi}}

Check dong giao dien tim kiem tin nhan khi cham vao vao dau x
    [Documentation]     Check dong giao dien tim kiem tin nhan khi cham vao vao dau x
    Click Element    ${xoa_truy_van}
    Sleep    2
    Page Should Contain Element    ${icon}

Check mo giao dien lien he khi cham vao dinh kem lien he
    [Documentation]     Check mo giao dien lien he khi cham vao dinh kem lien he
    Click Element    ${tck_dimh_kem_lh.${dinh_vi}}
    Sleep    2
    Page Should Contain Element    ${tim_kiem_lh}

Check mo giao dien tim kiem lien he khi cham vao kinh lup
    [Documentation]    Check mo giao dien tim kiem lien he khi cham vao kinh lup
    Click Element    ${tim_kiem_lup}
    Sleep    2
    Element Should Be Visible    ${xoa_truy_van_lh}

Check hien thi thong bao khong tim thay lien he
    [Documentation]    Check hien thi thong bao khong tim thay lien he
    Input Text   ${tim_lien_he}   @
    Sleep    2
    Element Should Be Visible       ${khong_thay_Lh}

Check xoa noi dung tim kiem da nhap khi cham vao dau x
    [Documentation]    Check xoa noi dung tim kiem da nhap khi cham vao dau x
    Click Element     ${xoa_truy_van_lh}
    Page Should not Contain Element     ${khong_thay_Lh}

Check dong giao dien lien he khi cham vao dau x
    [Documentation]    Check dong giao dien lien he khi cham vao dau x
    Click Element     ${xoa_truy_van_lh}
    Page Should Contain Element     ${tim_kiem_lup}
    Sleep    2

Check dong giao dien lien he khi cham vao mui ten Back
    [Documentation]    Check dong giao dien lien he khi cham vao mui ten Back
    Click Element    ${back_exit_lh.${dinh_vi}}
    Sleep    2
    Page Should Contain Element     ${go_noi_dung}

Check them chu de vao go noi dung tin nhan khi cham vao them chu de
    [Documentation]    Check them chu de vao go noi dung tin nhan khi cham vao them chu de
    Click Element     ${tck_them_chu_de.${dinh_vi}}
    Sleep    2
    Page Should Contain Element    ${xoa_chu_de}
    Page Should Contain Element    ${txt_chu_de}

Check hien thi trang thai tin nhan MMS khi nhap noi dung chu de
    [Documentation]    Check hien thi trang thai tin nhan MMS khi nhap noi dung chu de
    Input Text    ${txt_chu_de}    ${text1}
    sleep    1
    Page Should Contain Element    ${MMS}
    Clear Text    ${txt_chu_de}

Check so ki tu cho phep nhap toi da o phan chu de
    [Documentation]    Check so ki tu cho phep nhap toi da o phan chu de
    Input Text     ${txt_chu_de}    ${text2}
    Sleep    2
    Element Attribute Should Match     ${txt_chu_de}    text     ${text3}

Check dong truong hop nhap du lieu chu de khi cham vao x
    [Documentation]    Check dong truong hop nhap du lieu chu de khi cham vao x
    Click Element     ${xoa_chu_de}
    Sleep    1
    Page Should Not Contain Text     ${xoa_chu_de}

Check gui tin nhan co chu de
    [Documentation]    Check gui tin nhan co chu de
    Input Text     ${txt_chu_de}    ${text1}
    Input Text     ${go_noi_dung}    ${text1}
    Click Element    ${gui_tin_nhan}
    Sleep    3
    Page Should Contain Text     ${text1}

Check luu tru cuoc tro chuyen khi cham vao luu tru
    [Documentation]    Check luu tru cuoc tro chuyen khi cham vao luu tru
    Click Element    ${tck_luu_tru.${dinh_vi}}
    Sleep    3
    Page Should Not Contain Element    ${text1}
    Sleep    1

Check hien thi cuoc tro chuyen da luu trong danh sach luu tru
    [Documentation]    Check hien thi cuoc tro chuyen da luu trong danh sach luu tru
    Click Element    ${da_luu_tru.${dinh_vi}}
    Sleep    2
    Page Should Contain Text    ${text1}

Check huy cuoc tro chuyen da luu trong danh sach luu tru
    [Documentation]    Check huy cuoc tro chuyen da luu trong danh sach luu tru
    Click Element    bkav.android.btalk:id/swipeableContent
    Click Element At Coordinates    400    1200
    Sleep    2
    Click Element     ${huy_luu_tru.${dinh_vi}}

Check mo giao dien cai dat khi cham vao cai dat
    [Documentation]    Check mo giao dien cai dat khi cham vao cai dat
    Click Element       ${tck_cai_dat.${dinh_vi}}
    Page Should Contain Text    Chim hót

Check hien thi popup chan x khi cham vao chan X
    [Documentation]    Check hien thi popup chan x khi cham vao chan X
    Click Element At Coordinates    140    915
    Sleep    1
    Page Should Contain Element    ${dh_btn_dong_y}
    Page Should Contain Element    ${dh_btn_huy}

Check dong popup chan x khi cham vao huy
    [Documentation]    Check hien thi popup chan x khi cham vao chan X
    Click Element    ${dh_btn_huy}
    Sleep    1
    Page Should not Contain Element    ${dh_btn_huy}

Check chan so dien thoai x khi cham vao dong y
    [Documentation]    Check chan so dien thoai x khi cham vao dong y
    Click Element     ${dh_btn_dong_y}
    Sleep    2
    Page Should not Contain Element    ${dh_btn_huy}
    Page Should Not Contain Text      ${text1}

Check mo giao dien chi tiet lien he khi cham vao sdt hoac avata
    [Documentation]    Check mo giao dien chi tiet lien he khi cham vao sdt hoac avata
    Click Element     ${avata_lien_he}
    Sleep    2
    Page Should Contain Element    ${ten_lien_he}
    Page Should not Contain Element    ${nut_bat_tat}
    Swipe By Percent    0    50    20    50

Check dong giao dien cai dat khi cham vao mui ten back
    [Documentation]    Check dong giao dien cai dat khi cham vao mui ten back
    Click Element At Coordinates    50    168
    Page Should not Contain Element     ${nut_bat_tat}
    Sleep    2

Check mo giao dien chinh sua phan hoi nhanh
    [Documentation]    Check mo giao dien chinh sua phan hoi nhanh
    Click Element        ${tck_chinh_sua_phn.${dinh_vi}}
    Sleep    2
    Page Should Contain Element    ${dh_btn_dong_y}

Check dong popup phan hoi nhanh khi cham vao huy
    [Documentation]    Check dong popup phan hoi nhanh khi cham vao huy
    Click Element    ${dh_btn_huy}

Check mo popup phan hoi nhanh khi cham vao +
    [Documentation]    Check mo popup phan hoi nhanh khi cham vao +
    Click Element    ${add_phan_hoi}
    Sleep    3
    Page Should Contain Element    ${dh_btn_dong_y}

Check luu noi dung phan hoi nhanh
    [Documentation]    Check luu noi dung phan hoi nhanh
    Input Text    ${Noi_dung_phn}    ${text1}
    Sleep    1
    Click Element    ${dh_btn_dong_y}
    Sleep    2
    Page Should Contain Text    ${text1}

Check luu sau khi thay doi noi dung phan hoi nhanh
    [Documentation]    Check luu sau khi thay doi noi dung phan hoi nhanh
    Click Element     ${Noi_dung_phn}
    Clear Text     ${Noi_dung_phn}
    Click Element    ${dh_btn_dong_y}
    Sleep    2
    Page Should Not Contain Text     ${text1}

Check xoa noi dung phan hoi nhanh
    [Documentation]      Check xoa noi dung phan hoi nhanh
    Click Element     ${add_phan_hoi}
    Input Text    ${Noi_dung_phn}    Missyou
    Click Element    ${dh_btn_dong_y}
    Sleep    2
    Click Element     ${add_phan_hoi}
    Clear Text    ${Noi_dung_phn}
    Sleep    1
    Page Should Not Contain Text    Missyou

Check dong giao dien phan hoi nhanh khi cham
    [Documentation]      Check dong giao dien phan hoi nhanh khi cham
    Click Element     ${Nut_back_phn.${dinh_vi}}
    Page Should Not Contain Element     ${Nut_back_phn.${dinh_vi}}
    Page Should Not Contain Element      ${add_phan_hoi}

Check mo popup xoa nhung cuoc tro chuyen khi cham vao xoa
    [Documentation]    Check mo popup xoa nhung cuoc tro chuyen khi cham vao xoa
    Click Element     ${tck_xoa.${dinh_vi}}
    Page Should Contain Element     ${dh_btn_dong_y}

Check dong popup xoa khi cham vao huy
    [Documentation]    Check dong popup xoa khi cham vao huy
    Click Element     ${dh_btn_huy}
    Page Should not Contain Element     ${dh_btn_dong_y}

Check xoa cuoc tro chuyen khi cham vao xoa
    [Documentation]    Check xoa cuoc tro chuyen khi cham vao xoa
    Click Element    ${dh_btn_dong_y}
    Page Should not Contain Element     ${dh_btn_dong_y}
    Sleep    2
    # Page Should Contain Text            ${da_xoa_tb.${dinh_vi}}

Check hien thi mac dinh cua cuoc tro chuyen khi loc tap ghi am
    [Documentation]    Check hien thi mac dinh cua cuoc tro chuyen khi loc tap ghi am
    Click Element     ${tck_loc_tap_ga.${dinh_vi}}
    Sleep    2
    Page Should Contain Element    ${phat_tep_ga}

Check hien thi cuoc tro chuyen khi chon loc tep ghi am trong truong hop cuoc tro chuyen chi co sms
    [Documentation]    Check hien thi cuoc tro chuyen khi chon loc tep ghi am trong truong
    ...    hop cuoc tro chuyen chi co sms
    Vuot back ve giao dien truoc
    Click Element       ${name_lh}
    Check cham vao dau ba cham
    Click Element     ${tck_loc_tap_ga.${dinh_vi}}
    Page Should not Contain Element    ${phat_tep_ga}
    Page Should not Contain Element    ${noi_dung_tn}

Check hien thi cuoc tro truyen khi chon loc tin nhan sms
    [Documentation]    Check hien thi cuoc tro truyen khi chon loc tin nhan sms
    ...     trong truong hop co ca tep ghi am va tin nhan
    Vuot back ve giao dien truoc
    Click Element       ${name_lh}
    Sleep    2
    Check cham vao dau ba cham
    Click Element     ${tck_loc_tn_sms.${dinh_vi}}

Check mo popup chuyen tiep thu khi cham vao icon chuyen tiep
    [Documentation]    Check mo popup chuyen tiep thu khi cham vao icon chuyen tiep tren thanh cong cu
    Long Press     ${noi_dung_tn}      2000
    Click Element    ${chuyen_tiep}
    Sleep    2
    Page Should Contain Element    ${logo_tn}

Check cham vao giao dien tim kiem tin nhan khi cham vao kinh lup
    [Documentation]    Check cham vao giao dien tim kiem tin nhan khi cham vao kinh lup
    Click Element   ${tim_kiem_ct}
    Sleep    1
    Page Should Contain Element    ${tim_kiem_tn}
    Page Should Contain Element    ${xoa_truy_van}

Check hien thi thong bao khong tim thay tin nhan trong chuyen tiep
    [Documentation]    Check hien thi thong bao khong tim thay tin nhan khi nhap tu khoa
    ...     khong trung voi  bat ki tu khao nao trong danh sach
    Input Text    ${tim_kiem_tn}       @@
    Sleep    2
    Page Should Contain Element   ${text_ko_thay_tn}
Check xoa noi dung da nhap khi cham vao x
    [Documentation]    Check xoa noi dung da nhap khi cham vao x
    Click Element    ${xoa_truy_van}
    Sleep    1
    Element Should Not Contain Text     ${tim_kiem_tn}     @@

Check dong giao dien tim kien tn khi cham vao x
    [Documentation]    Check xoa noi dung da nhap khi cham vao x
    Click Element    ${xoa_truy_van}
    Sleep    1
    Page Should not Contain Element    ${xoa_truy_van}

Check mo giao dien soan tin nhan khi cham vao icon them moi
    [Documentation]    Check mo giao dien soan tin nhan khi cham vao icon them moi
    Click Element    ${them_moi}
    Sleep    1
    Page Should Contain Element    ${gui_tin_nhan}

Check mo popup chi tiet tin nhan
    [Documentation]    Check mo popup chi tiet tin nhan khi cham vao icon chi tiet tin nhan
    Long Press     ${noi_dung_tn}      2000
    Click Element    ${chi_tiet}
    Sleep    2
    Page Should Contain Element    ${popup_chi_tiet}

Check mo popup tuy chon trong thanh cong cu
    [Documentation]    Check mo popup chi tiet tin nhan khi cham vao icon chi tiet tin nhan
    Long Press     ${noi_dung_tn}      2000
    Click Element    ${chi_tiet}
    Sleep    2
    Page Should Contain Element    ${popup_chi_tiet}

Check mo tuy chon khi cham vao dau ba cham truong hop dang chon 1 tin nhan
    [Documentation]       Check mo tuy chon khi cham vao dau ba cham truong hop dang chon 1 tin nhan
    Long Press    ${noi_dung_tn}      2000
    Sleep    2
    Click Element At Coordinates    1015    145
    Sleep    1
    Page Should Contain Element    ${xoa_thanh_cc.${dinh_vi}}

Check mo popup xoa tin nhan nay khi cham vao xoa
    [Documentation]    Check mo popup xoa tin nhan nay khi cham vao xoa
    Click Element    ${xoa_thanh_cc.${dinh_vi}}
    Page Should Contain Element    ${dh_btn_huy}

Check xoa tin nhan khi cham vao xoa
    [Documentation]    Check xoa tin nhan khi cham vao xoa
    Long Press     ${noi_dung_tn}      2000
    Click Element At Coordinates    1015    145
    Sleep    1
    Click Element    ${xoa_thanh_cc.${dinh_vi}}
    Click Element    ${dh_btn_dong_y}
    Sleep    1
    Page Should not Contain Element       ${dh_btn_dong_y}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================