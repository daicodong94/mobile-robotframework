*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot

*** Variables ***

*** Keywords ***

bam vao dau 3 cham trong giao dien dien thoai
    [Documentation]    bam vao dau 3 cham trong giao dien dien thoai
    Click Element    ${dt_3_cham}
    Sleep    1

bam vao tin nhan trong dau 3 cham trong tin nhan
    [Documentation]    bam vao tin nhan trong dau 3 cham
    Click Element    ${tn_3_cham}
    Sleep    1

bam vao tin nhan
    [Documentation]    bam vao tin nhan
    Click Element     ${tn_btn_tin_nhan}
    Sleep    2

Bam vao cai dat trong tin nhan
    [Documentation]    Bam vao cai dat trong tin nhan
    Click Element    ${tn_3_cai_dat}
    Sleep    1

Check mo giao dien ung dung SMS mac dinh
    [Documentation]    Check mo giao dien ung dung SMS mac dinh
    Click Element    ${tn_cd_sms_mac_dinh}
    Sleep    1

Check luu lua chon khi an chon messenger lam ung dung mac dinh
    [Documentation]    Check luu lua chon khi an chon messenger lam ung dung mac dinh
    Click Element    ${tn_cd_sms_md_messenger}
    Sleep    1
Check disable cai dat thong bao tin nhan
    [Documentation]    Check disable cai dat thong bao tin nhan khi dat ung dung khac lam ung dung mac dinh
    Element Attribute Should Match    ${tn_cd_thong_bao_tn}    enabled    false
    Sleep    1

Check mo popup
    [Documentation]    Check mo popup ban muon dat btalk lam ung dung sms mac dinh
    Click Element    ${tn_cd_sms_mac_dinh}
    Sleep    2
    Page Should Contain Text    ${tn_txt_dat_sms_mac_dinh}
    Sleep    1

Check dong popup
    [Documentation]    Check dong popup ban muon chon btalk lam sms mac dinh khi cham vao huy
    Click Text   ${tn_txt_huy}
    Sleep    1

Check luu lua chon khi dat btalk lam ung dung sms mac dinh
    [Documentation]    Check luu lua chon khi dat btalk lam ung dung sms mac dinh
    Click Element    ${tn_cd_sms_mac_dinh}
    Sleep    1
    Click Element    ${tn_cd_btalk}
    Sleep    1
    Click Text   ${tn_txt_dat_lam_mac_dinh}
    Sleep    1

Check enable cai dat thong bao tin nhan
    [Documentation]       Check enable cai dat thong bao tin nhan sau khi dat btalk lam ung dung sms mac dinh
    Element Attribute Should Match    ${tn_cd_thong_bao_tn}    enabled    true
    Sleep    1

Check hien thi thong bao trong giao dien tin nhan
    [Documentation]    Check hien thi thong bao trong giao dien tin nhan sau khi chon ung dung khac lam ung dung sms mac dinh
    Click Element    ${tn_cd_sms_mac_dinh}
    Sleep    1
    click element     ${tn_cd_sms_md_messenger}
    Sleep    2
    Vuot back ve giao dien truoc
    Vuot back ve giao dien truoc
    Sleep    1
    Page Should Contain Element    ${tn_cd_sms_btalk_mac_dinh}

Check mo popup khi cham vao thay doi
    [Documentation]    Check mo popup ban muon chon btalk lam ung dung mac dinh khi cham vao thay doi
    Click Element    ${tn_cd_sms_thay_doi}
    Sleep    2
    Page Should Contain Text    ${tn_txt_dat_sms_mac_dinh}

Check dong popup khi cham vao huy
    [Documentation]    Check dong popup khi cham vao huy
    Click Text   ${tn_txt_huy}
    Sleep    2

Check luu lua chon khi chon sms lam ung dung mac dinh
    [Documentation]    Check luu lua chon khi chon sms lam ung dung mac dinh
    Click Element    ${tn_cd_sms_thay_doi}
    Sleep    1
    Click Element    ${tn_cd_btalk}
    Sleep    1
    Click Text   ${tn_txt_dat_lam_mac_dinh}
    Sleep    1

Check an thong bao dat btalk lam ung dung mac dinh
    [Documentation]    Check an thong bao dat btalk lam ung dung mac dinh
    Page Should Not Contain Text    Để gửi tin nhắn hãy đặt Btalk là ứng dụng nhắn tin SMS mặc định

Check mo popup ban muon chon sms mac dinh khi cham vao thay doi
    [Documentation]    Check mo popup ban muon chon sms mac dinh khi cham vao thay doi
    Click Element    ${tn_cd_sms_mac_dinh}
    Sleep    1
    Click Element    ${tn_cd_sms_md_messenger}
    Sleep    1
    Vuot back ve giao dien truoc
    Vuot back ve giao dien truoc
    Sleep    1
    Click Element    ${tn_cd_sms_thay_doi}
    Sleep    1
    Click Text   ${tn_txt_huy}
    Sleep    1

Check luu lua chon khi dat btalk lam ung dung mac dinh
    Click Element    ${tn_cd_sms_thay_doi}
    Sleep    1
    Click Element    ${tn_cd_btalk}
    Sleep    1
    Click Text   ${tn_txt_dat_lam_mac_dinh}
    Sleep    1

Check mo giao dien cai dat thong bao tin nhan
    Click Element    ${tn_cd_thong_bao_tn}
    Sleep    2
    Page Should Contain Element    ${tn_cd_tb_loai_tb}

Check mo rong danh sach cai dat khi cham vao nang cao chon canh bao
    Click Element    ${tn_cd_tb_canh bao}
    Sleep    1
    Click Element    ${tn_cd_tb_nang_cao}
    Sleep    1
    Swipe By Percent    50    90    50    20
    Page Should Contain Element    ${tn_cd_tb_am_bao}
    Page Should Contain Element    ${tn_cd_tb_rung}
    Page Should Contain Element    ${tn_cd_tb_man_hinh khoa}
    Page Should Contain Element    ${tn_cd_tb_nhap_nhay_den}
    Page Should Contain Element    ${tn_cd_tb_ht_dc_tn}
    Page Should Contain Element    ${tn_cd_tb_ghi_de}
    Sleep    1

Check hien thi am thanh thong bao mac dinh khi de mac dinh
    Page Should Contain Text    ${tn_txt_am_thanh_mac_dinh}

Check mo rong danh sach cai dat khi cham vao nang cao chon im lang
    Vuot back ve giao dien truoc
    Click Element    ${tn_cd_thong_bao_tn}
    Sleep    2
    Click Element    ${tn_cd_tb_im_lang}
    Sleep    1
    Click Element    ${tn_cd_tb_nang_cao}
    Sleep    1
    Swipe By Percent    50    90    50    20
    Page Should Contain Element    ${tn_cd_tb_thu_nho}
    Page Should Contain Element    ${tn_cd_tb_man_hinh khoa}
    Page Should Contain Element    ${tn_cd_tb_ht_dc_tn}
    Page Should Contain Element    ${tn_cd_tb_ghi_de}

Check an man hinh khoa khi chon im lang va bat che do thu nho
    Click Element    ${tn_cd_tb_thu_nho}
    Sleep    1
    Page Should not Contain Element    ${tn_cd_tb_man_hinh khoa}

Check mo popup man hinh khoa khi cham vao man hinh khoa
    Click Element    ${tn_cd_tb_thu_nho}
    Sleep    1
    Click Element    ${tn_cd_tb_man_hinh khoa}
    Sleep    1
    Element Attribute Should Match    ${tn_cd_tb_mhk_text1}    enabled    true

Check dong popup man hinh khi cham vao huy
    Click Text    ${tn_txt_huy}

Check mo giao dien cai dat chung khi cham vao bo sung trong ung dung
    Swipe By Percent    50    90    50    50
    Click Element     ${tn_cd_tb_bo_sung_ung_dung}
    Sleep    1
    Page Should Contain Element    ${tn_cd_btalk}

Check an avata o moi tin nhan o trang thai mac dinh
    [Documentation]    Check an avata o moi tin nhan o trang thai mac dinh
    Click Element    ${tn_lien_he}
    Sleep    1
    Page Should not Contain Element    ${tn_cd_avata_icon}

Check hien thi avata o moi tin nhan sau khi bat
    Vuot back ve giao dien truoc
    bam vao tin nhan trong dau 3 cham trong tin nhan
    Bam vao cai dat trong tin nhan
    Click Element    ${tn_cd_hien_avata}
    Vuot back ve giao dien truoc
    Click Element    ${tn_lien_he}
    Sleep    1
    Page Should Contain Element    ${tn_cd_avata_icon}

Check an hien thi avata o moi tin nhan sau khi tat
    Vuot back ve giao dien truoc
    bam vao tin nhan trong dau 3 cham trong tin nhan
    Bam vao cai dat trong tin nhan
    Click Element    ${tn_cd_hien_avata}
    Vuot back ve giao dien truoc
    Click Element    ${tn_lien_he}
    Sleep    1
    Page Should not Contain Element    ${tn_cd_avata_icon}

Check mo popup nhan tin theo nhom khi cham vao nhan tin theo nhom
    Click Element    ${tn_cd_nang cao}
    Sleep    1
    Click Element    ${tn_cd_nc_nhan_tin_theo_nhom}
    Sleep    1
    Page Should Contain Text    ${tn_txt_huy}

Check luu thay doi khi chon che do gui mms cho tat ca moi nguoi
    Click Element    ${tn_cd_nc_gui_mot_mms}
    Sleep    1
    Page Should not Contain Text    ${tn_txt_huy}
    Sleep    1
    Page Should Contain Text        ${tn_txt_gui_mms}

Check dong popup khi cham vao huy o popup nhan tin theo nhom
     Click Element    ${tn_cd_nc_nhan_tin_theo_nhom}
     Sleep    1
     Click Text    ${tn_txt_huy}

Check hien thi so dien thoai khong xac dinh khi chua nhap so dien thoai
    Click Element    ${tn_cd_nc_so_dien_thoai}
    Sleep    1
    Page Should Contain Text    ${tn_txt_huy}
    Page Should Contain Text   ${tn_txt_dong y}

Chek dong popup so dien thoai khi cham vao huy
    Click Text      ${tn_txt_huy}
    Sleep    1

Check mo giao dien canh bao khan cap khi cham vao canh bao khong day
    Click Element    ${tn_cd_nc_canh_bao_khong_day}
    Sleep    1
    Page Should Contain Text    ${tn_txt_khong_co_thong_bao}

check dong giao dien lich su khi cham vao icon mui ten back
    Click Element    ${tn_cd_back}
    Sleep    1
    Page Should Not Contain Text    ${tn_txt_khong_co_thong_bao}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================