*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot

*** Variables ***
# Cac tham so mo ung dung

${btalk_alias}       dien_thoai
${btalk_package}     bkav.android.btalk	
${btalk_activity}    bkav.android.btalk.activities.BtalkActivity

${dt}     xpath= //*[@content-desc="Điện thoại"]
${dt_cai_dat}    	 bkav.android.btalk:id/dialpad_overflow

*** Keywords ***
    
Bam vao lien he dau tien trong tab gan day
    [Documentation]    Bam vao lien he gan day trong tab dien thoai
    Click Element    ${dt_tab_gan_day_lien_he1}
    Sleep    2    
    
Bam vao tab dien thoai
    [Documentation]    Bam vao tab dien thoai
    Click Element    ${btalk_dien_thoai}
    Sleep    2
    
Bam nut ket thuc cuoc goi
    [Documentation]    Bam nut ket thuc cuoc goi
    Click Element    ${dt_ket_thuc_cuoc_goi}    
    
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================