*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Mo dien thoai roi vao cai dat dien thoai
    [Documentation]    Mo ung dung Dien thoai va cho vai giay de ung dung load xong
    Mo Btalk
    Click Element    ${dt_cai_dat}
    Sleep    2
    Click Element    ${cd_nut_dien_thoai}
    Sleep    3

An nut cai dat dien thoai
    [Documentation]   An nut cai dat dien thoai
    Click Element    ${dt_cai_dat}
    Click Element    ${cd_nut_dien_thoai}
    Sleep    2

Bam vao dien thoai roi vao cai dat dien thoai
    [Documentation]      Bam vao dien thoai roi vao cai dat dien thoai
    Click Element    ${app_dien_thoai}
    Sleep    2
    Click Element    ${dt_cai_dat}
    Sleep    2
    Click Element    ${cd_nut_dien_thoai}
    Sleep    2

An nut back tro ve giao dien truoc
    [Documentation]    An nut back tro ve giao dien truov
    Click Element    ${dt_cd_back}

An nut tuy chon hien thi
    [Documentation]    An nut tuy chon hien thi
    Click Element    ${dt_cd_nut_tuy_chon_hien_thi}

An vao checkbox hien thi 2 nut goi cho 2 sim o man tuy chon hien thi
    [Documentation]     An vao checkbox hien thi 2 nut goi cho 2 sim o man tuy chon hien thi
    Click Element    ${dt_cd_checkbox_tuy_chon_hien_thi}
    Sleep    2

An vao nut am thanh va rung
    [Documentation]    An vao nut am thanh va rung
    Click Element    ${dt_cd_nut_am_thanh_va_rung}
    Sleep    2

An nut nhac chuong dien thoai khi co 1 sim
    [Documentation]    AN nut nhac chuong dien thoai khi co 1 sim
    Click Element    ${dt_cd_nut_nhac_chuong_dien_thoai}
    Sleep    2

Bam vao nhac chuong khong
    [Documentation]    Bam vao nhac chuong khong
    Click Element    ${dt_cd_nut_nhac chuong_khong}
    Sleep    2

Bam vao dong y
    [Documentation]    Bam vao dong y
    Click Element    ${dt_cd_nut_dong_y}
    Sleep    3

Bam vao huy
    [Documentation]    Bam vao huy
    Click Element    ${dt_cd_nut_huy}
    Sleep    2

Bam vao huy bo
    [Documentation]    Bam vao huy bo
    Click Element    ${dt_cd_nut_huy_bo}
    Sleep    2
An vao nhac chuong hop nhac
    [Documentation]    An vao nhac chuong hop nhac
    Click Element    ${dt_cd_nut_nut_nhac_chuong_hop_nhac}
    Sleep    2

Mo dien thoai vao cai dat va vao am thanh va rung
    [Documentation]    Mo dien thoai vao cai dat va vao am thanh va rung
    Mo dien thoai roi vao cai dat dien thoai
    An vao nut am thanh va rung
    Sleep    2

Bam vao dien thoai vao cai dat va vao am thanh va rung
    [Documentation]       Bam vao dien thoai vao cai dat va vao am thanh va rung
    Bam vao dien thoai roi vao cai dat dien thoai
    An vao nut am thanh va rung
    Sleep    2


Vuot launcher danh sach nhac xuong down
    [Documentation]    Check vuot launcher tu tren xuong duoi
    AppiumLibrary.Swipe By Percent    50    40    50    70
    Sleep    2

An vao nut chon chi mot lan
    [Documentation]    An vao nut chon chi mot lan
    Click Element    ${dt_cd_nut_chi_mot_lan}
    Sleep    2

An vao nut luon chon
    [Documentation]    An vao nut luon chon
    Click Element    ${dt_cd_nut_luon_chon}
    Sleep    2

An vao nut bo nho phuong tien
    [Documentation]    An vao nut bo nho phuong tien
    Click Element    ${dt_cd_nut_bo_nho_phuong_tien}
    Sleep    2

An vao nut Quan li file
    [Documentation]    An vao nut quan li file
    Click Element    ${dt_cd_nut_quan_li_file}
    Sleep    2

An nut nhac chuong sim1
    [Documentation]    An nut nhac chuong sim 1
    Click Element    ${dt_cd_nut_nhac_chuong_sim1}
    Sleep    3

An nut nhac chuong sim2
    [Documentation]    An nut nhac chuong sim2
    Click Element    ${dt_cd_nut_nhac_chuong_sim2}
    Sleep    1

An checkbox Rung khi co cuoc goi
    [Documentation]    An nut rung khi co cuoc goi khi lap 1 sim
    Click Element     ${dt_cd_checkbox_rung_khi_co_cuoc_goi}
    Sleep    2

An checkbox Am ban phim so
    [Documentation]    An checkbox Am ban phim so khi lap 1sim
    Click Element    ${dt_cd_checkbox_am_ban_phim_so}
    Sleep    2

An nut tra loi nhanh
    [Documentation]    An nut tra loi nhanh
    Click Element    ${dt_cd_nut_tra_loi_nhanh}
    Sleep    2

Mo cai dat dien thoai va vao tra loi nhanh
    [Documentation]    Mo cai dat dien thoai va vao phan tra loi nhanh
    Mo dien thoai roi vao cai dat dien thoai
    An nut tra loi nhanh
    Sleep    2

Bam cai dat dien thoai va vao tra loi nhanh
    [Documentation]    Bam cai dat dien thoai va vao tra loi nhanh
    Bam vao dien thoai roi vao cai dat dien thoai
    An nut tra loi nhanh
    Sleep    2

An checkbox Hien thi tra loi nhanh
    [Documentation]    An checkbox Hien thi tra loi nhanh
    Click Element    ${dt_cd_nut_checkbox_tra_loi_nhanh}
     Sleep    2

Mo cai dat dien thoai va vao Tai khoan goi
    [Documentation]    Mo cai dat dien thoai va vao Tai khoan goi
    Mo dien thoai roi vao cai dat dien thoai
    An nut Tai khoan goi
    Sleep    2

Bam cai dat dien thoai va vao Tai khoan goi
    [Documentation]    Bam cai dat dien thoai va vao Tai khoan goi
    Bam vao dien thoai roi vao cai dat dien thoai
    An nut Tai khoan goi
    Sleep    2

An nut Tai khoan goi
    [Documentation]    An nut tai khoan goi
    Click Element    ${dt_cd_tai_khoan_goi}
    Sleep    2

An nut Tu dong ghi am cuoc goi
    [Documentation]    An nut tu dong ghi am cuoc goi
    Click Element    ${dt_cd_checkbox_tu_dong_ghi_am_cuoc_goi}
    Sleep    2

An nut Hinh nen cuoc goi
    [Documentation]    An nut hinh nen cuoc goi
    Click Element    ${dt_cd_checkbox_Hinh_nen_cuoc_goi}
    Sleep    2

An nut Hien thi thong tin nha mang trong cuoc goi
    [Documentation]    An nut Hien thi thong tin nha mang trong cuoc goi
    Click Element    ${dt_cd_Hien_thi_thong_tin_nha_mang_trong_cuoc_goi}
    Sleep    2

An nut hen gio cuoc goi
    [Documentation]    An nut hen gio cuoc goi
    Click Element    ${dt_cd_nut_hen_gio_cuoc_goi}
    Sleep    2

An nut dat thoi gian
    [Documentation]    An nut dat thoi gian
    Click Element    ${dt_cd_nut_dat_thoi_gian}
    Sleep    2

An nut Tat cuoc goi tu dong
    [Documentation]    An nut tat cuoc goi tu dong
    Click Element    ${dt_cd_checkbox_tat_cuoc_goi_tu_dong}
    Sleep    2

An nut Rung thong bao truoc 30s
    [Documentation]    An nut Rung thong bao truoc 30s
    Click Element    ${dt_cd_checkbox_rung_thong_bao_truoc_30s}
    Sleep    2

An nut Ap dung cho cuoc goi den
    [Documentation]    An nut Ap dung cho cuoc goi
    Click Element      ${dt_cd_checkbox_ap_dung_cho_ca_cuoc_goi_den}
    Sleep    2

An nut Thoi gian tu dong xoa ghi am cuoc goi
    [Documentation]    An nut Thoi gian tu dong xoa ghi am cuoc goi
    Click Element    ${dt_cd_nut_thoi_gian_tu_Dong_xoa_ghi_am_cuoc_goi}
    Sleep    2

An nut thuc hien cuoc goi bang
    [Documentation]    An nut thuc hien cuoc goi bang
    Click Element    ${dt_cd_nut_thuc_hien_cuoc_goi_bang}
    Sleep    2

An nut hoi truoc tai popup thuc hien cuoc goi bang
    [Documentation]    An nut hoi truoc tai popup thuc hien cuoc goi bang
    Click Element    ${dt_cd_checkbox_thuc_hien_cuoc_goi_bang_hoi_truoc}
    Sleep    2

An nut dat mac dinh thuc hien cuoc goi bang sim 1
    [Documentation]    An nut dat mac dinh thuc hien cuoc goi bang sim1
    Click Element    ${dt_cd_checkbox_thuc_hien_cuoc_goi_bang_sim1}

An nut dat mac dinh thuc hien cuoc goi bang sim 2
    [Documentation]    An nut dat mac dinh thuc hien cuoc goi bang sim2
    Click Element    ${dt_cd_checkbox_thuc_hien_cuoc_goi_bang_sim2}

Nhap sđt 0385986157
    [Documentation]    Nhap sđt 0385986157
    Click Element     ${nut_0}
    Click Element     ${nut_3}
    Click Element     ${nut_8}
    Click Element     ${nut_5}
    Click Element     ${nut_9}
    Click Element     ${nut_8}
    Click Element     ${nut_6}
    Click Element     ${nut_1}
    Click Element     ${nut_5}
    Click Element     ${nut_7}
    Sleep    2

An nut quay so
    [Documentation]    An nut quay so
    Click Element   ${dt_nut_quay_so}
    Sleep    1

An nut cai dat mo ung dung
    [Documentation]    An nut cai dat mo ung dung
    Click Element    ${dt_cd_nut_cd_mo_ung_dung}
    Sleep    2

An sang man tin nhan tren man hinh chinh btalk
    [Documentation]    An sang man tin nhan tren man hinh chinh btalk
    Click Element    ${tn_btn_tin_nhan}
    Sleep    2

An chon man dien thoai tren giao dien chinh btalk
    [Documentation]   An chon man dien thoai tren giao dien chinh btalk
    Click Element   ${dt}
    Sleep    2

An nut checkbox luu vi tri tab khi thoat ung dung
    [Documentation]    An nut luu vi tri tab khi thoat ung dung
    Click Element    ${dt_cd_checkbox_luu_vi_tri_tab_khi_thoat_ung_dung}
    Sleep    2

Mo cai dat dien thoai roi vao cai dat quay so nhanh
    [Documentation]    Mo cai dat dien thoai roi vao cai dat quay so nhanh
    Mo dien thoai roi vao cai dat dien thoai
    Click Element    ${dt_cd_nut_cd_quay _so_nhanh}
    Sleep    1
    Page Should Contain Text    Cài đặt quay số nhanh
    Sleep    2

Bam cai dat dien thoai roi vao cai dat quay so nhanh
    [Documentation]      Bam cai dat dien thoai roi vao cai dat quay so nhanh
    Bam vao dien thoai roi vao cai dat dien thoai
    Click Element    ${dt_cd_nut_cd_quay _so_nhanh}
    Sleep    1
    Page Should Contain Text    Cài đặt quay số nhanh
    Sleep    2

An nut thu thoai trong cai dat quay so nhanh
    [Documentation]    An nut thu thoai trong cai dat quay so nhanh
    Click Element    ${dt_cd_nut_quay_so_nhanh_thu_thoai}
    Sleep    2

An nut dich vu trong thu thoai
    [Documentation]    An nut dich vu trong thu thoai
    Click Element    ${dt_cd_nut_dich_vu}
    Sleep    2

An nut thiet lap trong thu thoai
    [Documentation]    An nut thiet lap trong thu thoai
    Click Element    ${dt_cd_nut_thiet_lap}
    Sleep    2

An nut thong bao trong thu thoai
    [Documentation]    An nut thong bao trong thu thoai
    Click Element    ${dt_cd_nut_thong bao}
    Sleep    2

An nut so thu thoai
    [Documentation]    An nut so thu thoai
    Click Element    ${dt_cd_nut_so_thu_thoai}
    Sleep    2

An nut nhap so thu thoai
    [Documentation]    An nut nhap so thu thoai
    Click Element    ${dt_cd_nhap_so_thu_thoai}
    Sleep    1

An nut 900 o thiet lap so thu thoai
    [Documentation]    An nut 900 o thiet lap so thu thoai
    AppiumLibrary.Input Text    ${dt_cd_nhap_so_thu_thoai}    900
    Sleep    2

An nut OK
    [Documentation]    An nut OK
    Click Element    ${dt_cd_nut_ok}
    Sleep    1

An so 2 cai dat quay so nhanh
    [Documentation]    An phim so 2
    Click Element    ${nut_2}
    Sleep    2

An so 3 cai dat quay so nhanh
    [Documentation]    An phim so 3
    Click Element    ${nut_3}
    Sleep    2

An so 4 cai dat quay so nhanh
    [Documentation]    An phim so 4
    Click Element    ${nut_4}
    Sleep    2

An so 5 cai dat quay so nhanh
    [Documentation]    An phim so 5
    Click Element    ${nut_5}
    Sleep    2

An so 6 cai dat quay so nhanh
    [Documentation]    An phim so 6
    Click Element   ${nut_6}
    Sleep    2

An so 7 cai dat quay so nhanh
    [Documentation]    An phim so 7
    Click Element    ${nut_7}
    Sleep    3

An so 8 cai dat quay so nhanh
    [Documentation]    An phim so 8
    Click Element    ${nut_8}
    Sleep    2

An so 9 cai dat quay so nhanh
    [Documentation]    An phim so 9
    Click Element    ${nut_9}
    Sleep    2

Nhap so 900 vao popup cai dat quay so nhanh
    [Documentation]    Nhap so 900 vao popup cai dat quay so nhanh
    AppiumLibrary.Input Text   ${dt_cd_cai_dat_quay_so_nhanh_nhap_sdt}    900
    Sleep    2

An vao icon danh ba o cai dat quay so nhanh
    [Documentation]    An vao icon danh ba o cai dat quay so nhanh
    Click Element    ${dt_cd_quay_so_nhanh_icon_danh_ba}
    Sleep    2

An vao lien he dau tien trong Chon nguoi lien he quay so nhanh
    [Documentation]    An vao lien he dau tien trong Chon nguoi lien he quay so nhanh
    Click Element    ${dt_cd_quay_so_nhanh_chon_lien_he1}
    Sleep    2

An vao nut thay doi
    [Documentation]    An vao nut thay doi
    Click Element    ${dt_cd_thay_doi}
    Sleep    2

An vao nut xoa bo
    [Documentation]    An vao nut xoa bo
    Click Element    ${dt_cd_xoa_bo}
    Sleep    2

An nut chan cuoc goi
    [Documentation]    An nut chan cuoc goi
    Click Element    ${dt_cd_nut_chan_cuoc_goi}
    Sleep    2

Bam vao Danh ba tren Launcher1
    [Documentation]    Bam vao Danh ba tren Launcher
    AppiumLibrary.Click Element    ${app_danh_ba}
    Sleep    5

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================