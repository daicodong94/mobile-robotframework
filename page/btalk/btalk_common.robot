*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot

*** Variables ***
${dt}    //*[@content-desc="Điện thoại"]
${dt_cai_dat}    	 bkav.android.btalk:id/dialpad_overflow

*** Keywords ***
Mo Btalk roi mo Gan day
    Log To Console    Mo Btalk roi mo Gan day
    Mo Btalk
    Click Element    ${btalk_gan_day}
    Sleep    2
    Page Should Contain Element   ${text_title_gan_day}

An nut ba cham dien thoai
    [Documentation]    An nut ba cham dien thoai
    Click Element    ${dt_cai_dat}