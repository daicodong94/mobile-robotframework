*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    tin_nhan.robot
Resource    tn_cuoc_tro_chuyen.robot
Resource    ../launcher/launcher_common.robot

*** Variables ***

# Cac phan tu thanh cong cu
${xoa_tcc}               bkav.android.btalk:id/action_home
${mui_ten_mo_rong}       bkav.android.btalk:id/selection_menu
${luu_tru}               bkav.android.btalk:id/action_archive
${xoa_tn}                bkav.android.btalk:id/action_delete
${on_off_thong_bao}      bkav.android.btalk:id/action_notification_off
${them_lh}               bkav.android.btalk:id/action_add_contact
${block}                 bkav.android.btalk:id/action_block
${chon_tat_ca}           //*[@text="Chọn tất cả"]
${bo_chon_tat_ca}        //*[@text="Bỏ chọn tất cả"]
${da_luu_tru}            //*[@text="Đã lưu trữ"]
${da_doc}                //*[@text="Đánh dấu tất cả đã đọc"]
${cai_dat}               //*[@text="Cài đặt"]


*** Keywords ***
Check mo danh sach cuoc tro chuyen
    [Documentation]    Check mo danh sach cuoc tro chuyen khi bam vao icon tin nhan tren hotseat
    Mo Tin nhan tu Launcher
    # Vuot quay ve man hinh Home

Check mo danh sach cuoc tro chuyen tu giao dien thoai bang thao tac vuot
    Log To Console    Check mo danh sach cuoc tro chuyen tu giao dien thoai bang thao tac vuot
    Mo Btalk
    Click Element    ${btalk_dien_thoai}
    Sleep    1
    Vuot sang man hinh ben phai
    Sleep    1
    Page Should Contain Element    ${logo_tn}

Check mo danh sach cuoc tro chuyen tu giao gan day bang thao tac vuot
    Log To Console    Check mo danh sach cuoc tro chuyen tu giao gan day bang thao tac vuot
    Click Element    ${btalk_gan_day}
    Sleep    1
    Vuot sang man hinh ben trai
    Sleep    1
    Page Should Contain Element    ${logo_tn}

Check mo danh sach cuoc tro chuyen tu giao dien danh ba bang thao tac cham vao tab
    Log To Console    Check mo danh sach cuoc tro chuyen tu giao dien danh ba bang thao tac cham vao tab
    Click Element    ${btalk_danh_ba}
    Vuot back ve giao dien truoc
    Click Element    ${btalk_tin_nhan}
    Sleep    1
    Page Should Contain Element    ${logo_tn}

Check hien thi thong bao khong tim thay tin nhan khi tim khiem tin nhan khong ra kq
    [Documentation]    Check hien thi thong bao khong tim thay tin nhan khi tim khiem tin nhan khong ra kq
    Click Element    ${tim_kiem_ct}
    Input Text    ${tim_kiem_tn}    ssngagfafa
    Page Should Contain Text    Không tìm thấy tin nhắn!

Check truong hop xoa noi dung tin nhan da nhan
    [Documentation]    Check truong hop xoa noi dung tin nhan da nhan
    Clear Text     ${tim_kiem_tn}
    Page Should Contain Text    Các tin nhắn tìm kiếm sẽ xuất hiện ở đây!

Check dong giao dien tim kiem tin nhan
    [Documentation]    Check dong giao dien tim kiem tin nhan
    Click Element    ${xoa_truy_van}
    Sleep    1
    Page Should Not Contain Element    ${xoa_truy_van}

Check bo chon tat ca cuoc tro truyen da chon
    [Documentation]    Check bo chon tat ca cuoc tro truyen da chon
    Long Press     ${name_lh_1}    2000
    Click Element    ${mui_ten_mo_rong}
    Sleep    2
    Click Element    ${chon_tat_ca}
    Click Element    ${mui_ten_mo_rong}
    Sleep    1
    Click Element   ${bo_chon_tat_ca}
    Sleep    1
    Page Should Contain Element    ${tim_kiem_ct}

Check hien thi luu tru cuoc tro chuyen khi cham vao icon luu tru
    [Documentation]    Check hien thi luu tru cuoc tro chuyen khi cham vao icon luu tru
    Long Press     ${name_lh_1}    2000
    Sleep    1
    Click Element    ${luu_tru}
    Sleep    1
    Page Should Contain Text     HOÀN TÁC

Check hien thi cuoc tro truyen trong danh sach luu tru
    [Documentation]    Check hien thi cuoc tro truyen trong danh sach luu tru
    Click Element    ${dau_ba_cham}
    Sleep    1
    Click Element At Coordinates    coordinate_X    coordinate_Y
    Sleep    1
    Page Should Contain Element    ${name_lh_1}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================