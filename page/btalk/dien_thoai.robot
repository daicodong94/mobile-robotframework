*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Mo Btalk vao tin nhan
    [Documentation]    Mo Btalk vao tin nhan
    Mo Btalk
    Sleep    2
    Click Element    ${btalk_tin_nhan}
    Sleep  2
	Page Should Contain Element    ${text_tin_nhan}

Bam mo app dien thoai
    [Documentation]    Bam mo app dien thoai
    Click Element    ${app_dien_thoai}
    Sleep    2

Bam mo app dien thoai vao gan day
    [Documentation]    Mo mo app dien thoai vao gan day
    Bam mo app dien thoai
    Click Element    ${btalk_gan_day}
    Sleep    2
    Page Should Contain Element   ${text_title_gan_day}

Bam tab dien thoai
    [Documentation]    Bam  tab dien thoai
    Click Element    ${btalk_dien_thoai}
    Sleep    2

An nut ngung tim kiem danh ba
    [Documentation]    An nut ngung tim kiem danh ba
    Click Element    ${dt_db_nut_ngung_tim_kiem}
    Sleep    2

An vao icon bam phim so db quay so nhanh
    [Documentation]     An vao icon bam phim so db quay so nhanh
    Click Element    ${dt_tab_dt_icon_bam_phim_so_quay_so_nhanh}
    Sleep    2

Bam sdt 0977888888
    [Documentation]    Bam sdt 0977888888
    Click Element    ${nut_0}
    Sleep    2
    Click Element    ${nut_9}
    Sleep    2
    Click Element    ${nut_7}
    Sleep    2
    Click Element    ${nut_7}
    Sleep    2
    Click Element    ${nut_8}
    Sleep    2
    Click Element    ${nut_8}
    Sleep    2
    Click Element    ${nut_8}
    Sleep    2
    Click Element    ${nut_8}
    Sleep    2
    Click Element    ${nut_8}
    Sleep    2
    Click Element    ${nut_8}
    Sleep    2

Bam vao app danh ba
    [Documentation]    Bam vao danh ba
    Click Element    ${app_danh_ba}
    Sleep    2

Bam sdt test
    [Documentation]    Bam sdt test (0823390409)
    Click Element    ${nut_0}
    Sleep    2
    Click Element    ${nut_8}
    Sleep    2
    Click Element    ${nut_2}
    Sleep    2
    Click Element    ${nut_3}
    Sleep    2
    Click Element    ${nut_3}
    Sleep    2
    Click Element    ${nut_9}
    Sleep    2
    Click Element    ${nut_0}
    Sleep    2
    Click Element    ${nut_4}
    Sleep    2
    Click Element    ${nut_0}
    Sleep    2
    Click Element    ${nut_9}
    Sleep    2

Bam so 2
    [Documentation]    Bam so 2
    Click Element    ${nut_2}
    Sleep    2

Bam vao sdt dau tien trong danh sach tim kiem
    [Documentation]    Bam vao sdt dau tien trong danh sach tim kiem
    Click Element   ${dt_tab_dt_ten_lien_he1_trong_danh_sach_tim_kiem}
    Sleep    2

Bam vao lien he thu 2 trong danh sach tim kiem
    [Documentation]    Bam vao lien he thu 2 trong danh sach tim kiem
    Click Element    ${dt_tab_dt_lien_he2_danh_sach_tim_kiem}
    Sleep    2

Bam nut quay so
    [Documentation]     Bam nut quay so
    Click Element    ${dt_nut_quay_so}
    Sleep    2

Bam nut ket thuc cuoc goi
    [Documentation]    Bam nut ket thuc cuoc goi
    Click Element    ${dt_ket_thuc_cuoc_goi}
    Sleep    2

Bam nut them lien he moi
    [Documentation]    Bam nut hem lien he moi
    Click Element    ${dt_tab_dt_them_lien_he}
    Sleep    2

An nut 2
    [Documentation]    An nut 2
    Click Element    ${nut_2}
    Sleep    2

An mui ten mo rong thu gon
    [Documentation]    An mui ten mo rong
    Click Element    ${dt_tab_dt_mui_ten_mo_rong_thu_gon}
    Sleep    2

An vao icon xoa 01 lan
    [Documentation]    An vao icon xoa 1 lan
    Click Element    ${dt_icon_xoa_khi_nhap_so}
    Sleep    2

Nhan giu icon xoa
    [Documentation]    An vao icon xoa 1 lan
    Long Press    ${dt_icon_xoa_khi_nhap_so}
    Sleep    2

Sao chep sdt dau tien o tab gan day
    [Documentation]    Sao chep sdt tu tab gan day
    Long Press    ${dt_gd_sdt1}
    Click Element    ${dt_gd_sao_chep_sdt}

An nut tat popup ban phim tu dong
    [Documentation]    An nut tat popup ban phim tu dong
    Click Element    ${dt_tab_dt_nut_tat_popup_ban_phim_khi_goi_thu_thoai}
    Sleep   2

Bam nut de sau popup phim chua duoc thiet lap
    [Documentation]    Bam nut de sau de dong popup phim chua duoc thiet lap
    Click Element    ${dt_tab_dt_nut_de_sau}
    Sleep    2

Bam nut dong y
    [Documentation]    Bam nut dong y
    Click Element    ${nut_dong_y_hoa}
    Sleep    2

Nhan giu phim 2
    [Documentation]    Nhan giu phim 2
    Long Press    ${nut_2}
    Sleep    2

Nhan giu phim 3
    [Documentation]    Nhan giu phim 3
    Long Press    ${nut_3}
    Sleep    1

Nhan giu phim 4
    [Documentation]    Nhan giu phim 4
    Long Press    ${nut_4}
    Sleep    1

Nhan giu phim 5
    [Documentation]    Nhan giu phim 5
    Long Press    ${nut_5}
    Sleep    1

Nhan giu phim 6
    [Documentation]    Nhan giu phim 6
    Long Press    ${nut_6}
    Sleep    1

Nhan giu phim 7
    [Documentation]    Nhan giu phim 7
    Long Press    ${nut_7}
    Sleep    1

Nhan giu phim 8
    [Documentation]    Nhan giu phim 8
    Long Press    ${nut_8}
    Sleep    1

Nhan giu phim 9
    [Documentation]    Nhan giu phim 9
    Long Press    ${nut_9}
    Sleep    1

input sdt
    [Documentation]    Input sdt
    Input Text    ${dt_cd_cai_dat_quay_so_nhanh_nhap_sdt}    0357360020
    Sleep    2

Vuot xuong launcher de vao danh ba quay so nhanh
    [Documentation]    Check vuot launcher tu tren xuong duoi
    AppiumLibrary.Swipe By Percent    50   15   60    60
    Sleep    2

Vuot len launcher de vao danh ba quay so nhanh
    [Documentation]    Check vuot launcher tu tren xuong duoi
    AppiumLibrary.Swipe By Percent    55   65  60    15
    Sleep    2

An nut them lien he yeu thich
    [Documentation]    An nut them lien he yeu thich
    Click Element    ${dt_tab_dt_nut_them_lien_he_yeu_thich}
    Sleep    2

An chon lien he dau tien chon them lien he yeu thich
    [Documentation]    An chon lien he dau tien chon lien he yeu thich
    Click Element    ${dt_db_ten_lien_he_1}
    Sleep    2

An vao lien he dau tien db quay so nhanh
    [Documentation]    An vao lien he dau tien db quay so nhanh
    Click Element    ${dt_tab_dt_db_quay_so_nhanh_lienhe1}
    Sleep    2

An vao nut 3cham db quay so nhanh lien he1
    [Documentation]    An vao nut 3cham db quay so nhanh lien he1
    Click Element    ${dt_tab_dt_nut_3cham_db_quay_so_nhanh_lien_he1}
    Sleep    3

Chon sim1 de quay so TH 2sim popup goi bang
    [Documentation]   Chon sim1 de quay so TH 2sim popup goi bang
    Click Element    ${dt_popup_goi_bang_sim1}
    Sleep    3

An vao lien he thu 2 trong db quay so nhanh
    [Documentation]    An vao lien he thu 2 trong db quay so nhanh
    Click Element    ${dt_db_quay_so_nhanh_ten_lien_he2}
    Sleep    2

Nhap ma *#06#
    [Documentation]    Nhap ma *#06#
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    *#06#
    Sleep    2

Nhap ma *#*#4636#*#*
    [Documentation]    Nhap ma *#*#4636#*#*
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    *#*#4636#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#3264#*#*
    [Documentation]    Nhap ma *#*#3264#*#*
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    *#*#3264#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#1111#*#*
    [Documentation]    Nhap ma *#*#1111#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}    *#*#1111#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#2222#*#*
    [Documentation]    Nhap ma *#*#2222#*#*
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    *#*#2222#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#2663#*#*
    [Documentation]    Nhap ma *#*#2663#*#*
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}        *#*#2663#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#232331#*#*
    [Documentation]    Nhap ma *#*#232331#*#*
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}        *#*#232331#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#1472365#*#*
    [Documentation]    Nhap ma *#*#1472365#*#*
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}        *#*#1472365#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#1575#*#*
    [Documentation]    Nhap ma *#*#1575#*#*
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}        *#*#1575#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#528#*#*
    [Documentation]    Nhap ma *#*#528#*#*
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}        *#*#528#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#526#*#*
    [Documentation]    Nhap ma *#*#526#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#*#526#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#232338#*#*
    [Documentation]    Nhap ma *#*#232338#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#*#232338#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#0842#*#*
    [Documentation]    Nhap ma *#*#0842#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#*#0842#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#0588#*#*
    [Documentation]    Nhap ma *#*#0588#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#*#0588#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#0289#*#*
    [Documentation]    Nhap ma *#*#0289#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#*#0289#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#0673#*#*
    [Documentation]    Nhap ma *#*#0673#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#*#0673#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#197328640#*#*
    [Documentation]    Nhap ma *#*#197328640#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#*#197328640#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#7262626#*#*
    [Documentation]    Nhap ma *#*#7262626#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#*#7262626#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *2767*3855#
    [Documentation]    Nhap ma *2767*3855#
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *2767*3855#
    Sleep    2

Nhap ma *#*#7780#*#*
    [Documentation]    Nhap ma *#*#7780#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#*#7780#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#273282*255*663282*#*#*
    [Documentation]    Nhap ma *#*#273282*255*663282*#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#*#273282*255*663282*#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#34971539#*#*
    [Documentation]    Nhap ma *#*#34971539#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#*#34971539#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#12580*369#
    [Documentation]    Nhap ma *#12580*369#
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#12580*369#
    Sleep    2

Nhap ma *#301279#
    [Documentation]    Nhap ma *#301279#
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#301279#
    Sleep    2

Nhap ma *#*#8255#*#*
    [Documentation]    Nhap ma *#*#8255#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#*#8255#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#4986*2650468#*#*
    [Documentation]    Nhap ma *#*#4986*2650468#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#*#4986*2650468#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#44336#*#*
    [Documentation]    Nhap ma  *#*#44336#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}  *#*#44336#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#0283#*#*
    [Documentation]    Nhap ma *#*#0283#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}  *#*#0283#*#
     Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#0*#*#*
    [Documentation]    Nhap ma *#*#0*#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}  *#*#0*#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#0*#
    [Documentation]    Nhap ma *#0*#
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}  *#0*#
    Sleep    2

Nhap ma *#872564#
    [Documentation]    Nhap ma *#872564#
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}  *#872564#
    Sleep    2

Nhap ma *#9090#
    [Documentation]    Nhap ma *#9090#
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}   *#9090#
    Sleep    2

Nhap ma *#9900#
    [Documentation]    Nhap ma   *#9900#
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}  *#9900#
    Sleep    2

Nhap ma *#7465625#
    [Documentation]    Nhap ma *#7465625#
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}  *#7465625#
    Sleep    2

Nhap ma **05**#
    [Documentation]    Nhap ma **05**#
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}  **05**#
    Sleep    2

Nhap ma *#*#8351#*#*
    [Documentation]    Nhap ma *#*#8351#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}  *#*#8351#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#8350#*#*
    [Documentation]    Nhap ma *#*#8350#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}  *#*#8351#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma *#*#7594#*#*
    [Documentation]    Nhap ma *#*#7594#*#*
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}  *#*#7594#*#
    Sleep    1
    Click Element    ${dt_nut_sao}
    Sleep    2

Nhap ma #*#232337#*#
    [Documentation]    Nhap ma #*#232337#*#
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}  \#*#232337#*#
    Sleep    2

Nhap ma ##7764726
    [Documentation]    Nhap ma ##7764726
    Input Text     ${dt_man_hinh_hien_thi_nhap_sdt}  \##7764726
    Sleep    2

Bam nut goi bang sim khac trong danh sach tim kiem
    [Documentation]    Bam nut goi bang sim khac trong danh sach tim kiem
    Click Element    ${dt_tab_dt_danh_sach_tim_kiem_nut_goi_bang_sim_khac}
    Sleep    2

Bam nut chia se trong danh sach tim kiem
    [Documentation]    Bam nut chia se trong danh sach tim kiem
    Click Element    ${dt_tab_dt_nut_chia_se}
    Sleep    2

input 900
    [Documentation]    input 900
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    900
    Sleep    2

Input 199
    [Documentation]    input 199
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    199
    Sleep    2

Input 121
    [Documentation]    Input 121
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    121
    Sleep    2

Input 1080
    [Documentation]    Input 1080
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    1080
    Sleep    2

input 197
    [Documentation]    Input 197
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    197
    Sleep    2

Input 198
    [Documentation]    Input 198
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    198
    Sleep    2

Input 1222
    [Documentation]    Input 1222
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    1222
    Sleep    2

Input 1789
    [Documentation]     Input 1789
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    1789
    Sleep    2

Input 9189
    [Documentation]     Input 9189
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    9189
    Sleep    2

Input 9198
    [Documentation]     Input 9198
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    9198
    Sleep    2

Input 9090
    [Documentation]     Input 9090
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    9090
    Sleep    2

Input 9393
    [Documentation]     Input 9393
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    9393
    Sleep    2

Input 9191
    [Documentation]    Input 9191
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    9191
    Sleep    2

Input 9192
    [Documentation]    Input 9192
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    9192
    Sleep    2

Input 888
    [Documentation]    Input 888
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    888
    Sleep    2

Input 789
    [Documentation]    Input 789
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    789
    Sleep    2

Input 123
    [Documentation]    Input 123
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    123
    Sleep    2

Input 360
    [Documentation]    Input 360
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    360
    Sleep    2

Input 3636
    [Documentation]    Input 3636
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    3636
    Sleep    2

Input 366
    [Documentation]    Input 366
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    366
    Sleep    2

Input 112
    [Documentation]    Input 112
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    112
    Sleep    2

Input 113
    [Documentation]    Input 113
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}     113
    Sleep    2

Input 114
    [Documentation]    Input 114
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    114
    Sleep    2

Input 115
    [Documentation]    Input 115
    Input Text    ${dt_man_hinh_hien_thi_nhap_sdt}    115
    Sleep    2

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================