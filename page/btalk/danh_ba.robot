*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Mo Danh ba
    [Documentation]    Mo danh ba
    Mo Btalk
    An nut danh ba
    
An nut danh ba
    [Documentation]    An nut danh ba
    Click Element    ${btalk_danh_ba}
    
An nut them lien he
    [Documentation]    An nut them lien he
    Click Element    ${dt_db_nut_them_lien_he}
   
An nut them lien he khi tim kiem
    [Documentation]    An nut them lien he khi timm kiem
    Click Element    ${dt_db_nut_them_lien_he_khi_tim_kiem} 

An nut cac lien he yeu thich
    [Documentation]    An nut cac lien he yeu thich
    Click Element    ${dt_db_nut_lien_he_yeu_thich} 
    
An nut tuy chon
    [Documentation]    An nut tuy chon
    Click Element   ${dt_db_tab_tuy_chon_cai_dat} 

An nut ngung tim kiem
    [Documentation]    An nut ngung tim kiem
    Click Element    ${dt_db_nut_ngung_tim_kiem}

An nut thoat khoi them lien he moi
    [Documentation]    An nut thoat them lien he moi
    Click Element    ${dt_db_nut_thoat_them_lien_he}

An nut dien thoai cua lien he dau tien
    [Documentation]    An nut dien thoai cua lien he dau tien
    Click Element    ${dt_db_nut_goi_lien_he_1}
    Sleep    2
    
#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================