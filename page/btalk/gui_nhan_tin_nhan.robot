*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***

Mo tin nhan tu laucher
    [Documentation]    Mo tin nhan tu lancher
    Mo Tin nhan tu Launcher
    Sleep    2
	Page Should Contain Element    ${text_tin_nhan}   
    
Mo tin nhan tu Btalk
    [Documentation]    Mo tin nhan tu Btalk
    Mo Btalk
    Sleep    2
    Click Element    ${btalk_tin_nhan}
    Sleep    2
    
Bam vao nut them tin nhan moi
    [Documentation]    Bam vao nut them tinnhan moi
    Click Element    ${tn_them_tin_nhan}
    Sleep    2
    Sleep  2

Bam vao logo tin nhan
    [Documentation]    Bam vao logo tin nhan
    Click Element    ${tn_logo}
    Sleep    2
   
Bam vao tab tin nhan
    [Documentation]    Bam vao tab tin nhan
    Click Element    ${btalk_tin_nhan}
    Sleep    2
    
Bam vao go noi dung tin nhan
    [Documentation]    Bam vao go noi dung tin nhan
    Click Element    ${tn_go_noi_dung_tin_nhan}
    Sleep    2
   
Bam vao sdt Sim Test
    [Documentation]    Bam vao sdt Sim Test
    Click Element    ${dt_sdt_sim_test}
    Sleep    2 

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================