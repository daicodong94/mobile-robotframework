*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot

*** Variables ***

*** Keywords ***
Chon 1 video tren man Quay Phim
    [Documentation]    Chon 1 video tren man Quay Phim
    Click Element    ${bst_video_thu_1}
    Sleep    2    
    
Chon Cat video tren video
    [Documentation]    Chon Cat video tren video
    Click Element    ${bst_videonut_cat}
    
 Chon Huy tren man cat video
    [Documentation]     Chon Huy tren man cat video
    Click Element    ${bst_video_cat_huy}
    
Cham vao khoang trong
    [Documentation]    Cham vao khoang trong
    Click Element At Coordinates    234    937
    
Cham vao nut Play tren man hinh
    [Documentation]    Cham vao nut Play tren man hinh
    Click Element    ${bst_video_nut_play}
    # Page Should Contain Element    ${bst_video_nut_play}   
    
Cham vao nut Over Play tren man hinh
    [Documentation]    Cham toa do vao nut Play tren man hinh
    # Click Element At Coordinates    540    2013
    Click Element    ${bst_video_nut_over_play}
    
Cham nut play tren video quay cham
    [Documentation]    Cham nut play tren video quay cham
    Click Element    ${bst_video_nut_play}

Vuot tang am luong tren video
    [Documentation]    Vuot tang am luong tren video
    Swipe By Percent    80    70    80    60    
    ${get_text}=    Get Text    ${get_text_am_luong}

    
Vuot giam am luong tren video
    [Documentation]    Vuot giam am luong tren video
    Swipe By Percent    80    60    80    70    
    ${get_text}=    Get Text    ${get_text_am_luong}
    
Vuot tang anh sang tren video
    [Documentation]    Vuot tang anh sang tren video
    Swipe By Percent    10    70    10    60    
    ${get_text}=    Get Text    ${bst_video_do_sang_man_hinh}

    
Vuot giam anh sang tren video
    [Documentation]    Vuot giam anh sang tren video
    Swipe By Percent    10    60    10    70    
    ${get_text}=    Get Text    ${bst_video_do_sang_man_hinh}

Vuot man tu trai sang phai
    [Documentation]    Vuot man tu trai sang phai
    Swipe By Percent    5   70    50    70    
      
Chon video quay cham - video thu 2 
    [Documentation]    Chon video quay cham - video thu 2
    Click Element    ${bst_video_video_quay_cham_2}
         
Vuot video tro ve 0s tren b86
    [Documentation]    Vuot video tro ve 0s
    Click Element At Coordinates    105    1814       

