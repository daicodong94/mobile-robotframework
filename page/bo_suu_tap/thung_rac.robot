*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot
Resource    album.robot

*** Variables ***

*** Keywords ***
Chon 2 anh trong Thung rac
    [Documentation]    Chon nhieu anh trong Thung rac
    Long Press    ${bst_TR_thung_rac_anh_1}  
    Click Element    ${bst_TR_thung_rac_anh_1}
    Sleep    1       
    Click Element    ${bst_TR_thung_rac_anh_2}
    Sleep    1    
       
Bo chon nhieu anh trong Thung rac
    [Documentation]    Bo chon nhieu anh trong Thung rac
    Click Element    ${bst_TR_thung_rac_anh_1}
    Click Element    ${bst_TR_thung_rac_anh_2}
    
Chon xoa anh tren Dong thoi gian
    [Documentation]    Chon xoa anh tren Dong thoi gian
    Click Element    ${bst_TR_nut_xoa}
      
Chon Huy tren popup Xoa khoi thung rac
    [Documentation]    Chon Huy tren popup Xoa khoi thung ra
    Click Element    ${bst_TR_nut_huy}
    
Chon Dong y tren popup Xoa khoi thung rac
    [Documentation]    Chon Dong y tren popup Xoa khoi thung rac
    Click Element    ${bst_TR_nut_dong_y}
    
Bam icon chon tat ca tren thanh toolbar
    [Documentation]    Bam icon chon tat ca tren thanh toolbar
    Click Element    ${bst_TR_selectall_toolbar}
    
Chon xoa anh trong Thung rac
    [Documentation]    Chon xoa anh trong Thung rac
    Click Element    ${bst_TR_nut_xoa}
    
Chon khoi phuc anh trong Thung rac
    [Documentation]    Chon khoi phuc anh trong Thung rac
    Click Element    ${bst_TR_nut_khoi_phuc}
    
Mo giao dien thung rac rong
    [Documentation]        Mo giao dien thung rac rong
    Click Element    ${bst_album_man_thung_rac}
    Sleep    1    
    Page Should Contain Element    ${bst_TR_thung_rac_rong}
    