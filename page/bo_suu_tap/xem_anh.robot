*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot
Resource    album.robot  
Resource    xem_video.robot  

*** Variables ***

*** Keywords ***

Chon 1 anh bat ky
    [Documentation]    Chon 1 anh bat ky
    Click Element    ${bst_xem_anh_anh_1}       
    Sleep    2    
Cham vao anh 
    [Documentation]    Cham vao anh
    Click Element At Coordinates    500    489
    Sleep    2       
    
Cham vao nut Huy 
    [Documentation]    Cham vao nut Huy
    Click Element    ${bst_xem_anh_chinh_anh_nut_huy}
    Sleep    3   
    
Cham vao nut Huy tren thanh edit tabbar
    [Documentation]    Cham vao nut Huy tren thanh edit tabbar
    Click Element    ${bst_xem_anh_chinh_sua_text_huy_bo}
    
Cham vao nut Luu tren thanh edit tabbar
    [Documentation]    Cham vao nut Luu tren thanh edit tabbar
    Click Element    ${bst_xem_anh_chinh_sua_text_luu}
    
Cham vao Sua anh tren thanh edit tabbar
    [Documentation]    Cham vao Sua anh tren thanh edit tabbar
    Click Element    ${bst_xem_anh_nut_chinh_sua}
    Sleep    3    
    
Cham vao icon Sua mau anh tren thanh edit tabbar
    [Documentation]        Cham vao icon Sua anh tren thanh edit tabbar
    Click Element    ${bst_xem_anh_chinh_anh_nut_fxbutton}
    
Cham vao iccon thay doi kich thuoc anh
    [Documentation]    Cham vao iccon thay doi kich thuoc anh
    Click Element    ${bst_xem_anh_chinh_anh_nut_geometry}
    
Cham vao nut Cat tren giao dien chinh sua kich thuoc anh
    [Documentation]    Cham vao nut Cat tren giao dien chinh sua kich thuoc anh
    Click Element    ${bst_xem_anh_cat_anh_leftText} 
    
Cham vao icon dieu chinh do phoi sang/tuong phan/ do sac net
    [Documentation]    Cham vao icon dieu chinh do phoi sang/tuong phan/ do sac net
    Click Element    ${bst_xem_anh_chinh_anh_nut_colors}
    
Cham vao icon chen them anh
    [Documentation]    Cham vao icon chen them anh
    Click Element    ${bst_xem_anh_chinh_anh_nut_waterMark}
    
Cham vao icon Vi Tri tren giao dien icon chem them anh
    [Documentation]    Cham vao icon Vi Tri tren giao dien icon chem them anh
    Click Element    ${bst_xem_anh_icon_vi_tri}
    
Cham vao icon thoi tiet tren giao dien icon chen them anh
    [Documentation]    Cham vao icon thoi tiet tren giao dien icon chen them anh
    Click Element    ${bst_xem_anh_icon_thoi_tiet}
    
Cham vao icon 1 tren giao dien icon chen them anh
    [Documentation]    Cham vao 1 icon tren giao dien icon chen them anh
    Click Element    ${bst_xem_anh_chen_icon_1}
    
Cham vao icon 2 tren giao dien icon chen them anh
    [Documentation]        Cham vao icon 2 tren giao dien icon chen them anh
    Click Element   ${bst_xem_anh_chen_icon_2}  
    
Cham vao huy trong thong bao tren giao dien chen them icon vao anh
    [Documentation]    Cham vao huy trong thong bao tren giao dien chen them icon vao anh
    Click Element    ${bst_xem_anh_chen_icon_nut_Huy}
    
Cham vao huy va thoat trong thong bao tren giao dien chen them icon vao anh
    [Documentation]    Cham vao huy va thoat trong thong bao tren giao dien chen them icon vao anh
    Click Element    ${bst_xem_anh_chen_icon_nut_huy_va_thoat}

Cham vao icon chinh sua anh nang cao
    [Documentation]    Cham vao icon chinh sua anh nang cao
    Click Element    ${bst_xem_anh_chinh_sua_nut_edit_photo}    

Nhan mo popup xem thong tin anh/video
    [Documentation]    Nhan mo popup xem thong tin anh/video
    Click Element    ${bst_xem_anh_nut_thong_tin}
    Sleep    3    

Chon album May anh tren giao dien album
    [Documentation]    Chon album May anh tren giao dien album
    Click Element    ${bst_album_may_anh}
    
Chon album Download tren giao dien album
    [Documentation]    Chon album Download tren giao dien album
    Click Element    ${bst_album_Download} 
    Sleep    2    
    
Chon album Chup man hinh tren giao dien album
    [Documentation]    Chon album Chup man hinh tren giao dien album
    Click Element    ${bst_album_chup_man_hinh}    
    Sleep    2       
    
Nhan chon ĐONG tren popup thong tin
    [Documentation]    Nhan chon ĐONG tren popup thong tin
    Click Element    ${bst_xem_anh_thong_tin_dong}

Vuot thoat man hinh tu trai sang phai
    [Documentation]    Vuot thoat man hinh tu trai sang phai
    Swipe By Percent    0    60    50    60    
    
Chon chia se anh tren thanh edit tabbar
    [Documentation]    Chon chia se anh tren thanh edit tabbar
    Click Element    ${bst_xem_anh_nut_chia_se}
    Sleep    3   
    
Chon thanh cong cu thay doi kich thuoc anh
    [Documentation]    Chon thanh cong cu thay doi kich thuoc anh
    Click Element    ${bst_xem_anh_chinh_anh_nut_geometry}
    
Cham vao icon ba cham tren thanh toolbar
    [Documentation]    Cham vao icon ... tren thanh toolbar
    Click Element    ${bst_xem_anh_nut_more_func}
    Sleep    2      
    
Cham vao Mo voi tu icon ba cham tren thanh toolbar
    [Documentation]    Cham vao Mo voi tu icon ba cham tren thanh toolbar
    Click Element    ${bst_xem_anh_ba_cham_mo_voi}
    Sleep    2    
    
Cham vao Dung lam tu icon ba cham tren thanh toolbar
    [Documentation]    Cham vao Dung lam tu icon ba cham tren thanh toolbar
    Click Element    ${bst_xem_anh_ba_cham_dung_lam}
    Sleep    2    
    
Cham vao In tu icon ba cham tren thanh toolbar
    [Documentation]    Cham vao In tu icon ba cham tren thanh toolbar
    Click Element    ${bst_xem_anh_ba_cham_in}
    Sleep    2    
    
Cham vao Doi ten tu icon ba cham tren thanh toolbar
    [Documentation]    Cham vao Doi ten tu icon ba cham tren thanh toolbar
    Click Element    ${bst_xem_anh_ba_cham_doi_ten}
    
Thay doi ten anh voi ky tu dac biet
    [Documentation]    Xoa ten anh
    Clear Text    ${bst_Xem_anh_edit_text}
    Input Text    ${bst_Xem_anh_edit_text}    test@😂
    
Cham vao dong y tren giao dien doi ten anh
    [Documentation]    Cham vao dong y tren giao dien doi ten anh
    Click Element    ${bst_Xem_anh_dat_ten_nut_DONG_Y}
    
Cham vao Huy tren giao dien doi ten anh
    [Documentation]    Cham vao Huy tren giao dien doi ten anh
    Click Element    ${bst_Xem_anh_dat_ten_nut_HUY}    