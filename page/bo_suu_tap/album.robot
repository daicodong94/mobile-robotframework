*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot

*** Variables ***

*** Keywords ***
Mo giao dien Dong thoi gian
    [Documentation]    Mo giao dien Dong thoi gian
    Click Element    ${bst_album_dtg_nut_anh}
    Sleep    1
    Page Should Contain Element    ${bst_album_dtg_nut_ngay}

Mo giao dien Album
    [Documentation]    Mo giao dien Album
    Click Element    ${bst_album_man_album}
    Sleep    1
    Page Should Contain Element    ${bst_album_nut_them_moi}    

Mo giao dien Quay phim
    [Documentation]    Mo giao dien Quay phim
    Click Element    ${bst_album_man_quay_phim}
    Sleep    1
    Page Should Not Contain Element    ${bst_album_nut_them_moi}

Mo giao dien Thung rac
    [Documentation]    Mo giao dien thung rac
    Click Element    ${bst_album_man_thung_rac}
    Sleep    1
    Page Should Contain Element    ${bst_album_thung_rac_mess} 

Mo giao dien Thung rac khi co anh
    [Documentation]    Mo giao dien thung rac
    Click Element    ${bst_album_man_thung_rac}
    Sleep    1
    Page Should Not Contain Element    ${bst_album_thung_rac_mess} 
    
Nhan giu chon album chup man hinh
    [Documentation]    Nhan giu vao album Chup man hinh
    Long Press    ${bst_album_chup_man_hinh}
    Sleep    1
    Page Should Contain Element    ${bst_album_title_1_muc_duoc_chon}
    Page Should Not Contain Element    ${bst_album_nut_them_moi}  
    Page Should Contain Element    ${bst_album_tabbar_them_vao_album}

Nhan giu chon album may anh
    [Documentation]    Nhan giu chon album may anh
    Long Press    ${bst_album_may_anh}
    Sleep    1
    Page Should Contain Element    ${bst_album_title_2_muc_duoc_chon}
    Page Should Not Contain Element    ${bst_album_nut_them_moi}   
    Page Should Contain Element    ${bst_album_tabbar_them_vao_album}
    Page Should Contain Element    ${bst_album_nut_chia_se}
    Page Should Contain Element    ${bst_album_tabbar_xoa}

Nhan chon chia se
    [Documentation]    Nhan chon chia se
    Click Element    ${bst_album_nut_chia_se}
    Sleep     1
    # Page Should Contain Element    ${bst_album_app_facebook}

Nhan chon vao khoang trong
    [Documentation]    Nhan chon vao khoang trong
    Click Element At Coordinates    715    921
    Page Should Contain Element    ${bst_album_nut_chia_se}

Nhan chon vao khoang trong tren man dat ten album
    [Documentation]    Nhan chon vao khoang trong tren man dat ten album
    Click Element At Coordinates    729    538
    
Vuot mo rong man chia se tu duoi len tren
    [Documentation]    Vuot mo rong man chia se tu duoi len tren
    Swipe By Percent    60    80    60    20
    Page Should Contain Element    ${bst_album_app_bluetooth}

Vuot man tu duoi len tren
    [Documentation]    Vuot mo rong man chia se tu duoi len tren
    Swipe By Percent    60    70    60    20

Vuot man hinh tu tren xuong
    [Documentation]    Vuot man hinh tu tren xuong
    Swipe By Percent    50    20    50    70

Vuot thu nho man chia se tu tren xuong
    [Documentation]    Vuot thu nho man chia se tu tren xuong
    Swipe By Percent    60    10    60    70

Vuot man tu phai qua trai
    [Documentation]    Vuot man tu trai qua phai
    Swipe By Percent    70    60    0    60    
    
Vuot man tu trai qua phai
    [Documentation]    Vuot man tu trai qua phai
    Swipe By Percent    0    70    70    70   
    
Bam vao icon tao them Album
    [Documentation]    Bam vao icon tao them Album
    Click Element    ${bst_album_nut_them_moi}

Vuot mo rong thumbnail khi vuot an ban phim
    [Documentation]    Vuot mo rong thumbnail khi vuot an ban phim
    Swipe By Percent    50    20    50    90
    Page Should Contain Element    ${bst_album_anh_vi_tri_1}

Chon 1 anh trong album
    [Documentation]    Chon 1 anh trong album
    Click A Point    126    129

Dat ten album trung voi ten album da co
    [Documentation]    Dat ten album trung voi ten album da co
    Input Text    ${bst_album_nhap_ten_album}    camera

Dat ten album khong co ky tu dac biet
    [Documentation]    Dat ten album khong co ky tu dac biet
    Input Text    ${bst_album_nhap_ten_album}    test

Dat ten album co ky tu dac biet
    [Documentation]    Dat ten album co ky tu dac biet
    Input Text    ${bst_album_nhap_ten_album}   test@😂

Bam chon dong y them ten moi cho them album
    [Documentation]    Bam chon dong y them moi them album
    Click Element    ${bst_album_tick_chon_dat_ten_album}
    Sleep    2
    Page Should Contain Element    ${bst_album_text_di_chuyen_hay_sao_chep}

Bam chon Sao chep trong popup Di chuyen hay sao chep
    [Documentation]    Bam chon sao chep trong popup Di chuyen hay sao chep
    Click Element    ${bst_album_nut_sao_chep}
    
Chon thu muc anh tu quan ly file
    [Documentation]    Chon anh tu quan ly file
    Click Element At Coordinates    891    626    
    
Thay doi ten album tu Quan ly file
    [Documentation]    Thay doi ten album tu Quan ly file
    Long Press    ${bst_album_test}
    Click Element    ${bst_QLF_nut_nhieu_hon}
    Sleep    2
    Log To Console    Nhap ten moi cho album thay doi
    Click Element    ${bst_QLF_doi_ten}
    Sleep    2
    Clear Text    ${bst_QLF_nhap_ten_album_moi}
    Input Text    ${bst_QLF_nhap_ten_album_moi}    camera3
    Click Element    ${bst_QLF_nut_dong_y}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================