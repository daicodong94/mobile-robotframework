*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot

*** Variables ***

*** Keywords ***
    
Vuot man tu duoi len tren man Dong thoi gian
    [Documentation]    Vuot man tu duoi len tren man Dong thoi gian
    Swipe By Percent    70    50    70    20   
    
Vuot man tu tren xuong tren man Dong thoi gian
    [Documentation]    Vuot man tu tren xuong tren man Dong thoi gian
    Swipe By Percent    50    20    50    50    
    
Kiem tra tim kiem gia tri text hom nay trong dong thoi gian
    [Documentation]    Luon cho phep ung dung truy cap vao vi tri cua thiet bi
    ${kiem_tra}    AppiumLibrary.Get Matching Xpath Count    ${bst_dtg_hom_nay}
    Run Keyword If    ${kiem_tra}>0    Get Text   ${bst_dtg_hom_nay}
    Sleep    5 
    
Kiem tra tim kiem gia tri text hom qua trong dong thoi gian
    ${kiem_tra}    AppiumLibrary.Get Matching Xpath Count    ${bst_dtg_hom_qua}
    Run Keyword If    ${kiem_tra}>0    Get Text    ${bst_dtg_hom_qua}
    Sleep    2
    
Kiem tra tim kiem gia tri text ngay rundom trong dong thoi gian
    ${kiem_tra}    AppiumLibrary.Get Matching Xpath Count    ${bst_dtg_nam_2021}  
    Run Keyword If    ${kiem_tra}>0    Get Text    ${bst_dtg_nam_2021}  
    Sleep    2
    
Chon thoi gian "nam" tren man Dong thoi gian
    [Documentation]    Chon thoi gian "nam" tren man Dong thoi gian
    Click Element    ${bst_dtg_text_nam}
    
Chon nam 2021 tren man thoi gian "nam"
    [Documentation]    Chon nam 2021 tren man thoi gian "nam"
    Click Element    ${bst_dtg_nam_2021}
    
Chon thoi gian "thang" tren man Dong thoi gian
    [Documentation]    Chon thoi gian "thang" tren man Dong thoi gian
    Click Element    ${bst_dtg_text_thang}
    

  
    
    