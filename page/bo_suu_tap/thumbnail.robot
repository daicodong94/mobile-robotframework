*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot
Resource    album.robot

*** Variables ***

*** Keywords ***  
    
Chon giu 1 anh trong Dong thoi gian
    [Documentation]    Chon 1 anh trong Dong thoi gian
    AppiumLibrary.Long Press    ${bst_TN_anh_1}    2000   
    Sleep    2         
    
Chon giu 1 video (vi tri thu 2) trong Dong thoi gian
    [Documentation]    Chon giu 1 video (vi tri thu 2) trong Dong thoi gian
        AppiumLibrary.Long Press    ${bst_TN_anh_2}    2000   

Chon anh 1 trong Dong thoi gian
    [Documentation]        Chon anh 1 trong Dong thoi gian
    Click Element    ${bst_TN_anh_1}
    
Chon anh thu 2 trong Dong thoi gian
    [Documentation]    Chon anh thu 2 trong Dong thoi gian
    Click Element    ${bst_TN_anh_2} 
    Sleep    2
    
Chon anh thu 3 trong Dong thoi gian
    [Documentation]    Chon anh thu 2 trong Dong thoi gian
    Click Element    ${bst_TN_anh_3} 
    Sleep    2

Chon dat lam trong Dong thoi gian
    [Documentation]    Chon dat lam trong Dong thoi gian
    Click Element    ${bst_TN_nut_dat_lam}

Cham vao anh 
    [Documentation]    Cham vao anh
    Click Element At Coordinates    500    489
    Sleep    2       
    
Bo chon 1 anh trong Dong thoi gian
    [Documentation]    Bo chon 1 anh trong Dong thoi gian
    Click Element    ${bst_TN_anh_1}
        
Chon nhieu anh trong Dong thoi gian
    [Documentation]    Chon nhieu anh trong Dong thoi gian
    AppiumLibrary.Click Element    ${bst_TN_anh_1}
    Sleep    2
    AppiumLibrary.Click Element    ${bst_TN_anh_2}
    Sleep    2    
    AppiumLibrary.Click Element    ${bst_TN_anh_4}
    Sleep    2    
    AppiumLibrary.Click Element   ${bst_TN_anh_6}
    Sleep    2  
    
Chon tat ca anh tren thanh toolbat
    [Documentation]    Chon tat ca anh tren thanh toolbat
    Click Element    ${bst_TN_toolbar_tick _chon_tat_ca}
    
Bo chon tat ca anh tren thanh toolbar
    [Documentation]    Bo chon tat ca anh tren thanh toolbar
    Click Element    ${bst_TN_toolbar_tick _chon_tat_ca}
    
Bo chon tat ca anh bang icon [X]
    [Documentation]    Bo chon tat ca anh bang icon [X]
    Click Element    ${bst_TN_toolbar_exit_x}
    
Vuot man tu trai sang phai
    [Documentation]   Vuot man tu trai sang phai
    Swipe By Percent    0    60    50    60    
    
Nhan chon Them vao album trong Dong thoi gian
    [Documentation]    Nhan chon Them vao album trong Dong thoi gian
    Click Element    ${bst_TN_nut_them_vao_album}
    Sleep    2    
    Page Should Contain Element    ${bst_TN_nut_huy}   
    Page Should Contain Element    ${bst_thumbnail_nut_album_moi}  
    Page Should Contain Element    ${bst_thumbnail_btn_dong_y}  
    Page Should Contain Element    ${bst_album_chup_man_hinh}  
    Page Should Contain Element    ${bst_album_may_anh}    
    
Nhan chon Huy tren giao dien them vao album
    [Documentation]    Nhan chon Huy tren giao dien them vao album
    Click Element    ${bst_TN_nut_huy}
    Page Should Contain Element    ${bst_TN_nut_ngay}
    
Nhan chon HUy trong giao dien Album moi
    [Documentation]        Nhan chon HUy trong giao dien Album moi
    Click Element    ${bst_TN_album_moi_nut_huy}

Nhan chon Album moi
    [Documentation]        Nhan chon Album moi
    Click Element    ${bst_thumbnail_nut_album_moi}
    Click Element    ${bst_thumbnail_nut_album_moi}
    Sleep    2    
    Page Should Contain Element    ${bst_TN_album_moi_nut_huy}   
    Page Should Contain Element    ${bst_TN_album_moi_nut_tao}
    
Cham nut Di chuyen
    [Documentation]         Cham nut Di chuyen 
    Click Element    ${bst_album_moi_nut_di_chuyen}
    
Cham nut Sao chep
    [Documentation]    Cham nut Sao chep
    Click Element    ${bst_album_moi_nut_sao_chep}        

Dat ten album moi <80 ky tu
    [Documentation]    Dat ten album moi <80 ky tu
    Input Text    ${bst_TN_album_moi_edit_text}    test3
    
Nhan chon Tao
    [Documentation]    Nhan chon tao
    Click Element    ${bst_TN_album_moi_nut_tao}
    Page Should Contain Element    ${bst_album_moi_nut_di_chuyen}
    Page Should Contain Element    ${bst_album_moi_nut_sao_chep}
    Page Should Contain Element    ${bst_Tn_popup_di_chuyen_sao_chep}     
        
Chon 1 video tren Dong thoi gian
    [Documentation]    Chon 1 video tren Dong thoi gian
    Long Press    ${bst_TN_video_20}
    
Chon Dat lam tren thanh edit tabbar
    [Documentation]       Chon Dat lam tren thanh edit tabbar
    Long Press    ${bst_TN_nut_dat_lam}
    
Dat ten album trung voi ten album da co san
    [Documentation]         Dat ten album trung voi ten album da co san
    Input Text   ${bst_TN_album_moi_edit_text}    Chụp màn hình
    
Dat ten album co ky tu dac biet, emoticon
    [Documentation]    Dat ten album co ky tu dac biet, emoticon
    Input Text    ${bst_TN_album_moi_edit_text}    tien@😂

Mo album co ten tien@😂
    [Documentation]    Mo album co ten tien@😂
    Click Text    tien@😂    
    
Mo album dau tien 
    [Documentation]    Mo album dau tien
    Click Element    ${bst_thumbnail_album_thu_1}
    
Vuot sang man hinh ben trai 2
    [Documentation]    Check vuot launcher tu trai qua phai
    Swipe By Percent    0    50    80    50  
    
Nhan chon album thu 1 sau khi da tao 1 album
    [Documentation]    Nhan chon album thu nhat sau khi da tao 1 album
    Click Element     ${bst_thumbnail_album_thu_1_sau_khi_tao_1_album}
    
    
    
    