*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot

*** Keywords ***
Mo quet QR code tu Dashboard
    Log To Console    Mo quet QR code tu Dashboard
    Vuot mo bang dieu khien
    Vuot sang man hinh ben phai
    AppiumLibrary.Click Element At Coordinates    ${db_nut_quet_qr_code_x}    ${db_nut_quet_qr_code_y}
    Sleep    5

Mo May anh tu man hinh khoa
    Log To Console    Mo May anh tu man hinh khoa
    AppiumLibrary.Swipe    ${cam_mh_khoa_start_x}    ${cam_mh_khoa_start_y}    ${cam_mh_khoa_offset_x}    ${cam_mh_khoa_offset_y}
    Sleep    5

Mo May anh roi mo AI MODE
    Log To Console    Mo May anh roi mo AI MODE
    Mo ung dung va cho phep cap quyen    ${cam_alias}    ${cam_package}    ${cam_activity}    2
    AppiumLibrary.Click Element    ${cam_ai_mode}
    Sleep    2

Bam nut mo che do CHUP ANH
    Log To Console    Bam nut mo che do CHUP ANH
    AppiumLibrary.Click Element    ${cam_chup_anh}
    Sleep    2

Xac nhan mo CHUP ANH thanh cong
    Log To Console    Xac nhan mo CHUP ANH thanh cong
    AppiumLibrary.Page Should Contain Element    ${cam_nut_hdr}
    AppiumLibrary.Page Should Contain Element    ${cam_nut_flash}
    AppiumLibrary.Page Should Contain Element    ${cam_nut_bo_loc}
    AppiumLibrary.Page Should Contain Element    ${cam_nut_cai_dat}
    AppiumLibrary.Page Should Contain Element    ${cam_nut_auto_adv}
    AppiumLibrary.Page Should Contain Element    ${cam_nut_quay_chup}

Mo May anh roi mo QUAY PHIM
    Log To Console    Mo May anh roi mo QUAY PHIM
    Mo ung dung va cho phep cap quyen    ${cam_alias}    ${cam_package}    ${cam_activity}    2
    AppiumLibrary.Click Element    ${cam_quay_phim}
    Sleep    2
    Xac nhan mo QUAY PHIM thanh cong

Xac nhan mo QUAY PHIM thanh cong
    Log To Console    Xac nhan mo QUAY PHIM thanh cong
    AppiumLibrary.Page Should Contain Element    ${cam_nut_video_fps}
    AppiumLibrary.Page Should Contain Element    ${cam_nut_ghi_am}
    AppiumLibrary.Page Should Contain Element    ${cam_nut_flash}
    AppiumLibrary.Page Should Contain Element    ${cam_nut_bo_loc}
    AppiumLibrary.Page Should Contain Element    ${cam_nut_cai_dat}
    AppiumLibrary.Page Should Contain Element    ${cam_nut_quay_chup}

Mo May anh roi mo che do chup Chan dung
    Log To Console    Mo May anh roi mo che do chup Chan dung
    Mo May anh roi mo AI MODE
    AppiumLibrary.Click Element    ${cam_chan_dung}
    Sleep    2
    Log To Console    Xac nhan mo che do chup Chan dung thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cam_scene_mode_text}    text    ${cam_chan_dung_txt}

Mo menu AI MODE roi mo che do chup Chan dung
    Log To Console    Mo menu AI MODE roi mo che do chup Chan dung
    AppiumLibrary.Click Element    ${cam_ai_mode}
    Sleep    1
    AppiumLibrary.Click Element    ${cam_chan_dung}
    Sleep    2
    Log To Console    Xac nhan mo che do chup Chan dung thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cam_scene_mode_text}    text    ${cam_chan_dung_txt}

Mo May anh roi mo che do chup sMacro
    Log To Console    Mo May anh roi mo che do chup sMacro
    Mo May anh roi mo AI MODE
    AppiumLibrary.Click Element    ${cam_s_macro}
    Sleep    1
    AppiumLibrary.Click Element    ${nut_dong_y}
    Sleep    2
    Log To Console    Xac nhan mo che do chup sMacro thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cam_scene_mode_text}    text    ${cam_s_macro_txt}

Mo menu AI MODE roi mo che do chup sMacro
    Log To Console    Mo menu AI MODE roi mo che do chup sMacro
    AppiumLibrary.Click Element    ${cam_ai_mode}
    Sleep    1
    AppiumLibrary.Click Element    ${cam_s_macro}
    Sleep    1
    AppiumLibrary.Click Element    ${nut_dong_y}
    Sleep    2
    Log To Console    Xac nhan mo che do chup sMacro thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cam_scene_mode_text}    text    ${cam_s_macro_txt}

Mo May anh roi mo che do chup San pham
    Log To Console    Mo May anh roi mo che do chup San pham
    Mo May anh roi mo AI MODE
    AppiumLibrary.Click Element    ${cam_san_pham}
    Sleep    1
    AppiumLibrary.Click Element    ${nut_dong_y}
    Sleep    2
    Log To Console    Xac nhan mo che do chup San pham thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cam_scene_mode_text}    text    ${cam_san_pham_txt}

Mo menu AI MODE roi mo che do chup San pham
    Log To Console    Mo menu AI MODE roi mo che do chup San pham
    AppiumLibrary.Click Element    ${cam_ai_mode}
    Sleep    1
    AppiumLibrary.Click Element    ${cam_san_pham}
    Sleep    1
    AppiumLibrary.Click Element    ${nut_dong_y}
    Sleep    2
    Log To Console    Xac nhan mo che do chup San pham thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cam_scene_mode_text}    text    ${cam_san_pham_txt}

Mo May anh roi mo che do chup Khoanh khac
    Log To Console    Mo May anh roi mo che do chup Khoanh khac
    Mo May anh roi mo AI MODE
    AppiumLibrary.Click Element    ${cam_khoanh_khac}
    Sleep    1
    AppiumLibrary.Click Element    ${nut_dong_y}
    Sleep    2
    Log To Console    Xac nhan mo che do chup Khoanh khac thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cam_scene_mode_text}    text    ${cam_khoanh_khac_txt}

Mo menu AI MODE roi mo che do chup Khoanh khac
    Log To Console    Mo menu AI MODE roi mo che do chup Khoanh khac
    AppiumLibrary.Click Element    ${cam_ai_mode}
    Sleep    1
    AppiumLibrary.Click Element    ${cam_khoanh_khac}
    Sleep    1
    AppiumLibrary.Click Element    ${nut_dong_y}
    Sleep    2
    Log To Console    Xac nhan mo che do chup Khoanh khac thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cam_scene_mode_text}    text    ${cam_khoanh_khac_txt}

Mo May anh roi mo che do chup sNight
    Log To Console    Mo May anh roi mo che do chup sNight
    Mo May anh roi mo AI MODE
    AppiumLibrary.Click Element    ${cam_s_night}
    Sleep    1
    AppiumLibrary.Click Element    ${nut_dong_y}
    Sleep    2
    Log To Console    Xac nhan mo che do chup sNight thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cam_scene_mode_text}    text    ${cam_s_night_txt}

Mo menu AI MODE roi mo che do chup sNight
    Log To Console    Mo menu AI MODE roi mo che do chup sNight
    AppiumLibrary.Click Element    ${cam_ai_mode}
    Sleep    1
    AppiumLibrary.Click Element    ${cam_s_night}
    Sleep    1
    AppiumLibrary.Click Element    ${nut_dong_y}
    Sleep    2
    Log To Console    Xac nhan mo che do chup sNight thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cam_scene_mode_text}    text    ${cam_s_night_txt}

Mo May anh roi mo che do chup Goc rong
    Log To Console    Mo May anh roi mo che do chup Goc rong
    Mo May anh roi mo AI MODE
    AppiumLibrary.Click Element    ${cam_goc_rong}
    Sleep    1
    AppiumLibrary.Click Element    ${nut_dong_y}
    Sleep    2
    Log To Console    Xac nhan mo che do chup Goc rong thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cam_scene_mode_text}    text    ${cam_goc_rong_txt}

Mo menu AI MODE roi mo che do chup Goc rong
    Log To Console    Mo menu AI MODE roi mo che do chup Goc rong
    AppiumLibrary.Click Element    ${cam_ai_mode}
    Sleep    1
    AppiumLibrary.Click Element    ${cam_goc_rong}
    Sleep    1
    AppiumLibrary.Click Element    ${nut_dong_y}
    Sleep    2
    Log To Console    Xac nhan mo che do chup Goc rong thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cam_scene_mode_text}    text    ${cam_goc_rong_txt}

Mo May anh roi mo THEM NUA
    Log To Console    Mo May anh roi mo THEM NUA
    Mo ung dung va cho phep cap quyen    ${cam_alias}    ${cam_package}    ${cam_activity}    2
    AppiumLibrary.Click Element    ${cam_them_nua}
    Sleep    2

Mo May anh roi mo che do chup Chuyen nghiep
    Log To Console    Mo May anh roi mo che do chup Chuyen nghiep
    Mo May anh roi mo THEM NUA
    AppiumLibrary.Click Element    ${cam_chuyen_nghiep}
    Sleep    1
    AppiumLibrary.Click Element    ${nut_dong_y}
    Sleep    5
    Log To Console    Xac nhan mo che do chup Chuyen nghiep thanh cong
    AppiumLibrary.Element Attribute Should Match    ${cam_scene_mode_text}    text    ${cam_chuyen_nghiep_txt}

Bam nut quay phim chup anh
    [Arguments]    ${thoi_gian_sleep}
    Log To Console    Bam nut quay phim chup anh
    AppiumLibrary.Click Element    ${cam_nut_quay_chup}
    Sleep    ${thoi_gian_sleep}

Bam nut chuyen doi cam truoc sau
    Log To Console    Bam nut chuyen doi cam truoc sau
    AppiumLibrary.Click Element    ${cam_nut_truoc_sau}
    Sleep    2

Mo che do xem cuon phim
    Log To Console    Mo che do xem cuon phim
    AppiumLibrary.Click Element    ${cam_xem_cuon_phim}
    Sleep    2

Bam de hien thi ten va tuy chon cua file
    Log To Console    Bam de hien thi thong tin va tuy chon anh
    AppiumLibrary.Click Element    ${cam_snackbar_container}
    Sleep    1

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================