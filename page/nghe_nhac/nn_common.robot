*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot

*** Keywords ***
An nut dung hoac phat nhac
    [Documentation]    Phat va dung nhac
    Click Element    ${nn_nut_phat_dung_nhac}
    Sleep    2

An nut dang phat
    [Documentation]    Chuyen tab dang phat
    Click Element    ${nn_nut_dang_phat}
    Sleep    2

An nut thu muc
    [Documentation]    Chuyen tab thu muc
    Click Element    ${nn_nut_thu_muc}
    Sleep    2

An nut danh sach
    [Documentation]    Chuyen tab danh sach
    Click Element    ${nn_nut_danh_sach}
    Sleep    2

An nut nghe si
    [Documentation]    Chuyen tab nghe si
    Click Element    ${nn_nut_nghe_si}
    Sleep    2

An nut album
    [Documentation]    Chuyen tab album
    Click Element    ${nn_nut_album}
    Sleep    2

An nut tuy chon nghe nhac
    [Documentation]    Mo menu tuy chon
    Click Element    ${nn_nut_tuy_chon}
    Sleep    2

An checkbox an bai hat dung luong thap
    [Documentation]    Tick checkbox
    Click Element    ${nn_checkbox_an_baihat_dl_thap}
    Sleep    2

An phat nhac va dung nhac tren notify
    [Documentation]    Dung nhac va phat nhac
    Click Element    ${nn_notify_play_pause}
    Sleep    1

An my recordings
    [Documentation]    an my recording
    Click Element    ${nn_danh_sach_my_recording}
    Sleep    2

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================