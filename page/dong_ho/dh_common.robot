*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Mo Dong ho roi mo giao dien Bao thuc
    Log To Console    Mo Dong ho roi mo giao dien Bao thuc
    Mo ung dung va cho phep cap quyen    ${dh_alias}    ${dh_package}    ${dh_activity}    2
    AppiumLibrary.Click Element    ${dh_tab_bao_thuc}
    Sleep    2

# Click cac nut tab menu
Nhan nut tab menu top bao thuc
    [Documentation]    Nhan vao bao thuc kiem tra hoat dong
    AppiumLibrary.Click Element    ${dh_tab_bao_thuc}
    AppiumLibrary.Element Should Be Visible    ${dh_tab_bao_thuc}
    Sleep    1

Nhan nut tab menu top dong ho
    [Documentation]    Nhan vao dong ho kiem tra hoat dong
    AppiumLibrary.Click Element    ${dh_tab_dong_ho}
    AppiumLibrary.Element Should Be Visible    ${dh_tab_dong_ho}
    Sleep    1

Nhan nut tab menu top hen gio
    [Documentation]    Nhan vao hen gio kiem tra hoat dong
    AppiumLibrary.Click Element    ${dh_tab_bo_hen_gio}
    AppiumLibrary.Element Should Be Visible    ${dh_tab_bo_hen_gio}
    Sleep    1

Nhan nut tab menu top bam gio
    [Documentation]    Nhan vao bam gio kiem tra hoat dong
    AppiumLibrary.Click Element    ${dh_tab_bam_gio}
    Sleep    1

Nhan nut tab menu top tuy chon
    [Documentation]    Nhan vao tuy chon va kiem tra trang thai hoat dong
    AppiumLibrary.Click Element    ${dh_tab_tuy_chon_khac}
    Sleep    1

# click cac nut chuc nang
Nhan nut icon gio quoc te
    [Documentation]    Nhan nut icon gio quoc te
    AppiumLibrary.Click Element    ${dh_btn_thanh_pho}
    Sleep    1

Nhan nut icon back quay lai
    [Documentation]    Nhan nut icon back quay lai
    AppiumLibrary.Click Element    ${dh_btn_nut_back}
    Sleep    1

Nhan nut icon tim kiem
    [Documentation]    Nhan vao icon tim kiem, kiem tra trang thai hoat dong
    AppiumLibrary.Click Element    ${dh_btn_icon_search}
    Sleep    1

Nhan nut xoa text tim kiem
    [Documentation]    Nhan nut xoa text tim kiem
    AppiumLibrary.Click Element    ${dh_icon_xoa_text_tim_kiem}
    Sleep    1

Nhan o checkbox tich chon hien thi
    [Documentation]    Nhan o checkbox tich chon hien thi
    AppiumLibrary.Click Element At Coordinates        577    843
    Sleep    1

Nhan nut chuyen danh sach gio quoc te sang dinh dang khac
    [Documentation]    thay doi ding dang khac
    AppiumLibrary.Click Element At Coordinates        759    138
    Sleep    1

Nhan nut cai dat
    [Documentation]    nhan nut cai dat theo toa do
    AppiumLibrary.Click Element At Coordinates        646    261
    Sleep    1

Check hien thi man hinh dong ho thoi gian
    [Documentation]    Check hien thi thoi gian kiem tra verify
    ${get_text}=     AppiumLibrary.Get Text    ${dh_hien_thi_text_dong_ho}
    Element Attribute Should Match     ${dh_hien_thi_text_dong_ho}   text    *:*
    Sleep    1

Check hien thi ngay thang date time
    [Documentation]    Check hien thi ngay thang date time kiem tra verify
    Page Should Contain Element    ${dh_hien_thi_text_thoi_gian}
    Sleep    1

Check hien thi chu cai ten cua cac nuoc quoc te
    [Documentation]    Check hien thi chu cai ten cua cac nuoc kiem tra verify
    Page Should Contain Element    ${dh_hien_thi_text_chu_cai}
    Sleep    1

Check hien thi ten cua cac nuoc
    [Documentation]    Check hien thi ten cua cac nuoc kiem tra verify
    Page Should Contain Element    ${dh_dh_input_tim_thanh_pho}
    Sleep    1

Check hien thi thoi gian canh ten cac nuoc
    [Documentation]    Check hien thi thoi gian canh ten cac nuoc kiem tra verify
    Page Should Contain Element    ${dh_dh_input_tim_thanh_pho}
    Sleep    1

Ckeck vuot launcher len tren
    [Documentation]    Check vuot launcher tu duoi len tren
    Swipe By Percent    50    90    50    10
    Sleep    1

Ckeck vuot launcher xuong
    [Documentation]    Check vuot launcher tu tren xuong duoi
    Swipe By Percent    50    10    50    90
    Sleep    1

Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    [Documentation]    Check dem phan tu hien thi danh sach cac nuoc tren man hinh
    [Return]    ${get_phan_tu}
    ${get_phan_tu}=    Get Matching Xpath Count    xpath=//*[contains(@resource-id,'bkav.android.deskclock:id/city_onoff')]
    Sleep    1
Check dem phan tu hien thi danh sach gio quoc te da duoc tich chon
    [Documentation]    Check dem phan tu hien thi danh sach gio quoc te da duoc tich chon
    [Return]    ${get_phan_tu}
    ${get_phan_tu}=    Get Matching Xpath Count    xpath=//*[contains(@resource-id,'bkav.android.deskclock:id/hours_ahead')]
    Sleep    1
Check kiem tra verify o tim kiem
    [Documentation]    Check kiem tra verify o tim kiem
    Page Should Contain Element    ${dh_text_tim_kiem}

Check kiem tra verify hien thi gio quoc te theo thoi gian
    [Documentation]    Check verify hien thi gio quoc te theo thoi gian
    Page Should Contain Element    ${dh_hien_thi_text_chu_cai}
    Sleep    1

Check kiem tra verify hien thi mui gio trong cai dat
    [Documentation]    Check verify hien thi gio quoc te theo thoi gian
    Element Attribute Should Match   ${dh_check_hien_thi_mui_gio}   text   ${mui_gio_thanh_pho}
    Sleep    1

Thuc hien input text tim kiem
    [Documentation]    Thuc hien input text tim kiem
    AppiumLibrary.Input Text    ${dh_text_tim_kiem}    ${dh_dh_input_ten_mot_tu}
    Sleep    1

Thuc hien input text tim kiem ha noi khong dau
    [Documentation]    Thuc hien input text tim kiem ha noi khong dau
    AppiumLibrary.Input Text    ${dh_text_tim_kiem}    ${dh_dh_input_ten_khong_dau}
    Sleep    1

Thuc hien input text tim kiem chu in hoa
    [Documentation]    Thuc hien input text tim kiem chu in hoa
    AppiumLibrary.Input Text     ${dh_text_tim_kiem}    ${dh_dh_input_ten_hoa}
    Sleep    1

Thuc hien input text tim kiem chu thuong co dau
    [Documentation]    Thuc hien input text tim kiem chu thuong co dau
    AppiumLibrary.Input Text    ${dh_text_tim_kiem}    ${dh_dh_input_ten_co_dau}
    Sleep    1

Thuc hien input text tim kiem khong co du lieu
    [Documentation]    Thuc hien input text tim kiem khong co du lieu
    AppiumLibrary.Input Text    ${dh_text_tim_kiem}    ${dh_dh_input_ten_chuoi_ky_tu}
    Sleep    1

Copy text Paste noi dung da copy
    [Documentation]    copy noi dung paste vao o tim kiem
    Page Should Contain Element    ${dh_text_tim_kiem}
    Sleep    1

Clear text tim kiem
    [Documentation]    clear text tren o tim kiem
    Clear Text    ${dh_text_tim_kiem}
    Sleep    1

Nhan cham va giu vao thanh pho da them tren giao dien dong ho
    [Documentation]    Nhan cham va giu vao thanh pho da them tren giao dien dong ho
    AppiumLibrary.Long Press    ${dh_hien_thi_text_dong_ho}    1000
    Sleep    1

Nhap chuot
    [Documentation]    Nhan cham va giu vao thanh pho da them tren giao dien dong ho
    AppiumLibrary.Long Press    ${dh_hien_thi_text_dong_ho}    100
    Sleep    1

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================