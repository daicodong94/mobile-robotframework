*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Cho phep dong ho set and clear
    [Documentation]    cho phep dong ho set an clear
    Log To Console    bam vao cho phep
    AppiumLibrary.Click Element    ${dh_cd_cho_phep_dong_ho_set}
    Sleep    2

Vuot dong giao dien bao thuc
    [Documentation]    dong giao dien bao thuc
    Log To Console    tro ve man hinh lancher
    Swipe By Percent    0    50    20    50
    Sleep    2

# Mo bao thuc nhanh tu bang dieu khien
    # [Documentation]    mo bao thuc tu bang dieu khien
    # Log To Console    bam vao bao bao thuc
    # Swipe By Percent    0    80    40    75

Bam vao nut bao thuc
    [DOCUMENTATION]    Mo bao thuc tu bang dieu khien
    LOG TO CONSOLE    bam vao bao thuc
    AppiumLibrary.Click Element    ${dh_tab_bao_thuc}
    Sleep    2

Bam dong toan bo app tren da nhiem
    [Documentation]    tat bao thuc nhanh
    Click Element At Coordinates    ${dh_bt_nut_tat_da_nhiem_x}    ${dh_bt_nut_tat_da_nhiem_y}
    Sleep    1

Bam vao bao thuc nhanh
    [Documentation]    bam vao bao thuc nhanh
    Log To Console    Mo giao dien bao thuc nhanh
    AppiumLibrary.Click Element    ${dh_bt_bao_thuc_nhanh}
    Sleep    2

Vuot tu mep phai vao giua man hinh
    [Documentation]    Vuot tat bao thuc nhanh
    Log To Console    Tat bao thuc nhanh
    Swipe By Percent    100    60    70    60
    Sleep    2

vuot tu mep duoi vao giua man hinh
    [Documentation]    tat bao thuc nhanh tu duoi len
    Log To Console    Tat bao thuc nhanh
    Swipe By Percent    50    99    50    70
    Sleep    2

kiem tra nut xong khong hien thi
    [Documentation]    kiem tra nut xong khong hien thi
    AppiumLibrary.Page Should not Contain Element    ${dh_bt_btn_xong}

Cham tat bao thuc nhanh
    [Documentation]    tat bao thuc nhanh
    Click Element At Coordinates    ${dh_bt_nut_tat_bao_thuc_nhanh_x}    ${dh_bt_nut_tat_bao_thuc_nhanh_y}
    Sleep    1

Vuot di chuyen cac moc thoi gian trong bao thuc nhanh
    [Documentation]    Vuot di chuyen cac moc thoi gian
    Log To Console    Vuot di chuyen cac moc thoi gian
    Swipe By Percent    50    60    50    25
    Sleep    2

Bam nut tao xong bao thuc
    [Documentation]    Vuot di chuyen cac moc thoi gian
    Log To Console    Vuot di chuyen cac moc thoi gian
    AppiumLibrary.Click Element    ${dh_bt_btn_xong}
    Sleep    2

Bam vao them bao thuc
    AppiumLibrary.Click Element    ${dh_bt_btn_nut_them_bao_thuc}
    Sleep    2

Chon thoi gian bao thuc trong them bao thuc
    AppiumLibrary.Click Element    ${dh_bt_nut_6}
    Sleep    2

Bam vao huy
    AppiumLibrary.Click Element    ${dh_bt_nut_huy}
    Sleep    2

Bam vao huy nhan
    AppiumLibrary.Click Element    ${dh_bt_nut_huy_nhan}
    Sleep    2

Bam vao dong y bao thuc
    AppiumLibrary.Click Element    ${dh_bt_nut_dong_y}
    Sleep    2

Bam vao bao nut ban phim
    AppiumLibrary.Click Element    ${dh_bt_nut_ban_phim}
    Sleep    2

Bam vao lap lai bao thuc
    AppiumLibrary.Click Element    ${dh_bt_nut_lap_lai_bao_thuc}
    Sleep    2

Bam vao cac thu trong bao thuc 2 3 4
    AppiumLibrary.Click Element    ${dh_bt_txt_hien_thi_thu_hai}
    AppiumLibrary.Click Element    ${dh_bt_txt_hien_thi_thu_ba}
    AppiumLibrary.Click Element    ${dh_bt_txt_hien_thi_thu_tu}
    Sleep    2

Bam vao cac thu trong bao thuc 5 6 7 cn
    AppiumLibrary.Click Element    ${dh_bt_txt_hien_thi_thu_nam}
    AppiumLibrary.Click Element    ${dh_bt_txt_hien_thi_thu_sau}
    AppiumLibrary.Click Element    ${dh_bt_txt_hien_thi_thu_bay}
    AppiumLibrary.Click Element    ${dh_bt_txt_hien_thi_chu_nhat}
    Sleep    2

Bam vao chuong
    AppiumLibrary.Click Element    ${dh_bt_chuong_mac_dinh}
    Sleep    2

Bam vao them am thanh moi
    AppiumLibrary.Click Element    ${dh_bt_them_am_thanh_moi}
    Sleep    2

vuot dong giao dien
    Swipe By Percent    0    50    20    50
    Sleep    2

Bam dong giao dien am thanh
    AppiumLibrary.Click Element    ${dh_bt_back_am_thanh_bao_thuc}
    Sleep    2

Bam vao rung
    AppiumLibrary.Click Element    ${dh_bt_cd_rung}
    Sleep    2

Bam vao nhan
    AppiumLibrary.Click Element    ${dh_bt_label_nhan_dan}
    Sleep    2

Click xoa chu tren text nhan
    Click A Point    ${gtv_nut_xoa_x}    ${gtv_nut_xoa_y}    duration=1000
    Sleep    1

Bam vao thanh nhap text nhan
    AppiumLibrary.Click Element    ${dh_bt_label_nhan_text}
    Sleep    2

Bam vao icon mo popup sua bo bao thuc
    AppiumLibrary.Click Element    ${dh_bt_nut_mo_popup_sua_bao_thuc}
    Sleep    2

Bam vao xoa
    [Documentation]    Bam vao nut xoa
    Log To Console    Bam vao nut xoa
    AppiumLibrary.Click Element    ${dh_bt_nut_xoa_bao_thuc}
    Sleep    2

Bam vao thu gon bao thuc
    AppiumLibrary.Click Element    ${dh_bt_tab_thu_gon_bao_thuc}
    Sleep    2

Bam vao text dong ho bao thuc hien thi
    AppiumLibrary.Click Element    ${dh_btn_dong_ho_so}
    Sleep    2

Bam vao mo rong bao thuc
    AppiumLibrary.Click Element    ${dh_cd_mo_rong_bao_thuc}
    Sleep    2

Bam vao mo rong bao thuc xoa lan luot bao thuc
    AppiumLibrary.Click Element    ${dh_bt_mo_rong_bao_thuc}
    Sleep    2

cham thoat bao thuc nhanh b3mm
    [Documentation]    Vuot tat bao thuc nhanh
    Log To Console    Tat bao thuc nhanh
    Swipe By Percent    50    10    40    10
    Sleep    2

Cham chon so gio dat bao thuc b3mm
    [Documentation]    tat bao thuc nhanh
    Click Element At Coordinates    ${dh_bt_nut_tat_bao_thuc_nhanh_x}    ${dh_bt_nut_tat_bao_thuc_nhanh_y}
    Sleep    1

Bam vao mo danh sach nha chuong
    AppiumLibrary.Click Element    ${dh_bt_mo_danh_sach_nhac_chuong}
    Sleep    2

Bam vao mo danh sach nhac chuong tai file chon
    AppiumLibrary.Click Element    ${dh_bt_chon_file_nhac_chuong}
    Sleep    2

Bam vao icon bat tat bao thuc
    Click Element    ${dh_cd_btn_nut_switch_on}

Bam vao icon tat bao thuc
    Click Element    ${dh_cd_btn_nut_switch}
    Sleep    1

Dat thoi gian bao thuc
   Input Text    ${dh_bt_input_gio}    9
   Input Text    ${dh_bt_input_phut}    29
   Sleep    1
   Page Should Contain Text    9
   Page Should Contain Text    29
   Sleep    1

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================