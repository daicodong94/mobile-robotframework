*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Cham vao dau ba cham
    [Documentation]    Cham vao dau ba cham
    Log To Console    Cham vao dau ba cham
    AppiumLibrary.Click Element    ${dh_cd_tuy_chon_khac}
    Sleep    1

Nhan vao nut cai dat
    AppiumLibrary.Click Element At Coordinates    600    300
    Sleep    1

giao dien bao thuc
    AppiumLibrary.Click Element    ${dh_tab_bao_thuc}
    Sleep    1

Giao dien dong ho
    AppiumLibrary.Click Element    ${dh_tab_dong_ho}
    Sleep    1

Giao dien bo hen gio
    AppiumLibrary.Click Element    ${dh_tab_bo_hen_gio}
    Sleep    1

Giao dien dong ho bam gio
    AppiumLibrary.Click Element    ${dh_tab_bam_gio}
    Sleep    1

Kieu hien thi
    AppiumLibrary.Click Element    ${dh_hien_thi_kieu_dong_ho}
    Sleep    2

Hien thi dong ho kim
    AppiumLibrary.Click Element    ${dh_hien_thi_kieu_dong_ho_kim}
    Sleep    1

Hien thi dong ho so
    AppiumLibrary.Click Element    ${dh_btn_check_kieu_dong_ho}
    Sleep    1

Cham on off dong ho
    AppiumLibrary.Click Element At Coordinates    450    920
    Sleep    1

cham vao nut on off dong ho tu dong
    AppiumLibrary.Click Element At Coordinates    950    930
    Sleep    1

cham mui gio
    AppiumLibrary.Click Element    ${dh_check_mui_gio_ho_chi_minh}
    Sleep    1

Chon mui gio dubai
    AppiumLibrary.Click Element    ${dh_check_mui_gio_du_bai}
    Sleep    1

Chon mui gio Jakarta
    AppiumLibrary.Click Element    ${dh_check_mui_gio_hien_thi_jakarta}
    Sleep    1

Cham vao mui gio
    AppiumLibrary.Click Element    ${dh_cd_mui_gio}
    Sleep    1

Cham khu vuc
    AppiumLibrary.Click Element    ${dh_cd_khu_vuc}
    Sleep    1

Vuot xuong
    Swipe By Percent    50    20    50    50
    Sleep    1

Vuot len
    Swipe By Percent    50    70    50    50
    Sleep    1

Cham vao huy
    AppiumLibrary.Click Element    ${dh_btn_huy}
    Sleep    1

cham vao dong y
    AppiumLibrary.Click Element    ${dh_bt_nut_dong_y}
    Sleep    1

Cham vao thay doi ngay gio
    AppiumLibrary.Click Element    ${dh_cd_thay_doi_ngay_gio}
    Sleep    1

ngay trong thay doi ngay va gio
    AppiumLibrary.Click Element At Coordinates    ${dh_cai_dat_x}    ${dh_cai_dat_y}
    sleep    1

Cham vao thoi gian trong ngay va gio
    AppiumLibrary.Click Element At Coordinates    300    800
    sleep    1

Cham vao ngay 1 trong thay doi ngay gio
    AppiumLibrary.Click Element At Coordinates    750    1006
    Sleep    1

cham vao mo rong trong bao thuc
    AppiumLibrary.Click Element    ${dh_cd_mo_rong_bao_thuc}
    sleep    1

Cham vao xoa trong bao thuc
    AppiumLibrary.Click Element    ${tt_nut_xoa}
    sleep    1

Cham xoa trong bao thuc
    AppiumLibrary.Click Element    ${dh_hg_nut_xoa_thoi_gian_thiet_lap}
    sleep    1

Cham vao im lang sau
    AppiumLibrary.Click Element    ${dh_cd_im_lang_sau}
    Sleep    1

Cham vao 5 phut
    AppiumLibrary.Click Element    ${dh_cd_im_lang_sau_5_phut}
    Sleep    1

Cham vao thoi luong bao thuc
    AppiumLibrary.Click Element    ${dh_cd_thoi_luong_bao_lai}
    Sleep    1

Cham tang dan am luong
    AppiumLibrary.Click Element    ${dh_cd_tang_dan_am_luong}
    Sleep    1

Check am thanh hen gio mac dinh
    [Documentation]    check hien thi mac dinh hen gio
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_txt_bo_hen_gio_het_han}
    Should Be Equal    Bộ hẹn giờ đã hết hạn    ${get_text}

Check hien thi ten am thanh Argon
    [Documentation]    check hien thi mac dinh hen gio
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_txt_Argon_am_nhac}
    Should Be Equal    Argon    ${get_text}

Check hien thi them am thanh moi
    [Documentation]    Check hien thi them am thanh moi
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_txt_them_am_thanh_moi}
    Should Be Equal    Thêm âm thanh mới    ${get_text}

Check hien thi icon duoc tich chon am thanh
    AppiumLibrary.Element Should Be Visible    ${dh_cd_icon_hien_thi_duoc_chon}
    Sleep    1

Check dem phan tu am thanh khi mac dinh va vuot len
    [Documentation]    Check dem phan tu am thanh khi mac dinh va vuot len
    ${get_phan_tu}=    Get Matching Xpath Count    //*[contains(@resource-id,'bkav.android.deskclock:id/ringtone_name')]
    Should Be Equal    11    ${get_phan_tu}

Check dem phan tu am thanh khi vuot xuong
    [Documentation]    Check dem phan tu am thanh khi vuot xuong
    ${get_phan_tu}=    Get Matching Xpath Count    //*[contains(@resource-id,'bkav.android.deskclock:id/ringtone_name')]
    Should Be Equal    13    ${get_phan_tu}

Nhan vao am thanh hen gio
    [Documentation]    nhan vao am thanh hen gio
    AppiumLibrary.Click Element    ${dh_cd_txt_am_thanh_hen_gio}
    Sleep    1

Nhan vao tang dan am luong
    [Documentation]    nhan vao am thanh hen gio
    AppiumLibrary.Click Element At Coordinates        404    1746
    Sleep    1

Nhan vao ten am thanh Argon
    [Documentation]    tich chon am thanh hen gio Argon
    AppiumLibrary.Click Element    ${dh_cd_txt_Argon_am_nhac}
    Sleep    1

Nhan vao icon thoi gian tang am luong
    [Documentation]    nhan vao am thanh hen gio
    ${get_text}=    AppiumLibrary.Get Text    //*[contains(@clickable,'true')][5]
    AppiumLibrary.Click Element     //*[contains(@clickable,'true')][5]
    Sleep    1

Check hien thi trang thai khong hoat dong cua switch
    [Documentation]    Check hien thi trang thai khong hoat dong cua switch
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_btn_nut_switch}
    Should Be Equal    TẮT    ${get_text}
    Sleep    1

Check hien thi trang thai hoat dong cua switch
    [Documentation]    Check hien thi trang thai hoat dong cua switch
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_btn_nut_switch_on}
    Should Be Equal    BẬT    ${get_text}

Nhan vao icon switch bat hoat dong
    [Documentation]    Nhan vao icon switch bat hoat dong
    AppiumLibrary.Click Element    ${dh_cd_btn_nut_switch}
    Sleep    1

Nhan vao tuan bat dau
    [Documentation]    Nhan vao tuan bat dau
    AppiumLibrary.Click Element    ${dh_cd_txt_tuan_bat_dau_vao}
    Sleep    1

Nhan vao chu nhat
    [Documentation]    Nhan vao chu nhat
    AppiumLibrary.Click Element    ${dh_cd_txt_chu_nhat}
    Sleep    1

Check hien thi thu hai
    [Documentation]    Check hien thi thu hai
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_txt_thu_hai}
    Should Be Equal    ${get_text}    ${dh_cd_text_thu_hai}
    Sleep    1

Check hien thi chu nhat
    [Documentation]    Check hien thi chu nhat
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_txt_chu_nhat}
    Should Be Equal     ${get_text}    ${dh_cd_text_chu_nhat}
    Sleep    1

Check hien thi thu bay
    [Documentation]    Check hien thi chu nhat
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_txt_thu_bay}
    Should Be Equal    ${get_text}    ${dh_cd_text_thu_bay}
    Sleep    1

Nhan vao hanh dong lac may
    [Documentation]    Nhan vao hanh dong lac may
    AppiumLibrary.Click Element    ${dh_cd_txt_lac_may_khong_lam_gi}
    Sleep    1

Nhan vao bao lai
    [Documentation]    Nhan vao bao lai
    AppiumLibrary.Click Element    ${dh_cd_txt_bao_lai}
    # AppiumLibrary.Click Element At Coordinates        300    917

Check hien thi hanh dong khi duoc chon
    [Documentation]    Check hien thi hanh dong khi duoc chon
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_txt_hien_thi_bao_lai}
    Should Be Equal    Lắc máy sẽ nhắc lại báo thức    ${get_text}
    Sleep    1

Check hien thi phan tu tren danh sach droplist
    [Documentation]    Check hien thi phan tu tren danh sach droplist
    ${get_phan_tu}=    Get Matching Xpath Count    //*[contains(@resource-id,'android:id/text1')]
    Should Be Equal    3    ${get_phan_tu}
    ${get_text}=    AppiumLibrary.Get Text    ${dh_cd_txt_bao_lai}
    Should Be Equal    Báo lại    ${get_text}
    ${get_text2}=    AppiumLibrary.Get Text    ${dh_cd_txt_loai_bo}
    Should Be Equal    Loại bỏ    ${get_text2}
    ${get_text3}=    AppiumLibrary.Get Text    ${dh_cd_txt_khong_lam_gi}
    Should Be Equal    Không làm gì    ${get_text3}

Vuot launcher xuong duoi b86
    [Documentation]    Vuot launcher xuong duoi
    Swipe By Percent    50    80    50    20
    Sleep    1

Click icon tat tu dong cap nhat gio theo mang
   AppiumLibrary.Click Element    ${dh_cd_icon_bat_mui_gio_tu_dong}

Vuot xuong mm
    Swipe    400    1267    400    932
    Swipe    400    1180    400    932
    Sleep    1

Click icon tat tu dong cap nhat ngay theo mang
    Click Element    ${dh_cd_icon_bat_ngay_tu_dong}
    Sleep    1

Click icon on va off tu dong mui gio chinh
    Click Element    ${on_off_an_or_hien_thanh_trang_thai}
    Sleep    1

Click cai dat thoi gian date
    Click Element    ${dh_cd_thoi_gian}
    Sleep    1

Click cai dat ngay
    Click Element    ${dh_cd_ngay}
    Sleep    1

Vuot launcher len tren
    Swipe By Percent    50    95    10    50
    Sleep    1

Bam vao mo danh sach nhac chuong tai file chon
    AppiumLibrary.Click Element    ${dh_bt_chon_file_nhac_chuong}
    Sleep    2

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================