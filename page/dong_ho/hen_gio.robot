*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

***variables***
# Cac phan tu Main interface
${dh_alias}    dong_ho
${dh_package}    bkav.android.deskclock
${dh_activity}    com.android.deskclock.DeskClock

*** Keywords ***
# nhan icon bao thuc
Nhan nut bao thuc
    [Documentation]    nhan icon bao thuc
    Log To Console    nhan icon bao thuc
    AppiumLibrary.Click Element    ${dh_tab_bao_thuc}
    Sleep    1
# Click nut dong ho

Nhan nut dong ho
    [Documentation]    nhan icon dong ho
    Log To Console    nhan icon dong ho
    AppiumLibrary.Click Element    ${dh_tab_dong_ho}
    Sleep    1
# Click nut dong ho bam gio
Nhan nut dong ho bam gio
    [Documentation]    nhan icon dong ho bam gio
    Log To Console    nhan icon dong ho bam gio
    AppiumLibrary.Click Element    ${dh_tab_bam_gio}
    Sleep    1

#Click nut hen gio
Nhan nut menu top hen gio
    [Documentation]    Nhan nut menu top hen gio
    Log To Console    Nhan nut menu top hen gio
    AppiumLibrary.Click Element    ${dh_tab_bo_hen_gio}
    AppiumLibrary.Element Should Be Visible    ${dh_tab_bo_hen_gio}
    Sleep    2

# Kiem tra emlement man hinh hen gio
Kiem tra emlement man hinh hen gio
    [Documentation]    Kiem tra emlement man hinh hen gio
    Log To Console    Kiem tra emlement man hinh hen gio
    Page Should Contain Element    ${dh_hg_hien_thi_thoi_gian_thiet_lap}
    Sleep    3
Xoa bo hen gio
    [Documentation]    xoa bo hen gio
    Log To Console    xoa bo hen gio
    AppiumLibrary.Click Element    ${dh_hg_xoa_thoi_gian_thiet_lap_hen_gio}
    Sleep    1
Thiet lap cai bo hen gio
    [Documentation]    Thiet lap cai bo hen gio
    Log To Console    Thiet lap cai bo hen gio
    AppiumLibrary.Click Element    ${dh_hg_nut_9}
    AppiumLibrary.Click Element    ${dh_hg_nut_6}
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_5}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}

Nhan nut bat dau hen gio
    [Documentation]    Nhan nut bat dau hen gio
    Log To Console    Nhan nut bat dau hen gio
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
Kiem tra veryfile man hinh hen gio da bat
    [Documentation]    Kiem tra veryfile man hinh hen gio da bat
    Log To Console    Kiem tra veryfile man hinh hen gio da bat
    ${get_text_expected}=    AppiumLibrary.Get Element Attribute    ${dh_hg_dong_ho_hien_thi}    text

Vuot thoat giao dien bao thuc mo tu notify
     [Documentation]     Vuot thoat giao dien bao thuc mo tu notify
     Log To Console      Vuot thoat giao dien bao thuc mo tu notify
     AppiumLibrary.Click Element    ${dh_hg_stop}
     Swipe By Percent    0    50    20    50
     Swipe By Percent    50    0    90    50
     AppiumLibrary.Click Element    ${dh_hg_nut_notyfi}
     Reset Application
     Sleep    2
     AppiumLibrary.Click Element    ${dh_hg_bat_dau}
     Sleep    2
     AppiumLibrary.Click Element    ${dh_hg_stop}
     Sleep    2
     Kiem tra veryfile man hinh hen gio da bat
     Sleep    1

Check hien thi nut Play Enabled
    [Documentation]    Check hien thi nut Play Enabled
    Log To Console    Check hien thi nut Play Enabled
    AppiumLibrary.Element Should Be Enabled    ${dh_hg_bat_dau}
    Sleep    1

Check khong hien thi nut Play disable
    [Documentation]    Check hien thi nut Play
    Log To Console    Check hien thi nut Play
    AppiumLibrary.Element Should Be Disabled    ${dh_hg_bat_dau}    ${True}
    Sleep    1

Thuc hien kiem tra cac chuc nang nhap tao bo hen gio
    [Documentation]    Thuc hien kiem tra nhap so va check nut xoa
    Log To Console    Thuc hien kiem tra nhap so va check nut xoa
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    # 13072021
    Log To Console    BP-674: Check TH enable icon Xoa
    Element Should Be Enabled      ${dh_hg_nut_xoa_thoi_gian_thiet_lap}
    Log To Console    BP-678: Check bam icon Xoa 1 lan
    AppiumLibrary.Click Element    ${dh_hg_nut_xoa_thoi_gian_thiet_lap}
    Element Should Be Disabled      ${dh_hg_nut_xoa_thoi_gian_thiet_lap}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_4}
    AppiumLibrary.Click Element    ${dh_hg_nut_4}
    Page Should Contain Text    00h 02m 44s
    Log To Console    BP-679: Check bam icon Xoa den khi xoa het
    AppiumLibrary.Click Element    ${dh_hg_nut_xoa_thoi_gian_thiet_lap}
    AppiumLibrary.Click Element    ${dh_hg_nut_xoa_thoi_gian_thiet_lap}
    Page Should Contain Text    00h 00m 02s
    AppiumLibrary.Click Element    ${dh_hg_nut_4}
    AppiumLibrary.Click Element    ${dh_hg_nut_4}
    Log To Console    BP-680: Check bam va giu icon Xoa
    Long Press    ${dh_hg_nut_xoa_thoi_gian_thiet_lap}    1000
    Sleep    1
    Page Should Contain Text    00h 00m 00s
    Log To Console    BP-683: Check bam va giu icon Xoa
    Page Should Not Contain Element    ${dh_hg_bat_dau}
    Sleep    1
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    Sleep    1
    Log To Console    BP-684: Check TH bam icon Play tren giao dien thiet lap thoi gian
    Page Should Contain Element    ${dh_hg_bat_dau}
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    1


Kiem tra play va pause bo hen gio
    Log To Console    BP-686: Check TH dung Bo hen gio bang thao tac bam vao icon Pause
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    1
    Log To Console    kiem tra verify dong ho hen gio
    Kiem tra veryfile man hinh hen gio da bat
    Sleep    1
    Log To Console    BP-690: Check TH tiep tuc chay Bo hen gio bang thao tac bam vao icon Pause
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    1
    Log To Console    BP-685: Check TH bam +1:00 tren giao dien Bo hen gio
    AppiumLibrary.Click Element    ${dh_hg_them_phut}
    Sleep    1
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    1
    Log To Console    BP-694: Check TH bam vao DAT LAI khi dang chay roi dung
    Sleep    1
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}
    Sleep    2
    Log To Console    Thuc hien Play lai
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    3
    Log To Console    BP-687: Check TH dung Bo hen gio bang thao tac bam vao thoi gian tren giao dien Bo hen gio
    AppiumLibrary.Click Element At Coordinates    557    1020
    Sleep    3
    Xoa bo hen gio

Thuc hien kiem tra text dat lai
   [Documentation]    Thuc hien kiem tra text dat lai
   Page Should Contain Text    ĐẶT LẠI
   Page Should Contain Element    ${dh_hg_nut_dat_lai_gio_hen}
   Sleep    2

Thuc hien tao hen gio check dat lai hen gio nhu ban dau khi nhan Pause
    [Documentation]    Thuc hien tao hen gio check dat lai hen gio nhu ban dau khi nhan Pause
    Log To Console    Thuc hien tao hen gio check dat lai hen gio nhu ban dau khi nhan Pause
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    Sleep    1
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    1
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    1
    Thuc hien kiem tra text dat lai
    Sleep    2

Thuc hien kiem tra text them 1 phut
   [Documentation]    Thuc hien kiem tra text them 1 phut
   Page Should Contain Text    + 1:00
   Page Should Contain Element    ${dh_hg_nut_dat_lai_gio_hen}
   Sleep    2

Thuc hien nhan dong ho hen gio dang chay
    [Documentation]    Thuc hien nhan dong ho hen gio dang chay
    Log To Console    Thuc hien nhan dong ho hen gio dang chay
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    Sleep    1
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    1
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    1
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}
    Sleep    1
    Thuc hien kiem tra text dat lai
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    1
    Log To Console    BP-692: Check TH hiển thị chức năng +1:00 bằng thao tác bấm vào icon Pause
    Thuc hien kiem tra text them 1 phut
    Sleep    2

 Thuc bam vao dat lai restet lai bo hen gio ve ban dau
    [Documentation]    Thuc bam vao dat lai restet lai bo hen gio ve ban dau
    Log To Console    Thuc bam vao dat lai restet lai bo hen gio ve ban dau
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Log To Console    Nhan nut Play
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_them_phut}
    Log To Console    Nhan nut them 1 phut (+1:00)
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    2
    Log To Console    Nhan nut dung lai Stop
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}
    ${get_text_expected}=    AppiumLibrary.Get Element Attribute    ${dh_hg_dong_ho_hien_thi}    text
    Sleep    2

 Thuc hien nhap vao nhan comment va thiet lap
    [Documentation]    Thuc bam vao dat lai restet lai bo hen gio ve ban dau
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    Log To Console    Nhan nut Play
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Log To Console    Nhan nut dung lai Stop
    Sleep    2
    Log To Console    BP-693: Check TH hien thi chuc nang +1:00 bang thao tac bam vao thoi gian dang chay trong giao dien Bo hen gio
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Log To Console    Nhan nut dat lai bo hen gio
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_nhan_comment}
    AppiumLibrary.Click Element    ${dh_hg_nhan_o_text_comment}
    AppiumLibrary.Input Text    ${dh_hg_input_text}    Thinhlq
    AppiumLibrary.Click Element    ${dh_hg_btn_dong_y_nhan_comment}
    AppiumLibrary.Click Element    ${dh_hg_nhan_comment}
    AppiumLibrary.Click Element    ${dh_hg_nhan_o_text_comment}
    Clear Text    ${dh_hg_input_text}
    AppiumLibrary.Input Text    ${dh_hg_input_text}    Thinhlq1234
    AppiumLibrary.Click Element    ${dh_hg_btn_dong_y_nhan_comment}
    ${get_text_expected}=    AppiumLibrary.Get Element Attribute    ${dh_hg_nhan_comment}    text
    Page Should Contain Text    ${get_text_expected}    text
    AppiumLibrary.Element Text Should Be    ${dh_hg_nhan_comment}    ${get_text_expected}
    AppiumLibrary.Click Element    ${dh_hg_nhan_comment}
    AppiumLibrary.Click Element    ${dh_hg_btn_huy_nhan_comment}
    Sleep    3

Thuc hien kiem tra notify bo hen gio qua han thong bao
    [Documentation]    Thuc hien kiem tra notify bo hen gio qua han thong bao
    [Return]    ${check_enabled_notify}
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    Nhan nut bat dau hen gio
    Sleep    10
    ${check_enabled_notify}=    AppiumLibrary.Click Element At Coordinates    350    300
    Element Should Not Contain Text    ${dh_hg_check_notify_text}    Hết giờ
    Sleep    2

Thuc hien kiem tra bo hen gio qua han nhan cong them thoi gian 1 phut
    [Documentation]    Thuc hien kiem tra bo hen gio qua han nhan cong them thoi gian 1 phut
    ...    tat am thong bao notify - bo hen gio chay lai tu dau va them 1 phut
    [Return]    ${get_text_expected}
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    Sleep    2
    Nhan nut bat dau hen gio
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_them_phut}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}
    Sleep    3
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    3
    AppiumLibrary.Click Element At Coordinates    735    300
    AppiumLibrary.Click Element    ${dh_hg_stop}
    ${get_text_expected}=    AppiumLibrary.Get Element Attribute    ${dh_hg_dong_ho_hien_thi}    text
    Page Should Contain Text    ${get_text_expected}    text
    AppiumLibrary.Element Text Should Be    ${dh_hg_dong_ho_hien_thi}    ${get_text_expected}
Thuc hien thiet lap bo hen gio kiem tra nut xoa khi bo hen gio dang chay
    [Documentation]    Thuc hien thiet lap bo hen gio kiem tra nut xoa khi bo hen gio dang chay
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_6}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    10
    AppiumLibrary.Click Element    ${dh_hg_xoa_thoi_gian_thiet_lap_hen_gio}
Thuc hien thiet lap bo hen gio kiem tra nut xoa khi bo hen gio da tam dung
    [Documentation]    Thuc hien thiet lap bo hen gio kiem tra nut xoa khi bo hen gio da tam dung
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_6}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    10
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_xoa_thoi_gian_thiet_lap_hen_gio}
Thuc hien thiet lap bo hen gio moi
    [Documentation]    Thuc hien thiet lap bo hen gio kiem tra nut xoa khi bo hen gio dang chay
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    15
    Thuc hien nhan nut them moi bo hen gio
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    AppiumLibrary.Click Element    ${dh_hg_nut_4}
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    3
    AppiumLibrary.Click Element At Coordinates    735    300
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_stop}
    ${get_text_expected}=    AppiumLibrary.Get Element Attribute    ${dh_hg_dong_ho_hien_thi}    text
    Sleep    1

Thuc hien kiem tra nhap so va check nut xoa
    [Documentation]    Thuc hien kiem tra nhap so va check nut xoa
    Log To Console    Thuc hien kiem tra nhap so va check nut xoa
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_xoa_thoi_gian_thiet_lap}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_4}
    AppiumLibrary.Click Element    ${dh_hg_nut_4}
    # Check hien thi thang ngang hoat dong khi nhap so
    Long Press    ${dh_hg_nut_xoa_thoi_gian_thiet_lap}    1000
    Sleep    4
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_2}
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    Sleep    5
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    # AppiumLibrary.Click Element At Coordinates    542    1904
    Sleep    5
    Log To Console    Thuc hien stop hen gio
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    3
    Log To Console    kiem tra verify dong ho hen gio
    Kiem tra veryfile man hinh hen gio da bat
    Sleep    5
    Log To Console    Thuc hien Play hen gio
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    3
    Log To Console    Thuc hien cong them 1 phut
    AppiumLibrary.Click Element    ${dh_hg_them_phut}
    Sleep    5
    Log To Console    Thuc hien dung lai
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    3
    Log To Console    Thuc hien dat lai ve trang thai ban dau
    Sleep    3
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}
    Sleep    5
    Log To Console    Thuc hien Play lai
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    3
    Log To Console    Thuc hien nhan vao dong ho dung lai
    AppiumLibrary.Click Element At Coordinates    557    1020
    Sleep    3
    Xoa bo hen gio

Thuc hien nhan nut them moi bo hen gio
    [Documentation]    Click nut them moi bo hen gio
    AppiumLibrary.Click Element    ${dh_hg_them_gio_hen}
Thuc hien nhan nut huy them moi bo hen gio
    [Documentation]    Thuc hien nhan nut huy them moi bo hen gio
    AppiumLibrary.Click Element    ${dh_hg_btn_huy_them_bo_hen_gio}
    # AppiumLibrary.Element Should Be Visible    ${dh_hg_btn_huy_them_bo_hen_gio}
Thuc hien thiet lap huy bo hen gio moi
    [Documentation]    Thuc hien thiet lap bo hen gio kiem tra nut huy khi them moi bo hen gio
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_5}
    AppiumLibrary.Click Element    ${dh_hg_nut_6}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    2
    Thuc hien nhan nut them moi bo hen gio
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_nut_8}
    AppiumLibrary.Click Element    ${dh_hg_nut_5}
    Sleep    2
    Thuc hien nhan nut huy them moi bo hen gio
    Sleep    2
    Xoa bo hen gio
    Sleep    2

Thuc hien vuot chuyen tu giao dien bo hen gio 1 den giao dien 2 va 3
    [Documentation]    Check vi tri cua bo giao dien khi thay doi
    Log To Console    check vuot thay doi vi tri
    AppiumLibrary.Click Element    ${dh_hg_nut_6}
    AppiumLibrary.Click Element    ${dh_hg_nut_6}
    AppiumLibrary.Click Element    ${dh_hg_nut_7}
    AppiumLibrary.Click Element    ${dh_hg_nut_7}
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}
    Sleep    4
    Thuc hien nhan nut them moi bo hen gio
    Log To Console    Them bo hen gio thu 2
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    AppiumLibrary.Click Element    ${dh_hg_nut_4}
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}
    Sleep    2
    Log To Console    check hien thi bo gio thu 2
    ${expected}=    AppiumLibrary.Get Element Attribute    ${dh_hg_dong_ho_hien_thi}    text
    Element Attribute Should Match    ${dh_hg_dong_ho_hien_thi}    text    ${expected}
    AppiumLibrary.Element Text Should Be    ${dh_hg_dong_ho_hien_thi}    ${expected}
    Sleep    4
    Thuc hien nhan nut them moi bo hen gio
    Log To Console    Them bo hen gio thu 3
    AppiumLibrary.Click Element    ${dh_hg_nut_9}
    AppiumLibrary.Click Element    ${dh_hg_nut_1}
    AppiumLibrary.Click Element    ${dh_hg_nut_7}
    AppiumLibrary.Click Element    ${dh_hg_nut_8}
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}
    Sleep    2
    Log To Console    check hien thi bo gio thu 3
    ${expected}=    AppiumLibrary.Get Element Attribute    ${dh_hg_dong_ho_hien_thi}    text
    Element Attribute Should Match    ${dh_hg_dong_ho_hien_thi}    text    ${expected}
    AppiumLibrary.Element Text Should Be    ${dh_hg_dong_ho_hien_thi}    ${expected}
    Sleep    4
    Log To Console    check hien thi bo gio thu nhat
    Swipe By Percent    50    90    50    10
    Swipe By Percent    50    90    50    10
    Swipe By Percent    50    10    50    90
    ${get_text_expected}=    AppiumLibrary.Get Element Attribute    ${dh_hg_dong_ho_hien_thi}    text
    Element Attribute Should Match    ${dh_hg_dong_ho_hien_thi}    text    ${get_text_expected}
    AppiumLibrary.Element Text Should Be    ${dh_hg_dong_ho_hien_thi}    ${get_text_expected}
    Log To Console    xoa bo gio thu nhat
    Xoa bo hen gio
    Sleep    2

Thuc hien them 3 bo hen gio va xoa bo 2 giu 1 va 3
    [Documentation]    Check vi tri cua bo giao dien khi thay doi
    Log To Console    them bo hen gio thu nhat
    AppiumLibrary.Click Element    ${dh_hg_nut_6}
    AppiumLibrary.Click Element    ${dh_hg_nut_6}
    AppiumLibrary.Click Element    ${dh_hg_nut_7}
    AppiumLibrary.Click Element    ${dh_hg_nut_9}
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}
    Sleep    4
    Thuc hien nhan nut them moi bo hen gio
    Log To Console    Them bo hen gio thu 2
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    AppiumLibrary.Click Element    ${dh_hg_nut_4}
    AppiumLibrary.Click Element    ${dh_hg_nut_3}
    AppiumLibrary.Click Element    ${dh_hg_nut_8}
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}
    Log To Console    check hien thi bo gio thu 2
    Sleep    4
    Thuc hien nhan nut them moi bo hen gio
    Log To Console    Them bo hen gio thu 3
    AppiumLibrary.Click Element    ${dh_hg_nut_6}
    AppiumLibrary.Click Element    ${dh_hg_nut_7}
    AppiumLibrary.Click Element    ${dh_hg_nut_9}
    AppiumLibrary.Click Element    ${dh_hg_nut_8}
    AppiumLibrary.Click Element    ${dh_hg_bat_dau}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_stop}
    Sleep    2
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}
    Log To Console    check hien thi bo gio thu 3
    Sleep    4
    Log To Console    check hien thi bo gio thu nhat
    Swipe By Percent    50    90    50    10
    Log To Console    xoa bo gio thu nhat
    Xoa bo hen gio
    Sleep    2
    Log To Console    check hien thi cac bo hen gio con lai
    ${get_phan_tu}=    Get Matching Xpath Count    //*[contains(@resource-id,'bkav.android.deskclock:id/page_indicator')]
    Should Be Equal    ${get_phan_tu}    2
    Xoa bo hen gio
    ${get_phan_tu}=    Get Matching Xpath Count    //*[contains(@resource-id,'bkav.android.deskclock:id/page_indicator')]
    Should Be Equal    ${get_phan_tu}    1
    AppiumLibrary.Click Element    ${dh_hg_nut_dat_lai_gio_hen}
    Sleep    2

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================