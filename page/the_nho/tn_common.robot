*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary    

Resource    ../common.robot
Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Mo cai dat va chuyen tu bo nho di dong sang bo nho dien thoai
    Log To Console    Mo cai dat va chuyen tu bo nho di dong sang bo nho dien thoai   
    Mo Cai dat
    Swipe By Percent    50    25    50    10    
    Sleep    2    
    AppiumLibrary.Click Element    ${nut_luu_tru}
    Sleep    2    
    ${trang_thai}    Run Keyword And Return Status    Page Should Contain Element    ${tn_nut_ngat_ket_noi_bn}        
    Run Keyword If    ${trang_thai}==True      Chuyen tu bo nho di dong sang bo nho dien thoai  
    ...    ELSE    Sleep    2    
    
Chuyen tu bo nho dien thoai sang bo nho di dong
    Log To Console    Chuyen tu bo nho dien thoai sang bo nho di dong   
    Click Element    ${tn_sd}
    Sleep    2    
    Click Element    ${cd_nut_tuy_chon_khac}
    Sleep    2    
    Click Element    ${tn_bn_dt_dinh_dang_bo_nho_di_dong}
    Sleep    2    
    Click Element    ${tn_nut_dinh_dang}
    Sleep    15    
    AppiumLibrary.Wait Until Page Contains Element    ${tn_nut_xong}    
    AppiumLibrary.Click Element    ${tn_nut_xong}
    
Chuyen tu bo nho di dong sang bo nho dien thoai
    Log To Console    Chuyen tu bo nho di dong sang bo nho dien thoai    
    Click Element    ${tn_sd}
    Sleep    2    
    Click Element    ${nut_tuy_chon_khac}
    Sleep    2    
    Click Element    ${tn_cd_bo_nho}
    Sleep    2    
    Click Element    ${tn_cd_dinh_dang_bo_nho_trong}
    Sleep    1    
    Click Element    ${tn_dinh_dang_the_SD}
    Sleep    20    
    # AppiumLibrary.Wait Until Page Contains Element    ${tn_di_chuyen_noi_dung}    timeout=25
    AppiumLibrary.Click Element    ${tn_di_chuyen_noi_dung}
    Sleep    30
    # AppiumLibrary.Wait Until Page Contains Element    ${tn_nut_xong}    timeout=30    
    AppiumLibrary.Click Element    ${tn_nut_xong}
    
Click the nho SD va mo tuy chon
    Click Element    ${tn_sd}
    Sleep    2    
    Click Element    ${cd_nut_tuy_chon_khac}
    Sleep    2    
    
Mo cai dat tu launcher va navigate den luu tru
    Log To Console    Mo cai dat tu launcher va navigate den luu tru   
    Mo Cai dat tu Launcher
    Swipe By Percent    50    25    50    10    
    Sleep    2    
    AppiumLibrary.Click Element    ${nut_luu_tru}
    Sleep    2    
    ${trang_thai}    Run Keyword And Return Status    Page Should Contain Element    ${tn_nut_ngat_ket_noi_bn}        
    Run Keyword If    ${trang_thai}==True      Chuyen tu bo nho di dong sang bo nho dien thoai  
    ...    ELSE    Sleep    2    
        
        
    









     
    








#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================