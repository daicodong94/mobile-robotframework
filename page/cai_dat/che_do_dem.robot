*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot

*** Keywords ***
Nhap vao tab hien thi cai dat
    Click Element    ${cdd_text_hien_thi}
    Sleep    1

Nhap vao tab che do dem hien thi cai dat
    Click Element    ${cdd_tab_che_do_dem}
    Sleep    1

Kiem tra lich bieu mac dinh hien thi khong co
    [Documentation]    Kiem tra lich bieu mac dinh hien thi khong co
    ${kiem_tra}=    AppiumLibrary.Get Matching Xpath Count      ${cdd_che_do_hoang_hon}
    Run Keyword If    ${kiem_tra}==0   Nhan nut bat check list lich bieu
    Sleep    1

Kiem tra lich bieu hien thi thoi gian tuy chinh
    [Documentation]    Kiem tra lich bieu hien thi thoi gian tuy chinh
    ${kiem_tra}=    AppiumLibrary.Get Matching Xpath Count    ${cdd_che_do_thoi_gian}
    Run Keyword If    ${kiem_tra}==0    Nhan nut bat check list lich bieu
    Sleep    1

Kiem tra lich bieu hien thi hoang hon den binh minh
    [Documentation]    Kiem tra lich bieu hien thi hoang hon den binh minh
    ${kiem_tra}=    AppiumLibrary.Get Matching Xpath Count    ${cdd_che_do_hoang_hon}
    Run Keyword If    ${kiem_tra}==0    Nhan nut bat check list lich bieu
    Sleep    1

Nhan nut bat check list lich bieu
    Click Element    ${cdd_lich_bieu}
    Sleep    1

Nhan nut bat che do khong co tai lich bieu
    Click Element    ${cdd_che_do_khong_co}
    Sleep    1

Nhan nut bat che do thoi gian tuy chinh tai lich bieu
    Click Element    ${cdd_che_do_thoi_gian}
    Sleep    1

Nhan nut bat che tu hoang hon den binh minh tai lich bieu
    Click Element    ${cdd_che_do_hoang_hon}
    Sleep    1

Nhan nut bat ngay che do dem
    Click Element    ${cdd_bat_che_do_dem}
    Sleep    1

Nhan nut tat ngay che do dem
    Click Element    ${cdd_tat_che_do_dem}
    Sleep    2

Click toa do bat che do dem
    Click Element At Coordinates    ${cdd_nhan_toa_do_x}    ${cdd_nhan_toa_do_y}
    Sleep    3

Click toa do tat che do dem
    Click Element At Coordinates    ${cdd_nhan_toa_do_x}    ${cdd_nhan_toa_do_y}
    Sleep    3

Kiem tra hien thi da bat che do dem
    ${get_text}=    Get Text    ${cdd_tat_che_do_dem}
    Element Attribute Should Match    ${cdd_tat_che_do_dem}    text    ${cdd_text_tat_che_do_dem}
    Sleep    1
    Page Should Not Contain Element    ${cdd_bat_che_do_dem}
    Sleep    1

Kiem tra hien thi che do bat theo thoi gian tuy chinh
    ${get_text1}=    Get Text    ${cdd_bat_vao_thoi_gian_tuy_chinh}
    Element Attribute Should Match    ${cdd_bat_vao_thoi_gian_tuy_chinh}    text    *${cdd_text_thoi_gian_tuy_chinh}*
    Sleep    1

Kiem tra hien thi che do bat hoang hon den binh minh
    ${get_text}=    Get Text    ${cdd_bat_den_binh_minh}
    Element Attribute Should Match    ${cdd_bat_den_binh_minh}    text    ${cdd_text_den_binh_minh}
    Sleep    1

Kiem tra hien thi da tat che do dem
    ${get_text}=    Get Text    ${cdd_bat_che_do_dem}
    Element Attribute Should Match    ${cdd_bat_che_do_dem}    text    ${cdd_text_bat_che_do_dem}
    Sleep    1
    Page Should Not Contain Element    ${cdd_tat_che_do_dem}
    Sleep    1

Bam vao Cai dat tren man hinh launcher
    [Documentation]    Bam vao Cai dat tren man hinh  Launcher
    AppiumLibrary.Click Element    ${app_cai_dat}
    Sleep    5
    
Bam vao He thong 
    [Documentation]    Bam vao He thong
    Click Element    ${cd_nut_he_thong}
    Sleep    2
    
Bam vao Ngay va gio
    [Documentation]    Bam vao ngay va gio
    Click Element    ${cd_ngay_va_gio}
    Sleep    2    
    
Bam vao Su dung dinh dang 24 gio
    [Documentation]    Bam vao Su dung dinh dang 24h
    Click Element    ${cd_su_dung_dinh_dang_24_gio}
    Sleep    2   

Bam vao thoi gian bat dau
    [Documentation]    Bam vao thoi gian bat dau
    Click Element    ${cdd_thoi_gian_bat_dau}
    Sleep    2    
    
Bam vao dong y
    [Documentation]    Bam vao dong y
    Click Element    ${nut_dong_y_hoa}
    Sleep    2

Bam vao huy
    [Documentation]    Bam vao huy
    Click Element    ${nut_huy_hoa}
    Sleep    2    
        
Bam chon 2gio15phut
    [Documentation]    BAM CHON 2H15PHUT
    Click Element    ${cdd_dong_ho_2gio}
    Sleep    1    
    Click Element    ${cdd_dong_ho_15phut}
    Sleep    1       
  
Bam chon 22h
    [Documentation]    Bam chon 22h
    Click Element    ${cdd_dong_ho_22gio}
    Sleep    2    
    Click Element    ${cdd_dong_ho_00phut}
    Sleep    2    
    
Cham vao icon ban phim
    [Documentation]    Cham vao icon ban phim
    Click Element    ${cdd_chuyen_sang_dong_ho_so}
    Sleep    2    
      
Input gio tren dong ho so
    [Documentation]    Input gio tren dong ho so
    Input Text    ${cdd_input_gio_dong_ho_so}    3
    Sleep    2    
    
Input phut tren dong ho so
    [Documentation]    Input phut tren dong ho so
    Input Text    ${cdd_input_phut_dong_ho_so}    20
    Sleep    2    
    
Input gio khong hop le vao gio
    [Documentation]    Input gio khong hop le vao gio
    Input Text    ${cdd_input_gio_dong_ho_so}    24
    Sleep    2    
    
Input gio khong hop le vao phut
    [Documentation]    Input gio khong hop le vao phut
    Input Text    ${cdd_input_phut_dong_ho_so}    60
    Sleep    2
   
Input 23 vao gio
    [Documentation]    Input gio khong hop le vao gio
    Input Text    ${cdd_input_gio_dong_ho_so}    23
    Sleep    2    
   
Input 59 vao phut  
    [Documentation]    Input gio khong hop le vao phut
    Input Text    ${cdd_input_phut_dong_ho_so}    59
    Sleep    2

Bam vao icon dong ho
    [Documentation]    Bam vao icon dong ho
    Click Element    ${cdd_icon_dong_ho}
    Sleep    2    
    
Fake time chung
    [Documentation]    Fake time chung
    ${kiem_tra1}    Run Keyword And Return Status    Element Attribute Should Match    ${cd_su_dung_thoi_gian_do_mang_cung_cap}    checked    false        
    Run Keyword If    ${kiem_tra1}==False    Click Element       ${cd_su_dung_thoi_gian_do_mang_cung_cap}
    
Bam vao thoi gian ket thuc
    [Documentation]    Bam vao thoi gian ket thuc
    Click Element    ${cdd_thoi_gian_ket_thuc}
    Sleep    2    
    
Bam chon 6h
    [Documentation]    Bam chon 6h
    Click Element    ${cdd_dong_ho_6gio}
    Sleep    2    
    Click Element    ${cdd_dong_ho_00phut}
    Sleep    2    
    
Bat dinh dang 24h 
    [Documentation]    Bat dinh dang 24h
    ${kiem_tra}    Run Keyword And Return Status    Element Attribute Should Match    ${cd_su_dung_dinh_dang_24_gio}    checked    true 
    Run Keyword If   ${kiem_tra}==False     Click Element   ${cd_su_dung_dinh_dang_24_gio}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================