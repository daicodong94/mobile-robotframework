*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot

*** Keywords ***
Nhap vao ma pin
    Click Element    ${nut_sao}
    Click Element    ${nut_8}
    Click Element    ${nut_7}
    Click Element    ${nut_2}
    Click Element    ${nut_4}
    Click Element    ${nut_6}
    Click Element    ${nut_4}
    Click Element    ${nut_4}
    Click Element    ${nut_4}
    Click Element    ${nut_3}
    Click Element    ${nut_6}
    Click Element    ${nut_thang}
    Sleep    1

Nhan nut quay so
    Click Element    ${dt_nut_quay_so}
    Sleep    1

Kiem tra hien thi man hinh ma pin
    ${get_text}=    Get Text    ${dt_text_hien_thi_ma_pin}
    Element Attribute Should Match    ${dt_text_hien_thi_ma_pin}    text    ${dt_text_ma_pin}
    Sleep    3

Nhan vao o nhap ma pin
    Click Element    ${dt_input_ma_pin}
    Input Text    ${dt_input_ma_pin}    2528
    Sleep    1

Nhan nut bat che do trai nghiem
    Click Element    ${nut_tat}
    Sleep    1

Nhan nut tat che do trai nghiem
    Click Element    ${nut_bat}
    Sleep    1

Kiem tra trang thai da bat tra nghiem
    Page Should Contain Element    ${nut_bat}
    Sleep    1

Kiem tra thong bao khi khong lap sim
    Log To Console    Kiem tra thong bao khi khong lap sim
    ${kiem_tra_popup1} =  AppiumLibrary.Get Matching Xpath Count    ${dt_txt_loi_ko_lap_sim}
    Run Keyword If    ${kiem_tra_popup1}==0    Kiem tra hien thi popup chon sim
    ...    ELSE    Fail    YEU CAU LAP SIM DE CHAY TEST CASE NAY

Kiem tra hien thi popup chon sim
    ${kiem_tra_popup1} =  AppiumLibrary.Get Matching Xpath Count    ${dt_nut_chon_sim_goi}
    Run Keyword If    ${kiem_tra_popup1}>0   CLick vao chon sim 1
    ...    ELSE    Kiem tra hien thi man hinh ma pin

CLick vao chon sim 1
    ${dt_nut_chon_sim_goi1}    Set Variable    xpath=(${dt_nut_chon_sim_goi})[1]
    AppiumLibrary.Click Element    ${dt_nut_chon_sim_goi1}
    Sleep    1

Click thoat popup huong dan che do
    AppiumLibrary.Click Element    ${dt_nut_da_hieu_huong_dan}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================