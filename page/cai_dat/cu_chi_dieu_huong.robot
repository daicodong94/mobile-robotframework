*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Nhan vao tab dieu huong
    Click Element    ${cd_nut_dieu_huong}
    Sleep    1

Nhan vao tab dieu huong bang cu chi
    Click Element    ${cd_tab_bang_cu_chi}
    Sleep    1

Nhan vao tab dieu huong bang thanh dieu huong
    Click Element    ${cd_tab_bang_thanh_dieu_huong}
    Sleep    1

Nhan vao nut icon tat cu chi dieu huong
    Click Element    ${nut_bat}
    Sleep    1

Nhan vao nut icon bat cu chi dieu huong
    Click Element    ${nut_tat}
    Sleep    1

Kiem tra hien thi mac dinh dieu huong bang cu chi
    Element Attribute Should Match    ${cd_tab_bang_cu_chi}    text    ${cd_text_bang_cu_chi}
    Sleep    1

Kiem tra hien thi tai trang cai dat
    Element Attribute Should Match    ${cd_text_cai_dat}    text    ${cd_hien_thi_text_cai_dat}
    Sleep    1

Nhan mo thanh tang giam do sang
    Click A Point    ${bam_cu_chi_mo_thanh_tang_giam_do_sang_x}    ${bam_cu_chi_mo_thanh_tang_giam_do_sang_y}    1000
    Sleep    1

Nhan mo thanh tang giam am thanh
    Click A Point    ${bam_cu_chi_mo_thanh_tang_giam_am_thanh_x}    ${bam_cu_chi_mo_thanh_tang_giam_am_thanh_y}    1000
    Sleep    1

Vuot cu chi giam do sang
    Swipe    ${cu_chi_bat_dau_giam_do_sang_toa_do_x}    ${cu_chi_bat_dau_giam_do_sang_toa_do_y}    ${cu_chi_ket_thuc_giam_do_sang_toa_do_x}    ${cu_chi_ket_thuc_giam_do_sang_toa_do_y}    duration=1000
    Sleep    1

Vuot cu chi tang do sang
    Swipe    ${cu_chi_bat_dau_tang_do_sang_toa_do_x}    ${cu_chi_bat_dau_tang_do_sang_toa_do_y}    ${cu_chi_ket_thuc_tang_do_sang_toa_do_x}    ${cu_chi_ket_thuc_tang_do_sang_toa_do_y}    duration=1000
    Sleep    1

Vuot cu chi giam am thanh
    Swipe    ${cu_chi_bat_dau_giam_am_thanh_toa_do_x}    ${cu_chi_bat_dau_giam_am_thanh_toa_do_y}    ${cu_chi_ket_thuc_giam_am_thanh_toa_do_x}    ${cu_chi_ket_thuc_giam_am_thanh_toa_do_y}    duration=1000
    Sleep    1

Vuot cu chi tang am thanh
    Swipe    ${cu_chi_bat_dau_tang_am_thanh_toa_do_x}    ${cu_chi_bat_dau_tang_am_thanh_toa_do_y}    ${cu_chi_ket_thuc_tang_am_thanh_toa_do_x}    ${cu_chi_ket_thuc_tang_am_thanh_toa_do_y}    duration=1000
    Sleep    1

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================