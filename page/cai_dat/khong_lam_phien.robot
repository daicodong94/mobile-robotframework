*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../common.robot
Resource    ../config/mac_dinh.robot

*** Keywords ***
Cham vao nut khong lam phien bang toa do
    Click Element At Coordinates    ${klp_cham_vao_toa_do_x}    ${klp_cham_vao_toa_do_y}
    Sleep    2

Mo kich hoat che do khong lam phien trong cai dat
    CLick vao cai dat am thanh
    Click vao text khong lam phien

CLick vao cai dat am thanh
    Click Element    ${cd_nut_am_thanh}
    Sleep    1

Click vao text khong lam phien
    Click Element    ${klp_cham_vao_text_khong_lam_phien}
    Sleep    1

Click vao bat khong lam phien
    Click Element    ${klp_text_bat_ngay_bay_gio}
    Sleep    1

Click vao tat khong lam phien
    Click Element    ${klp_text_tat_ngay_bay_gio}
    Sleep    1

Kiem tra hien thi da tat khong lam phien
    ${get_text}=    Get Text    ${klp_text_bat_ngay_bay_gio}
    Element Attribute Should Match    ${klp_text_bat_ngay_bay_gio}    text    ${klp_text_hien_thi_bat}
    Sleep    2
    Page Should Not Contain Element    ${klp_text_tat_ngay_bay_gio}

Kiem tra hien thi da bat khong lam phien
    ${get_text}=    Get Text    ${klp_text_tat_ngay_bay_gio}
    Element Attribute Should Match    ${klp_text_tat_ngay_bay_gio}    text    ${klp_text_hien_thi_tat}
    Sleep    2
    Page Should Not Contain Element    ${klp_text_bat_ngay_bay_gio}

Bat khong lam phien neu hien thi popup
    Log To Console    Cho phep cap quyen cho ung dung
    ${kiem_tra_popup1} =  AppiumLibrary.Get Matching Xpath Count    ${nut_bat}
    Run Keyword If    ${kiem_tra_popup1}>0   AppiumLibrary.Click Element    ${nut_bat}
    Sleep    1
    ${kiem_tra_popup2}    AppiumLibrary.Get Matching Xpath Count    ${nut_bat}
    Run Keyword If    ${kiem_tra_popup2}>0    Click toa do vao nut bat cai dat
    Sleep    1
    Run Keyword If    ${kiem_tra_popup1}==0 and ${kiem_tra_popup2}==0    Click toa do vao nut bat cai dat

Click toa do vao nut bat cai dat
    Click Element At Coordinates    ${klp_bat_dau_toa_do_x}    ${klp_bat_dau_toa_do_y}
    Sleep    1

Kiem tra hien thi mac dinh bat che do lam phien luon hoi
    Log To Console    Kiem tra hien thi mac dinh bat che do lam phien luon hoi
    ${kiem_tra_popup1} =     AppiumLibrary.Get Matching Xpath Count    ${klp_text_luon_hoi}
    Run Keyword If    ${kiem_tra_popup1}==0    Chon thoi luong mac dinh la luon hoi

Chon thoi luong mac dinh la luon hoi
    Click Element    ${klp_text_thoi_luong_mac_dinh}
    Sleep    1
    Click Element    ${klp_text_luon_hoi}
    Sleep    1
    Click Element    ${nut_dong_y_hoa}
    Sleep    1

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================