*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot

*** Keywords ***
Mo cai dat va vao ung dung va thong bao
    [Documentation]    Mo cai dat va vao ung dung va thong bao
    Mo Cai dat tu Launcher
    An nut ung dung va thong bao
    Sleep    2

An nut ung dung va thong bao
    [Documentation]    An nut ung dung va thong bao
    Click Element    ${cd_nut_ung_dung_va_thong_bao}
    Sleep    2

An nut xem tat ca ung dung
    [Documentation]    An nut xem tat ca ung dung
    Click Element    ${cd_xem_tat_ca_ung_dung}
    Sleep    2

An nut tuy chon khac
    [Documentation]    An nut tuy chon khac
    Click Element    ${dh_cd_tuy_chon_khac}
    Sleep    2

An nut hien thi he thong
    [Documentation]    An nut hien thi he thong
    Click Element    ${cd_hien_thi_he_thong}
    Sleep    2

An nut tim kiem thong tin ung dung
    [Documentation]    An nut tim kiem thong tin ung dung
    Click Element    ${cd_nut_tim_kiem_thong_tin_ung_dung}
    Sleep    2

An nut nang cao
    [Documentation]    An nut nang cao
    Click Element    ${cd_nut_nang_cao}
    Sleep    2

An vao ung dung bo nho phuong tien
    [Documentation]    An vao ung dung bo nho phuong tien
    Click Element    ${cd_ung_dung_bo_nho_phuong_tien}
    Sleep    2

An nut mo theo mac dinh
    [Documentation]    An nut mo theo mac dinh
    Click Element    ${cd_mo_theo_mac_dinh}
    Sleep    2

An nut xoa mac dinh
    [Documentation]    An nut xoa mac dinh
    Click Element    ${cd_nut_xoa_mac_dinh}
    Sleep    2

Xoa mac dinh bo nho phuong tien
    [Documentation]    Xoa mac dinh bo nho phuong tien
    Mo cai dat va vao ung dung va thong bao
    An nut xem tat ca ung dung
    An nut tuy chon khac
    An nut hien thi he thong
    An nut tim kiem thong tin ung dung
    AppiumLibrary.Input Text    ${cd_thanh_tim_kiem}    Bộ nhớ phương
    An vao ung dung bo nho phuong tien
    An nut nang cao
    Vuot sang man hinh ben duoi
    Sleep    2
    An nut mo theo mac dinh
    Sleep    2
    An nut xoa mac dinh

An vao ung dung quan ly file
    [Documentation]    An vao ung dung quan ly file
    Click Element    ${cd_ung_dung_quan_ly_file}
    Sleep    2

Xoa mac dinh Quan ly file
    [Documentation]    Xoa mac dinh quan ly file
    Mo cai dat va vao ung dung va thong bao
    An nut xem tat ca ung dung
    An nut tuy chon khac
    An nut hien thi he thong
    An nut tim kiem thong tin ung dung
    AppiumLibrary.Input Text    ${cd_thanh_tim_kiem}    Quản lý file
    An vao ung dung quan ly file
    An nut nang cao
    Vuot sang man hinh ben duoi
    An nut mo theo mac dinh
    An nut xoa mac dinh

Mo cai dat vao He thong
    [Documentation]    Mo cai dat vao he thong
    Mo Cai dat
    Vuot sang man hinh ben duoi
    Vuot sang man hinh ben duoi
    Click Element    ${cd_nut_he_thong}
    Sleep    2

An nut Ngon ngu va nhap lieu
    [Documentation]    An nut Ngon ngu va nhap lieu
    Click Element    ${cd_nut_ngon_ngu_va_nhap_lieu}
    Sleep    2

An nut Ngon ngu chon ngon ngu English
    [Documentation]    An nut Ngon ngu chon ngon ngu English
    Click Element    ${cd_nut_ngon_ngu}
    Sleep    2
    Click Element    ${cd_nut_english}
    Sleep    2

Click toa do vao cai dat tai dashboard
    Vuot mo bang dieu khien
    Click toa do bat cai dat

Click toa do bat cai dat
    Click Element At Coordinates    ${cd_nhan_toa_do_x}    ${cd_nhan_toa_do_y}
    Sleep    3

Click toa do bat wifi
    Click Element At Coordinates    ${cd_wifi_nhan_toa_do_x}    ${cd_wifi_nhan_toa_do_y}
    Sleep    3

Cham giu toa do bat wifi vao trong cai dat
    Click A Point   ${cd_wifi_nhan_toa_do_x}    ${cd_wifi_nhan_toa_do_y}    1000
    Sleep    3

Click tab mo mang va internet
    Click Element    ${cd_nhan_mang_va_internet}
    Sleep    1

Click vao tab wifi trong cai dat
    Click Element    ${cd_tab_wifi}
    Sleep    1

Click vao bat wifi trong cai dat
    Click Element    ${nut_tat}
    Sleep    1

Click vao tat wifi trong cai dat
    Click Element    ${nut_bat}
    Sleep    1

Check kiem tra nut bat wifi dang hoat dong
    Page Should Contain Element    ${nut_bat}
    Sleep    1

Check kiem tra nut bat wifi dang khong hoat dong
    Page Should Contain Element    ${nut_tat}
    Sleep    1

Kiem tra ket noi wifi thanh cong
    ${get_text}=    Get Text    ${cd_text_da_ket_noi}
    Element Attribute Should Match    ${cd_text_da_ket_noi}    text    ${cd_hien_thi_text_da_ket_noi}
    Sleep    1

cho ket noi wifi
    Log To Console    cho ket noi wifi
    ${kiem_tra} =    Run Keyword And Return Status    AppiumLibrary.Wait Until Page Contains Element    ${cd_text_da_ket_noi}    timeout=30
    Run Keyword If    ${kiem_tra}==True    Kiem tra ket noi wifi thanh cong

cho hien thi danh sach wifi
    Log To Console    cho hien thi danh sach wifi
    ${kiem_tra} =    Run Keyword And Return Status    AppiumLibrary.Wait Until Page Contains Element    ${cd_wifi_name}    timeout=30
    Run Keyword If    ${kiem_tra}==True    Click vao ten wifi muon ket noi trong cai dat

Click vao ten wifi muon ket noi trong cai dat
    Click Element    ${cd_wifi_name}
    Sleep    1

Input password ket noi wifi
    Input Password    ${cd_wifi_password}    Bkav@@QA
    Sleep    1

Kiem tra hien thi dung du lieu da nhap trong password
    ${get_text}    Get Text    ${cd_wifi_password}
    Should Be Equal    Bkav@@QA    ${get_text}
    Sleep    1

Click checkbox hien thi password kieu du lieu text
    Click Element    ${cd_wifi_show_password}
    Sleep    1

Kiem tra da an pop up mat khau
    Page Should Not Contain Element    ${cd_wifi_show_password}
    Sleep    1

Click vao dong y ket noi wifi
    Click Element    ${cd_nut_ket_noi_wifi}
    Sleep    1

Click vao huy ket noi wifi
    Click Element    ${nut_huy_hoa}
    Sleep    1

An vao Hien thi
    Click Element    ${cd_nut_hien_thi}
    Sleep    2

An vao kich thuoc phong chu
    Click Element    ${cd_kich_thuoc_phong_chu}
    Sleep    2

An vao kich thuoc hien thi
    Click Element    ${cd_kich_thuoc_hien_thi}
    Sleep    2

An vao kich thuoc phong chu nho
    Click Element    ${cd_kich_thuoc_phong_chu_nho}
    Sleep    2

An vao kich thuoc phong chu mac dinh
    Click Element    ${cd_kich_thuoc_phong_chu_mac_dinh}
    Sleep    2

An vao kich thuoc phong chu lon
    Click Element    ${cd_kich_thuoc_phong_chu_lon}
    Sleep    2

An vao kich thuoc phong chu lon nhat
    Click Element    ${cd_kich_thuoc_phong_chu_lon_nhat}
    Sleep    2

An vao kich thuoc hien thi nho
    Click Element    ${cd_kich_thuoc_hien_thi_nho}
    Sleep    2

An vao kich thuoc hien thi mac dinh
    Click Element    ${cd_kich_thuoc_hien_thi_mac_dinh}
    Sleep    2

An vao kich thuoc hien thi lon
    Click Element    ${cd_kich_thuoc_hien_thi_lon}
    Sleep    2

Bam chuyen kich thuoc phong chu va hien thi ve max
    An vao Hien thi
    An nut nang cao
    An vao kich thuoc hien thi
    An vao kich thuoc hien thi lon
    Go Back
    An vao kich thuoc phong chu
    An vao kich thuoc phong chu lon nhat
Bam chuyen kich thuoc phong va hien thi ve mac dinh
    An vao Hien thi
    An nut nang cao
    An vao kich thuoc phong chu
    An vao kich thuoc phong chu mac dinh
    Go Back
    An vao kich thuoc hien thi
    An vao kich thuoc phong chu mac dinh

Bam chuyen kich thuoc phong va hien thi ve min
    An vao Hien thi
    An nut nang cao
    An vao kich thuoc phong chu
    An vao kich thuoc phong chu nho
    Go Back
    An vao kich thuoc hien thi
    An vao kich thuoc hien thi nho

An vao dieu huong
    Click Element    ${cd_nut_dieu_huong}
    Sleep    2

An vao Cach dieu huong
    Click Element    ${cd_cach_dieu_huong}
    Sleep    2

An vao Dieu huong Bang thanh dieu huong
    Click Element    ${cd_dieu_huong_bang_thanh_dieu_huong}
    Sleep    2

An vao dieu huong bang cu chi
    Click Element    ${cd_dieu_huong_bang_cu_chi}
    Sleep    2

Bat thanh dieu huong
    An vao dieu huong
    An vao Cach dieu huong
    An vao Dieu huong Bang thanh dieu huong

Tat thanh dieu huong
    An vao dieu huong
    An vao Cach dieu huong
    An vao dieu huong bang cu chi

Vuot sang trai launcher va click app cai dat
    [Documentation]    Vuot sang trai launcher va click app cai dat
    Vuot sang man hinh ben phai
    Sleep    2
    Click Element    ${app_cai_dat}
    Sleep    2

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================