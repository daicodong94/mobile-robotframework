*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../config/mac_dinh.robot
Resource    ../common.robot
Resource    ../launcher/launcher_common.robot

*** Variable***

*** Keywords ***
Bam vao website huong dan su dung
    [Documentation]    Bam vao website huong dan su dung
    Click Element    ${eSim_khong_sim_link_Bphone.vn}
    Sleep    2

Bam vao button mong muon ho so sim 1    
    [Documentation]    Bam vao button mong muon ho so dang active
    ${kiem_tra}    Run Keyword And Return Status    Element Attribute Should Match    ${eSim_co_sim_chon_checkbox_sim_1}    checked    False    
    Run Keyword If    ${kiem_tra}==True    Click Element    ${eSim_co_sim_chon_checkbox_sim_1}
    Sleep    1
    
Bam bo chon checkbox active sim 1   
    [Documentation]    Bam vao button mong muon ho so dang active
    ${kiem_tra}    Run Keyword And Return Status    Element Attribute Should Match    ${eSim_co_sim_chon_checkbox_sim_1}    checked    true    
    Run Keyword If    ${kiem_tra}==True    Click Element    ${eSim_co_sim_chon_checkbox_sim_1}
    Sleep    2
    
Bam vao button mong muon ho so sim 2   
    [Documentation]    Bam vao button mong muon ho so dang active
    Click Element    ${eSim_co_sim_chon_checkbox_sim_2}
    Sleep    2

Bam vao them ho so khay sim 1
    [Documentation]    Bam vao them ho so
    Click Element    ${eSim_add_icon_sim_1}
    Sleep    2

Bam vao dong
    [Documentation]    Bam vao dong
    Click Element    ${eSim_co_sim_dong}
    Sleep    3

vuot tu trai sang dong ma QR
    [Documentation]    vuot tu trai sang dong ma QR
    Swipe By Percent    0    50    50    50
    Sleep    2

vuot tu phai sang dong ma QR
    [Documentation]    vuot tu trai sang dong ma QR
    Swipe By Percent    99    50    45     50
    Sleep    2

bam vao profile nha mang - sim 1
    [Documentation]    bam vao profile nha mang
    Click Element    ${eSim_co_sim_icon_sim}
    Sleep    2

bam vao so dien thoai trong ho so
    [Documentation]    bam vao so dien thoai trong ho so
    Click Element    ${eSim_co_sim_hs_so_dt_sim}
    Sleep    2

clear text so dien thoai
    [Documentation]    clear text
    AppiumLibrary.Clear Text    ${eSim_co_sim_hs_so_dt_sim}
    Sleep    1

nhap text
    [Documentation]    nhap text
    Input Text    ${eSim_co_sim_hs_so_dt_sim}      0977888999
    Sleep    1

bam vao luu
    [Documentation]    bam vao luu
    Click Element    ${eSim_co_sim_hs_sim_nut_luu}
    Sleep    1

bam vao huy
    [Documentation]    bam vao huy
    Click Element    ${eSim_co_sim_hs_sim_nut_huy}
    Sleep    3
    
Bam vao Dong y
    [Documentation]    Bam vao dong y
    Click Element    ${eSim_co_sim_hs_sim_nut_dong_y}
    Sleep    2    

nhap so dien thoai moi
    [Documentation]    nhap so dien thoai moi
    Input Text      ${eSim_co_sim_hs_so_dt_sim}    0988999999
    Sleep    2

bam vao combobox mau sac
    [Documentation]    bam vao combobox mau sac
    Click Element    ${eSim_co_sim_hs_icon_mau_sim}
    Sleep    2

bam vao esim 2 viettel
    [Documentation]    bam vao esim 2 viettel
    Click Element At Coordinates    400    600
    Sleep    1

bam vao xoa ho so
    [Documentation]    bam vao xoa ho so
    Click Element    ${eSim_co_sim_hs_xoa}
    Sleep    2

vuot tu mep trai vao giua man hinh
    [Documentation]    vuot tu mep trai vao giua man hinh
    Swipe By Percent    1    50    50    50
    Sleep    2    
    
Mo eSIM tu Launcher khi co thong bao
    [Documentation]    Tim va mo ung dung eSIM tu Launcher roi cho vai giay de ung dung on dinh
    ${so_man_launcher_int}    Convert To Integer    ${so_man_launcher}
    FOR    ${i}    IN RANGE    0    ${so_man_launcher_int+1}
        Run Keyword If    ${i}==${so_man_launcher_int}    Vuot sang man hinh ben phai de tim ung dung eSIM
        Log To Console     Tim ung dung eSIM tren Launcher
        ${kiem_tra_launcher}    AppiumLibrary.Get Matching Xpath Count    ${app_esim}
        Run Keyword If    ${kiem_tra_launcher}==0    Vuot sang man hinh ben trai
        ...    ELSE    Bam vao eSIM tren Launcher
    END
    Log To Console     Xac nhan mo ung dung eSIM thanh cong
    AppiumLibrary.Page Should Contain Element    ${eSim_co_sim_thong_bao}

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================