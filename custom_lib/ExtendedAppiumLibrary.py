import cv2
import os
from robot.api import logger

class ExtendedAppiumLibrary:

    ROBOT_LIBRARY_SCOPE = 'GLOBAL'
    ROBOT_LIBRARY_VERSION = 1.0

    def compare_two_images(self, image1_path, image2_path, different_image, threshold=5):
        """
        Compare two images
    
        image1_path: The absolute path to image 1 in Unix format with file name extension, e.g., /folder/image1.png
    
        image2_path: The absolute path to image 2 in Unix format with file name extension, e.g., /folder/image2.png
    
        different_image: The absolute path to output different image in Unix format with file name extension, e.g., /folder/different_image.png
    
        threshold: An integer to determine the difference of two images. Default = 5%
    
        Assertion cases:
        
        1. The two images have different size or format, raises assertion error

        2. The difference of two images <= threshold, logs info "These two images are similar."
    
        3. The difference of two images > threshold, raises assertion error and outputs a different image = image 1 - image 2
        """
        count  = 0
        path1  = image1_path.replace('/', os.sep)
        image1 = cv2.imread(path1)
        path2  = image2_path.replace('/', os.sep)
        image2 = cv2.imread(path2)
    
        area = image1.shape[0] * image1.shape[1]

        # 1. The two images have different size or format, raises assertion error
        if image1.shape != image2.shape:
            raise AssertionError("These two images have different size or format.\nImage 1: %s.\nImage 2: %s." % (image1.shape, image2.shape))
        else:
            for row in range(image1.shape[0]):
                for col in range(image1.shape[1]):
                    if  image1[row][col][0] != image2[row][col][0] \
                    and image1[row][col][1] != image2[row][col][1] \
                    and image1[row][col][2] != image2[row][col][2]:
                        count += 1
    
            percent = count * 100 / area
            
            # 2. The difference of two images <= threshold, logs info "These two images are similar."
            if percent <= threshold:
                logger.info("The difference of two images = %.2f%% is less than or equal to threshold = %s%% => These two images are similar." % (percent, threshold))
            # 3. The difference of two images > threshold, raises assertion error and outputs a different image = image 1 - image 2
            else:
                difference = cv2.subtract(image1, image2)
                different_image_path = different_image.replace('/', os.sep)
                cv2.imwrite(different_image_path, difference)
                image_path, different_image_name = os.path.split(different_image_path)
                # Image is shown on its own row and thus prev row is closed on purpose
                logger.info('</td></tr><tr><td colspan="3"><a href="%s"><img src="%s" width="800px"></a>' % (different_image_name, different_image_name), True, False)
                raise AssertionError("The difference of two images = %.2f%% is greater than threshold = %s%%." % (percent, threshold))

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================