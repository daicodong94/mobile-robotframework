*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/launcher/launcher_common.robot
Resource    ../page/xem_phim/xem_video.robot

Suite Setup       Mo Xem phim
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
# BP-3685
    # [Documentation]    Check giao dien man hinh trinh phat video khi hien thi khu dieu khien
    # [Tags]    BP-3685    BP-3684
    # Mo app xem phim va mo xem video
    # Vuot mo bang dieu khien
    # Click Element At Coordinates    886    1239
    # AppiumLibrary.Landscape    
    # Sleep    5       
    # Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_XemPhim_XemVideo_khu_dieu_khien_xoay_ngang    2
    # Log To Console    BP-3684    Check giao dien man hinh fullscreen khi xoay ngang dien thoai 
    # Click Element At Coordinates    540    1220
    # Sleep    1    
    # Page Should Not Contain Element    ${xp_v_nut_overplay}   
    # Close Application
    
BP-3686
    [Documentation]     Check giao dien man hinh trinh phat video khi Bi khoa
    [Tags]    BP-3686
    Cho phep cap quyen cho ung dung
    Click Element    ${xp_v_mo_video_file_mp4}
    Sleep    2    
    Click Element    ${xp_v_sap_xep_theo}
    Sleep    2    
    ${trang_thai}    Run Keyword And Return Status    AppiumLibrary.Page Should Contain Element    ${xp_tm_sap_xep_theo_ngay_moi_nhat} 
    Run Keyword If    ${trang_thai}==True    AppiumLibrary.Click Element    ${xp_tm_sap_xep_theo_ngay_moi_nhat}    
    ...    ELSE    Vuot back ve giao dien truoc 
    Sleep    2    
    Click Element    ${xp_v_video_t5_trong_thu_muc_vd}
    Sleep    2
    ${hien_thi}    Run Keyword And Return Status    Page Should Contain Element    ${xp_tm_text_da_nam_duoc}   
    Run Keyword If    ${hien_thi}==True    Chon da nam duoc tai huong dan su dung khi xem video    
    ...    ELSE    Sleep    1    
    Click Element    ${xp_v_nut_khoa}
    Page Should Contain Text    ${xp_v_text_khoa_video}    
    Click Element At Coordinates    540    1220
    Chup anh man hinh va so sanh voi anh mau    ${xp_alias}    kc_d_ttdh_d_d_XemPhim_XemVideo_khoa_video    5
    Dong toan bo ung dung trong da nhiem
    
BP-3780
    [Documentation]    Check chuyen phat video tiep theo khi cham icon Next trong giao dien trinh phat video         
    [Tags]    BP-3780    BP-3782    BP-3785
    Mo app xem phim va mo xem video   
    Click Element At Coordinates    ${xp_v_khoang_trong_ngoai_v_x}    ${xp_v_khoang_trong_ngoai_v_y}
    Click Element    ${xp_v_nut_function}
    Sleep    2    
    Click Element At Coordinates    ${xp_v_khoang_trong_ngoai_v_x}    ${xp_v_khoang_trong_ngoai_v_y}
    Click Element    ${xp_v_nut_overplay}
    Page Should Contain Text    3_360x240.mp4
    Log To Console    BP-3782    BP-3785    Check tro ve giao dien Danh sach video khi cham icon Next trong giao dien trinh phat video cuoi cung      
    Sleep    2    
    Click Element At Coordinates    ${xp_v_khoang_trong_ngoai_v_x}    ${xp_v_khoang_trong_ngoai_v_y}              
    Click Element    ${xp_v_nut_function}
    Sleep    2    
    Page Should Contain Element    ${xp_v_textview}
    Dong toan bo ung dung trong da nhiem
    
BP-3787
    [Documentation]    Check tu dong chuyen phat video 2 khi phat het video 1
    [Tags]    BP-3787    BP-3789    BP-3791
    Mo app xem phim va mo xem video
    Sleep    31  
    Click Element At Coordinates    ${xp_v_khoang_trong_ngoai_v_x}    ${xp_v_khoang_trong_ngoai_v_y}
    Click Element    ${xp_v_nut_overplay}
    Page Should Contain Text    3_360x240.mp4
    Sleep    2    
    Click Element At Coordinates    ${xp_v_khoang_trong_ngoai_v_x}    ${xp_v_khoang_trong_ngoai_v_y}
    Click Element    ${xp_v_nut_overplay}    
    Log To Console    BP-3789    BP-3791    Check tu dong tro ve giao dien Danh sach video khi phat het video cuoi cung trong danh sach
    Sleep    28    
    Page Should Contain Element    ${xp_v_textview}    
    

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================