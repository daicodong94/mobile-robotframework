*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary
Library     String

Resource    ../page/common.robot
Resource    ../page/ghi_am/ga_common.robot
Resource    ../page/nghe_nhac/nn_common.robot

Suite Setup       Mo Ghi am
Suite Teardown    Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-4382
    [Documentation]    Kiem tra hien thi mac dinh cua chuc nang Cai dat
    [Tags]    BP-4382
    Sleep    2
    Cho phep cap quyen cho ung dung
    Vuot quay ve man hinh Home
    Xoa toan bo du lieu app ghi am trong quan ly file
    Mo Ghi am tu Launcher
    Click Element    ${ga_tab_tuy_chon_khac}
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${ga_alias}    kc_d_ttdh_d_d_Cai_dat    2
    Vuot back ve giao dien truoc
    
BP-4383
    [Documentation]    Kiem tra hien thi popup Ghi ra file khi checked checkbox Tu dat ten
    [Tags]    BP-4383
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    An nut danh sach ban ghi
    Page Should Contain Element    ${ga_tieu_de}
    An nut dong y
    Vuot back ve giao dien truoc

BP-4384
    [Documentation]    Kiem tra tu dong luu file vao Danh sach ban ghi khi unchecked checkbox Tu dat ten
    [Tags]    BP-4384    BP-4385
    Click Element    ${ga_tab_tuy_chon_khac}
    Sleep    2
    Click Element    ${ga_cai_dat_tu_dat_ten_file}
    Sleep    2
    Go Back
    Log To Console    BP-4385    Kiem tra chuc nang Dat ten theo ngay thang khi checked checkbox Dat ten theo ngay thang
    An nut bat dau ghi am
    An nut danh sach ban ghi
    Page Should Contain Element    ${ga_tieu_de}
    ${y}=    Get Time    year
    ${m}=    Get Time    month
    ${d}=    Get Time    day
    ${h}=    Get Time    hour
    ${ten}=    Get Text    ${ga_ban_ghi_ten}
    ${str}=    Get Substring    ${ten}    4    14
    Should Be Equal    ${y}${m}${d}${h}    ${str}
    Vuot back ve giao dien truoc

BP-4386
    [Documentation]    Kiem tra khong dat ten theo ngay thang khi unchecked checkbox Dat ten theo ngay thang
    [Tags]    BP-4386
    Click Element    ${ga_tab_tuy_chon_khac}
    Sleep    2
    Click Element    ${ga_cai_dat_ten_theo_ngay_thang}
    Click Element    ${ga_cai_dat_tu_dat_ten_file}
    Sleep    2
    Go Back
    An nut bat dau ghi am
    An nut danh sach ban ghi
    ${ten_ban_ghi}=    Get Text    ${ga_textbox_ten_ban_ghi}
    Sleep    2
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    Element Text Should Be    ${ga_ban_ghi_ten}    ${ten_ban_ghi}
    Vuot back ve giao dien truoc
    Vuot quay ve man hinh Home

BP-4387
    [Documentation]    Kiem tra an ban ghi khoi trinh choi nhac khi checked checkbox An khoi trinh choi nhac
    [Tags]    BP-4387    
    Tao ban ghi va tick checkbox an khoi trinh choi nhac
    Vuot quay ve man hinh Home
    Mo Nghe nhac tu Launcher
    Sleep    5
    ${hien_thi}    Run Keyword And Return Status    Page Should Contain Element    ${nut_cho_phep_hoa}
    Run Keyword If    ${hien_thi}==True     AppiumLibrary.Click Element    ${nut_cho_phep_hoa}
    ...    ELSE    Sleep    2
    Sleep    5
    AppiumLibrary.Click Element    ${nn_nut_danh_sach}
    Sleep    10    
    # AppiumLibrary.Wait Until Page Contains Element    ${nn_danh_sach_my_recording}    timeout=30
    AppiumLibrary.Click Element    ${nn_danh_sach_my_recording}
    Chup anh man hinh va so sanh voi anh mau    ${ga_alias}    kc_d_ttdh_d_d_Danh_Sach_my_recording    5
    
BP-4388
    [Documentation]    Kiem tra hien thi ban ghi o trinh choi nhac khi unchecked checkbox An khoi trinh choi nhac
    [Tags]    BP-4388    
    Vuot quay ve man hinh Home
    Mo Ghi am tu Launcher
    An nut tuy chon
    Click Element    ${ga_checkbox_an_khoi_trinh_choi_nhac}
    Vuot back ve giao dien truoc
    Vuot quay ve man hinh Home
    Mo Nghe nhac tu Launcher
    An nut tuy chon nghe nhac
    An checkbox an bai hat dung luong thap
    Sleep    2
    Page Should Contain Text    ban ghi 1

BP-4395
    [Documentation]    Kiem tra hien thi popup Cho phep Ghi am truy cap ung dung khong lam phien khi tick checked chuc nang
    [Tags]    BP-4395
    Vuot quay ve man hinh Home
    Mo Ghi am tu Launcher
    ga_common.An nut tuy chon
    Click Element    ${ga_cai_dat_che_do_im_lang}
    Sleep    2
    Page Should Contain Element    ${ga_cai_dat_popup}
    
BP-4396
    [Documentation]    Kiem tra hien thi man hinh Truy cap khong lam phien khi bam Cai dat
    [Tags]    BP-4396     
    Click Element At Coordinates    846    1295
    Sleep    2
    Page Should Contain Element    ${ga_truy_cap_khong_lam_phien}
    
BP-4397
    [Documentation]    On switch Ghi am trong Truy cap khong lam phien, kiem tra am thanh chuyen ve che do im lang khi dang ghi am    
    [Tags]    BP-4397
    Swipe By Percent    50    80    50    30
    Swipe By Percent    50    80    50    30
    Sleep    2
    AppiumLibrary.Click Element    ${app_ghi_am}
    Sleep    5
    Element Attribute Should Match    ${ga_bat_ghi_de_khong_lam_phien}    checked    false
    AppiumLibrary.Click Element    ${ga_bat_ghi_de_khong_lam_phien}
    Sleep    2
    Click Element    ${nut_cho_phep_hoa}
    Sleep    5
    
BP-4399
    [Documentation]    Tick checked Che do im lang, kiem tra am thanh chuyen ve che do im lang khi dang ghi am    
    [Tags]    BP-4399
    Vuot back ve giao dien truoc
    Vuot back ve giao dien truoc
    Vuot back ve giao dien truoc
    Sleep    2
    An nut bat dau ghi am
    Sleep    5
    An nut tam dung ghi am
    Chup anh man hinh va so sanh voi anh mau    ${ga_alias}    kc_d_ttdh_d_d_ghi_am_bat_ghi_de_KLP    2
    An nut danh sach ban ghi
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    Chup anh man hinh va so sanh voi anh mau    ${ga_alias}    kc_d_ttdh_d_d_Ds_ban_ghi_data    5

#=================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================