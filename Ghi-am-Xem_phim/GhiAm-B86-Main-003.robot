*** Comments ***
Canh bao ban quyen: Moi thu trong Du an nay thuoc ban quyen cua Bkav. Nghiem cam moi hanh vi phat tan, sao chep.
Moi viec su dung Du an nay deu phai duoc gui bang van ban va duoc Bkav chap thuan bang van ban.

Copyright Bkav
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

o Neither the name of Bkav nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*** Settings ***
Library     AppiumLibrary

Resource    ../page/common.robot
Resource    ../page/ghi_am/ga_common.robot
Resource    ../page/notification/notification.robot

Suite Setup        Mo Ghi am
Suite Teardown     Dong toan bo ung dung va ket thuc phien kiem thu

*** Test Cases ***
BP-3983
    [Documentation]        Kiem tra dong bo khi doi ten file tai Quan ly file
    [Tags]    BP-3983     
    Cho phep cap quyen cho ung dung
    Vuot quay ve man hinh Home
    Xoa toan bo du lieu app ghi am trong quan ly file
    Mo Ghi am tu Launcher
    An nut bat dau ghi am
    Cho phep cap quyen cho ung dung
    Cho phep cap quyen cho ung dung
    Sleep    10
    Click Element At Coordinates    ${ga_nut_danh_sach_ban_ghi_x}    ${ga_nut_danh_sach_ban_ghi_y}
    An nut dong y
    Vuot back ve giao dien truoc
    Sleep    2
    Vuot quay ve man hinh Home
    Mo quan ly file va doi ten tep ghi am
    Mo da nhiem
    Bam mo ung dung trai trong 2 ung dung tren da nhiem
    Click Element    ${nut_danh_sach_ban_ghi}
    Sleep    2
    Page Should Contain Text    DaiTVb
    Dong toan bo ung dung trong da nhiem
        
BP-4000
    [Documentation]    Kiem tra luu ten file vua bi trung khi da xoa ten file bi trung trong danh sach ban ghi
    [Tags]    BP-4000    BP-4001    BP-3999    
    Mo Ghi am tu Launcher
    An nut danh sach ban ghi
    Sleep    5    
    Long Press    ${ga_doi_ten_file}
    Sleep    2
    Click Element    ${ga_ban_ghi_xoa}
    Sleep    2
    Vuot back ve giao dien truoc
    An nut bat dau ghi am
    Sleep    10
    An nut tam dung ghi am
    Click Element    ${nut_danh_sach_ban_ghi}
    Sleep    2
    Clear Text    ${ga_textbox_ten_ban_ghi}
    Input Text    ${ga_textbox_ten_ban_ghi}    DaiTVb
    Click Element    ${nut_dong_y_hoa}
    Sleep    2
    Page Should Contain Element    ${ga_doi_ten_file}
    Log To Console    BP-4001 : Kiem tra thu muc luu file tren dien thoai
    Vuot quay ve man hinh Home
    Mo thu muc ghi am trong quan ly file
    Page Should Contain Text    DaiTVb
    Log To Console    BP-3999    Kiem tra luu file trung ten voi file da co trong Danh sach ban ghi tai popup Ghi ra file
    Mo da nhiem
    Bam mo ung dung trai trong 2 ung dung tren da nhiem
    Tao file ghi am trung voi file ghi am da co
    Page Should Contain Text    ${ga_text_ten_bi_trung}  
    

# ================================================================================================
#    HET NOI DUNG TAP TIN
#=================================================================================================